//
//  Constants.h
//  Zerved For Merchants
//
//  Created by Zerved Development Mac on 02/08/13.
//  Copyright (c) 2013 Zerved. All rights reserved.
//

#ifndef Zerved_For_Merchants_Constants_h
#define Zerved_For_Merchants_Constants_h

// Print mode
#define PRINT_PENDING_ORDERS      0
#define PRINT_PREPARING_ORDERS    1

// Printing methods
#define PRINT_ORDER_CANCELLED 0 // Prints a cancelled order receipt
#define PRINT_ORDER_MANUALLY  1 // Prints an order manually (when 'Print' button is pressed)
#define PRINT_ORDER_AUTO      2 // Prints an order depending on the printer settings and the order status

#endif
