//
//  ZEStarPrinter.h
//  Zerved For Merchants
//
//  Created by Zerved Development Mac on 02/08/13.
//  Copyright (c) 2013 Zerved. All rights reserved.
//

#import "ZEPrinter.h"
#import <StarIO/SMPort.h>
#import <StarIO/Port.h>

@interface ZEStarPrinter : ZEPrinter<NSCoding>

//@property (strong, nonatomic) SMPort *port;

- (ZEStarPrinter *) initWithName:(NSString*)name andModel:(NSString*)model;

- (id)initWithCoder:(NSCoder *)coder;
- (void) encodeWithCoder:(NSCoder *)coder;


- (BOOL)printImageWithPortname:(NSString *)portName portSettings:(NSString*)portSettings imageToPrint:(UIImage*)imageToPrint maxWidth:(int)maxWidth compressionEnable:(BOOL)compressionEnable withDrawerKick:(BOOL)drawerKick;
- (BOOL)sendCommand:(NSData *)commandsToPrint portName:(NSString *)portName portSettings:(NSString *)portSettings timeoutMillis:(u_int32_t)timeoutMillis;

@end

