//
//  ZEPrintQueue.m
//  Zerved For Merchants
//
//  Created by Anders Rasmussen on 03/12/13.
//  Copyright (c) 2013 Zerved. All rights reserved.
//

#import "ZEPrintQueue.h"

@implementation ZEPrintQueue

- (ZEPrintQueue *)initWithPrinter:(ZEPrinter *)printer
{
    if (self = [super init]) {
        self.orders = [[NSMutableArray alloc] init];
        self.ordersCancelled = [[NSMutableArray alloc] init];
        self.printer = printer;
        return self;
    }
    return nil;
}

- (void)printOrder:(NSDictionary *)order
{
    [self.orders addObject:order];
    [self printOrdersInQueue];
}

- (void)printCancelledOrder:(NSDictionary *)order
{
    [self.ordersCancelled addObject:order];
    [self printOrdersInQueue];
}

- (void)printOrdersInQueue
{
    // Run in a 'background' thread
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        if (self.isPrinting) {
            return; // Already printing
        }
        if ([self.orders count] == 0 && [self.ordersCancelled count] == 0) {
            return; // No orders
        }
        self.isPrinting = YES;
        NSLog(@"Open printer");
        // Opens the printer
        if(![self.printer openPrinterWithIP:self.printer.name]){
            [self performSelectorOnMainThread:@selector(showErrorMsg:) withObject:NSLocalizedString(@"Failed to open printer connection", nil) waitUntilDone:NO];
        }
        // Process queues
        while ([self.orders count] > 0) {
            NSDictionary *order = [self.orders objectAtIndex:0];
            [self.printer printOrder:order toIP:self.printer.name];
            if ([self.orders count] > 0) [self.orders removeObjectAtIndex:0];
        }
        while ([self.ordersCancelled count] > 0) {
            NSDictionary *order = [self.ordersCancelled objectAtIndex:0];
            [self.printer printCancelledOrder:order];
            if ([self.ordersCancelled count] > 0) [self.ordersCancelled removeObjectAtIndex:0];
        }
        // Closes printer
        NSLog(@"Close printer");
        if(![self.printer closePrinter]) {
            [self performSelectorOnMainThread:@selector(showErrorMsg:) withObject:NSLocalizedString(@"Connection to the printer can't be closed", nil) waitUntilDone:NO];
        }
        self.isPrinting = NO;
    });
}

@end
