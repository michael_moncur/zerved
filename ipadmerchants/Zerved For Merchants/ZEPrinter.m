
//
//  ZEPrinter.m
//  Zerved For Merchants
//
//  Created by Zerved Development Mac on 02/08/13.
//  Copyright (c) 2013 Zerved. All rights reserved.
//

#import "ZEPrinter.h"
#import "ZEStarPrinter.h"

@implementation ZEPrinter

-(ZEPrinter *) initWithName: (NSString *)name{
    if(self = [super init]){
        self.name = name;
        self.copies = 1;
        self.printMode = PRINT_PENDING_ORDERS;
    }
    return self;
}

- (NSString *)description
{
    return [NSString stringWithFormat:@"Printer %@ : %@ %@ (Copies: %d, Print mode: %@)",
            self.name,
            self.brand,
            self.model,
            self.copies,
            self.printMode == PRINT_PENDING_ORDERS ? @"(0, pending)" : @"(1, preparing)"];
}

- (void) showErrorMsg:(NSString *)msg {
    [SVProgressHUD showErrorWithStatus:msg];
}

// "Abstract" methods

//      Printer methods
- (BOOL) openPrinterWithIP:(NSString *)ip {return NO; }
- (BOOL) closePrinter { return NO; }
- (NSString *) getPrinterStatus {return @"";}
- (BOOL) isReadyToPrint { return NO; }

//      Printing methods
- (BOOL) printTest { return NO; }
- (BOOL) printOrder:(NSDictionary*)order toIP:(NSString *)deviceName { return NO; }
- (BOOL) printCancelledOrder:(NSDictionary*)order {return NO;}

//      Encoding/decoding methods
- (void)encodeWithCoder:(NSCoder *)coder{}
- (id)initWithCoder:(NSCoder *)coder{ return nil;}


@end
