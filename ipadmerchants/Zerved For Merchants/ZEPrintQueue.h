//
//  ZEPrintQueue.h
//  Zerved For Merchants
//
//  Created by Anders Rasmussen on 03/12/13.
//  Copyright (c) 2013 Zerved. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ZEPrinter.h"

@interface ZEPrintQueue : NSObject

@property (strong, nonatomic) NSMutableArray *orders;
@property (strong, nonatomic) NSMutableArray *ordersCancelled;
@property (strong, nonatomic) ZEPrinter *printer;
@property BOOL isPrinting;

- (ZEPrintQueue *)initWithPrinter:(ZEPrinter *)printer;
- (void)printOrder:(NSDictionary *)order;
- (void)printCancelledOrder:(NSDictionary *)order;

@end
