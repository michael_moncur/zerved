//
//  ZEPrinter.h
//  Zerved For Merchants
//
//  Created by Zerved Development Mac on 02/08/13.
//  Copyright (c) 2013 Zerved. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Constants.h"
#import "SVProgressHUD.h"

@interface ZEPrinter : NSObject<NSCoding>

#define SEND_TIMEOUT              10 * 1000

// Printer information and settings
@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSString *brand;
@property (strong, nonatomic) NSString *model;
@property int copies;
@property int printMode;
@property int printEachOrder;

@property (strong, nonatomic) NSArray *supportedModels;

// Common methods
- (void) showErrorMsg:(NSString *)msg;

// Printer methods
- (ZEPrinter *) initWithName: (NSString *)name;
- (NSString *) description;

// "Abstract" methods
- (BOOL) openPrinterWithIP:(NSString *)ip;
- (BOOL) closePrinter;
- (BOOL) isReadyToPrint;
- (NSString *) getPrinterStatus;
- (BOOL) printTest;
- (BOOL) printOrder:(NSDictionary*)order toIP:(NSString *)deviceName;
- (BOOL) printCancelledOrder:(NSDictionary*)order;
- (void) encodeWithCoder:(NSCoder *)coder;
- (id) initWithCoder:(NSCoder *)coder;

@end
