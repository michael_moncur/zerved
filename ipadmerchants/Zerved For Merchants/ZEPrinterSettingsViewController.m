//
//  ZEPrinterSettingsViewController.m
//  Zerved For Merchants
//
//  Created by Anders Rasmussen on 04/07/13.
//  Copyright (c) 2013 Zerved. All rights reserved.
//

#import "ZEPrinterSettingsViewController.h"
#import "SVProgressHUD.h"
#import "ZEPrinter.h"

#define UPDATE_STATUS_INTERVAL  10.0

@interface ZEPrinterSettingsViewController ()

@end

@implementation ZEPrinterSettingsViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Amout of copies
    self.copiesLabel.text = NSLocalizedString(@"Print", @"How many copies of the order to print");
    self.segmentedControl.selectedSegmentIndex = self.printer.printEachOrder;
    [self.stepper setValue:(double)self.printer.copies];
    self.amountCopy.text = [NSString stringWithFormat:@"%d", self.printer.copies];
    
    // Printer models
    for(int i = 0; i < [self.tableView numberOfRowsInSection:3]; i++) {
        UITableViewCell * cell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForItem:i inSection:3]];
        if( [cell.textLabel.text isEqualToString:self.printer.model] ){
            [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
            self.previousIndexPath = [NSIndexPath indexPathForItem:i inSection:3];
        }
    }
    
    [self setStatusCellText:NSLocalizedString(@"Loading", nil)];
}

- (void) viewDidAppear:(BOOL)animated
{
    // Set the timer and update the printer status
    // The timer value is used to update continuously the printer status cell's text 
    self.timer = YES;
    [self updatePrinterStatus];
}

- (void)viewWillDisappear:(BOOL)animated
{
    self.timer = NO;
    // Displays an alert if the printer model is not selected
    if([self.printer.model isEqualToString:@""])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Printer model", nil) message:NSLocalizedString(@"Please select the printer model.", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
        [alert show];
    }
}

- (void) setStatusCellText:(NSString*) text
{
    UITableViewCell *printerStatusCell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:0]];
    printerStatusCell.textLabel.text = text;
}

// Update the printer status cell's text
- (void) updatePrinterStatus{
    // Update status cell - runs in background
    dispatch_queue_t q_background = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0);
    double delayInSeconds = UPDATE_STATUS_INTERVAL;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, q_background, ^(void){
        if(self.timer == YES){
            if([self.printer.model isEqualToString:@""])
                [self performSelectorOnMainThread:@selector(setStatusCellText:) withObject:NSLocalizedString(@"No printer model selected", nil) waitUntilDone:NO];
            else {
                NSString *status = [self.printer getPrinterStatus];
                [self performSelectorOnMainThread:@selector(setStatusCellText:) withObject:status waitUntilDone:NO];
            }
            [self updatePrinterStatus];
        }
    });
}

// Changes the amount of copies
- (IBAction)changeCopiesAmount:(UIStepper*)sender {
    double v = [sender value];
    self.amountCopy.text = [NSString stringWithFormat:@"%d", (int)v];
    self.printer.copies = (int)v;
    [self.delegate updateSavedPrinter:self.printer];
}


// Print receipt for each order / item
- (IBAction)changeSegmentedControl:(UISegmentedControl*)sender {
    self.printer.printEachOrder = sender.selectedSegmentIndex;
    [self.delegate updateSavedPrinter:self.printer];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section == 1) {
        // Print mode
        [[tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:indexPath.section]] setAccessoryType:UITableViewCellAccessoryNone];
        [[tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:indexPath.section]] setAccessoryType:UITableViewCellAccessoryNone];
        self.printer.printMode = indexPath.row;
        NSLog(@"Print mode selected : %d", self.printer.printMode);
        [[tableView cellForRowAtIndexPath:indexPath] setAccessoryType:UITableViewCellAccessoryCheckmark];
        [self.delegate updateSavedPrinter:self.printer];
    }
    if (indexPath.section == 3) {
        if([self.printer.brand isEqualToString:@"epson"]){
            // Printer models
            [[tableView cellForRowAtIndexPath:self.previousIndexPath] setAccessoryType:UITableViewCellAccessoryNone];
            [[tableView cellForRowAtIndexPath:indexPath] setAccessoryType:UITableViewCellAccessoryCheckmark];
            self.printer.model = [[[tableView cellForRowAtIndexPath:indexPath] textLabel] text];
            NSLog(@"Printer model selected : %@", self.printer.model);
            self.previousIndexPath = indexPath;
            [self.delegate updateSavedPrinter:self.printer];
        }
    }
    if (indexPath.section == 4 && indexPath.row == 0) {
        // Test print
        self.timer = NO;
        [self.printer printTest];
        self.timer = YES;
    }
    if (indexPath.section == 4 && indexPath.row == 1) {
        // Remove
        [self.delegate printerSettingsDidRemovePrinter:self.printer];
    }
}

-(int) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(section == 0 || section == 2)
        return 1;
    else if(section == 3)
        return [self.printer.brand isEqualToString:@"epson"] ? [self.printer.supportedModels count] : 1;
    else return 2;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return @[NSLocalizedString(@"Printer status", nil),
             NSLocalizedString(@"Print mode", @"When to print orders"),
             NSLocalizedString(@"Settings", nil),
             NSLocalizedString(@"Printer model", nil),
             NSLocalizedString(@"Actions", @"Things you can do with the printer")][section];
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 1 && indexPath.row == 0) {
        cell.textLabel.text = NSLocalizedString(@"Print pending orders", nil);
        if(self.printer.printMode == 0)
            [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
    } else if (indexPath.section == 1 && indexPath.row == 1) {
        cell.textLabel.text = NSLocalizedString(@"Print preparing orders", nil);
        if(self.printer.printMode == 1)
            [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
    }
    else if (indexPath.section == 3){
        if([self.printer.brand isEqualToString:@"epson"]){
            cell.textLabel.text = [self.printer.supportedModels objectAtIndex:indexPath.row];
            if( [cell.textLabel.text isEqualToString:self.printer.model] ){
                [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
                self.previousIndexPath = indexPath;
            }
        }
        else { // Star printer : model is detected automatically, no need to choose
            cell.textLabel.text = self.printer.model;
            [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
        }
    }
    else if (indexPath.section == 4 && indexPath.row == 0) {
        cell.textLabel.text = NSLocalizedString(@"Test print", nil);
    } else if (indexPath.section == 4 && indexPath.row == 1) {
        cell.textLabel.text = NSLocalizedString(@"Remove printer", nil);
    }
}

@end
