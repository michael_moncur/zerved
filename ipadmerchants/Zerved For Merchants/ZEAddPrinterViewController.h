//
//  ZEAddPrinterViewController.h
//  Zerved For Merchants
//
//  Created by Anders Rasmussen on 03/07/13.
//  Copyright (c) 2013 Zerved. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZEPrinter.h"
#import <StarIO/SMPort.h>
#import <StarIO/Port.h>
@protocol ZEAddPrinterDelegate <NSObject>

- (void)didAddPrinter:(id)printer;

@end

@interface ZEAddPrinterViewController : UITableViewController

@property (nonatomic, strong) NSArray *printers;
@property (nonatomic, strong) NSTimer *timer;
@property (nonatomic, weak) id<ZEAddPrinterDelegate> delegate;

@end
