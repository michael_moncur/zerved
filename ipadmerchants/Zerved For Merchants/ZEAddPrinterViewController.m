//
//  ZEAddPrinterViewController.m
//  Zerved For Merchants
//
//  Created by Anders Rasmussen on 03/07/13.
//  Copyright (c) 2013 Zerved. All rights reserved.
//

#import "ZEAddPrinterViewController.h"
#import "ZEPrinter.h"
#import "ZEStarPrinter.h"
#import "ZEPrinterInfoCell.h"
#import "SVProgressHUD.h"
#import "ZEDetectedPrinter.h"

#define DISCOVERY_INTERVAL  0.5

@interface ZEAddPrinterViewController ()

@end

@implementation ZEAddPrinterViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];    
}

- (void)viewWillAppear:(BOOL)animated
{
    [self findStart];
}

- (void)viewWillDisappear:(BOOL)animated
{
    //stop old finder
    if(self.timer != nil){
        [self.timer invalidate];
        self.timer = nil;
    }
//    [EpsonIoFinder stop];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//find printer timer
- (void)timerFindPrinter:(NSTimer*)timer
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSArray *starPrintersArray = [SMPort searchPrinter];
    
        NSMutableArray *starPrinters = [[NSMutableArray alloc] init];
        for (int i = 0; i < starPrintersArray.count; i++) {
            PortInfo *port = [starPrintersArray objectAtIndex:i];
            ZEDetectedPrinter * p = [[ZEDetectedPrinter alloc] initWithPortName:port.portName MAC:port.macAddress andModel:port.modelName];
            [starPrinters addObject:p];         
        }
    
        if (starPrinters != nil){
            self.printers = starPrinters;
            [self performSelectorOnMainThread:@selector(reloadPrinterView) withObject:nil waitUntilDone:YES];
        }
    });
}

//find start/restart
- (void)findStart
{
    //stop find timer
    if(self.timer != nil){
        [self.timer invalidate];
        self.timer = nil;
    }
    
    //clear list
    self.printers = nil;
    self.printers = [[NSArray alloc] init];
    [self.tableView reloadData];
    
    //start timer
    self.timer = [NSTimer scheduledTimerWithTimeInterval:DISCOVERY_INTERVAL
                                              target:self
                                            selector:@selector(timerFindPrinter:)
                                            userInfo:nil
                                             repeats:YES];
}

- (void)reloadPrinterView
{
    [self.tableView reloadData];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return @[NSLocalizedString(@"Help", nil), NSLocalizedString(@"Detected printers", nil)][section];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(section == 0){
        return 1;
    }
    return ([self.printers count] > 0) ? [self.printers count] : 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0 && indexPath.row == 0) {
        return 260;
    }
    return 44;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
    if(indexPath.section == 0)
    {
        ZEPrinterInfoCell *c = [tableView dequeueReusableCellWithIdentifier:@"PrinterInfoCell" forIndexPath:indexPath];
        c.line1.text = NSLocalizedString(@"Please select the printer IP address below.", nil);
        c.line2.text = NSLocalizedString(@"Make sure it's connected to the same local network as your iPad.", nil);
        c.models.text = [NSLocalizedString(@"Currently supported models", nil) stringByAppendingString:@"\n\n"
                                           "Epson: TM-T88V, TM-T70, TM-T70II, TM-P60, TM-P60II, TM-U220, TM-T20, TM-T81II, TM-T82, TM-T82II\n\n"
                                           "Star Micronics: TSP650, TSP650II, FVP10, TSP700II, TSP800II, TUP500, TSP100U, TSP100GT, TSP100ECO, SM-T220i, SM-T300, SM-T300i, SM-T400i"];
        cell = c;
    }
    else {
        cell = [tableView dequeueReusableCellWithIdentifier:@"PrinterCell" forIndexPath:indexPath];
    
        if([self.printers count] > 0){
            [cell setUserInteractionEnabled:YES];
            [cell.textLabel setTextAlignment:NSTextAlignmentLeft];
            [cell.textLabel setEnabled:YES];
            cell.textLabel.text = [(ZEDetectedPrinter*)[self.printers objectAtIndex:indexPath.row] description];
        }
        else
        {
            [cell setUserInteractionEnabled:NO];
            [cell.textLabel setTextAlignment:NSTextAlignmentCenter];
            [cell.textLabel setEnabled:NO];
            cell.textLabel.text = NSLocalizedString(@"Searching for printers...", nil);
        }
    }
    return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if(indexPath.section == 1)
    {
        ZEPrinter* printer;
        ZEDetectedPrinter * detectedPrinter = (ZEDetectedPrinter *)[self.printers objectAtIndex:indexPath.row];
        
//        if([detectedPrinter.brand isEqualToString:@"epson"])
//            printer = [[ZEEpsonPrinter alloc] initWithName:detectedPrinter.port andModel:detectedPrinter.model];
//        else
		if([detectedPrinter.brand isEqualToString:@"star"])
            printer = [[ZEStarPrinter alloc] initWithName:detectedPrinter.port andModel:detectedPrinter.model];
            
        [self.delegate didAddPrinter:printer];

    }
}

@end
