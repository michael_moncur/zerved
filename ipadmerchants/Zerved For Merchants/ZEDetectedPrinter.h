//
//  ZEDetectedPrinter.h
//  Zerved For Merchants
//
//  Created by Zerved Development Mac on 09/08/13.
//  Copyright (c) 2013 Zerved. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ZEDetectedPrinter : NSObject

@property(nonatomic, retain) NSString *port;
@property(nonatomic, retain) NSString *mac;
@property(nonatomic, retain) NSString *model;
@property(nonatomic, retain) NSString *brand;

- (id) initWithPortName:(NSString*)port MAC:(NSString *)mac andModel:(NSString *) model;
- (NSString *) description;


@end
