//
//  ZEStarPrinter.m
//  Zerved For Merchants
//
//  Created by Zerved Development Mac on 02/08/13.
//  Copyright (c) 2013 Zerved. All rights reserved.
//

#import "ZEStarPrinter.h"
#import "RasterDocument.h"
#import "StarBitmap.h"
#import <sys/time.h>
#include <unistd.h>

@implementation ZEStarPrinter


- (ZEStarPrinter *) initWithName:(NSString*)name andModel:(NSString*)model
{
    if(self = [super initWithName:name]){
        self.model = model;
        self.brand = @"star";
        //self.port = nil;

        self.supportedModels = [[NSArray alloc] initWithObjects:@"TSP100LAN",
                                @"TSP650",
                                @"TSP650II",
                                @"FVP10",
                                @"TSP700II",
                                @"TSP800II",
                                @"TUP500",
                                @"TSP100U",
                                @"TSP100GT",
                                @"TSP100ECO",
                                @"SM-T220i",
                                @"SM-T300",
                                @"SM-T300i",
                                @"SM-T400i",
                                nil];
        return self;
    }
    return nil;
}

// ---------------------------------------------------------------------
// Printer methods
// ---------------------------------------------------------------------

- (BOOL)openPrinterWithIP:(NSString *)deviceName{return YES;}
- (BOOL)closePrinter{return YES;}

// Returns the printer status
- (NSString *) getPrinterStatus
{
    SMPort *starPort = nil;
    @try
    {
        starPort = [SMPort getPort:self.name :@"" :10000];
        if (starPort == nil){
            return NSLocalizedString(@"Failed to open printer connection", nil);
        }
        usleep(1000 * 1000);
        
        StarPrinterStatus_2 status;
        [starPort getParsedStatus:&status :2];
        
        NSString *message = @"";
        if (status.offline == SM_TRUE)
        {
            message = NSLocalizedString(@"Printer is offline", nil);
            if (status.coverOpen == SM_TRUE)
            {
                message = [NSString stringWithFormat:@"%@ (%@)", message, NSLocalizedString(@"The cover is open", nil)];
            }
            else if (status.receiptPaperEmpty == SM_TRUE)
            {
                message = [NSString stringWithFormat:@"%@ (%@)", message, NSLocalizedString(@"Out of paper", nil)];
            }
        }
        else message = NSLocalizedString(@"Printer is ready", nil);
        return message;
    }
    @catch (PortException *exception)
    {
      return NSLocalizedString(@"Printer error", nil);
    }
    @finally
    {
        [SMPort releasePort:starPort];
    }

}

// Checks that the printer settings are correct and that the connection to the printer is open
- (BOOL) isReadyToPrint
{
    // Not an Epson printer
    if(![self.brand isEqualToString:@"star"])
        return NO;
    // Model has not been selected
    if ([self.model isEqualToString:@""]){
        [self performSelectorOnMainThread:@selector(showErrorMsg:) withObject:NSLocalizedString(@"Please select the printer model.", @"Label for printer model error") waitUntilDone:NO];
        return NO;
    }
    return YES;
}

- (BOOL) isPortablePrinter
{
    return [self.model hasPrefix:@"SM"];
}

// ---------------------------------------------------------------------
// Printing methods
// ---------------------------------------------------------------------

- (BOOL)printTest
{
    NSLog(@"Star printer : printTest launched");
    
    [SVProgressHUD showWithStatus:NSLocalizedString(@"Printing", nil)];
    
    // Prepares the date
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateStyle:NSDateFormatterMediumStyle];
    [dateFormatter setTimeStyle:NSDateFormatterMediumStyle];
    
    // Creates the template
    NSString *textToPrint = [NSString stringWithFormat:@""
                             "--------------------------------------\r\n\r\n"
                             "%@\r\n"
                             "%@\r\n"
                             "%@\r\n"
                             "\r\n"
                             "--------------------------------------\r\n",
                             [self alignCenter:NSLocalizedString(@"Zerved Test Print", nil) fontSize:24],
                             [self alignCenter:[dateFormatter stringFromDate:[NSDate date]] fontSize:24],
                             [self alignCenter:[NSString stringWithFormat:NSLocalizedString(@"Printer: %@", @"Printer's name in printTest"), self.name] fontSize:24]
                             ];

    // Prints
    if([self printStringTemplate:textToPrint withTitle:@""]){
        [SVProgressHUD showErrorWithStatus:[NSString stringWithFormat:NSLocalizedString(@"Printed on %@", @"Printed on printer IP/name"), self.name]];
        return YES;
    }
    
    [SVProgressHUD showErrorWithStatus:NSLocalizedString(@"Failed to print.", nil)];
    return NO;
}

- (BOOL) printOrder:(NSDictionary*)order toIP:(NSString *)deviceName
{
    
    // Prepares the template data
    
    NSString *title = [NSString stringWithFormat:@"        #%@\r\n", [order objectForKey:@"queue_number"]];
   
    NSString * service;
    if([[order objectForKey:@"service"] isEqualToString:@"table"]){
        // Table service
       service = [NSString stringWithFormat:NSLocalizedString(@"*** SERVE AT TABLE NO. %@ ***", @"Label for table service with table number"), [order objectForKey:@"table_number"]];
    } else if([[order objectForKey:@"service"] isEqualToString:@"counter"]) {
        // Counter service
        //service = NSLocalizedString(@"*** FOR PICKUP AT COUNTER ***", @"Label for counter service");
        if ([[order objectForKey:@"to_go"] boolValue]) {
            service = NSLocalizedString(@"*** COLLECT - TO GO ***", @"Label for counter service to go");
        } else {
            service = NSLocalizedString(@"*** COLLECT - TO STAY ***", @"Label for counter service to stay");
        }
    } else if ([[order objectForKey:@"service"] isEqualToString:@"delivery"]) {
        service = NSLocalizedString(@"*** DELIVER TO ADDRESS ***", @"Label for delivery service");
    }
    
    // Event
    NSString *event1 = @"";
    NSString *event2 = @"";
    if ([order objectForKey:@"event_name"] != [NSNull null] && [order objectForKey:@"event_time_label"] != [NSNull null] && [order objectForKey:@"delivery_date"] != [NSNull null] && ![[order objectForKey:@"event_name"] isEqualToString:@""]) {
        event1 = [event1 stringByAppendingString:[NSString stringWithFormat:@"*** %@ ***", [order objectForKey:@"event_name"]]];
        event2 = [event2 stringByAppendingString:[NSString stringWithFormat:@"*** %@ %@ ***", [order objectForKey:@"event_time_label"], [order objectForKey:@"delivery_date"]]];
    }
    
    NSString * items = @"";
    // Items
    for(NSDictionary *item in [order objectForKey:@"items"]){
        items = [items stringByAppendingString:[self receiptLineWithLeft:[NSString stringWithFormat: @"%@: %@ x %@", [item objectForKey:@"category_name"], [item objectForKey:@"quantity"], [item objectForKey:@"name"]] andRight:[NSString stringWithFormat: @"%@", [item objectForKey:@"total_incl_tax"]] forFontSize:24]];
        // Options
        for(NSDictionary *option in [item objectForKey:@"options"]){
            NSString *values = @"";
            for(NSString *value in [option objectForKey:@"values"]){
                values = [NSString stringWithFormat:@"%@ %@", values, value];
            }
            items = [items stringByAppendingString:[NSString stringWithFormat: @"\n        %@ : %@", [option objectForKey:@"option_name"], values]];
        }
        items = [items stringByAppendingString:@"\r\n--------------------------------------\r\n"];
    }
    
    // Creates the template
    NSString *textToPrint = [NSString stringWithFormat:@"\r\n\r\n\r\n"
                             "%@\r\n\r\n",
                             [self alignCenter:service fontSize:24]];
    // Loyalty number
    if ([order objectForKey:@"loyalty_code"] != [NSNull null]) {
        NSString *loyaltyString = [NSString stringWithFormat:NSLocalizedString(@"*** LOYALTY CODE: %@ ***", nil), [order objectForKey:@"loyalty_code"]];
        textToPrint = [textToPrint stringByAppendingString:[NSString stringWithFormat:@"%@\r\n\r\n", [self alignCenter:loyaltyString fontSize:24]]];
    }
    // Event
    if (![event1 isEqualToString:@""] && ![event2 isEqualToString:@""]) {
        textToPrint = [textToPrint stringByAppendingString:[NSString stringWithFormat:@"%@\r\n%@\r\n\r\n", [self alignCenter:event1 fontSize:24], [self alignCenter:event2 fontSize:24]]];
    }
    // Address
    if ([order objectForKey:@"delivery_address_formatted"] != [NSNull null] && [order objectForKey:@"delivery_name"] != [NSNull null] && ![[order objectForKey:@"delivery_name"] isEqualToString:@""]) {
        textToPrint = [textToPrint stringByAppendingString:[NSString stringWithFormat:@"%@\r\n\r\n", [order objectForKey:@"delivery_address_formatted"]]];
    }
    if ([order objectForKey:@"delivery_instructions"] != [NSNull null] && ![[order objectForKey:@"delivery_instructions"] isEqualToString:@""]) {
        textToPrint = [textToPrint stringByAppendingString:[NSString stringWithFormat:@"%@\r\n%@\r\n\r\n", NSLocalizedString(@"Delivery instructions:", nil), [order objectForKey:@"delivery_instructions"]]];

    }
    
    // Order header
    textToPrint = [textToPrint stringByAppendingString:[NSString stringWithFormat:@"%@\r\n"
                             "%@\r\n\r\n"
                             "%@\r\n"
                             "%@\r\n",
                             [self receiptLineWithLeft:@"Zerved" andRight:[order objectForKey:@"bar"] forFontSize:24],
                             [self receiptLineWithLeft:[NSString stringWithFormat:NSLocalizedString(@"Order no. %@", nil), [order objectForKey:@"order_number"]] andRight:[order objectForKey:@"time_created"] forFontSize:24],
                             items,
                             [self receiptLineWithLeft:@"" andRight:[NSString stringWithFormat:NSLocalizedString(@"Total: %@", nil), [order objectForKey:@"total_incl_tax"]] forFontSize:24]
                            ]];
    
    return [self printStringTemplate:textToPrint withTitle:title];
}

- (BOOL) printCancelledOrder:(NSDictionary*)order
{
    NSString *textToPrint = @"\r\n\r\n\r\n\r\n\r\n"; // empty space allocated
    return [self printStringTemplate:textToPrint withTitle:[NSString stringWithFormat:@"%@\r\n%@\r\n",
                                                            [self alignCenter:[NSString stringWithFormat:NSLocalizedString(@"Order #%@", @"Order no."), [order objectForKey:@"queue_number"]]  fontSize:48],
                                                            [self alignCenter:NSLocalizedString(@"cancelled", nil) fontSize:48]
                                                            ]];
}

-(BOOL) printStringTemplate:(NSString*) template withTitle:(NSString *) title
{
    // 38 chars per line
    int width = 576; // max width
    
    // Font
    NSString *titleFontName = @"Courier";
    int titleFontSize = 48;
    
    NSString *contentFontName = @"Courier";
    int contentFontSize = 24;
    
    UIFont *titleFont = [UIFont fontWithName:titleFontName size:titleFontSize];
    UIFont *contentFont = [UIFont fontWithName:contentFontName size:contentFontSize];
    
    // Creates an image from the text and prints i
    
    // Max size of the receipt
    CGSize size = CGSizeMake(width, 10000);
    // Content size
    CGSize contentMeasuredSize = [template sizeWithFont:contentFont constrainedToSize:size lineBreakMode:NSLineBreakByWordWrapping];
	// Title size
    CGSize titleMeasuredSize = [title sizeWithFont:titleFont constrainedToSize:size lineBreakMode:NSLineBreakByWordWrapping];
    // Total size
    CGSize measuredSize = CGSizeMake( MAX(contentMeasuredSize.width, titleMeasuredSize.width), titleMeasuredSize.height + contentMeasuredSize.height);
    
	if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)]) {
		if ([[UIScreen mainScreen] scale] == 2.0) {
			UIGraphicsBeginImageContextWithOptions(measuredSize, NO, 1.0);
		} else {
			UIGraphicsBeginImageContext(measuredSize);
		}
	} else {
		UIGraphicsBeginImageContext(measuredSize);
	}
    
    CGContextRef ctr = UIGraphicsGetCurrentContext();
    UIColor *color = [UIColor whiteColor];
    [color set];
    
    CGRect rect = CGRectMake(0, 0, measuredSize.width, measuredSize.height);
    CGContextFillRect(ctr, rect);
    
    color = [UIColor blackColor];
    [color set];

    [title drawInRect:rect withFont:titleFont lineBreakMode:NSLineBreakByWordWrapping];
    [template drawInRect:rect withFont:contentFont lineBreakMode:NSLineBreakByWordWrapping];
    
    UIImage *imageToPrint = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    NSLog(@"Printer model detected: %@",self.model);
    NSString *portSettings = [self isPortablePrinter] ? @"mini" : @"";
    return [self printImageWithPortname:self.name portSettings:portSettings imageToPrint:imageToPrint maxWidth:width compressionEnable:YES withDrawerKick:NO];

}

- (BOOL)printImageWithPortname:(NSString *)portName portSettings:(NSString*)portSettings imageToPrint:(UIImage*)imageToPrint maxWidth:(int)maxWidth compressionEnable:(BOOL)compressionEnable withDrawerKick:(BOOL)drawerKick
{
    NSMutableData *commandsToPrint = [[NSMutableData alloc] init];
    if ([portSettings isEqualToString:@"mini"]) {
        // Portable image
        StarBitmap *starbitmap = [[StarBitmap alloc] initWithUIImage:imageToPrint :maxWidth :false];
        [commandsToPrint appendData:[starbitmap getImageMiniDataForPrinting:compressionEnable pageModeEnable:NO]];
    } else {
        // POS image
        RasterDocument *rasterDoc = [[RasterDocument alloc] initWithDefaults:RasSpeed_Medium endOfPageBehaviour:RasPageEndMode_FeedAndFullCut endOfDocumentBahaviour:RasPageEndMode_FeedAndFullCut topMargin:RasTopMargin_Standard pageLength:0 leftMargin:0 rightMargin:0];
        StarBitmap *starbitmap = [[StarBitmap alloc] initWithUIImage:imageToPrint :maxWidth :false];
        
        
        NSData *shortcommand = [rasterDoc BeginDocumentCommandData];
        [commandsToPrint appendData:shortcommand];
        
        shortcommand = [starbitmap getImageDataForPrinting:compressionEnable];
        [commandsToPrint appendData:shortcommand];
        
        shortcommand = [rasterDoc EndDocumentCommandData];
        [commandsToPrint appendData:shortcommand];
        
        if (drawerKick == YES) {
            [commandsToPrint appendBytes:"\x07"
                                  length:sizeof("\x07") - 1];    // KickCashDrawer
        }
    }
    
    return [self sendCommand:commandsToPrint portName:portName portSettings:portSettings timeoutMillis:10000];
   
}

- (BOOL)sendCommand:(NSData *)commandsToPrint portName:(NSString *)portName portSettings:(NSString *)portSettings timeoutMillis:(u_int32_t)timeoutMillis
{
    int commandSize = [commandsToPrint length];
    unsigned char *dataToSentToPrinter = (unsigned char *)malloc(commandSize);
    [commandsToPrint getBytes:dataToSentToPrinter];
    
    SMPort *starPort = nil;
    @try
    {
        starPort = [SMPort getPort:portName :portSettings :timeoutMillis];
        if (starPort == nil)
        {
            [self performSelectorOnMainThread:@selector(showErrorMsg:) withObject:NSLocalizedString(@"Failed to open printer connection", nil) waitUntilDone:NO];
            NSLog(@"Fail to Open Port");
            return NO;
        }
        
        StarPrinterStatus_2 status;
        [starPort beginCheckedBlock:&status :2];
        if (status.offline == SM_TRUE) {
            [self performSelectorOnMainThread:@selector(showErrorMsg:) withObject:NSLocalizedString(@"Printer is offline", nil) waitUntilDone:NO];
            NSLog(@"Printer is offline");
            return NO;
        }
        
        struct timeval endTime;
        gettimeofday(&endTime, NULL);
        endTime.tv_sec += 30;
        
        int totalAmountWritten = 0;
        while (totalAmountWritten < commandSize)
        {
            int remaining = commandSize - totalAmountWritten;
            int amountWritten = [starPort writePort:dataToSentToPrinter :totalAmountWritten :remaining];
            totalAmountWritten += amountWritten;
            
            struct timeval now;
            gettimeofday(&now, NULL);
            if (now.tv_sec > endTime.tv_sec)
            {
                break;
            }
        }
        
        if (totalAmountWritten < commandSize)
        {
            [self performSelectorOnMainThread:@selector(showErrorMsg:) withObject:NSLocalizedString(@"Printer error - timed out", nil) waitUntilDone:NO];
            NSLog(@"Printer Error - Write port timed out");
            return NO;
        }
        
        [starPort endCheckedBlock:&status :2];
        if (status.offline == SM_TRUE) {
            [self performSelectorOnMainThread:@selector(showErrorMsg:) withObject:NSLocalizedString(@"Printer is offline", nil) waitUntilDone:NO];
            NSLog(@"Printer is offline");
            return NO;
        }
    }
    @catch (PortException *exception)
    {
        [self performSelectorOnMainThread:@selector(showErrorMsg:) withObject:NSLocalizedString(@"Printer error - timed out", nil) waitUntilDone:NO];
        NSLog(@"Exception: Printer Error - Write port timed out");
        return NO;
    }
    @finally
    {
        free(dataToSentToPrinter);
        [SMPort releasePort:starPort];
        return YES;
    }	
}

-(NSString *) alignCenter:(NSString *) str fontSize:(int) fontSize {
    int n_spaces;
    if (str.length < [self getMaxCharacters:fontSize]) {
        n_spaces = ([self getMaxCharacters:fontSize] - str.length) / 2;
    } else {
        n_spaces = 0;
    }
    return [NSString stringWithFormat:@"%*s%@", n_spaces, "", str];
}

- (NSString*)receiptLineWithLeft:(NSString*)textLeft andRight:(NSString*)textRight forFontSize:(int)f
{
    int maxChars = [self getMaxCharacters:f];
    // No. of spaces on the last line = Line length - (No. of characters on the last line)
    // No. of spaces on the last line = Line length - (Remainder of total text length divided by line length)
    int n_spaces = maxChars - ((textLeft.length + textRight.length) % maxChars);
    // Return left text + n spaces + right text
    return [NSString stringWithFormat:@"%@%*s%@", textLeft, n_spaces, "", textRight];
}

-(int) getMaxCharacters:(int) fontSize
{
    // ratio = nunber of characters / font size (if font size == 24 -> 38 char. per line)
    switch (fontSize) {
        case 24:
            return 38;
            
        case 48:
            return 18;
            
        default:
            return 38;
    }
}

// ---------------------------------------------------------------------
// Encoding/decoding object from the user defaults
// ---------------------------------------------------------------------

- (void)encodeWithCoder:(NSCoder *)coder
{
    [coder encodeObject:self.name forKey:@"name"];
    [coder encodeObject:self.model forKey:@"model"];
    [coder encodeObject:self.brand forKey:@"brand"];
    [coder encodeInteger:self.copies forKey:@"copies"];
    [coder encodeInteger:self.printMode forKey:@"printMode"];
    [coder encodeInteger:self.printEachOrder forKey:@"printEachOrder"];
    [coder encodeObject:self.supportedModels forKey:@"supportedModels"];
}

- (id)initWithCoder:(NSCoder *)coder
{
    self = [[ZEStarPrinter alloc] init];
    if (self != nil)
    {
        self.name  = [coder decodeObjectForKey:@"name"];
        self.model = [coder decodeObjectForKey:@"model"];
        self.brand = [coder decodeObjectForKey:@"brand"];
        self.copies = [coder decodeIntegerForKey:@"copies"];
        self.printMode = [coder decodeIntegerForKey:@"printMode"];
        self.printEachOrder = [coder decodeIntegerForKey:@"printEachOrder"];
        self.supportedModels = [coder decodeObjectForKey:@"supportedModels"];
    }
    return self;
}

@end
