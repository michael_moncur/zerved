//
//  ZEPrinterSettingsViewController.h
//  Zerved For Merchants
//
//  Created by Anders Rasmussen on 04/07/13.
//  Copyright (c) 2013 Zerved. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZEPrinter.h"

@protocol ZEPrinterSettingsProtocol <NSObject>

- (void)printerSettingsDidRemovePrinter:(ZEPrinter *)printer;
- (BOOL)updateSavedPrinter:(ZEPrinter *)printer;

@end

@interface ZEPrinterSettingsViewController : UITableViewController

@property (nonatomic, retain) ZEPrinter *printer;
@property (strong, nonatomic) NSArray *printerModels;
@property (strong, nonatomic) NSIndexPath *previousIndexPath;
@property (nonatomic, weak) id<ZEPrinterSettingsProtocol> delegate;
@property (strong, nonatomic) IBOutlet UILabel *amountCopy;
@property (strong, nonatomic) IBOutlet UIStepper *stepper;
@property (weak, nonatomic) IBOutlet UILabel *copiesLabel;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentedControl;


@property BOOL timer;

- (id)initWithStyle:(UITableViewStyle)style;
- (void)viewDidLoad;
- (void)viewDidAppear:(BOOL)animated;
- (void)viewWillDisappear:(BOOL)animated;

- (void) setStatusCellText:(NSString*) text;
- (void) updatePrinterStatus;

- (IBAction)changeCopiesAmount:(UIStepper*)sender;
- (IBAction)changeSegmentedControl:(UISegmentedControl*)sender;

- (void)didReceiveMemoryWarning;

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath;
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section;
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath;

@end
