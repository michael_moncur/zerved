//
//  ZEServiceViewController.h
//  Zerved For Merchants
//
//  Created by Anders Rasmussen on 03/07/13.
//  Copyright (c) 2013 Zerved. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZEAddPrinterViewController.h"
#import "ZEPrinterSettingsViewController.h"
#import "ZEStarPrinter.h"
#import "Constants.h"
@interface ZEServiceViewController : UIViewController <UIWebViewDelegate, ZEAddPrinterDelegate, ZEPrinterSettingsProtocol, UIPopoverControllerDelegate>

@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (strong, nonatomic) UIBarButtonItem *addPrinterButton;
@property (weak, nonatomic) IBOutlet UIToolbar *toolbar;
@property (strong, nonatomic) UIPopoverController *popover;
@property (strong, nonatomic) NSMutableDictionary *printQueues;


// Printers related methods
- (NSMutableArray *)getPrinters;
- (void)setPrinters:(NSMutableArray *)printers;
- (BOOL)updateSavedPrinter:(ZEPrinter *)printer;
- (BOOL)inArray:(ZEPrinter *)printer;
- (id)getPrinterByName:(NSString *)name;
- (int)indexOfPrinter:(ZEPrinter *)printer;

- (void)addPrinter;
- (void)didAddPrinter:(id)printer;
- (void)selectPrinter:(UIBarButtonItem *)sender;
- (void)printerSettingsDidRemovePrinter:(ZEPrinter *)printer;

// Orders related methods
- (NSMutableDictionary *)getOrders;
- (void)setOrders:(NSMutableDictionary *)orders;
- (void)savePrintedOrder:(NSString *)orderId withStatus:(NSString *)status;
- (BOOL)hasOrderBeenPrinted:(NSString *)orderId withStatus:(NSString *)status;

// Printing related methods
- (void)printOrder:(NSDictionary*)order printMethod:(int)method;
- (void)automaticallyPrintOrder:(NSDictionary*)order;

//
- (void)updateToolbar;
- (void)showSuccessMsg:(NSString *)msg;
- (void)showErrorMsg:(NSString *)msg;
- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController;

//
- (void)handleCall:(NSString*)functionName callbackId:(int)callbackId args:(NSDictionary*)args;

@end
