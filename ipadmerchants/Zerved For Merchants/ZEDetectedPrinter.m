//
//  ZEDetectedPrinter.m
//  Zerved For Merchants
//
//  Created by Zerved Development Mac on 09/08/13.
//  Copyright (c) 2013 Zerved. All rights reserved.
//

#import "ZEDetectedPrinter.h"

@implementation ZEDetectedPrinter

- (id) initWithPortName:(NSString*)port MAC:(NSString *)mac andModel:(NSString *) model{

    if(self = [super init]){
        self.port = port;
        self.model = model;
        self.mac = mac;
        self.brand = ([self.port hasPrefix:@"TCP:"] || [self.port hasPrefix:@"BT:"] ) ? @"star" : @"epson";
        return self;
    }
    return nil;
}

- (NSString *) description {
    return [NSString stringWithFormat:@"%@ (%@)", self.port, [self.brand isEqualToString:@"star"] ? [NSString stringWithFormat:@"Star %@", self.model] : @"Epson"];
}

@end
