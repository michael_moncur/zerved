//
//  ZEServiceViewController.m
//  Zerved For Merchants
//
//  Created by Anders Rasmussen on 03/07/13.
//  Copyright (c) 2013 Zerved. All rights reserved.
//

#import "ZEServiceViewController.h"
#import "ZEPrinterSettingsViewController.h"
#import "SVProgressHUD.h"
#import <AudioToolbox/AudioToolbox.h>
#import "ZEPrintQueue.h"

@interface ZEServiceViewController ()

@end

@implementation ZEServiceViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.printQueues = [[NSMutableDictionary alloc] init];
	
    [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/merchants/serve/bars", SERVER_URL]]]];
    
    self.addPrinterButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Add printer", nil) style:UIBarButtonItemStyleBordered target:self action:@selector(addPrinter)];

    // Uncomment these lines to reset the saved printers and orders
    //[self setOrders:nil];
    //[self setPrinters:nil];
    
    [self updateToolbar];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

- (void)refresh {
    [self.webView reload];
}

#pragma-mark Orders

- (NSMutableDictionary *)getOrders
{
    // Get stored orders
    NSUserDefaults *currentDefaults = [NSUserDefaults standardUserDefaults];
    NSMutableDictionary *orders = [currentDefaults objectForKey:@"orders"];
    if(orders != nil){
        return [[NSMutableDictionary alloc] initWithDictionary:orders];
    }
    return [[NSMutableDictionary alloc] init];
}

- (void)setOrders:(NSMutableDictionary *)orders
{
    // Save orders
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:orders forKey:@"orders"];
}

- (void)savePrintedOrder:(NSString *)orderId withStatus:(NSString *)status
{
    // Add a printed order to the orders dictionary
    NSMutableDictionary *orders = [self getOrders];
    [orders setObject:@"printed" forKey:[NSString stringWithFormat:@"%@_%@", orderId, status]];
    [self setOrders:orders];
}

- (BOOL)hasOrderBeenPrinted:(NSString *)orderId withStatus:(NSString *)status
{
    // Returns if an order has been printed (=saved in the orders dictionary)
    NSMutableDictionary *orders = [self getOrders];
    for(NSString *ID in [orders allKeys]){
        if ([ID isEqualToString:[NSString stringWithFormat:@"%@_%@", orderId, status]]){
            return YES;
        }
    }
    return NO;
}

#pragma-mark Printers

- (NSMutableArray *)getPrinters
{
    // Get stored printers        
    NSUserDefaults *currentDefaults = [NSUserDefaults standardUserDefaults];
    NSData *savedData = [currentDefaults objectForKey:@"printers"];
    if (savedData != nil){
        NSArray *oldSavedArray = [NSKeyedUnarchiver unarchiveObjectWithData:savedData];
        if (oldSavedArray != nil)
            return [[NSMutableArray alloc] initWithArray:oldSavedArray];
    }
    return [[NSMutableArray alloc] init];
}

- (void)setPrinters:(NSMutableArray *)printers
{
    // Save printers
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:[NSKeyedArchiver archivedDataWithRootObject:printers] forKey:@"printers"];
}

- (BOOL)updateSavedPrinter:(ZEPrinter *)printer
{
    // Update a saved printer by removing it and adding it again in the printers array
    if([self inArray:printer]){
        NSMutableArray *printers = [self getPrinters];
        [printers removeObjectAtIndex:[self indexOfPrinter:printer]];
        [printers addObject:printer];
        [self setPrinters:printers];
        return YES;
    }
    return NO;
}

- (void)updateToolbar
{
    NSMutableArray *printers = [self getPrinters];
    NSMutableArray *buttons = [[NSMutableArray alloc] initWithCapacity:1];
    
    // Add back button if webview is showing another URL than ours
    if (![[self.webView.request.URL absoluteString] hasPrefix:SERVER_URL] && [self.webView canGoBack]) {
        UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Back", nil) style:UIBarButtonItemStyleBordered target:self.webView action:@selector(goBack)];
        [buttons addObject:backButton];
    }
    // Refresh button
    UIBarButtonItem *refreshButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemRefresh target:self action:@selector(refresh)];
    [buttons addObject:refreshButton];
    [buttons addObject:[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil]];
    
    // Add buttons for printers
    for (int i=0; i < [printers count]; i++){
        ZEPrinter *p = [printers objectAtIndex:i];
        UIBarButtonItem *button = [[UIBarButtonItem alloc] initWithTitle:p.name style:UIBarButtonItemStyleBordered target:self action:@selector(selectPrinter:)];
        [buttons addObject:button];
    }
    [buttons addObject:self.addPrinterButton];

    [self.toolbar setItems:buttons animated:YES];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"AddPrinter"]) {
        ZEAddPrinterViewController *controller = [segue destinationViewController];
        controller.delegate = self;
    }
}

- (void)addPrinter
{
    if (![self.popover isPopoverVisible]) {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Storyboard" bundle:nil];
        ZEAddPrinterViewController *addController = [storyboard instantiateViewControllerWithIdentifier:@"AddPrinter"];
        addController.delegate = self;
        self.popover = [[UIPopoverController alloc] initWithContentViewController:addController];
        self.popover.delegate = self;
        [self.popover presentPopoverFromBarButtonItem:self.addPrinterButton permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    } else {
        [self.popover dismissPopoverAnimated:YES];
    }
}

- (BOOL)inArray:(ZEPrinter *)printer
{
    // returns YES if a printer of the same *name* is already in the *printers* array, NO otherwise
    NSMutableArray *printers = [self getPrinters];
    for (int i=0; i < [printers count]; i++){
        ZEPrinter *p = (ZEPrinter*) [printers objectAtIndex:i];
        if([p.name isEqualToString:printer.name])
            return YES;
    }
    return NO;
}
- (ZEPrinter *)getPrinterByName:(NSString *)name
{
    // returns the index of a printer, -1 if the printer is not found
    NSMutableArray *printers = [self getPrinters];
    for (int i=0; i < [printers count]; i++){
        ZEPrinter *p = (ZEPrinter*) [printers objectAtIndex:i];
        if([p.name isEqualToString:name])
            return p;
    }
    return nil;
}
- (int)indexOfPrinter:(ZEPrinter *)printer
{
    // returns the index of a printer, -1 if the printer is not found
    NSMutableArray *printers = [self getPrinters];
    for (int i=0; i < [printers count]; i++){
        ZEPrinter *p = (ZEPrinter*) [printers objectAtIndex:i];
        if([p.name isEqualToString:printer.name])
            return i;
    }
    return -1;
}
- (void)didAddPrinter:(ZEPrinter *)printer
{
    NSMutableArray *printers = [self getPrinters];
    if ([self inArray:printer] == NO) {
        [printers addObject:printer];
        [self setPrinters:printers];
    }
    [self.popover dismissPopoverAnimated:NO];
    [self updateToolbar];

    for( UIBarButtonItem *item in [self.toolbar items] ){
        if([item.title isEqualToString:((ZEPrinter *)printer).name]){
            [item.target performSelector:@selector(selectPrinter:) withObject:item];
        }
    }
}

- (void)selectPrinter:(UIBarButtonItem *)sender
{
    if (![self.popover isPopoverVisible]) {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Storyboard" bundle:nil];
        ZEPrinterSettingsViewController *printerController = [storyboard instantiateViewControllerWithIdentifier:@"PrinterSettings"];
        printerController.delegate = self;
        printerController.printer = [self getPrinterByName:sender.title];
        self.popover = [[UIPopoverController alloc] initWithContentViewController:printerController];
        self.popover.delegate = self;
        [self.popover presentPopoverFromBarButtonItem:sender permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    } else {
        [self.popover dismissPopoverAnimated:YES];
    }
}

- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController
{
    
}

- (void)printerSettingsDidRemovePrinter:(ZEPrinter *)printer
{
    NSMutableArray *printers = [self getPrinters];
    [self.printQueues removeObjectForKey:printer.name];
    [printers removeObjectAtIndex:[self indexOfPrinter:printer]];
    [self setPrinters:printers];
    [self.popover dismissPopoverAnimated:YES];
    [self updateToolbar];
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    NSString *requestString = [[request URL] absoluteString];
    if ([requestString hasPrefix:@"js-frame:"]) {
        // App call from webpage
        
        NSArray *components = [requestString componentsSeparatedByString:@":"];
        
        NSString *function = (NSString*)[components objectAtIndex:1];
        int callbackId = [((NSString*)[components objectAtIndex:2]) intValue];
        NSString *argsAsString = [(NSString*)[components objectAtIndex:3]
                                  stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSData *argsAsData = [argsAsString dataUsingEncoding:NSUTF8StringEncoding];
        
        NSError *e = nil;
        NSDictionary *jsonArgs = [NSJSONSerialization JSONObjectWithData:argsAsData options:NSJSONReadingMutableContainers error:&e];
        
        [self handleCall:function callbackId:callbackId args:jsonArgs];
        
        return NO;
    }
    return YES;
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [self updateToolbar];
}

#pragma-mark Sound

-(void) playSound:(NSString *)path extension:(NSString *)ext {
    SystemSoundID _newMsgSound;
    NSURL *soundURL = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:path ofType:ext]];
    AudioServicesCreateSystemSoundID((__bridge CFURLRef)soundURL, &_newMsgSound);
    AudioServicesPlaySystemSound(_newMsgSound);

}

-(void)playNewOrderSound {
    [self playSound:@"zerved_new_msg" extension:@"wav"];
}

-(void)playSplashSound {
    [self playSound:@"zerved_splash_sound" extension:@"wav"];
}


#pragma-mark Printing

// Main methods to print orders
// method : PRINT_ORDER_AUTO : prints orders depending on their arrival and the printer settings
//        : PRINT_ORDER_MANUALLY : Prints any order (Print button pressed)
//        : PRINT_ORDER_CANCELLED : Prints a cancelled order receipt
- (void)printOrder:(NSDictionary*)order printMethod:(int)method
{
    if(method == PRINT_ORDER_AUTO){
        [self automaticallyPrintOrder:order];
        return;
    }
    NSString * orderId = [NSString stringWithFormat:@"%@", [[order objectForKey:@"order"] objectForKey:@"id"]];
    NSLog(@"Printing order (id=%@) - %@", orderId, method == PRINT_ORDER_CANCELLED ? @"(cancelled order)" : @"(manually)");
    
    NSMutableArray *printers = [self getPrinters];
    for (int i=0; i < [printers count]; i++){
        ZEPrinter *p = [printers objectAtIndex:i];
        // Prints
        int copies = p.copies;
        if (p.printEachOrder == 1) {
            // Print a copy for every item
            int totalItems = [[[order objectForKey:@"order"] objectForKey:@"total_quantity"] intValue];
            copies = copies * totalItems;
        }
        if(method == PRINT_ORDER_MANUALLY){
            for(int j = 0; j < copies; j++){
                [[self printQueueForPrinter:p] printOrder:[order objectForKey:@"order"]];
            }
            [self performSelectorOnMainThread:@selector(showSuccessMsg:) withObject:[NSString stringWithFormat:NSLocalizedString(@"Printed on %@", @"Printed on printer IP/name"), p.name] waitUntilDone:NO];
        }
        else if(method == PRINT_ORDER_CANCELLED){
            for(int j = 0; j < p.copies; j++){
                [[self printQueueForPrinter:p] printCancelledOrder:order];
            }
            [self performSelectorOnMainThread:@selector(showSuccessMsg:) withObject:[NSString stringWithFormat:NSLocalizedString(@"Printed on %@", @"Printed on printer IP/name"), p.name] waitUntilDone:NO];
        }
    }
}

// Prints and saves orders when they arrive depending on the printers settings
- (void)automaticallyPrintOrder:(NSDictionary*)order
{
    NSString * orderId = [NSString stringWithFormat:@"%@", [[order objectForKey:@"order"] objectForKey:@"id"]];
    NSString * orderStatus = [NSString stringWithFormat:@"%@", [[order objectForKey:@"order"] objectForKey:@"status"]];
    BOOL printedForPending = NO, printedForPreparing = NO;
    NSLog(@"orderDidArrive: order id = %@", orderId);
    NSMutableArray *printers = [self getPrinters];
    
    for (int i=0; i < [printers count]; i++){
        ZEPrinter *p = [printers objectAtIndex:i];
        if((p.printMode == 0 && [orderStatus isEqualToString:@"pending"] && [self hasOrderBeenPrinted:orderId withStatus:@"pending"] == NO) ||
           (p.printMode == 1 && [orderStatus isEqualToString:@"preparing"] && [self hasOrderBeenPrinted:orderId withStatus:@"preparing"] == NO))
        {
            // Prints
            int copies = p.copies;
            if (p.printEachOrder == 1) {
                // Print a copy for every item
                int totalItems = [[[order objectForKey:@"order"] objectForKey:@"total_quantity"] intValue];
                copies = copies * totalItems;
            }
            for(int j = 0; j < copies; j++){
                [[self printQueueForPrinter:p] printOrder:[order objectForKey:@"order"]];
            }
            [self performSelectorOnMainThread:@selector(showSuccessMsg:) withObject:[NSString stringWithFormat:NSLocalizedString(@"Printed on %@", @"Printed on printer IP/name"), p.name] waitUntilDone:NO];
            printedForPending = [orderStatus isEqualToString:@"pending"];
            printedForPreparing = [orderStatus isEqualToString:@"preparing"];
        }
    }
    
    // save printed orders ids and statuses locally so that they don't get printed every minute/refresh
    if(printedForPending)
        [self savePrintedOrder:orderId withStatus:@"pending"];
    
    if(printedForPreparing)
        [self savePrintedOrder:orderId withStatus:@"preparing"];

}

- (ZEPrintQueue *)printQueueForPrinter:(ZEPrinter *)printer
{
    ZEPrintQueue *printQueue = [self.printQueues objectForKey:printer.name];
    if (printQueue == nil) {
        printQueue = [[ZEPrintQueue alloc] initWithPrinter:printer];
        [self.printQueues setObject:printQueue forKey:printer.name];
    }
    return printQueue;
}

- (void) showSuccessMsg:(NSString *)msg {
    [SVProgressHUD showSuccessWithStatus:msg];
}
- (void) showErrorMsg:(NSString *)msg {
    [SVProgressHUD showErrorWithStatus:msg];
}

- (void)handleCall:(NSString*)functionName callbackId:(int)callbackId args:(NSDictionary*)args
{
    // A new order has arrived
    if ([functionName isEqualToString:@"orderDidArrive"]) {
        [self printOrder:args printMethod:PRINT_ORDER_AUTO];
    }
    // Print button pressed
    else if([functionName isEqualToString:@"printOrder"]) {
        [self printOrder:args printMethod:PRINT_ORDER_MANUALLY];
    }
    // An order has been cancelled
    else if([functionName isEqualToString:@"printCancelledOrder"]) {
        [self printOrder:args printMethod:PRINT_ORDER_CANCELLED];
    }
    // Play new order sound
    else if ([functionName isEqualToString:@"playOrderSound"]) {
        [self playNewOrderSound];
    }
    // Play new order sound
    else if ([functionName isEqualToString:@"playSplashSound"]) {
        [self playSplashSound];
    }
    else NSLog(@"Unimplemented method '%@'",functionName);
}

@end
