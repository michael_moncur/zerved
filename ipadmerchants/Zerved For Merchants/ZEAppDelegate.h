//
//  ZEAppDelegate.h
//  Zerved For Merchants
//
//  Created by Anders Rasmussen on 25/06/13.
//  Copyright (c) 2013 Zerved. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZEAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (retain, nonatomic) NSMutableData *receivedData;

@end
