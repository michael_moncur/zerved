//
//  ZEAppDelegate.m
//  Zerved For Merchants
//
//  Created by Anders Rasmussen on 25/06/13.
//  Copyright (c) 2013 Zerved. All rights reserved.
//

#import "ZEAppDelegate.h"

@implementation ZEAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [self checkForUpdates];
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (void) checkForUpdates
{
    NSString *jsonUrl = @"https://itunes.apple.com/lookup?id=688813790";
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:[NSURL URLWithString:jsonUrl]];
    NSURLConnection *theConnection = [[NSURLConnection alloc] initWithRequest:requestObj delegate:self];
    if (theConnection) {
        self.receivedData = [NSMutableData data];
    } else {
        NSLog(@"Connection failed");
    }
}


- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    [self.receivedData setLength:0];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [self.receivedData appendData:data];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSError *e = nil;
    NSDictionary *data = [NSJSONSerialization JSONObjectWithData:self.receivedData options:NSJSONReadingMutableContainers error:&e];
    int results = [[data objectForKey:@"resultCount"] integerValue];
    
    if(results >0) // has results
    {
        NSArray *results = [data objectForKey:@"results"];
        NSDictionary *aResult = [results objectAtIndex:0];
        
        NSString    *appStoreVersion = [aResult valueForKey:@"version"];
        NSString    *localVersion    = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"];
        
        if([appStoreVersion compare:localVersion options:NSNumericSearch] == NSOrderedDescending)
        {
            //there is an update available.
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Please update", nil) message:NSLocalizedString(@"There is an update available for Zerved. Please go to the app store to update the app.", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel", nil) otherButtonTitles:NSLocalizedString(@"Update", nil), nil];
            [alertView show];
        }
    }
}

- (void)connection:(NSURLConnection *)connection
  didFailWithError:(NSError *)error
{
    NSLog(@"Connection failed! Error - %@ %@",
         [error localizedDescription],
         [[error userInfo] objectForKey:NSURLErrorFailingURLStringErrorKey]);
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
    if ([title isEqualToString:NSLocalizedString(@"Cancel", nil)]) {
        
    } else if ([title isEqualToString:NSLocalizedString(@"Update", nil)]) {
        // Go to app store
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"itms://itunes.com/apps/Zerved/ZervedForMerchants"]];
    }
}

@end
