//
//  ZEPrinterInfoCell.h
//  Zerved For Merchants
//
//  Created by Zerved Development Mac on 23/07/13.
//  Copyright (c) 2013 Zerved. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZEPrinterInfoCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *line1;
@property (weak, nonatomic) IBOutlet UILabel *line2;
@property (weak, nonatomic) IBOutlet UILabel *models;
@end
