//
//  main.m
//  Zerved For Merchants
//
//  Created by Anders Rasmussen on 25/06/13.
//  Copyright (c) 2013 Zerved. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ZEAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([ZEAppDelegate class]));
    }
}
