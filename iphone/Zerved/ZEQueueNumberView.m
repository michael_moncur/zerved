//
//  ZEQueueNumberView.m
//  Zerved
//
//  Created by Zerved Development Mac on 18/09/13.
//
//

#import "ZEQueueNumberView.h"

@implementation ZEQueueNumberView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.queueNr = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, self.frame.size.width - 20, 70)];
        self.queueNr.textColor = [UIColor blackColor];
        self.queueNr.textAlignment = NSTextAlignmentCenter;
        self.queueNr.backgroundColor = [UIColor colorWithWhite:0.0f alpha:0.0f];
        self.queueNr.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:64];

        [self.layer insertSublayer:self.orderNrLabel.layer atIndex:0];
        [self.layer insertSublayer:self.queueNr.layer atIndex:1];
    }
    return self;
}

- (void) setQueueNumber:(NSString *) q
{
    self.queueNr.text = [NSString stringWithFormat:@"#%@", q];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
