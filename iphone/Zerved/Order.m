//
//  Order.m
//  Zerved
//
//  Created by Anders Rasmussen on 05/07/13.
//
//

#import "Order.h"


@implementation Order


- (NSString *)state {
    return [self.status isEqualToString:@"complete"] ? @"closed" : @"open";
}

- (NSString *)serviceSummary {
    if ([self.service isEqualToString:@"counter"]) {
        return NSLocalizedString(@"Pickup at counter", @"Label on order with counter service");
    } else if ([self.service isEqualToString:@"table"]) {
        return NSLocalizedString(@"Serve at table", @"Label on order with table service");
    } else if ([self.service isEqualToString:@"delivery"]) {
        return NSLocalizedString(@"Deliver to address", @"Label on order with counter service");
    }
    return nil;
}

@end
