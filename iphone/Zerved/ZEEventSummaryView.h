//
//  ZEEventSummaryView.h
//  Zerved
//
//  Created by Anders Rasmussen on 13/11/13.
//
//

#import <UIKit/UIKit.h>
#import "ZEEvent.h"

@interface ZEEventSummaryView : UIView

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *subtitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UIView *backgroundView;
@property (weak, nonatomic) IBOutlet UIImageView *arrowImage;
@property (weak, nonatomic) IBOutlet UILabel *orderingLabel;
@property (weak, nonatomic) IBOutlet UIImageView *clockIcon;

- (void)setEvent:(ZEEvent *)event;
- (void)setWhiteBackground;

@end
