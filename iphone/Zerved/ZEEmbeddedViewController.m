//
//  ZEEmbeddedViewController.m
//  Zerved
//
//  Created by Henrik Karlsen on 10/10/16.
//
//

//
//  ZEInfoViewController.m
//  Zerved
//
//  Created by Anders Rasmussen on 08/06/12.
//  Copyright (c) 2012 Zerved. All rights reserved.
//
//  Simple information page displayed as a web page loaded from zervedapp.com.
//

#import "ZEEmbeddedViewController.h"
#import "MobilePayPayment.h"
#import "MobilePayManager.h"

@interface ZEEmbeddedViewController ()

@end

@implementation ZEEmbeddedViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    //[self addGreyBackground];
    if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1) {
        self.navigationController.navigationBar.tintColor = [UIColor blackColor];
    }
    [self reload];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    self.webView = nil;
}

- (void)reload
{

    NSString *fullURL = @"";
#ifdef EMBEDDED_MERCHANT_URL
    fullURL = [NSString stringWithFormat:@"%@", EMBEDDED_MERCHANT_URL];
#endif
    
    NSURL *url = [NSURL URLWithString:fullURL];
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
    [self.webView loadRequest:requestObj];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void) paymentSuccess {
    [self.webView performSelectorOnMainThread:@selector(stringByEvaluatingJavaScriptFromString:) withObject:@"paymentSuccess()" waitUntilDone:NO];
}

- (void) cancelOrder {
    [self.webView performSelectorOnMainThread:@selector(stringByEvaluatingJavaScriptFromString:) withObject:@"cancelOrder()" waitUntilDone:NO];
}

- (void) handleInterruptedMobilePayPayment {
    [self.webView performSelectorOnMainThread:@selector(stringByEvaluatingJavaScriptFromString:) withObject:@"handleInterruptedMobilePayPayment()" waitUntilDone:NO];
}

//[self showErrorPageInWebView:self.webView withMessage:NSLocalizedString(@"Logged out", nil)];
//ZEAppDelegate *delegate = [[UIApplication sharedApplication] delegate];
//delegate.cart_mobile_pay_order = [self getCart];
//delegate.order_id_mobile_pay_order = result.orderId;

- (void)handleCall:(NSString*)functionName callbackId:(int)callbackId args:(NSDictionary*)args
{
    if ([functionName isEqualToString:@"openMobilePay"]) {
 
        
        if (![[MobilePayManager sharedInstance]isMobilePayInstalled:MobilePayCountry_Denmark]) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Missing MobilePay app", nil)
                                                            message:@"Please install the MobilePay app."
                                                           delegate:self
                                                  cancelButtonTitle:NSLocalizedString(@"Cancel", nil)
                                                  otherButtonTitles:NSLocalizedString(@"Install MobilePay", nil), nil];
            alert.tag = 1;
            [alert show];
            return;
        }
        
        [[MobilePayManager sharedInstance] setupWithMerchantId:MOBILEPAY_MERCHANT_NUMBER merchantUrlScheme:@"zervedapp" timeoutSeconds:120 returnSeconds:1 captureType:MobilePayCaptureType_Reserve country:MobilePayCountry_Denmark];
        
        NSString* callbackurl = @"";
        #ifdef EMBEDDED_MOBILEPAY_CALLBACK_URL
            callbackurl = [NSString stringWithFormat:@"%@", EMBEDDED_MOBILEPAY_CALLBACK_URL];
        #endif
        
        [[MobilePayManager sharedInstance] setServerCallbackUrl:callbackurl];
        
        NSString* orderId = args[@"orderId"];
        float amount = [args[@"amount"] floatValue] / 100;
        MobilePayPayment *payment = [[MobilePayPayment alloc]initWithOrderId:orderId productPrice:amount];
        
        
        
        [[MobilePayManager sharedInstance]beginMobilePaymentWithPayment:payment error:^(NSError * _Nonnull error) {
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:error.localizedDescription
                                                            message:[NSString stringWithFormat:@"reason: %@, suggestion: %@",error.localizedFailureReason, error.localizedRecoverySuggestion]
                                                           delegate:self
                                                  cancelButtonTitle:NSLocalizedString(@"Cancel", nil)
                                                  otherButtonTitles:NSLocalizedString(@"Install MobilePay", nil),nil];
            alert.tag = 1;
            [alert show];
        }];
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
}

- (void)modalWebpageViewControllerDone:(ZEModalWebpageViewController *)termsController
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
