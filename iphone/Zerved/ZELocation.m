//
//  ZELocation.m
//  Zerved
//
//  Created by Anders Rasmussen on 15/04/13.
//
//

#import "ZELocation.h"

@implementation ZELocation

- (NSString *)description
{
    return self.name;
}

- (NSString *)describeDistanceTo:(CLLocation *)location
{
    CLLocationDistance meters = [location distanceFromLocation:[[CLLocation alloc] initWithLatitude:[self.latitude doubleValue] longitude:[self.longitude doubleValue]]];
    if ([[[NSLocale preferredLanguages] objectAtIndex:0] isEqualToString:@"en"]) {
        // Miles
        double yards = meters/0.9144;
        double miles = meters/1609.344;
        // <30 yards is here
        if (yards < 30) {
            return NSLocalizedString(@"Here", @"When location is close");
        }
        // <1000 yards round to closest 50 yards
        else if (yards < 1000) {
            double rounddist = 50 * floor((yards/50)+0.5);
            return [NSString stringWithFormat:NSLocalizedString(@"%0.f yards", nil), rounddist];
        }
        // <1750 yards round to closest 100 yards
        else if (yards < 1750) {
            double rounddist = 100 * floor((yards/100)+0.5);
            return [NSString stringWithFormat:NSLocalizedString(@"%0.f yards", nil), rounddist];
        }
        // >1750 yards round to closest mile
        else {
            return [NSString stringWithFormat:NSLocalizedString(@"%0.f miles", nil), round(miles)];
        }
    } else {
        // Meters
        // <30 meters is here
        if (meters < 30) {
            return NSLocalizedString(@"Here", @"When location is close");
        }
        // <1km round to closest 50m
        else if (meters < 1000) {
            double rounddist = 50 * floor((meters/50)+0.5);
            return [NSString stringWithFormat:NSLocalizedString(@"%0.f m", nil), rounddist];
        // <2km round to closest 100m
        } else if (meters < 2000) {
            double rounddist = 100 * floor((meters/100)+0.5);
            return [NSString stringWithFormat:NSLocalizedString(@"%0.f m", nil), rounddist];
        // >2km round to closest km
        } else {
            return [NSString stringWithFormat:NSLocalizedString(@"%0.f km", nil), round(meters/1000)];
        }
    }
}

- (ZEBar *)getBarWithId:(NSString *)barId
{
    for (ZEBar *bar in self.bars) {
        if ([bar.barId isEqualToString:barId]) {
            return bar;
        }
    }
    return nil;
}

- (BOOL)hasOpenBars
{
    for (ZEBar *bar in self.bars) {
        if ([bar.open boolValue]) {
            return YES;
        }
    }
    return NO;
}

- (NSString *)onelineAddress
{
    return [self.address stringByReplacingOccurrencesOfString:@"\r\n" withString:@", "];
}

@end
