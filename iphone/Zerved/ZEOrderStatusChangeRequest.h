//
//  ZEOrderStatusChangeRequest.h
//  Zerved
//
//  Created by Anders Rasmussen on 15/10/13.
//
//

#import <Foundation/Foundation.h>

@interface ZEOrderStatusChangeRequest : NSObject

@property (nonatomic, strong) NSString *status;
@property (nonatomic, strong) NSString *entrancePincode;

@end
