//
//  ZEConsumer.m
//  Zerved
//
//  Created by Anders Rasmussen on 07/05/12.
//  Copyright (c) 2012 Zerved. All rights reserved.
//

#import "ZEConsumerManager.h"
#import "KeychainItemWrapper.h"
#import "SSKeychain.h"
#import "ZELoginViewController.h"
#import "SVProgressHUD.h"
#import "NSData+Additions.h"
#import <RestKit/RestKit.h>

@implementation ZEConsumerManager

@synthesize receivedData = _receivedData;
@synthesize keychain = _keychain;
@synthesize delegate = _delegate;
@synthesize accessToken = _accessToken;
@synthesize lastResponse = _lastResponse;
@synthesize createAccountConnection = _createAccountConnection;
@synthesize loginConnection = _loginConnection;
@synthesize emailLoginConnection = _emailLoginConnection;
@synthesize resetConnection = _resetConnection;
@synthesize createAccountViewConnection = _createAccountViewConnection;
@synthesize termsLastAccepted = _termsLastAccepted;
@synthesize presentingController = _presentingController;
@synthesize enteredPassword = _enteredPassword;

- (id)init
{
    if (self = [super init]) {
        // Logout on init
        self.accessToken = @"";
        
        return self;
    }
    return nil;
}

- (void)createNewAccountInBackground
{
    NSDictionary *params = [[NSDictionary alloc] initWithObjectsAndKeys:@"iphone", @"device", nil];
    NSString *fullURL = [NSString stringWithFormat:@"%@%@", API_URL, @"consumers/create"];
    NSURL *url = [NSURL URLWithString:fullURL];
    NSMutableURLRequest *requestObj = [NSMutableURLRequest requestWithURL:url];
    [requestObj setHTTPMethod:@"POST"];
    [requestObj setValue:@"application/json" forHTTPHeaderField:@"content-type"];
    NSError *e = nil;
    [requestObj setHTTPBody:[NSJSONSerialization dataWithJSONObject:params options:0 error:&e]];
    self.createAccountConnection=[[NSURLConnection alloc] initWithRequest:requestObj delegate:self];
    if (self.createAccountConnection) {
        self.receivedData = [NSMutableData data];
    } else {
        DLog(@"Connection failed");
    }
}

- (void)createNewAccountFromView
{
    NSDictionary *params = [[NSDictionary alloc] initWithObjectsAndKeys:@"iphone", @"device", nil];
    NSString *fullURL = [NSString stringWithFormat:@"%@%@", API_URL, @"consumers/create"];
    NSURL *url = [NSURL URLWithString:fullURL];
    NSMutableURLRequest *requestObj = [NSMutableURLRequest requestWithURL:url];
    [requestObj setHTTPMethod:@"POST"];
    [requestObj setValue:@"application/json" forHTTPHeaderField:@"content-type"];
    NSError *e = nil;
    [requestObj setHTTPBody:[NSJSONSerialization dataWithJSONObject:params options:0 error:&e]];
    self.createAccountViewConnection=[[NSURLConnection alloc] initWithRequest:requestObj delegate:self];
    if (self.createAccountViewConnection) {
        self.receivedData = [NSMutableData data];
    } else {
        DLog(@"Connection failed");
    }
}

- (void)login:(NSString *)email password:(NSString *)password
{
    //self.email = email;
    self.enteredPassword = [password copy];
    NSDictionary *params = [[NSDictionary alloc] initWithObjectsAndKeys:email, @"email", password, @"password", nil];
    NSString *fullURL = [NSString stringWithFormat:@"%@%@", API_URL, @"consumers/login"];
    NSURL *url = [NSURL URLWithString:fullURL];
    NSMutableURLRequest *requestObj = [NSMutableURLRequest requestWithURL:url];
    [requestObj setHTTPMethod:@"POST"];
    [requestObj setValue:@"application/json" forHTTPHeaderField:@"content-type"];
    NSError *e = nil;
    [requestObj setHTTPBody:[NSJSONSerialization dataWithJSONObject:params options:0 error:&e]];
    self.emailLoginConnection=[[NSURLConnection alloc] initWithRequest:requestObj delegate:self];
    if (self.emailLoginConnection) {
        self.receivedData = [NSMutableData data];
    } else {
        DLog(@"Connection failed");
    }
}

- (void)loginWithKey:(NSString *)consumerKey password:(NSString *)password
{
    NSDictionary *params = [[NSDictionary alloc] initWithObjectsAndKeys:consumerKey, @"key", password, @"password", nil];
    NSString *fullURL = [NSString stringWithFormat:@"%@%@", API_URL, @"consumers/login"];
    NSURL *url = [NSURL URLWithString:fullURL];
    NSMutableURLRequest *requestObj = [NSMutableURLRequest requestWithURL:url];
    [requestObj setHTTPMethod:@"POST"];
    [requestObj setValue:@"application/json" forHTTPHeaderField:@"content-type"];
    NSError *e = nil;
    [requestObj setHTTPBody:[NSJSONSerialization dataWithJSONObject:params options:0 error:&e]];
    self.loginConnection=[[NSURLConnection alloc] initWithRequest:requestObj delegate:self];
    if (self.loginConnection) {
        self.receivedData = [NSMutableData data];
    } else {
        DLog(@"Connection failed");
    }
}

- (void)authenticateFromView:(UIViewController *)controller
{
    DLog(@"authenticateFromView");
    // Check if consumer login is available in keychain
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *consumerKey = [defaults stringForKey:[NSString stringWithFormat:@"%@_consumer_key", SERVER_URL]];
    NSError *error = nil;
    if (consumerKey != nil) {
        DLog(@"Logging in with saved credentials");
        NSString *password = [SSKeychain passwordForService:SERVER_URL account:consumerKey error:&error];
        DLog(@"%@", [error localizedDescription]);
        [self loginWithKey:consumerKey password:password];
    } else {
        // Check for keychain login made prior to v1.8
        self.keychain = [[KeychainItemWrapper alloc] initWithIdentifier:SERVER_URL accessGroup:nil];
        // Set service for keychain uniqueness http://stackoverflow.com/questions/4309110/error-saving-in-the-keychain-with-iphone-sdk
        [self.keychain setObject:@"Zerved" forKey:(__bridge id)kSecAttrService];
        consumerKey = [self.keychain objectForKey:(__bridge NSString *)kSecAttrAccount];
        NSString *password = [self.keychain objectForKey:(__bridge NSString *)kSecValueData];
        if (![consumerKey isEqualToString:@""] && ![password isEqualToString:@""]) {
            DLog(@"Logging in with deprecated credentials");
            // Update saved login, remove old login and login with consumer key and password
            [SSKeychain setPassword:password forService:SERVER_URL account:consumerKey error:&error];
            DLog(@"%@", [error localizedDescription]);
            [self.keychain resetKeychainItem];
            [self loginWithKey:consumerKey password:password];
        } else {
            // Check for email login made in older version
            KeychainItemWrapper *oldKeychain = [[KeychainItemWrapper alloc] initWithIdentifier:SERVER_URL accessGroup:nil];
            NSString *email = [oldKeychain objectForKey:(__bridge NSString *)kSecAttrAccount];
            if (![email isEqualToString:@""]) {
                DLog(@"Has email, but not complete credentials");
                // Email saved from old app version => Show login dialog
                [self authenticateNewAccountFromView:controller email:email];
            } else {
                DLog(@"Create new account");
                // Create consumer
                [self createNewAccountInBackground];
            }
        }
    }
}

- (void)authenticateNewAccountFromView:(UIViewController *)controller email:(NSString *)email
{
    UIStoryboard *storyboard = controller.storyboard;
    UINavigationController *loginNavController = [storyboard instantiateViewControllerWithIdentifier:@"LoginNavController"];
    ZELoginViewController *loginController = [loginNavController.viewControllers objectAtIndex:0];
    loginController.delegate = self;
    loginController.presetEmail = email;
    self.presentingController = controller;
    [controller presentViewController:loginNavController animated:YES completion:nil];
}

- (void)resetPassword:(NSString *)email
{
    NSDictionary *params = [[NSDictionary alloc] initWithObjectsAndKeys:email, @"email", nil];
    NSString *fullURL = [NSString stringWithFormat:@"%@%@", API_URL, @"consumers/resetpassword"];
    NSURL *url = [NSURL URLWithString:fullURL];
    NSMutableURLRequest *requestObj = [NSMutableURLRequest requestWithURL:url];
    [requestObj setHTTPMethod:@"POST"];
    [requestObj setValue:@"application/json" forHTTPHeaderField:@"content-type"];
    NSError *e = nil;
    [requestObj setHTTPBody:[NSJSONSerialization dataWithJSONObject:params options:0 error:&e]];
    self.resetConnection=[[NSURLConnection alloc] initWithRequest:requestObj delegate:self];
    if (self.resetConnection) {
        self.receivedData = [NSMutableData data];
    } else {
        DLog(@"Connection failed");
    }

}

- (void)deleteAccount
{
    DLog(@"Deleting account");
    NSString *fullURL = [NSString stringWithFormat:@"%@consumers/%@", API_URL, self.consumerKey];
    NSURL *url = [NSURL URLWithString:fullURL];
    NSMutableURLRequest *requestObj = [NSMutableURLRequest requestWithURL:url];
    [requestObj setHTTPMethod:@"DELETE"];
    NSData *accessTokenData = [self.accessToken dataUsingEncoding:NSUTF8StringEncoding];
    NSString *authValue = [NSString stringWithFormat:@"Basic %@", [accessTokenData base64Encoding]];
    [requestObj setValue:authValue forHTTPHeaderField:@"Authorization"];
    NSURLConnection *theConnection=[[NSURLConnection alloc] initWithRequest:requestObj delegate:self];
    if (theConnection) {
        self.receivedData = [NSMutableData data];
    } else {
        DLog(@"Connection failed");
    }
    self.accessToken = @"";
    //self.password = @"";
    NSError *error = nil;
    [SSKeychain deletePasswordForService:SERVER_URL account:self.consumerKey error:&error];
    DLog(@"%@", [error localizedDescription]);
    [self setConsumerKey:nil];
    [self setConsumerId:nil];
    [[RKObjectManager sharedManager].HTTPClient setDefaultHeader:@"Authorization" value:nil];
}

- (NSMutableURLRequest *)authenticatedRequestWithUrl:(NSString *)urlString
{
    NSURL *url = [NSURL URLWithString:urlString];
    NSMutableURLRequest *requestObj = [NSMutableURLRequest requestWithURL:url];
    NSData *accessTokenData = [self.accessToken dataUsingEncoding:NSUTF8StringEncoding];
    NSString *authValue = [NSString stringWithFormat:@"Basic %@", [accessTokenData base64Encoding]];
    [requestObj setValue:authValue forHTTPHeaderField:@"Authorization"];
    return requestObj;
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    [self.receivedData setLength:0];
    self.lastResponse = (NSHTTPURLResponse *)response;
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [self.receivedData appendData:data];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    if (connection == self.createAccountConnection || connection == self.createAccountViewConnection) {
        if (self.lastResponse.statusCode == 200) {
            DLog(@"consumer created");
            NSError *e = nil;
            NSDictionary *consumerData = [NSJSONSerialization JSONObjectWithData:self.receivedData options:NSJSONReadingMutableContainers error:&e];
            self.accessToken = [consumerData objectForKey:@"access_token"];
            [self setConsumerKey:[consumerData objectForKey:@"key"]];
            [self setConsumerId:[consumerData objectForKey:@"id"]];
            //self.password = [consumerData objectForKey:@"password"];
            NSError *error = nil;
            [SSKeychain setPassword:[consumerData objectForKey:@"password"] forService:SERVER_URL account:[consumerData objectForKey:@"key"] error:&error];
            DLog(@"%@", [error localizedDescription]);
            RKObjectManager *objectManager = [RKObjectManager sharedManager];
            NSData *accessTokenData = [self.accessToken dataUsingEncoding:NSUTF8StringEncoding];
            NSString *authValue = [NSString stringWithFormat:@"Basic %@", [accessTokenData base64Encoding]];
            [objectManager.HTTPClient setDefaultHeader:@"Authorization" value:authValue];
            if (connection == self.createAccountViewConnection) {
                [self.presentingController dismissViewControllerAnimated:YES completion:nil];
            }
            [self.delegate didAuthenticate];
        } else {
            DLog(@"Failed to create consumer");
            [SVProgressHUD showErrorWithStatus:[[NSString alloc] initWithData:self.receivedData encoding:NSUTF8StringEncoding]];
        }
    } else if (connection == self.loginConnection || connection == self.emailLoginConnection) {
        if (self.lastResponse.statusCode == 200) {
            DLog(@"Logged in");
            NSError *e = nil;
            NSDictionary *consumerData = [NSJSONSerialization JSONObjectWithData:self.receivedData options:NSJSONReadingMutableContainers error:&e];
            self.accessToken = [consumerData objectForKey:@"access_token"];
            [self setConsumerKey:[consumerData objectForKey:@"key"]];
            [self setConsumerId:[consumerData objectForKey:@"id"]];
            RKObjectManager *objectManager = [RKObjectManager sharedManager];
            NSData *accessTokenData = [self.accessToken dataUsingEncoding:NSUTF8StringEncoding];
            NSString *authValue = [NSString stringWithFormat:@"Basic %@", [accessTokenData base64Encoding]];
            [objectManager.HTTPClient setDefaultHeader:@"Authorization" value:authValue];
            if (connection == self.emailLoginConnection) {
                //self.password = self.enteredPassword;
                NSError *error = nil;
                [SSKeychain setPassword:self.enteredPassword forService:SERVER_URL account:self.consumerKey error:&error];
                DLog(@"%@", [error localizedDescription]);
                [self.presentingController dismissViewControllerAnimated:YES completion:nil];
            }
            [self.delegate didAuthenticate];
        } else if (self.lastResponse.statusCode == 404) {
            // Account does not exist, delete the saved login
            DLog(@"Account not found, deleting saved credentials");
            NSError *error = nil;
            [SSKeychain deletePasswordForService:SERVER_URL account:self.consumerKey error:&error];
            DLog(@"%@", [error localizedDescription]);
            [self setConsumerKey:nil];
            [self setConsumerId:nil];
            [SVProgressHUD showErrorWithStatus:[[NSString alloc] initWithData:self.receivedData encoding:NSUTF8StringEncoding]];
        } else {
            DLog(@"Failed to login");
            [SVProgressHUD showErrorWithStatus:[[NSString alloc] initWithData:self.receivedData encoding:NSUTF8StringEncoding]];
        }
    } else if (connection == self.resetConnection) {
        if (self.lastResponse.statusCode == 200) {
            [SVProgressHUD dismiss];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Reset password", nil) message:NSLocalizedString(@"Please check your email for a link to reset your password.", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
            [alert setTag:2];
            [alert show];
        } else {
            [SVProgressHUD showErrorWithStatus:[[NSString alloc] initWithData:self.receivedData encoding:NSUTF8StringEncoding]];
        }
    } else if(connection == self.deleteFavouriteConnection) {
        if(self.lastResponse.statusCode == 200) {
            [SVProgressHUD dismiss];
            [SVProgressHUD showSuccessWithStatus:NSLocalizedString(@"Removed from favourites",nil)];
            [self.updView reload];
        }
        else [SVProgressHUD showErrorWithStatus:NSLocalizedString(@"Error",nil)];
    }
}

- (void)connection:(NSURLConnection *)connection
  didFailWithError:(NSError *)error
{
    // inform the user
    DLog(@"Connection failed! Error - %@ %@",
          [error localizedDescription],
          [[error userInfo] objectForKey:NSURLErrorFailingURLStringErrorKey]);
    [SVProgressHUD showErrorWithStatus:NSLocalizedString(@"Connection failed", nil)];
}

- (NSString *)consumerKey
{
    //return [self.keychain objectForKey:(__bridge NSString *)kSecAttrAccount];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    return [defaults stringForKey:[NSString stringWithFormat:@"%@_consumer_key", SERVER_URL]];
}

- (void)setConsumerKey:(NSString *)newConsumerKey
{
    //[self.keychain setObject:newConsumerKey forKey:(__bridge NSString *)kSecAttrAccount];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if (newConsumerKey == nil) {
        [defaults removeObjectForKey:[NSString stringWithFormat:@"%@_consumer_key", SERVER_URL]];
    } else {
        [defaults setObject:newConsumerKey forKey:[NSString stringWithFormat:@"%@_consumer_key", SERVER_URL]];
    }
}

- (NSString *)consumerId
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    return [defaults stringForKey:[NSString stringWithFormat:@"%@_consumer_id", SERVER_URL]];
}

- (void)setConsumerId:(NSString *)newConsumerId
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if (newConsumerId == nil) {
        [defaults removeObjectForKey:[NSString stringWithFormat:@"%@_consumer_id", SERVER_URL]];
    } else {
        [defaults setObject:newConsumerId forKey:[NSString stringWithFormat:@"%@_consumer_id", SERVER_URL]];
    }
}

- (void)setTermsAccepted:(BOOL)accepted
{
    if (accepted) {
        self.termsLastAccepted = [NSDate date]; // accepted now
    } else {
        self.termsLastAccepted = nil; // not accepted
    }
}

- (BOOL)termsAcceptedRecently
{
    if (!self.termsLastAccepted) {
        return NO;
    }
    // Is terms accepted within the last 24 hours
    return (-[self.termsLastAccepted timeIntervalSinceNow] < 24*60*60);
}

// Send a request to delete a favourite
-(BOOL) deleteConsumerFavourite:(ZEFavourite *)favourite updateView:(id)s{
    // Creates the request
    NSString *fullURL = [NSString stringWithFormat:@"%@favourites/%@", API_URL, favourite.key];
    NSLog(@"%@", fullURL);
    NSURL *url = [NSURL URLWithString:fullURL];
    NSMutableURLRequest *requestObj = [NSMutableURLRequest requestWithURL:url];
    [requestObj setHTTPMethod:@"DELETE"];
    [requestObj setValue:@"application/json" forHTTPHeaderField:@"content-type"];
    //NSError *e = nil;
    //[requestObj setHTTPBody:[NSJSONSerialization dataWithJSONObject:params options:0 error:&e]];
    NSData *accessTokenData = [self.accessToken dataUsingEncoding:NSUTF8StringEncoding];
    NSString *authValue = [NSString stringWithFormat:@"Basic %@", [accessTokenData base64Encoding]];
    [requestObj setValue:authValue forHTTPHeaderField:@"Authorization"];
    // Creates the connection and send the request
    self.updView = s;
    self.deleteFavouriteConnection = [[NSURLConnection alloc] initWithRequest:requestObj delegate:self];
    return (self.deleteFavouriteConnection) ? YES : NO;
}

#pragma mark -
#pragma mark ZELoginViewControllerDelegate Methods
- (void)loginViewControllerDidLoginWithEmail:(NSString *)email password:(NSString *)password
{
    [self login:email password:password];
}

- (void)loginViewControllerDidCancel
{
    DLog(@"Login view cancelled");
    [self.delegate didFailToAuthenticate];
    [self.presentingController dismissViewControllerAnimated:YES completion:nil];
}

- (void)loginViewControllerDidRequestResetPasswordForEmail:(NSString *)email
{
    [self resetPassword:email];
}

- (void)loginViewControllerDidRequestNewAccount
{
    [self createNewAccountFromView];
}

@end
