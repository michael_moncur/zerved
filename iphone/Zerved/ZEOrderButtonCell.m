//
//  ZEOrderButtonCell.m
//  Zerved
//
//  Created by Zerved Development Mac on 19/08/13.
//
//

#import "ZEOrderButtonCell.h"

@implementation ZEOrderButtonCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
