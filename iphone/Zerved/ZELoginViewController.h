//
//  ZELoginViewController.h
//  Zerved
//
//  Created by Anders Rasmussen on 16/10/12.
//
//

#import <UIKit/UIKit.h>
#import "ZELoginInfoViewController.h"

@interface ZELoginViewController : UIViewController <UITextFieldDelegate, ZELoginInfoDelegate, UIAlertViewDelegate>
@property (weak, nonatomic) IBOutlet UILabel *headerLabel;
@property (weak, nonatomic) IBOutlet UILabel *subtitleLabel;
@property (weak, nonatomic) IBOutlet UITextField *emailField;
@property (weak, nonatomic) IBOutlet UITextField *passwordField;
@property (weak, nonatomic) IBOutlet UITextField *repeatPasswordField;
@property (weak, nonatomic) IBOutlet UILabel *passwordHelpLabel;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *confirmButton;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentBar;
@property (weak, nonatomic) IBOutlet UIButton *infoButton;
@property (weak, nonatomic) IBOutlet UIButton *forgotPasswordButton;
@property (weak, nonatomic) id delegate;
@property (strong, nonatomic) NSString *customLoginHeader;
@property (strong, nonatomic) NSString *customSignupHeader;
@property (strong, nonatomic) NSString *customLoginSubtitle;
@property (strong, nonatomic) NSString *customSignupSubtitle;
@property (strong, nonatomic) NSString *presetEmail;
- (IBAction)segmentChanged:(UISegmentedControl *)sender;
- (IBAction)confirm:(id)sender;
- (IBAction)cancel:(id)sender;
- (IBAction)forgotPassword:(id)sender;

@end

@protocol ZELoginViewControllerDelegate <NSObject>

- (void)loginViewControllerDidLoginWithEmail:(NSString *)email password:(NSString *)password;
- (void)loginViewControllerDidCancel;
- (void)loginViewControllerDidRequestResetPasswordForEmail:(NSString *)email;
- (void)loginViewControllerDidRequestNewAccount;

@end