//
//  ZETextSerialization.m
//  Zerved
//
//  Created by Anders Rasmussen on 24/04/13.
//
//

#import "ZETextSerialization.h"

@implementation ZETextSerialization

+ (id)objectFromData:(NSData *)data error:(NSError *__autoreleasing *)error
{
    return [NSDictionary dictionaryWithObject:[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding] forKey:@"data"];
}

+ (NSData *)dataFromObject:(id)object error:(NSError *__autoreleasing *)error
{
    return nil;
}

@end
