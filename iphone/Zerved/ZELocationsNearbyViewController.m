//
//  ZELocationsViewController.m
//  Zerved
//
//  Created by Anders Rasmussen on 16/04/13.
//
//  List of nearby locations. This is the initial app screen.
//  Locations go to ZELocationViewController.
//

#import "ZELocationsNearbyViewController.h"
#import "ZELocation.h"
#import "ZELocationViewController.h"
#import "ZETabBarController.h"
#import "SVProgressHUD.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "QuartzCore/CAGradientLayer.h"
#import "ZEEmptyView.h"
#import "ZELocationCell.h"

@interface ZELocationsNearbyViewController ()

@end

@implementation ZELocationsNearbyViewController

@synthesize locationManager = _locationManager;
@dynamic tableView;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.title = NSLocalizedString(@"Locations", nil);
    
    // Initialize location manager
    if (nil == self.locationManager) {
        self.locationManager = [[CLLocationManager alloc] init];
    }
    self.locationManager.delegate = self;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters;
    self.locationManager.distanceFilter = 100;
    
    [self reload];
}

- (void)viewDidUnload {
    [self setTableView:nil];
    [self setLocationManager:nil];
    [super viewDidUnload];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [self.locationManager stopUpdatingLocation];
    self.updatingLocation = NO;
}

- (void)reload {
    // Nearby
    [SVProgressHUD showWithStatus:NSLocalizedString(@"Finding your location", nil)];
    // Start location updater (stop first to force a new location update)
    self.currentLocation = nil;
    self.updatingLocation = YES;
    [self.locationManager stopUpdatingLocation];
    
    // Check for iOS 8. Without this guard the code will crash with "unknown selector" on iOS 7.
    if ([self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
        [self.locationManager requestWhenInUseAuthorization];
    }
    
    [self.locationManager startUpdatingLocation];
    // Timeout after 10 sec
    [NSTimer scheduledTimerWithTimeInterval:10.0 target:self selector:@selector(locationTimeout:) userInfo:nil repeats:NO];
}

- (IBAction)reload:(id)sender {
    [self reload];
}

- (void)loadView
{
    [super loadView];
}

- (void)reloadTableView
{
    if(self.isViewLoaded) {
        [self.tableView reloadData];
        if ([self.locations count]) {
            [self.tableView setTableHeaderView:nil];
        } else {
            ZEEmptyView *emptyView = [[[NSBundle mainBundle] loadNibNamed:@"EmptyTableView" owner:self options:nil] objectAtIndex:0];
            [emptyView setupViewForTableView:self.tableView text:NSLocalizedString(@"No locations found", nil)];
            [self.tableView setTableHeaderView:emptyView];
        }
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"ViewLocation"]) {
        // Go to categories
        ZELocationViewController *destinationController = [segue destinationViewController];
        ZELocation *location = [self.locations objectAtIndex:[self.tableView indexPathForSelectedRow].row];
        destinationController.locationKey = location.key;
        destinationController.title = location.name;
    }
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.locations count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"LocationCell";
    ZELocationCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    // Configure the cell...
    if (cell == nil) {
        cell = [[ZELocationCell alloc] initWithStyle:UITableViewCellStyleDefault  reuseIdentifier:CellIdentifier];
    }
    // Add location text
    ZELocation* location = [self.locations objectAtIndex:indexPath.row];
    cell.titleLabel.text = [location name];
    if (self.currentLocation) {
        cell.subtitleLabel.text = [location describeDistanceTo:self.currentLocation];
    } else {
        cell.subtitleLabel.text = @"";
    }
    // Merchant logo
    // On old iPhones the images make the app crash with memory warning
    if (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1) {
        [cell.locationImage sd_setImageWithURL:[NSURL URLWithString:location.merchant.imageUrl] placeholderImage:[UIImage imageNamed:@"bar_logo_dummy.png"] options:SDWebImageRefreshCached];
    }
    return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Larger cells (default is approx. 45)
    return 70;
}

#pragma mark -
#pragma mark CLLocationManagerDelegate Methods

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    [SVProgressHUD showWithStatus:NSLocalizedString(@"Fetching nearby locations", nil)];
    self.currentLocation = newLocation;
    // Load nearby locations
    RKObjectManager *objectManager = [RKObjectManager sharedManager];
    [objectManager getObjectsAtPath:[NSString stringWithFormat:@"locations/nearby/%.6f/%.6f", newLocation.coordinate.latitude, newLocation.coordinate.longitude]
                         parameters:nil
                            success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
                                // Locations loaded
                                [SVProgressHUD dismiss];
                                self.locations = [mappingResult array];
                                [self reloadTableView];
                            }
                            failure:^(RKObjectRequestOperation *operation, NSError *error) {
                                // Failed to load locations
                                [SVProgressHUD dismiss];
                                [self.locationManager stopUpdatingLocation];
                                self.updatingLocation = NO;
                                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil)
                                                                                message:[error localizedDescription]
                                                                               delegate:nil
                                                                      cancelButtonTitle:NSLocalizedString(@"OK", nil)
                                                                      otherButtonTitles:nil];
                                [alert show];
                            }];
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    if ([error code] == kCLErrorDenied) {
        [self.locationManager stopUpdatingLocation];
        self.updatingLocation = NO;
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil)
                                                        message:NSLocalizedString(@"To find places near you, please enable location services", @"Error message for location denied")
                                                       delegate:nil
                                              cancelButtonTitle:NSLocalizedString(@"OK", nil)
                                              otherButtonTitles:nil];
        [alert show];
    }
}

- (void)locationTimeout:(NSTimer *)timer
{
    if (self.updatingLocation) {
        [self.locationManager stopUpdatingLocation];
        self.updatingLocation = NO;
        if (!self.currentLocation) {
            [SVProgressHUD showErrorWithStatus:NSLocalizedString(@"Unable to locate you", @"Error msg when the location service failed to find the user's location")];
        }
    }
}

@end
