//
//  ZECountriesViewController.m
//  Zerved
//
//  Created by Anders Rasmussen on 01/08/13.
//
//  List available merchant countries. Countries link to ZELocationsAlphabeticViewController.
//

#import "ZECountriesViewController.h"
#import "SVProgressHUD.h"
#import "ZECountry.h"
#import "QuartzCore/CAGradientLayer.h"
#import "ZELocationsAlphabeticViewController.h"

@interface ZECountriesViewController ()

@end

@implementation ZECountriesViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = NSLocalizedString(@"Countries", nil);
    // Set white background view
    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.opaque = NO;
    self.tableView.backgroundView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.tableView.frame.size.width, self.tableView.frame.size.height)];
    self.tableView.backgroundView.backgroundColor = [UIColor whiteColor];
    [self reload];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)reload
{
    // Load countries
    [SVProgressHUD showWithStatus:NSLocalizedString(@"Loading", nil)];
    RKObjectManager *objectManager = [RKObjectManager sharedManager];
    [objectManager getObjectsAtPath:[NSString stringWithFormat:@"countries"]
                         parameters:nil
                            success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
                                // Locations loaded
                                [SVProgressHUD dismiss];
                                self.countries = [mappingResult array];
                                [self.tableView reloadData];
                            }
                            failure:^(RKObjectRequestOperation *operation, NSError *error) {
                                // Failed to load locations
                                [SVProgressHUD dismiss];
                                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil)
                                                                                message:[error localizedDescription]
                                                                               delegate:nil
                                                                      cancelButtonTitle:NSLocalizedString(@"OK", nil)
                                                                      otherButtonTitles:nil];
                                [alert show];
                            }];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return ([self.countries count] > 0 ? 1 : 0);
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.countries count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"CountryCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    // Configure the cell...
    ZECountry *country = [self.countries objectAtIndex:indexPath.row];
    cell.textLabel.text = country.name;
    
    return cell;
}

#pragma mark - Table view delegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"ViewLocations"]) {
        // Go to categories
        ZELocationsAlphabeticViewController *destinationController = [segue destinationViewController];
        destinationController.country = [self.countries objectAtIndex:[self.tableView indexPathForSelectedRow].row];
    }
}

@end
