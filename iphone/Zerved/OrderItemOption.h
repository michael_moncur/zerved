//
//  OrderItemOption.h
//  Zerved
//
//  Created by Zerved Development Mac on 16/08/13.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@interface OrderItemOption : NSObject

@property (nonatomic, strong) NSString * optionName;
@property (nonatomic, strong) NSArray * values;
@property (nonatomic, strong) NSObject * orderItem;
@end
