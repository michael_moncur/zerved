//
//  ZEOrderViewController.h
//  Zerved
//
//  Created by Zerved Development Mac on 15/08/13.
//
//

#import <UIKit/UIKit.h>
#import "ZEPinViewController.h"
#import <RestKit/RestKit.h>
#import "SVProgressHUD.h"
#import "ZEAppDelegate.h"
#import "Order.h"
#import "OrderItem.h"
#import "OrderItemOption.h"
#import "OrderItemOptionValue.h"
#import "ZEReceiptView.h"
#import "TaxTotal.h"
#import "ZEConsumerDelegate.h"

@interface ZEOrderViewController : UIViewController <ZEPinViewControllerDelegate, ZEConsumerDelegate, UIAlertViewDelegate>

@property (strong, nonatomic) Order * order;
@property (strong, nonatomic) NSString *orderId;
@property (strong, nonatomic) NSString *orderKey;
@property (strong, nonatomic) ZEPinViewController *pinController;
@property (strong, nonatomic) NSString *pincode;

@property CGFloat current_y;

@property (nonatomic, retain) IBOutlet UIScrollView  *scrollView;
@property (nonatomic, retain) IBOutlet ZEReceiptView *receiptView;

@property BOOL showNotificationsMessage;
@property BOOL showStatusMessage;

-(IBAction)refresh:(id)sender;


@end
