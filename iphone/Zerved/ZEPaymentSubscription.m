//
//  ZEPaymentSubscription.m
//  Zerved
//
//  Created by Anders Rasmussen on 15/05/13.
//
//

#import "ZEPaymentSubscription.h"

@implementation ZEPaymentSubscription

@synthesize description;

- (BOOL)isEqual:(id)anObject
{
    if (anObject == self)
        return YES;
    if (!anObject || ![anObject isKindOfClass:[self class]])
        return NO;
    return [self.key isEqualToString:((ZEPaymentSubscription *)anObject).key];
}

@end
