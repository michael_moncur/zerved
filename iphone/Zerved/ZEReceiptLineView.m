//
//  ZEReceiptLineView.m
//  Zerved
//
//  Created by Zerved Development Mac on 19/08/13.
//
//

#import "ZEReceiptLineView.h"

@implementation ZEReceiptLineView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.backgroundColor = [UIColor colorWithWhite:0.0f alpha:0.0f];
        self.isItem = NO;
    }
    return self;
}

- (void) setText:(NSString *) string textAlign:(NSTextAlignment)alignment
{
    self.label.text = string;
    self.label.textAlignment = alignment;
    if(self.isItem) {
        self.label.textColor = [UIColor blackColor];
        self.label.font = [UIFont fontWithName:@"Courier" size:14.0];
    }
}


- (void) setTextWithLeft:(NSString *)left Right:(NSString *)right important:(BOOL)i shareSpaceEven:(BOOL)shareEven
{
    self.label.text = [self receiptLineLeft:left Right:right important:i shareSpaceEven:shareEven];
    self.label.textAlignment = NSTextAlignmentLeft;
    if(self.isItem) {
        self.label.textColor = [UIColor blackColor];
        self.label.font = [UIFont fontWithName:@"Courier" size:14.0];
    }
}



- (NSString *) receiptLineLeft:(NSString *)left Right:(NSString *) right important:(BOOL)i shareSpaceEven:(BOOL)shareEven
{
	int lineWidth = (i ? 32 : 44);
	int maxRight = shareEven ? ((lineWidth - 2) / 2) : lineWidth;
	if(right.length > maxRight)
	{
		right = [NSString stringWithFormat:@"%@...", [right substringToIndex:maxRight - 3]];
	}
	int maxLeft = shareEven ? ((lineWidth - 2) / 2) : (lineWidth - (int)right.length);
	if (left.length > maxLeft)
	{
		left = [NSString stringWithFormat:@"%@... ", [left substringToIndex:maxLeft - 4]];
	}
	int spacing = lineWidth - ((int)left.length + (int)right.length);
	return [NSString stringWithFormat:@"%@%*s%@", left, spacing, "", right];
}

@end