//
//  ZETermsViewController.h
//  Zerved
//
//  Created by Anders Rasmussen on 01/08/12.
//  Copyright (c) 2012 Zerved. All rights reserved.
//

#import "ZEWebViewController.h"

@interface ZEModalWebpageViewController : ZEWebViewController
@property id delegate;
@property (nonatomic, strong) NSString *url;
- (IBAction)done:(id)sender;
@end


@protocol ZEModalWebpageViewControllerDelegate <NSObject>
- (void)modalWebpageViewControllerDone:(ZEModalWebpageViewController *)termsController;
@end