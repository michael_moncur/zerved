//
//  ZEEmbeddedViewController.h
//  Zerved
//
//  Created by Henrik Karlsen on 10/10/16.
//
//

#import "ZEWebViewController.h"
#import "ZEModalWebpageViewController.h"

@interface ZEEmbeddedViewController : ZEWebViewController <ZEModalWebpageViewControllerDelegate>

- (void) paymentSuccess;
- (void) cancelOrder;
- (void) handleInterruptedMobilePayPayment;

@end
