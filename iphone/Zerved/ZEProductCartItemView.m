//
//  ZEProductCartItemView.m
//  Zerved
//
//  Created by Anders Rasmussen on 26/09/13.
//
//

#import "ZEProductCartItemView.h"

@implementation ZEProductCartItemView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

- (void)attachQuantityStepper
{
    [self.quantityStepper addTarget:self action:@selector(quantityChanged:) forControlEvents:UIControlEventValueChanged];
}

- (void)quantityChanged:(UIStepper *)sender
{
    self.quantityLabel.text = [NSString stringWithFormat:@"%d", (int)sender.value];
    self.priceLabel.text = @"--";
}

@end
