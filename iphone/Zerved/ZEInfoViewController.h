//
//  ZEInfoViewController.h
//  Zerved
//
//  Created by Anders Rasmussen on 08/06/12.
//  Copyright (c) 2012 Zerved. All rights reserved.
//

#import "ZEWebViewController.h"
#import "ZEModalWebpageViewController.h"

@interface ZEInfoViewController : ZEWebViewController <ZEModalWebpageViewControllerDelegate>

@end
