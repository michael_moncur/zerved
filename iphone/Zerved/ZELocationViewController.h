//
//  ZELocationViewController.h
//  Zerved
//
//  Created by Anders Rasmussen on 19/04/13.
//
//

#import <UIKit/UIKit.h>
#import <RestKit/RestKit.h>
#import "ZELocation.h"
#import <MapKit/MapKit.h>

#define BASE_EVENT_CELL_HEIGHT 60
#define BASE_EVENT_TITLE_HEIGHT 21
#define EVENT_TITLE_WIDTH 272
#define ICON_TITLE_OFFSET_Y 6

extern const int SECTION_ENTRANCE_CATEGORIES;
extern const int SECTION_BARS;
extern const int SECTION_MENUS;
extern const int SECTION_MENU_CATEGORIES;
extern const int SECTION_EVENTS;
extern const int SECTION_NOW_AND_EVENTS;
extern const int EVENT_CELL_TAG;
extern const int MAX_EVENTS;

@interface ZELocationViewController : UITableViewController <MKMapViewDelegate, UIScrollViewDelegate>

@property (strong, nonatomic) NSString *locationKey;
@property (strong, nonatomic) ZELocation *location;
@property (nonatomic, assign) BOOL showAllBars;

@property (strong, nonatomic) UIView *headerView;
@property (strong, nonatomic) UIScrollView *headerScrollView;
@property (strong, nonatomic) MKMapView *mapView;
@property (strong, nonatomic) UIPageControl *pageControl;

@end
