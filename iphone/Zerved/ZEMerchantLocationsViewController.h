//
//  ZEMerchantLocationsViewController.h
//  Zerved
//
//  Created by Anders Rasmussen on 05/08/13.
//
//

#import <UIKit/UIKit.h>
#import "ZELocation.h"
#import <RestKit/RestKit.h>

@interface ZEMerchantLocationsViewController : UITableViewController

@property (strong, nonatomic) NSArray *locations;
@property (strong, nonatomic) CLLocationManager *locationManager;

@end
