//
//  ZEConsumer.m
//  Zerved
//
//  Created by Anders Rasmussen on 15/05/13.
//
//

#import "ZEConsumer.h"

@implementation ZEConsumer

@synthesize phone=_phone;

- (void)setPhone:(NSString *)phone
{
    _phone = [phone stringByReplacingOccurrencesOfString:@" " withString:@""];
}

- (NSMutableArray *)arrayOfStrings
{
    // Return array of email and address texts. An array cannot contain nil, so we add empty string in that case.
    NSMutableArray *strings = [[NSMutableArray alloc] initWithCapacity:6];
    [strings addObject:(self.email != nil ? self.email : @"")];
    [strings addObject:(self.name != nil ? self.name : @"")];
    [strings addObject:(self.address1 != nil ? self.address1 : @"")];
    [strings addObject:(self.address2 != nil ? self.address2 : @"")];
    [strings addObject:(self.city != nil ? self.city : @"")];
    [strings addObject:(self.postcode != nil ? self.postcode : @"")];
    [strings addObject:(self.phone != nil ? self.phone : @"")];
    return strings;
}

@end
