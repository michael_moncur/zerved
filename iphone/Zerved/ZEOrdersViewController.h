//
//  ZEOrdersViewController.h
//  Zerved
//
//  Created by Anders Rasmussen on 05/07/13.
//
//

#import <UIKit/UIKit.h>
#import "ZEConsumerDelegate.h"

@interface ZEOrdersViewController : UITableViewController <ZEConsumerDelegate>
@end
