//
//  ZETextFieldLabelCell.h
//  Zerved
//
//  Created by Anders Rasmussen on 15/05/13.
//
//

#import <UIKit/UIKit.h>

@interface ZETextFieldLabelCell : UITableViewCell
@property (nonatomic, weak) IBOutlet UITextField *inputField;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@end
