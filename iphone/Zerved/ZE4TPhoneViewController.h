//
//  ZE4TPhoneViewController.h
//  Zerved
//
//  Created by Anders Rasmussen on 29/08/13.
//
//

#import <UIKit/UIKit.h>
#import <RestKit/RestKit.h>
#import "ZEConsumer.h"
#import "ZECart.h"

@protocol ZE4TPhoneDelegate;

@interface ZE4TPhoneViewController : UIViewController

@property (nonatomic, strong) ZEConsumer *consumer;
@property (strong, nonatomic) ZECart *cart;
@property (weak, nonatomic) id<ZE4TPhoneDelegate> delegate;

@property (weak, nonatomic) IBOutlet UITextField *phoneField;
- (IBAction)done:(id)sender;

@end

@protocol ZE4TPhoneDelegate <NSObject>
@optional
- (void)ze4TViewControllerDidPlaceOrder:(ZE4TPhoneViewController *)controller id:(NSString *)orderId key:(NSString *)orderKey;
@end