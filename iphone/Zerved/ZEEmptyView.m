//
//  ZEEmptyView.m
//  Zerved
//
//  Created by Anders Rasmussen on 22/04/13.
//
//

#import "ZEEmptyView.h"
#import "QuartzCore/CAGradientLayer.h"

@implementation ZEEmptyView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setupViewWithFrame:(CGRect)frame text:(NSString *)text
{
    self.frame = frame;
    /*CAGradientLayer *grad = [CAGradientLayer layer];
    grad.frame = frame;
    grad.colors = [NSArray arrayWithObjects:(id)[[UIColor whiteColor] CGColor], (id)[[UIColor colorWithRed:0.82 green:0.82 blue:0.82 alpha:1.0] CGColor], nil];
    [self.layer insertSublayer:grad atIndex:0];*/
    self.emptyLabel.text = text;
}

- (void)setupViewForTableView:(UITableView *)parentView text:(NSString *)text
{
    self.frame = CGRectMake(0, 0, parentView.frame.size.width, parentView.frame.size.height-parentView.contentInset.top-parentView.contentInset.bottom);
    self.emptyLabel.text = text;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
