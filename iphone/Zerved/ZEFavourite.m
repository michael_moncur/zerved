
//
//  ZEFavourite.m
//  Zerved
//
//  Created by Zerved Development Mac on 26/07/13.
//
//

#import "ZEFavourite.h"

@implementation ZEFavourite

-(id) initWithProduct:(ZEProduct*)product andOptions:(NSMutableArray*)opt{
    if (self = [super init]) {
        self.product = product;
        self.options = opt;
        return self;
    }
    return nil;
}
- (BOOL)isEqual:(id)anObject
{
    if (anObject == self)
        return YES;
    if (!anObject || ![anObject isKindOfClass:[self class]])
        return NO;
    
    ZEFavourite *favourite = (ZEFavourite *)anObject;
    
    // product
    if(![self.product.key isEqualToString:favourite.product.key])
        return NO;
    
    // options
    if(![self optionsEqualToOptions:favourite.options]) // Not working yet
        return NO;
    
    return YES;
}

- (BOOL) optionsEqualToOptions:(NSMutableArray *)options{
    
    BOOL hasEqualOption = NO;
    if([self.options count] != [options count])
        return NO;

    for(int i=0; i < [self.options count]; i++){
        ZECartItemOptionValue * option = (ZECartItemOptionValue *) [self.options objectAtIndex:i];
        hasEqualOption = NO;
        for(int j=0; j < [options count]; j++){
            if([option isEqual:[options objectAtIndex:j]]){
                hasEqualOption = YES;
            }
        }
        if(!hasEqualOption)
            return NO;
    }
    return YES;
}

@end
