//
//  ZELocationsViewController.h
//  Zerved
//
//  Created by Anders Rasmussen on 16/04/13.
//
//

#import <UIKit/UIKit.h>
#import <RestKit/RestKit.h>
#import "CoreLocation/CoreLocation.h"
#import "ZELocationsViewController.h"

@interface ZELocationsNearbyViewController : UITableViewController <CLLocationManagerDelegate, ZELocationsViewProtocol>
@property (strong, nonatomic) CLLocationManager *locationManager;
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSArray *locations;
@property (strong, nonatomic) CLLocation *currentLocation;
@property (nonatomic, assign) BOOL updatingLocation;
- (IBAction)reload:(id)sender;
@end
