//
//  ZEProductOptionsViewController.h
//  Zerved
//
//  Created by Anders Rasmussen on 06/06/13.
//
//

#import <UIKit/UIKit.h>
#import <RestKit/RestKit.h>
#import "ZECart.h"
#import "ZECartItem.h"
#import "ZEProduct.h"

@interface ZEProductOptionsViewController : UITableViewController

@property (nonatomic, strong) NSString *productKey;
@property (nonatomic, strong) ZECart *cart;
@property (nonatomic, strong) ZECartItem *cartItem;
@property (nonatomic) NSUInteger itemIndex;
@property (nonatomic, strong) ZEProduct *product;
- (IBAction)done:(id)sender;

@end
