//
//  ZEInfoViewController.m
//  Zerved
//
//  Created by Anders Rasmussen on 08/06/12.
//  Copyright (c) 2012 Zerved. All rights reserved.
//
//  Simple information page displayed as a web page loaded from zervedapp.com.
//

#import "ZEInfoViewController.h"

@interface ZEInfoViewController ()

@end

@implementation ZEInfoViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    //[self addGreyBackground];
    if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1) {
        self.navigationController.navigationBar.tintColor = [UIColor blackColor];
    }
    [self reload];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    self.webView = nil;
}

- (void)reload
{
    NSString *fullURL = [self getUrl:@"info"];
    NSURL *url = [NSURL URLWithString:fullURL];
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
    [self.webView loadRequest:requestObj];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)handleCall:(NSString*)functionName callbackId:(int)callbackId args:(NSDictionary*)args
{
    if ([functionName isEqualToString:@"showTerms"]) {
        [self performSegueWithIdentifier:@"ShowTerms" sender:self];
    } else if ([functionName isEqualToString:@"showLicenses"]) {
        [self performSegueWithIdentifier:@"ShowLicenses" sender:self];
    } else if ([functionName isEqualToString:@"recommendVenue"]) {
        [self performSegueWithIdentifier:@"RecommendVenue" sender:self];
    } else {
        [super handleCall:functionName callbackId:callbackId args:args];
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"ShowTerms"]) {
        UINavigationController *navController = [segue destinationViewController];
        ZEModalWebpageViewController *termsController = [navController.viewControllers objectAtIndex:0];
        termsController.url = @"terms";
        termsController.delegate = self;
    } else if ([[segue identifier] isEqualToString:@"ShowLicenses"]) {
        UINavigationController *navController = [segue destinationViewController];
        ZEModalWebpageViewController *termsController = [navController.viewControllers objectAtIndex:0];
        termsController.url = @"softwarelicenses";
        termsController.delegate = self;
    }
}

- (void)modalWebpageViewControllerDone:(ZEModalWebpageViewController *)termsController
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
