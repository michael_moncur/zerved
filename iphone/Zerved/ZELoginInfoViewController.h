//
//  ZELoginInfoViewController.h
//  Zerved
//
//  Created by Anders Rasmussen on 22/10/12.
//
//

#import "ZEWebViewController.h"

@interface ZELoginInfoViewController : ZEWebViewController
@property id delegate;
- (IBAction)done:(id)sender;
@end

@protocol ZELoginInfoDelegate <NSObject>
- (void)loginInfoControllerDone:(ZELoginInfoViewController *)loginInfoController;
@end