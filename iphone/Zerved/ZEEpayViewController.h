//
//  ZEEpayViewController.h
//  Zerved
//
//  Created by Henrik Hoey Karlsen on 25/09/14.
//
//

#import <UIKit/UIKit.h>
#import <RestKit/RestKit.h>
#import "ZEModalWebpageViewController.h"
#import "ZECart.h"

FOUNDATION_EXPORT NSString *const EPAY_PROVIDER;

@protocol ZEEpayViewControllerDelegate;

@interface ZEEpayViewController : UITableViewController <UITextFieldDelegate, ZEModalWebpageViewControllerDelegate, ZEConsumerDelegate>
{
    UIToolbar* numberToolbar;
}

// Set these parameters when loading
@property (nonatomic, assign) BOOL shouldPlaceOrder;
@property (strong, nonatomic) ZECart *cart;

// Objects
@property (weak, nonatomic) id<ZEEpayViewControllerDelegate> delegate;
@property (nonatomic, strong) NSTimer *timer;
@property (nonatomic, strong) NSDate *paymentStartedAt;
@property (nonatomic) NSUInteger tabIndex;
@property (nonatomic, copy) NSString *orderId;
@property (nonatomic, copy) NSString *orderKey;

// UI elements
@property (weak, nonatomic) IBOutlet UITextField *pinField;
@property (strong, nonatomic) UIBarButtonItem *submitButton;
@property (weak, nonatomic) IBOutlet UITableViewCell *pinCell;
@property (weak, nonatomic) IBOutlet UITableViewCell *saveCardCell;
@property (weak, nonatomic) IBOutlet UISwitch *saveCardSwitch;
@property (weak, nonatomic) IBOutlet UILabel *saveCardReserveAmountInfoLabel;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicatorView;

// Methods
- (void)submit;
- (IBAction)saveCardSwitched:(UISwitch *)sender;

@end

@protocol ZEEpayViewControllerDelegate <NSObject>
@optional
- (void)epayViewControllerDidPlaceOrder:(ZEEpayViewController *)controller id:(NSString *)orderId key:(NSString *)orderKey fromTabIndex:(NSUInteger)tabIndex;

@end