//
//  ZEMessageView.h
//  Zerved
//
//  Created by Zerved Development Mac on 20/08/13.
//
//

#import <UIKit/UIKit.h>
#import "QuartzCore/CAGradientLayer.h"
#import <QuartzCore/QuartzCore.h>

@interface ZEMessageView : UIView


@property (nonatomic, retain) CAGradientLayer *grad;

@property (nonatomic, retain) UILabel *title;
@property (nonatomic, retain) UILabel *message;


- (void)setMessageTitle:(NSString *) title;
- (void)setMessageContent:(NSString *) content;
- (void)setCommentTheme:(NSString *)comment;
@end
