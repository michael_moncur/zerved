//
//  ZEInfoPanelView.m
//  Zerved
//
//  Created by Anders Rasmussen on 29/01/14.
//
//

#import "ZEInfoPanelView.h"
#import "NSString+Additions.h"
#import <QuartzCore/QuartzCore.h>

@implementation ZEInfoPanelView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)createWithLocation:(ZELocation *)location maxHeight:(float)maxHeight
{
    UIFont *font = [UIFont fontWithName:@"HelveticaNeue-Light" size:12];
    UIFont *boldFont = [UIFont fontWithName:@"HelveticaNeue-Medium" size:12];
    self.collapsedHeight = 0; // Height of content when collapsed
    self.expandedHeight = 0; // Height of content when expanded
    self.textWidth = self.frame.size.width-35-10;
    self.textHeight = 14;
    self.textMargin = 5;
    self.x = 35;
    self.y = 0;
    
    // Adding content to the panel
    
    self.y += self.textMargin;
    self.collapsedHeight += self.textMargin;
    self.expandedHeight += self.textMargin;
    
    if (location.address && ![location.address isEqualToString:@""]) {
        // Short address (oneline)
        self.shortAddressLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.x, self.y, self.textWidth, self.textHeight)];
        self.shortAddressLabel.text = location.onelineAddress;
        self.shortAddressLabel.textColor = [UIColor whiteColor];
        self.shortAddressLabel.font = font;
        self.shortAddressLabel.backgroundColor = [UIColor clearColor];
        [self.scrollView addSubview:self.shortAddressLabel];
        self.collapsedHeight += self.shortAddressLabel.frame.size.height + self.textMargin;
        
        // Address icon
        [self addIconNamed:@"infopanel_address_icon.png"];
        
        // Full address (dynamic height)
        CGSize labelSize = [location.address sizeWithFont:font constrainedTo:CGSizeMake(self.textWidth, FLT_MAX)];
        self.fullAddressLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.x, self.y-0.5, ceil(labelSize.width), ceil(labelSize.height))]; // for some reason we need to move it 0.5pt up to match the short position exactly
        self.fullAddressLabel.text = location.address;
        self.fullAddressLabel.textColor = [UIColor whiteColor];
        self.fullAddressLabel.font = font;
        self.fullAddressLabel.backgroundColor = [UIColor clearColor];
        self.fullAddressLabel.numberOfLines = 0;
        self.fullAddressLabel.hidden = YES;
        [self.scrollView addSubview:self.fullAddressLabel];
        
        [self moveCursorDown:self.fullAddressLabel.frame.size.height+self.textMargin];
    }
    
    if (location.websiteUrl && ![location.websiteUrl isEqualToString:@""]) {
        [self addLabelWithText:location.websiteUrl font:font iconNamed:@"infopanel_www_icon.png"];
    }
    
    if (location.facebook && ![location.facebook isEqualToString:@""]) {
        [self addLabelWithText:[NSString stringWithFormat:@"facebook.com/%@", location.facebook] font:font iconNamed:@"infopanel_fb_icon.png"];
    }
    
    if (location.twitter && ![location.twitter isEqualToString:@""]) {
        [self addLabelWithText:[NSString stringWithFormat:@"@%@", location.twitter] font:font iconNamed:@"infopanel_tw_icon.png"];
    }
    
    if (location.googlePlus && ![location.googlePlus isEqualToString:@""]) {
        [self addLabelWithText:[NSString stringWithFormat:@"plus.google.com/%@", location.googlePlus] font:font iconNamed:@"infopanel_gp_icon.png"];
    }
    
    if (location.linkedIn && ![location.linkedIn isEqualToString:@""]) {
        [self addLabelWithText:[NSString stringWithFormat:@"linkedin.com/company/%@", location.linkedIn] font:font iconNamed:@"infopanel_in_icon.png"];
    }
    
    if (location.instagram && ![location.instagram isEqualToString:@""]) {
        [self addLabelWithText:[NSString stringWithFormat:@"instagram.com/%@", location.instagram] font:font iconNamed:@"infopanel_instagram_icon.png"];
    }
    
    if (location.pinterest && ![location.pinterest isEqualToString:@""]) {
        [self addLabelWithText:[NSString stringWithFormat:@"pinterest.com/%@", location.pinterest] font:font iconNamed:@"infopanel_pin_icon.png"];
    }
    
    if (location.flickr && ![location.flickr isEqualToString:@""]) {
        [self addLabelWithText:[NSString stringWithFormat:@"flickr.com/people/%@", location.flickr] font:font iconNamed:@"infopanel_flickr_icon.png"];
    }
    
    if (location.phone && ![location.phone isEqualToString:@""]) {
        [self addLabelWithText:location.phone font:font iconNamed:@"infopanel_phone_icon.png"];
    }
    
    if (location.phone2 && ![location.phone2 isEqualToString:@""]) {
        [self addLabelWithText:location.phone2 font:font iconNamed:@"infopanel_phone_icon.png"];
    }
    
    if (location.openingHoursMonday) {
        [self addOpeningHourLabelsForDay:NSLocalizedString(@"Monday", nil) openingHours:location.openingHoursMonday font:font iconNamed:@"infopanel_clock_icon.png"];
        [self addOpeningHourLabelsForDay:NSLocalizedString(@"Tuesday", nil) openingHours:location.openingHoursTuesday font:font iconNamed:nil];
        [self addOpeningHourLabelsForDay:NSLocalizedString(@"Wednesday", nil) openingHours:location.openingHoursWednesday font:font iconNamed:nil];
        [self addOpeningHourLabelsForDay:NSLocalizedString(@"Thursday", nil) openingHours:location.openingHoursThursday font:font iconNamed:nil];
        [self addOpeningHourLabelsForDay:NSLocalizedString(@"Friday", nil) openingHours:location.openingHoursFriday font:font iconNamed:nil];
        [self addOpeningHourLabelsForDay:NSLocalizedString(@"Saturday", nil) openingHours:location.openingHoursSaturday font:font iconNamed:nil];
        [self addOpeningHourLabelsForDay:NSLocalizedString(@"Sunday", nil) openingHours:location.openingHoursSunday font:font iconNamed:nil];
        [self moveCursorDown:self.textMargin];
    }
    
    if ([location.bars count] > 0) {
        [self addIconNamed:@"infopanel_zerved_icon.png"];
        for (ZEBar *bar in location.bars) {
            [self addBarLabelsForBar:bar barFont:boldFont serviceFont:font];
        }
        [self moveCursorDown:self.textMargin];
    }
    
    // Update height to match collapsed content + button
    self.scrollViewHeightConstraint = [NSLayoutConstraint constraintWithItem:self.scrollView attribute:NSLayoutAttributeHeight relatedBy:0 toItem:Nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1 constant:self.collapsedHeight];
    [self.scrollView addConstraint:self.scrollViewHeightConstraint];
    self.frame = CGRectMake(0, 0, self.frame.size.width, self.collapsedHeight+self.button.frame.size.height);
    
    // Scrollable content should be size of all text
    self.scrollView.contentSize = CGSizeMake(320, self.expandedHeight);
    
    // Disable scrolling when collapsed
    self.scrollView.scrollEnabled = NO;
    
    // Decrease expanded height if it is over max height top stop expansion at the bottom
    if (self.expandedHeight + self.button.frame.size.height > maxHeight) {
        self.expandedHeight = maxHeight - self.button.frame.size.height;
    }
    
    // Create background gradient
    CAGradientLayer *bgGradient = [CAGradientLayer layer];
    bgGradient.frame = self.gradientView.bounds;
    bgGradient.colors = [NSArray arrayWithObjects:(id)[UIColor colorWithWhite:0.0 alpha:0.8].CGColor, (id)[UIColor clearColor].CGColor, nil];
    [self.gradientView.layer insertSublayer:bgGradient atIndex:0];
    
    // Button
    [self.button addTarget:self action:@selector(expandCollapse:) forControlEvents:UIControlEventTouchUpInside];
}

- (UILabel *)addLabelWithText:(NSString *)text font:(UIFont *)font iconNamed:(NSString *)iconName
{
    return [self addLabelWithText:text font:font iconNamed:iconName margin:self.textMargin];
}

- (UILabel *)addLabelWithText:(NSString *)text font:(UIFont *)font iconNamed:(NSString *)iconName margin:(float)margin
{
    // Dynamic text height
    //CGSize labelSize = [text ios67CompatibleSizeWithFont:font constrainedTo:CGSizeMake(self.textWidth, FLT_MAX)];
    //UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(self.x, self.y, ceil(labelSize.width), ceil(labelSize.height))];
    
    // Fixed text height
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(self.x, self.y, self.textWidth, self.textHeight)];
    
    // Label
    label.text = text;
    label.textColor = [UIColor whiteColor];
    label.font = font;
    label.backgroundColor = [UIColor clearColor];
    [self.scrollView addSubview:label];
    
    // Icon
    if (iconName != nil) {
        [self addIconNamed:iconName];
    }
    
    // Record height / move cursor down
    [self moveCursorDown:label.frame.size.height+margin];
    return label;
}

- (void)addOpeningHourLabelsForDay:(NSString *)day openingHours:(NSString *)openingHours font:(UIFont *)font iconNamed:(NSString *)iconName
{
    float col1width = 80;
    
    // Day label
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(self.x, self.y, col1width, self.textHeight)];
    label.text = day;
    label.textColor = [UIColor whiteColor];
    label.font = font;
    label.backgroundColor = [UIColor clearColor];
    [self.scrollView addSubview:label];
    
    // Opening hour value
    UILabel *label2 = [[UILabel alloc] initWithFrame:CGRectMake(self.x+col1width, self.y, self.textWidth-col1width, self.textHeight)];
    label2.text = openingHours;
    label2.textColor = [UIColor whiteColor];
    label2.font = font;
    label2.backgroundColor = [UIColor clearColor];
    [self.scrollView addSubview:label2];
    
    // Icon
    if (iconName != nil) {
        [self addIconNamed:iconName];
    }
    
    // Record height / move cursor down
    [self moveCursorDown:label.frame.size.height];
}

- (void)addBarLabelsForBar:(ZEBar *)bar barFont:(UIFont *)barFont serviceFont:(UIFont *)serviceFont
{
    // Bar label
    [self addLabelWithText:bar.name font:barFont iconNamed:nil margin:0];
    
    // Service types
    if ([bar.counterServiceToStay boolValue]) {
        [self addLabelWithText:NSLocalizedString(@"Pickup at counter to stay", nil) font:serviceFont iconNamed:nil margin:0];
    }
    if ([bar.counterServiceToGo boolValue]) {
        [self addLabelWithText:NSLocalizedString(@"Pickup at counter to go", nil) font:serviceFont iconNamed:nil margin:0];
    }
    if ([bar.tableService boolValue]) {
        [self addLabelWithText:NSLocalizedString(@"Serve at table", nil) font:serviceFont iconNamed:nil margin:0];
    }
    if ([bar.deliveryService boolValue]) {
        [self addLabelWithText:NSLocalizedString(@"Deliver to address", nil) font:serviceFont iconNamed:nil margin:0];
    }
}

- (void)moveCursorDown:(float)height
{
    self.expandedHeight += height;
    self.y += height;
}

- (void)addIconNamed:(NSString *)iconName
{
    UIImage *icon = [UIImage imageNamed:iconName];
    float iconX = 17.0 + (9.0-icon.size.width)/2.0; // horizontal align center in a 9pt column
    float iconY = self.y + (self.textHeight - icon.size.height)/2.0; // vertical align center with label
    UIImageView *addressIcon = [[UIImageView alloc] initWithFrame:CGRectMake(round(iconX), round(iconY), icon.size.width, icon.size.height)];
    addressIcon.image = icon;
    [self.scrollView addSubview:addressIcon];
}

- (void)expandCollapse:(UIButton *)sender
{
    sender.selected = !sender.selected;
    self.scrollView.scrollEnabled = sender.selected;
    float newHeight = sender.selected ? self.expandedHeight : self.collapsedHeight;
    // Animate
    [UIView animateWithDuration:0.5 animations:^{
        // New height
        self.scrollViewHeightConstraint.constant = newHeight;
        self.frame = CGRectMake(0, 0, self.frame.size.width, newHeight+self.button.frame.size.height);
        // Toggle between oneline and full address
        self.shortAddressLabel.hidden = sender.selected;
        self.fullAddressLabel.hidden = !sender.selected;
        [self layoutIfNeeded];
    } completion:nil];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
