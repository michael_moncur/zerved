//
//  ZEQuoteItem.h
//  Zerved
//
//  Created by Anders Rasmussen on 30/05/13.
//
//

#import <Foundation/Foundation.h>

@interface ZEQuoteItem : NSObject

@property (nonatomic, strong) NSNumber *qty;
@property (nonatomic, strong) NSNumber *price;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *priceFormatted;
@property (nonatomic, copy) NSNumber *valid;
@property (nonatomic, strong) NSString *optionsSummary;
@property (nonatomic, copy) NSNumber *productHasOptions;

@end
