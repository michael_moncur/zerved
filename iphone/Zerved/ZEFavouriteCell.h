//
//  ZEFavouriteCell.h
//  Zerved
//
//  Created by Zerved Development Mac on 29/07/13.
//
//

#import <UIKit/UIKit.h>
#import "ZEFavourite.h"

@interface ZEFavouriteCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UILabel *options;
@property (weak, nonatomic) IBOutlet UIButton *addToCart;
@property (weak, nonatomic) IBOutlet UIButton *deleteFavourite;
@property (nonatomic, retain) ZEFavourite * favourite;
@property (weak, nonatomic) IBOutlet UIView *itemsView;

@end
