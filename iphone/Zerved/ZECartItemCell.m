//
//  ZECartItemCell.m
//  Zerved
//
//  Created by Anders Rasmussen on 04/06/13.
//
//

#import "ZECartItemCell.h"
#import "DTCustomColoredAccessory.h"

@implementation ZECartItemCell

@synthesize cartItem = _cartItem;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        DLog(@"Init cart item cell");
        //[self.quantityStepper addTarget:self action:@selector(changeQuantity:) forControlEvents:UIControlEventTouchUpInside];
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setCartItem:(ZECartItem *)cartItem
{
    _cartItem = cartItem;
    self.quantityStepper.value = cartItem.quantity;
    // Show qty button if expanded
    self.quantityStepper.hidden = !cartItem.isExpanded;
    
    
    if (self.favouriteButton) {
        // Favourite spinner
        self.spinner = [[UIActivityIndicatorView alloc]initWithFrame:CGRectMake(15,8,22,22)];
        self.spinner.color = [UIColor grayColor];
        [self.spinner startAnimating];
        [self addSubview:self.spinner];
    
        // Show add to favourite button
        UIImage * image = (cartItem.isFavourite) ? [UIImage imageNamed:@"star_filled.png"] : [UIImage imageNamed:@"star.png"] ;
        [self.favouriteButton setImage:image forState:UIControlStateNormal];
    }
    
    // Show options if expanded and product has options
    self.optionsLabel.hidden = !cartItem.isExpanded || !cartItem.quoteItem || ![cartItem.quoteItem.productHasOptions boolValue];
    self.editButton.hidden = !cartItem.isExpanded || !cartItem.quoteItem || ![cartItem.quoteItem.productHasOptions boolValue];
    
    if (cartItem.quoteItem) {
        // Quote item text
        self.titleLabel.text = [NSString stringWithFormat:@"%d x %@", cartItem.quantity, cartItem.quoteItem.name];
        self.priceLabel.text = cartItem.quoteItem.priceFormatted;
        self.optionsLabel.text = cartItem.quoteItem.optionsSummary;
        if ([cartItem.quoteItem.optionsSummary isEqualToString:@""] && [cartItem.quoteItem.productHasOptions boolValue]) {
            self.optionsLabel.text = NSLocalizedString(@"No options selected", @"Cart item label when product options are available, but none has been selected");
        }
        [self.spinner setHidden:YES];
        if (self.favouriteButton) {
            [self.favouriteButton setHidden:NO];
        }
    } else {
        // Default text (before quote loaded)
        self.titleLabel.text = [NSString stringWithFormat:@"%d x %@", cartItem.quantity, cartItem.productName];
        self.priceLabel.text = @"--";
        self.optionsLabel.text = @"";
        [self.spinner setHidden:NO];
        if (self.favouriteButton) {
            [self.favouriteButton setHidden:YES];
        }
    }
    
    // Expand/collapse arrow
    self.arrowView.accessoryColor = [UIColor grayColor];
    if (self.cartItem.isExpanded) {
        self.arrowView.type = DTCustomColoredAccessoryTypeDown;
    } else {
        self.arrowView.type = DTCustomColoredAccessoryTypeRight;
    }
}

@end
