//
//  ZECartManager.m
//  Zerved
//
//  Created by Anders Rasmussen on 28/05/13.
//
//

#import "ZECartManager.h"
#import "ZEAppDelegate.h"
#import "ZEConsumerManager.h"

@implementation ZECartManager

- (id)init
{
    if (self = [super init]) {
        self.carts = [[NSMutableDictionary alloc] initWithCapacity:1];
        return self;
    }
    return nil;
}

- (ZECart *)getCartForLocation:(ZELocation *)location bar:(ZEBar *)bar group:(NSString *)group event:(ZEEvent *)event
{
    NSString *cartId = [self getCartIdForLocation:location bar:bar group:group event:event];
    ZECart *cart = [self.carts valueForKey:cartId];
    if (!cart) {
        cart = [[ZECart alloc] initWithLocation:location bar:bar group:group event:event];
        [self.carts setObject:cart forKey:cartId];
    }
    return cart;
}

- (NSString *)getCartIdForLocation:(ZELocation *)location bar:(ZEBar *)bar group:(NSString *)group event:(ZEEvent *)event
{
    if ([group isEqualToString:@"menu"]) {
        if (event != nil) {
            return [NSString stringWithFormat:@"%@_%@_%@", bar.key, group, event.key];
        } else {
            return [NSString stringWithFormat:@"%@_%@", bar.key, group];
        }
    } else {
        return [NSString stringWithFormat:@"%@_%@", location.key, group];
    }
}

@end
