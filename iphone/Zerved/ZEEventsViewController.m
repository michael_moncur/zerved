//
//  ZEEventsViewController.m
//  Zerved
//
//  Created by Anders Rasmussen on 24/10/13.
//
//  Displays a date picker and a list of events for the selected date.
//  When selecting an event, goes to bar list, or menu list of only one bar,
//  or category list if only one bar with one menu.
//

#import "ZEEventsViewController.h"
#import "SVProgressHUD.h"
#import "ZEBarsViewController.h"
#import "ZEBarMenusViewController.h"
#import "ZEBarCategoriesViewController.h"
#import "ZEEventCell.h"
#import "NSString+Additions.h"

@interface ZEEventsViewController ()

@end

@implementation ZEEventsViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.isLoaded = NO;
    
    self.headerLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
    self.headerLabel.backgroundColor = [UIColor colorWithRed:235/255.0f green:235/255.0f blue:236/255.0f alpha:1.0f];
    self.headerLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:15.0f];
    self.headerLabel.textAlignment = NSTextAlignmentCenter;
    self.headerLabel.text = NSLocalizedString(@"Select your event and serving time...", nil);
    CALayer *bottomBorder = [CALayer layer];
    bottomBorder.frame = CGRectMake(0.0f, 43.5f, self.headerLabel.frame.size.width, 0.5f);
    bottomBorder.backgroundColor = [UIColor colorWithWhite:0.8f
                                                     alpha:1.0f].CGColor;
    [self.headerLabel.layer addSublayer:bottomBorder];
    
    self.datePicker = [[UIDatePicker alloc] init];
    [self.datePicker sizeToFit];
    self.datePicker.frame = CGRectMake(0, self.headerLabel.frame.size.height, self.datePicker.frame.size.width, self.datePicker.frame.size.height);
    self.datePicker.datePickerMode = UIDatePickerModeDate;
    [self.datePicker addTarget:self action:@selector(dateChanged:) forControlEvents:UIControlEventValueChanged];
    
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, self.datePicker.frame.size.height+self.headerLabel.frame.size.height)];
    [headerView addSubview:self.headerLabel];
    [headerView addSubview:self.datePicker];
    [self.tableView setTableHeaderView:headerView];
    
    [self reload];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dateChanged:(id)sender
{
    [self reload];
}

- (void)reload
{
    [SVProgressHUD showWithStatus:NSLocalizedString(@"Loading events", nil)];
    self.isLoaded = NO;
    RKObjectManager *objectManager = [RKObjectManager sharedManager];
    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:self.datePicker.date];
    [objectManager getObjectsAtPath:[NSString stringWithFormat:@"locations/%@/events/%ld/%ld/%ld", self.location.key, (long)components.year, (long)components.month, (long)components.day]
                         parameters:nil
                            success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
                                // Location loaded
                                [SVProgressHUD dismiss];
                                self.isLoaded = YES;
                                self.events = [mappingResult array];
                                [self.tableView reloadData];
                            }
                            failure:^(RKObjectRequestOperation *operation, NSError *error) {
                                [SVProgressHUD dismiss];
                                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil)
                                                                                message:[error localizedDescription]
                                                                               delegate:nil
                                                                      cancelButtonTitle:NSLocalizedString(@"OK", nil)
                                                                      otherButtonTitles:nil];
                                [alert show];
                            }];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (self.isLoaded && [self.events count] == 0) {
        return 1; // empty msg cell
    } else {
        return [self.events count];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([self.events count] == 0) {
        // Empty msg cell
        UITableViewCell *emptyCell = [tableView dequeueReusableCellWithIdentifier:@"EmptyCell" forIndexPath:indexPath];
        return emptyCell;
    }
    
    static NSString *CellIdentifier = @"EventCell";
    ZEEventCell *eventCell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    ZEEvent *event = [self.events objectAtIndex:indexPath.row];
    eventCell.titleLabel.text = [NSString stringWithFormat:@"%@ %@", event.name, event.dateStartTimeFormatted];
    CGSize titleSize = [eventCell.titleLabel.text sizeWithFont:eventCell.titleLabel.font constrainedTo:CGSizeMake(EVENT_TITLE_WIDTH, FLT_MAX)];
    eventCell.titleLabel.frame = CGRectMake(eventCell.titleLabel.frame.origin.x, eventCell.titleLabel.frame.origin.y, EVENT_TITLE_WIDTH, ceil(titleSize.height));
    eventCell.subtitleLabel.text = [NSString stringWithFormat:NSLocalizedString(@"Serving time %@ (%@)", @"Label for serving time and time label, e.g. Serving time 16:00 (interval)"), event.dateDeliveryTimeFormatted, event.timeLabel];
    eventCell.subtitleLabel.frame = CGRectMake(eventCell.subtitleLabel.frame.origin.x, eventCell.titleLabel.frame.origin.y+eventCell.titleLabel.frame.size.height, eventCell.subtitleLabel.frame.size.width, eventCell.subtitleLabel.frame.size.height);
    eventCell.clockIcon.frame = CGRectMake(eventCell.clockIcon.frame.origin.x, eventCell.subtitleLabel.frame.origin.y+ICON_TITLE_OFFSET_Y, eventCell.clockIcon.frame.size.width, eventCell.clockIcon.frame.size.height);
    
    return eventCell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([self.events count] == 0) {
        return;
    }
    ZEEvent *selectedEvent = [self.events objectAtIndex:indexPath.row];
    // Event selected, load all event data
    [SVProgressHUD showWithStatus:NSLocalizedString(@"Loading event", nil)];
    RKObjectManager *objectManager = [RKObjectManager sharedManager];
    [objectManager getObjectsAtPath:[NSString stringWithFormat:@"events/%@", selectedEvent.key]
                         parameters:nil
                            success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
                                // Event loaded
                                ZEEvent *event = [mappingResult firstObject];
                                if ([event.bars count] == 0) {
                                    // No bars
                                    [SVProgressHUD showErrorWithStatus:NSLocalizedString(@"Event is not taking orders", @"Error msg for event with no bars/menus")];
                                } else if ([event.bars count] > 1) {
                                    // List bars
                                    [SVProgressHUD dismiss];
                                    ZEBarsViewController *barsController = [self.storyboard instantiateViewControllerWithIdentifier:@"BarsViewController"];
                                    barsController.location = self.location;
                                    barsController.bars = self.location.bars;
                                    barsController.title = event.name;
                                    barsController.event = event;
                                    [self.navigationController pushViewController:barsController animated:YES];
                                } else {
                                    // One bar
                                    ZEBar *bar = [event.bars firstObject];
                                    if ([bar.menus count] == 0) {
                                        // No menus
                                        [SVProgressHUD showErrorWithStatus:NSLocalizedString(@"Event is not taking orders", @"Error msg for event with no bars/menus")];
                                    } else if ([bar.menus count] > 1) {
                                        // List menus
                                        [SVProgressHUD dismiss];
                                        ZEBarMenusViewController *menusController = [self.storyboard instantiateViewControllerWithIdentifier:@"BarMenusViewController"];
                                        menusController.location = self.location;
                                        menusController.bar = bar;
                                        menusController.title = event.name;
                                        menusController.event = event;
                                        [self.navigationController pushViewController:menusController animated:YES];
                                    } else {
                                        // One menu, list categories
                                        [SVProgressHUD dismiss];
                                        ZEMenu *menu = [bar.menus firstObject];
                                        ZEBarCategoriesViewController *categoriesController = [self.storyboard instantiateViewControllerWithIdentifier:@"BarCategoriesViewController"];
                                        categoriesController.location = self.location;
                                        categoriesController.title = event.name;
                                        categoriesController.bar = bar;
                                        categoriesController.menu = menu;
                                        categoriesController.event = event;
                                        [self.navigationController pushViewController:categoriesController animated:YES];
                                    }
                                }
                            }
                            failure:^(RKObjectRequestOperation *operation, NSError *error) {
                                [SVProgressHUD dismiss];
                                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil)
                                                                                message:[error localizedDescription]
                                                                               delegate:nil
                                                                      cancelButtonTitle:NSLocalizedString(@"OK", nil)
                                                                      otherButtonTitles:nil];
                                [alert show];
                            }];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([self.events count] == 0) {
        return 60; // empty msg cell
    }
    
    ZEEvent *event = [self.events objectAtIndex:indexPath.row];
    NSString *titleString = [NSString stringWithFormat:@"%@ %@", event.name, event.dateStartTimeFormatted];
    CGSize titleSize = [titleString sizeWithFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:18] constrainedTo:CGSizeMake(EVENT_TITLE_WIDTH, FLT_MAX)];
    // Adjust base height to new height
    return BASE_EVENT_CELL_HEIGHT + ceil(titleSize.height) - BASE_EVENT_TITLE_HEIGHT;
}

@end
