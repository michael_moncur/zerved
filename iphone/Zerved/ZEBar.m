//
//  ZEBar.m
//  Zerved
//
//  Created by Anders Rasmussen on 19/04/13.
//
//

#import "ZEBar.h"

@implementation ZEBar

- (BOOL)hasCounterService
{
    return ([self.counterServiceToStay boolValue] || [self.counterServiceToGo boolValue]);
}

- (BOOL)hasToStayAndToGo
{
    return ([self.counterServiceToStay boolValue] && [self.counterServiceToGo boolValue]);
}

@end
