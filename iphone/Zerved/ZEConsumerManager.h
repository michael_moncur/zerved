//
//  ZEConsumer.h
//  Zerved
//
//  Created by Anders Rasmussen on 07/05/12.
//  Copyright (c) 2012 Zerved. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KeychainItemWrapper.h"
#import "ZELoginViewController.h"
#import "ZEConsumerDelegate.h"
#import "ZEFavourite.h"

@protocol ZEConsumerDelegate;

@interface ZEConsumerManager : NSObject <ZELoginViewControllerDelegate>

@property (retain, nonatomic) NSMutableData *receivedData;
@property (retain, nonatomic) KeychainItemWrapper *keychain;
@property (weak, nonatomic) id <ZEConsumerDelegate> delegate;
@property (weak, nonatomic) id updView;
@property (retain, nonatomic) NSString *accessToken;
@property (retain, nonatomic) NSHTTPURLResponse *lastResponse;
@property (retain, nonatomic) NSURLConnection *createAccountConnection;
@property (retain, nonatomic) NSURLConnection *loginConnection;
@property (retain, nonatomic) NSURLConnection *emailLoginConnection;
@property (retain, nonatomic) NSURLConnection *resetConnection;
@property (retain, nonatomic) NSURLConnection *createAccountViewConnection;
@property (retain, nonatomic) NSURLConnection *deleteFavouriteConnection;
@property (strong, nonatomic) NSDate *termsLastAccepted;
@property (weak, nonatomic) UIViewController *presentingController;
@property (strong, nonatomic) NSString *enteredPassword;

- (NSString *)consumerKey;
- (NSString *)consumerId;
- (void)createNewAccountInBackground;
- (void)createNewAccountFromView;
- (void)login:(NSString *)email password:(NSString *)password;
- (void)authenticateFromView:(UIViewController *)controller;
- (void)authenticateNewAccountFromView:(UIViewController *)controller email:(NSString *)email;
- (void)resetPassword:(NSString *)email;
- (void)setTermsAccepted:(BOOL)accepted;
- (BOOL)termsAcceptedRecently;
- (BOOL)deleteConsumerFavourite:(ZEFavourite *)favourite updateView:(id)s;
- (void)deleteAccount;
- (NSMutableURLRequest *)authenticatedRequestWithUrl:(NSString *)urlString;

@end
