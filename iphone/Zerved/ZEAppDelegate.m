//
//  ZEAppDelegate.m
//  Zerved
//
//  Created by Anders Rasmussen on 11/04/12.
//  Copyright (c) 2012 Zerved. All rights reserved.
//

#import "ZEAppDelegate.h"

#import <Crashlytics/Crashlytics.h>

#import "ZETabBarController.h"
#import "ZERestManager.h"
#import "SVProgressHUD.h"
#import "MobilePayManager.h"
#import "ZEOrderStatusChangeRequest.h"
#import "ZEOrderSpecialCommandRequest.h"
#import "ZEEmbeddedViewController.h"

@implementation ZEAppDelegate

@synthesize window = _window;
@synthesize cartManager = _cartManager;
@synthesize consumer = _consumer;
@synthesize lastNotification = _lastNotification;
@synthesize cart_mobile_pay_order = _cart_mobile_pay_order;
@synthesize order_id_mobile_pay_order = _order_id_mobile_pay_order;

int MobilePayPaymentPendingAlertViewTag = 97;
int MobilePayErrorAlertViewTag = 98;
int MobilePayCancelledAlertViewTag = 99;

BOOL app_was_opened_by_mobilepay;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // System wide background image
    /*UIImage *backgroundImage;
    if ([[UIScreen mainScreen] bounds].size.height == 568) {
        backgroundImage = [UIImage imageNamed:@"bg-568h.png"];
    } else {
        backgroundImage = [UIImage imageNamed:@"bg.png"];
    }
    [self.window addSubview:[[UIImageView alloc] initWithImage:backgroundImage]];
    */
    
    self.window = [[UIWindow alloc] initWithFrame:UIScreen.mainScreen.bounds];
    
    NSString* entryStoryBoardId = @"tabbarcontroller";
    
    #if defined(DEBUG) || defined(DEVELOPMENT)
        #ifdef EMBEDDED_MODE
            entryStoryBoardId = @"embedded";
        #endif
    #endif
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    UIViewController *viewController =  [storyboard instantiateViewControllerWithIdentifier:entryStoryBoardId];
    self.window.rootViewController = viewController;
    [self.window makeKeyAndVisible];
    
    self.window.backgroundColor = [UIColor whiteColor];

	[SVProgressHUD setDefaultStyle:SVProgressHUDStyleDark];

    self.cartManager = [[ZECartManager alloc] init];
    self.consumer = [[ZEConsumerManager alloc] init];
	
    app_was_opened_by_mobilepay = NO;
    
	// Clear application badge when app launches
	application.applicationIconBadgeNumber = 0;
    
    // If there's a notification, go to the tab
    UILocalNotification *localNotif = launchOptions[UIApplicationLaunchOptionsLocalNotificationKey];
    if (localNotif) {
        NSDictionary *userInfo = localNotif.userInfo;
        [self showOrderWithUserInfo:userInfo];
    }
    
    // Check for updates
    [self checkForUpdates];
    
    // Initialize REST
    [ZERestManager initRest];

    // Set useragent for UIWebViews (required if webview is used for hosting the embedded Zerved client).
    UIWebView *webView = [[UIWebView alloc] initWithFrame:CGRectZero];
    NSString *agent = [webView stringByEvaluatingJavaScriptFromString:@"navigator.userAgent"];
    NSDictionary *dictionary = [NSDictionary dictionaryWithObjectsAndKeys:[NSString stringWithFormat:@"%@ ZervedEmbedded ZervedIos/1.0", agent], @"UserAgent", nil];
    [[NSUserDefaults standardUserDefaults] registerDefaults:dictionary];
    
	// Crashlytics
	// Must be last according to this:
	// http://support.crashlytics.com/knowledgebase/articles/121064-why-don-t-i-see-data-from-my-first-crash-in-the-da
	[Crashlytics startWithAPIKey:@"254184b562a59b6c819c86b2aac51b9b5854fd9b"];
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    BOOL isAppSwitchInProgress = [[MobilePayManager sharedInstance]isAppSwitchInProgress];
    NSLog(@"isAppSwitchInProgress %d", isAppSwitchInProgress);
    NSLog(@"app_was_opened_by_mobilepay %d", app_was_opened_by_mobilepay);
    
    if (!app_was_opened_by_mobilepay) {
        // Check for interrupted MobilePay payment flow, eg. user has pressed the home button during payment in the MobilePay app and opened Zerved manually.
        
        BOOL is_resume_after_interrupted_mobilepay_payment = (self.cart_mobile_pay_order != nil && [self.cart_mobile_pay_order.card isEqualToString:@"mobilepay_appswitch"]);
        if (is_resume_after_interrupted_mobilepay_payment) {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"MobilePay payment", nil)
                                                                message:NSLocalizedString(@"The MobilePay payment experienced an unexpected condition. We will now check the payment status. Please check the status of your order.", nil)
                                                               delegate:self
                                                      cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
            alertView.tag = MobilePayPaymentPendingAlertViewTag;
            [alertView show];
        }
    }
        
    app_was_opened_by_mobilepay = NO;

}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

/* 
 * --------------------------------------------------------------------------------------------------------------
 *  BEGIN APNS CODE 
 * --------------------------------------------------------------------------------------------------------------
 */

- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings {
    [application registerForRemoteNotifications];
}

/**
 * Fetch and Format Device Token and Register Important Information to Remote Server
 */
- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)devToken {
	
#if !TARGET_IPHONE_SIMULATOR
    
	// Prepare the Device Token for Registration (remove spaces and < >)
	NSString *deviceToken = [[[[devToken description] 
                               stringByReplacingOccurrencesOfString:@"<"withString:@""] 
                              stringByReplacingOccurrencesOfString:@">" withString:@""] 
                             stringByReplacingOccurrencesOfString: @" " withString: @""];
    
    // Save consumer's device token on server
    NSString *fullURL = [NSString stringWithFormat:@"%@%@", SERVER_URL, @"savedevicetoken"];
    NSURL *url = [NSURL URLWithString:fullURL];
    NSString *requestString = [NSString stringWithFormat:@"access_token=%@&device_token=%@&sandbox=%@",
                               [self.consumer.accessToken stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],
                               [deviceToken stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],
                               APNS_USE_SANDBOX];
    NSData *requestData = [NSData dataWithBytes:[requestString UTF8String] length:[requestString length]];
    NSMutableURLRequest *requestObj = [NSMutableURLRequest requestWithURL:url];
    [requestObj setHTTPMethod:@"POST"];
    [requestObj setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
    [requestObj setHTTPBody:requestData];
    NSURLConnection *theConnection=[[NSURLConnection alloc] initWithRequest:requestObj delegate:nil];
    if (!theConnection) {
        DLog(@"Connection failed");
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"pushRegistrationCompleted" object:nil];
	
#endif
}

/**
 * Failed to Register for Remote Notifications
 */
- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
	
#if !TARGET_IPHONE_SIMULATOR
	
	DLog(@"Error in registration. Error: %@", error.debugDescription);
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"pushRegistrationFailed" object:nil];
	
#endif
}

/**
 * Remote Notification Received while application was open.
 */
- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
	
#if !TARGET_IPHONE_SIMULATOR
    
	NSDictionary *apsInfo = userInfo[@"aps"];
	application.applicationIconBadgeNumber = [apsInfo[@"badge"] integerValue];
	
    // Display local notification if running in foreground
    if (application.applicationState == UIApplicationStateActive) {
        NSDictionary *alertInfo = apsInfo[@"alert"];
        self.lastNotification = userInfo;
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Order Update", nil)
                                                            message:alertInfo[@"body"]
                                                           delegate:self cancelButtonTitle:alertInfo[@"action-loc-key"]
                                                  otherButtonTitles:nil];
        [alertView show];
    // If launched from notification, go directly to tab
    } else {
        // Shut down modal windows
        [[NSNotificationCenter defaultCenter] postNotificationName:@"switchToOrder" object:nil];
        [self showOrderWithUserInfo:userInfo];
    }
    
#endif
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == MobilePayErrorAlertViewTag) {
        return;
    }
    else if (alertView.tag == MobilePayPaymentPendingAlertViewTag) {
        
        if (_cart_mobile_pay_order != nil) {
                [SVProgressHUD showWithStatus:NSLocalizedString(@"Loading", nil)];
                ZEOrderSpecialCommandRequest *specialCommand = [ZEOrderSpecialCommandRequest alloc];
                specialCommand.special_command = @"mobilepay_handle_interrupted_payment";
                RKObjectManager *objectManager = [RKObjectManager sharedManager];
                [objectManager postObject:specialCommand path:[NSString stringWithFormat:@"orders/%@", self.order_id_mobile_pay_order] parameters:nil success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
                    // Refresh updated order
                    ZETabBarController *tabBarController = (ZETabBarController *)self.window.rootViewController;
                    [tabBarController viewOrderById:self.order_id_mobile_pay_order];

                    [self.cart_mobile_pay_order clear];
                    self.order_id_mobile_pay_order = nil;
                    self.cart_mobile_pay_order = nil;

                    [SVProgressHUD dismiss];
                } failure:^(RKObjectRequestOperation *operation, NSError *error) {
                    // Server error
                    [self.cart_mobile_pay_order clear];
                    self.order_id_mobile_pay_order = nil;
                    self.cart_mobile_pay_order = nil;
                    [SVProgressHUD showErrorWithStatus:error.localizedDescription];
                }];
        }

        return;
    }
    if (alertView.tag != MobilePayCancelledAlertViewTag) {
        NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
        if ([title isEqualToString:NSLocalizedString(@"Cancel", nil)]) {
            
        } else if ([title isEqualToString:NSLocalizedString(@"Update", nil)]) {
            // Go to app store (note: does not work in the emulator as it has no app store)
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"itms://itunes.com/apps/Zerved/Zerved"]];
        } else {
            // Show order
            // Shut down modal windows
            [[NSNotificationCenter defaultCenter] postNotificationName:@"switchToOrder" object:nil];
            [self showOrderWithUserInfo:self.lastNotification];
        }
    }
}

- (void)showOrderWithUserInfo:(NSDictionary *)userInfo
{
    ZETabBarController *tabBarController = (ZETabBarController *)self.window.rootViewController;
    NSString *orderId = (NSString *) userInfo[@"order_id"];
    [tabBarController viewOrderById:orderId];
}

/* 
 * --------------------------------------------------------------------------------------------------------------
 *  END APNS CODE 
 * --------------------------------------------------------------------------------------------------------------
 */

- (void) checkForUpdates
{
    // Load app store information about Zerved to see if there's a new version available
    NSString *jsonUrl = @"https://itunes.apple.com/lookup?id=554151816";
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:[NSURL URLWithString:jsonUrl]];
    NSURLConnection *theConnection = [[NSURLConnection alloc] initWithRequest:requestObj delegate:self];
    if (theConnection) {
        self.receivedData = [NSMutableData data];
    } else {
        DLog(@"Connection failed");
    }
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    [self.receivedData setLength:0];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [self.receivedData appendData:data];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSError *e = nil;
    NSDictionary *data = [NSJSONSerialization JSONObjectWithData:self.receivedData options:NSJSONReadingMutableContainers error:&e];
    NSInteger numResults = [data[@"resultCount"] integerValue];
    
    if(numResults > 0) // has results
    {
        NSArray *results = data[@"results"];
        NSDictionary *aResult = results[0];
        
        NSString    *appStoreVersion = [aResult valueForKey:@"version"];
        NSString    *localVersion    = [[NSBundle mainBundle] infoDictionary][@"CFBundleVersion"];
        
        if([appStoreVersion compare:localVersion options:NSNumericSearch] == NSOrderedDescending)
        {
            //there is an update available.
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Please update", nil) message:NSLocalizedString(@"There is an update available for Zerved. Please go to the app store to update the app.", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel", nil) otherButtonTitles:NSLocalizedString(@"Update", nil), nil];
            [alertView show];
        }
    }
}

- (void)connection:(NSURLConnection *)connection
  didFailWithError:(NSError *)error
{
    DLog(@"Connection failed! Error - %@ %@",
            [error localizedDescription],
            [error userInfo][NSURLErrorFailingURLStringErrorKey]);
}


/*
 * --------------------------------------------------------------------------------------------------------------
 *  MOBILEPAY START
 * --------------------------------------------------------------------------------------------------------------
 */

-(BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<NSString *,id> *)options
{
    //IMPORTANT - YOU MUST USE THIS IF YOU COMPILING YOUR AGAINST IOS9 SDK
    [self handleMobilePayPaymentWithUrl:url];
    return YES;
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
    //IMPORTANT - THIS IS DEPRECATED IN IOS9 - USE 'application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<NSString *,id> *)options' INSTEAD
    [self handleMobilePayPaymentWithUrl:url];
    return YES;
}

-(BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url {
    
    //IMPORTANT - THIS IS DEPRECATED IN IOS9 - USE 'application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<NSString *,id> *)options' INSTEAD
    [self handleMobilePayPaymentWithUrl:url];
    return YES;
}

- (void)handleMobilePayPaymentWithUrl:(NSURL *)url
{
#if defined(DEBUG) || defined(DEVELOPMENT)
#ifdef EMBEDDED_MODE
    [self handleMobilePayPaymentWithUrlEmbedded:url];
    return;
#endif
#endif

    app_was_opened_by_mobilepay = YES;

    [[MobilePayManager sharedInstance]handleMobilePayPaymentWithUrl:url success:^(MobilePaySuccessfulPayment * _Nullable mobilePaySuccessfulPayment) {
        NSString *orderId = mobilePaySuccessfulPayment.orderId;
        NSString *transactionId = mobilePaySuccessfulPayment.transactionId;
        NSString *amountWithdrawnFromCard = [NSString stringWithFormat:@"%f",mobilePaySuccessfulPayment.amountWithdrawnFromCard];
        NSLog(@"MobilePay purchase succeeded: order id '%@' and MobilePay transaction id '%@' and the amount: '%@'", orderId, transactionId,amountWithdrawnFromCard);
        
        if (self.cart_mobile_pay_order != nil) {
            [self.cart_mobile_pay_order clear];
        }
        self.order_id_mobile_pay_order = nil;

        ZETabBarController *tabBarController = (ZETabBarController *)self.window.rootViewController;
        [tabBarController viewOrderById:orderId];
        
        
    } error:^(NSError * _Nonnull error) {
        NSDictionary *dict = error.userInfo;
        NSString *errorMessage = [dict valueForKey:NSLocalizedFailureReasonErrorKey];
        NSLog(@"MobilePay purchase failed:  Error code '%li' and message '%@'",(long)error.code,errorMessage);

        NSString* title = @"";
        NSString* message = @"";
        if (error.code == MobilePayErrorCodeUpdateApp) {
            title = NSLocalizedString(@"You must update your MobilePay app ", nil);
            message = errorMessage;
        }
        else if (error.code == MobilePayErrorCodeTimeOut) {
            title = NSLocalizedString(@"MobilePay timeout ", nil);
            message = errorMessage;
        }
        else if (error.code == MobilePayErrorCodeLimitsExceeded) {
            title = NSLocalizedString(@"MobilePay amount limits exceeded ", nil);
            message = NSLocalizedString(@"Open MobilePay 'Beløbsgrænser' to see your status.", nil);
        }
        else if (error.code == MobilePayErrorCodeMerchantTimeout) {
            title = NSLocalizedString(@"MobilePay timeout ", nil);
            message = errorMessage;
        }
        else {
            // Show generic message
            title = NSLocalizedString(@"MobilePay Error", nil);
            message = [NSString stringWithFormat:@"%@%li", NSLocalizedString(@"Please try again later. ErrorCode: ", nil), (long)error.code];
        }
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title
                                                            message:message
                                                           delegate:self
                                                  cancelButtonTitle:NSLocalizedString(@"OK", nil)
                                                  otherButtonTitles:nil];
        alertView.tag = MobilePayErrorAlertViewTag;
        [alertView show];
        
        
    } cancel:^(MobilePayCancelledPayment * _Nullable mobilePayCancelledPayment) {
        NSLog(@"MobilePay purchase with order id '%@' cancelled by user", mobilePayCancelledPayment.orderId);
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"MobilePay Canceled", nil)
                                                            message:NSLocalizedString(@"You cancelled the payment flow from MobilePay. Your order has been cancelled.", nil)
                                                           delegate:self
                                                  cancelButtonTitle:NSLocalizedString(@"OK", nil)
                                                  otherButtonTitles:nil];
        alertView.tag = MobilePayCancelledAlertViewTag;
        [alertView show];
        [self cancelOrder:mobilePayCancelledPayment.orderId];

    }];
}

- (void)handleMobilePayPaymentWithUrlEmbedded:(NSURL *)url {
    [[MobilePayManager sharedInstance]handleMobilePayPaymentWithUrl:url success:^(MobilePaySuccessfulPayment * _Nullable mobilePaySuccessfulPayment) {
        NSString *orderId = mobilePaySuccessfulPayment.orderId;
        NSString *transactionId = mobilePaySuccessfulPayment.transactionId;
        NSString *amountWithdrawnFromCard = [NSString stringWithFormat:@"%f",mobilePaySuccessfulPayment.amountWithdrawnFromCard];
        NSLog(@"MobilePay purchase succeeded: order id '%@' and MobilePay transaction id '%@' and the amount: '%@'", orderId, transactionId,amountWithdrawnFromCard);
        
        ZEEmbeddedViewController *embeddedViewController = (ZEEmbeddedViewController *)self.window.rootViewController.childViewControllers[0];
        [embeddedViewController paymentSuccess];
        
    } error:^(NSError * _Nonnull error) {
        NSDictionary *dict = error.userInfo;
        NSString *errorMessage = [dict valueForKey:NSLocalizedFailureReasonErrorKey];
        NSLog(@"MobilePay purchase failed:  Error code '%li' and message '%@'",(long)error.code,errorMessage);
        
        NSString* title = @"";
        NSString* message = @"";
        if (error.code == MobilePayErrorCodeUpdateApp) {
            title = NSLocalizedString(@"You must update your MobilePay app ", nil);
            message = errorMessage;
        }
        else if (error.code == MobilePayErrorCodeTimeOut) {
            title = NSLocalizedString(@"MobilePay timeout ", nil);
            message = errorMessage;
        }
        else if (error.code == MobilePayErrorCodeLimitsExceeded) {
            title = NSLocalizedString(@"MobilePay amount limits exceeded ", nil);
            message = NSLocalizedString(@"Open MobilePay 'Beløbsgrænser' to see your status.", nil);
        }
        else if (error.code == MobilePayErrorCodeMerchantTimeout) {
            title = NSLocalizedString(@"MobilePay timeout ", nil);
            message = errorMessage;
        }
        else {
            // Show generic message
            title = NSLocalizedString(@"MobilePay Error", nil);
            message = [NSString stringWithFormat:@"%@%li", NSLocalizedString(@"Please try again later. ErrorCode: ", nil), (long)error.code];
        }
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title
                                                            message:message
                                                           delegate:self
                                                  cancelButtonTitle:NSLocalizedString(@"OK", nil)
                                                  otherButtonTitles:nil];
        alertView.tag = MobilePayErrorAlertViewTag;
        [alertView show];
        
        
    } cancel:^(MobilePayCancelledPayment * _Nullable mobilePayCancelledPayment) {
        NSLog(@"MobilePay purchase with order id '%@' cancelled by user", mobilePayCancelledPayment.orderId);
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"MobilePay Canceled", nil)
                                                            message:NSLocalizedString(@"You cancelled the payment flow from MobilePay. Your order has been cancelled.", nil)
                                                           delegate:self
                                                  cancelButtonTitle:NSLocalizedString(@"OK", nil)
                                                  otherButtonTitles:nil];
        alertView.tag = MobilePayCancelledAlertViewTag;
        [alertView show];
                
        ZEEmbeddedViewController *embeddedViewController = (ZEEmbeddedViewController *)self.window.rootViewController.childViewControllers[0];
        [embeddedViewController cancelOrder];
    }];
}

- (void) cancelOrder:(NSString*) orderId
{
    [SVProgressHUD showWithStatus:NSLocalizedString(@"Loading", nil)];
    ZEOrderStatusChangeRequest *newStatus = [ZEOrderStatusChangeRequest alloc];
    newStatus.status = @"cancelled";
    RKObjectManager *objectManager = [RKObjectManager sharedManager];
    [objectManager postObject:newStatus path:[NSString stringWithFormat:@"orders/%@", orderId] parameters:nil success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
        // Refresh updated order
        //self.order = [mappingResult array][0];
        //[self refreshView];
        ZETabBarController *tabBarController = (ZETabBarController *)self.window.rootViewController;
        [tabBarController viewOrderById:orderId];
        
        [SVProgressHUD dismiss];
        //[self.navigationController popViewControllerAnimated:YES];
    } failure:^(RKObjectRequestOperation *operation, NSError *error) {
        // Server error
        [SVProgressHUD showErrorWithStatus:error.localizedDescription];
    }];
}
/*
 * --------------------------------------------------------------------------------------------------------------
 *  MOBILEPAY END
 * --------------------------------------------------------------------------------------------------------------
 */

@end
