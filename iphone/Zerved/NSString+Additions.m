//
//  NSString+Additions.m
//  Zerved
//
//  Created by Anders Rasmussen on 20/11/13.
//
//

#import "NSString+Additions.h"

@implementation NSString (Additions)

- (CGSize)sizeWithFont:(UIFont *)font constrainedTo:(CGSize)size
{
    NSMutableDictionary *atts = [[NSMutableDictionary alloc] init];
    atts[NSFontAttributeName] = font;
    CGRect rect = [self boundingRectWithSize:size options:NSStringDrawingUsesLineFragmentOrigin attributes:atts context:nil];
    return rect.size;
}

@end
