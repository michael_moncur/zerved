//
//  ZERestManager.m
//  Zerved
//
//  Created by Anders Rasmussen on 24/04/13.
//
//  This class sets up all the objects for RESTkit used for communication with the
//  Zerved API.
//

#import "ZERestManager.h"
#import <RestKit/RestKit.h>
#import "ZELocation.h"
#import "ZEMerchant.h"
#import "ZEBar.h"
#import "ZEFavourite.h"
#import "ZECategory.h"
#import "ZEConsumer.h"
#import "ZEPaymentSubscription.h"
#import "ZECart.h"
#import "ZECartItem.h"
#import "ZECartItemOptionValue.h"
#import "ZEQuote.h"
#import "ZEQuoteItem.h"
#import "ZEQuoteFee.h"
#import "ZEProduct.h"
#import "ZEProductOption.h"
#import "ZEProductOptionValue.h"
#import "ZEPlaceOrderResult.h"
#import "ZECountry.h"
#import "ZEMenu.h"
#import "ZEEvent.h"
#import "ZETextSerialization.h"

#import "Order.h"
#import "OrderItem.h"
#import "OrderItemOption.h"
#import "OrderItemOptionValue.h"
#import "TaxTotal.h"

#import "ZEOrderStatusChangeRequest.h"
#import "ZEOrderSpecialCommandRequest.h"

@implementation ZERestManager

+ (void)initRest
{
    // Initialize HTTPClient
    NSURL *baseURL = [NSURL URLWithString:API_URL];
    AFHTTPClient* client = [[AFHTTPClient alloc] initWithBaseURL:baseURL];
    //we want to work with JSON-Data
    [client setDefaultHeader:@"Accept" value:RKMIMETypeJSON];
    // Accept text data for error messages
    [RKMIMETypeSerialization registerClass:[ZETextSerialization class] forMIMEType:@"text/html"];
    [RKMIMETypeSerialization registerClass:[ZETextSerialization class] forMIMEType:@"text/plain"];
    // Enable Activity Indicator Spinner
    [AFNetworkActivityIndicatorManager sharedManager].enabled = YES;
    
    // Date format
    NSDateFormatter *dateFormatter = [NSDateFormatter new];
    [dateFormatter  setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss'.'SSSSSS"];
    dateFormatter.timeZone = [NSTimeZone timeZoneWithAbbreviation:@"UTC"];
    dateFormatter.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
	RKCompoundValueTransformer *compoundValueTransformer = [RKValueTransformer defaultValueTransformer];
	[compoundValueTransformer addValueTransformer:dateFormatter];
	[RKValueTransformer setDefaultValueTransformer:compoundValueTransformer];

    // Initialize RestKit
    RKObjectManager *objectManager = [[RKObjectManager alloc] initWithHTTPClient:client];
    objectManager.requestSerializationMIMEType = RKMIMETypeJSON;

    
    // Mappings between JSON and NSObject classes

    // Favourite
    RKObjectMapping* favouriteMapping = [RKObjectMapping mappingForClass:[ZEFavourite class]];
    [favouriteMapping addAttributeMappingsFromDictionary:@{
     @"id": @"favouriteId",
     @"key": @"key",
     }];
    // Favourite options
    RKObjectMapping* favouriteOptionMapping = [RKObjectMapping mappingForClass:[ZECartItemOptionValue class]];
    [favouriteOptionMapping addAttributeMappingsFromDictionary:@{
     @"option_value": @"selectedIndex",
     @"option_id": @"optionId",
     }];
    // ----
    
    RKObjectMapping* countryMapping = [RKObjectMapping mappingForClass:[ZECountry class]];
    [countryMapping addAttributeMappingsFromDictionary:@{
     @"country_code": @"countryCode",
     @"name": @"name"
     }];
    
    RKObjectMapping* locationMapping = [RKObjectMapping mappingForClass:[ZELocation class]];
    [locationMapping addAttributeMappingsFromDictionary:@{
     @"key": @"key",
     @"id": @"locationId",
     @"name": @"name",
     @"latitude": @"latitude",
     @"longitude": @"longitude",
	 @"country": @"country"
     }];
    
    RKObjectMapping *menuMapping = [RKObjectMapping mappingForClass:[ZEMenu class]];
    [menuMapping addAttributeMappingsFromDictionary:@{
     @"key": @"key",
     @"id": @"menuId",
     @"name": @"name",
     }];
    
    RKObjectMapping* locationCompleteMapping = [RKObjectMapping mappingForClass:[ZELocation class]];
    [locationCompleteMapping addAttributeMappingsFromDictionary:@{
     @"key": @"key",
     @"id": @"locationId",
     @"name": @"name",
     @"latitude": @"latitude",
     @"longitude": @"longitude",
	 @"country": @"country",
     @"cover_urls": @"coverUrls",
     @"info_panel_enabled": @"infoPanelEnabled",
     @"address": @"address",
     @"website_url": @"websiteUrl",
     @"phone": @"phone",
     @"phone2": @"phone2",
     @"facebook": @"facebook",
     @"twitter": @"twitter",
     @"google_plus": @"googlePlus",
     @"linked_in": @"linkedIn",
     @"instagram": @"instagram",
     @"pinterest": @"pinterest",
     @"flickr": @"flickr",
     @"opening_hours_monday": @"openingHoursMonday",
     @"opening_hours_tuesday": @"openingHoursTuesday",
     @"opening_hours_wednesday": @"openingHoursWednesday",
     @"opening_hours_thursday": @"openingHoursThursday",
     @"opening_hours_friday": @"openingHoursFriday",
     @"opening_hours_saturday": @"openingHoursSaturday",
     @"opening_hours_sunday": @"openingHoursSunday",
     }];
    
    RKObjectMapping* merchantMapping = [RKObjectMapping mappingForClass:[ZEMerchant class]];
    [merchantMapping addAttributeMappingsFromDictionary:@{
     @"key": @"key",
     @"image_url": @"imageUrl"
     }];
    
    RKObjectMapping* eventMapping = [RKObjectMapping mappingForClass:[ZEEvent class]];
    [eventMapping addAttributeMappingsFromDictionary:@{
      @"key": @"key",
      @"name": @"name",
      @"time_label": @"timeLabel",
      @"date_start_formatted": @"dateStartFormatted",
      @"date_delivery_formatted": @"dateDeliveryFormatted",
      @"date_start_time_formatted": @"dateStartTimeFormatted",
      @"date_delivery_time_formatted": @"dateDeliveryTimeFormatted",
      @"date_start_date_formatted": @"dateStartDateFormatted",
      @"date_delivery_date_formatted": @"dateDeliveryDateFormatted",
      @"open_for_orders": @"openForOrders"
      }];
    
    RKObjectMapping* barMapping = [RKObjectMapping mappingForClass:[ZEBar class]];
    [barMapping addAttributeMappingsFromDictionary:@{
     @"key": @"key",
     @"id": @"barId",
     @"name": @"name",
     @"table_service": @"tableService",
     @"counter_service_to_go": @"counterServiceToGo",
     @"counter_service_to_stay": @"counterServiceToStay",
     @"delivery_service": @"deliveryService",
     @"estimated_waiting_time": @"estimatedWaitingTime",
     @"estimated_waiting_time_delivery": @"estimatedWaitingTimeDelivery",
     @"open": @"open",
     @"delivery_min_order_amount": @"deliveryMinOrderAmount",
     @"delivery_min_order_amount_formatted": @"deliveryMinOrderAmountFormatted"
     }];
    
    RKObjectMapping* categoryMapping = [RKObjectMapping mappingForClass:[ZECategory class]];
    [categoryMapping addAttributeMappingsFromDictionary:@{
     @"key": @"key",
     @"id": @"categoryId",
     @"name": @"name"
     }];
    
    RKObjectMapping* consumerMapping = [RKObjectMapping mappingForClass:[ZEConsumer class]];
    [consumerMapping addAttributeMappingsFromDictionary:@{
     @"key": @"key",
     @"email": @"email",
     @"name": @"name",
     @"phone": @"phone",
     @"address1": @"address1",
     @"address2": @"address2",
     @"postcode": @"postcode",
     @"city": @"city"
     }];
    
    RKObjectMapping* paymentSubscriptionMapping = [RKObjectMapping mappingForClass:[ZEPaymentSubscription class]];
    [paymentSubscriptionMapping addAttributeMappingsFromDictionary:@{
     @"key": @"key",
     @"description": @"description"
     }];
    
    RKObjectMapping *paymentSubscriptionRequestMapping = [RKObjectMapping requestMapping];
    paymentSubscriptionRequestMapping.assignsDefaultValueForMissingAttributes = NO;
    [paymentSubscriptionRequestMapping addAttributeMappingsFromArray:@[@"key", @"description"]];
    
    RKObjectMapping *cartMapping = [RKObjectMapping requestMapping];
    cartMapping.assignsDefaultValueForMissingAttributes = NO;
    [cartMapping addAttributeMappingsFromDictionary:@{
     @"locationKey": @"location",
     @"barKey": @"bar",
     @"group": @"group",
     @"service": @"service",
     @"tableNumber": @"table_number",
     @"toGo": @"to_go",
     @"termsAccepted": @"terms_accepted",
     @"tipPercent": @"tip_percent",
     @"eventKey": @"event",
     @"deliveryName": @"delivery_name",
     @"deliveryAddress1": @"delivery_address1",
     @"deliveryAddress2": @"delivery_address2",
     @"deliveryPostcode": @"delivery_postcode",
     @"deliveryCity": @"delivery_city",
     @"deliveryPhone": @"delivery_phone",
     @"deliveryInstructions": @"delivery_instructions",
     @"loyaltyCode": @"loyalty_code"
     }];
    
    [cartMapping addAttributeMappingsFromDictionary:@{
     @"card": @"card",
     @"pincode": @"pincode",
     @"obscuredCardNumber": @"obscured_card_number",
     @"nordpayRegistration": @"nordpay_registration",
     @"nordpayAuthorization": @"nordpay_authorization",
     @"nordpayMethod": @"nordpay_method"
     }];
    
    
    RKObjectMapping *cartItemMapping = [RKObjectMapping requestMapping];
    cartItemMapping.assignsDefaultValueForMissingAttributes = NO;
    [cartItemMapping addAttributeMappingsFromDictionary:@{
     @"productKey": @"product",
     @"quantity": @"qty"
     }];
    
    RKObjectMapping *cartItemOptionMapping = [RKObjectMapping requestMapping];
    cartItemOptionMapping.assignsDefaultValueForMissingAttributes = NO;
    [cartItemOptionMapping addAttributeMappingsFromDictionary:@{
     @"optionId": @"option_id",
     @"selectedIndex": @"option_value"
     }];
    
    RKObjectMapping *quoteMapping = [RKObjectMapping mappingForClass:[ZEQuote class]];
    [quoteMapping addAttributeMappingsFromDictionary:@{
     @"total": @"total",
     @"total_formatted": @"totalFormatted",
     @"currency": @"currency",
     @"tip": @"tip",
     @"tip_formatted": @"tipFormatted",
     @"subtotal": @"subtotal",
     @"subtotal_formatted": @"subtotalFormatted",
     @"total_tax": @"totalTax",
     @"total_tax_formatted": @"totalTaxFormatted",
     @"total_incl_tax": @"totalInclTax",
     @"total_incl_tax_formatted": @"totalInclTaxFormatted",
	 @"total_loyalty_discount_incl_tax": @"totalLoyaltyDiscountInclTax",
	 @"total_loyalty_discount_incl_tax_formatted": @"totalLoyaltyDiscountInclTaxFormatted",
     @"tip_enabled": @"tipEnabled",
     @"has_loyalty_program": @"hasLoyaltyProgram",
     @"loyalty_code": @"loyaltyCode",
	 @"loyalty_code_valid": @"loyaltyCodeValid",
	 @"loyalty_program_name": @"loyaltyName",
     @"custom_allowed_payment_methods": @"customAllowedPaymentMethods"
     }];
    
    RKObjectMapping *quoteItemMapping = [RKObjectMapping mappingForClass:[ZEQuoteItem class]];
    [quoteItemMapping addAttributeMappingsFromDictionary:@{
     @"qty": @"qty",
     @"price": @"price",
     @"name": @"name",
     @"price_formatted": @"priceFormatted",
     @"valid": @"valid",
     @"options_summary": @"optionsSummary",
     @"product_has_options": @"productHasOptions"
     }];
    
    RKObjectMapping *quoteFeeMapping = [RKObjectMapping mappingForClass:[ZEQuoteFee class]];
    [quoteFeeMapping addAttributeMappingsFromDictionary:@{
     @"name": @"name",
     @"price": @"price",
     @"price_formatted": @"priceFormatted"}];
    
    RKObjectMapping *productMapping = [RKObjectMapping mappingForClass:[ZEProduct class]];
    [productMapping addAttributeMappingsFromDictionary:@{
     @"key": @"key",
     @"name": @"name",
     @"final_price_formatted": @"finalPriceFormatted",
     @"description": @"description",
     @"image_url": @"imageUrl",
     @"tiered_price_descriptions": @"tieredPriceDescriptions",
     @"message_when_closed": @"messageWhenClosed",
     @"merchant_open": @"merchantOpen",
     @"menu_active": @"menuActive"
     }];
    
    RKObjectMapping *productOptionMapping = [RKObjectMapping mappingForClass:[ZEProductOption class]];
    [productOptionMapping addAttributeMappingsFromDictionary:@{
     @"key": @"key",
     @"id": @"optionId",
     @"name": @"name",
     @"selection_type": @"selectionType",
     @"required": @"required"
     }];
    
    RKObjectMapping *productOptionValueMapping = [RKObjectMapping mappingForClass:[ZEProductOptionValue class]];
    [productOptionValueMapping addAttributeMappingsFromDictionary:@{
     @"index": @"index",
     @"label": @"label",
     @"price_label": @"priceLabel"
     }];
    
    RKObjectMapping *placeOrderMapping = [RKObjectMapping mappingForClass:[ZEPlaceOrderResult class]];
    [placeOrderMapping addAttributeMappingsFromDictionary:@{
     @"order_key": @"orderKey",
     @"order_id": @"orderId",
     @"payment_needed": @"paymentNeeded"
     }];
    
    // Order mapping (with offline caching)
    RKObjectMapping *orderMapping = [RKObjectMapping mappingForClass:[Order class]];
    [orderMapping addAttributeMappingsFromDictionary:@{
     @"key": @"key",
     @"id": @"orderId",
     @"status": @"status",
     @"status_label": @"statusLabel",
     @"order_number": @"orderNumber",
     @"queue_number": @"queueNumber",
     @"date_created": @"dateCreated",
     @"date_created_formatted": @"dateCreatedFormatted",
     @"location_name": @"locationName",
     @"merchant.image_url": @"merchantImageUrl",
     @"status_title_for_consumer": @"statusTitleForConsumer",
     @"status_description_for_consumer": @"statusDescriptionForConsumer",
     @"estimated_waiting_time": @"estimatedWaitingTime",
     @"total_excl_tax_formatted": @"totalExclTaxFormatted",
     @"total_incl_tax_formatted": @"totalInclTaxFormatted",
     @"total_tax_formatted": @"totalTaxFormatted",
	 @"total_loyalty_discount_incl_tax": @"totalLoyaltyDiscountInclTax",
	 @"total_loyalty_discount_incl_tax_formatted": @"totalLoyaltyDiscountInclTaxFormatted",
     @"comment": @"comment",
     @"service": @"service",
     @"merchant.serve_with_consumer_device": @"serveWithConsumerDevice",
     @"consumer_has_email": @"consumerHasEmail",
     @"merchant.entrance_pincode": @"pincode",
     @"company_name": @"companyName",
     @"company_vat": @"companyVat",
     @"date_delivery_formatted": @"dateDeliveryFormatted",
     @"event_name": @"eventName",
     @"event_time_label": @"eventTimeLabel"
     }];

    // Order tax totals mapping
    RKObjectMapping *orderTaxTotalsMapping = [RKObjectMapping mappingForClass:[TaxTotal class]];
    [orderTaxTotalsMapping addAttributeMappingsFromDictionary:@{@"name": @"name",
                                                                @"rate": @"rate",
                                                                @"total_formatted": @"totalFormatted"}];

    // Order items mapping (with offline caching)
    RKObjectMapping *orderItemMapping = [RKObjectMapping mappingForClass:[OrderItem class]];
    [orderItemMapping addAttributeMappingsFromDictionary:@{
     @"key": @"key",
     @"name": @"productName",
     @"quantity": @"quantity",
     @"total_excl_tax_formatted": @"totalExclTaxFormatted",
     @"total_incl_tax_formatted": @"totalInclTaxFormatted",
     @"total_tax_formatted": @"totalTaxFormatted",
	 @"total_before_loyalty_discount_incl_tax_formatted": @"totalBeforeLoyaltyDiscountInclTaxFormatted"
     }];

    RKObjectMapping *orderItemOptionMapping = [RKObjectMapping mappingForClass:[OrderItemOption class]];
    [orderItemOptionMapping addAttributeMappingsFromDictionary:@{
     @"option_name": @"optionName"
     }];

    RKObjectMapping *orderItemOptionValueMapping = [RKObjectMapping mappingForClass:[OrderItemOptionValue class]];
    [orderItemOptionValueMapping addAttributeMappingsFromDictionary:@{
     @"value_label": @"valueLabel"
     }];

    RKObjectMapping *orderStatusChangeMapping = [RKObjectMapping requestMapping];
    orderStatusChangeMapping.assignsDefaultValueForMissingAttributes = NO;
    [orderStatusChangeMapping addAttributeMappingsFromDictionary:@{
                                                       @"status": @"status",
                                                       @"entrancePincode": @"entrance_pincode",
                                                       }];

    RKObjectMapping *orderSpecialCommandMapping = [RKObjectMapping requestMapping];
    orderSpecialCommandMapping.assignsDefaultValueForMissingAttributes = NO;
    [orderSpecialCommandMapping addAttributeMappingsFromDictionary:@{
                                                                   @"special_command": @"special_command",
                                                                   }];

    // Define object relationships
    
    [orderMapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"total_tax_grouped" toKeyPath:@"groupedTaxTotals" withMapping:orderTaxTotalsMapping]];
    [orderMapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"items" toKeyPath:@"items" withMapping:orderItemMapping]];
    [orderItemMapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"option_values" toKeyPath:@"optionValues" withMapping:orderItemOptionMapping]];

    [orderItemOptionMapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"values" toKeyPath:@"values" withMapping:orderItemOptionValueMapping]];
    
    [favouriteMapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"location" toKeyPath:@"location" withMapping:locationMapping]];
    [favouriteMapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"bar" toKeyPath:@"bar" withMapping:barMapping]];
    [favouriteMapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"product" toKeyPath:@"product" withMapping:productMapping]];
    [favouriteMapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"options" toKeyPath:@"options" withMapping:favouriteOptionMapping]];
    
    [eventMapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"bars" toKeyPath:@"bars" withMapping:barMapping]];
    [locationMapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"merchant" toKeyPath:@"merchant" withMapping:merchantMapping]];
    [locationCompleteMapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"merchant" toKeyPath:@"merchant" withMapping:merchantMapping]];
    [locationCompleteMapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"entrance_categories" toKeyPath:@"entranceCategories" withMapping:categoryMapping]];
    [locationCompleteMapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"bars" toKeyPath:@"bars" withMapping:barMapping]];
    [locationCompleteMapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"upcoming_events" toKeyPath:@"upcomingEvents" withMapping:eventMapping]];
    [barMapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"menus" toKeyPath:@"menus" withMapping:menuMapping]];
    [menuMapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"categories" toKeyPath:@"categories" withMapping:categoryMapping]];
    
    [consumerMapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"current_payment_subscription" toKeyPath:@"currentPaymentSubscription" withMapping:paymentSubscriptionMapping]];
    [consumerMapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"payment_subscriptions" toKeyPath:@"paymentSubscriptions" withMapping:paymentSubscriptionMapping]];
    
    RKObjectMapping *consumerRequestMapping = [consumerMapping inverseMapping];
    consumerRequestMapping.assignsDefaultValueForMissingAttributes = NO;
    
    [cartMapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"items" toKeyPath:@"items" withMapping:cartItemMapping]];
    [cartItemMapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"options" toKeyPath:@"options" withMapping:cartItemOptionMapping]];
    [quoteMapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"items" toKeyPath:@"items" withMapping:quoteItemMapping]];
    [quoteMapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"location" toKeyPath:@"location" withMapping:locationCompleteMapping]];
    [quoteMapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"bar" toKeyPath:@"bar" withMapping:barMapping]];
    [quoteMapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"fees" toKeyPath:@"fees" withMapping:quoteFeeMapping]];
    
    [productMapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"product_options" toKeyPath:@"productOptions" withMapping:productOptionMapping]];
    [productOptionMapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"values" toKeyPath:@"values" withMapping:productOptionValueMapping]];
    
    // Response descriptors - map a response for a URL to the object mappings defined above
	RKResponseDescriptor *countryDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:countryMapping method:RKRequestMethodAny pathPattern:@"countries" keyPath:nil statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    [objectManager addResponseDescriptor:countryDescriptor];
    RKResponseDescriptor *locationDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:locationMapping method:RKRequestMethodAny pathPattern:@"locations/nearby/:latitude/:longitude" keyPath:nil statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    [objectManager addResponseDescriptor:locationDescriptor];
    RKResponseDescriptor *locationAlphabeticDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:locationMapping method:RKRequestMethodAny pathPattern:@"locations/country/:country" keyPath:nil statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    [objectManager addResponseDescriptor:locationAlphabeticDescriptor];
    RKResponseDescriptor *merchantLocationsDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:locationMapping method:RKRequestMethodAny pathPattern:@"merchants/:merchantId/locations" keyPath:nil statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    [objectManager addResponseDescriptor:merchantLocationsDescriptor];
    RKResponseDescriptor *locationCompleteDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:locationCompleteMapping method:RKRequestMethodAny pathPattern:@"locations/:key" keyPath:nil statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    [objectManager addResponseDescriptor:locationCompleteDescriptor];
    RKResponseDescriptor *paymentsubscriptionsCompleteDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:consumerMapping method:RKRequestMethodAny pathPattern:@"paymentsubscriptions/:key" keyPath:nil statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    [objectManager addResponseDescriptor:paymentsubscriptionsCompleteDescriptor];
    RKResponseDescriptor *locationCompleteWithAllBarsDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:locationCompleteMapping method:RKRequestMethodAny pathPattern:@"locations/withallbars/:key" keyPath:nil statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    [objectManager addResponseDescriptor:locationCompleteWithAllBarsDescriptor];
    RKResponseDescriptor *allLocationsDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:locationMapping method:RKRequestMethodAny pathPattern:@"locations" keyPath:nil statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    [objectManager addResponseDescriptor:allLocationsDescriptor];
    RKResponseDescriptor *locationCategoriesDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:categoryMapping method:RKRequestMethodAny pathPattern:@"locations/:key/categories" keyPath:nil statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    [objectManager addResponseDescriptor:locationCategoriesDescriptor];
    RKResponseDescriptor *barCategoriesDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:categoryMapping method:RKRequestMethodAny pathPattern:@"bars/:key/categories" keyPath:nil statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    [objectManager addResponseDescriptor:barCategoriesDescriptor];
    RKResponseDescriptor *barMenusDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:menuMapping method:RKRequestMethodAny pathPattern:@"bars/:key/menus" keyPath:nil statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    [objectManager addResponseDescriptor:barMenusDescriptor];
    RKResponseDescriptor *barMenusCategoriesDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:menuMapping method:RKRequestMethodAny pathPattern:@"bars/:key/menus/:key/categories" keyPath:nil statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    [objectManager addResponseDescriptor:barMenusCategoriesDescriptor];
    RKResponseDescriptor *eventListDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:eventMapping method:RKRequestMethodAny pathPattern:@"locations/:key/events/:year/:month/:day" keyPath:nil statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    [objectManager addResponseDescriptor:eventListDescriptor];
    RKResponseDescriptor *eventDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:eventMapping method:RKRequestMethodAny pathPattern:@"events/:key" keyPath:nil statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    [objectManager addResponseDescriptor:eventDescriptor];
    RKResponseDescriptor *consumerDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:consumerMapping method:RKRequestMethodAny pathPattern:@"consumers/:key" keyPath:nil statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    [objectManager addResponseDescriptor:consumerDescriptor];
    RKResponseDescriptor *quoteDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:quoteMapping method:RKRequestMethodAny pathPattern:@"quote" keyPath:nil statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    [objectManager addResponseDescriptor:quoteDescriptor];
    RKResponseDescriptor *productDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:productMapping method:RKRequestMethodAny pathPattern:@"products/:key" keyPath:nil statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    [objectManager addResponseDescriptor:productDescriptor];
    RKResponseDescriptor *locationProductsDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:productMapping method:RKRequestMethodAny pathPattern:@"locations/:locationKey/categories/:categoryKey/products" keyPath:nil statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    [objectManager addResponseDescriptor:locationProductsDescriptor];
    RKResponseDescriptor *barProductsDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:productMapping method:RKRequestMethodAny pathPattern:@"bars/:barKey/menus/:menuKey/categories/:categoryKey/products" keyPath:nil statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    [objectManager addResponseDescriptor:barProductsDescriptor];
    
    RKResponseDescriptor *placeOrderDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:placeOrderMapping method:RKRequestMethodAny pathPattern:@"placeorder" keyPath:nil statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    [objectManager addResponseDescriptor:placeOrderDescriptor];
    
    RKResponseDescriptor *orderListDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:orderMapping method:RKRequestMethodAny pathPattern:@"consumers/:consumerKey/orders/open,closed" keyPath:nil statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    [objectManager addResponseDescriptor:orderListDescriptor];
    RKResponseDescriptor *orderDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:orderMapping method:RKRequestMethodAny pathPattern:@"orders/:orderKey" keyPath:nil statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    [objectManager addResponseDescriptor:orderDescriptor];

    RKResponseDescriptor *favouritesDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:favouriteMapping method:RKRequestMethodAny pathPattern:@"consumers/:consumerKey/favourites" keyPath:nil statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    [objectManager addResponseDescriptor:favouritesDescriptor];
    
    // Request descriptors - map objects to HTTP request
    RKRequestDescriptor *paymentSubscriptionRequestDescriptor = [RKRequestDescriptor requestDescriptorWithMapping:paymentSubscriptionRequestMapping objectClass:[ZEPaymentSubscription class] rootKeyPath:@"paymentsubscriptions" method:RKRequestMethodAny];
    [objectManager addRequestDescriptor:paymentSubscriptionRequestDescriptor];
    RKRequestDescriptor *consumerRequestDescriptor = [RKRequestDescriptor requestDescriptorWithMapping:consumerRequestMapping objectClass:[ZEConsumer class] rootKeyPath:nil method:RKRequestMethodAny];
    [objectManager addRequestDescriptor:consumerRequestDescriptor];
    RKRequestDescriptor *cartRequestDescriptor = [RKRequestDescriptor requestDescriptorWithMapping:cartMapping objectClass:[ZECart class] rootKeyPath:nil method:RKRequestMethodAny];
    [objectManager addRequestDescriptor:cartRequestDescriptor];
    RKRequestDescriptor *orderStatusChangeDescriptor = [RKRequestDescriptor requestDescriptorWithMapping:orderStatusChangeMapping objectClass:[ZEOrderStatusChangeRequest class] rootKeyPath:nil method:RKRequestMethodAny];
    [objectManager addRequestDescriptor:orderStatusChangeDescriptor];
    RKRequestDescriptor *orderSpecialCommandDescriptor = [RKRequestDescriptor requestDescriptorWithMapping:orderSpecialCommandMapping objectClass:[ZEOrderSpecialCommandRequest class] rootKeyPath:nil method:RKRequestMethodAny];
    [objectManager addRequestDescriptor:orderSpecialCommandDescriptor];

    
    // Error response descriptors - catch HTTP 4XX and 5XX responses
    RKObjectMapping *errorMapping = [RKObjectMapping mappingForClass:[RKErrorMessage class]];
    [errorMapping addAttributeMappingsFromDictionary:@{@"data": @"errorMessage"}];
    RKResponseDescriptor *errorDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:errorMapping method:RKRequestMethodAny pathPattern:nil keyPath:nil statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassClientError)];
    [objectManager addResponseDescriptor:errorDescriptor];
    RKResponseDescriptor *serverErrorDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:errorMapping method:RKRequestMethodAny pathPattern:nil keyPath:nil statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassServerError)];
    [objectManager addResponseDescriptor:serverErrorDescriptor];
    
        
    // Log all HTTP traffic with request and response bodies
    //RKLogConfigureByName("RestKit/Network", RKLogLevelTrace);
    
    // Log debugging info about Core Data
    //RKLogConfigureByName("RestKit/CoreData", RKLogLevelDebug);
}

@end
