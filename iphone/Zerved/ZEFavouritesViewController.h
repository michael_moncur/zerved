//
//  ZEFavouritesViewController.h
//  Zerved
//
//  Created by Zerved Development Mac on 26/07/13.
//
//

#import <UIKit/UIKit.h>
#import "ZEConsumerDelegate.h"
#import "ZEFavouriteCell.h"


extern const char MyConstantKey;
@interface ZEFavouritesViewController : UITableViewController <ZEConsumerDelegate, UIActionSheetDelegate>

@property(nonatomic, retain) NSMutableArray *favourites;
@property(nonatomic, retain) NSMutableArray * barsAndLocations;
-(IBAction)refresh:(id)sender;
-(void)reload;
-(void)addToCart:(id)sender;
-(void)deleteFavourite:(ZEFavouriteCell *)sender;

@end
