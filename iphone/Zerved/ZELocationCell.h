//
//  ZELocationCell.h
//  Zerved
//
//  Created by Anders Rasmussen on 02/10/13.
//
//

#import <UIKit/UIKit.h>

@interface ZELocationCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *locationImage;

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *subtitleLabel;

@end
