//
//  ZEMenu.h
//  Zerved
//
//  Created by Zerved Development Mac on 28/08/13.
//
//

#import <Foundation/Foundation.h>

@interface ZEMenu : NSObject

@property (nonatomic, retain) NSString * key;
@property (nonatomic, retain) NSNumber * menuId;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSArray *barKeys;
@property (nonatomic, strong) NSArray *categories;

@end
