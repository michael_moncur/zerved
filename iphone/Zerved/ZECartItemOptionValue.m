//
//  ZECartItemOptionValue.m
//  Zerved
//
//  Created by Anders Rasmussen on 28/05/13.
//
//

#import "ZECartItemOptionValue.h"

@implementation ZECartItemOptionValue

- (id)initWithOptionId:(NSString *)optionId selectedIndex:(NSInteger)selectedIndex
{
    self = [super init];
    if (self) {
        //self.optionKey = nil;
        self.optionId = optionId;
        self.selectedIndex = selectedIndex;
    }
    return self;
}

- (BOOL)isEqual:(id)anObject
{
    if (anObject == self)
        return YES;
    if (!anObject || ![anObject isKindOfClass:[self class]])
        return NO;
    ZECartItemOptionValue *option = (ZECartItemOptionValue *)anObject;
    return ([self.optionId isEqualToString:option.optionId] && self.selectedIndex == option.selectedIndex);
}

- (id)jsonObject
{
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithObjectsAndKeys:self.optionId, @"option_id", [NSNumber numberWithInteger:self.selectedIndex], @"option_value", nil];
    return dict;
}

- (NSString *)description
{
    return [NSString stringWithFormat:@"(%@, %ld)", self.optionId, (long)self.selectedIndex];
}

@end
