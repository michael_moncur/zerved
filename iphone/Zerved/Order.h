//
//  Order.h
//  Zerved
//
//  Created by Anders Rasmussen on 05/07/13.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Order : NSObject

@property (nonatomic, strong) NSString * key;
@property (nonatomic, strong) NSNumber * orderId;
@property (nonatomic, strong) NSString * status;
@property (nonatomic, strong) NSNumber * orderNumber;
@property (nonatomic, strong) NSString * queueNumber;
@property (nonatomic, strong) NSDate * dateCreated;
@property (nonatomic, strong) NSString * dateCreatedFormatted;
@property (nonatomic, strong) NSString * locationName;
@property (nonatomic, strong) NSString * merchantImageUrl;
@property (nonatomic, strong) NSString * statusTitleForConsumer;
@property (nonatomic, strong) NSString * statusDescriptionForConsumer;
@property (nonatomic, strong) NSNumber * estimatedWaitingTime;
@property (nonatomic, strong) NSString * totalExclTaxFormatted;
@property (nonatomic, strong) NSString * totalInclTaxFormatted;
@property (nonatomic, strong) NSString * totalTaxFormatted;
@property (nonatomic, strong) NSNumber * totalLoyaltyDiscountInclTax;
@property (nonatomic, strong) NSString * totalLoyaltyDiscountInclTaxFormatted;
@property (nonatomic, strong) NSString * comment;
@property (nonatomic, strong) NSString * companyName;
@property (nonatomic, strong) NSString * companyVat;
@property (nonatomic, strong) NSString * statusLabel;
@property BOOL serveWithConsumerDevice;
@property BOOL consumerHasEmail;
@property (nonatomic, strong) NSString * service;
@property (nonatomic, strong) NSString * pincode;
@property (nonatomic, strong) NSArray * items;
@property (nonatomic, strong) NSArray * groupedTaxTotals;
@property (nonatomic, strong) NSString * state;
@property (nonatomic, strong) NSString * dateDeliveryFormatted;
@property (nonatomic, strong) NSString * eventName;
@property (nonatomic, strong) NSString * eventTimeLabel;

- (NSString *)serviceSummary;

@end
