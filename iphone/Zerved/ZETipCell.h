//
//  ZETipCell.h
//  Zerved
//
//  Created by Anders Rasmussen on 10/06/13.
//
//

#import <UIKit/UIKit.h>

@interface ZETipCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UISlider *tipSlider;
@property (weak, nonatomic) IBOutlet UILabel *tipPercentageLabel;
@property (weak, nonatomic) IBOutlet UIButton *priceButton;

@end
