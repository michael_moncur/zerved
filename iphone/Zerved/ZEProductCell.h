//
//  ZEProductCell.h
//  Zerved
//
//  Created by Anders Rasmussen on 07/08/13.
//
//

#import <UIKit/UIKit.h>
#import "ZEProduct.h"
#import "ZECart.h"
#import "ZEProductsViewController.h"

#define PRODUCT_NAME_WIDTH 164
#define PRODUCT_DESCRIPTION_WIDTH 255
#define PRODUCT_DESCRIPTION_HEIGHT 34

@interface ZEProductCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *tieredPrices;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) ZEProduct *product;
@property (weak, nonatomic) ZECart *cart;
@property (weak, nonatomic) IBOutlet UIButton *addToCartButton;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;
@property (weak, nonatomic) IBOutlet UIView *itemsView;
@property (strong, nonatomic) NSArray *items;

+ (CGFloat)heightForProduct:(ZEProduct *)product andCart:(ZECart *)cart;
+ (CGFloat)heightForItems:(NSArray *)items;

- (void)setProduct:(ZEProduct *)product andCart:(ZECart *)cart andProductController:(ZEProductsViewController *)productController andOpen:(BOOL)open;

@end
