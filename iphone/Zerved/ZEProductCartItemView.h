//
//  ZEProductCartItemView.h
//  Zerved
//
//  Created by Anders Rasmussen on 26/09/13.
//
//

#import <UIKit/UIKit.h>

@interface ZEProductCartItemView : UIView
@property (weak, nonatomic) IBOutlet UILabel *quantityLabel;
@property (weak, nonatomic) IBOutlet UIStepper *quantityStepper;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;

- (void)attachQuantityStepper;

@end
