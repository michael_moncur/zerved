//
//  ZEProductOptionToggleButton.h
//  Zerved
//
//  Created by Anders Rasmussen on 13/08/13.
//
//

#import <UIKit/UIKit.h>
#import "ZEProductOptionValue.h"

@interface ZEProductOptionToggleButton : UIButton

@property (nonatomic, weak) ZEProductOptionValue *productOptionValue;

@end
