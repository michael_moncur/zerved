//
//  ZEConsumerDelegate.h
//  Zerved
//
//  Created by Anders Rasmussen on 05/03/13.
//
//

#import <Foundation/Foundation.h>

@protocol ZEConsumerDelegate <NSObject>
@optional
//- (void)didCreateConsumer;
//- (void)didFailToCreateConsumerWithError:(NSString *)error;
//- (void)didLogin;
//- (void)didFailToLoginWithError:(NSString *)error;
//- (void)didResetPassword;
//- (void)didFailToResetPasswordWithError:(NSString *)error;
- (void)didAuthenticate;
- (void)didFailToAuthenticate;
@end