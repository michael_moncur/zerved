//
//  ZEProductOptionDropdownButton.m
//  Zerved
//
//  Created by Anders Rasmussen on 13/08/13.
//
//

#import "ZEProductOptionDropdownButton.h"
#import "ZEProductOptionValue.h"
#import "ActionSheetStringPicker.h"

@implementation ZEProductOptionDropdownButton

@synthesize productOption = _productOption;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setProductOption:(ZEProductOption *)productOption
{
    _productOption = productOption;
    
    UIImage *buttonImage = [UIImage imageNamed:@"dropdown.png"];
    [self setBackgroundImage:buttonImage forState:UIControlStateNormal];
    [self setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
    [self setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    [self setContentEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 25)];
    [self.titleLabel setFont:[UIFont systemFontOfSize:12]];
    [self.titleLabel setLineBreakMode:NSLineBreakByWordWrapping];
    // If required, preselect first option
    if (self.productOption.selectedIndex == -1 && [self.productOption.required boolValue]) {
        [self.productOption setSelectedIndex:0];
    }
    [self updateTitle];
    [self addTarget:self action:@selector(pickOption:) forControlEvents:UIControlEventTouchUpInside];
}

- (void)updateTitle
{
    if (self.productOption.selectedIndex == -1) {
        // No selection
        [self setTitle:[NSString stringWithFormat:NSLocalizedString(@"Select %@", nil), self.productOption.name] forState:UIControlStateNormal];
    } else {
        // Show selected label
        ZEProductOptionValue *value = (ZEProductOptionValue *)[self.productOption.values objectAtIndex:self.productOption.selectedIndex];
        [self setTitle:[NSString stringWithFormat:@"%@ %@", value.label, value.priceLabel] forState:UIControlStateNormal];
    }
}

- (void)pickOption:(UIButton *)sender
{
    NSMutableArray *labels = [[NSMutableArray alloc] initWithCapacity:[self.productOption.values count]];
    if (![self.productOption.required boolValue]) {
        // If not required, use an empty label to select nothing
        [labels addObject:@""];
    }
    for (ZEProductOptionValue *value in self.productOption.values) {
        [labels addObject:[NSString stringWithFormat:@"%@ %@", value.label, value.priceLabel]];
    }
    NSInteger initialSelection = [self.productOption selectedIndex];
    if (initialSelection == -1) {
        // If nothing selected, preselect the first value
        initialSelection = 0;
    } else if (![self.productOption.required boolValue]) {
        // If not required, there's an additional label at the beginning
        initialSelection += 1;
    }
    [ActionSheetStringPicker showPickerWithTitle:[NSString stringWithFormat:NSLocalizedString(@"Select %@", nil), self.productOption.name] rows:labels initialSelection:initialSelection doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
        if (![self.productOption.required boolValue]) {
            // If not required, there's an additional label at the beginning
            selectedIndex -= 1;
        }
        [self.productOption setSelectedIndex:selectedIndex];
        [self updateTitle];
    } cancelBlock:^(ActionSheetStringPicker *picker) {
        
    } origin:self];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
