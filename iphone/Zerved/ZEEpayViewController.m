//
//  ZEEpayViewController.m
//  Zerved
//
//  Created by Henrik Hoey Karlsen on 25/09/14.
//
//  Epay payment (using epay mobile sdk), used to pay for an order, or to add a credit card to a
//  consumer account. From this screen the app communicates directly with Epay
//  to save card or authorize payments. If the screen is launched from the shopping
//  cart it will also place the order after authorizing the payment.
//

#import "ZEEpayViewController.h"
#import "SVProgressHUD.h"
#import "ZEAppDelegate.h"
#import "ZEPlaceOrderResult.h"
#import "ePayLib.h"
#import "ePayParameter.h"
#import <CommonCrypto/CommonDigest.h>

NSString *const EPAY_PROVIDER = @"not used atm";

@interface ZEEpayViewController ()
{
    ePayLib* epaylib;
}
@end

@implementation ZEEpayViewController

@synthesize activityIndicatorView;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Listen to ePay Lib notifications
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(eventFromEpayLib:) name:PaymentAcceptedNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(eventFromEpayLib:) name:PaymentLoadingAcceptPageNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(eventFromEpayLib:) name:PaymentWindowCancelledNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(eventFromEpayLib:) name:PaymentWindowLoadedNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(eventFromEpayLib:) name:PaymentWindowLoadingNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(eventFromEpayLib:) name:ErrorOccurredNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(eventFromEpayLib:) name:NetworkActivityNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(eventFromEpayLib:) name:NetworkNoActivityNotification object:nil];
    
    // Toolbar with buttons to pop up with numpad
    numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 50)];
    numberToolbar.barStyle = UIBarStyleDefault;
    NSString *submitTitle = NSLocalizedString(@"Continue", @"ZEEpayViewController");
    self.submitButton = [[UIBarButtonItem alloc] initWithTitle:submitTitle style:UIBarButtonItemStyleBordered target:self action:@selector(submit)];
    [numberToolbar setTranslucent:NO];
    if (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1) {
        [numberToolbar setBarTintColor:[UIColor colorWithRed:72/255.0f green:217/255.0f blue:116/255.0f alpha:1.0]];
    }
    
    [self validateInput];
    numberToolbar.items = [NSArray arrayWithObjects:
                           //[[UIBarButtonItem alloc]initWithTitle:NSLocalizedString(@"Hide", nil) style:UIBarButtonItemStyleBordered target:self action:@selector(closeNumberPad)],
                           [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                           self.submitButton,
                           [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                           nil];
    [numberToolbar sizeToFit];
    
    self.pinField.inputAccessoryView = numberToolbar;
    
    self.paymentStartedAt = [NSDate date];
    
    self.tabIndex = self.tabBarController.selectedIndex;
    
    // Save card cell hidden if saving card.
    if (!self.shouldPlaceOrder) {
        self.saveCardCell.hidden = YES;
        
        // Set left/right margin for saveCardReserveAmountInfoLabel
        NSMutableParagraphStyle *style =  [[NSParagraphStyle defaultParagraphStyle] mutableCopy];
        style.firstLineHeadIndent = 15.0f;
        style.headIndent = 15.0f;
        style.tailIndent = -15.0f;
        
        NSAttributedString *attrText = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"saveCardReserveAmountInfoLabel", @"saveCardReserveAmountInfoLabel") attributes:@{ NSParagraphStyleAttributeName : style}];
        
        self.saveCardReserveAmountInfoLabel.attributedText = attrText;
        
        // The info label is currently disabled. Set hidden to NO to enable again.
        //self.saveCardReserveAmountInfoLabel.hidden = NO;
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [self.pinField becomeFirstResponder];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setSubmitButton:nil];
    [self setPinField:nil];
    [self setPinCell:nil];
    [self setSaveCardCell:nil];
    [self setSaveCardSwitch:nil];
    [self setTimer:nil];
    [super viewDidUnload];
}

- (void)viewWillAppear:(BOOL)animated
{
    // Check expiration every second
    self.timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(checkExpiration) userInfo:nil repeats:YES];
    [super viewWillAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [self.timer invalidate];
//    [numberToolbar removeFromSuperview];
    [super viewWillDisappear:animated];
}

- (void)checkExpiration
{
    // Close window if more than 5 min old
    if ([[NSDate date] timeIntervalSinceDate:self.paymentStartedAt] > 60*5.0) {
        [SVProgressHUD showErrorWithStatus:NSLocalizedString(@"Payment expired", @"Error msg when customer took too long to complete the payment")];
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Pin hidden when save card off
    if (indexPath.row == 5 && ![self.saveCardSwitch isOn]) {
        return 0;
    }
    return 44;
}

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Allow first row to be selected
    return (indexPath.row == 0) ? indexPath : nil;
}

- (IBAction)saveCardSwitched:(UISwitch *)sender {
    self.pinCell.hidden = ![sender isOn];
    
    [self validateInput];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString *finalString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    if ([finalString isEqualToString:@""]) {
        // Empty field after backspace
        textField.text = finalString;
        [self validateInput];
        return NO;
    }
    else if (textField == self.pinField) {
        if (finalString.length <= 4) {
            textField.text = finalString;
        }
    }
    [self validateInput];
    return NO;
}

- (void)closeNumberPad
{
    [self.view endEditing:YES];
}

- (void)validateInput
{
    BOOL valid = YES;
    if ([self.saveCardSwitch isOn] && self.pinField.text.length != 4) {
        valid = NO;
    }
    
    self.submitButton.enabled = valid;
    if (valid) {
        self.submitButton.tintColor = [UIColor whiteColor];//[UIColor colorWithRed:37/255.0f green:181/255.0f blue:0.0 alpha:1.0]; // green
    } else {
        self.submitButton.tintColor = [UIColor colorWithRed:104/255.0f green:104/255.0f blue:104/255.0f alpha:1.0]; // gray
    }
}

- (void)submit {
    NSLog(@"%s", __FUNCTION__);

    [self closeNumberPad];

    self.navigationItem.hidesBackButton = YES;
    
    [SVProgressHUD showWithStatus:NSLocalizedString(@"Loading", nil)];
    
    [self submitOrderForEPayPayment];
}

- (void)submitOrderForEPayPayment
{
    self.cart.card = @"epay_new_card";
    self.cart.pincode = nil;
    self.cart.nordpayAuthorization = nil;
    self.cart.nordpayRegistration = nil;
    self.cart.nordpayMethod = nil;
    self.cart.obscuredCardNumber = nil;
    
    if (self.shouldPlaceOrder) {
        [self submitOrder];
    }
    else {
        [self saveCard];
    }
}

- (void)submitOrder
{
    [SVProgressHUD showWithStatus:NSLocalizedString(@"Placing your order", @"Progress indicator after payment")];
    
    RKObjectManager *objectManager = [RKObjectManager sharedManager];
    [objectManager postObject:self.cart path:@"placeorder" parameters:nil success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
        // Response OK
        NSLog(@"Response OK: %@",operation.HTTPRequestOperation.responseString);
        ZEPlaceOrderResult *result = [mappingResult firstObject];
        if (result.orderId) {
            [SVProgressHUD dismiss];
            
            // Success
            self.navigationItem.hidesBackButton = NO;
            self.orderId = result.orderId;
            self.orderKey = result.orderKey;
            
            [self initPayment:self.cart.quote.totalInclTax currency:self.cart.quote.currency orderId:self.orderId description:self.orderId];
        }
    } failure:^(RKObjectRequestOperation *operation, NSError *error) {
        [SVProgressHUD showErrorWithStatus:error.localizedDescription];
        self.navigationItem.hidesBackButton = NO;
        
    }];
}        

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"LoginInfo"]) {
		UINavigationController *navController = [segue destinationViewController];
		ZEModalWebpageViewController *webpageViewController = [navController.viewControllers objectAtIndex:0];
		webpageViewController.url = @"logininfo";
		webpageViewController.delegate = self;
    }
}

- (void)modalWebpageViewControllerDone:(ZEModalWebpageViewController *)termsController
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)saveCard {
    [self.saveCardSwitch setOn:YES animated:NO];
    
    ZEConsumerManager *consumerManager = ((ZEAppDelegate *)[[UIApplication sharedApplication] delegate]).consumer;
    
    // Needs to be logged in
    if ([consumerManager.accessToken isEqualToString:@""]) {
        consumerManager.delegate = self;
        [consumerManager authenticateFromView:self];
        return;
    }

    // Important: The save_card_prefix_identifier must be the same on the GAE server (gae/app/api/epay.py).
    // It is used to distinguish regular orders from "save card" orders.
    NSString* save_card_prefix_identifier = @"save";
    NSString* prefixedOrderId = [save_card_prefix_identifier stringByAppendingString:consumerManager.consumerId];
    
    // Currency 208 = DKK Danish krone.
    [self initPayment:[NSNumber numberWithDouble:0] currency:@"208" orderId:prefixedOrderId description:@"Zerved: Save credit card (iOS)"];
}

- (void)initPayment:(NSNumber *)totalInclTax currency:(NSString *)currency orderId:(NSString *)orderId description:(NSString *) description {
    // Init the ePay Lib with parameters and the view to add it to.
    epaylib = [[ePayLib alloc] initWithView:self.view];

    NSString* amount = [NSString stringWithFormat:@"%i", (int)([totalInclTax doubleValue] * 100)];
    
    // Add parameters to the array
    [epaylib.parameters addObject:[ePayParameter key:@"merchantnumber" value:EPAY_MERCHANT_NUMBER]];          //http://tech.epay.dk/en/specification#258
    [epaylib.parameters addObject:[ePayParameter key:@"currency" value:currency]];
                                               //http://tech.epay.dk/en/specification#259
    [epaylib.parameters addObject:[ePayParameter key:@"amount" value:amount]];                      //http://tech.epay.dk/en/specification#260
    [epaylib.parameters addObject:[ePayParameter key:@"orderid" value:orderId]];                    //http://tech.epay.dk/en/specification#261
    //[parameters addObject:[KeyValue initKey:@"windowid" value:@"1"]];                             //http://tech.epay.dk/en/specification#262
    [epaylib.parameters addObject:[ePayParameter key:@"paymentcollection" value:@"0"]];             //http://tech.epay.dk/en/specification#263
    [epaylib.parameters addObject:[ePayParameter key:@"lockpaymentcollection" value:@"0"]];         //http://tech.epay.dk/en/specification#264
    [epaylib.parameters addObject:[ePayParameter key:@"paymenttype" value:@"1,3,4"]];                      //http://tech.epay.dk/en/specification#265
    [epaylib.parameters addObject:[ePayParameter key:@"language" value:@"0"]];                      //http://tech.epay.dk/en/specification#266
    [epaylib.parameters addObject:[ePayParameter key:@"encoding" value:@"UTF-8"]];                  //http://tech.epay.dk/en/specification#267
    [epaylib.parameters addObject:[ePayParameter key:@"mobilecssurl" value:[NSString stringWithFormat:@"%@css/epay.css?v=5", SERVER_URL]]]; //http://tech.epay.dk/en/specification#269
    [epaylib.parameters addObject:[ePayParameter key:@"instantcapture" value:@"0"]];                //http://tech.epay.dk/en/specification#270
    //[parameters addObject:[KeyValue initKey:@"splitpayment" value:@"0"]];                         //http://tech.epay.dk/en/specification#272
    [epaylib.parameters addObject:[ePayParameter key:@"instantcallback" value:@"1"]];               //http://tech.epay.dk/en/specification#276
    [epaylib.parameters addObject:[ePayParameter key:@"ordertext" value:@"Zerved"]];              //http://tech.epay.dk/en/specification#278
    //[parameters addObject:[KeyValue initKey:@"group" value:@"group"]];                            //http://tech.epay.dk/en/specification#279
    [epaylib.parameters addObject:[ePayParameter key:@"description" value:description]];//http://tech.epay.dk/en/specification#280
    
    NSString* subscriptionEnableDisable = @"0";
    NSString* callbackPincodeString = @"";
    if ([self.saveCardSwitch isOn]) {
        subscriptionEnableDisable = @"1";
        callbackPincodeString = [NSString stringWithFormat:@"?pincode=%@", self.pinField.text];
    }
    
    [epaylib.parameters addObject:[ePayParameter key:@"callbackurl" value:[NSString stringWithFormat:@"%@orders/epaycallback_v2%@", API_URL, callbackPincodeString]]];                           //http://tech.epay.dk/en/specification#275

    [epaylib.parameters addObject:[ePayParameter key:@"subscription" value:subscriptionEnableDisable]];                  //http://tech.epay.dk/en/specification#282
    [epaylib.parameters addObject:[ePayParameter key:@"subscriptionname" value:@"Zerved"]];         //http://tech.epay.dk/en/specification#283
    //[parameters addObject:[KeyValue initKey:@"mailreceipt" value:@""]];                           //http://tech.epay.dk/en/specification#284
    //[parameters addObject:[KeyValue initKey:@"googletracker" value:@"0"]];                        //http://tech.epay.dk/en/specification#286
    //[parameters addObject:[KeyValue initKey:@"backgroundcolor" value:@""]];                       //http://tech.epay.dk/en/specification#287
    //[parameters addObject:[KeyValue initKey:@"opacity" value:@""]];                               //http://tech.epay.dk/en/specification#288
    //[epaylib.parameters addObject:[ePayParameter key:@"declinetext" value:@"DeclineText"]];        //http://tech.epay.dk/en/specification#289
    [epaylib.parameters addObject:[ePayParameter key:@"ownreceipt" value:@"1"]];
    [epaylib.parameters addObject:[ePayParameter key:@"accepturl" value:[NSString stringWithFormat:@"%@orders/epayaccept_v2", API_URL]]]; // The accepturl is not used as the app changes the tab when a 'PaymentAcceptedNotification' event is received from the ePay mobile SDK. The documentation says accepturl must be filled out if ownreceipt = 1 and is thus supplied.
    
    NSLog(@"Payment window parameters");
    NSMutableString *hashinputstring = [NSMutableString string];
    for (ePayParameter *item in epaylib.parameters) {
        NSLog(@"%@ = %@", item.key, item.value);
        [hashinputstring appendString:item.value];
    }
    
    NSString* hashedParameters = [[NSString stringWithString:hashinputstring] md5Hash];
    
   [epaylib.parameters addObject:[ePayParameter key:@"hash" value:[NSString stringWithFormat:@"%@%@", hashedParameters, EPAY_MD5_HASH_KEY]]];                                  //http://tech.epay.dk/en/specification#281
    
    // Set the hash key
    [epaylib setHashKey:EPAY_MD5_HASH_KEY];
    
    // Show/hide the Cancel button
    [epaylib setDisplayCancelButton:YES];
    
    // Load the payment window
    [epaylib loadPaymentWindow];
}

-(BOOL) isHashOK:(id) parametersObject{
    NSString* receivedHash = @"";

    NSMutableString* stringToHash = [NSMutableString stringWithString:@""];
    for (ePayParameter *item in parametersObject) {
        NSLog(@"Data: %@ = %@", item.key, item.value);
        if ([item.key isEqualToString:@"hash"]) {
            receivedHash = item.value;
        } else
        {
            [stringToHash appendString:item.value];
        }
    }
    
    [stringToHash appendString:EPAY_MD5_HASH_KEY];
    NSString* calculatedHash = [[NSString stringWithString:stringToHash] md5Hash];
    
    if ([calculatedHash isEqualToString:receivedHash]) {
        DLog(@"Received hash is ok");
        return YES;
    }

    return NO;
}

- (void)eventFromEpayLib:(NSNotification*)notification {
    // Here we handle all events sent from the ePay Lib
    
    if ([[notification name] isEqualToString:PaymentAcceptedNotification]) {
        NSLog(@"EVENT: PaymentAcceptedNotification");
        
        for (ePayParameter *item in [notification object]) {
            NSLog(@"Data: %@ = %@", item.key, item.value);
        }
        
        if ([self isHashOK:[notification object]]) {
        } else {
            // The received hash does not match the expected hash. There is not much to do about it at this time.
            DLog(@"Received epay md5 hash NOT ok");
            [SVProgressHUD showErrorWithStatus:@"Received epay md5 hash NOT ok"];
        }
        
        [self.cart clear];

        [self.delegate epayViewControllerDidPlaceOrder:self id:self.orderId key:self.orderKey fromTabIndex:self.tabIndex];
    }
    else if ([[notification name] isEqualToString:PaymentLoadingAcceptPageNotification]) {
    }
    else if ([[notification name] isEqualToString:PaymentWindowCancelledNotification]) {
    }
    else if ([[notification name] isEqualToString:PaymentWindowLoadingNotification]) {
        // Display a loading indicator while loading the payment window
        [activityIndicatorView startAnimating];
    }
    else if ([[notification name] isEqualToString:PaymentWindowLoadedNotification]) {
        // Stop our loading indicator when the payment window is loaded
        [activityIndicatorView stopAnimating];
        [SVProgressHUD dismiss];
    }
    else if ([[notification name] isEqualToString:ErrorOccurredNotification]) {
        NSLog(@"EVENT: ErrorOccurredNotification - %@", [notification object]);
    }
    else if ([[notification name] isEqualToString:NetworkActivityNotification]) {
        // Display network indicator in the statusbar
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    }
    else if ([[notification name] isEqualToString:NetworkNoActivityNotification]) {
        // Hide network indicator in the statusbar
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    }
}

#pragma mark -

@end
