
//
//  ZEBarMenusViewController.m
//  Zerved
//
//  Created by Zerved Development Mac on 28/08/13.
//
//  List the menus available at a bar. The menus go to ZEBarCategoriesViewController.
//

#import "ZEBarMenusViewController.h"
#import "ZEBarCategoriesViewController.h"
#import "QuartzCore/CAGradientLayer.h"
#import "SVProgressHUD.h"
#import <RestKit/RestKit.h>
#import "ZEEmptyView.h"
#import "ZEEventSummaryView.h"

@interface ZEBarMenusViewController ()

@end

@implementation ZEBarMenusViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Set white background view
    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.opaque = NO;
    self.tableView.backgroundView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.tableView.frame.size.width, self.tableView.frame.size.height)];
    self.tableView.backgroundView.backgroundColor = [UIColor whiteColor];
    
    if (self.event) {
        // Get event menus
        self.menus = ((ZEBar *)[self.event.bars firstObject]).menus;
        [self reloadTableView];
    } else {
        // Load bar menus
        [SVProgressHUD showWithStatus:NSLocalizedString(@"Loading", nil)];
        RKObjectManager *objectManager = [RKObjectManager sharedManager];
        [objectManager getObjectsAtPath:[NSString stringWithFormat:@"bars/%@/menus", self.bar.key]
                             parameters:nil
                                success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
                                    [SVProgressHUD dismiss];
                                    self.menus = [mappingResult array];
                                    [self reloadTableView];
                                }
                                failure:^(RKObjectRequestOperation *operation, NSError *error) {
                                    [SVProgressHUD dismiss];
                                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil)
                                                                                    message:[error localizedDescription]
                                                                                   delegate:nil
                                                                          cancelButtonTitle:NSLocalizedString(@"OK", nil)
                                                                          otherButtonTitles:nil];
                                    [alert show];
                                }];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)reloadTableView
{
    if(self.isViewLoaded) {
        [self.tableView reloadData];
        if ([self.menus count] == 0) {
            ZEEmptyView *emptyView = [[[NSBundle mainBundle] loadNibNamed:@"EmptyTableView" owner:self options:nil] objectAtIndex:0];
            [emptyView setupViewForTableView:self.tableView text:NSLocalizedString(@"No menus at this bar", nil)];
            [self.tableView setTableHeaderView:emptyView];
        } else if (self.event) {
            if (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1) {
                // Event summary
                // Disabled for iOS 6 because the autolayout with title height resizing does not layout correctly.
                // For some reason it works on other pages, but not this one. I have not been able to find
                // the bug, so I had to disable it for IOS6.
                ZEEventSummaryView *eventSummaryView = [[[NSBundle mainBundle] loadNibNamed:@"EventSummaryView" owner:self options:nil] objectAtIndex:0];
                [eventSummaryView setEvent:self.event];
                [self.tableView setTableHeaderView:eventSummaryView];
            }
        }
    }
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [self.menus count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
    cell = [tableView dequeueReusableCellWithIdentifier:@"MenuCell" forIndexPath:indexPath];
    ZEMenu *menu = [self.menus objectAtIndex:indexPath.row];
    cell.textLabel.text = menu.name;
    return cell;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"ViewBarCategories"]) {
        ZEBarCategoriesViewController *destinationController = [segue destinationViewController];
        destinationController.location = self.location;
        destinationController.title = [self.tableView cellForRowAtIndexPath:[self.tableView indexPathForSelectedRow]].textLabel.text;
        destinationController.bar = self.bar;
        destinationController.menu = [self.menus objectAtIndex:[self.tableView indexPathForSelectedRow].row];
        destinationController.event = self.event;
    }
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     */
}

@end
