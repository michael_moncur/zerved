//
//  ZEServiceAddressViewController.m
//  Zerved
//
//  Created by Anders Rasmussen on 25/11/13.
//
//

#import "ZEServiceAddressViewController.h"
#import "SVProgressHUD.h"
#import "ZEAppDelegate.h"
#import "ZEConsumerManager.h"

@interface ZEServiceAddressViewController ()

@end

@implementation ZEServiceAddressViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.nameField.text = self.cart.deliveryName;
    self.phoneField.text = self.cart.deliveryPhone;
    self.address1Field.text = self.cart.deliveryAddress1;
    self.address2Field.text = self.cart.deliveryAddress2;
    self.cityField.text = self.cart.deliveryCity;
    self.postcodeField.text = self.cart.deliveryPostcode;
    self.instructionsField.text = self.cart.deliveryInstructions;
    
    if ([self.cart.deliveryName isEqualToString:@""]) {
        [self loadConsumerAddress];
    }
}

- (void)loadConsumerAddress
{
    [SVProgressHUD showWithStatus:NSLocalizedString(@"Loading", nil)];
    ZEConsumerManager *consumerManager = ((ZEAppDelegate *)[[UIApplication sharedApplication] delegate]).consumer;
    if ([consumerManager.accessToken isEqualToString:@""]) {
        consumerManager.delegate = self;
        [consumerManager authenticateFromView:self];
        return;
    }
    RKObjectManager *objectManager = [RKObjectManager sharedManager];
    [objectManager getObjectsAtPath:[NSString stringWithFormat:@"consumers/%@", [consumerManager consumerKey]]
                         parameters:nil
                            success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
                                // Consumer data received, update view
                                [SVProgressHUD dismiss];
                                self.consumer = [mappingResult firstObject];
                                self.nameField.text = self.consumer.name;
                                self.phoneField.text = self.consumer.phone;
                                self.address1Field.text = self.consumer.address1;
                                self.address2Field.text = self.consumer.address2;
                                self.cityField.text = self.consumer.city;
                                self.postcodeField.text = self.consumer.postcode;
                            }
                            failure:^(RKObjectRequestOperation *operation, NSError *error) {
                                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil)
                                                                                message:[error localizedDescription]
                                                                               delegate:nil
                                                                      cancelButtonTitle:NSLocalizedString(@"OK", nil)
                                                                      otherButtonTitles:nil];
                                [alert show];
                            }];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)done:(id)sender {
    NSArray *requiredFields = @[self.nameField, self.address1Field, self.cityField, self.postcodeField, self.phoneField];
    for (UITextField *field in requiredFields) {
        if ([field.text isEqualToString:@""]) {
            [SVProgressHUD showErrorWithStatus:[NSString stringWithFormat:NSLocalizedString(@"%@ is required", @"Error msg for required field with placeholder for field name"), field.placeholder]];
            return;
        }
    }
    
    self.cart.service = @"delivery";
    self.cart.deliveryName = self.nameField.text;
    self.cart.deliveryPhone = self.phoneField.text;
    self.cart.deliveryAddress1 = self.address1Field.text;
    self.cart.deliveryAddress2 = self.address2Field.text;
    self.cart.deliveryCity = self.cityField.text;
    self.cart.deliveryPostcode = self.postcodeField.text;
    self.cart.deliveryInstructions = self.instructionsField.text;
    
    [self.delegate serviceDone];
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    if ([self.bar.deliveryMinOrderAmount doubleValue] > 0.0) {
        UILabel *label = [[UILabel alloc] init];
        label.frame = CGRectMake(15, 10, 290, 40);
        label.backgroundColor = [UIColor clearColor];
        label.textColor = [UIColor colorWithRed:185/255.0 green:74/255.0 blue:72/255.0 alpha:1.0];
        label.font = [UIFont boldSystemFontOfSize:14];
        label.textAlignment = NSTextAlignmentCenter;
        label.numberOfLines = 0;
        label.text = [NSString stringWithFormat:NSLocalizedString(@"The minimum order amount for delivery is %@", nil), self.bar.deliveryMinOrderAmountFormatted];
        UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 60)];
        [footerView addSubview:label];
        return footerView;
    }
    return nil;
}

#pragma mark - Consumer delegate

- (void)didAuthenticate
{
    [self loadConsumerAddress];
}

- (void)didFailToAuthenticate
{
    
}

@end
