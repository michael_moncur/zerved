//
//  ZELoginViewController.m
//  Zerved
//
//  Created by Anders Rasmussen on 16/10/12.
//
//

#import "ZELoginViewController.h"
#import "ZEAppDelegate.h"
#import "SVProgressHUD.h"

@interface ZELoginViewController ()

@end

@implementation ZELoginViewController

@synthesize headerLabel = _headerLabel;
@synthesize emailField = _emailField;
@synthesize passwordField = _passwordField;
@synthesize repeatPasswordField = _repeatPasswordField;
@synthesize confirmButton = _confirmButton;
@synthesize segmentBar = _segmentBar;
@synthesize passwordHelpLabel = _passwordHelpLabel;
@synthesize infoButton = _infoButton;
@synthesize forgotPasswordButton = _forgotPasswordButton;
@synthesize delegate = _delegate;
@synthesize customLoginHeader, customLoginSubtitle, customSignupHeader, customSignupSubtitle, presetEmail;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1) {
        self.navigationController.navigationBar.tintColor = [UIColor blackColor];
    }
    [self.segmentBar setTitle:NSLocalizedString(@"Log In", nil) forSegmentAtIndex:0];
    [self.segmentBar setTitle:NSLocalizedString(@"New Account", nil) forSegmentAtIndex:1];
    [self.emailField setPlaceholder:NSLocalizedString(@"Email", @"Login/create account email field")];
    [self.passwordField setPlaceholder:NSLocalizedString(@"Password", @"Login/create account password field")];
    [self.repeatPasswordField setPlaceholder:NSLocalizedString(@"Repeat password", @"Login/create account repeat password field")];
    [self.passwordHelpLabel setText:NSLocalizedString(@"Must contain letters and digits, minimum 6 characters", @"Login/create account password help label")];
    if (self.presetEmail) {
        self.emailField.text = self.presetEmail;
    }
    
    self.segmentBar.selectedSegmentIndex = 0;
    [self switchViewSegment];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setHeaderLabel:nil];
    [self setEmailField:nil];
    [self setPasswordField:nil];
    [self setRepeatPasswordField:nil];
    [self setConfirmButton:nil];
    [self setSegmentBar:nil];
    [self setPasswordHelpLabel:nil];
    [self setSubtitleLabel:nil];
    [self setCustomLoginHeader:nil];
    [self setCustomLoginSubtitle:nil];
    [self setCustomSignupHeader:nil];
    [self setCustomSignupSubtitle:nil];
    [self setInfoButton:nil];
    [self setForgotPasswordButton:nil];
    [self setPresetEmail:nil];
    [super viewDidUnload];
}

- (void)switchViewSegment {
    NSInteger selectedSegment = self.segmentBar.selectedSegmentIndex;
    if (selectedSegment == 0) {
        // Login
        [self.emailField setHidden:NO];
        [self.passwordField setHidden:NO];
        [self.repeatPasswordField setHidden:YES];
        [self.passwordHelpLabel setHidden:YES];
        [self.forgotPasswordButton setHidden:NO];
        self.headerLabel.text = NSLocalizedString(@"Log in to your Zerved account", @"Log in title");
        self.subtitleLabel.text = NSLocalizedString(@"...to view order history and edit profile data", nil);
        self.confirmButton.title = NSLocalizedString(@"Log In", nil);
        self.passwordField.returnKeyType = UIReturnKeyGo;
        self.infoButton.hidden = YES;
        if (self.customLoginHeader) {
            self.headerLabel.text = self.customLoginHeader;
        }
        if (self.customLoginSubtitle) {
            self.subtitleLabel.text = self.customLoginSubtitle;
        }
        if ([self.emailField.text isEqualToString:@""]) {
            [self.emailField becomeFirstResponder];
        } else {
            [self.passwordField becomeFirstResponder];
        }
    } else {
        // Create account
        // No email or password required anymore
        [self.emailField setHidden:YES];
        [self.passwordField setHidden:YES];
        [self.repeatPasswordField setHidden:YES];
        [self.passwordHelpLabel setHidden:YES];
        [self.forgotPasswordButton setHidden:YES];
        self.headerLabel.text = NSLocalizedString(@"New Account", nil);
        self.subtitleLabel.text = NSLocalizedString(@"Reset the app with a fresh Zerved account", nil);
        self.confirmButton.title = NSLocalizedString(@"Create", nil);
        [self.view endEditing:YES];
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    NSInteger nextTag = textField.tag + 1;
    UIResponder* nextResponder = [textField.superview viewWithTag:nextTag];
    if (self.segmentBar.selectedSegmentIndex == 0 && nextTag == 2) {
        //[textField resignFirstResponder];
        [self confirm:nil];
        return NO;
    }
    if (nextResponder) {
        [nextResponder becomeFirstResponder];
    } else {
        //[textField resignFirstResponder];
        [self confirm:nil];
    }
    return NO;
}

- (IBAction)segmentChanged:(UISegmentedControl *)sender {
    [self switchViewSegment];
}

- (IBAction)confirm:(id)sender {
    if (self.segmentBar.selectedSegmentIndex == 0) {
        // Validate email
        NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
        NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
        if (![emailTest evaluateWithObject:self.emailField.text]) {
            [SVProgressHUD showErrorWithStatus:NSLocalizedString(@"Invalid email address", nil)];
            return;
        }
        [self.delegate loginViewControllerDidLoginWithEmail:self.emailField.text password:self.passwordField.text];
    } else {
        [self.delegate loginViewControllerDidRequestNewAccount];
    }
}

- (IBAction)cancel:(id)sender
{
    [self.delegate loginViewControllerDidCancel];
}

- (IBAction)forgotPassword:(id)sender {
    if ([self.emailField.text isEqualToString:@""]) {
        [SVProgressHUD showErrorWithStatus:NSLocalizedString(@"Invalid email address", nil)];
    } else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Reset password", nil) message:[NSString stringWithFormat:NSLocalizedString(@"Send a password reset link to %@?", nil), self.emailField.text] delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel", nil) otherButtonTitles:NSLocalizedString(@"Send reset link", nil), nil];
        [alert setTag:1];
        [alert show];
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"LoginInfo"]) {
        UINavigationController *navController = [segue destinationViewController];
        ZELoginInfoViewController *loginInfoController = [navController.viewControllers objectAtIndex:0];
        loginInfoController.delegate = self;
    }
}

#pragma mark -
#pragma mark ZELogininfoDelegate Methods
- (void)loginInfoControllerDone:(ZELoginInfoViewController *)loginInfoController
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark -
#pragma mark UIAlertViewDelegate Methods
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 1 && buttonIndex == 1) {
        [SVProgressHUD showWithStatus:NSLocalizedString(@"Loading", @"Progress indicator loading")];
        [self.delegate loginViewControllerDidRequestResetPasswordForEmail:self.emailField.text];
    }
}

@end
