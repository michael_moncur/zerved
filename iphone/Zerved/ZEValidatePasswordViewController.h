//
//  ZEValidatePasswordViewController.h
//  Zerved
//
//  Created by Anders Rasmussen on 12/03/13.
//
//

#import <UIKit/UIKit.h>

@protocol ZEValidatePasswordDelegate;

@interface ZEValidatePasswordViewController : UIViewController <UITextFieldDelegate>

@property (weak, nonatomic) id <ZEValidatePasswordDelegate> delegate;
@property (weak, nonatomic) IBOutlet UITextField *passwordField;
- (IBAction)confirm:(id)sender;
- (IBAction)forgotPassword:(id)sender;
- (IBAction)typePassword:(id)sender;

@end

@protocol ZEValidatePasswordDelegate
- (void)validatePasswordDidEnterPassword:(NSString *)password;
- (void)validatePasswordForgotPassword;
@end