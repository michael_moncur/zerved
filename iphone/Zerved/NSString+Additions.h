//
//  NSString+Additions.h
//  Zerved
//
//  Created by Anders Rasmussen on 20/11/13.
//
//

#import <Foundation/Foundation.h>

@interface NSString (Additions)

- (CGSize)sizeWithFont:(UIFont *)font constrainedTo:(CGSize)size;

@end
