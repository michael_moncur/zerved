//
//  ZECart.m
//  Zerved
//
//  Created by Anders Rasmussen on 15/04/12.
//  Copyright (c) 2012 Zerved. All rights reserved.
//

#import "ZECart.h"
#import "ZECartItem.h"
#import "ZEAppDelegate.h"
#import "ZEConsumerManager.h"

@implementation ZECart

@synthesize location = _location, bar = _bar, tipPercent = _tipPercent;

- (id)initWithLocation:(ZELocation *)location bar:(ZEBar *)bar group:(NSString *)group event:(ZEEvent *)event
{
    self = [super init];
    if (self) {
        self.group = group;
        self.location = location;
        self.bar = bar;
        self.event = event;
        self.items = [[NSMutableArray alloc] init];
        [self clear];
    }
    return self;
}

- (void)setLocation:(ZELocation *)location
{
    _location = location;
    if ([self.group isEqualToString:@"entrance"]) {
        self.service = @"entrance";
    }
}

- (void)setBar:(ZEBar *)bar
{
    _bar = bar;
    // Preselect a service type if only one is available
    if (bar && [bar hasCounterService] && ![bar.tableService boolValue] && ![bar hasToStayAndToGo] && ![bar.deliveryService boolValue]) {
        self.service = @"counter";
        if ([bar.counterServiceToGo boolValue]) {
            self.toGo = YES;
        } else if ([bar.counterServiceToStay boolValue]) {
            self.toGo = NO;
        }
    }
    if (bar && ![bar hasCounterService] && [bar.tableService boolValue] && ![bar.deliveryService boolValue]) {
        self.service = @"table";
    }
}
/*
- (void)setTipPercent:(float)tipPercent
{
    _tipPercent = tipPercent;
    if (self.quote) {
        double tip = [self.quote.subtotal doubleValue] * tipPercent/100.0f;
        self.tipAmount = [NSNumber numberWithDouble:tip];
        DLog(@"%@", self.tipAmount);
    }
}
*/
- (int)count
{
    int items = 0;
    for (ZECartItem *item in self.items) {
        items += item.quantity;
    }
    return items;
}

- (NSUInteger)countRows
{
    return [self.items count];
}

- (void)clear
{
    [self.items removeAllObjects];
    self.service = @"";
    self.tableNumber = @"";
    self.tipPercent = 0;
    self.card = nil;
    self.pincode = nil;
    self.nordpayRegistration = nil;
    self.nordpayAuthorization = nil;
    self.quote = nil;
    ZEConsumerManager *consumerManager = ((ZEAppDelegate *)[[UIApplication sharedApplication] delegate]).consumer;
    self.termsAccepted = [consumerManager termsAcceptedRecently];
    self.deliveryName = @"";
    self.deliveryPhone = @"";
    self.deliveryAddress1 = @"";
    self.deliveryAddress2 = @"";
    self.deliveryCity = @"";
    self.deliveryPostcode = @"";
    self.deliveryInstructions = @"";
    self.loyaltyCode = nil;
}

- (void)addItem:(ZECartItem *)item
{
    [self addItem:item atIndex:self.items.count];
}

- (void)addItem:(ZECartItem *)item atIndex:(NSUInteger)index
{
    NSUInteger existingIndex = [self.items indexOfObject:item];
    if (existingIndex == NSNotFound) {
        [self.items insertObject:item atIndex:index];
    } else {
        [(ZECartItem *)[self.items objectAtIndex:existingIndex] addQuantity:item.quantity];
    }
    DLog(@"%@", [self description]);
}

- (void)removeItem:(ZECartItem *)item
{
    [self.items removeObject:item];
}

- (void)updateItem:(ZECartItem *)item atIndex:(NSUInteger)index
{
    // Remove and insert again to avoid two rows with same options
    [self.items removeObjectAtIndex:index];
    [self addItem:item atIndex:index];
}

- (NSArray *)itemsForProduct:(NSString *)productKey
{
    NSMutableArray *itemsForProduct = [[NSMutableArray alloc] init];
    for (ZECartItem *item in self.items) {
        if ([item.productKey isEqualToString:productKey]) {
            [itemsForProduct addObject:item];
        }
    }
    return itemsForProduct;
}

- (id)jsonObject
{
    NSMutableDictionary *data = [[NSMutableDictionary alloc] initWithObjectsAndKeys:
                                 self.group, @"group",
                                 self.service, @"service",
                                 self.tableNumber, @"table_number",
                                 self.termsAccepted, @"terms_accepted",
                                 self.toGo, @"to_go", nil];
    if ([self.group isEqualToString:@"menu"]) {
        [data setObject:self.bar.key forKey:@"bar"];
    } else {
        [data setObject:self.location.key forKey:@"location"];
    }
    NSMutableArray *items = [[NSMutableArray alloc] init];
    for (ZECartItem *item in self.items) {
        [items addObject:[item jsonObject]];
    }
    [data setObject:items forKey:@"items"];

    return data;
}

- (NSString *)locationKey
{
    return self.location.key;
}

- (NSString *)barKey {
    if (self.bar != nil) {
        return self.bar.key;
    }
    return nil;
}

- (NSString *)eventKey {
    if (self.event != nil) {
        return self.event.key;
    }
    return nil;
}

- (void)attachQuote:(ZEQuote *)quote
{
    self.quote = quote;
    // Update location/bar objects
    if (quote.location) {
        self.location = quote.location;
    }
    if (quote.bar) {
        self.bar = quote.bar;
    }
    // Attach quote items to cart items
    for (int i=0; i<[quote.items count]; i++) {
		if (self.items.count > i)
			((ZECartItem *)[self.items objectAtIndex:i]).quoteItem = [quote.items objectAtIndex:i];
    }
    // Update service
    if ([self.service isEqualToString:@""]) {
        if ([self.group isEqualToString:@"entrance"]) {
            self.service = @"entrance";
        } else if (self.bar && [self.bar hasCounterService] && ![self.bar.tableService boolValue] && ![self.bar hasToStayAndToGo] && ![self.bar.deliveryService boolValue]) {
            self.service = @"counter";
            if ([self.bar.counterServiceToGo boolValue]) {
                self.toGo = YES;
            } else if ([self.bar.counterServiceToStay boolValue]) {
                self.toGo = NO;
            }
        } else if (self.bar && [self.bar.tableService boolValue] && ![self.bar hasCounterService] && ![self.bar.deliveryService boolValue]) {
            self.service = @"table";
        }
    } else if ([self.service isEqualToString:@"counter"] && self.bar && ![self.bar hasCounterService]) {
        self.service = @"";
    } else if ([self.service isEqualToString:@"table"] && self.bar && ![self.bar.tableService boolValue]) {
        self.service = @"";
    } else if ([self.service isEqualToString:@"delivery"] && self.bar && ![self.bar.deliveryService boolValue]) {
        self.service = @"";
    }
    // Recalculate tip
    [self setTipPercent:self.tipPercent];
    // Loyalty code
    if (self.loyaltyCode == nil && ![quote.loyaltyCode isEqualToString:@""]) {
        self.loyaltyCode = quote.loyaltyCode;
    }
}

- (NSInteger)validateQuoteItems
{
    NSMutableArray *invalidItems = [NSMutableArray array];
    for (ZECartItem *item in self.items) {
        if (![item.quoteItem.valid boolValue]) {
            [invalidItems addObject:item];
        }
    }
    [self.items removeObjectsInArray:invalidItems];
    return [invalidItems count];
}

- (int)estimatedWaitingTime
{
    if (self.event) {
        return 0; // No waiting time for event orders
    }
    if (([self.service isEqualToString:@"counter"] || [self.service isEqualToString:@"table"]) && [self.bar.estimatedWaitingTime intValue] > 0) {
        return [self.bar.estimatedWaitingTime intValue];
    }
    if ([self.service isEqualToString:@"delivery"] && [self.bar.estimatedWaitingTimeDelivery intValue] > 0) {
        return [self.bar.estimatedWaitingTimeDelivery intValue];
    }
    return 0;
}

- (NSString *)description
{
    NSString *desc = [NSString stringWithFormat:@"%@ %@", self.location, self.group];
    for (ZECartItem *item in self.items) {
        desc = [desc stringByAppendingFormat:@"\n%@", item];
    }
    return desc;
}

@end
