//
//  ZETabBarController.h
//  Zerved
//
//  Created by Anders Rasmussen on 02/05/12.
//  Copyright (c) 2012 Zerved. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZECartViewController.h"
#import "ZE4TPhoneViewController.h"
#import "ZEEpayViewController.h"

@interface ZETabBarController : UITabBarController <UITabBarControllerDelegate, ZECartViewControllerDelegate, ZEEpayViewControllerDelegate, ZE4TPhoneDelegate>

@property (strong, nonatomic) NSString *location;
@property (strong, nonatomic) NSString *locationName;
- (void)placeOrderWithId:(NSString *)orderId key:(NSString *)orderKey fromTabIndex:(NSInteger)tabIndex;
- (void)viewOrderByKey:(NSString *)orderKey;
- (void)viewOrderById:(NSString *)orderId;

@end
