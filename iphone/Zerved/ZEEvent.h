//
//  ZEEvent.h
//  Zerved
//
//  Created by Anders Rasmussen on 23/10/13.
//
//

#import <Foundation/Foundation.h>

@interface ZEEvent : NSObject

@property (nonatomic, copy) NSString *key;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *timeLabel;
@property (nonatomic, copy) NSString *dateStartFormatted;
@property (nonatomic, copy) NSString *dateDeliveryFormatted;
@property (nonatomic, copy) NSString *dateStartTimeFormatted;
@property (nonatomic, copy) NSString *dateDeliveryTimeFormatted;
@property (nonatomic, copy) NSString *dateStartDateFormatted;
@property (nonatomic, copy) NSString *dateDeliveryDateFormatted;
@property (nonatomic, copy) NSNumber *openForOrders;
@property (nonatomic, strong) NSArray *bars;

@end
