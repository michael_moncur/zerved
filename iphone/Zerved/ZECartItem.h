//
//  ZECartItem.h
//  Zerved
//
//  Created by Anders Rasmussen on 28/05/13.
//
//

#import <Foundation/Foundation.h>
#import "ZEQuoteItem.h"
#import "ZEFavourite.h"

@interface ZECartItem : NSObject

@property (nonatomic, strong) NSString *productKey;
@property (nonatomic) int quantity;
@property (nonatomic, strong) NSString *productName;
@property (nonatomic, strong) NSMutableArray *options;
@property (nonatomic, strong) ZEQuoteItem *quoteItem;
@property (nonatomic, assign) BOOL isExpanded;
@property (nonatomic, assign) BOOL isFavourite;
@property (nonatomic, strong) NSNumber *favouriteId;
@property (nonatomic, strong) ZEFavourite *favourite;
- (id)initWithProductKey:(NSString *)productKey quantity:(int)quantity options:(NSMutableArray *)options;
- (void)addQuantity:(int)quantity;
- (id)jsonObject;
- (BOOL)hasValue:(NSInteger)selectedIndex forOptionId:(NSString *)optionId;
- (BOOL)hasValueForOptionId:(NSString *)optionId;
- (void)clearOptionId:(NSString *)optionId;
- (void)removeOptionWithOptionId:(NSString *)optionId value:(NSInteger)selectedIndex;

@end
