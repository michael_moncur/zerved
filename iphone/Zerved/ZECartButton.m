//
//  ZECartButton.m
//  Zerved
//
//  Created by Anders Rasmussen on 20/11/13.
//
//

#import "ZECartButton.h"

@implementation ZECartButton

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self.layer setMasksToBounds:YES];
        [self.layer setCornerRadius:3.0f];
        self.titleLabel.textAlignment = NSTextAlignmentCenter;
        self.titleLabel.textColor = [UIColor whiteColor];
        self.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:15.0f];
        self.enabled = NO;
    }
    return self;
}

- (UIEdgeInsets)alignmentRectInsets
{
    return UIEdgeInsetsMake(0, 0, 0, 9.0f);
}

- (void)updateAnimated:(BOOL)animated
{
    int items = (self.cart) ? [self.cart count] : 0;
    NSString* itemsString = [NSString stringWithFormat:NSLocalizedString(@"Cart (%d)", @"Cart button with number of items"), items];
    
    [self setTitle:itemsString forState:UIControlStateNormal];
    [self sizeToFit];
    // 10 px padding left/tight
    self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, self.frame.size.width+20, self.frame.size.height);
    if (items > 0) {
        self.backgroundColor = [UIColor colorWithRed:72/255.0f green:217/255.0f blue:116/255.0f alpha:1.0];
    } else {
        self.backgroundColor = [UIColor colorWithRed:0.8 green:0.8 blue:0.8 alpha:0.5];
    }
    self.enabled = items > 0;
    
    // Flash 3 times
    if (animated && items > 0) {
        self.alpha = 1.0f;
        [UIView animateWithDuration:0.2 delay:0.0 options:UIViewAnimationOptionCurveEaseInOut |
         UIViewAnimationOptionRepeat |
         UIViewAnimationOptionAutoreverse animations:^{
             [UIView setAnimationRepeatCount:2];
             self.alpha = 0.0f;
         } completion:^(BOOL finished) {
             [UIView animateWithDuration:0.4 delay:0.0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
                 self.alpha = 1.0f;
             } completion:nil];
         }];
    }
}

@end
