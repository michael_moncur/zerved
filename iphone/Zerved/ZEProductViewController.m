//
//  ZEProductViewController.m
//  Zerved
//
//  Created by Anders Rasmussen on 11/11/13.
//
//  Product view, including name, price, image, description, tier prices, and options.
//  Goes back to product list after adding to cart.
//

#import "ZEProductViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "SVProgressHUD.h"
#import "ZEProductOptionToggleButton.h"
#import "ZEProductOptionDropdownButton.h"
#import "ZEAppDelegate.h"
#import "ZECart.h"
#import "ZEProductCartItemView.h"
#import "ZECartViewController.h"
#import "ZETabBarController.h"
#import "ZEProductOption.h"

@interface ZEProductViewController ()

@end

@implementation ZEProductViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    // Create custom cart button
    self.cartButton = [[ZECartButton alloc] initWithFrame:CGRectMake(0, 0, 100, 44)];
    [self.cartButton addTarget:self action:@selector(goToCart:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:self.cartButton];
    
    self.optionsTable.delegate = self;
    self.optionsTable.dataSource = self;
    self.optionsTable.alwaysBounceVertical = NO;
    self.optionsTable.scrollEnabled = NO;
    
    // Preselect first option value in required options
    for (ZEProductOption *option in self.product.productOptions) {
        if ([option.required boolValue] && [option.selectionType isEqualToString:@"single"] && option.selectedIndex == -1 && [option.values count] > 0) {
            option.selectedIndex = 0;
        }
    }
    
    [self reloadView];
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    // Resize options table
    CGFloat optionsHeight = [self.product.productOptions count] > 0 ? 44*[self.product.productOptions count] + 55 : 0;
    self.optionsTable.frame = CGRectMake(self.optionsTable.frame.origin.x, self.optionsTable.frame.origin.y, self.optionsTable.frame.size.width, optionsHeight);
}

- (void)reloadView
{
    // Title
    self.title = self.product.name;
    
    // Get cart items
    ZEAppDelegate *delegate = [[UIApplication sharedApplication] delegate];
    ZECart *cart = [delegate.cartManager getCartForLocation:self.location bar:self.bar group:self.group event:self.event];
    self.cartButton.cart = cart;
    self.items = [cart itemsForProduct:self.product.key];
    [self.cartButton updateAnimated:NO];
    
    float y = 0;
    //float yRight = 0;
    
    // Top msg
    self.statusLabel.text = [self.product.messageWhenClosed uppercaseString];
    BOOL openForOrders;
    if (self.event) {
        openForOrders = [self.event.openForOrders boolValue];
    } else {
        openForOrders = ([self.product.merchantOpen boolValue] && [self.product.menuActive boolValue]);
    }
    if (openForOrders) {
        // Merchant open
        self.statusLabel.text = [NSLocalizedString(@"Open for orders", @"Green top message in product list when customers may order") uppercaseString];
        self.statusLabel.backgroundColor = [UIColor colorWithRed:72/255.0f green:217/255.0f blue:116/255.0f alpha:1.0];
    } else {
        // Merchant closed
        self.statusLabel.text = [self.product.messageWhenClosed uppercaseString];
        self.statusLabel.backgroundColor = [UIColor redColor];
    }
    [self.statusLabel sizeToFit];
    self.statusLabel.frame = CGRectMake(self.statusLabel.frame.origin.x, self.statusLabel.frame.origin.y, 320, self.statusLabel.frame.size.height);
    
    // Header
    y += self.statusLabel.frame.size.height;
    float yHeader = y;
    self.headerView.frame = CGRectMake(self.headerView.frame.origin.x, yHeader, self.headerView.frame.size.width, self.headerView.frame.size.height);
    //yRight += self.statusLabel.frame.size.height;
    //self.imageView.frame = CGRectMake(self.imageView.frame.origin.x, y, self.imageView.frame.size.width, self.imageView.frame.size.height);
    if (self.product.imageUrl) {
        // Image
        CALayer *borderLayerRight = [CALayer layer];
        CGRect borderFrameRight = CGRectMake(self.imageView.frame.size.width-IMAGE_BORDER_WIDTH, 0, IMAGE_BORDER_WIDTH, self.imageView.frame.size.height);
        [borderLayerRight setBackgroundColor:[[UIColor colorWithWhite:0.9f alpha:1.0f] CGColor]];
        [borderLayerRight setFrame:borderFrameRight];
        [self.imageView.layer addSublayer:borderLayerRight];
        CALayer *borderLayerBottom = [CALayer layer];
        CGRect borderFrameBottom = CGRectMake(0, self.imageView.frame.size.height-IMAGE_BORDER_WIDTH, self.imageView.frame.size.width, IMAGE_BORDER_WIDTH);
        [borderLayerBottom setBackgroundColor:[[UIColor colorWithWhite:0.9f alpha:1.0f] CGColor]];
        [borderLayerBottom setFrame:borderFrameBottom];
        [self.imageView.layer addSublayer:borderLayerBottom];
        [self.imageView sd_setImageWithURL:[NSURL URLWithString:self.product.imageUrl] placeholderImage:nil options:SDWebImageRefreshCached];
        self.imageView.clipsToBounds = YES;
        
        // Name and price
        //yRight += 10;
        CGSize nameSize = [self sizeForString:self.product.name constrainedTo:CGSizeMake(RIGHT_COL_TEXT_WIDTH, FLT_MAX) withFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:18]];
        self.nameLabel.text = self.product.name;
        self.nameLabel.frame = CGRectMake(self.nameLabel.frame.origin.x, self.nameLabel.frame.origin.y, self.nameLabel.frame.size.width, MIN(ceil(nameSize.height), PRODUCT_NAME_MAX_HEIGHT));
        //yRight += self.nameLabel.frame.size.height;
        self.priceLabel.text = self.product.finalPriceFormatted;
        //self.priceLabel.frame = CGRectMake(self.priceLabel.frame.origin.x, yRight, self.priceLabel.frame.size.width, self.priceLabel.frame.size.height);
        
        y += IMAGE_SIZE;
    } else {
        // Header layout for no image
        /*
         Name line 1...Price
         Name line 2.....Qty
         */
        CGSize nameSize = [self sizeForString:self.product.name constrainedTo:CGSizeMake(NOIMAGE_LEFT_WIDTH, FLT_MAX) withFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:18]];
        self.nameLabel.text = self.product.name;
        self.nameLabel.frame = CGRectMake(15, self.nameLabel.frame.origin.y, NOIMAGE_LEFT_WIDTH, ceil(nameSize.height));
        self.priceLabel.text = self.product.finalPriceFormatted;
        self.priceLabel.frame = CGRectMake(15+PRODUCT_CONTENT_WIDTH-NOIMAGE_RIGHT_WIDTH, self.nameLabel.frame.origin.y, NOIMAGE_RIGHT_WIDTH, self.priceLabel.frame.size.height);
        self.quantityLabel.frame = CGRectMake(self.quantityLabel.frame.origin.x, self.priceLabel.frame.origin.y+self.priceLabel.frame.size.height+4, self.quantityLabel.frame.size.width, self.quantityLabel.frame.size.height);
        self.quantityStepper.frame = CGRectMake(self.quantityStepper.frame.origin.x, self.priceLabel.frame.origin.y+self.priceLabel.frame.size.height, self.quantityStepper.frame.size.width, self.quantityStepper.frame.size.height);
        float rightHeight = self.priceLabel.frame.size.height+self.quantityStepper.frame.size.height;
        y += MAX(self.nameLabel.frame.size.height, rightHeight);
    }
    y += 20;
    
    // Description
    CGSize descriptionSize = [self sizeForString:self.product.description constrainedTo:CGSizeMake(PRODUCT_CONTENT_WIDTH, FLT_MAX) withFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:14]];
    self.descriptionLabel.text = self.product.description;
    self.descriptionLabel.frame = CGRectMake(self.descriptionLabel.frame.origin.x, y-yHeader, self.descriptionLabel.frame.size.width, ceil(descriptionSize.height));
    
    // Tiered prices
    y += self.descriptionLabel.frame.size.height;
    [[self.tieredPrices subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
    self.tieredPrices.frame = CGRectMake(self.tieredPrices.frame.origin.x, y-yHeader, self.tieredPrices.frame.size.width, 18*[self.product.tieredPriceDescriptions count]);
    float tieredPriceY = 0;
    for (NSString *p in self.product.tieredPriceDescriptions) {
        UILabel *tieredPriceLabel = [[UILabel alloc] initWithFrame:CGRectMake(2, 2, 0, 0)];
        // Label
        tieredPriceLabel.text = p;
        tieredPriceLabel.backgroundColor = [UIColor clearColor];
        tieredPriceLabel.font = [UIFont systemFontOfSize:10];
        tieredPriceLabel.textColor = [UIColor whiteColor];
        [tieredPriceLabel sizeToFit];
        UIView *tieredPriceView = [[UIView alloc] initWithFrame:CGRectMake(0, tieredPriceY+4, tieredPriceLabel.frame.size.width+4, tieredPriceLabel.frame.size.height+4)];
        tieredPriceView.backgroundColor = [UIColor orangeColor];
        [tieredPriceView addSubview:tieredPriceLabel];
        [self.tieredPrices addSubview:tieredPriceView];
        tieredPriceY += 18;
    }
    
    y += self.tieredPrices.frame.size.height + 10;
    
    if (!self.product.quantity) {
        self.product.quantity = 1;
    }
    self.quantityLabel.text = [NSString stringWithFormat:@"%d", (int)self.product.quantity];
    self.quantityStepper.value = (double)self.product.quantity;
    [self.quantityStepper addTarget:self action:@selector(changedQuantity:) forControlEvents:UIControlEventValueChanged];
    
    // Resize header to fit contents
    self.headerView.frame = CGRectMake(self.headerView.frame.origin.x, self.headerView.frame.origin.y, self.headerView.frame.size.width, y-yHeader);
    
    // Product options
    CGFloat optionsHeight = [self.product.productOptions count] > 0 ? 44*[self.product.productOptions count] + 55 : 0;
    self.optionsTable.frame = CGRectMake(self.optionsTable.frame.origin.x, y, self.optionsTable.frame.size.width, optionsHeight);
    y += optionsHeight;
    
    // Add to cart
    y += 10;
    self.addToCartButton.frame = CGRectMake(self.addToCartButton.frame.origin.x, y, self.addToCartButton.frame.size.width, self.addToCartButton.frame.size.height);
    self.addToCartButton.backgroundColor = [UIColor colorWithRed:72/255.0f green:217/255.0f blue:116/255.0f alpha:1.0];
    [self.addToCartButton.layer setMasksToBounds:YES];
    [self.addToCartButton.layer setCornerRadius:3.0f];
    self.addToCartButton.titleLabel.textAlignment = NSTextAlignmentCenter;
    self.addToCartButton.titleLabel.textColor = [UIColor whiteColor];
    self.addToCartButton.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:18.0f];
    [self.addToCartButton setTitle:NSLocalizedString(@"Add to Cart", nil) forState:UIControlStateNormal];
    [self.addToCartButton setEnabled:openForOrders];
    self.addToCartButton.alpha = openForOrders ? 1.0 : 0.5;
    [self.addToCartButton addTarget:self action:@selector(addToCart:) forControlEvents:UIControlEventTouchUpInside];
    
    // Cart items
    y += self.addToCartButton.frame.size.height;
    y += 10;
    // Removed cart items
    self.itemsView.hidden = YES;
    /*
    [[self.itemsView subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
    self.itemsView.frame = CGRectMake(15, y, self.itemsView.frame.size.width, [self heightForItems:self.items]);
    float itemY = 0;
    for (int i=0; i<[self.items count]; i++) {
        ZECartItem *item = [self.items objectAtIndex:i];
        ZEProductCartItemView *itemView = [[[NSBundle mainBundle] loadNibNamed:@"ProductCartItemView" owner:self options:nil] objectAtIndex:0];
        float height = (item.quoteItem && ![item.quoteItem.optionsSummary isEqualToString:@""]) ? 73 : 49;
        itemView.frame = CGRectMake(0, itemY, 305, height);
        itemView.quantityLabel.text = [NSString stringWithFormat:@"%d", item.quantity];
        [itemView.quantityStepper setValue:(double)item.quantity];
        [itemView.quantityStepper setTag:i];
        [itemView.quantityStepper addTarget:self action:@selector(changeCartItemQuantity:) forControlEvents:UIControlEventValueChanged];
        [itemView attachQuantityStepper];
        if (item.quoteItem) {
            itemView.titleLabel.text = item.quoteItem.optionsSummary;
            itemView.priceLabel.text = item.quoteItem.priceFormatted;
        }
        [self.itemsView addSubview:itemView];
        itemY += height;
    }
    y += self.itemsView.frame.size.height;
     */
    
    self.scrollView.contentSize = CGSizeMake(320, y);
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (CGSize)sizeForString:(NSString *)string constrainedTo:(CGSize)size withFont:(UIFont *)font
{
	NSMutableDictionary *atts = [[NSMutableDictionary alloc] init];
	[atts setObject:font forKey:NSFontAttributeName];
	CGRect rect = [string boundingRectWithSize:size options:NSStringDrawingUsesLineFragmentOrigin attributes:atts context:nil];
	return rect.size;
}

- (CGFloat)additionalHeight
{
    float rightSideHeight = 69;
    float optionsHeight = 0;
    for (ZEProductOption *option in self.product.productOptions) {
        if ([option.selectionType isEqualToString:@"single"]) {
            optionsHeight += 35; // Single selct = one button
        } else {
            optionsHeight += 35*[option.values count]; // Multiple select = one button for each value
        }
    }
    return MAX(rightSideHeight, optionsHeight);
}

- (CGFloat)heightForItems:(NSArray *)items
{
    float itemsSize = 0;
    for (ZECartItem *item in items) {
        itemsSize += (item.quoteItem && ![item.quoteItem.optionsSummary isEqualToString:@""]) ? 73 : 49;
    }
    return itemsSize;
}

- (void)addToCart:(id)sender
{
    // Validate options
    for (ZEProductOption *option in self.product.productOptions) {
        if ([option.required boolValue] && option.selectedIndex == -1) {
            [SVProgressHUD showErrorWithStatus:[NSString stringWithFormat:NSLocalizedString(@"%@ is required", @"Error message when a product option is required"), option.name]];
            return;
        }
    }
    
    // Create cart item and add to cart
    ZECartItem *item = [self.product createCartItem];
    item.productName = self.product.name;
    ZEAppDelegate *delegate = [[UIApplication sharedApplication] delegate];
    ZECart *cart = [delegate.cartManager getCartForLocation:self.location bar:self.bar group:self.group event:self.event];
    [cart addItem:item];
    
    //[SVProgressHUD showWithStatus:NSLocalizedString(@"Adding to cart", nil)];
    //[self loadQuoteAndUpdateItemsAnimated:YES];
    
    // Go to product page
    [self.delegate productViewDidAddToCart];
}

-(void)changedQuantity:(UIStepper *)sender
{
    self.quantityLabel.text = [NSString stringWithFormat:@"%d", (int)sender.value];
    self.product.quantity = (int)sender.value;
}

- (IBAction)changeCartItemQuantity:(UIStepper *)sender {
    ZECartItem *cartItem = [self.items objectAtIndex:sender.tag];
    ZEAppDelegate *delegate = [[UIApplication sharedApplication] delegate];
    ZECart *cart = [delegate.cartManager getCartForLocation:self.location bar:self.bar group:self.group event:self.event];
    if (cartItem) {
        if ((int)[sender value] == 0) {
            // Remove item
            [cart removeItem:cartItem];
            RKObjectManager *objectManager = [RKObjectManager sharedManager];
            [objectManager cancelAllObjectRequestOperationsWithMethod:RKRequestMethodPOST matchingPathPattern:@"quote"];
            [self reloadView];
        } else {
            // Update item
            cartItem.quantity = [sender value];
            [self loadQuoteAndUpdateItemsAnimated:NO];
        }
        [self.cartButton updateAnimated:NO];
    }
}

- (void)loadQuoteAndUpdateItemsAnimated:(BOOL)animated
{
    ZEAppDelegate *delegate = [[UIApplication sharedApplication] delegate];
    ZECart *cart = [delegate.cartManager getCartForLocation:self.location bar:self.bar group:self.group event:self.event];
    // Cancel existing requests
    RKObjectManager *objectManager = [RKObjectManager sharedManager];
    [objectManager cancelAllObjectRequestOperationsWithMethod:RKRequestMethodPOST matchingPathPattern:@"quote"];
    // Load quote from server
    [objectManager postObject:cart path:@"quote" parameters:nil success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
        [SVProgressHUD dismiss];
        // Quote received
        ZEQuote *quote = [mappingResult firstObject];
        [cart attachQuote:quote];
        // Remove invalid items
        [cart validateQuoteItems];
        
        [self reloadView];
        [self.cartButton updateAnimated:animated];
        float offset = self.scrollView.contentSize.height - self.scrollView.bounds.size.height + self.scrollView.contentInset.bottom;
        if (offset > 0) {
            CGPoint offsetPoint = CGPointMake(0, offset);
            [self.scrollView setContentOffset:offsetPoint animated:YES];
        }
        
    } failure:^(RKObjectRequestOperation *operation, NSError *error) {
        [SVProgressHUD dismiss];
        [self.cartButton updateAnimated:NO];
        // Error
        if (error.code == -999) {
            // Don't show error msg for cancelled requests
            return;
        }
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil)
                                                        message:[error localizedDescription]
                                                       delegate:nil
                                              cancelButtonTitle:NSLocalizedString(@"OK", nil)
                                              otherButtonTitles:nil];
        [alert show];
    }];
}

- (void)goToCart:(id)sender
{
    [self performSegueWithIdentifier:@"ViewCart" sender:sender];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"ViewCart"]) {
        ZECartViewController *cartViewController = [segue destinationViewController];
        cartViewController.delegate = (ZETabBarController *)self.navigationController.tabBarController;
        cartViewController.group = [self.group copy];
        cartViewController.location = self.location;
        cartViewController.bar = self.bar;
        cartViewController.event = self.event;
    } else if ([[segue identifier] isEqualToString:@"SelectOption"]) {
        UINavigationController *navController = [segue destinationViewController];
        ZEOptionViewController *optionController = [navController.viewControllers objectAtIndex:0];
        optionController.product = self.product;
        optionController.optionIndex = [self.optionsTable indexPathForSelectedRow].row;
        optionController.goToNext = NO;
        optionController.delegate = self;
    }
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return NSLocalizedString(@"Options", nil);
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [self.product.productOptions count] > 0 ? 1 : 0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.product.productOptions count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"OptionCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    cell.textLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:15.0];
    cell.detailTextLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:15.0];
    cell.detailTextLabel.adjustsFontSizeToFitWidth = YES;
    cell.detailTextLabel.minimumScaleFactor = 0.7;
    
    ZEProductOption *option = [self.product.productOptions objectAtIndex:indexPath.row];
    cell.textLabel.text = option.name;
    
    if ([option.selectionType isEqualToString:@"single"]) {
        if (option.selectedIndex == -1) {
            cell.detailTextLabel.text = NSLocalizedString(@"Choose", nil);
        } else {
            cell.detailTextLabel.text = ((ZEProductOptionValue *)[option.values objectAtIndex:option.selectedIndex]).labelWithPrice;
        }
    } else {
        NSPredicate *selectedPredicate = [NSPredicate predicateWithFormat:@"selected == YES"];
        NSArray *selectedOptionLabels = [[option.values filteredArrayUsingPredicate:selectedPredicate] valueForKey:@"labelWithPrice"];
        if ([selectedOptionLabels count] > 0) {
            cell.detailTextLabel.text = [selectedOptionLabels componentsJoinedByString:@", "];
        } else {
            cell.detailTextLabel.text = NSLocalizedString(@"Choose", nil);
        }
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}

- (void)selectOptionControllerDidSelectOptions
{
    [self dismissViewControllerAnimated:YES completion:nil];
    [self.optionsTable reloadData];
}

- (void)selectOptionControllerDidCancel
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
