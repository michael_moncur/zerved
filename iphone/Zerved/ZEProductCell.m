//
//  ZEProductCell.m
//  Zerved
//
//  Created by Anders Rasmussen on 07/08/13.
//
//

#import "ZEProductCell.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "QuartzCore/CAGradientLayer.h"
#import "ZEProductOption.h"
#import "ZEProductOptionValue.h"
#import "ActionSheetStringPicker.h"
#import "ZEProductOptionToggleButton.h"
#import "ZEProductOptionDropdownButton.h"
#import "SVProgressHUD.h"
#import "ZECartItem.h"
#import "ZEProductCartItemView.h"
#import "NSString+Additions.h"

@implementation ZEProductCell

@synthesize product = _product, cart = _cart;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setProduct:(ZEProduct *)product andCart:(ZECart *)cart andProductController:(ZEProductsViewController *)productController andOpen:(BOOL)open
{
    _product = product;
    _cart = cart;
    self.items = [self.cart itemsForProduct:product.key];
    
    float y = 10;
    // Name and price
    self.nameLabel.text = product.name;
    self.priceLabel.text = product.finalPriceFormatted;

    CGSize nameSize = [product.name sizeWithFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:18] constrainedTo:CGSizeMake(PRODUCT_NAME_WIDTH, FLT_MAX)];
    self.nameLabel.frame = CGRectMake(self.nameLabel.frame.origin.x, y, self.nameLabel.frame.size.width, ceil(nameSize.height));
    
    [self.addToCartButton setEnabled:open];
    
    // Description
    y += self.nameLabel.frame.size.height;
    //float descriptionSize = [product.description isEqualToString:@""] ? 0 : 16;
    self.descriptionLabel.text = self.product.description;
    self.descriptionLabel.frame = CGRectMake(self.descriptionLabel.frame.origin.x, y, self.descriptionLabel.frame.size.width, self.descriptionLabel.frame.size.height);
    
    // Tiered prices
    y += self.descriptionLabel.frame.size.height;
    [[self.tieredPrices subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
    self.tieredPrices.frame = CGRectMake(self.tieredPrices.frame.origin.x, y, self.tieredPrices.frame.size.width, 18*[product.tieredPriceDescriptions count]);
    float tieredPriceY = 0;
    for (NSString *p in product.tieredPriceDescriptions) {
        UILabel *tieredPriceLabel = [[UILabel alloc] initWithFrame:CGRectMake(2, 2, 0, 0)];
        // Label
        tieredPriceLabel.text = p;
        tieredPriceLabel.backgroundColor = [UIColor clearColor];
        tieredPriceLabel.font = [UIFont systemFontOfSize:10];
        tieredPriceLabel.textColor = [UIColor whiteColor];
        [tieredPriceLabel sizeToFit];
        UIView *tieredPriceView = [[UIView alloc] initWithFrame:CGRectMake(0, tieredPriceY+4, tieredPriceLabel.frame.size.width+4, tieredPriceLabel.frame.size.height+4)];
        tieredPriceView.backgroundColor = [UIColor orangeColor];
        [tieredPriceView addSubview:tieredPriceLabel];
        [self.tieredPrices addSubview:tieredPriceView];
        y += 18;
        tieredPriceY += 18;
    }
    
    // Cart items
    y += 10;
    [[self.itemsView subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
    self.itemsView.frame = CGRectMake(15, y, self.itemsView.frame.size.width, [ZEProductCell heightForItems:self.items]);
    float itemY = 0;
    for (int i=0; i<[self.items count]; i++) {
        ZECartItem *item = [self.items objectAtIndex:i];
        ZEProductCartItemView *itemView = [[[NSBundle mainBundle] loadNibNamed:@"ProductCartItemView" owner:self options:nil] objectAtIndex:0];
        float height = (item.quoteItem && ![item.quoteItem.optionsSummary isEqualToString:@""]) ? 73 : 49;
        itemView.frame = CGRectMake(0, itemY, 305, height);
        itemView.quantityLabel.text = [NSString stringWithFormat:@"%d", item.quantity];
        [itemView.quantityStepper setValue:(double)item.quantity];
        [itemView.quantityStepper setTag:i];
        [productController attachQuantityStepper:itemView.quantityStepper];
        [itemView attachQuantityStepper];
        if (item.quoteItem) {
            itemView.titleLabel.text = item.quoteItem.optionsSummary;
            itemView.priceLabel.text = item.quoteItem.priceFormatted;
        }
        [self.itemsView addSubview:itemView];
        itemY += height;
    }
}

+ (CGFloat)heightForProduct:(ZEProduct *)product andCart:(ZECart *)cart
{
    float tieredPricesHeight = [product.tieredPriceDescriptions count]*18;
    //float descriptionSize = [product.description isEqualToString:@""] ? 0 : 16;
    CGSize nameSize = [product.name sizeWithFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:18] constrainedTo:CGSizeMake(164, FLT_MAX)];
    float itemsSize = [self heightForItems:[cart itemsForProduct:product.key]];
    return 10+ceil(nameSize.height)+PRODUCT_DESCRIPTION_HEIGHT+10+tieredPricesHeight+itemsSize;
}

+ (CGFloat)heightForItems:(NSArray *)items
{
    float itemsSize = 0;
    for (ZECartItem *item in items) {
        itemsSize += (item.quoteItem && ![item.quoteItem.optionsSummary isEqualToString:@""]) ? 73 : 49;
    }
    return itemsSize;
}

@end
