//
//  ZECartItem.m
//  Zerved
//
//  Created by Anders Rasmussen on 28/05/13.
//
//

#import "ZECartItem.h"
#import "ZECartItemOptionValue.h"

@implementation ZECartItem

- (id)initWithProductKey:(NSString *)productKey quantity:(int)quantity options:(NSMutableArray *)options
{
    self = [super init];
    if (self) {
        self.productKey = productKey;
        self.quantity = quantity;
        self.options = options;
    }
    return self;
}

- (void)addQuantity:(int)quantity
{
    self.quantity += quantity;
}

- (BOOL)isEqual:(id)anObject
{
    if (anObject == self)
        return YES;
    if (!anObject || ![anObject isKindOfClass:[self class]])
        return NO;
    ZECartItem *item = (ZECartItem *)anObject;
    return ([self.productKey isEqualToString:item.productKey] && [self.options isEqualToArray:item.options]);
}

- (id)jsonObject
{
    NSMutableDictionary *data = [[NSMutableDictionary alloc] initWithObjectsAndKeys:
                                 self.productKey, @"product",
                                 self.quantity, @"qty", nil];
    NSMutableArray *options = [[NSMutableArray alloc] init];
    for (ZECartItemOptionValue *optionValue in self.options) {
        [options addObject:[optionValue jsonObject]];
    }
    [data setObject:options forKey:@"options"];
    return data;
}

- (BOOL)hasValue:(NSInteger)selectedIndex forOptionId:(NSString *)optionId
{
    for (ZECartItemOptionValue *optionValue in self.options) {
        if ([optionValue.optionId isEqualToString:optionId] && optionValue.selectedIndex == selectedIndex) {
            return YES;
        }
    }
    return NO;
}

- (BOOL)hasValueForOptionId:(NSString *)optionId
{
    for (ZECartItemOptionValue *optionValue in self.options) {
        if ([optionValue.optionId isEqualToString:optionId]) {
            return YES;
        }
    }
    return NO;
}

- (void)clearOptionId:(NSString *)optionId
{
    NSMutableArray *destroyedOptionValues = [[NSMutableArray alloc] init];
    for (ZECartItemOptionValue *optionValue in self.options) {
        if ([optionValue.optionId isEqualToString:optionId]) {
            [destroyedOptionValues addObject:optionValue];
        }
    }
    [self.options removeObjectsInArray:destroyedOptionValues];
}

- (void)removeOptionWithOptionId:(NSString *)optionId value:(NSInteger)selectedIndex
{
    ZECartItemOptionValue *toRemove = nil;
    for (ZECartItemOptionValue *optionValue in self.options) {
        if ([optionValue.optionId isEqualToString:optionId] && optionValue.selectedIndex == selectedIndex) {
            toRemove = optionValue;
        }
    }
    [self.options removeObject:toRemove];
}

- (NSString *)description
{
    NSString *desc = [NSString stringWithFormat:@"%d x %@", self.quantity, self.productKey];
    for (ZECartItemOptionValue *option in self.options) {
        desc = [desc stringByAppendingFormat:@" %@", option];
    }
    return desc;
}

@end
