//
//  ZEQuote.h
//  Zerved
//
//  Created by Anders Rasmussen on 30/05/13.
//
//

#import <Foundation/Foundation.h>
#import "ZEQuoteItem.h"
#import "ZELocation.h"
#import "ZEBar.h"
#import "ZEQuoteFee.h"

@interface ZEQuote : NSObject

@property (nonatomic, strong) NSNumber *subtotal;
@property (nonatomic, strong) NSString *subtotalFormatted;
@property (nonatomic, strong) NSNumber *tip;
@property (nonatomic, strong) NSString *tipFormatted;
@property (nonatomic, strong) NSNumber *total;
@property (nonatomic, strong) NSString *totalFormatted;
@property (nonatomic, strong) NSNumber *totalTax;
@property (nonatomic, strong) NSString *totalTaxFormatted;
@property (nonatomic, strong) NSNumber *totalInclTax;
@property (nonatomic, strong) NSString *totalInclTaxFormatted;
@property (nonatomic, strong) NSNumber *totalLoyaltyDiscountInclTax;
@property (nonatomic, strong) NSString *totalLoyaltyDiscountInclTaxFormatted;
@property (nonatomic, strong) NSMutableArray *items;
@property (nonatomic, strong) ZELocation *location;
@property (nonatomic, strong) ZEBar *bar;
@property (nonatomic, strong) NSString *currency;
@property (nonatomic, strong) NSMutableArray *fees;
@property (nonatomic, copy) NSNumber *tipEnabled;
@property (nonatomic, copy) NSNumber *hasLoyaltyProgram;
@property (nonatomic, strong) NSString *loyaltyCode;
@property (nonatomic, strong) NSNumber *loyaltyCodeValid;
@property (nonatomic, strong) NSString *loyaltyName;
@property (nonatomic, strong) NSString *customAllowedPaymentMethods;

@end
