//
//  ZEMerchantLocationsViewController.m
//  Zerved
//
//  Created by Anders Rasmussen on 05/08/13.
//
//  List locations owned by merchant
//

#import "ZEMerchantLocationsViewController.h"
#import "SVProgressHUD.h"
#import "QuartzCore/CAGradientLayer.h"
#import "ZEEmptyView.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "ZELocationViewController.h"
#import "ZELocationCell.h"

@interface ZEMerchantLocationsViewController ()

@end

@implementation ZEMerchantLocationsViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.title = NSLocalizedString(@"My Locations", nil);
    if (nil == self.locationManager) {
        self.locationManager = [[CLLocationManager alloc] init];
    }
    [self reload];
}

- (void)viewDidUnload {
    [self setLocationManager:nil];
    [super viewDidUnload];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)reload
{
    NSString *merchantId = [[NSUserDefaults standardUserDefaults] stringForKey:@"merchant_id"];
    if ([merchantId isEqualToString:@""]) {
        return;
    }
    [SVProgressHUD showWithStatus:NSLocalizedString(@"Loading", nil)];
    RKObjectManager *objectManager = [RKObjectManager sharedManager];
    [objectManager getObjectsAtPath:[NSString stringWithFormat:@"merchants/%@/locations", merchantId]
                         parameters:nil
                            success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
                                // Locations loaded
                                [SVProgressHUD dismiss];
                                self.locations = [mappingResult array];
                                [self reloadTableView];
                            }
                            failure:^(RKObjectRequestOperation *operation, NSError *error) {
                                // Failed to load locations
                                [SVProgressHUD dismiss];
                                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil)
                                                                                message:[error localizedDescription]
                                                                               delegate:nil
                                                                      cancelButtonTitle:NSLocalizedString(@"OK", nil)
                                                                      otherButtonTitles:nil];
                                [alert show];
                            }];
}

- (void)reloadTableView
{
    if(self.isViewLoaded) {
        [self.tableView reloadData];
        if ([self.locations count]) {
            [self.tableView setTableHeaderView:nil];
        } else {
            ZEEmptyView *emptyView = [[[NSBundle mainBundle] loadNibNamed:@"EmptyTableView" owner:self options:nil] objectAtIndex:0];
            [emptyView setupViewForTableView:self.tableView text:NSLocalizedString(@"No locations found", nil)];
            [self.tableView setTableHeaderView:emptyView];
        }
    }
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return ([self.locations count] > 0) ? 1 : 0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.locations count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"LocationCell";
    ZELocationCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    // Add location text
    ZELocation* location = [self.locations objectAtIndex:indexPath.row];
    cell.titleLabel.text = [location name];
    // Show distance if there's a recent last known location (5 minutes)
    CLLocation *lastLocation = self.locationManager.location;
    if (lastLocation && -[lastLocation.timestamp timeIntervalSinceNow] < 5*60) {
        cell.subtitleLabel.text = [location describeDistanceTo:lastLocation];
    } else {
        cell.subtitleLabel.text = @"";
    }
    // Merchant logo
    // On old iPhones the images make the app crash with memory warning
    if (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1) {
        [cell.locationImage sd_setImageWithURL:[NSURL URLWithString:location.merchant.imageUrl] placeholderImage:[UIImage imageNamed:@"bar_logo_dummy.png"] options:SDWebImageRefreshCached];
    }
    return cell;
}

#pragma mark - Table view delegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Larger cells (default is approx. 45)
    return 70;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (section == 0) {
        UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 60)];
        [headerView setBackgroundColor:[UIColor clearColor]];
        CAGradientLayer *grad = [CAGradientLayer layer];
        grad.frame = headerView.bounds;
        grad.colors = [NSArray arrayWithObjects:(id)[[UIColor whiteColor] CGColor], (id)[[UIColor colorWithRed:0.82 green:0.82 blue:0.82 alpha:1.0] CGColor], nil];
        [headerView.layer insertSublayer:grad atIndex:0];
        
        UILabel *headerLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 4, 300, 18)];
        headerLabel.backgroundColor = [UIColor clearColor];
        headerLabel.textAlignment = NSTextAlignmentCenter;
        headerLabel.font = [UIFont boldSystemFontOfSize:14];
        headerLabel.text = NSLocalizedString(@"Menu Preview", nil);
        [headerView addSubview:headerLabel];
        
        UILabel *subtitle = [[UILabel alloc] initWithFrame:CGRectMake(10, 22, 300, 34)];
        subtitle.backgroundColor = [UIColor clearColor];
        subtitle.textAlignment = NSTextAlignmentCenter;
        subtitle.font = [UIFont systemFontOfSize:14];
        subtitle.textColor = [UIColor grayColor];
        subtitle.numberOfLines = 2;
        subtitle.minimumScaleFactor = 0.7;
        subtitle.adjustsFontSizeToFitWidth = YES;
        subtitle.text = NSLocalizedString(@"Showing the menu of the merchant ID configured in the settings.", nil);
        [headerView addSubview:subtitle];
        
        return headerView;
    }
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 0) {
        return 60;
    }
    return 0;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"ViewLocation"]) {
        // Go to categories
        ZELocationViewController *destinationController = [segue destinationViewController];
        ZELocation *location = [self.locations objectAtIndex:[self.tableView indexPathForSelectedRow].row];
        destinationController.locationKey = location.key;
        destinationController.title = location.name;
        destinationController.showAllBars = YES;
    }
}

@end
