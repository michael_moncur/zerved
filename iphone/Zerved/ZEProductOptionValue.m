//
//  ZEProductOptionValue.m
//  Zerved
//
//  Created by Anders Rasmussen on 06/06/13.
//
//

#import "ZEProductOptionValue.h"

@implementation ZEProductOptionValue

- (NSString *)labelWithPrice
{
    if ([self.priceLabel isEqualToString:@""]) {
        return self.label;
    } else {
        return [NSString stringWithFormat:@"%@ (%@)", self.label, self.priceLabel];
    }
}

@end
