//
//  ZECartViewController.m
//  Zerved
//
//  Created by Anders Rasmussen on 30/05/13.
//
//  Displays the shopping cart (items, totals, tip, terms, service type)
//  for a certain location/bar/event.
//  The cart view can place the order (if payment is available) or go to
//  the payment screen if payment is needed.
//

#import "ZECartViewController.h"
#import "ZEAppDelegate.h"
#import "SVProgressHUD.h"
#import "ZECartItemCell.h"
#import "ZEEmptyView.h"
#import "ZETextFieldLabelCell.h"
#import "ZETermsCell.h"
#import "ZETipCell.h"
#import "ZEProductOptionsViewController.h"
#import "ZEFavouritesViewController.h"
#import "ZEConsumerManager.h"
#import "ZEConsumer.h"
#import "ZEEpayViewController.h"
#import "ZETabBarController.h"
#import "ZEPlaceOrderResult.h"
#import "ZE4TPhoneViewController.h"
#import "ZECartItemOptionValue.h"
#import "ZEFavourite.h"
#import "ZEProduct.h"
#import "NSData+Additions.h"
#import "ZEServiceViewController.h"
#import "ZEEventSummaryView.h"
#import "MobilePayManager.h"

@interface ZECartViewController ()

@end

@implementation ZECartViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Collapse all items
    for (ZECartItem *item in [self getCart].items) {
        item.isExpanded = NO;
    }
    
    // Activity indicator
    self.activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    [self.activityIndicator startAnimating];
    
    self.allowedPaymentMethodButtons = [[NSMutableArray alloc] init];

    // Place order button
    self.footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 80)];
    self.placeOrderButton = [[UIButton alloc] initWithFrame:CGRectMake(10, 20, 300, 40)];
    self.placeOrderButton.backgroundColor = [UIColor colorWithRed:72/255.0f green:217/255.0f blue:116/255.0f alpha:1.0];
    [self.placeOrderButton.layer setMasksToBounds:YES];
    [self.placeOrderButton.layer setCornerRadius:3.0f];
    self.placeOrderButton.titleLabel.textAlignment = NSTextAlignmentCenter;
    self.placeOrderButton.titleLabel.textColor = [UIColor whiteColor];
    self.placeOrderButton.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:18.0f];
    [self.placeOrderButton setTitle:NSLocalizedString(@"Place Order", @"Place order button in cart") forState:UIControlStateNormal];
    [self.placeOrderButton addTarget:self action:@selector(placeOrder:) forControlEvents:UIControlEventTouchUpInside];
    [self.footerView addSubview:self.placeOrderButton];
    
    // Service popup
    if (([[self getCart].service isEqualToString:@""] && [self.group isEqualToString:@"menu"]) || ([[self getCart].service isEqualToString:@"table"] && [[self getCart].tableNumber isEqualToString:@""])) {
        [self performSegueWithIdentifier:@"EditService" sender:self];
    }
    
    self.tabIndex = self.tabBarController.selectedIndex;
}

- (void)viewDidUnload {
    [self setWaitingTimeLabel:nil];
    [self setPlaceOrderButton:nil];
    [self setFooterView:nil];
    [super viewDidUnload];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self reload];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
    // Refresh every min
    self.timer = [NSTimer scheduledTimerWithTimeInterval:60 target:self selector:@selector(reload) userInfo:nil repeats:YES];
    [super viewWillAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [self.timer invalidate];
    [super viewWillDisappear:animated];
}

- (void)reload
{
    // Disallow place order while loading
    self.placeOrderButton.enabled = NO;
    // Login
    ZEAppDelegate *delegate = [[UIApplication sharedApplication] delegate];
    delegate.consumer.delegate = self;
    [delegate.consumer authenticateFromView:self];

}

- (void)didAuthenticate
{
    // Cancel any existing requests to prevent multiple loads at the same time
    RKObjectManager *objectManager = [RKObjectManager sharedManager];
    [objectManager cancelAllObjectRequestOperationsWithMethod:RKRequestMethodPOST matchingPathPattern:@"quote"];
    
    // Don't load quote if no items in cart (prevents reload after order placed)
    if ([[self getCart] countRows] == 0) {
        self.navigationItem.titleView = nil;
        [self reloadTableView];
        return;
    }
    
    // Load quote
    self.navigationItem.titleView = self.activityIndicator;
    if (!self.event) {
        for(int i=0; i < [[self getCart] countRows]; i++) {
            ZECartItemCell *cell = (ZECartItemCell*)[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForItem:i inSection:0]];
            [cell.spinner setHidden:NO];
            [cell.favouriteButton setHidden:YES];
        }
    }
    [objectManager postObject:[self getCart] path:@"quote" parameters:nil success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
        // Quote received
        self.quote = [mappingResult firstObject];
        [[self getCart] attachQuote:self.quote];
        if (self.quote.bar) {
            self.bar = self.quote.bar;
        }
        // Remove invalid items
        NSInteger invalidItems = [[self getCart] validateQuoteItems];
        
        // Load consumer's favourites
        if (!self.event) {
        ZEConsumerManager *consumerManager = ((ZEAppDelegate *)[[UIApplication sharedApplication] delegate]).consumer;
        RKObjectManager *objectManager = [RKObjectManager sharedManager];
        [objectManager getObjectsAtPath:[NSString stringWithFormat:@"consumers/%@/favourites", consumerManager.consumerKey]
                             parameters:nil
                                success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
                                    [SVProgressHUD dismiss];
                                    NSMutableArray *favourites = [[NSMutableArray alloc] initWithArray:[mappingResult array]];
                                  
                                    for(int i=0; i < [[self getCart] countRows]; i++){
                                        ZECartItem *item = [[self getCart].items objectAtIndex:i];
                                        
                                        // Creates a fake favourite to compare
                                        ZEFavourite *testFavourite = [[ZEFavourite alloc] initWithProduct:[[ZEProduct alloc] initWithKey:item.productKey]
                                                                                               andOptions:item.options];
                                        
                                        // checks if the current item is a consumer's favorite item
                                        ((ZECartItem *)[[self getCart].items objectAtIndex:i]).isFavourite = NO;
                                        ((ZECartItem *)[[self getCart].items objectAtIndex:i]).favouriteId = [NSNumber numberWithInt:0];
                                        for(int j=0; j < [favourites count]; j++)
                                        {
                                            if([testFavourite isEqual:[favourites objectAtIndex:j]]){
                                                // Update the cart item if it's a consumer's favourite item
                                                ((ZECartItem *)[[self getCart].items objectAtIndex:i]).isFavourite = YES;
                                                ((ZECartItem *)[[self getCart].items objectAtIndex:i]).favouriteId = ((ZEFavourite *)[favourites objectAtIndex:j]).favouriteId;
                                                ((ZECartItem *)[[self getCart].items objectAtIndex:i]).favourite = [favourites objectAtIndex:j];
                                                break; // No need to go further
                                            }
                                        }
                                    }
                                    // Update view
                                    [self reloadTableView];
									return;
                                }
                                failure:^(RKObjectRequestOperation *operation, NSError *error) {
                                    [SVProgressHUD dismiss];
                                    [SVProgressHUD showErrorWithStatus:NSLocalizedString(@"Error", nil)];
                                    return;
                                }];
        }
        // Update view
        [self reloadTableView];
		// Alert if loyalty code is invalid
		if (!self.quote.loyaltyCodeValid.boolValue && self.quote.loyaltyCode.length > 0)
		{
			NSString *loyaltyTitle = NSLocalizedString(@"Loyalty number", @"Title for loyalty code");
			[SVProgressHUD showErrorWithStatus:[NSString stringWithFormat:NSLocalizedString(@"%@ does not exist", @"Title for alert when loyalty code is invalid"), loyaltyTitle]];
		}
        // Alert if invalid items were removed
        if (invalidItems > 0) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:[NSString stringWithFormat:NSLocalizedString(@"%d item(s) have been removed from your cart because the menu has changed.", nil), invalidItems] delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
            [alert show];
        }
        // Allow place order
        self.placeOrderButton.enabled = YES;
        
    } failure:^(RKObjectRequestOperation *operation, NSError *error) {
        // Error
        if (error.code == -999) {
            // Don't show error msg for cancelled requests
            return;
        }
        self.navigationItem.titleView = nil;
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil)
                                                        message:[error localizedDescription]
                                                       delegate:nil
                                              cancelButtonTitle:NSLocalizedString(@"OK", nil)
                                              otherButtonTitles:nil];
        [alert show];
    }];
}

- (void)reloadTableView
{
    [self updateFields];
    if(self.isViewLoaded) {
        self.navigationItem.titleView = nil;
        [self.tableView reloadData];
        if ([[self getCart] countRows]) {
            if (self.event) {
                ZEEventSummaryView *eventSummaryView = [[[NSBundle mainBundle] loadNibNamed:@"EventSummaryView" owner:self options:nil] objectAtIndex:0];
                [eventSummaryView setEvent:self.event];
                [eventSummaryView setWhiteBackground];
                [self.tableView setTableHeaderView:eventSummaryView];
            } else {
                [self.tableView setTableHeaderView:nil];
            }
        } else {
            ZEEmptyView *emptyView = [[[NSBundle mainBundle] loadNibNamed:@"EmptyTableView" owner:self options:nil] objectAtIndex:0];
            [emptyView setupViewForTableView:self.tableView text:NSLocalizedString(@"No items in cart", nil)];
            [self.tableView setTableHeaderView:emptyView];
        }
    }
}

- (void)updateFields
{
    // Create an array to define the fields displayed in section 1
    self.section1Fields = [[NSMutableArray alloc] initWithCapacity:3];
    if ([self.group isEqualToString:@"menu"]) {
        if ([[self getServiceMethods] count] > 1) {
            [self.section1Fields addObject:@"service"];
        }
        
        if ([self getCart].estimatedWaitingTime > 0) {
            [self.section1Fields addObject:@"waitingtime"];
        }
        if ([self getCart].quote && [[self getCart].quote.hasLoyaltyProgram boolValue]) {
            [self.section1Fields addObject:@"loyalty"];
        }
    }
    [self.section1Fields addObject:@"terms"];
}

- (ZECart *)getCart
{
    ZEAppDelegate *delegate = (ZEAppDelegate *)[[UIApplication sharedApplication] delegate];
    return [delegate.cartManager getCartForLocation:self.location bar:self.bar group:self.group event:self.event];
}

-(NSMutableArray*) getServiceMethods
{
    NSMutableArray* methods = [NSMutableArray arrayWithCapacity:1];
    if ([self.bar hasCounterService]) {
        if ([self.bar hasToStayAndToGo]) {
            [methods addObject:@"counter_tostay"];
            [methods addObject:@"counter_togo"];
        } else {
            [methods addObject:@"counter"];
        }
    }
    if ([self.bar.tableService boolValue]) {
        [methods addObject:@"table"];
    }
    if ([self.bar.deliveryService boolValue]) {
        [methods addObject:@"delivery"];
    }
    
    return methods;
}

- (IBAction)changeQuantity:(UIStepper *)sender {
    ZECartItemCell *cell;
    UIView *v = sender;
    while (v && ![v isKindOfClass:[ZECartItemCell class]]) v = v.superview;
    cell = (ZECartItemCell *)v;
    
    cell.priceLabel.text = @"--";
    if (cell.cartItem) {
        if ((int)[sender value] == 0) {
            // Remove item
            [[self getCart] removeItem:cell.cartItem];
            // If there are still rows left, we can animate the removal
            if ([[self getCart] countRows] > 0) {
                [self.tableView deleteRowsAtIndexPaths:@[[self.tableView indexPathForCell:cell]] withRowAnimation:UITableViewRowAnimationFade];
            }
        } else {
            // Update item
            cell.cartItem.quantity = [sender value];
        }
        if (cell.cartItem.quoteItem) {
            cell.titleLabel.text = [NSString stringWithFormat:@"%d x %@", cell.cartItem.quantity, cell.cartItem.quoteItem.name];
        } else {
            cell.titleLabel.text = [NSString stringWithFormat:@"%d x %@", cell.cartItem.quantity, cell.cartItem.productName];
        }
        [self reload];
    }
}

- (IBAction)toggleFavourite:(UIButton *)sender {
    
    ZECartItemCell *cell;
    UIView *v = sender;
    while (v && ![v isKindOfClass:[ZECartItemCell class]]) v = v.superview;
    cell = (ZECartItemCell *)v;
    
    if (cell.cartItem)
    { 
        // hide the button and show a spinner instead
        [cell.favouriteButton setHidden:YES];
        [cell.spinner setHidden:NO];

        // ADD TO FAVOURITES
        if(!cell.cartItem.isFavourite)
        {
            UIImage * image = [UIImage imageNamed:@"star_filled.png"];
            [cell.favouriteButton setImage:image forState:UIControlStateNormal];
            // Gets the consumerManager from the delegate to access the consumer key
            ZEConsumerManager *consumerManager = ((ZEAppDelegate *)[[UIApplication sharedApplication] delegate]).consumer;
        
            // Item options
            NSMutableArray *opt = [[NSMutableArray alloc] init];
            for (ZECartItemOptionValue *optionValue in cell.cartItem.options) {
                [opt addObject:(NSMutableDictionary *)[optionValue jsonObject]];
            }
            NSArray *objects, *keys;
            // Item is an entrance
            if([self getCart].quote.bar.key == NULL){
                objects = [[NSArray alloc] initWithObjects: [NSString stringWithFormat:@"%d", cell.cartItem.quantity], [self getCart].quote.location.key, consumerManager.consumerKey, cell.cartItem.productKey, opt, nil];
                keys = [[NSArray alloc] initWithObjects:@"quantity", @"location", @"consumer", @"product", @"options", nil];
            }
            // Item is from the menu
            else {
                objects = [[NSArray alloc] initWithObjects: [NSString stringWithFormat:@"%d", cell.cartItem.quantity], [self getCart].quote.bar.key, consumerManager.consumerKey, cell.cartItem.productKey, opt, nil];
                keys = [[NSArray alloc] initWithObjects:@"quantity", @"bar", @"consumer", @"product", @"options", nil];
            }
        
            // Creates the request
            NSDictionary *params = [[NSDictionary alloc] initWithObjects:objects forKeys:keys];
            NSString *fullURL = [NSString stringWithFormat:@"%@%@", API_URL, @"favourites"];
            NSLog(@"%@", fullURL);
            NSURL *url = [NSURL URLWithString:fullURL];
            NSMutableURLRequest *requestObj = [NSMutableURLRequest requestWithURL:url];
            [requestObj setHTTPMethod:@"POST"];
            [requestObj setValue:@"application/json" forHTTPHeaderField:@"content-type"];
            NSError *e = nil;
            [requestObj setHTTPBody:[NSJSONSerialization dataWithJSONObject:params options:0 error:&e]];
            NSData *accessTokenData = [consumerManager.accessToken dataUsingEncoding:NSUTF8StringEncoding];
            NSString *authValue = [NSString stringWithFormat:@"Basic %@", [accessTokenData base64Encoding]];
            [requestObj setValue:authValue forHTTPHeaderField:@"Authorization"];
            // Creates the connection and send the request
            self.addFavouriteConnection = [[NSURLConnection alloc] initWithRequest:requestObj delegate:self];
            cell.cartItem.isFavourite = YES;
        }
        // DELETE FROM FAVOURITES
        else {
            UIImage * image = [UIImage imageNamed:@"star.png"];
            [cell.favouriteButton setImage:image forState:UIControlStateNormal];
            cell.cartItem.isFavourite = NO;
            ZEAppDelegate *delegate = [[UIApplication sharedApplication] delegate];
            [delegate.consumer deleteConsumerFavourite:cell.cartItem.favourite updateView:self];
        }
    }
    
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    self.lastResponse = (NSHTTPURLResponse *)response;
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
  if(connection == self.addFavouriteConnection) {
        if(self.lastResponse.statusCode == 200) {
            [SVProgressHUD dismiss];
            [SVProgressHUD showSuccessWithStatus:NSLocalizedString(@"Added to favourites",nil)];
            [self reload];
        }
        else [SVProgressHUD showErrorWithStatus:NSLocalizedString(@"Error",nil)];
    }
}


- (IBAction)changeServiceType:(UISegmentedControl *)sender {
    [self getCart].service = sender.selectedSegmentIndex == 1 ? @"table" : @"counter";
    [self.tableView reloadData];
}

- (IBAction)changeTakeaway:(UISegmentedControl *)sender {
    [self getCart].toGo = sender.selectedSegmentIndex == 1 ? YES : NO;
    [self.tableView reloadData];
}

- (void)closeNumberPad
{
    [self.view endEditing:YES];
}

- (IBAction)acceptTermsClicked:(UIButton *)sender {
    sender.selected = ![sender isSelected];
    [self getCart].termsAccepted = [sender isSelected];
    ZEConsumerManager *consumerManager = ((ZEAppDelegate *)[[UIApplication sharedApplication] delegate]).consumer;
    [consumerManager setTermsAccepted:[sender isSelected]];
}

- (IBAction)termsClicked:(id)sender {
    [self performSegueWithIdentifier:@"ShowTerms" sender:self];
}

- (IBAction)editOptions:(id)sender {
    [self performSegueWithIdentifier:@"EditOptions" sender:sender];
}

- (IBAction)tipSliderDragged:(UISlider *)sender {
    sender.value = roundf(sender.value);
    self.tipPercentageLabel.text = [NSString stringWithFormat:@"%d %%", (int)sender.value];
}

- (IBAction)tipSliderReleased:(UISlider *)sender {
    [[self getCart] setTipPercent:[NSNumber numberWithFloat:sender.value]];
    [self reload];
}

- (IBAction)addTip:(id)sender
{
    [[self getCart] setTipPercent:[NSNumber numberWithFloat:10.0f]];
    [self reload];
}

- (IBAction)placeOrder:(id)sender {
    // Validate terms
    if (![self getCart].termsAccepted) {
        [SVProgressHUD showErrorWithStatus:NSLocalizedString(@"Please accept the Terms & Conditions", nil)];
        [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:[self tableView:self.tableView numberOfRowsInSection:1]-1 inSection:1] atScrollPosition:UITableViewScrollPositionTop animated:YES];
        return;
    }
    
    // Validate table number
    if ([[self getCart].service isEqualToString:@"table"] && [[self getCart].tableNumber isEqualToString:@""]) {
        [SVProgressHUD showErrorWithStatus:NSLocalizedString(@"Please enter your table number", nil)];
        return;
    }
    
    // Validate service
    if ([[self getCart].service isEqualToString:@""]) {
        [SVProgressHUD showErrorWithStatus:NSLocalizedString(@"Please choose a service type", nil)];
        return;
    }
    
    // Validate min order amount
    if ([[self getCart].service isEqualToString:@"delivery"] && [[self getCart].quote.subtotal doubleValue] < [[self getCart].bar.deliveryMinOrderAmount doubleValue]) {
        [SVProgressHUD showErrorWithStatus:[NSString stringWithFormat:NSLocalizedString(@"The minimum order amount for delivery is %@", nil), self.bar.deliveryMinOrderAmountFormatted]];
        return;
    }
    
    [self checkPayment];
}

- (void)checkPayment
{
    NSString* customAllowedPaymentMethodsStr = [self getCart].quote.customAllowedPaymentMethods;
    [self.allowedPaymentMethodButtons removeAllObjects];
    
    if (customAllowedPaymentMethodsStr == nil) {
        // Custom allowed payment methods not enabled. Use defaults.
            [self checkPaymentWithType:1];
        }
    else {
        // Custom allowed payment methods specified.
        NSLog(@"checkpayment, custom allowed payment methods: %@", customAllowedPaymentMethodsStr);
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:NSLocalizedString(@"select_payment_method", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel", nil) otherButtonTitles:nil];
        alert.tag = 3;
        NSArray *customAllowedPaymentMethods = [customAllowedPaymentMethodsStr componentsSeparatedByString:@","];
        
        if ([customAllowedPaymentMethods indexOfObject:@"epay"] != NSNotFound) {
            [alert addButtonWithTitle:NSLocalizedString(@"payment_method_creditcard", nil)];
            [self.allowedPaymentMethodButtons addObject:@"epay"];
        }
        
        if ([customAllowedPaymentMethods indexOfObject:@"swipp"] != NSNotFound) {
            [alert addButtonWithTitle:NSLocalizedString(@"payment_method_swipp", nil)];
            [self.allowedPaymentMethodButtons addObject:@"swipp"];
        }
        if ([customAllowedPaymentMethods indexOfObject:@"mobilepay_appswitch"] != NSNotFound) {
            [alert addButtonWithTitle:NSLocalizedString(@"payment_method_mobilepay", nil)];
            [self.allowedPaymentMethodButtons addObject:@"mobilepay_appswitch"];
        }

        if ([self.allowedPaymentMethodButtons count] > 1) {
            [alert show];
        }
        else if ([self.allowedPaymentMethodButtons count] == 1) {
            [self checkPaymentWithType:[self getPaymentType:[self.allowedPaymentMethodButtons objectAtIndex:0]]];
        }
        else {
             UIAlertView *alertNoPaymentMethodsFound = [[UIAlertView alloc] initWithTitle:nil message:NSLocalizedString(@"no_payment_methods_found", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel", nil) otherButtonTitles:nil];
            [alertNoPaymentMethodsFound show];
        }
    }
}

- (void)submitOrderWithPin:(NSString *)pincode
{
    [SVProgressHUD showWithStatus:NSLocalizedString(@"Placing your order", @"Progress indicator after payment")];
    
    // Setup parameters
    [self getCart].card = @"current";
    [self getCart].pincode = pincode;
    [self getCart].obscuredCardNumber = nil;
    [self getCart].nordpayAuthorization = nil;
    [self getCart].nordpayRegistration = nil;
    
    // Submit order
    RKObjectManager *objectManager = [RKObjectManager sharedManager];
    [objectManager postObject:[self getCart] path:@"placeorder" parameters:nil success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
        // Response OK
        ZEPlaceOrderResult *result = [mappingResult firstObject];
        if (result.paymentNeeded && [result.paymentNeeded boolValue]) {
            // Payment needed
            [SVProgressHUD dismiss];
            [self performSegueWithIdentifier:@"Pay" sender:self];
        } else {
            // Order placed
            [[self getCart] clear];
            [self.pinController dismiss];
            [self dismissViewControllerAnimated:YES completion:^{
                [self.delegate cartViewControllerDidPlaceOrder:self id:result.orderId key:result.orderKey fromTabIndex:self.tabIndex];
            }];
        }
    } failure:^(RKObjectRequestOperation *operation, NSError *error) {
        if (operation.HTTPRequestOperation.response.statusCode == 403) {
            // Wrong code
            [self.pinController showError:NSLocalizedString(@"Wrong code", nil)];
        } else {
            // Other error - dismiss PIN view and display error
            [self.pinController dismiss];
            [self dismissViewControllerAnimated:YES completion:^{
                if (operation.HTTPRequestOperation.response.statusCode == 409) {
                    // Validation error - reload cart to get latest quote/bar data
                    [self reload];
                }
                [SVProgressHUD showErrorWithStatus:error.localizedDescription];
            }];
        }
    }];
}

- (void)submitOrderWithMobilePay
{
    if (![[MobilePayManager sharedInstance]isMobilePayInstalled:MobilePayCountry_Denmark]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Missing MobilePay app", nil)
                                                        message:@"Please install the MobilePay app."
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"Cancel", nil)
                                              otherButtonTitles:NSLocalizedString(@"Install MobilePay", nil), nil];
        alert.tag = 1;
        [alert show];
        return;
    }

    [[MobilePayManager sharedInstance] setupWithMerchantId:MOBILEPAY_MERCHANT_NUMBER merchantUrlScheme:@"zervedapp" timeoutSeconds:120 returnSeconds:1 captureType:MobilePayCaptureType_Reserve country:MobilePayCountry_Denmark];

    [SVProgressHUD showWithStatus:NSLocalizedString(@"Placing your order", @"Progress indicator after payment")];
    
    NSString* callbackurl = [NSString stringWithFormat:@"%@/api/v2/orders/mobilepayappswitchcallback", BASE_SERVER_URL];
    [[MobilePayManager sharedInstance] setServerCallbackUrl:callbackurl];
     
    // Setup parameters
    [self getCart].card = @"mobilepay_appswitch";
    
    RKObjectManager *objectManager = [RKObjectManager sharedManager];
    [objectManager postObject:[self getCart] path:@"placeorder" parameters:nil success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
        // Response OK
        NSLog(@"Response OK: %@",operation.HTTPRequestOperation.responseString);
        ZEPlaceOrderResult *result = [mappingResult firstObject];
        if (result.orderId) {
            [SVProgressHUD dismiss];
            
            // Success
            self.navigationItem.hidesBackButton = NO;

            // Save cart to be able to clear it if successful payment and return from MobilePay to AppDelegate
            ZEAppDelegate *delegate = [[UIApplication sharedApplication] delegate];
            delegate.cart_mobile_pay_order = [self getCart];
            delegate.order_id_mobile_pay_order = result.orderId;
            
            NSLog(@"order id %@", result.orderId);
            float price = [[self getCart].quote.totalInclTax floatValue];
            MobilePayPayment *payment = [[MobilePayPayment alloc]initWithOrderId:result.orderId productPrice:price];
            
            
           
            [[MobilePayManager sharedInstance]beginMobilePaymentWithPayment:payment error:^(NSError * _Nonnull error) {
                
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:error.localizedDescription
                                                                message:[NSString stringWithFormat:@"reason: %@, suggestion: %@",error.localizedFailureReason, error.localizedRecoverySuggestion]
                                                               delegate:self
                                                      cancelButtonTitle:NSLocalizedString(@"Cancel", nil)
                                                      otherButtonTitles:NSLocalizedString(@"Install MobilePay", nil),nil];
                alert.tag = 1;
                [alert show];
            }];
        }
    } failure:^(RKObjectRequestOperation *operation, NSError *error) {
        [SVProgressHUD showErrorWithStatus:error.localizedDescription];
        self.navigationItem.hidesBackButton = NO;
        
    }];
}
     
- (void)submitOrderWith4T
{
    [SVProgressHUD showWithStatus:NSLocalizedString(@"Placing your order", @"Progress indicator after payment")];
    
    // Setup parameters
    [self getCart].card = @"4t";
    //[self getCart].pincode = pincode;
    [self getCart].obscuredCardNumber = nil;
    [self getCart].nordpayAuthorization = nil;
    [self getCart].nordpayRegistration = nil;
	[self getCart].deliveryPhone = self.consumer.phone;
    
    // Submit order
    RKObjectManager *objectManager = [RKObjectManager sharedManager];
    [objectManager postObject:[self getCart] path:@"placeorder" parameters:nil success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
        // Response OK
        ZEPlaceOrderResult *result = [mappingResult firstObject];
        if ([result.paymentNeeded boolValue]) {
            // Payment needed
            [SVProgressHUD dismiss];
            NSLog(@"Payment needed");
            //[self performSegueWithIdentifier:@"Pay" sender:self];
        } else {
            // Order placed
            [[self getCart] clear];
            [self.delegate cartViewControllerDidPlaceOrder:self id:result.orderId key:result.orderKey fromTabIndex:self.tabBarController.selectedIndex];
        }
    } failure:^(RKObjectRequestOperation *operation, NSError *error) {
        [SVProgressHUD showErrorWithStatus:error.localizedDescription];
    }];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"ShowTerms"]) {
        UINavigationController *navController = [segue destinationViewController];
        ZEModalWebpageViewController *termsController = [navController.viewControllers objectAtIndex:0];
        termsController.url = @"terms";
        termsController.delegate = self;
    } else if ([[segue identifier] isEqualToString:@"EditOptions"]) {
        UIButton *editButton = (UIButton *)sender;
        DLog(@"Edit %ld", (long)editButton.tag);
        ZECartItem *item = [[self getCart].items objectAtIndex:editButton.tag];
        UINavigationController *navController = [segue destinationViewController];
        ZEProductOptionsViewController *destinationController = [navController.viewControllers objectAtIndex:0];
        destinationController.productKey = item.productKey;
        destinationController.cart = [self getCart];
        destinationController.cartItem = item;
        destinationController.itemIndex = editButton.tag;
    } else if ([[segue identifier] isEqualToString:@"Pay"]) {
        ZEEpayViewController *paymentViewController = [segue destinationViewController];
        paymentViewController.delegate = (ZETabBarController *)self.navigationController.tabBarController;
        paymentViewController.shouldPlaceOrder = YES;
        paymentViewController.title = NSLocalizedString(@"Payment", @"Payment window title");
        paymentViewController.cart = [self getCart];
    } else if ([[segue identifier] isEqualToString:@"ValidatePin"]) {
        UINavigationController *navController = [segue destinationViewController];
        self.pinController = [navController.viewControllers objectAtIndex:0];
        self.pinController.delegate = self;
        self.pinController.title = NSLocalizedString(@"Confirm Payment", @"PIN view title for saved credit card");
        self.pinController.header = NSLocalizedString(@"Enter your payment code", @"PIN header for saved credit card");
        self.pinController.subtitle = NSLocalizedString(@"for your saved card", @"PIN view subtitle for saved credit card");
        self.pinController.helpText = NSLocalizedString(@"Forgot your code or got a new card?", @"PIN view help text for saved credit card");
        self.pinController.displayForgotButton = YES;
    } else if ([[segue identifier] isEqualToString:@"AddPhone"]) {
        ZE4TPhoneViewController *phoneController = [segue destinationViewController];
        phoneController.consumer = self.consumer;
        phoneController.cart = [self getCart];
        phoneController.delegate = (ZETabBarController *)self.navigationController.tabBarController;
    } else if ([[segue identifier] isEqualToString:@"EditService"]) {
        UINavigationController *navController = [segue destinationViewController];
        ZEServiceViewController *serviceController = [navController.viewControllers objectAtIndex:0];
        serviceController.bar = self.bar;
        serviceController.cart = [self getCart];
        serviceController.methods = [self getServiceMethods];
        serviceController.delegate = self;
    } else {
        [super prepareForSegue:segue sender:sender];
    }
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [[self getCart] countRows] > 0 && self.quote ? 2 : 0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) {
        NSInteger totals = [[self getCart] countRows] + 1; // items + discount + tip + grand total
		if (self.quote && [self.quote.totalLoyaltyDiscountInclTax floatValue] != 0.f) {
			totals += 1;
		}
        if (self.quote && [self.quote.tipEnabled boolValue]) {
            totals += 1;
        }
        if (self.quote) {
            totals += [self.quote.fees count];
        }
        return totals;
    } else if (section == 1) {
        return [self.section1Fields count];
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
	BOOL hasDiscount = (self.quote && [self.quote.totalLoyaltyDiscountInclTax floatValue] != 0.f);
    
    if (indexPath.section == 0) {
        NSInteger rowsInSection = [self tableView:tableView numberOfRowsInSection:indexPath.section];
        if (indexPath.row < [[self getCart] countRows]) {
            // Item cell
            ZECartItemCell *cartItemCell = [tableView dequeueReusableCellWithIdentifier:(self.event ? @"CartItemWithoutFavouriteCell" : @"CartItemCell") forIndexPath:indexPath];
            ZECartItem *item = [[self getCart].items objectAtIndex:indexPath.row];
            [cartItemCell setCartItem:item];
            [cartItemCell.quantityStepper addTarget:self action:@selector(changeQuantity:) forControlEvents:UIControlEventTouchUpInside];
            [cartItemCell.editButton setTag:indexPath.row];
            [cartItemCell.editButton addTarget:self action:@selector(editOptions:) forControlEvents:UIControlEventTouchUpInside];
            cell = cartItemCell;
        }
        // Totals
        /*if (indexPath.row == 0) {
            // Subtotal
            cell = [tableView dequeueReusableCellWithIdentifier:@"TotalCell" forIndexPath:indexPath];
            cell.textLabel.text = NSLocalizedString(@"Subtotal", nil);
            cell.detailTextLabel.text = ([self getCart].quote) ? [self getCart].quote.subtotalFormatted : @"--";
        }*/
		else if (indexPath.row == [self getCart].countRows && hasDiscount)
		{
			cell = [tableView dequeueReusableCellWithIdentifier:@"DiscountCell" forIndexPath:indexPath];
			cell.textLabel.text = NSLocalizedString(@"Discount", nil);
			cell.detailTextLabel.text = self.quote.totalLoyaltyDiscountInclTaxFormatted;
		}
		else if (((indexPath.row == [[self getCart] countRows] && !hasDiscount) || (indexPath.row == [[self getCart] countRows]+1 && hasDiscount)) && self.quote && [self.quote.tipEnabled boolValue]) {
            // Tip
            ZETipCell *tipCell = [tableView dequeueReusableCellWithIdentifier:@"TipCell" forIndexPath:indexPath];
            //tipCell.priceLabel.text = ([self getCart].quote) ? [self getCart].quote.tipFormatted : @"--";
            self.tipPercentageLabel = tipCell.tipPercentageLabel;
            tipCell.tipSlider.value = [[self getCart].tipPercent floatValue];
            tipCell.tipPercentageLabel.text = [NSString stringWithFormat:@"%d %%", (int)[[self getCart].tipPercent floatValue]];
            [tipCell.tipSlider addTarget:self action:@selector(tipSliderDragged:) forControlEvents:UIControlEventValueChanged];
            [tipCell.tipSlider addTarget:self action:@selector(tipSliderReleased:) forControlEvents:UIControlEventTouchUpInside];
            if ([[self getCart].tipPercent intValue] > 0) {
                // Tip applied, show tip price
                tipCell.priceLabel.text = ([self getCart].quote) ? [self getCart].quote.tipFormatted : @"--";
                tipCell.priceButton.hidden = YES;
				tipCell.tipSlider.hidden = NO;
				tipCell.tipPercentageLabel.hidden = NO;
            } else {
                // No tip, show add tip button
                tipCell.priceLabel.text = nil;
                tipCell.priceButton.hidden = NO;
				tipCell.tipSlider.hidden = YES;
				tipCell.tipPercentageLabel.hidden = YES;
            }
            cell = tipCell;
        } else if (indexPath.row < rowsInSection - 1) {
            // Fees
            NSInteger feeIndex = indexPath.row - [[self getCart] countRows];
			if (self.quote && [self.quote.totalLoyaltyDiscountInclTax floatValue] != 0.f)
				feeIndex -= 1;
            if (self.quote && [self.quote.tipEnabled boolValue])
                feeIndex -= 1;
            ZEQuoteFee *fee = [self.quote.fees objectAtIndex:feeIndex];
            cell = [tableView dequeueReusableCellWithIdentifier:@"FeeCell" forIndexPath:indexPath];
            cell.textLabel.text = fee.name;
            cell.detailTextLabel.text = fee.priceFormatted;
        /*} else if (indexPath.row == rowsInSection - 2) {
            // VAT
            cell = [tableView dequeueReusableCellWithIdentifier:@"TotalCell" forIndexPath:indexPath];
            cell.textLabel.text = NSLocalizedString(@"VAT", @"Label for total amount in given currency");
            cell.detailTextLabel.text = ([self getCart].quote) ? [self getCart].quote.totalTaxFormatted : @"--";
           */
        } else if (indexPath.row == rowsInSection - 1) {
            // Grand total
            cell = [tableView dequeueReusableCellWithIdentifier:@"TotalCell" forIndexPath:indexPath];
            cell.textLabel.text = [NSString stringWithFormat:NSLocalizedString(@"Total %@", @"Label for total amount in given currency"), ([self getCart].quote) ? [self getCart].quote.currency : @""];
            cell.detailTextLabel.text = ([self getCart].quote) ? [self getCart].quote.totalInclTaxFormatted : @"--";
        }
    } else if (indexPath.section == 1) {
        if ([[self.section1Fields objectAtIndex:indexPath.row] isEqualToString:@"service"]) {
            // Service cell
            cell = [tableView dequeueReusableCellWithIdentifier:@"ServiceCell" forIndexPath:indexPath];
            
            cell.textLabel.text = NSLocalizedString(@"Service", @"How/where is the order served (pickup at counter, serve at table etc.)");
            if ([[self getCart].service isEqualToString:@"table"]) {
                cell.detailTextLabel.text = [NSString stringWithFormat:NSLocalizedString(@"Serve at table no. %@", nil), [self getCart].tableNumber];
            } else if ([[self getCart].service isEqualToString:@"counter"]) {
                if ([[self getCart].bar hasToStayAndToGo]) {
                    if ([self getCart].toGo) {
                        cell.detailTextLabel.text = NSLocalizedString(@"Pickup at counter to go", nil);
                    } else {
                        cell.detailTextLabel.text = NSLocalizedString(@"Pickup at counter to stay", nil);
                    }
                } else {
                    cell.detailTextLabel.text = NSLocalizedString(@"Pickup at counter", nil);
                }
            } else if ([[self getCart].service isEqualToString:@"delivery"]) {
                cell.detailTextLabel.text = [NSString stringWithFormat:NSLocalizedString(@"Deliver to %@", @"Deliver to postcode"), [self getCart].deliveryPostcode];
            } else if ([[self getCart].service isEqualToString:@""]) {
                cell.detailTextLabel.text = NSLocalizedString(@"Select", nil);
            }
        } else if ([[self.section1Fields objectAtIndex:indexPath.row] isEqualToString:@"waitingtime"]) {
            // Waiting time
            cell = [tableView dequeueReusableCellWithIdentifier:@"WaitingTimeCell" forIndexPath:indexPath];
            cell.textLabel.text = NSLocalizedString(@"Average waiting time", nil);
            cell.detailTextLabel.text = [NSString stringWithFormat:NSLocalizedString(@"%d min.", @"Waiting time estimate in cart"), [self getCart].estimatedWaitingTime];
        } else if ([[self.section1Fields objectAtIndex:indexPath.row] isEqualToString:@"loyalty"]) {
            cell = [tableView dequeueReusableCellWithIdentifier:@"LoyaltyCell" forIndexPath:indexPath];
			NSString *loyaltyTitle = self.quote.loyaltyName == nil || [self.quote.loyaltyName isEqualToString:@""] ? NSLocalizedString(@"Loyalty number", nil) : self.quote.loyaltyName;
            cell.textLabel.text = loyaltyTitle;
            cell.detailTextLabel.text = [self getCart].loyaltyCode.length > 0 ? [self decodeHTMLEntities:[self getCart].loyaltyCode] : NSLocalizedString(@"Add", @"Add loyalty code to order");
        } else if ([[self.section1Fields objectAtIndex:indexPath.row] isEqualToString:@"terms"]) {
            // Terms
            ZETermsCell *termsCell = [tableView dequeueReusableCellWithIdentifier:@"TermsCell" forIndexPath:indexPath];
            [termsCell.acceptTermsButton setSelected:[self getCart].termsAccepted];
            [termsCell.acceptTermsButton addTarget:self action:@selector(acceptTermsClicked:) forControlEvents:UIControlEventTouchUpInside];
            [termsCell.termsButton addTarget:self action:@selector(termsClicked:) forControlEvents:UIControlEventTouchUpInside];
            cell = termsCell;
        }
    }
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	BOOL hasDiscount = (self.quote && [self.quote.totalLoyaltyDiscountInclTax floatValue] != 0.f);
	
	if (indexPath.section == 0)
	{
		if (indexPath.row < [[self getCart] countRows]) {
			// Item
			ZECartItem *item = [[self getCart].items objectAtIndex:indexPath.row];
			return item.isExpanded ? 107 : 40;
		} else if (((indexPath.row == [[self getCart] countRows] && !hasDiscount) || (indexPath.row == [[self getCart] countRows]+1 && hasDiscount)) && self.quote && [self.quote.tipEnabled boolValue]) {
			// Tip
			if ([[self getCart].tipPercent intValue] > 0) {
				return 80;
			} else {
				return 44;
			}
		} else {
			return 44;
		}
	}
	return 44;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if (section == 0 && self.bar) {
        if (self.event) {
            return nil;
            //return [NSString stringWithFormat:@"%@\r\n%@ %@", self.event.name, self.event.timeLabel, self.event.dateDeliveryFormatted];
        } else if (self.bar) {
            return self.bar.name;
        }
    } else if (section == 0 && self.location) {
        return self.location.name;
    }
    return @[NSLocalizedString(@"Items", @"Title for cart items"),
             NSLocalizedString(@"Details", @"Title for order details in cart")][section];
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0 && indexPath.row < [[self getCart] countRows]) {
        ZECartItem *item = [[self getCart].items objectAtIndex:indexPath.row];
        item.isExpanded = !item.isExpanded;
        [tableView reloadData];
    } else if (indexPath.section == 1) {
        if ([[self.section1Fields objectAtIndex:indexPath.row] isEqualToString:@"loyalty"]) {
			NSString *loyaltyTitle = self.quote.loyaltyName == nil || [self.quote.loyaltyName isEqualToString:@""] ? NSLocalizedString(@"Loyalty number", @"Title for loyalty code") : self.quote.loyaltyName;
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:loyaltyTitle message:nil delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel", nil) otherButtonTitles:NSLocalizedString(@"Save", nil), nil];
            alertView.alertViewStyle = UIAlertViewStylePlainTextInput;
            alertView.tag = 2;
            [alertView textFieldAtIndex:0].text = [self decodeHTMLEntities:[self getCart].loyaltyCode];
            [alertView show];
        }
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 0 && self.event) {
        return 0.1f; // hack to remove section title (zero doesn't work)
    }
    return UITableViewAutomaticDimension;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    if (section == 1) {
        return 76;
    }
    return 0;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    if (section == 1) {
        return self.footerView;
    }
    return nil;
}

#pragma mark - UITextFieldDelegate Methods

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [self getCart].tableNumber = textField.text;
}

#pragma mark - ZETermsDelegate

- (void)modalWebpageViewControllerDone:(ZEModalWebpageViewController *)termsController
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)didFailToAuthenticate
{
    
}

#pragma mark - ZEPinViewControllerDelegate

- (void)pinViewControllerDidEnterPin:(ZEPinViewController *)controller pin:(NSString *)pin
{
    [self submitOrderWithPin:pin];
}

- (void)pinViewControllerDidCancel:(ZEPinViewController *)controller
{
    [self.pinController dismiss];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)pinViewControllerForgotPin:(ZEPinViewController *)controller
{
    [self.pinController dismiss];
    [self dismissViewControllerAnimated:YES completion:^{
        [self performSegueWithIdentifier:@"Pay" sender:self];
    }];
}

#pragma mark - ZEServiceDelegate Methods

- (void)serviceDone {
    [self dismissViewControllerAnimated:YES completion:^{
        //[self reload];
    }];
}

- (void)serviceCancelled {
    [self dismissViewControllerAnimated:YES completion:nil];
    // Go back if service not set
    if ([[self getCart].service isEqualToString:@""]) {
        [self.navigationController popViewControllerAnimated:NO];
    }
}

#pragma mark - UIAlertViewDelegate Methods

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0) {
        return;
    }
    
    if (alertView.tag == 1 && buttonIndex == 1) /* NO = 0, YES = 1 */
    {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[MobilePayManager sharedInstance].mobilePayAppStoreLinkDK]];
    }
    
        
    else if (alertView.tag == 2) {
        // Save Loyalty code
        [self getCart].loyaltyCode = [self encodeHTMLEntities:[alertView textFieldAtIndex:0].text];
        [self reload];
        [self reloadTableView];
    }
    else if (alertView.tag == 3)
    {
        int indexOfFirstPaymentButton = 1;
        NSString* clickedPaymentType = [self.allowedPaymentMethodButtons objectAtIndex:buttonIndex - indexOfFirstPaymentButton];
        [self checkPaymentWithType:[self getPaymentType:clickedPaymentType]];
    }
}

- (int)getPaymentType:(NSString*)paymentName {
    if ([paymentName isEqualToString:@"epay"]) {
        return 1;
    }
    else if ([paymentName isEqualToString:@"swipp"]) {
        return 2;
    }
    else if ([paymentName isEqualToString:@"mobilepay_appswitch"]) {
        return 3;
    }
    
    return -1;
}


- (void)checkPaymentWithType:(NSInteger)paymentIndex {
    [SVProgressHUD showWithStatus:NSLocalizedString(@"Loading", nil)];
    ZEConsumerManager *consumerManager = ((ZEAppDelegate *)[[UIApplication sharedApplication] delegate]).consumer;
    RKObjectManager *objectManager = [RKObjectManager sharedManager];
    // Fetch consumer details
    [objectManager getObjectsAtPath:[NSString stringWithFormat:@"consumers/%@", [consumerManager consumerKey]]
							 parameters:nil
								success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {

									[SVProgressHUD dismiss];
									self.consumer = [mappingResult firstObject];
									if (paymentIndex == 1) {
										// Check if consumer has a saved card (payment subscription)
										if (self.consumer.currentPaymentSubscription) {
											// Card saved, ask for payment code
											[self performSegueWithIdentifier:@"ValidatePin" sender:self];
										} else {
											// No card saved, go to payment
											[self performSegueWithIdentifier:@"Pay" sender:self];
										}
									}
									else if (paymentIndex == 2) {
										// 4T
										if (self.consumer.phone.length != 8) {
											// Phone no. needed
											[self performSegueWithIdentifier:@"AddPhone" sender:self];
										} else {
											[self submitOrderWith4T];
										}
									}
                                    else if (paymentIndex == 3) {
                                        // MobilePay
                                        [self submitOrderWithMobilePay];
                                    }
								}
								failure:^(RKObjectRequestOperation *operation, NSError *error) {
									UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil)
																					message:[error localizedDescription]
																				   delegate:nil
																		  cancelButtonTitle:NSLocalizedString(@"OK", nil)
																		  otherButtonTitles:nil];
                                    alert.tag = 1;
									[alert show];
								}];
}

#pragma mark - HTML entities

- (NSString *)decodeHTMLEntities:(NSString *)string
{
	string = [string stringByReplacingOccurrencesOfString:@"&Aring;" withString:@"Å"];
	string = [string stringByReplacingOccurrencesOfString:@"&AElig;" withString:@"Æ"];
	string = [string stringByReplacingOccurrencesOfString:@"&Oslash;" withString:@"Ø"];
	string = [string stringByReplacingOccurrencesOfString:@"&aring;" withString:@"å"];
	string = [string stringByReplacingOccurrencesOfString:@"&aelig;" withString:@"æ"];
	string = [string stringByReplacingOccurrencesOfString:@"&oslash;" withString:@"ø"];
	return string;
}

- (NSString *)encodeHTMLEntities:(NSString *)string
{
	string = [string stringByReplacingOccurrencesOfString:@"Å" withString:@"&Aring;"];
	string = [string stringByReplacingOccurrencesOfString:@"Æ" withString:@"&AElig;"];
	string = [string stringByReplacingOccurrencesOfString:@"Ø" withString:@"&Oslash;"];
	string = [string stringByReplacingOccurrencesOfString:@"å" withString:@"&aring;"];
	string = [string stringByReplacingOccurrencesOfString:@"æ" withString:@"&aelig;"];
	string = [string stringByReplacingOccurrencesOfString:@"ø" withString:@"&oslash;"];
	return string;
}

@end
