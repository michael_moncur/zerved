//
//  ZEAppDelegate.h
//  Zerved
//
//  Created by Anders Rasmussen on 11/04/12.
//  Copyright (c) 2012 Zerved. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZECartManager.h"
#import "ZEConsumerManager.h"

@interface ZEAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) ZECartManager *cartManager;
@property (strong, nonatomic) ZEConsumerManager *consumer;
@property (strong, nonatomic) NSDictionary *lastNotification;
@property (retain, nonatomic) NSMutableData *receivedData;
@property (strong, nonatomic) ZECart *cart_mobile_pay_order;
@property (strong, nonatomic) NSString *order_id_mobile_pay_order;

@end
