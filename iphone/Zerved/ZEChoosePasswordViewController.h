//
//  ZEChoosePasswordViewController.h
//  Zerved
//
//  Created by Anders Rasmussen on 06/03/13.
//
//

#import <UIKit/UIKit.h>
#import "ZELoginInfoViewController.h"

@protocol ZEChoosePasswordDelegate;

@interface ZEChoosePasswordViewController : UIViewController <ZELoginInfoDelegate, UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *passwordField;
@property (weak, nonatomic) IBOutlet UILabel *validationLabelLetters;
@property (weak, nonatomic) IBOutlet UILabel *validationLabelNumbers;
@property (weak, nonatomic) IBOutlet UILabel *validationLabelLength;
@property (weak, nonatomic) id <ZEChoosePasswordDelegate> delegate;
- (IBAction)skip:(id)sender;
- (IBAction)save:(id)sender;
- (IBAction)typePassword:(UITextField *)sender;

@end

@protocol ZEChoosePasswordDelegate <NSObject>
- (void)choosePasswordDidEnterPassword:(ZEChoosePasswordViewController *)controller password:(NSString *)password;
- (void)choosePasswordDidCancel:(ZEChoosePasswordViewController *)controller;
@end