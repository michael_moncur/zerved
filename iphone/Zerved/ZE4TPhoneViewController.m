//
//  ZE4TPhoneViewController.m
//  Zerved
//
//  Created by Anders Rasmussen on 29/08/13.
//
//

#import "ZE4TPhoneViewController.h"

#import "SVProgressHUD.h"
#import "TTTAttributedLabel.h"

#import "ZEPlaceOrderResult.h"

@interface ZE4TPhoneViewController () <TTTAttributedLabelDelegate>
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *subtitleLabel;
@property (weak, nonatomic) IBOutlet TTTAttributedLabel *infoLabel;
@end

@implementation ZE4TPhoneViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	
	self.navigationItem.titleView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"paii_logo"]];
	
	self.titleLabel.text = NSLocalizedString(@"paii_enter_phonenumber_title", nil);
	self.subtitleLabel.text = NSLocalizedString(@"paii_enter_phonenumber_subtext", nil);
	
	self.infoLabel.delegate = self;
    self.infoLabel.linkAttributes = @{(id)kCTForegroundColorAttributeName: [UIColor colorWithRed:0 green:136.f/255.f blue:204.f/255.f alpha:1.f], (id)kCTUnderlineStyleAttributeName: @(kCTUnderlineStyleNone)};
	self.infoLabel.text = NSLocalizedString(@"paii_infotext", nil);
	NSRange downloadRange = [self.infoLabel.text rangeOfString:NSLocalizedString(@"paii_infotext_linkpattern", nil)];
	[self.infoLabel addLinkToURL:[NSURL URLWithString:@"http://www.paii.dk"] withRange:downloadRange];
	[self.infoLabel sizeToFit];
	self.infoLabel.center = CGPointMake(self.view.bounds.size.width/2, self.infoLabel.center.y);
	
	if (!IS_OS_7_OR_LATER)
	{
		self.titleLabel.frame = CGRectOffset(self.titleLabel.frame, 0, -64);
		self.subtitleLabel.frame = CGRectOffset(self.subtitleLabel.frame, 0, -64);
		self.phoneField.frame = CGRectOffset(self.phoneField.frame, 0, -64);
		self.infoLabel.frame = CGRectOffset(self.infoLabel.frame, 0, -64);
	}
	
	self.phoneField.text = self.consumer.phone;
	
    [self.phoneField becomeFirstResponder];
}

- (void)viewDidAppear:(BOOL)animated
{
	[super viewDidAppear:animated];
	
	if (self.consumer.phone && ![self.consumer.phone isEqualToString:@""])
		[SVProgressHUD showErrorWithStatus:NSLocalizedString(@"paii_error_invalid_phone", nil)];
}

- (IBAction)done:(id)sender {
    // Validate
    if ([self.phoneField.text length] != 8) {
        [SVProgressHUD showErrorWithStatus:NSLocalizedString(@"paii_error_invalid_phone", nil)];
        return;
    }
    
    // Save phone
	self.consumer.phone = self.phoneField.text;
    [SVProgressHUD showWithStatus:NSLocalizedString(@"Saving", nil)];
    RKObjectManager *objectManager = [RKObjectManager sharedManager];
    [objectManager postObject:self.consumer path:[NSString stringWithFormat:@"consumers/%@", self.consumer.key] parameters:nil success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
        // Submit order
        [self submitOrder];
    } failure:^(RKObjectRequestOperation *operation, NSError *error) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil)
                                                        message:NSLocalizedString(@"paii_error_saving_phone", nil)
                                                       delegate:nil
                                              cancelButtonTitle:NSLocalizedString(@"OK", nil)
                                              otherButtonTitles:nil];
        [alert show];
    }];

}

- (void)submitOrder
{
    [SVProgressHUD showWithStatus:NSLocalizedString(@"Placing your order", @"Progress indicator after payment")];
    
    self.cart.card = @"4t";
    self.cart.pincode = nil;
    self.cart.nordpayAuthorization = nil;
    self.cart.nordpayRegistration = nil;
    self.cart.nordpayMethod = nil;
    self.cart.obscuredCardNumber = nil;
	self.cart.deliveryPhone = self.consumer.phone;
    
    RKObjectManager *objectManager = [RKObjectManager sharedManager];
    [objectManager postObject:self.cart path:@"placeorder" parameters:nil success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
        // Response OK
        ZEPlaceOrderResult *result = [mappingResult firstObject];
        if (result.orderId) {
            // Success
            [self.cart clear];
            [self.delegate ze4TViewControllerDidPlaceOrder:self id:result.orderId key:result.orderKey];
        }
    } failure:^(RKObjectRequestOperation *operation, NSError *error) {
        [SVProgressHUD showErrorWithStatus:error.localizedDescription];
    }];
}

#pragma mark - TTTAttributed label delegate

- (void)attributedLabel:(TTTAttributedLabel *)label didSelectLinkWithURL:(NSURL *)url
{
	[[UIApplication sharedApplication] openURL:url];
}

@end