//
//  ZETabBarController.m
//  Zerved
//
//  Created by Anders Rasmussen on 02/05/12.
//  Copyright (c) 2012 Zerved. All rights reserved.
//
//  The builtin tab bar controller is extended to provide methods for automatically
//  switching tabs after placing an order and resetting the shopping flow.
//

#import "ZETabBarController.h"
#import "ZEOrderViewController.h"
#import "ZEAppDelegate.h"
#import "ZELocationViewController.h"
#import "ZEBarCategoriesViewController.h"
#import "ZEEpayViewController.h"
#import "ZEEmbeddedViewController.h"

@interface ZETabBarController ()

@end

@implementation ZETabBarController

@synthesize location = _location;
@synthesize locationName = _locationName;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1) {
        self.tabBar.tintColor = [UIColor blackColor];
    }
    self.delegate = self;
    [[self.tabBar.items objectAtIndex:0] setSelectedImage:[UIImage imageNamed:@"places_tabbar_icon_selected.png"]];
    [[self.tabBar.items objectAtIndex:1] setSelectedImage:[UIImage imageNamed:@"receipt_tabbar_icon_selected.png"]];
    [[self.tabBar.items objectAtIndex:3] setSelectedImage:[UIImage imageNamed:@"profile_tabbar_icon_selected.png"]];
    [[self.tabBar.items objectAtIndex:4] setSelectedImage:[UIImage imageNamed:@"info_tabbar_icon_selected.png"]];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    self.location = nil;
    self.locationName = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)placeOrderWithId:(NSString *)orderId key:(NSString *)orderKey fromTabIndex:(NSInteger)tabIndex
{
    [self viewOrderByKey:orderKey];
    
    // In shopping tab, go back to location view
    UINavigationController *menuNav = (UINavigationController *)[self.viewControllers objectAtIndex:tabIndex];
    int index = 1;
    for (int i=0; i<[menuNav.viewControllers count]; i++) {
        UIViewController *viewController = [menuNav.viewControllers objectAtIndex:i];
        if ([viewController isKindOfClass:[ZELocationViewController class]] || [viewController isKindOfClass:[ZEBarCategoriesViewController class]]) {
            index = i;
            break;
        }
    }
    [menuNav popToViewController:[menuNav.viewControllers objectAtIndex:index] animated:NO];
}

- (void)viewOrderByKey:(NSString *)orderKey
{
    UINavigationController *ordersNav = (UINavigationController *)[self.viewControllers objectAtIndex:1];
    
    // Reset and reload orders, and go to current order
    [ordersNav popToRootViewControllerAnimated:NO];
    UIStoryboard *storyboard = self.storyboard;
    ZEOrderViewController *orderView = [storyboard instantiateViewControllerWithIdentifier:@"OrderViewController"];
    [orderView setOrderKey:orderKey];
    [ordersNav pushViewController:orderView animated:YES];
    
    // Go to order tab
    self.selectedIndex = 1;
}

- (void)viewOrderById:(NSString *)orderId
{
    UINavigationController *ordersNav = (UINavigationController *)[self.viewControllers objectAtIndex:1];
    
    // Reset and reload orders, and go to current order
    [ordersNav popToRootViewControllerAnimated:NO];
    UIStoryboard *storyboard = self.storyboard;
    ZEOrderViewController *orderView = [storyboard instantiateViewControllerWithIdentifier:@"OrderViewController"];
    [orderView setOrderId:orderId];
    [ordersNav pushViewController:orderView animated:YES];
    
    // Go to order tab
    self.selectedIndex = 1;
}

- (void)cartViewControllerDidPlaceOrder:(ZECartViewController *)controller id:(NSString *)orderId key:(NSString *)orderKey fromTabIndex:(NSInteger)tabIndex
{
    DLog(@"cartViewControllerDidPlaceOrder:");
    [self placeOrderWithId:orderId key:orderKey fromTabIndex:tabIndex];
}


- (void)epayViewControllerDidPlaceOrder:(ZEEpayViewController *)controller id:(NSString *)orderId key:(NSString *)orderKey fromTabIndex:(NSUInteger)tabIndex
{
    [self placeOrderWithId:orderId key:orderKey fromTabIndex:tabIndex];
}

- (void)ze4TViewControllerDidPlaceOrder:(ZE4TPhoneViewController *)controller id:(NSString *)orderId key:(NSString *)orderKey
{
    [self placeOrderWithId:orderId key:orderKey fromTabIndex:self.tabBarController.selectedIndex];
}

@end
