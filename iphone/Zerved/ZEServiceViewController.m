//
//  ZEServiceViewController.m
//  Zerved
//
//  Created by Anders Rasmussen on 25/09/13.
//
//  Popup to select service (table, counter, delivery...)
//

#import "ZEServiceViewController.h"
#import "ZETextFieldLabelCell.h"
#import "SVProgressHUD.h"
#import "ZEServiceAddressViewController.h"

@interface ZEServiceViewController ()

@end

@implementation ZEServiceViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1) {
        self.navigationController.navigationBar.tintColor = [UIColor blackColor];
    }
    
    NSString *selectedMethod = self.cart.service;
    if ([selectedMethod isEqualToString:@"counter"] && [self.bar hasToStayAndToGo]) {
        if (self.cart.toGo) {
            selectedMethod = [selectedMethod stringByAppendingString:@"_togo"];
        } else {
            selectedMethod = [selectedMethod stringByAppendingString:@"_tostay"];
        }
    }
    self.selectedIndex = [self.methods indexOfObject:selectedMethod];
    [self.tableView reloadData];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(switchToOrder) name:@"switchToOrder" object:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.methods count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
    NSString *method = [self.methods objectAtIndex:indexPath.row];
    if ([method isEqualToString:@"counter"]) {
        cell = [tableView dequeueReusableCellWithIdentifier:@"BasicCell" forIndexPath:indexPath];
        cell.textLabel.text = NSLocalizedString(@"Pickup at counter", nil);
    } else if ([method isEqualToString:@"counter_tostay"]) {
        cell = [tableView dequeueReusableCellWithIdentifier:@"BasicCell" forIndexPath:indexPath];
        cell.textLabel.text = NSLocalizedString(@"Pickup at counter to stay", nil);
    } else if ([method isEqualToString:@"counter_togo"]) {
        cell = [tableView dequeueReusableCellWithIdentifier:@"BasicCell" forIndexPath:indexPath];
        cell.textLabel.text = NSLocalizedString(@"Pickup at counter to go", nil);
    } else if ([method isEqualToString:@"table"]) {
        ZETextFieldLabelCell *tablenumberCell = [tableView dequeueReusableCellWithIdentifier:@"TableNumberCell" forIndexPath:indexPath];
        self.tableNumberField = tablenumberCell.inputField;
        //tablenumberCell.textLabel.text = NSLocalizedString(@"Serve at table", nil);
        tablenumberCell.inputField.text = self.cart.tableNumber;
        tablenumberCell.inputField.delegate = self;
        tablenumberCell.inputField.tag = indexPath.row;
        UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 50)];
        UIBarButtonItem *doneButton = [[UIBarButtonItem alloc]initWithTitle:NSLocalizedString(@"Hide keyboard", nil) style:UIBarButtonItemStyleBordered target:self action:@selector(closeNumberPad)];
        if (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1) {
            doneButton.tintColor = self.view.tintColor;
        }
        numberToolbar.items = [NSArray arrayWithObjects:
                               [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                               doneButton, nil];
        [numberToolbar sizeToFit];
        tablenumberCell.inputField.inputAccessoryView = numberToolbar;
        cell = tablenumberCell;
    } else if ([method isEqualToString:@"delivery"]) {
        cell = [tableView dequeueReusableCellWithIdentifier:@"DeliveryCell" forIndexPath:indexPath];
        //cell.textLabel.text = NSLocalizedString(@"Deliver to address", nil);
    }
    
    if (indexPath.row == self.selectedIndex) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    } else {
        cell.accessoryType = UITableViewCellAccessoryNone;
    }

    return cell;
}
/*
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    if ([self.bar.estimatedWaitingTime intValue] > 0 && !self.cart.event) {
        UILabel *waitLabel = [[UILabel alloc] init];
        waitLabel.frame = CGRectMake(15, 10, 290, 20);
        waitLabel.backgroundColor = [UIColor clearColor];
        waitLabel.textColor = [UIColor colorWithRed:185/255.0 green:74/255.0 blue:72/255.0 alpha:1.0];
        waitLabel.font = [UIFont boldSystemFontOfSize:14];
        waitLabel.textAlignment = NSTextAlignmentCenter;
        waitLabel.text = [NSString stringWithFormat:NSLocalizedString(@"Average waiting time: %@ min.", @"Waiting time estimate in cart"), self.bar.estimatedWaitingTime];
        UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 40)];
        [footerView addSubview:waitLabel];
        return footerView;
    }
    return nil;
}
*/

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    if (self.selectedIndex == indexPath.row) {
        return;
    }
    NSIndexPath *oldIndexPath = [NSIndexPath indexPathForRow:self.selectedIndex inSection:0];
    
    UITableViewCell *newCell = [tableView cellForRowAtIndexPath:indexPath];
    if (newCell.accessoryType == UITableViewCellAccessoryNone) {
        newCell.accessoryType = UITableViewCellAccessoryCheckmark;
        self.selectedIndex = indexPath.row;
    }
    
    UITableViewCell *oldCell = [tableView cellForRowAtIndexPath:oldIndexPath];
    if (oldCell.accessoryType == UITableViewCellAccessoryCheckmark) {
        oldCell.accessoryType = UITableViewCellAccessoryNone;
    }
}

#pragma mark - UITextFieldDelegate Methods

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:textField.tag inSection:0];
    [self tableView:self.tableView didSelectRowAtIndexPath:indexPath];
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    self.cart.tableNumber = textField.text;
}

- (void)closeNumberPad
{
    [self.view endEditing:YES];
}

- (IBAction)done:(id)sender {
    if (self.selectedIndex == NSNotFound) {
        [SVProgressHUD showErrorWithStatus:NSLocalizedString(@"Please select one", nil)];
        return;
    }
    
    NSString *method = [self.methods objectAtIndex:self.selectedIndex];
    if ([method isEqualToString:@"counter"] || [method isEqualToString:@"counter_tostay"] || [method isEqualToString:@"counter_togo"]) {
        self.cart.service = @"counter";
        self.cart.toGo = NO;
    }
    if ([method isEqualToString:@"counter_togo"]) {
        self.cart.toGo = YES;
    }
    if ([method isEqualToString:@"table"]) {
        self.cart.service = @"table";
        self.cart.tableNumber = self.tableNumberField.text;
    }
    
    if ([self.cart.service isEqualToString:@"table"] && [self.cart.tableNumber isEqualToString:@""]) {
        [SVProgressHUD showErrorWithStatus:NSLocalizedString(@"Please enter your table number", nil)];
        [self.tableNumberField becomeFirstResponder];
        return;
    }
    
    if ([method isEqualToString:@"delivery"]) {
        [self performSegueWithIdentifier:@"Delivery" sender:self];
        return;
    }
    
    [self.delegate serviceDone];
}

- (IBAction)cancel:(id)sender {
    [self.delegate serviceCancelled];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"Delivery"]) {
        ZEServiceAddressViewController *destinationController = [segue destinationViewController];
        destinationController.delegate = self.delegate;
        destinationController.cart = self.cart;
        destinationController.bar = self.bar;
    }
}

- (void)switchToOrder
{
    // Close view when switched to order from notification
    [self dismissViewControllerAnimated:NO completion:nil];
}

@end
