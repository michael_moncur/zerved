//
//  ZEOptionViewController.h
//  Zerved
//
//  Created by Anders Rasmussen on 26/09/13.
//
//

#import <UIKit/UIKit.h>
#import "ZEProduct.h"
#import "ZECartItem.h"

@protocol ZESelectOptionDelegate;

@interface ZEOptionViewController : UITableViewController

@property (nonatomic, strong) ZEProduct *product;
@property NSUInteger optionIndex;
@property (weak, nonatomic) id <ZESelectOptionDelegate> delegate;
@property (nonatomic, strong) ZECartItem *cartItem;
@property BOOL goToNext; // YES when used from product list, NO when used from product page
- (IBAction)continue:(id)sender;

@end

@protocol ZESelectOptionDelegate <NSObject>

- (void)selectOptionControllerDidCancel;
@optional
- (void)selectOptionControllerDidCreateCartItem:(ZECartItem *)item;
- (void)selectOptionControllerDidSelectOptions;

@end