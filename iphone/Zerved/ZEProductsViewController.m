//
//  ZEProductsViewController.m
//  Zerved
//
//  Created by Anders Rasmussen on 07/08/13.
//
//  List products in category. The list lets you go to product view, or add
//  directly to cart.
//

#import "ZEProductsViewController.h"
#import "ZEEmptyView.h"
#import "SVProgressHUD.h"
#import "ZEProductCell.h"
#import "ZEAppDelegate.h"
#import "ZECartViewController.h"
#import "ZETabBarController.h"
#import "ZEEventSummaryView.h"

@interface ZEProductsViewController ()

@end

@implementation ZEProductsViewController

@synthesize categoryName = _categoryName;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Create custom cart button
    self.cartButton = [[ZECartButton alloc] initWithFrame:CGRectMake(0, 0, 100, 44)];
    [self.cartButton addTarget:self action:@selector(goToCart:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:self.cartButton];
    ZEAppDelegate *delegate = [[UIApplication sharedApplication] delegate];
    self.cartButton.cart = [delegate.cartManager getCartForLocation:self.location bar:self.bar group:self.group event:self.event];

    [self reload];
}

- (void)viewWillAppear:(BOOL)animated {
    [self.cartButton updateAnimated:NO];
    [self reloadTableView];
    [super viewWillAppear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setCategoryName:(NSString *)categoryName
{
    _categoryName = categoryName;
    self.title = self.categoryName;
}

- (void)reload
{
    [SVProgressHUD showWithStatus:NSLocalizedString(@"Loading", @"Progress indicator loading")];
    RKObjectManager *objectManager = [RKObjectManager sharedManager];
    NSString * url_menu = [NSString stringWithFormat:@"bars/%@/menus/%@/categories/%@/products", self.placeKey, self.menu.key, self.category];
    NSString * url_entrance = [NSString stringWithFormat:@"locations/%@/categories/%@/products", self.placeKey, self.category];
    [objectManager getObjectsAtPath:[self.group isEqualToString:@"menu"] ? url_menu : url_entrance
                         parameters:nil
                            success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
                                // Products loaded
                                [SVProgressHUD dismiss];
                                self.products = [mappingResult array];
                                if (self.event) {
                                    self.openForOrders = [self.event.openForOrders boolValue];
                                } else if ([self.products count] > 0) {
                                    ZEProduct *firstProduct = [self.products firstObject];
                                    self.openForOrders = ([firstProduct.merchantOpen boolValue] && [firstProduct.menuActive boolValue]);
                                }
                                [self reloadTableView];
                            }
                            failure:^(RKObjectRequestOperation *operation, NSError *error) {
                                // Failed to load products
                                [SVProgressHUD dismiss];
                                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil)
                                                                                message:[error localizedDescription]
                                                                               delegate:nil
                                                                      cancelButtonTitle:NSLocalizedString(@"OK", nil)
                                                                      otherButtonTitles:nil];
                                [alert show];
                            }];
}

- (void)reloadTableView
{
    if(self.isViewLoaded) {
        [self.tableView reloadData];
        if ([self.products count]) {
            // Products loaded
            ZEProduct *firstProduct = self.products[0];
            
            // Top msg
            float y = 0;
            UILabel *msg = [[UILabel alloc] initWithFrame:CGRectMake(0, y, self.tableView.frame.size.width, 20)];
            msg.textColor = [UIColor whiteColor];
            msg.textAlignment = NSTextAlignmentCenter;
            msg.font = [UIFont systemFontOfSize:12];
            msg.text = [firstProduct.messageWhenClosed uppercaseString];
            msg.numberOfLines = 0;
            if (self.openForOrders) {
                // Merchant open
                msg.text = [NSLocalizedString(@"Open for orders", @"Green top message in product list when customers may order") uppercaseString];
                msg.backgroundColor = [UIColor colorWithRed:72/255.0f green:217/255.0f blue:116/255.0f alpha:1.0];
            } else {
                // Merchant closed
                msg.text = [firstProduct.messageWhenClosed uppercaseString];
                msg.backgroundColor = [UIColor redColor];
            }
            [msg sizeToFit];
            msg.frame = CGRectMake(0, y, 320, msg.frame.size.height);
            y += msg.frame.size.height;
            
            // Event summary
            if (self.event) {
                ZEEventSummaryView *eventSummaryView = [[NSBundle mainBundle] loadNibNamed:@"EventSummaryView" owner:self options:nil][0];
                eventSummaryView.frame = CGRectMake(0, y, eventSummaryView.frame.size.width, eventSummaryView.frame.size.height);
                [eventSummaryView setEvent:self.event];
                y += eventSummaryView.frame.size.height;
                UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, y)];
                [headerView addSubview:msg];
                [headerView addSubview:eventSummaryView];
                [self.tableView setTableHeaderView:headerView];
            } else {
                UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, y)];
                [headerView addSubview:msg];
                [self.tableView setTableHeaderView:headerView];
            }
        } else {
            // No products
            ZEEmptyView *emptyView = [[NSBundle mainBundle] loadNibNamed:@"EmptyTableView" owner:self options:nil][0];
            [emptyView setupViewForTableView:self.tableView text:nil];
            [self.tableView setTableHeaderView:emptyView];
        }
    }
}

- (void)addToCart:(UIButton *)sender
{
    ZEProduct *product = self.products[(NSUInteger) sender.tag];
    if ([product.productOptions count] > 0) {
        // Start options selector
        [self performSegueWithIdentifier:@"AddToCart" sender:sender];
    } else {
        // No options, add to cart immediately
        ZECartItem *item = [product createCartItem];
        item.productName = product.name;
        item.quantity = 1;
        [self addCartItemToCart:item];
    }
}

- (void)addCartItemToCart:(ZECartItem *)item
{
    [SVProgressHUD showWithStatus:NSLocalizedString(@"Adding to cart", nil)];
    
    // Cart item with options created, add it to cart
    ZEAppDelegate *delegate = [[UIApplication sharedApplication] delegate];
    ZECart *cart = [delegate.cartManager getCartForLocation:self.location bar:self.bar group:self.group event:self.event];
    [cart addItem:item];
    [self loadQuoteAndUpdateItemsAnimated:YES];
}

- (void)loadQuoteAndUpdateItemsAnimated:(BOOL)animated
{
    ZEAppDelegate *delegate = [[UIApplication sharedApplication] delegate];
    ZECart *cart = [delegate.cartManager getCartForLocation:self.location bar:self.bar group:self.group event:self.event];
    // Cancel existing requests
    RKObjectManager *objectManager = [RKObjectManager sharedManager];
    [objectManager cancelAllObjectRequestOperationsWithMethod:RKRequestMethodPOST matchingPathPattern:@"quote"];
    // Load quote from server
    [objectManager postObject:cart path:@"quote" parameters:nil success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
        [SVProgressHUD dismiss];
        // Quote received
        ZEQuote *quote = [mappingResult firstObject];
        [cart attachQuote:quote];
        // Remove invalid items
        [cart validateQuoteItems];
        
        // Reload table rows that contain cart items
        [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationAutomatic];
        /*
        int index = [self.products indexOfObjectPassingTest:^BOOL(id obj, NSUInteger idx, BOOL *stop) {
            return [((ZEProduct *)obj).key isEqualToString:item.productKey];
        }];
        [self.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:index inSection:0]] withRowAnimation:UITableViewRowAnimationAutomatic];
         */
        [self.cartButton updateAnimated:animated];
    } failure:^(RKObjectRequestOperation *operation, NSError *error) {
        [SVProgressHUD dismiss];
        [self.cartButton updateAnimated:NO];
        // Error
        if (error.code == -999) {
            // Don't show error msg for cancelled requests
            return;
        }
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil)
                                                        message:[error localizedDescription]
                                                       delegate:nil
                                              cancelButtonTitle:NSLocalizedString(@"OK", nil)
                                              otherButtonTitles:nil];
        [alert show];
    }];
}

- (void)attachQuantityStepper:(UIStepper *)stepper
{
    [stepper addTarget:self action:@selector(changeQuantity:) forControlEvents:UIControlEventValueChanged];
}

- (IBAction)changeQuantity:(UIStepper *)sender {
    ZEProductCell *cell;
    UIView *v = sender;
    while (v && ![v isKindOfClass:[ZEProductCell class]]) v = v.superview;
    cell = (ZEProductCell *)v;
    ZECartItem *cartItem = cell.items[(NSUInteger) sender.tag];
    ZEAppDelegate *delegate = [[UIApplication sharedApplication] delegate];
    ZECart *cart = [delegate.cartManager getCartForLocation:self.location bar:self.bar group:self.group event:self.event];
    if (cartItem) {
        if ((int)[sender value] == 0) {
            // Remove item
            [cart removeItem:cartItem];
            RKObjectManager *objectManager = [RKObjectManager sharedManager];
            [objectManager cancelAllObjectRequestOperationsWithMethod:RKRequestMethodPOST matchingPathPattern:@"quote"];
            //[self.tableView reloadRowsAtIndexPaths:@[[self.tableView indexPathForCell:cell]] withRowAnimation:UITableViewRowAnimationAutomatic];
            [self reload];
        } else {
            // Update item
            cartItem.quantity = (int) [sender value];
            //[self.tableView reloadRowsAtIndexPaths:@[[self.tableView indexPathForCell:cell]] withRowAnimation:UITableViewRowAnimationNone];
            [self loadQuoteAndUpdateItemsAnimated:NO];
        }
        [self.cartButton updateAnimated:NO];
    }
}

- (void)goToCart:(id)sender
{
    [self performSegueWithIdentifier:@"ViewCart" sender:sender];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"ViewCart"]) {
        ZECartViewController *cartViewController = (ZECartViewController *) [segue destinationViewController];
        cartViewController.delegate = (ZETabBarController *)self.navigationController.tabBarController;
        cartViewController.group = [self.group copy];
        cartViewController.location = self.location;
        cartViewController.bar = self.bar;
        cartViewController.event = self.event;
    } else if ([[segue identifier] isEqualToString:@"AddToCart"]) {
        ZEProduct *product = self.products[(NSUInteger) ((UIButton *) sender).tag];
        UINavigationController *navController = (UINavigationController *) [segue destinationViewController];
        ZEOptionViewController *optionController = (ZEOptionViewController *) navController.viewControllers[0];
        optionController.product = product;
        optionController.optionIndex = 0;
        optionController.goToNext = YES;
        optionController.delegate = self;
        ZECartItem *item = [product createCartItem];
        item.productName = product.name;
        item.quantity = 1;
        optionController.cartItem = item;
    } else if ([[segue identifier] isEqualToString:@"ViewProductDetails"]) {
        ZEProductViewController *productController = (ZEProductViewController *) [segue destinationViewController];
        productController.delegate = self;
        productController.group = [self.group copy];
        productController.location = self.location;
        productController.bar = self.bar;
        productController.event = self.event;
        productController.product = self.products[(NSUInteger) [self.tableView indexPathForSelectedRow].row];
    }
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [self.products count] > 0 ? 1 : 0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.products count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"ProductCell";
    ZEProductCell *cell = (ZEProductCell *) [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    // Configure the cell...
    ZEProduct *product = self.products[(NSUInteger) indexPath.row];
    ZEAppDelegate *delegate = [[UIApplication sharedApplication] delegate];
    ZECart *cart = [delegate.cartManager getCartForLocation:self.location bar:self.bar group:self.group event:self.event];
    [cell setProduct:product andCart:cart andProductController:self andOpen:self.openForOrders];

    [cell.addToCartButton setTag:indexPath.row];
    [cell.addToCartButton addTarget:self action:@selector(addToCart:) forControlEvents:UIControlEventTouchUpInside];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ZEProduct *product = self.products[(NSUInteger) indexPath.row];
    ZEAppDelegate *delegate = [[UIApplication sharedApplication] delegate];
    ZECart *cart = [delegate.cartManager getCartForLocation:self.location bar:self.bar group:self.group event:self.event];
    return [ZEProductCell heightForProduct:product andCart:cart];
}

#pragma mark - Table view delegate
/*
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}
*/
#pragma mark - Action sheet delegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0) {
        [self performSegueWithIdentifier:@"ViewCart" sender:self];
    }
}

#pragma mark - ZESelectOptionDelegate Methods

- (void)selectOptionControllerDidCreateCartItem:(ZECartItem *)item
{
    [self addCartItemToCart:item];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)selectOptionControllerDidCancel
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - ZEProductViewDelegate Methods

- (void)productViewDidAddToCart
{
    [SVProgressHUD showWithStatus:NSLocalizedString(@"Adding to cart", nil)];
    [self.navigationController popViewControllerAnimated:YES];;
    [self loadQuoteAndUpdateItemsAnimated:YES];
}

@end
