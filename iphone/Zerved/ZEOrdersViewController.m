//
//  ZEOrdersViewController.m
//  Zerved
//
//  Created by Anders Rasmussen on 05/07/13.
//
//  List consumer's orders, grouped by open and closed orders.
//

#import "ZEOrdersViewController.h"
#import <RestKit/RestKit.h>
#import "Order.h"
#import "ZEAppDelegate.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "ZEOrderViewController.h"
#import "ZEEmptyView.h"
#import "ZELocationCell.h"

@interface ZEOrdersViewController ()
@property(nonatomic, strong) NSArray *openOrders;
@property(nonatomic, strong) NSArray *closedOrders;
@end

@implementation ZEOrdersViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
        self.openOrders = @[];
        self.closedOrders = @[];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1) {
        self.navigationController.navigationBar.tintColor = [UIColor blackColor];
    }
    self.title = NSLocalizedString(@"Orders", @"Title for order list");

    ZEEmptyView *emptyView = [[[NSBundle mainBundle] loadNibNamed:@"EmptyTableView" owner:self options:nil] objectAtIndex:0];
    [emptyView setupViewForTableView:self.tableView text:@""];
    [self.tableView setTableHeaderView:emptyView];
}

- (void)viewDidAppear:(BOOL)animated
{
    [self reload];
}

- (void)reload {
    // Check login and fetch from server
    [SVProgressHUD showWithStatus:NSLocalizedString(@"Loading", nil)];
    ZEConsumerManager *consumerManager = ((ZEAppDelegate *)[[UIApplication sharedApplication] delegate]).consumer;
    if ([consumerManager.accessToken isEqualToString:@""]) {
        consumerManager.delegate = self;
        [consumerManager authenticateFromView:self];
        return;
    }
    
    [[RKObjectManager sharedManager] getObjectsAtPath:[NSString stringWithFormat:@"consumers/%@/orders/open,closed", [consumerManager consumerKey]] parameters:nil success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
        DLog(@"Success %@", mappingResult);
        NSMutableArray *mutableOpenOrders = [NSMutableArray array];
        NSMutableArray *mutableClosedOrders = [NSMutableArray array];
        for (Order *order in [mappingResult array]) {
            if ([@"pending" isEqualToString:order.status] || [@"preparing" isEqualToString:order.status]) {
                [mutableOpenOrders addObject:order];
            } else if ([@"complete" isEqualToString:order.status]) {
                [mutableClosedOrders addObject:order];
            }
        }
        NSSortDescriptor *dateDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"dateCreated" ascending:NO];
        self.openOrders = [mutableOpenOrders sortedArrayUsingDescriptors:@[dateDescriptor]];
        self.closedOrders = [mutableClosedOrders sortedArrayUsingDescriptors:@[dateDescriptor]];
        [self reloadTableView];
    } failure:^(RKObjectRequestOperation *operation, NSError *error) {
        DLog(@"Error %@", error);
        [self reloadTableView];
    }];
}

- (void)reloadTableView
{
    [SVProgressHUD dismiss];
    if(self.isViewLoaded) {
        [self.tableView reloadData];
        if ([self.openOrders count] || [self.closedOrders count]) {
            [self.tableView setTableHeaderView:nil];
        } else {
            ZEEmptyView *emptyView = [[[NSBundle mainBundle] loadNibNamed:@"EmptyTableView" owner:self options:nil] objectAtIndex:0];
            [emptyView setupViewForTableView:self.tableView text:NSLocalizedString(@"You have no orders", nil)];
            [self.tableView setTableHeaderView:emptyView];
        }
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (Order *)orderAtIndexPath:(NSIndexPath *)indexPath
{
    // First section is open orders, except when there are zero open orders, then it's the closed orders
    Order *order;
    if (indexPath.section == 0 && [self.openOrders count] > 0) {
        order = self.openOrders[(NSUInteger) indexPath.row];
    }  else {
        order = self.closedOrders[(NSUInteger) indexPath.row];
    }
    return order;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"ShowOrder"]) {
        ZEOrderViewController *destinationController = [segue destinationViewController];
        Order *order = [self orderAtIndexPath:[self.tableView indexPathForSelectedRow]];
        [destinationController setOrderKey:[order.key copy]];
    }
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    int sections = 0;
    if ([self.openOrders count]) sections += 1;
    if ([self.closedOrders count]) sections += 1;
    return sections;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // First section is open orders, except when there are zero open orders, then it's the closed orders
    if (section == 0) {
        NSInteger openOrders = [self.openOrders count];
        if (openOrders > 0) {
            return openOrders;
        } else {
            return [self.closedOrders count];
        }
    } else {
        return [self.closedOrders count];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"OrderCell";
    ZELocationCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    // Configure the cell...
    Order *order = [self orderAtIndexPath:indexPath];
    cell.titleLabel.text = [NSString stringWithFormat:@"#%@ %@", order.queueNumber, order.locationName];
    cell.subtitleLabel.text = order.dateCreatedFormatted;
    // On old iPhones the images make the app crash with memory warning
    if (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1) {
        [cell.locationImage sd_setImageWithURL:[NSURL URLWithString:order.merchantImageUrl] placeholderImage:[UIImage imageNamed:@"bar_logo_dummy.png"] options:SDWebImageRefreshCached];
    }
    return cell;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    // First section is open orders, except when there are zero open orders, then it's the closed orders
    if (section == 0 && [self.openOrders count] > 0) {
        return NSLocalizedString(@"Open", nil);
    } else {
        return NSLocalizedString(@"Closed", nil);
    }
}

#pragma mark - Table view delegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Larger cells (default is approx. 45)
    return 70;
}

#pragma mark - Consumer delegate

- (void)didAuthenticate
{
    [self reload];
}

- (void)didFailToAuthenticate
{
    
}

@end
