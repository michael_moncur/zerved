//
//  ZEChoosePasswordViewController.m
//  Zerved
//
//  Created by Anders Rasmussen on 06/03/13.
//
//

#import "ZEChoosePasswordViewController.h"
#import "SVProgressHUD.h"

@interface ZEChoosePasswordViewController ()

@end

@implementation ZEChoosePasswordViewController

@synthesize validationLabelLetters, validationLabelNumbers, validationLabelLength, passwordField, delegate;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [self.passwordField becomeFirstResponder];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setPasswordField:nil];
    [self setValidationLabelLetters:nil];
    [self setValidationLabelNumbers:nil];
    [self setValidationLabelLength:nil];
    //[[NSNotificationCenter defaultCenter] removeObserver:self];
    [super viewDidUnload];
}

- (IBAction)skip:(id)sender {
    [self.delegate choosePasswordDidCancel:self];
}

- (IBAction)save:(id)sender {
    if ([self validatePassword]) {
        [self.delegate choosePasswordDidEnterPassword:self password:self.passwordField.text];
    } else {
        [SVProgressHUD showErrorWithStatus:NSLocalizedString(@"Invalid password", nil)];
    }
}

- (IBAction)typePassword:(UITextField *)sender {
    [self validatePassword];
}

- (BOOL)validatePassword {
    // Validate password
    NSUInteger letterLength = [self.passwordField.text rangeOfCharacterFromSet:[NSCharacterSet letterCharacterSet]].length;
    NSUInteger decimalLength = [self.passwordField.text rangeOfCharacterFromSet:[NSCharacterSet decimalDigitCharacterSet]].length;
    BOOL lettersValid = (letterLength > 0);
    BOOL numbersValid = (decimalLength > 0);
    BOOL lengthValid = ([self.passwordField.text length] >= 6);
    
    // Update validation labels
    UIColor *validColor = [UIColor colorWithRed:30/255.0f green:147/255.0f blue:0.0f alpha:1.0f];
    UIColor *invalidColor = [UIColor darkGrayColor];
    self.validationLabelLetters.text = [NSString stringWithFormat:@"%@ %@", (lettersValid ? @"✓" : @"➞"), NSLocalizedString(@"Letters", @"Password validation label for letters")];
    self.validationLabelLetters.textColor = lettersValid ? validColor : invalidColor;
    self.validationLabelNumbers.text = [NSString stringWithFormat:@"%@ %@", (numbersValid ? @"✓" : @"➞"), NSLocalizedString(@"Numbers", @"Password validation label for numbers")];

    self.validationLabelNumbers.textColor = numbersValid ? validColor : invalidColor;
    self.validationLabelLength.text = [NSString stringWithFormat:@"%@ %@", (lengthValid ? @"✓" : @"➞"), NSLocalizedString(@"Min. 6 characters", @"Password validation label for length")];
    self.validationLabelLength.textColor = lengthValid ? validColor : invalidColor;
    self.validationLabelLetters.hidden = NO;
    self.validationLabelNumbers.hidden = NO;
    self.validationLabelLength.hidden = NO;
    
    // Return validation result
    return (lettersValid && numbersValid && lengthValid);
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"LoginInfo"]) {
        UINavigationController *navController = [segue destinationViewController];
        ZELoginInfoViewController *loginInfoController = [navController.viewControllers objectAtIndex:0];
        loginInfoController.delegate = self;
    }
}

#pragma mark -
#pragma mark ZELogininfoDelegate Methods
- (void)loginInfoControllerDone:(ZELoginInfoViewController *)loginInfoController
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark -
#pragma mark UITextFieldDelegate Methods

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self save:nil];
    return YES;
}

@end
