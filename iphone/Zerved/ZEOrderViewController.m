//
//  ZEOrderViewController.m
//  Zerved
//
//  Created by Zerved Development Mac on 15/08/13.
//
//  View an order. Offline caching is used to show an order even if offline.
//  Buttons allow order completion or cancellation, and sending email receipt.
//

#import "ZEOrderViewController.h"
#import "ZEReceiptLineView.h"
#import "ZEMessageView.h"
#import "ZEQueueNumberView.h"
#import "ZEOrderStatusChangeRequest.h"

@interface ZEOrderViewController ()
@property (strong) UIAlertView *sessionExpiredAlertView;
@end

@implementation ZEOrderViewController
@synthesize orderId = _orderId, orderKey = _orderKey;
@synthesize pinController = _pinController;
@synthesize pincode = _pincode;

#define RECEIPT_LINE_HEIGHT 15
#define MARGIN 15

- (id)init
{
    self = [super init];
    if (self) {
        self.showStatusMessage = NO;
        self.showNotificationsMessage = NO;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reload) name:UIApplicationWillEnterForegroundNotification object:nil];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIApplicationWillEnterForegroundNotification
                                                  object:nil];
}

- (void)viewWillAppear:(BOOL)animated
{
    
    // Add grey background
    /*UIImageView *backgroundImage;
    if ([[UIScreen mainScreen] bounds].size.height == 568) {
        backgroundImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"bg_grey-568h.png"]];
    } else {
        backgroundImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"bg_grey.png"]];
    }
    [self.view addSubview:backgroundImage];
    [self.view sendSubviewToBack:backgroundImage];*/
    
    if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1) {
        self.navigationController.navigationBar.tintColor = [UIColor blackColor];
    }
    
    // Hides the receipt view while being loaded
    [self.receiptView setHidden:YES];

    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    BOOL pushPermissionAsked = [defaults boolForKey:@"pushPermissionAsked"];
    if (pushPermissionAsked) {
        // Add registration for remote notifications
        
        if ([[UIApplication sharedApplication] respondsToSelector:@selector(isRegisteredForRemoteNotifications)]) {
            // iOS 8 Notifications
            [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
        }
        else {
            // iOS < 8 Notifications
            [[UIApplication sharedApplication] registerForRemoteNotificationTypes:
             (UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeSound)];
        }
    } else {
        // Show push notification information popup
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Important", @"Push permission title") message:NSLocalizedString(@"We need your permission to notify you when your order is ready. Please accept push notifications in the next dialog box.", @"Push permission message") delegate:self cancelButtonTitle:NSLocalizedString(@"Got it", @"Push permission button") otherButtonTitles:nil, nil];
        [alert setTag:2];
        [alert show];
    }
    
    // Push permission callbacks
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshPushPermissionError) name:@"pushRegistrationCompleted" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshPushPermissionError) name:@"pushRegistrationFailed" object:nil];
   
    [self reload];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(IBAction)refresh:(id)sender{
    [self reload];
}

- (void)reload
{
    BOOL orderIdOrKeyAvailable = NO;
    
    if (self.orderId && ![self.orderId isEqualToString:@""]) {
        orderIdOrKeyAvailable = YES;
    }

    if (self.orderKey && ![self.orderKey isEqualToString:@""]) {
        orderIdOrKeyAvailable = YES;
    }

    if (!orderIdOrKeyAvailable) {
        // No need to call server without orderid and/or orderkey.
        return;
    }
    
    [SVProgressHUD showWithStatus:NSLocalizedString(@"Loading", nil)];
    // Loads the order from server
    RKObjectManager *objectManager = [RKObjectManager sharedManager];
    [objectManager getObjectsAtPath:[NSString stringWithFormat:@"orders/%@", (self.orderKey && ![self.orderKey isEqualToString:@""] ? self.orderKey : self.orderId)]
                         parameters:nil
                            success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
                                self.order = [mappingResult array][0];
                                [self refreshView];
                                [SVProgressHUD dismiss];
                            }
                            failure:^(RKObjectRequestOperation *operation, NSError *e) {
                                [self handleRestFailure:operation error:e];
                            }];
}

- (void)handleRestFailure:(RKObjectRequestOperation *)operation error:(NSError *)error
{
    [SVProgressHUD dismiss];
    if (operation.HTTPRequestOperation.response.statusCode == 401) {
        // Need to login
		if (!self.sessionExpiredAlertView.visible)
		{
			self.sessionExpiredAlertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Session expired", nil)
																	  message:NSLocalizedString(@"Zerved could not access your account", nil)
																	 delegate:self
															cancelButtonTitle:NSLocalizedString(@"Cancel", nil)
															otherButtonTitles:NSLocalizedString(@"Reconnect", nil), nil];
			[self.sessionExpiredAlertView setTag:3];
			[self.sessionExpiredAlertView show];
		}
    } else {
        // Other error or offline
    }
}

- (void) refreshView
{
    if(self.order != nil){
        self.title = [NSString stringWithFormat:@"#%@", self.order.queueNumber];
    }
    
    self.current_y = 0;
    
    // Removes all subviews except the receipt (receipt lines are removed later)
    for(UIView * view in self.scrollView.subviews){
        if(![view isMemberOfClass:[ZEReceiptView class]])
            [view removeFromSuperview];
    }
    
    // Push notifications message
    // if((![self.order.status isEqualToString:@"complete"]) && self.showNotificationsMessage == YES)
    //    [self addPushNotificationsMessageView];
    
    // Order status message
    if(self.order.statusTitleForConsumer != nil || self.order.statusDescriptionForConsumer != nil)
        [self addStatusMessageView];
    // Order number
    [self addQueueNumberView];
    
    // Order number and status
    [self addOrderStatusView];
    
    // Service type
    [self addServiceView];
    
    // Order comment
    //if(self.order.comment != nil)
    //    [self addCommentMessageView];
    
    // Estimated waiting time or event
    if (self.order.eventName && ![self.order.eventName isEqualToString:@""]) {
        [self addEventView];
    }
    else if([self.order.estimatedWaitingTime intValue] > 0) {
        [self addEstimatedWaitingTimeView];
    }
    
    // Staff button: Accept Order
    if([self.order.service isEqualToString:@"entrance"] && [self.order.status isEqualToString:@"pending"]) {
        //[self addOrderButtonViewOfType:1 labelText:NSLocalizedString(@"This button is for staff only:", nil) labelColor:[UIColor redColor] buttonText:NSLocalizedString(@"Staff: Accept Order", nil) buttonAction:@selector(completeOrder)];
        [self addCompleteButtonWithButtonText:NSLocalizedString(@"Staff: Accept Order", nil) labelText:NSLocalizedString(@"This button is for staff only:", nil)];
    }
    
    // Staff button: Serve & Complete
    if([self.order.status isEqualToString:@"preparing"] && self.order.serveWithConsumerDevice) {
        //[self addOrderButtonViewOfType:1 labelText:NSLocalizedString(@"This button is for staff only:", nil) labelColor:[UIColor redColor] buttonText:NSLocalizedString(@"Staff: Serve & Complete", nil) buttonAction:@selector(completeOrder)];
        [self addCompleteButtonWithButtonText:NSLocalizedString(@"Staff: Serve & Complete", nil) labelText:NSLocalizedString(@"This button is for staff only:", nil)];
    }
    
    // Receipt
    [self addReceiptView];
    
    // Send by email button
    if([self.order.status isEqualToString:@"complete"]){
        [self addEmailButton];
    }
    // Cancel button
    if([self.order.status isEqualToString:@"pending"]) {
        //[self addOrderButtonViewOfType:0 labelText:@"" labelColor:nil buttonText:NSLocalizedString(@"Cancel", nil) buttonAction:@selector(cancelOrder)];
        [self addCancelButton];
    }
    
    // Text depending on status
    
    if(![self.order.status isEqualToString:@"cancelled"]) {
        self.current_y += MARGIN;
        UILabel *text = [[UILabel alloc] initWithFrame:CGRectMake(MARGIN, self.current_y, 320 - (2 * MARGIN), 32)];
        text.textColor = [UIColor colorWithRed:(164.f/255.f) green:(64.f/255.f) blue:(64.f/255.f) alpha:1];
        text.backgroundColor = [UIColor colorWithWhite:0 alpha:0];
        text.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:12.0f];
        text.numberOfLines = 0;
        text.lineBreakMode = NSLineBreakByWordWrapping;
        text.textAlignment = NSTextAlignmentCenter;
        if([self.order.status isEqualToString:@"complete"])
            text.text = NSLocalizedString(@"The money has been withdrawn from your credit card.", nil);
        else text.text = NSLocalizedString(@"The money will be withdrawn once your order is delivered.", nil);
		[self.scrollView addSubview:text];
        self.current_y += 32 + MARGIN;
    }
    // Update scrollView's height
    self.scrollView.contentSize = CGSizeMake(320, self.current_y);
    
    // Show the receipt view previously hidden
    [self.receiptView setHidden:NO];
}

- (void) addPushNotificationsMessageView
{
    ZEMessageView *msgView = [[ZEMessageView alloc] initWithFrame:CGRectMake(MARGIN, self.current_y, 320 - (2 * MARGIN - 2), 120)];
    [msgView setMessageTitle:NSLocalizedString(@"Please enable push notifications", nil)];
    [msgView setMessageContent:NSLocalizedString(@"We are unable to notify you about your order status. Please go to the phone's Settings > Notifications > Zerved and allow alerts and sounds.", nil)];
    [self.scrollView addSubview:msgView];
    self.current_y += msgView.frame.size.height + MARGIN;
}

- (void) addStatusMessageView
{
    ZEMessageView *msgView = [[ZEMessageView alloc] initWithFrame:CGRectMake(0, self.current_y, 320, 80)];
    [msgView setMessageTitle:self.order.statusTitleForConsumer];
    [msgView setMessageContent:self.order.statusDescriptionForConsumer];
	if ([self.order.status isEqualToString:@"pending_payment"]) {
		// Pending payment (Paii)
		msgView.backgroundColor = [UIColor colorWithRed:1.f green:19.f/255.f blue:0.f alpha:1.f];
	} else if ([self.order.eventName isEqualToString:@""]) {
        // Realtime order
        msgView.backgroundColor = [UIColor colorWithRed:72/255.0f green:217/255.0f blue:116/255.0f alpha:1.0];
    } else {
        // Event order
        msgView.backgroundColor = [UIColor orangeColor];
    }
    [self.scrollView addSubview:msgView];
    self.current_y += msgView.frame.size.height + MARGIN;
}

- (void) addCommentMessageView
{
    ZEMessageView *msgView = [[ZEMessageView alloc] initWithFrame:CGRectMake(MARGIN, self.current_y, 320 - (2 * MARGIN - 2), 120)];
    [msgView setCommentTheme:self.order.comment];
    [self.scrollView addSubview:msgView];
    self.current_y += msgView.frame.size.height + MARGIN;
}

- (void) addQueueNumberView
{
    ZEQueueNumberView *queueView = [[ZEQueueNumberView alloc] initWithFrame:CGRectMake(MARGIN, self.current_y, 320 - (2 * MARGIN - 2), 70)];
    [queueView setQueueNumber:self.order.queueNumber];
    [self.scrollView addSubview:queueView];
    self.current_y += queueView.frame.size.height + MARGIN;
}

- (void) addEstimatedWaitingTimeView
{
    /*
    ZEEstimatedWaitingTimeView *ewtView = [[ZEEstimatedWaitingTimeView alloc] initWithFrame:CGRectMake(MARGIN, self.current_y, 320 - (2 * MARGIN - 2), 30)];
    [ewtView setWaitingTime:self.order.estimatedWaitingTime];
    [self.scrollView addSubview:ewtView];
    self.current_y += ewtView.frame.size.height + MARGIN;
     */
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, self.current_y, self.scrollView.frame.size.width, 20)];
    label.textColor = [UIColor darkGrayColor];
    label.textAlignment = NSTextAlignmentCenter;
    label.backgroundColor = [UIColor clearColor];
    label.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:15.0f];
    label.numberOfLines = 0;
    label.text = [NSString stringWithFormat:NSLocalizedString(@"Average waiting time: %d min.", @"Minutes waiting time"), [self.order.estimatedWaitingTime intValue]];
    [self.scrollView addSubview:label];
    self.current_y += label.frame.size.height + MARGIN;
}

- (void)addEventView
{
    // Extra margin
    self.current_y += MARGIN;
    
    // Event name
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, self.current_y, self.scrollView.frame.size.width, 20)];
    label.textColor = [UIColor darkGrayColor];
    label.textAlignment = NSTextAlignmentCenter;
    label.backgroundColor = [UIColor clearColor];
    label.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:15.0f];
    label.numberOfLines = 0;
    label.text = self.order.eventName;
    [self.scrollView addSubview:label];
    self.current_y += label.frame.size.height;
    
    // Event time
    UILabel *label2 = [[UILabel alloc] initWithFrame:CGRectMake(0, self.current_y, self.scrollView.frame.size.width, 15)];
    label2.textColor = [UIColor darkGrayColor];
    label2.textAlignment = NSTextAlignmentCenter;
    label2.backgroundColor = [UIColor clearColor];
    label2.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:12.0f];
    label2.numberOfLines = 0;
    label2.text = [NSString stringWithFormat:@"%@ %@", self.order.eventTimeLabel, self.order.dateDeliveryFormatted];
    [self.scrollView addSubview:label2];
    self.current_y += label2.frame.size.height + MARGIN;
}

- (void) addOrderStatusView
{
    /*
    ZEOrderStatusView *orderStatusView = [[ZEOrderStatusView alloc] initWithFrame:CGRectMake(MARGIN, self.current_y, 320 - (2 * MARGIN - 2), 30)] ;
    [orderStatusView setOrderStatus:self.order.status];
    [self.scrollView addSubview:orderStatusView];
    self.current_y += orderStatusView.frame.size.height + MARGIN;
     */
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, self.current_y, self.scrollView.frame.size.width, 20)];
    label.textColor = [UIColor darkGrayColor];
    label.textAlignment = NSTextAlignmentCenter;
    label.backgroundColor = [UIColor clearColor];
    label.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:15.0f];
    label.numberOfLines = 0;
    label.text = [NSString stringWithFormat:NSLocalizedString(@"Status: %@", @"Order status label"), self.order.statusLabel];
    [self.scrollView addSubview:label];
    self.current_y += label.frame.size.height;
}

- (void)addServiceView
{
    if (self.order.serviceSummary != nil) {
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, self.current_y, self.scrollView.frame.size.width, 20)];
        label.textColor = [UIColor darkGrayColor];
        label.textAlignment = NSTextAlignmentCenter;
        label.backgroundColor = [UIColor clearColor];
        label.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:15.0f];
        label.numberOfLines = 0;
        label.text = [NSString stringWithFormat:NSLocalizedString(@"Service type: %@", @"Order label for service type counter/table/delivery"), self.order.serviceSummary];
        [self.scrollView addSubview:label];
        self.current_y += label.frame.size.height;
    }
}

- (void)addCompleteButtonWithButtonText:(NSString *)buttonText labelText:(NSString *)lblText
{
    // Label before button
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, self.current_y, self.scrollView.frame.size.width, 30)];
    label.textColor = [UIColor darkGrayColor];
    label.textAlignment = NSTextAlignmentCenter;
    label.backgroundColor = [UIColor clearColor];
    label.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:12.0f];
    label.numberOfLines = 0;
    label.text = lblText;
    [self.scrollView addSubview:label];
    self.current_y += label.frame.size.height;
    
    // Button
    UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(40, self.current_y, self.scrollView.frame.size.width - 80, 40)];
    button.backgroundColor = [UIColor colorWithRed:72/255.0f green:217/255.0f blue:116/255.0f alpha:1.0];
    [button.layer setMasksToBounds:YES];
    [button.layer setCornerRadius:3.0f];
    button.titleLabel.textAlignment = NSTextAlignmentCenter;
    button.titleLabel.textColor = [UIColor whiteColor];
    button.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:18.0f];
    [button setTitle:buttonText forState:UIControlStateNormal];
    [button addTarget:self action:@selector(completeOrder) forControlEvents:UIControlEventTouchUpInside];
    [self.scrollView addSubview:button];
    self.current_y += button.frame.size.height + MARGIN;
}

- (void)addCancelButton
{
    UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(40, self.current_y, self.scrollView.frame.size.width - 80, 40)];
    button.backgroundColor = [UIColor redColor];
    [button.layer setMasksToBounds:YES];
    [button.layer setCornerRadius:3.0f];
    button.titleLabel.textAlignment = NSTextAlignmentCenter;
    button.titleLabel.textColor = [UIColor whiteColor];
    button.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:18.0f];
    [button setTitle:NSLocalizedString(@"Cancel", nil) forState:UIControlStateNormal];
    [button addTarget:self action:@selector(cancelOrderClick) forControlEvents:UIControlEventTouchUpInside];
    [self.scrollView addSubview:button];
    self.current_y += button.frame.size.height + MARGIN;
}

- (void)addEmailButton
{
    // Label before button
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, self.current_y, self.scrollView.frame.size.width, 30)];
    label.textColor = [UIColor darkGrayColor];
    label.textAlignment = NSTextAlignmentCenter;
    label.backgroundColor = [UIColor clearColor];
    label.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:12.0f];
    label.numberOfLines = 0;
    NSString * s = (self.order.consumerHasEmail) ?
    NSLocalizedString(@"The receipt will be sent to the email address you provided under \"Profile\"", nil) :
    NSLocalizedString(@"Go to \"Profile\" to add your email address", nil) ;
    label.text = s;
    [self.scrollView addSubview:label];
    self.current_y += label.frame.size.height;
    
    // Button
    UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(40, self.current_y, self.scrollView.frame.size.width - 80, 40)];
    button.backgroundColor = [UIColor colorWithRed:72/255.0f green:217/255.0f blue:116/255.0f alpha:1.0];
    [button.layer setMasksToBounds:YES];
    [button.layer setCornerRadius:3.0f];
    button.titleLabel.textAlignment = NSTextAlignmentCenter;
    button.titleLabel.textColor = [UIColor whiteColor];
    button.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:18.0f];
    [button setTitle:NSLocalizedString(@"Send receipt by email", nil) forState:UIControlStateNormal];
    [button addTarget:self action:@selector(sendReceipt) forControlEvents:UIControlEventTouchUpInside];
    [self.scrollView addSubview:button];
    self.current_y += button.frame.size.height + MARGIN;
}

- (void) addReceiptView{
    
    int pos = 0;
    self.receiptView.frame = CGRectMake(self.receiptView.frame.origin.x, self.current_y, self.receiptView.frame.size.width, [self getReceiptTotalHeight]);
    self.receiptView.content.frame = CGRectMake(5, self.receiptView.top.frame.size.height, 320, [self getReceiptContentHeight]);
    for(ZEReceiptLineView *view in self.receiptView.content.subviews){
        [view removeFromSuperview];
    }
    // Header
    pos = [self addReceiptLine:@"" align:NSTextAlignmentCenter pos:pos important:NO];
    pos = [self addReceiptLine:self.order.companyName right:self.order.locationName pos:pos important:NO shareEven:YES];
    pos = [self addReceiptLine:[NSString stringWithFormat:NSLocalizedString(@"VAT no. %@", nil), self.order.companyVat] right:self.order.dateCreatedFormatted pos:pos important:NO shareEven:NO];
    pos = [self addReceiptLine:@"" right:[NSString stringWithFormat:NSLocalizedString(@"Order no. %@", nil), self.order.orderNumber] pos:pos important:NO shareEven:NO];
    pos = [self addReceiptLine:@"" align:NSTextAlignmentRight pos:pos important:NO];
    
    // Items and options
    for(OrderItem *item in self.order.items){
        pos = [self addReceiptLine:[NSString stringWithFormat:@"%@ x %@", item.quantity, item.productName] right:item.totalBeforeLoyaltyDiscountInclTaxFormatted pos:pos important:YES shareEven:NO];
        for(OrderItemOption * option in item.optionValues){
            pos = [self addReceiptLine:[NSString stringWithFormat:@"    %@:", option.optionName] align:NSTextAlignmentLeft pos:pos important:YES];
            for(OrderItemOptionValue * value in option.values){
                pos = [self addReceiptLine:[NSString stringWithFormat:@"      %@", value.valueLabel] align:NSTextAlignmentLeft pos:pos important:YES];
            }
        }
    }
	if (self.order.totalLoyaltyDiscountInclTax.floatValue != 0.f)
	{
		pos = [self addReceiptLine:NSLocalizedString(@"Discount", nil) right:self.order.totalLoyaltyDiscountInclTaxFormatted pos:pos important:YES shareEven:NO];
	}
    // Totals
    pos = [self addReceiptLine:@"____________________________________________" align:NSTextAlignmentRight pos:pos important:NO];
    pos = [self addReceiptLine:@"" align:NSTextAlignmentCenter pos:pos important:NO];
    pos = [self addReceiptLine:[NSString stringWithFormat:@"%@: %@", NSLocalizedString(@"Total excl. Tax", nil), self.order.totalExclTaxFormatted] align:NSTextAlignmentRight pos:pos important:YES];
    for (TaxTotal *total in self.order.groupedTaxTotals)
	{
		NSString *rateAndTotalFormatted = [NSString stringWithFormat:@" %@%%: %@", total.rate, total.totalFormatted];
		NSString *totalName = total.name;
		if (totalName.length + rateAndTotalFormatted.length > 32)
		{
			totalName = [NSString stringWithFormat:@"%@...", [totalName substringToIndex:32 - rateAndTotalFormatted.length - 3]];
		}
        pos = [self addReceiptLine:[NSString stringWithFormat:@"%@%@", totalName, rateAndTotalFormatted] align:NSTextAlignmentRight pos:pos important:YES];
    }
    pos = [self addReceiptLine:[NSString stringWithFormat:@"%@: %@", NSLocalizedString(@"Total incl. Tax", nil), self.order.totalInclTaxFormatted] align:NSTextAlignmentRight pos:pos important:YES];
    pos = [self addReceiptLine:@"" align:NSTextAlignmentCenter pos:pos important:NO];

    self.current_y += self.receiptView.frame.size.height + MARGIN;
}

- (int)addReceiptLine:(NSString *)left right:(NSString *)right pos:(int)pos important:(BOOL)important shareEven:(BOOL)shareEven {
    ZEReceiptLineView *line = [[NSBundle mainBundle] loadNibNamed:@"ReceiptLineView" owner:self options:nil][0];
    line.frame = CGRectMake(0, RECEIPT_LINE_HEIGHT * pos, 310, RECEIPT_LINE_HEIGHT);
    line.isItem = important;
    [line setTextWithLeft:left Right:right important:important shareSpaceEven:shareEven];
    [self.receiptView.content addSubview:line];
    return pos + 1;
}

- (int)addReceiptLine:(NSString *)text align:(NSTextAlignment)align pos:(int)pos important:(BOOL)important {
    ZEReceiptLineView *line = [[NSBundle mainBundle] loadNibNamed:@"ReceiptLineView" owner:self options:nil][0];
    line.frame = CGRectMake(0, RECEIPT_LINE_HEIGHT * pos, 310, RECEIPT_LINE_HEIGHT);
    line.isItem = important;
    [line setText:text textAlign:align];
    [self.receiptView.content addSubview:line];
    return pos + 1;
}

- (int) getNumberOfReceiptLines
{
    int n = 10; // Header and Total lines
    n += [self.order.groupedTaxTotals count]; // tax lines
    for(OrderItem * item in self.order.items){
        n += 1; // item
        for(OrderItemOption * option in item.optionValues) {
            if ([option.values count] > 0) {
                n += 1 + [option.values count]; // option title plus values
            }
        }
    }
	if (self.order.totalLoyaltyDiscountInclTax.floatValue != 0.f) n++;
    return n;
}

- (CGFloat) getReceiptContentHeight
{
    return RECEIPT_LINE_HEIGHT * [self getNumberOfReceiptLines];
}

- (CGFloat) getReceiptTotalHeight
{
    return [self getReceiptContentHeight] + self.receiptView.top.frame.size.height + self.receiptView.bottom.frame.size.height - 1 ;
}

- (void) refreshPushPermissionError
{
    /*
    // Disabled, because for some reason, the warning is displayed on the first order view despite pushPermissionAsked
     
    // Toggle push permission warning
    UIRemoteNotificationType types = [[UIApplication sharedApplication] enabledRemoteNotificationTypes];
    BOOL pushEnabled = types != UIRemoteNotificationTypeNone;
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    BOOL pushPermissionAsked = [defaults boolForKey:@"pushPermissionAsked"];
    if (pushEnabled || !pushPermissionAsked) {
        self.showNotificationsMessage = NO;
        self.showStatusMessage = YES;
    } else {
        self.showNotificationsMessage = YES;
        self.showStatusMessage = NO;
    }
    */
}

- (void) completeOrder
{
    self.pincode = self.order.pincode;
    [self performSegueWithIdentifier:@"ShowPin" sender:self];
}

- (void) sendReceipt
{
    if (!self.order.consumerHasEmail){
        [SVProgressHUD showErrorWithStatus:NSLocalizedString(@"You need to enter your email address under \"Profile\"", nil)];
        return;
    }
    [SVProgressHUD showWithStatus:NSLocalizedString(@"Sending", @"Progress indicator for send receipt button")];
    RKObjectManager *objectManager = [RKObjectManager sharedManager];
    [objectManager.HTTPClient postPath:[NSString stringWithFormat:@"orders/%@/sendreceipt", self.order.key] parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [SVProgressHUD showSuccessWithStatus:NSLocalizedString(@"Receipt sent", nil)];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        // Server error
        [SVProgressHUD showErrorWithStatus:error.localizedDescription];
    }];
}

- (void)cancelOrderClick
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Do you want to cancel the order?", nil) message:nil delegate:self cancelButtonTitle:NSLocalizedString(@"Keep order", @"Button to cancel an order cancellation") otherButtonTitles:NSLocalizedString(@"Cancel order", @"Button to confirm an order cancellation"), nil];
    [alert setTag:1];
    [alert show];
}

- (void) cancelOrder
{
    [SVProgressHUD showWithStatus:NSLocalizedString(@"Loading", nil)];
    ZEOrderStatusChangeRequest *newStatus = [ZEOrderStatusChangeRequest alloc];
    newStatus.status = @"cancelled";
    RKObjectManager *objectManager = [RKObjectManager sharedManager];
    [objectManager postObject:newStatus path:[NSString stringWithFormat:@"orders/%@", self.order.key] parameters:nil success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
        // Refresh updated order
        self.order = [mappingResult array][0];
        [self refreshView];
        [SVProgressHUD dismiss];
        [self.navigationController popViewControllerAnimated:YES];
    } failure:^(RKObjectRequestOperation *operation, NSError *error) {
        // Server error
        [SVProgressHUD showErrorWithStatus:error.localizedDescription];
    }];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"ShowPin"]) {
        UINavigationController *pinNavController = (UINavigationController *) [segue destinationViewController];
        self.pinController = (ZEPinViewController *) pinNavController.viewControllers[0];
        self.pinController.delegate = self;
        self.pinController.title = NSLocalizedString(@"Staff PIN", @"PIN view title for staff processing");
        self.pinController.header = NSLocalizedString(@"Enter staff PIN", @"PIN view header for staff processing");
        self.pinController.subtitle = NSLocalizedString(@"to approve the order", @"PIN view subtitle for staff processing");
        self.pinController.helpText = NSLocalizedString(@"Staff only", @"PIN view help text for staff processing");
    } else {
        [super prepareForSegue:segue sender:sender];
    }
}

- (NSString *)getUrl:(NSString *)url
{
    return [NSString stringWithFormat:@"%@%@", SERVER_URL, url];
}

- (void)pinViewControllerDidEnterPin:(ZEPinViewController *)controller pin:(NSString *)pin
{
    if ([pin isEqualToString:self.pincode]) {
        // Correct pin entered - complete order
        [SVProgressHUD showWithStatus:NSLocalizedString(@"Processing order", nil)];
        ZEOrderStatusChangeRequest *newStatus = [ZEOrderStatusChangeRequest alloc];
        newStatus.status = @"complete";
        newStatus.entrancePincode = pin;
        RKObjectManager *objectManager = [RKObjectManager sharedManager];
        [objectManager postObject:newStatus path:[NSString stringWithFormat:@"orders/%@", self.order.key] parameters:nil success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
            // Refresh updated order
            self.order = [mappingResult array][0];
            [self refreshView];
            [self.pinController dismiss];
            [self dismissViewControllerAnimated:YES completion:nil];
        } failure:^(RKObjectRequestOperation *operation, NSError *error) {
            // Server error
            [self.pinController showError:error.localizedDescription];
        }];
    } else {
        // Wrong pincode entered
        [self.pinController showError:NSLocalizedString(@"Wrong code", nil)];
    } 
}

- (void)pinViewControllerDidCancel:(ZEPinViewController *)controller
{
    [self.pinController dismiss];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)pinViewControllerForgotPin:(ZEPinViewController *)controller
{
    
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 2 && buttonIndex == 0) {
        // Add registration for remote notifications
        if ([[UIApplication sharedApplication] respondsToSelector:@selector(isRegisteredForRemoteNotifications)]) {
            // iOS 8 Notifications
            [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
        }
        else {
            // iOS < 8 Notifications
            [[UIApplication sharedApplication] registerForRemoteNotificationTypes:
             (UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeSound)];
        }
      
        // Remember permission has been asked
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setBool:YES forKey:@"pushPermissionAsked"];
    }
    else if (alertView.tag == 1 && buttonIndex == 1) {
        [self cancelOrder];
    } else if (alertView.tag == 3 && buttonIndex == 1) {
        // Log in
		self.sessionExpiredAlertView.delegate = nil;
		self.sessionExpiredAlertView = nil;
        ZEConsumerManager *consumerManager = ((ZEAppDelegate *)[[UIApplication sharedApplication] delegate]).consumer;
        consumerManager.delegate = self;
        [consumerManager authenticateFromView:self];
    }
}

- (void)dealloc
{
	self.sessionExpiredAlertView.delegate = nil;
	self.sessionExpiredAlertView = nil;
}

#pragma mark - Consumer delegate

- (void)didAuthenticate
{
    [self reload];
}

- (void)didFailToAuthenticate
{
    
}

@end
