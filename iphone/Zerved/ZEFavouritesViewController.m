//
//  ZEFavouritesViewController.m
//  Zerved
//
//  Created by Zerved Development Mac on 26/07/13.
//
//  List of favourite products divided by location/bar. The favourites contain
//  option values and can be added to cart directly from here.
//

#import "ZEFavouritesViewController.h"
#import "ZEAppDelegate.h"
#import "SVProgressHUD.h"
#import "ZEFavourite.h"
#import "ZEFavouriteCell.h"
#import "ZEConsumerManager.h"
#import "ZECartItem.h"
#import "ZEProductOption.h"
#import "ZEProductOptionValue.h"
#import "ZECartItemOptionValue.h"
#import "ZEEmptyView.h"
#import <RestKit/RestKit.h>
#import "QuartzCore/CAGradientLayer.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import <objc/runtime.h>
#import "ZECartViewController.h"
#import "ZELocationViewController.h"
#import "ZEBarCategoriesViewController.h"
#import "ZETabBarController.h"
#import "ZEBarMenusViewController.h"
#import "ZEProductCartItemView.h"

@interface ZEFavouritesViewController ()

@end

const char goToCart;
const char deleteFav;

@implementation ZEFavouritesViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1) {
        self.navigationController.navigationBar.tintColor = [UIColor blackColor];
    }
    self.title = NSLocalizedString(@"Favourites", nil);
}

- (void)viewDidAppear:(BOOL)animated
{
    [self reload];
}

-(IBAction)refresh:(id)sender
{
    [self reload];
}

- (void)reload
{       
    [SVProgressHUD showWithStatus:NSLocalizedString(@"Loading", nil)];
    ZEConsumerManager *consumerManager = ((ZEAppDelegate *)[[UIApplication sharedApplication] delegate]).consumer;
    
    // Needs to be logged in
    if ([consumerManager.accessToken isEqualToString:@""]) {
        consumerManager.delegate = self;
        [consumerManager authenticateFromView:self];
        return;
    }
    
    RKObjectManager *objectManager = [RKObjectManager sharedManager];
    [objectManager getObjectsAtPath:[NSString stringWithFormat:@"consumers/%@/favourites", consumerManager.consumerKey]
                         parameters:nil
                            success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
                                // Consumer data received, update view
                                [SVProgressHUD dismiss];
                                self.favourites = [[NSMutableArray alloc] initWithArray:[mappingResult array]];
                                for (ZEFavourite * favourite in self.favourites) {
                                    favourite.optionsString = @"";
                                    for(int i=0; i < [favourite.options count]; i++){
                                        ZECartItemOptionValue * fav_option = (ZECartItemOptionValue *)[favourite.options objectAtIndex:i];
                                        for(int j=0; j < [favourite.product.productOptions count]; j++){
                                            ZEProductOption * pro_option = (ZEProductOption *)[favourite.product.productOptions objectAtIndex:j];
                                            if([pro_option.optionId isEqualToString:fav_option.optionId]){
                                                for(int k = 0; k < [pro_option.values count]; k++){
                                                    ZEProductOptionValue *pro_option_value = (ZEProductOptionValue *)[pro_option.values objectAtIndex:k];
                                                    if([pro_option_value.index intValue] == fav_option.selectedIndex){
                                                        favourite.optionsString = [NSString stringWithFormat:@"%@%@: %@.  ", favourite.optionsString, pro_option.name, pro_option_value.label];
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                [self.tableView reloadData];
                                [self.tableView setTableHeaderView:nil];
                                if ([self.favourites count] == 0) {
                                    ZEEmptyView *emptyView = [[[NSBundle mainBundle] loadNibNamed:@"EmptyTableView" owner:self options:nil] objectAtIndex:0];
                                    [emptyView setupViewForTableView:self.tableView text:NSLocalizedString(@"Save your favourite items here by tapping the star when you order", @"Text shown in the empty view when no favourite items saved")];
                                    [self.tableView setTableHeaderView:emptyView];
                                }                           
                            }
                            failure:^(RKObjectRequestOperation *operation, NSError *error) {
                                [SVProgressHUD dismiss];
                                [SVProgressHUD showErrorWithStatus:NSLocalizedString(@"Favourites can't be loaded.", @"Error msg when favourites can't be retrieved")];
                            }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source

// Saves the bars and locations names and returns the number of sections
-(int) getNumberOfSections
{
    int sections = 0;
    self.barsAndLocations = [[NSMutableArray alloc] init];
    for(ZEFavourite * favourite in self.favourites){
        NSString * sectionName = [self getSectionNameFromLocation:favourite.location andBar:favourite.bar];
        if(favourite.bar != nil && [self.barsAndLocations containsObject:sectionName] == NO){
            [self.barsAndLocations addObject:sectionName];
            sections++;
        }
        else if(favourite.location != nil && favourite.bar == nil && [self.barsAndLocations containsObject:favourite.location.name] == NO){
            [self.barsAndLocations addObject:favourite.location.name];
            sections++;
        }
    }
    return sections;
}

-(int) getNumberOfRowsInSection:(NSInteger)section
{
    int rows = 0;
    for(ZEFavourite * favourite in self.favourites){
        if( (favourite.bar != nil && [[self getSectionNameFromLocation:favourite.location andBar:favourite.bar] isEqualToString:[self.barsAndLocations objectAtIndex:section]] )
         || (favourite.location != nil && favourite.bar == nil && [favourite.location.name isEqualToString:[self.barsAndLocations objectAtIndex:section]] ))
            rows++;
    }
    return rows;
}

-(NSMutableArray *) getFavouritesForSection:(NSInteger)section
{
    NSMutableArray *favourites = [[NSMutableArray alloc] init];
    for(ZEFavourite *favourite in self.favourites){
        if( (favourite.bar != nil && [[self getSectionNameFromLocation:favourite.location andBar:favourite.bar] isEqualToString:[self.barsAndLocations objectAtIndex:section]] )
           || (favourite.location != nil && favourite.bar == nil &&  [favourite.location.name isEqualToString:[self.barsAndLocations objectAtIndex:section]] )){
            [favourites addObject:favourite];
        }
    }
    return favourites;
}

- (NSString *) getSectionNameFromLocation:(ZELocation *)location andBar:(ZEBar *)bar
{
    return (bar != nil) ? [NSString stringWithFormat:@"%@: %@", location.name, bar.name] : location.name;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    ZEFavourite * favourite = (ZEFavourite *)[ [self getFavouritesForSection:indexPath.section] objectAtIndex:indexPath.row];
    ZECartItem *favouriteItem = [self createCartItemForFavourite:favourite];
    ZECart *cart = [self cartForFavourite:favourite];
    float height = 41;
    if (favourite.optionsString && ![favourite.optionsString isEqualToString:@""]) {
        height += 41; // Make room for the options
    } else {
        height += 15; // Even without options, add some empty space to avoid plus buttons to close to each other
    }
    if ([cart.items containsObject:favouriteItem]) {
        height += 49; // Yellow shopping cart line
    }
    return height;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [self getNumberOfSections];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self getNumberOfRowsInSection:section];
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return [self.barsAndLocations objectAtIndex:section];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 56;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    ZEFavourite *firstFavourite = [[self getFavouritesForSection:section] objectAtIndex:0];
    ZECart *cart = [self cartForFavourite:firstFavourite];
    
    UIView *sectionHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, tableView.sectionHeaderHeight)];
    sectionHeaderView.backgroundColor = [UIColor colorWithRed:235/255.0f green:235/255.0f blue:235/255.0f alpha:1.0];
    sectionHeaderView.userInteractionEnabled = YES;
    sectionHeaderView.tag = section;
    
    UILabel *headerLabel = [[UILabel alloc] initWithFrame:CGRectMake(15, 10, tableView.bounds.size.width-10-100, 36)];
    headerLabel.backgroundColor = [UIColor clearColor];
    headerLabel.textColor = [UIColor blackColor];
    headerLabel.font = [UIFont boldSystemFontOfSize:17];
    headerLabel.text = [self tableView:tableView titleForHeaderInSection:section];
    
    UIButton *cartButton = [UIButton buttonWithType:UIButtonTypeCustom];
    cartButton.frame = CGRectMake(tableView.bounds.size.width-100, 10, 90, 36);
    cartButton.backgroundColor = [UIColor clearColor];
    [cartButton.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue" size:17]];
    cartButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    [cartButton setTitleColor:[UIColor colorWithRed:0.0f green:153/255.0f blue:0.0f alpha:1.0f] forState:UIControlStateNormal];
    cartButton.tintColor = [UIColor colorWithRed:0.0f green:153/255.0f blue:0.0f alpha:1.0f];
    [cartButton setTag:section];
    [cartButton addTarget:self action:@selector(goToCartForPlace:) forControlEvents:UIControlEventTouchUpInside];
    int items = [cart count];
    NSString* itemsString = [NSString stringWithFormat:NSLocalizedString(@"Cart (%d)", @"Cart button with number of items"), items];
    [cartButton setTitle:itemsString forState:UIControlStateNormal];
    [cartButton setHidden:(items == 0)];
    [sectionHeaderView addSubview:headerLabel];
    [sectionHeaderView addSubview:cartButton];
    
    return sectionHeaderView;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ZEFavourite * favourite = (ZEFavourite *)[ [self getFavouritesForSection:indexPath.section] objectAtIndex:indexPath.row];
    
    static NSString *CellIdentifier = @"FavouriteCell";
    ZEFavouriteCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    cell.favourite = favourite;
    
    // Product name
    cell.name.text = [NSString stringWithFormat:@"%@", favourite.product.name ];
    float y = 33;
    // Displays the product options
    cell.options.text = favourite.optionsString;
    if (favourite.optionsString && ![favourite.optionsString isEqualToString:@""]) {
        y += 41;
    } else {
        y += 15;
    }
    
    // Add to cart
    [cell.addToCart addTarget:self action:@selector(addToCart:) forControlEvents:UIControlEventTouchUpInside];
    
    // Delete
    [cell.deleteFavourite addTarget:self action:@selector(deleteFavourite:) forControlEvents:UIControlEventTouchUpInside];
    
    // Cart item
    y += 10;
    [[cell.itemsView subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
    [cell.itemsView setFrame:CGRectMake(cell.itemsView.frame.origin.x, y, cell.itemsView.frame.size.width, 49)];
    ZECartItem *favouriteItem = [self createCartItemForFavourite:favourite];
    ZECart *cart = [self cartForFavourite:favourite];
    NSUInteger existingIndex = [cart.items indexOfObject:favouriteItem];
    if (existingIndex != NSNotFound) {
        // Favourite is in cart
        ZECartItem *item = [cart.items objectAtIndex:existingIndex];
        ZEProductCartItemView *itemView = [[[NSBundle mainBundle] loadNibNamed:@"ProductCartItemView" owner:self options:nil] objectAtIndex:0];
        itemView.frame = CGRectMake(0, 0, 305, 49);
        itemView.quantityLabel.text = [NSString stringWithFormat:@"%d", item.quantity];
        [itemView.quantityStepper setValue:(double)item.quantity];
        [itemView attachQuantityStepper];
        [itemView.quantityStepper addTarget:self action:@selector(changeQuantity:) forControlEvents:UIControlEventValueChanged];
        if (item.quoteItem) {
            itemView.priceLabel.text = item.quoteItem.priceFormatted;
        }
        [cell.itemsView addSubview:itemView];
    }
    
    return cell;
}

#pragma mark - Cart related methods

-(void) addToCart:(id)sender
{
    // Adds a favourite item to the cart
    ZEFavouriteCell *cell;
    UIView *v = sender;
    while (v && ![v isKindOfClass:[ZEFavouriteCell class]]) v = v.superview;
    cell = (ZEFavouriteCell *)v;
    ZEFavourite *favourite = cell.favourite;
    
    ZECartItem *item = [self createCartItemForFavourite:favourite];
    ZECart *cart = [self cartForFavourite:favourite];
    [cart addItem:item];
    
    [SVProgressHUD showSuccessWithStatus:NSLocalizedString(@"Added to Cart", nil)];
    //[self reload];
    [self loadQuoteForCart:cart andUpdateCell:[self.tableView indexPathForCell:cell]];
}

- (IBAction)changeQuantity:(UIStepper *)sender {
    ZEFavouriteCell *cell;
    UIView *v = sender;
    while (v && ![v isKindOfClass:[ZEFavouriteCell class]]) v = v.superview;
    cell = (ZEFavouriteCell *)v;
    ZEFavourite *favourite = cell.favourite;
    ZECartItem *item = [self createCartItemForFavourite:favourite];
    ZECart *cart = [self cartForFavourite:favourite];
    if ([cart.items containsObject:item]) {
		ZECartItem *cartItem = [cart.items objectAtIndex:[cart.items indexOfObject:item]];
		if (cartItem) {
			if ((int)[sender value] == 0) {
				// Remove item
				[cart removeItem:cartItem];
				RKObjectManager *objectManager = [RKObjectManager sharedManager];
				[objectManager cancelAllObjectRequestOperationsWithMethod:RKRequestMethodPOST matchingPathPattern:@"quote"];
				//[self.tableView reloadSections:[NSIndexSet indexSetWithIndex:[self.tableView indexPathForCell:cell].section] withRowAnimation:UITableViewRowAnimationAutomatic];
				[self reload];
			} else {
				// Update item
				cartItem.quantity = [sender value];
				//[self.tableView reloadRowsAtIndexPaths:@[[self.tableView indexPathForCell:cell]] withRowAnimation:UITableViewRowAnimationNone];
				[self loadQuoteForCart:cart andUpdateCell:[self.tableView indexPathForCell:cell]];
			}
		}
    }
}

- (ZECart *)cartForFavourite:(ZEFavourite *)favourite {
    ZEAppDelegate *delegate = [[UIApplication sharedApplication] delegate];
    if(favourite.bar != nil)
        return [delegate.cartManager getCartForLocation:nil bar:favourite.bar group:@"menu" event:nil];
    else
        return [delegate.cartManager getCartForLocation:favourite.location bar:nil group:@"entrance" event:nil];
}

- (ZECartItem *)createCartItemForFavourite:(ZEFavourite *)favourite {
    ZECartItem *item = [[ZECartItem alloc] initWithProductKey:favourite.product.key quantity:1 options:favourite.options];
    item.productName = favourite.product.name;
    return item;
}

-(void) goToCart:(ZEFavourite *) favourite{
    
    ZECartViewController *cartViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ZECartViewController"];
    
    if(favourite.bar != nil){
        RKObjectManager *objectManager = [RKObjectManager sharedManager];
        [objectManager getObjectsAtPath:[NSString stringWithFormat:@"bars/%@/menus", favourite.bar.key]
                             parameters:nil
                                success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
                                    [SVProgressHUD dismiss];
                                    favourite.bar.menus = [[NSMutableArray alloc] initWithArray:[mappingResult array]];
                                    if ([favourite.bar.menus count] == 1) {
                                        // Directly go to the menu categories
                                        ZEBarCategoriesViewController *barCategoriesViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"BarCategoriesViewController"];
                                        barCategoriesViewController.location = favourite.location;
                                        barCategoriesViewController.title = favourite.bar.name;
                                        barCategoriesViewController.bar = favourite.bar;
                                        barCategoriesViewController.menu = [favourite.bar.menus objectAtIndex:0];
                                        [self.navigationController pushViewController:barCategoriesViewController animated:NO];
                                    }
                                    else {
                                        ZEBarMenusViewController * barMenusViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"BarMenusViewController"];
                                        barMenusViewController.title = favourite.bar.name;
                                        barMenusViewController.bar = favourite.bar;
                                        barMenusViewController.location = favourite.location;
                                        [self.navigationController pushViewController:barMenusViewController animated:NO];
                                    }
                                    
                                    cartViewController.delegate = (ZETabBarController *)self.navigationController.tabBarController;
                                    cartViewController.bar = favourite.bar;
                                    cartViewController.location = favourite.location;
                                    cartViewController.group = @"menu";
                                    [self.navigationController pushViewController:cartViewController animated:YES];
                                }
                                failure:^(RKObjectRequestOperation *operation, NSError *error) {
                                    [SVProgressHUD dismiss];
                                    [SVProgressHUD showErrorWithStatus:NSLocalizedString(@"Can't load bar menu cards", @"Error msg when menu cards can't be retrieved")];
                                }];
    }
    else {
        ZELocationViewController * locationViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ZELocationViewController"];
        locationViewController.locationKey = favourite.location.key;
        [self.navigationController pushViewController:locationViewController animated:NO];
        
        cartViewController.delegate = (ZETabBarController *)self.navigationController.tabBarController;
        cartViewController.bar = nil;
        cartViewController.location = favourite.location;
        cartViewController.group = @"entrance";
        [self.navigationController pushViewController:cartViewController animated:YES];
    }
}

- (void)goToCartForPlace:(UIButton *)sender
{
    // Use any favourite in the section to get the location/bar and go to cart
    ZEFavourite *firstFavourite = [[self getFavouritesForSection:sender.tag] objectAtIndex:0];
    [self goToCart:firstFavourite];
}

- (void)loadQuoteForCart:(ZECart *)cart andUpdateCell:(NSIndexPath *)indexPath
{
    // Cancel existing requests
    RKObjectManager *objectManager = [RKObjectManager sharedManager];
    [objectManager cancelAllObjectRequestOperationsWithMethod:RKRequestMethodPOST matchingPathPattern:@"quote"];
    // Load quote from server
    [objectManager postObject:cart path:@"quote" parameters:nil success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
        [SVProgressHUD dismiss];
        // Quote received
        ZEQuote *quote = [mappingResult firstObject];
        [cart attachQuote:quote];
        // Remove invalid items
        [cart validateQuoteItems];
        
        // Reload table section
        [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:indexPath.section] withRowAnimation:UITableViewRowAnimationAutomatic];
    } failure:^(RKObjectRequestOperation *operation, NSError *error) {
        [SVProgressHUD dismiss];
        // Error
        if (error.code == -999) {
            // Don't show error msg for cancelled requests
            return;
        }
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil)
                                                        message:[error localizedDescription]
                                                       delegate:nil
                                              cancelButtonTitle:NSLocalizedString(@"OK", nil)
                                              otherButtonTitles:nil];
        [alert show];
    }];
}

#pragma mark - Delete favourite

// Displays an alert to confirm the deletion
-(void) deleteFavourite:(id)sender
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Do you want to remove this item from favourites?", nil) message:nil delegate:self cancelButtonTitle:NSLocalizedString(@"Keep favourite", @"Button for cancelling the deletion of a favourite item") otherButtonTitles:nil];
    [alert addButtonWithTitle:NSLocalizedString(@"Delete favourite", @"Button to confirm deletion of favourite item")];
    
    ZEFavouriteCell *cell;
    UIView *v = sender;
    while (v && ![v isKindOfClass:[ZEFavouriteCell class]]) v = v.superview;
    cell = (ZEFavouriteCell *)v;
    ZEFavourite *favourite = cell.favourite;

    // Allows to pass the favourite to alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
    objc_setAssociatedObject(alert, &deleteFav, favourite, OBJC_ASSOCIATION_RETAIN_NONATOMIC);

    [alert show];
}

// Confirm deletion or cancel
- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    // Cancel pressed
    if (buttonIndex != 1)
        return;
    
    // Retrieve the favourite
    ZEFavourite *favourite = objc_getAssociatedObject(alertView, &deleteFav);
    // Delete the favourite
    ZEAppDelegate *delegate = [[UIApplication sharedApplication] delegate];
    [delegate.consumer deleteConsumerFavourite:favourite updateView:self];

}

#pragma mark - Authentication

- (void)didAuthenticate
{
    [self reload];
}

- (void)didFailToAuthenticate
{
    [SVProgressHUD showErrorWithStatus:NSLocalizedString(@"Failed to authenticate", nil)];
}

@end
