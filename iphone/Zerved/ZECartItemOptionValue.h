//
//  ZECartItemOptionValue.h
//  Zerved
//
//  Created by Anders Rasmussen on 28/05/13.
//
//

#import <Foundation/Foundation.h>

@interface ZECartItemOptionValue : NSObject

//@property (nonatomic, strong) NSString *optionKey;
@property (nonatomic, strong) NSString *optionId;
@property (nonatomic) NSInteger selectedIndex;

- (id)initWithOptionId:(NSString *)optionId selectedIndex:(NSInteger)selectedIndex;
- (id)jsonObject;

@end
