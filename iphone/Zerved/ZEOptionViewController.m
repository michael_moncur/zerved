//
//  ZEOptionViewController.m
//  Zerved
//
//  Created by Anders Rasmussen on 26/09/13.
//
//  This view is used as a modal popup from the product list and from the product view.
//  In the product list, it is used when clicking add to cart and goes to the next option when done.
//  In the product view, it is used to select a single option and returns to the product view when done.
//

#import "ZEOptionViewController.h"
#import "ZEProductOption.h"
#import "ZEProductOptionValue.h"
#import "SVProgressHUD.h"

@interface ZEOptionViewController ()

@end

@implementation ZEOptionViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1) {
        self.navigationController.navigationBar.tintColor = [UIColor blackColor];
    }
    
    self.title = [self getProductOption].name;
    
    if (self.optionIndex == 0 && self.goToNext) {
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Cancel", nil) style:UIBarButtonItemStylePlain target:self action:@selector(cancel)];
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(cancel) name:@"switchToOrder" object:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (ZEProductOption *)getProductOption {
    return (ZEProductOption *)[self.product.productOptions objectAtIndex:self.optionIndex];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[self getProductOption].values count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"OptionValueCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    ZEProductOptionValue *value = [[self getProductOption].values objectAtIndex:indexPath.row];
    cell.textLabel.text = value.label;
    cell.detailTextLabel.text = value.priceLabel;
    cell.accessoryType = value.selected ? UITableViewCellAccessoryCheckmark : UITableViewCellAccessoryNone;
    
    return cell;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if ([[self getProductOption].selectionType isEqualToString:@"single"]) {
        if ([[self getProductOption].required boolValue]) {
            return NSLocalizedString(@"Select one", @"Instructions for mandatory single-select option");
        } else {
            return NSLocalizedString(@"Select one (optional)", @"Instructions for optional single-select option");
        }
    } else {
        if ([[self getProductOption].required boolValue]) {
            return NSLocalizedString(@"Select at least one", @"Instructions for mandatory multi-select option");
        } else {
            return NSLocalizedString(@"Select one or more (optional)", @"Instructions for optinal multi-select option");
        }
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    ZEProductOption *productOption = [self getProductOption];
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    if ([productOption.selectionType isEqualToString:@"single"]) {
        // Single selection
        if (cell.accessoryType == UITableViewCellAccessoryNone) {
            // Update cart item
            [self.cartItem clearOptionId:productOption.optionId];
            [self.cartItem.options addObject:[[ZECartItemOptionValue alloc] initWithOptionId:productOption.optionId selectedIndex:indexPath.row]];
            // Update product selection
            [[self getProductOption] setSelectedIndex:indexPath.row];
            // Remove old checkmarks
            for (int i=0; i<[self tableView:tableView numberOfRowsInSection:indexPath.section]; i++) {
                UITableViewCell *oldCell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:i inSection:indexPath.section]];
                oldCell.accessoryType = UITableViewCellAccessoryNone;
            }
            // Add new checkmarks
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
        } else if (cell.accessoryType == UITableViewCellAccessoryCheckmark && ![productOption.required boolValue]) {
            // Deselecting (only allowed if not required)
            // Remove option value
            cell.accessoryType = UITableViewCellAccessoryNone;
            [self.cartItem removeOptionWithOptionId:productOption.optionId value:indexPath.row];
            [[self getProductOption] setSelectedIndex:-1];
        }
    } else if ([productOption.selectionType isEqualToString:@"multiple"]) {
        // Multiple selection
        if (cell.accessoryType == UITableViewCellAccessoryNone) {
            // Add option value
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
            [self.cartItem.options addObject:[[ZECartItemOptionValue alloc] initWithOptionId:productOption.optionId selectedIndex:indexPath.row]];
            ((ZEProductOptionValue *)[[self getProductOption].values objectAtIndex:indexPath.row]).selected = YES;
        } else if (cell.accessoryType == UITableViewCellAccessoryCheckmark) {
            // Remove option value
            cell.accessoryType = UITableViewCellAccessoryNone;
            [self.cartItem removeOptionWithOptionId:productOption.optionId value:indexPath.row];
            ((ZEProductOptionValue *)[[self getProductOption].values objectAtIndex:indexPath.row]).selected = NO;
        }
    }
}

- (IBAction)continue:(id)sender {
    // Validate
    if ([[self getProductOption].required boolValue] && [self getProductOption].selectedIndex == -1) {
        [SVProgressHUD showErrorWithStatus:[NSString stringWithFormat:NSLocalizedString(@"Please select %@", @"Error msg for required option selection"), [self getProductOption].name]];
        return;
    }
    
    if (self.goToNext) {
        if ([self.product.productOptions count] > self.optionIndex + 1) {
            // More options, go to next
            ZEOptionViewController *nextOption = [self.storyboard instantiateViewControllerWithIdentifier:@"OptionController"];
            nextOption.product = self.product;
            nextOption.optionIndex = self.optionIndex + 1;
            nextOption.delegate = self.delegate;
            nextOption.cartItem = self.cartItem;
            nextOption.goToNext = self.goToNext;
            [self.navigationController pushViewController:nextOption animated:YES];
        } else {
            // No more options, return to product list
            
            [self.delegate selectOptionControllerDidCreateCartItem:self.cartItem];
        }
    } else {
        // Return to product view
        [self.delegate selectOptionControllerDidSelectOptions];
    }
}

- (void)cancel {
    [self.delegate selectOptionControllerDidCancel];
}

@end
