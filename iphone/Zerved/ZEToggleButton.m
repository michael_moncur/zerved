//
//  ZEToggleButton.m
//  Zerved
//
//  Created by Anders Rasmussen on 04/10/13.
//
//

#import "ZEToggleButton.h"

@implementation ZEToggleButton

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self setBackgroundImage:[UIImage imageNamed:@"chkbox_unchecked.png"] forState:UIControlStateNormal];
        [self setBackgroundImage:[UIImage imageNamed:@"chkbox_checked.png"] forState:UIControlStateSelected];
        [self setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
        [self setContentEdgeInsets:UIEdgeInsetsMake(0, 38, 0, 0)];
        [self addTarget:self action:@selector(toggleOption:) forControlEvents:UIControlEventTouchUpInside];
    }
    return self;
}

- (void)toggleOption:(UIButton *)sender
{
    self.selected = !self.selected;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
