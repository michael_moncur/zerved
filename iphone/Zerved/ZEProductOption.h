//
//  ZEProductOption.h
//  Zerved
//
//  Created by Anders Rasmussen on 06/06/13.
//
//

#import <Foundation/Foundation.h>

@interface ZEProductOption : NSObject

@property (nonatomic, copy) NSString *key;
@property (nonatomic, copy) NSString *optionId;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *selectionType;
@property (nonatomic, copy) NSNumber *required;
@property (nonatomic, strong) NSArray *values;

- (NSInteger)selectedIndex;
- (void)setSelectedIndex:(NSInteger)index;

@end
