//
//  ZEBarsViewController.h
//  Zerved
//
//  Created by Anders Rasmussen on 24/10/13.
//
//

#import <UIKit/UIKit.h>
#import "ZELocation.h"
#import "ZEBar.h"
#import "ZEEvent.h"

@interface ZEBarsViewController : UITableViewController

@property (nonatomic, strong) ZELocation *location;
@property (nonatomic, strong) NSArray *bars;
@property (nonatomic, strong) ZEEvent *event;

@end
