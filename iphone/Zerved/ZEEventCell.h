//
//  ZEEventCell.h
//  Zerved
//
//  Created by Anders Rasmussen on 24/10/13.
//
//

#import <UIKit/UIKit.h>

@interface ZEEventCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *subtitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *subSubtitleLabel;
@property (weak, nonatomic) IBOutlet UIImageView *clockIcon;

@end
