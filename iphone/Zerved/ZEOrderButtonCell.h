//
//  ZEOrderButtonCell.h
//  Zerved
//
//  Created by Zerved Development Mac on 19/08/13.
//
//

#import <UIKit/UIKit.h>

@interface ZEOrderButtonCell : UITableViewCell

@property(nonatomic, retain) IBOutlet UILabel *label;
@property(nonatomic, retain) IBOutlet UIButton *button;


@end
