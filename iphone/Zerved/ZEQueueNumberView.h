//
//  ZEQueueNumberView.h
//  Zerved
//
//  Created by Zerved Development Mac on 18/09/13.
//
//

#import <UIKit/UIKit.h>
#import "QuartzCore/CAGradientLayer.h"
#import <QuartzCore/QuartzCore.h>

@interface ZEQueueNumberView : UIView

@property (nonatomic, retain) CAGradientLayer *grad;

@property (nonatomic, retain) UILabel *orderNrLabel;
@property (nonatomic, retain) UILabel *queueNr;
- (void) setQueueNumber:(NSString *) q;

@end
