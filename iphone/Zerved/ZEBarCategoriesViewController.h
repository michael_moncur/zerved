//
//  ZEBarCategoriesViewController.h
//  Zerved
//
//  Created by Anders Rasmussen on 07/08/13.
//
//

#import <UIKit/UIKit.h>
#import <RestKit/RestKit.h>
#import "ZELocation.h"
#import "ZEBar.h"
#import "ZEMenu.h"
#import "ZEEvent.h"
#import "ZECartButton.h"

@interface ZEBarCategoriesViewController : UITableViewController

@property (strong, nonatomic) ZELocation *location;
@property (strong, nonatomic) ZEBar *bar; // Bar for this view
@property (strong, nonatomic) ZEMenu *menu;
@property (nonatomic, strong) ZEEvent *event;
@property (strong, nonatomic) NSArray *categories;
@property (strong, nonatomic) ZECartButton *cartButton;

@end
