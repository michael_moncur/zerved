//
//  ZEWebViewController.h
//  Zerved
//
//  Created by Anders Rasmussen on 11/04/12.
//  Copyright (c) 2012 Zerved. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZEConsumerDelegate.h"

@interface ZEWebViewController : UIViewController <UIWebViewDelegate, ZEConsumerDelegate, UIAlertViewDelegate>

@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property BOOL loginCancelled;
- (void)handleCall:(NSString*)functionName callbackId:(int)callbackId args:(NSDictionary*)args;
- (void)returnResult:(int)callbackId args:(id)arg, ...;
- (NSString *)getUrl:(NSString *)url;
//- (NSMutableURLRequest *)getRequest:(NSString *)url;
- (void)reload;
- (void)showErrorPageInWebView:(UIWebView *)webView withMessage:(NSString *)message;
- (IBAction)reload:(id)sender;
- (void)addGreyBackground;
@end
