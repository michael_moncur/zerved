//
//  ZEReceiptLineView.h
//  Zerved
//
//  Created by Zerved Development Mac on 19/08/13.
//
//

#import <UIKit/UIKit.h>

@interface ZEReceiptLineView : UIView

@property (nonatomic, retain) IBOutlet UIImageView *background;
@property (nonatomic, retain) IBOutlet UILabel *label;
@property BOOL isItem;

- (void) setText:(NSString *) string textAlign:(NSTextAlignment)alignment;
- (void) setTextWithLeft:(NSString *)left Right:(NSString *)right important:(BOOL)i shareSpaceEven:(BOOL)shareEven;
- (NSString *) receiptLineLeft:(NSString *)left Right:(NSString *) right important:(BOOL)i shareSpaceEven:(BOOL)shareEven;

@end
