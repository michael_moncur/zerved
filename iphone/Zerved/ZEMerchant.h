//
//  ZEMerchant.h
//  Zerved
//
//  Created by Anders Rasmussen on 17/04/13.
//
//

#import <Foundation/Foundation.h>

@interface ZEMerchant : NSObject
@property (nonatomic, copy) NSString *key;
@property (nonatomic, copy) NSString *imageUrl;
@end
