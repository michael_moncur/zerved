//
//  ZECartItemCell.h
//  Zerved
//
//  Created by Anders Rasmussen on 04/06/13.
//
//

#import <UIKit/UIKit.h>
#import "ZECartItem.h"
#import "DTCustomColoredAccessory.h"

@interface ZECartItemCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UILabel *optionsLabel;
@property (weak, nonatomic) IBOutlet UIStepper *quantityStepper;
@property (weak, nonatomic) IBOutlet UIButton *editButton;
@property (weak, nonatomic) IBOutlet UIButton *favouriteButton;
@property (weak, nonatomic) ZECartItem *cartItem;
@property (weak, nonatomic) IBOutlet DTCustomColoredAccessory *arrowView;
@property (nonatomic, retain) UIActivityIndicatorView *spinner;
-(void)setCartItem:(ZECartItem *)cartItem;

@end
