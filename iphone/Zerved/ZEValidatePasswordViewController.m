//
//  ZEValidatePasswordViewController.m
//  Zerved
//
//  Created by Anders Rasmussen on 12/03/13.
//
//

#import "ZEValidatePasswordViewController.h"
#import "SVProgressHUD.h"

@interface ZEValidatePasswordViewController ()

@end

@implementation ZEValidatePasswordViewController

@synthesize delegate, passwordField;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [self.passwordField becomeFirstResponder];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setPasswordField:nil];
    [super viewDidUnload];
}

- (IBAction)confirm:(id)sender {
    [self.delegate validatePasswordDidEnterPassword:self.passwordField.text];
}

- (IBAction)forgotPassword:(id)sender {
    [self.delegate validatePasswordForgotPassword];
}

- (IBAction)typePassword:(id)sender {
    self.navigationItem.rightBarButtonItem.enabled = (self.passwordField.text.length > 0);
}

#pragma mark -
#pragma mark UITextFieldDelegate Methods

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self confirm:nil];
    return YES;
}

@end
