//
//  ZEEventSummaryView.m
//  Zerved
//
//  Created by Anders Rasmussen on 13/11/13.
//
//

#import "ZEEventSummaryView.h"
#import "NSString+Additions.h"

@implementation ZEEventSummaryView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        
    }
    return self;
}

- (void)setEvent:(ZEEvent *)event
{
    self.orderingLabel.text = NSLocalizedString(@"Ordering for:", @"Start label for event summary box, before event name");
    NSString *titleString = [NSString stringWithFormat:@"%@ %@", event.name, event.dateStartTimeFormatted];
    self.titleLabel.text = titleString;
    // Resize title height to fit text
    CGSize titleSize = [titleString sizeWithFont:self.titleLabel.font constrainedTo:CGSizeMake(self.titleLabel.frame.size.width, FLT_MAX)];
    self.titleLabel.frame = CGRectMake(self.titleLabel.frame.origin.x, self.titleLabel.frame.origin.y, ceil(titleSize.width), ceil(titleSize.height));
    self.subtitleLabel.text = event.dateStartDateFormatted;
    self.timeLabel.text = [NSString stringWithFormat:NSLocalizedString(@"Serving %@ (%@)", @"Label for serving time and time label, e.g. Serving 16:00 (interval)"), event.dateDeliveryTimeFormatted, event.timeLabel];
    // Resize box to fit new title height (original box height 97, original title height 29)
    float height = 97 + ceil(titleSize.height) - 29;
    self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, self.frame.size.width, height);
}

- (void)setWhiteBackground
{
    self.backgroundView.backgroundColor = [UIColor whiteColor];
    self.arrowImage.image = [UIImage imageNamed:@"summarybox_arrow_white.png"];
}

@end
