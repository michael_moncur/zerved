//
//  ZEProduct.m
//  Zerved
//
//  Created by Anders Rasmussen on 06/06/13.
//
//

#import "ZEProduct.h"
#import "ZEProductOption.h"
#import "ZEProductOptionValue.h"
#import "ZECartItemOptionValue.h"
#import "ZECartItem.h"

@implementation ZEProduct

@synthesize description;

-(id) initWithKey:(NSString*)key {
    if (self = [super init]) {
        self.key = key;
        return self;
    }
    return nil;
}

- (ZECartItem *)createCartItem
{
    NSMutableArray *cartOptions = [[NSMutableArray alloc] initWithCapacity:0];
    for (ZEProductOption *option in self.productOptions) {
        for (ZEProductOptionValue *value in option.values) {
            if (value.selected) {
                [cartOptions addObject:[[ZECartItemOptionValue alloc] initWithOptionId:option.optionId selectedIndex:[value.index intValue]]];
            }
        }
    }
    ZECartItem *item = [[ZECartItem alloc] initWithProductKey:self.key quantity:self.quantity options:cartOptions];
    return item;
}

@end
