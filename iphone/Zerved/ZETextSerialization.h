//
//  ZETextSerialization.h
//  Zerved
//
//  Created by Anders Rasmussen on 24/04/13.
//
//

#import <Foundation/Foundation.h>
#import <RestKit/Support/RKSerialization.h>

@interface ZETextSerialization : NSObject <RKSerialization>

@end
