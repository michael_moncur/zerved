//
//  ZELocationViewController.m
//  Zerved
//
//  Created by Anders Rasmussen on 19/04/13.
//
//  Main screen for a location. Shows image/map and list of menu ordering options.
//  The list depends on the location data (bars, menus, categories, events etc.)
//

#import "ZELocationViewController.h"
#import "ZECategory.h"
#import "ZEProductsViewController.h"
#import "ZEBarCategoriesViewController.h"
#import "ZEBarMenusViewController.h"
#import "ZEEmptyView.h"
#import "SVProgressHUD.h"
#import "ZEBarsViewController.h"
#import "ZEEventsViewController.h"
#import "ZEEventCell.h"
#import "NSString+Additions.h"
#import "ZEInfoPanelView.h"

const int SECTION_ENTRANCE_CATEGORIES = 1;
const int SECTION_BARS = 2;
const int SECTION_MENUS = 3;
const int SECTION_MENU_CATEGORIES = 4;
const int SECTION_EVENTS = 5;
const int SECTION_NOW_AND_EVENTS = 6;
const int EVENT_CELL_TAG = 10;

const int MAX_EVENTS = 2;

@interface ZELocationViewController ()
@property (strong) MKMapView *map;
@end

@implementation ZELocationViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Set white background view
    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.opaque = NO;
    self.tableView.backgroundView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.tableView.frame.size.width, self.tableView.frame.size.height)];
    self.tableView.backgroundView.backgroundColor = [UIColor whiteColor];
    
    // Load location
    [SVProgressHUD showWithStatus:NSLocalizedString(@"Loading", nil)];
    RKObjectManager *objectManager = [RKObjectManager sharedManager];
    NSString *path = self.showAllBars ? @"locations/withallbars/%@" : @"locations/%@";
    [objectManager getObjectsAtPath:[NSString stringWithFormat:path, self.locationKey]
                         parameters:nil
                            success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
                                // Location loaded
                                [SVProgressHUD dismiss];
                                self.location = [mappingResult firstObject];
                                self.title = self.location.name;
                                [self createHeaderView];
                                [self reloadTableView];
                                [self createInfoPanel];
                            }
                            failure:^(RKObjectRequestOperation *operation, NSError *error) {
                                [SVProgressHUD dismiss];
                                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil)
                                                                                message:[error localizedDescription]
                                                                                delegate:nil
                                                                        cancelButtonTitle:NSLocalizedString(@"OK", nil)
                                                                        otherButtonTitles:nil];
                                [alert show];
                            }];
 
}

- (void)viewDidUnload
{
    [self setLocationKey:nil];
    [self setLocation:nil];
}

- (void)dealloc
{
	self.map.delegate = nil;
}

- (void)createHeaderView
{
    float aspectRatio = 0.5625;
    self.headerView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, 320, aspectRatio*320)];
    self.headerScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, 320, aspectRatio*320)];
    self.headerScrollView.pagingEnabled = YES;
    self.headerScrollView.delegate = self;
    NSInteger page = 0;
    self.headerScrollView.contentSize = CGSizeMake(320*([self.location.coverUrls count]+1), aspectRatio*320);
    
    if ([self.location.coverUrls count] > 0) {
        // Cover image
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320, aspectRatio*320)];
        [imageView setImageWithURL:[NSURL URLWithString:self.location.coverUrls[0]]];
        imageView.contentMode = UIViewContentModeScaleAspectFill;
        imageView.clipsToBounds = YES;
        [self.headerScrollView addSubview:imageView];
        page += 1;
    }
    
    // Map
    self.map = [[MKMapView alloc] initWithFrame:CGRectMake(page*320, 0, 320, aspectRatio*320)];
    CLLocationCoordinate2D coord = CLLocationCoordinate2DMake([self.location.latitude floatValue], [self.location.longitude floatValue]);
    [self.map setRegion:MKCoordinateRegionMakeWithDistance(coord, 300*aspectRatio, 300)];
    MKPointAnnotation *annotation = [[MKPointAnnotation alloc] init];
    annotation.coordinate = coord;
    self.map.delegate = self;
    [self.map addAnnotation:annotation];
    self.map.scrollEnabled = NO;
    if (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1) {
        self.map.rotateEnabled = NO;
    }
    page += 1;
    [self.headerScrollView addSubview:self.map];
    
    // Rest of images
    if ([self.location.coverUrls count] > 1) {
        for (NSUInteger i=1; i<[self.location.coverUrls count]; i++) {
            // Cover image
            UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(page*320, 0, 320, aspectRatio*320)];
            [imageView setImageWithURL:[NSURL URLWithString:self.location.coverUrls[i]]];
            imageView.contentMode = UIViewContentModeScaleAspectFill;
            imageView.clipsToBounds = YES;
            [self.headerScrollView addSubview:imageView];
            page += 1;
        }
    }
    
    // Pager
    self.pageControl = [[UIPageControl alloc] initWithFrame:CGRectMake(0, aspectRatio*320-20, 320, 20)];
    self.pageControl.numberOfPages = page;
    self.pageControl.currentPage = 0;
    self.pageControl.hidesForSinglePage = YES;
    [self.pageControl addTarget:self action:@selector(changeHeaderPage:) forControlEvents:UIControlEventValueChanged];
    self.pageControl.pageIndicatorTintColor = [UIColor colorWithRed:0.0f green:136/255.0f blue:204/255.0f alpha:0.5f];
    self.pageControl.currentPageIndicatorTintColor = [UIColor colorWithRed:0.0f green:136/255.0f blue:204/255.0f alpha:1.0f];
    
    [self.headerView addSubview:self.headerScrollView];
    [self.headerView addSubview:self.pageControl];
}

- (void)reloadTableView
{
    if(self.isViewLoaded) {
        [self.tableView reloadData];
        if ([self numberOfSectionsInTableView:self.tableView] > 0) {
            [self.tableView setTableHeaderView:self.headerView];
        } else {
            // No location data, show empty view
            ZEEmptyView *emptyView = [[NSBundle mainBundle] loadNibNamed:@"EmptyTableView" owner:self options:nil][0];
            [emptyView setupViewForTableView:self.tableView text:NSLocalizedString(@"No products at this location", nil)];
            [self.tableView setTableHeaderView:emptyView];
        }
    }
}

- (void)createInfoPanel
{
    if ([self.location.infoPanelEnabled boolValue]) {
        ZEInfoPanelView *panel = [[NSBundle mainBundle] loadNibNamed:@"InfoPanelView" owner:self options:nil][0];
        float maxHeight = self.tableView.frame.size.height-self.tableView.contentInset.top-self.tableView.contentInset.bottom - 17;
        [panel createWithLocation:self.location maxHeight:maxHeight];
        [self.view addSubview:panel];
    }
}

- (int)sectionTypeForSection:(NSInteger)section
{
    if (section == 0 && [self.location.entranceCategories count] > 0) {
        return SECTION_ENTRANCE_CATEGORIES;
    } else {
        if ([self.location.upcomingEvents count] > 0 && !self.location.hasOpenBars) {
            // Preorder only
            return SECTION_EVENTS;
        } else if ([self.location.upcomingEvents count] == 0) {
            // Real time orders only
            if ([self.location.bars count] > 1) {
                // Bars
                return SECTION_BARS;
            } else {
                ZEBar *bar = [self.location.bars firstObject];
                if ([bar.menus count] > 1) {
                    // Menus
                    return SECTION_MENUS;
                } else if ([bar.menus count] == 1) {
                    // Categories
                    return SECTION_MENU_CATEGORIES;
                }
            }
        } else {
            // Both preorder and realtime
            return SECTION_NOW_AND_EVENTS;
        }
        
    }
    return 0;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"ViewEntranceProducts"]) {
        ZEProductsViewController *detailViewController = (ZEProductsViewController *) [segue destinationViewController];
        detailViewController.placeKey = self.location.key;
        ZECategory *category = self.location.entranceCategories[(NSUInteger) [self.tableView indexPathForSelectedRow].row];
        detailViewController.category = category.key;
        detailViewController.categoryName = category.name;
        detailViewController.group = @"entrance";
        detailViewController.location = self.location;
        detailViewController.bar = nil;
        detailViewController.event = nil;
    }
    else if ([[segue identifier] isEqualToString:@"ViewBarCategories"]) {
        ZEBar *bar = (ZEBar*) self.location.bars[(NSUInteger) [self.tableView indexPathForSelectedRow].row];
        // Directly go to the menu categories
        ZEBarCategoriesViewController *destinationController = (ZEBarCategoriesViewController *) [segue destinationViewController];
        destinationController.location = self.location;
        destinationController.title = bar.name;
        destinationController.bar = bar;
        destinationController.menu = bar.menus[0];
        destinationController.event = nil;
    }
    else if ([[segue identifier] isEqualToString:@"ViewMenuCategories"]) {
        ZEBar *bar = (ZEBar*) self.location.bars[0];
        ZEMenu *menu = bar.menus[(NSUInteger) [self.tableView indexPathForSelectedRow].row];
        ZEBarCategoriesViewController *destinationController = (ZEBarCategoriesViewController *) [segue destinationViewController];
        destinationController.location = self.location;
        destinationController.title = menu.name;
        destinationController.bar = bar;
        destinationController.menu = bar.menus[(NSUInteger) [self.tableView indexPathForSelectedRow].row];
        destinationController.event = nil;
    }
    else if ([[segue identifier] isEqualToString:@"ViewBarMenus"]) {
        // Go to the menus list
        ZEBarMenusViewController *destinationController = (ZEBarMenusViewController *) [segue destinationViewController];
        ZEBar *bar = (ZEBar*) self.location.bars[(NSUInteger) [self.tableView indexPathForSelectedRow].row];
        destinationController.title = bar.name;
        destinationController.bar = bar;
        destinationController.location = self.location;
        destinationController.event = nil;
    }
    else if ([[segue identifier] isEqualToString:@"ViewMenuProducts"]) {
        // Go to products for bar menu category
        ZEProductsViewController *detailViewController = (ZEProductsViewController *) [segue destinationViewController];
        ZEBar *bar = [self.location.bars firstObject];
        ZEMenu *menu = [bar.menus firstObject];
        ZECategory *category = menu.categories[(NSUInteger) [self.tableView indexPathForSelectedRow].row];
        detailViewController.placeKey = bar.key;
        detailViewController.category = category.key;
        detailViewController.categoryName = category.name;
        detailViewController.group = @"menu";
        detailViewController.location = self.location;
        detailViewController.bar = bar;
        detailViewController.menu = menu;
        detailViewController.event = nil;
    } else if ([[segue identifier] isEqualToString:@"ViewEvents"] || [[segue identifier] isEqualToString:@"ViewMoreEvents"]) {
        ZEEventsViewController *eventsController = (ZEEventsViewController *) [segue destinationViewController];
        eventsController.location = self.location;
    }
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    int sections = 0;
    if (self.location) {
        // Ticket section
        if ([self.location.entranceCategories count] > 0) sections++;
        // Bar/event section
        if ([self.location.bars count] > 0 || [self.location.upcomingEvents count] > 0) sections++;
    }
    return sections;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    switch ([self sectionTypeForSection:section]) {
        case SECTION_ENTRANCE_CATEGORIES:
            return [self.location.entranceCategories count];
        case SECTION_BARS:
            return [self.location.bars count];
        case SECTION_MENUS:
            return [((ZEBar *)[self.location.bars firstObject]).menus count];
        case SECTION_MENU_CATEGORIES:
            return [((ZEMenu *)[((ZEBar *)[self.location.bars firstObject]).menus firstObject]).categories count];
        case SECTION_EVENTS: {
            // Limit the number of events to a constant
            if ([self.location.upcomingEvents count] > MAX_EVENTS) {
                return MAX_EVENTS + 1; // more events button
            } else {
                return MIN([self.location.upcomingEvents count], MAX_EVENTS);
            }
        }
        case SECTION_NOW_AND_EVENTS:
            return 2; // Order now or preorder
        default:
            return 0;
    }
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    switch ([self sectionTypeForSection:section]) {
        case SECTION_ENTRANCE_CATEGORIES:
            return NSLocalizedString(@"Tickets", @"Title for list of ticket categories");
        case SECTION_BARS:
        case SECTION_MENUS:
        case SECTION_MENU_CATEGORIES:
            return NSLocalizedString(@"Menu", @"Title for bars/menus/categories");
        case SECTION_NOW_AND_EVENTS:
            return NSLocalizedString(@"What would you like to do?", @"Title for order for now or preorder for later");
        case SECTION_EVENTS:
            return NSLocalizedString(@"Preorder for event", @"Title for list of events");
        default:
            return @"";
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    // Use custom large title when we have the now vs. event question
    if ([self.location.upcomingEvents count] > 0 && self.location.hasOpenBars) {
        UILabel *headerLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
        headerLabel.backgroundColor = [UIColor colorWithRed:235/255.0f green:235/255.0f blue:236/255.0f alpha:1.0f];
        headerLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:15.0f];
        headerLabel.textAlignment = NSTextAlignmentCenter;
        headerLabel.text = [self tableView:tableView titleForHeaderInSection:section];
        CALayer *bottomBorder = [CALayer layer];
        bottomBorder.frame = CGRectMake(0.0f, 43.5f, headerLabel.frame.size.width, 0.5f);
        bottomBorder.backgroundColor = [UIColor colorWithWhite:0.8f
                                                         alpha:1.0f].CGColor;
        [headerLabel.layer addSublayer:bottomBorder];
        return headerLabel;
    }
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    // Use custom large title when we have the now vs. event question
    if ([self.location.upcomingEvents count] > 0 && self.location.hasOpenBars) {
        return 44;
    }
    return UITableViewAutomaticDimension;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
    
    switch ([self sectionTypeForSection:indexPath.section]) {
        case SECTION_ENTRANCE_CATEGORIES: {
            cell = [tableView dequeueReusableCellWithIdentifier:@"EntranceCategoryCell" forIndexPath:indexPath];
            ZECategory *category = self.location.entranceCategories[(NSUInteger) indexPath.row];
            cell.textLabel.text = category.name;
            break;
        }
        case SECTION_BARS: {
            ZEBar *bar = self.location.bars[(NSUInteger) indexPath.row];
            if([bar.menus count] == 1) {
                cell = [tableView dequeueReusableCellWithIdentifier:@"BarSkipMenuCell" forIndexPath:indexPath];
            }
            else cell = [tableView dequeueReusableCellWithIdentifier:@"BarCell" forIndexPath:indexPath];
          
            cell.textLabel.text = bar.name;
            break;
        }
        case SECTION_MENUS: {
            cell = [tableView dequeueReusableCellWithIdentifier:@"MenuCell" forIndexPath:indexPath];
            ZEMenu *menu = ((ZEBar *) [self.location.bars firstObject]).menus[(NSUInteger) indexPath.row];
            cell.textLabel.text = menu.name;
            break;
        }
        case SECTION_MENU_CATEGORIES: {
            cell = [tableView dequeueReusableCellWithIdentifier:@"MenuCategoryCell" forIndexPath:indexPath];
            ZEMenu *menu = [((ZEBar *)[self.location.bars firstObject]).menus firstObject];
            ZECategory *category = menu.categories[(NSUInteger) indexPath.row];
            cell.textLabel.text = category.name;
            break;
        }
        case SECTION_EVENTS: {
            if (indexPath.row == [tableView numberOfRowsInSection:indexPath.section] - 1 && [self.location.upcomingEvents count] > MAX_EVENTS) {
                cell = [tableView dequeueReusableCellWithIdentifier:@"MoreEventsCell" forIndexPath:indexPath];
                cell.textLabel.text = NSLocalizedString(@"More events", @"Button for event preordering");
                cell.tag = SECTION_EVENTS;
            }
            else {
                static NSString *CellIdentifier = @"EventCell";
                ZEEventCell *eventCell = (ZEEventCell *) [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
                ZEEvent *event = self.location.upcomingEvents[(NSUInteger) indexPath.row];
                NSString *titleString = [NSString stringWithFormat:@"%@ %@", event.name, event.dateStartTimeFormatted];
                eventCell.titleLabel.text = titleString;
                CGSize titleSize = [eventCell.titleLabel.text sizeWithFont:eventCell.titleLabel.font constrainedTo:CGSizeMake(EVENT_TITLE_WIDTH, FLT_MAX)];
                eventCell.titleLabel.frame = CGRectMake(eventCell.titleLabel.frame.origin.x, eventCell.titleLabel.frame.origin.y, EVENT_TITLE_WIDTH, ceilf(titleSize.height));
                eventCell.subtitleLabel.text = event.dateStartDateFormatted;
                eventCell.subSubtitleLabel.text = [NSString stringWithFormat:NSLocalizedString(@"Serving %@ (%@)", @"Label for serving time and time label, e.g. Serving 16:00 (interval)"), event.dateDeliveryTimeFormatted, event.timeLabel];
                eventCell.subtitleLabel.frame = CGRectMake(eventCell.subtitleLabel.frame.origin.x, eventCell.titleLabel.frame.origin.y+eventCell.titleLabel.frame.size.height, eventCell.subtitleLabel.frame.size.width, eventCell.subtitleLabel.frame.size.height);
                eventCell.clockIcon.frame = CGRectMake(eventCell.clockIcon.frame.origin.x, eventCell.subtitleLabel.frame.origin.y+ICON_TITLE_OFFSET_Y, eventCell.clockIcon.frame.size.width, eventCell.clockIcon.frame.size.height);
                eventCell.subSubtitleLabel.frame = CGRectMake(eventCell.subSubtitleLabel.frame.origin.x, eventCell.titleLabel.frame.origin.y+eventCell.titleLabel.frame.size.height, eventCell.subSubtitleLabel.frame.size.width, eventCell.subSubtitleLabel.frame.size.height);
                eventCell.tag = EVENT_CELL_TAG;
                cell = eventCell;
            }
            break;
        }
        case SECTION_NOW_AND_EVENTS: {
            if (indexPath.row == 0) {
                // Order for now
                cell = [tableView dequeueReusableCellWithIdentifier:@"OrderNowCell" forIndexPath:indexPath];
                cell.tag = SECTION_BARS;
            } else {
                // Preorder
                cell = [tableView dequeueReusableCellWithIdentifier:@"OrderForEventsCell" forIndexPath:indexPath];
                cell.tag = SECTION_EVENTS;
            }
        }
        default:break;
    }
    
    return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([self sectionTypeForSection:indexPath.section] == SECTION_EVENTS && [tableView cellForRowAtIndexPath:indexPath].tag == EVENT_CELL_TAG) {
        // Event selected, load all event data
        ZEEvent *selectedEvent = self.location.upcomingEvents[(NSUInteger) indexPath.row];
        [self openEventForKey:selectedEvent.key];
    } else if ([self sectionTypeForSection:indexPath.section] == SECTION_NOW_AND_EVENTS && [tableView cellForRowAtIndexPath:indexPath].tag == EVENT_CELL_TAG) {
        // Event selected
        NSUInteger eventIndex = (NSUInteger) ((self.location.hasOpenBars) ? indexPath.row-1 : indexPath.row);
        ZEEvent *selectedEvent = self.location.upcomingEvents[eventIndex];
        [self openEventForKey:selectedEvent.key];
    } else if ([self sectionTypeForSection:indexPath.section] == SECTION_NOW_AND_EVENTS && [tableView cellForRowAtIndexPath:indexPath].tag == SECTION_BARS) {
        // Order for now
        if ([self.location.bars count] == 0) {
            // No bars
            [SVProgressHUD showErrorWithStatus:NSLocalizedString(@"Location is not taking orders", @"Error msg for location with no bars/menus")];
        } else if ([self.location.bars count] > 1) {
            // List bars
            ZEBarsViewController *barsController = (ZEBarsViewController *) [self.storyboard instantiateViewControllerWithIdentifier:@"BarsViewController"];
            barsController.location = self.location;
            barsController.bars = self.location.bars;
            barsController.title = NSLocalizedString(@"Sales points", nil);
            barsController.event = nil;
            [self.navigationController pushViewController:barsController animated:YES];
        } else {
            // One bar
            ZEBar *bar = [self.location.bars firstObject];
            if ([bar.menus count] == 0) {
                // No menus
                [SVProgressHUD showErrorWithStatus:NSLocalizedString(@"Location is not taking orders", @"Error msg for location with no bars/menus")];
            } else if ([bar.menus count] > 1) {
                // List menus
                ZEBarMenusViewController *menusController = (ZEBarMenusViewController *) [self.storyboard instantiateViewControllerWithIdentifier:@"BarMenusViewController"];
                menusController.location = self.location;
                menusController.bar = bar;
                menusController.title = bar.name;
                menusController.event = nil;
                [self.navigationController pushViewController:menusController animated:YES];
            } else {
                // One menu, list categories
                ZEMenu *menu = [bar.menus firstObject];
                ZEBarCategoriesViewController *categoriesController = (ZEBarCategoriesViewController *) [self.storyboard instantiateViewControllerWithIdentifier:@"BarCategoriesViewController"];
                categoriesController.location = self.location;
                categoriesController.title = bar.name;
                categoriesController.bar = bar;
                categoriesController.menu = menu;
                categoriesController.event = nil;
                [self.navigationController pushViewController:categoriesController animated:YES];
            }
        }
    }
}

- (void)openEventForKey:(NSString *)eventKey
{
    // Load complete event data
    [SVProgressHUD showWithStatus:NSLocalizedString(@"Loading event", nil)];
    RKObjectManager *objectManager = [RKObjectManager sharedManager];
    [objectManager getObjectsAtPath:[NSString stringWithFormat:@"events/%@", eventKey]
                         parameters:nil
                            success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
                                // Event loaded
                                ZEEvent *event = [mappingResult firstObject];
                                if ([event.bars count] == 0) {
                                    // No bars
                                    [SVProgressHUD showErrorWithStatus:NSLocalizedString(@"Event is not taking orders", @"Error msg for event with no bars/menus")];
                                } else if ([event.bars count] > 1) {
                                    // List bars
                                    [SVProgressHUD dismiss];
                                    ZEBarsViewController *barsController = (ZEBarsViewController *) [self.storyboard instantiateViewControllerWithIdentifier:@"BarsViewController"];
                                    barsController.location = self.location;
                                    barsController.bars = self.location.bars;
                                    barsController.title = event.name;
                                    barsController.event = event;
                                    [self.navigationController pushViewController:barsController animated:YES];
                                } else {
                                    // One bar
                                    ZEBar *bar = [event.bars firstObject];
                                    if ([bar.menus count] == 0) {
                                        // No menus
                                        [SVProgressHUD showErrorWithStatus:NSLocalizedString(@"Event is not taking orders", @"Error msg for event with no bars/menus")];
                                    } else if ([bar.menus count] > 1) {
                                        // List menus
                                        [SVProgressHUD dismiss];
                                        ZEBarMenusViewController *menusController = (ZEBarMenusViewController *) [self.storyboard instantiateViewControllerWithIdentifier:@"BarMenusViewController"];
                                        menusController.location = self.location;
                                        menusController.bar = bar;
                                        menusController.title = event.name;
                                        menusController.event = event;
                                        [self.navigationController pushViewController:menusController animated:YES];
                                    } else {
                                        // One menu, list categories
                                        [SVProgressHUD dismiss];
                                        ZEMenu *menu = [bar.menus firstObject];
                                        ZEBarCategoriesViewController *categoriesController = (ZEBarCategoriesViewController *) [self.storyboard instantiateViewControllerWithIdentifier:@"BarCategoriesViewController"];
                                        categoriesController.location = self.location;
                                        categoriesController.title = event.name;
                                        categoriesController.bar = bar;
                                        categoriesController.menu = menu;
                                        categoriesController.event = event;
                                        [self.navigationController pushViewController:categoriesController animated:YES];
                                    }
                                }
                            }
                            failure:^(RKObjectRequestOperation *operation, NSError *error) {
                                [SVProgressHUD dismiss];
                                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil)
                                                                                message:[error localizedDescription]
                                                                               delegate:nil
                                                                      cancelButtonTitle:NSLocalizedString(@"OK", nil)
                                                                      otherButtonTitles:nil];
                                [alert show];
                            }];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    BOOL isLastCell = indexPath.row == [self tableView:tableView numberOfRowsInSection:indexPath.section] - 1;
    BOOL showMoreButton = [self.location.upcomingEvents count] > MAX_EVENTS;
    if ([self sectionTypeForSection:indexPath.section] == SECTION_EVENTS && (!isLastCell || !showMoreButton)) {
        // Event cell
        ZEEvent *event = self.location.upcomingEvents[(NSUInteger) indexPath.row];
        NSString *titleString = [NSString stringWithFormat:@"%@ %@", event.name, event.dateStartTimeFormatted];
        CGSize titleSize = [titleString sizeWithFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:18] constrainedTo:CGSizeMake(EVENT_TITLE_WIDTH, FLT_MAX)];
        // Adjust base height to new height
        return BASE_EVENT_CELL_HEIGHT + ceilf(titleSize.height) - BASE_EVENT_TITLE_HEIGHT;
    } else if ([self sectionTypeForSection:indexPath.section] == SECTION_NOW_AND_EVENTS) {
        // Order type cell
        return 70;
    }
    return UITableViewAutomaticDimension;
}

#pragma mark - Map view delegate methods

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation
{
    MKPinAnnotationView*    pinView = (MKPinAnnotationView*)[mapView
                                                             dequeueReusableAnnotationViewWithIdentifier:@"LocationPin"];
    if (!pinView) {
        pinView = [[MKPinAnnotationView alloc] initWithAnnotation:annotation
                                                   reuseIdentifier:@"LocationPin"];
    } else {
        pinView.annotation = annotation;
    }
    return pinView;
}

#pragma mark - Scroll view delegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    CGFloat pageWidth = scrollView.frame.size.width;
    int page = (int)((scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
    self.pageControl.currentPage = page;
}

- (IBAction)changeHeaderPage:(id)sender
{
    NSInteger page = self.pageControl.currentPage;
    [self.headerScrollView scrollRectToVisible:CGRectMake(page*320, 0, 320, 160) animated:YES];
}

@end
