//
//  ZECartManager.h
//  Zerved
//
//  Created by Anders Rasmussen on 28/05/13.
//
//

#import <Foundation/Foundation.h>
#import "ZECart.h"

@interface ZECartManager : NSObject

@property (nonatomic, strong) NSMutableDictionary *carts;

- (ZECart *)getCartForLocation:(ZELocation *)location bar:(ZEBar *)bar group:(NSString *)group event:(ZEEvent *)event;
- (NSString *)getCartIdForLocation:(ZELocation *)location bar:(ZEBar *)bar group:(NSString *)group event:(ZEEvent *)event;

@end
