//
//  ZEProduct.h
//  Zerved
//
//  Created by Anders Rasmussen on 06/06/13.
//
//

#import <Foundation/Foundation.h>

@class ZECartItem;

@interface ZEProduct : NSObject

@property (nonatomic, copy) NSString *key;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *finalPriceFormatted;
@property (nonatomic, copy) NSString *imageUrl;
@property (nonatomic, strong) NSArray *productOptions;
@property (nonatomic, copy) NSString *description;
@property (nonatomic, strong) NSArray *tieredPriceDescriptions;
@property (nonatomic, copy) NSString *messageWhenClosed;
@property (nonatomic, copy) NSNumber *merchantOpen;
@property (nonatomic, copy) NSNumber *menuActive;

@property (nonatomic) int quantity;



- (ZECartItem *)createCartItem;

-(id) initWithKey:(NSString*)key;

@end
