//
//  ZECart.h
//  Zerved
//
//  Created by Anders Rasmussen on 15/04/12.
//  Copyright (c) 2012 Zerved. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ZELocation.h"
#import "ZEBar.h"
#import "ZECartItem.h"
#import "ZEQuote.h"
#import "ZEEvent.h"

@interface ZECart : NSObject

@property (nonatomic, strong) ZELocation *location;
@property (nonatomic, strong) ZEBar *bar;
@property (nonatomic, strong) ZEEvent *event;
@property (nonatomic, strong) NSString *group;
@property (nonatomic, strong) NSMutableArray *items;
@property (nonatomic, strong) NSString *service;
@property (nonatomic, strong) NSString *tableNumber;
@property (nonatomic) BOOL toGo;
@property (nonatomic) BOOL termsAccepted;
@property (nonatomic, strong) ZEQuote *quote;
@property (nonatomic, strong) NSNumber *tipPercent;
@property (nonatomic, strong) NSNumber *tipAmount;
@property (nonatomic, strong) NSString *deliveryName;
@property (nonatomic, strong) NSString *deliveryAddress1;
@property (nonatomic, strong) NSString *deliveryAddress2;
@property (nonatomic, strong) NSString *deliveryPostcode;
@property (nonatomic, strong) NSString *deliveryCity;
@property (nonatomic, strong) NSString *deliveryPhone;
@property (nonatomic, strong) NSString *deliveryInstructions;
@property (nonatomic, strong) NSString *loyaltyCode;

// Additional parameters for place order
@property (nonatomic, strong) NSString *card;
@property (nonatomic, strong) NSString *pincode;
@property (nonatomic, strong) NSString *obscuredCardNumber;
@property (nonatomic, strong) NSString *nordpayRegistration;
@property (nonatomic, strong) NSString *nordpayAuthorization;
@property (nonatomic, strong) NSString *nordpayMethod;

- (id)initWithLocation:(ZELocation *)location bar:(ZEBar *)bar group:(NSString *)group event:(ZEEvent *)event;
- (int)count;
- (NSUInteger)countRows;
- (void)clear;
- (void)addItem:(ZECartItem *)item;
- (void)removeItem:(ZECartItem *)item;
- (void)updateItem:(ZECartItem *)item atIndex:(NSUInteger)index;
- (NSArray *)itemsForProduct:(NSString *)productKey;
- (id)jsonObject;
- (NSString *)locationKey;
- (NSString *)barKey;
- (NSString *)eventKey;
- (void)attachQuote:(ZEQuote *)quote;
- (NSInteger)validateQuoteItems;
- (int)estimatedWaitingTime;

@end
