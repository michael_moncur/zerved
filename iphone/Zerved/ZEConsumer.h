//
//  ZEConsumer.h
//  Zerved
//
//  Created by Anders Rasmussen on 15/05/13.
//
//

#import <Foundation/Foundation.h>
#import "ZEPaymentSubscription.h"

@interface ZEConsumer : NSObject
@property (nonatomic, copy) NSString *key;
@property (nonatomic, copy) NSString *email;
@property (nonatomic, strong) ZEPaymentSubscription *currentPaymentSubscription;
@property (nonatomic, strong) NSMutableArray *paymentSubscriptions;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *phone;
@property (nonatomic, copy) NSString *address1;
@property (nonatomic, copy) NSString *address2;
@property (nonatomic, copy) NSString *postcode;
@property (nonatomic, copy) NSString *city;

- (NSMutableArray *)arrayOfStrings;
@end
