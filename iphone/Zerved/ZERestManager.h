//
//  ZERestManager.h
//  Zerved
//
//  Created by Anders Rasmussen on 24/04/13.
//
//

#import <Foundation/Foundation.h>

@interface ZERestManager : NSObject
+ (void)initRest;
@end
