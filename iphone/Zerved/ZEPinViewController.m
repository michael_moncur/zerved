//
//  ZEPinViewController.m
//  Zerved
//
//  Created by Anders Rasmussen on 03/08/12.
//  Copyright (c) 2012 Zerved. All rights reserved.
//
//  4 digit PIN input view
//

#import "ZEPinViewController.h"
#import "SVProgressHUD.h"

@interface ZEPinViewController ()

@end

@implementation ZEPinViewController
@synthesize labelOne;
@synthesize labelTwo;
@synthesize labelThree;
@synthesize labelFour;
@synthesize pinTextField;
@synthesize subtitleLabel;
@synthesize helpLabel;
@synthesize headerLabel;
@synthesize delegate;
@synthesize dismissed;
@synthesize subtitle;
@synthesize helpText;
@synthesize cancelButton;
@synthesize header;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1) {
        self.navigationController.navigationBar.tintColor = [UIColor blackColor];
    }
    if (self.header) {
        self.headerLabel.text = self.header;
    }
    if (self.subtitle) {
        self.subtitleLabel.text = self.subtitle;
    }
    if (self.helpText) {
        self.helpLabel.text = self.helpText;
        self.helpLabel.hidden = NO;
    }
    if (self.displayForgotButton) {
        self.forgotButton.hidden = NO;
    }
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textDidChange:) name:UITextFieldTextDidChangeNotification object:nil];
    [self.pinTextField becomeFirstResponder];
    self.dismissed = NO;
}

- (void)viewDidAppear:(BOOL)animated
{
    [self.pinTextField becomeFirstResponder];
}

- (void)viewDidUnload
{
    [self setLabelOne:nil];
    [self setLabelTwo:nil];
    [self setLabelThree:nil];
    [self setLabelFour:nil];
    [self setPinTextField:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [self setSubtitleLabel:nil];
    [self setDelegate:nil];
    [self setSubtitle:nil];
    [self setHelpText:nil];
    [self setCancelButton:nil];
    [self setHelpLabel:nil];
    [self setHeaderLabel:nil];
    [self setHeader:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)textDidChange:(NSNotification *)notif
{
    if ([notif object] == self.pinTextField) {
        [self updateLabels];
        if ([self.pinTextField.text length] == 4) {
            [SVProgressHUD showWithStatus:NSLocalizedString(@"Sending code", @"PIN view progress indicator (for new pin and for pin validation)")];
            self.forgotButton.enabled = NO;
            self.cancelButton.enabled = NO;
            [self.delegate pinViewControllerDidEnterPin:self pin:pinTextField.text];
        }
    }
}

- (void)updateLabels
{
    NSUInteger length = [self.pinTextField.text length];
    self.labelOne.text = (length > 0) ? @"●" : @"";
    self.labelTwo.text = (length > 1) ? @"●" : @"";
    self.labelThree.text = (length > 2) ? @"●" : @"";
    self.labelFour.text = (length > 3) ? @"●" : @"";
}

- (void)showError:(NSString *)message
{
    [SVProgressHUD showErrorWithStatus:message];
    self.pinTextField.text = @"";
    [self updateLabels];
    self.helpLabel.hidden = NO;
    self.forgotButton.enabled = YES;
    self.cancelButton.enabled = YES;
}

- (void)dismiss
{
    self.dismissed = YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if ([textField.text length] == 4 && [string length] > 0) {
        return NO;
    }
    else {
        return YES;
    }
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    return self.dismissed;
}

- (IBAction)cancel:(id)sender
{
    [self.delegate pinViewControllerDidCancel:self];
}

- (IBAction)forgotPin:(id)sender
{
    [self.delegate pinViewControllerForgotPin:self];
}

@end
