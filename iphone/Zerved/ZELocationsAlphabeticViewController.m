//
//  ZELocationsAlphabeticViewController.m
//  Zerved
//
//  Created by Anders Rasmussen on 01/08/13.
//
//  List of all locations for a country.
//

#import "ZELocationsAlphabeticViewController.h"
#import "SVProgressHUD.h"
#import "ZELocationViewController.h"
#import "QuartzCore/CAGradientLayer.h"
#import "ZEEmptyView.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "ZELocationCell.h"

@interface ZELocationsAlphabeticViewController ()

@end

@implementation ZELocationsAlphabeticViewController


- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = self.country.name;
    
    // Set white background view
    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.opaque = NO;
    self.tableView.backgroundView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.tableView.frame.size.width, self.tableView.frame.size.height)];
    self.tableView.backgroundView.backgroundColor = [UIColor whiteColor];
    
    if (nil == self.locationManager) {
        self.locationManager = [[CLLocationManager alloc] init];
    }
    [self reload];
}

- (void)viewDidUnload {
    [self setLocationManager:nil];
    [super viewDidUnload];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)reload
{
    [SVProgressHUD showWithStatus:NSLocalizedString(@"Loading", nil)];
    RKObjectManager *objectManager = [RKObjectManager sharedManager];
    [objectManager getObjectsAtPath:[NSString stringWithFormat:@"locations/country/%@", self.country.countryCode]
                         parameters:nil
                            success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
                                // Locations loaded
                                [SVProgressHUD dismiss];
                                self.locations = [mappingResult array];
                                [self reloadTableView];
                            }
                            failure:^(RKObjectRequestOperation *operation, NSError *error) {
                                // Failed to load locations
                                [SVProgressHUD dismiss];
                                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil)
                                                                                message:[error localizedDescription]
                                                                               delegate:nil
                                                                      cancelButtonTitle:NSLocalizedString(@"OK", nil)
                                                                      otherButtonTitles:nil];
                                [alert show];
                            }];
}

- (void)reloadTableView
{
    if(self.isViewLoaded) {
        lastLocation = self.locationManager.location;
        
        [self.tableView reloadData];
        if ([self.locations count]) {
            [self.tableView setTableHeaderView:nil];
        } else {
            ZEEmptyView *emptyView = [[[NSBundle mainBundle] loadNibNamed:@"EmptyTableView" owner:self options:nil] objectAtIndex:0];
            [emptyView setupViewForTableView:self.tableView text:NSLocalizedString(@"No locations found", nil)];
            [self.tableView setTableHeaderView:emptyView];
        }
    }
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return ([self.locations count] > 0) ? 1 : 0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.locations count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"LocationCell";
    ZELocationCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    // Add location text
    ZELocation* location = [self.locations objectAtIndex:indexPath.row];
    cell.titleLabel.text = [location name];
    
     // Show distance if there's a recent last known location (5 minutes)
    if (lastLocation && -[lastLocation.timestamp timeIntervalSinceNow] < 5*60) {
        cell.subtitleLabel.text = [location describeDistanceTo:lastLocation];
    } else {
        cell.subtitleLabel.text = @"";
    }
    // Merchant logo
    // On old iPhones the images make the app crash with memory warning
    if (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1) {
        [cell.locationImage sd_setImageWithURL:[NSURL URLWithString:location.merchant.imageUrl] placeholderImage:[UIImage imageNamed:@"bar_logo_dummy.png"] options:SDWebImageRefreshCached];
    }
    
    return cell;
}

#pragma mark - Table view delegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Larger cells (default is approx. 45)
    return 70;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"ViewLocation"]) {
        // Go to categories
        ZELocationViewController *destinationController = [segue destinationViewController];
        ZELocation *location = [self.locations objectAtIndex:[self.tableView indexPathForSelectedRow].row];
        destinationController.locationKey = location.key;
        destinationController.title = location.name;
    }
}

@end
