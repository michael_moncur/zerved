//
//  ZEBar.h
//  Zerved
//
//  Created by Anders Rasmussen on 19/04/13.
//
//

#import <Foundation/Foundation.h>
#import "ZEMenu.h"

@interface ZEBar : NSObject
@property (nonatomic, copy) NSString *key;
@property (nonatomic, copy) NSString *barId;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSNumber *open;
@property (nonatomic, copy) NSNumber *tableService;
@property (nonatomic, copy) NSNumber *counterServiceToGo;
@property (nonatomic, copy) NSNumber *counterServiceToStay;
@property (nonatomic, copy) NSNumber *deliveryService;
@property (nonatomic, copy) NSNumber *enabled;
@property (nonatomic, strong) NSArray *categories;
@property (nonatomic, copy) NSNumber *estimatedWaitingTime;
@property (nonatomic, copy) NSNumber *estimatedWaitingTimeDelivery;
@property (nonatomic, strong) NSArray *menus;
@property (nonatomic, copy) NSNumber *deliveryMinOrderAmount;
@property (nonatomic, copy) NSString *deliveryMinOrderAmountFormatted;

- (BOOL)hasCounterService;
- (BOOL)hasToStayAndToGo;

@end
