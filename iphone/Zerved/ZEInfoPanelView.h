//
//  ZEInfoPanelView.h
//  Zerved
//
//  Created by Anders Rasmussen on 29/01/14.
//
//

#import <UIKit/UIKit.h>
#import "ZELocation.h"

@interface ZEInfoPanelView : UIView

@property float collapsedHeight;
@property float expandedHeight;
@property float x;
@property float y;
@property float textWidth;
@property float textHeight;
@property float textMargin;

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIButton *button;
@property (weak, nonatomic) IBOutlet UIView *gradientView;
@property (weak, nonatomic) IBOutlet UIView *topBackgroundView;
@property (strong, nonatomic) NSLayoutConstraint *scrollViewHeightConstraint;
@property (strong, nonatomic) UILabel *shortAddressLabel;
@property (strong, nonatomic) UILabel *fullAddressLabel;

- (void)createWithLocation:(ZELocation *)location maxHeight:(float)maxHeight;

@end
