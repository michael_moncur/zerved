//
//  ZEServiceViewController.h
//  Zerved
//
//  Created by Anders Rasmussen on 25/09/13.
//
//

#import <UIKit/UIKit.h>
#import "ZEBar.h"
#import "ZECart.h"

@protocol ZEServiceDelegate;

@interface ZEServiceViewController : UITableViewController <UITextFieldDelegate>

@property (weak, nonatomic) id<ZEServiceDelegate> delegate;
@property (nonatomic, strong) ZEBar *bar;
@property (nonatomic, strong) ZECart *cart;
@property (nonatomic, strong) NSMutableArray *methods;
@property (nonatomic, assign) NSInteger selectedIndex;
@property (weak, nonatomic) UITextField *tableNumberField;
- (IBAction)done:(id)sender;
- (IBAction)cancel:(id)sender;

@end

@protocol ZEServiceDelegate <NSObject>

- (void)serviceDone;
- (void)serviceCancelled;

@end