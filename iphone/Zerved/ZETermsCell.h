//
//  ZETermsCell.h
//  Zerved
//
//  Created by Anders Rasmussen on 06/06/13.
//
//

#import <UIKit/UIKit.h>

@interface ZETermsCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UISwitch *acceptTermsSwitch;
@property (weak, nonatomic) IBOutlet UIButton *termsButton;
@property (weak, nonatomic) IBOutlet UIButton *acceptTermsButton;

@end
