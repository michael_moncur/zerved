//
//  ZEProductOptionValue.h
//  Zerved
//
//  Created by Anders Rasmussen on 06/06/13.
//
//

#import <Foundation/Foundation.h>

@interface ZEProductOptionValue : NSObject

@property (nonatomic, copy) NSNumber *index;
@property (nonatomic, copy) NSString *label;
@property (nonatomic, copy) NSString *priceLabel;

@property (nonatomic, assign) BOOL selected;

- (NSString *)labelWithPrice;

@end
