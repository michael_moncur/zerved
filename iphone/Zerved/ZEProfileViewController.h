//
//  ZEProfileViewController.h
//  Zerved
//
//  Created by Anders Rasmussen on 15/05/13.
//
//

#import <UIKit/UIKit.h>
#import <RestKit/RestKit.h>
#import "ZEConsumer.h"
#import "ZEConsumerDelegate.h"
#import "ZEEpayViewController.h"

@interface ZEProfileViewController : UITableViewController <ZEConsumerDelegate, UITextFieldDelegate, UIAlertViewDelegate, ZEEpayViewControllerDelegate>

@property (strong, nonatomic) ZEConsumer *consumer;
- (IBAction)edit:(id)sender;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *editButton;
@property (weak, nonatomic) UITextField *emailField;
@property (weak, nonatomic) IBOutlet UITextField *nameField;
@property (weak, nonatomic) IBOutlet UITextField *address1Field;
@property (weak, nonatomic) IBOutlet UITextField *address2Field;
@property (weak, nonatomic) IBOutlet UITextField *cityField;
@property (weak, nonatomic) IBOutlet UITextField *postcodeField;
@property (weak, nonatomic) IBOutlet UITextField *phoneField;
@property (nonatomic) NSUInteger tabIndex;

@property (strong, nonatomic) NSMutableArray *textfieldState;

@end
