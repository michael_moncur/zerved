//
//  ZEProductViewController.h
//  Zerved
//
//  Created by Anders Rasmussen on 11/11/13.
//
//

#import <UIKit/UIKit.h>
#import <RestKit/RestKit.h>
#import "ZELocation.h"
#import "ZEBar.h"
#import "ZEMenu.h"
#import "ZEEvent.h"
#import "ZEProduct.h"
#import "ZECartButton.h"
#import "ZEOptionViewController.h"

#define IMAGE_SIZE 160
#define IMAGE_BORDER_WIDTH 1
#define RIGHT_COL_TEXT_WIDTH 130
#define PRODUCT_CONTENT_WIDTH 290
#define PRODUCT_NAME_MAX_HEIGHT 81
#define NOIMAGE_LEFT_WIDTH 160
#define NOIMAGE_RIGHT_WIDTH 130

@protocol ZEProductViewDelegate;

@interface ZEProductViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, ZESelectOptionDelegate>

@property (strong, nonatomic) NSString *group;
@property (nonatomic, strong) ZELocation *location;
@property (nonatomic, strong) ZEBar *bar;
@property (nonatomic, strong) ZEMenu *menu;
@property (nonatomic, strong) ZEEvent *event;
@property (nonatomic, strong) ZEProduct *product;
@property (strong, nonatomic) NSArray *items;
@property (strong, nonatomic) ZECartButton *cartButton;
@property (weak, nonatomic) id <ZEProductViewDelegate> delegate;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;
@property (weak, nonatomic) IBOutlet UIView *tieredPrices;
@property (weak, nonatomic) IBOutlet UIView *itemsView;
@property (weak, nonatomic) IBOutlet UIButton *addToCartButton;
@property (weak, nonatomic) IBOutlet UIStepper *quantityStepper;
@property (weak, nonatomic) IBOutlet UILabel *quantityLabel;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UILabel *statusLabel;
@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UITableView *optionsTable;

@end

@protocol ZEProductViewDelegate <NSObject>

- (void)productViewDidAddToCart;

@end
