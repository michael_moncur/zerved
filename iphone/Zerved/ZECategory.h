//
//  ZECategory.h
//  Zerved
//
//  Created by Anders Rasmussen on 19/04/13.
//
//

#import <Foundation/Foundation.h>

@interface ZECategory : NSObject
@property (nonatomic, copy) NSString *key;
@property (nonatomic, copy) NSString *categoryId;
@property (nonatomic, copy) NSString *name;
@end
