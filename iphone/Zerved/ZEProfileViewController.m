//
//  ZEProfileViewController.m
//  Zerved
//
//  Created by Anders Rasmussen on 15/05/13.
//
//  Consumer account editor for email, address and credit cards.
//

#import "ZEProfileViewController.h"
#import "ZEConsumer.h"
#import "ZEPaymentSubscription.h"
#import "ZEAppDelegate.h"
#import "ZEConsumerManager.h"
#import "SVProgressHUD.h"
#import "NSData+Additions.h"
#import "ZETextFieldLabelCell.h"
#import "ZEEpayViewController.h"

@interface ZEProfileViewController ()
@property (strong) UIAlertView *sessionExpiredAlertView;
@end

@implementation ZEProfileViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1) {
        self.navigationController.navigationBar.tintColor = [UIColor blackColor];
    }
    
    self.tabIndex = self.tabBarController.selectedIndex;
}

- (void)viewDidUnload {
    [self setEditButton:nil];
    [self setEmailField:nil];
    [super viewDidUnload];
}

- (void)viewDidAppear:(BOOL)animated
{
    [self reload];
}

- (void)reload
{
    [SVProgressHUD showWithStatus:NSLocalizedString(@"Loading", nil)];
    ZEConsumerManager *consumerManager = ((ZEAppDelegate *)[[UIApplication sharedApplication] delegate]).consumer;
    if ([consumerManager.accessToken isEqualToString:@""]) {
        consumerManager.delegate = self;
        [consumerManager authenticateFromView:self];
        return;
    }
    RKObjectManager *objectManager = [RKObjectManager sharedManager];
    [objectManager getObjectsAtPath:[NSString stringWithFormat:@"consumers/%@", [consumerManager consumerKey]]
                         parameters:nil
                            success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
                                // Consumer data received, update view
                                [SVProgressHUD dismiss];
                                self.consumer = [mappingResult firstObject];
                                self.textfieldState = [self.consumer arrayOfStrings];
                                [self.tableView reloadData];
                            }
                            failure:^(RKObjectRequestOperation *operation, NSError *error) {
                                [self handleRestFailure:operation error:error];
                            }];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)edit:(id)sender {
    if ([self.tableView isEditing]) {
        // Done editing
        [self endEditing];
    } else {
        // Start editing
        [self beginEditing];
    }
}

- (void)beginEditing
{
    [self.tableView setEditing:YES animated:YES];
    [self.editButton setTitle:NSLocalizedString(@"Done", nil)];
}

- (void)endEditing
{
    if (![self.consumer.email isEqualToString:self.textfieldState[0]]
        || ![self.consumer.name isEqualToString:self.textfieldState[1]]
        || ![self.consumer.address1 isEqualToString:self.textfieldState[2]]
        || ![self.consumer.address2 isEqualToString:self.textfieldState[3]]
        || ![self.consumer.city isEqualToString:self.textfieldState[4]]
        || ![self.consumer.postcode isEqualToString:self.textfieldState[5]]
        || ![self.consumer.phone isEqualToString:self.textfieldState[6]]) {
        // Email or address changed, save consumer
        [SVProgressHUD showWithStatus:NSLocalizedString(@"Saving", nil)];
        NSString *oldEmail = [self.consumer.email copy];
        NSString *oldName = [self.consumer.name copy];
        NSString *oldAddress1 = [self.consumer.address1 copy];
        NSString *oldAddress2 = [self.consumer.address2 copy];
        NSString *oldCity = [self.consumer.city copy];
        NSString *oldPostcode = [self.consumer.postcode copy];
        NSString *oldPhone = [self.consumer.phone copy];
        self.consumer.email = self.textfieldState[0];
        self.consumer.name = self.textfieldState[1];
        self.consumer.address1 = self.textfieldState[2];
        self.consumer.address2 = self.textfieldState[3];
        self.consumer.city = self.textfieldState[4];
        self.consumer.postcode = self.textfieldState[5];
        self.consumer.phone = self.textfieldState[6];
        RKObjectManager *objectManager = [RKObjectManager sharedManager];
        [objectManager postObject:self.consumer path:[NSString stringWithFormat:@"consumers/%@", self.consumer.key] parameters:nil success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
            [self.tableView setEditing:NO animated:YES];
            [self.editButton setTitle:NSLocalizedString(@"Edit", nil)];
            [SVProgressHUD showSuccessWithStatus:NSLocalizedString(@"Saved", nil)];
            [self.view endEditing:YES];
        } failure:^(RKObjectRequestOperation *operation, NSError *error) {
            // Revert address
            self.consumer.email = oldEmail;
            self.consumer.name = oldName;
            self.consumer.address1 = oldAddress1;
            self.consumer.address2 = oldAddress2;
            self.consumer.city = oldCity;
            self.consumer.postcode = oldPostcode;
            self.consumer.phone = oldPhone;
            [self handleRestFailure:operation error:error];
        }];
    } else {
        [self.tableView setEditing:NO animated:YES];
        [self.editButton setTitle:NSLocalizedString(@"Edit", nil)];
        [self.view endEditing:YES];
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"AddCard"]) {
        ZEEpayViewController *paymentViewController = [segue destinationViewController];
        paymentViewController.delegate = self;
        paymentViewController.shouldPlaceOrder = NO;
        paymentViewController.title = NSLocalizedString(@"Add Card", @"Add card window title");
        
    }
}

- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender {
    if ([identifier isEqualToString:@"AddCard"]) {
        if ([self.tableView isEditing]) {
            [SVProgressHUD showErrorWithStatus:NSLocalizedString(@"Please save profile fields first", @"Error msg when trying to switch to add card screen when currently editing profile fields")];
            return NO;
        }
        return YES;
    }
    
    return YES;
}

- (void)deleteAccount
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Warning", nil) message:NSLocalizedString(@"Your saved cards will be deleted and you will lose access to your receipts. The app will be reset with an empty Zerved account. Are you sure you want to delete your account?", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"Keep", nil) otherButtonTitles:NSLocalizedString(@"Delete", nil), nil];
    [alert setTag:1];
    [alert show];
}

- (void)handleRestFailure:(RKObjectRequestOperation *)operation error:(NSError *)error
{
    [SVProgressHUD dismiss];
    if (operation.HTTPRequestOperation.response.statusCode == 401) {
        // Need to login
		if (!self.sessionExpiredAlertView.visible)
		{
			self.sessionExpiredAlertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Session expired", nil)
															message:NSLocalizedString(@"Zerved could not access your account", nil)
														   delegate:self
												  cancelButtonTitle:NSLocalizedString(@"Cancel", nil)
												  otherButtonTitles:NSLocalizedString(@"Reconnect", nil), nil];
			[self.sessionExpiredAlertView setTag:2];
			[self.sessionExpiredAlertView show];
		}
    } else {
        // Other error
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil)
                                                        message:[error localizedDescription]
                                                       delegate:nil
                                              cancelButtonTitle:NSLocalizedString(@"OK", nil)
                                              otherButtonTitles:nil];
        [alert show];
    }
}

#pragma mark - Consumer delegate

- (void)didAuthenticate
{
    [self reload];
}

- (void)didFailToAuthenticate
{
    
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.consumer ? 2 : 0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) {
        return [self.consumer.paymentSubscriptions count] + 1;
    } else {
        return 7;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
    if (indexPath.section == 0 && indexPath.row < [self.consumer.paymentSubscriptions count]) {
        cell = [tableView dequeueReusableCellWithIdentifier:@"CreditCardCell" forIndexPath:indexPath];
        ZEPaymentSubscription *sub = [self.consumer.paymentSubscriptions objectAtIndex:indexPath.row];
        cell.textLabel.text = sub.description;
        if (self.consumer.currentPaymentSubscription && [self.consumer.currentPaymentSubscription.key isEqualToString:sub.key]) {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
            cell.editingAccessoryType = UITableViewCellAccessoryCheckmark;
        } else {
            cell.accessoryType = UITableViewCellAccessoryNone;
            cell.editingAccessoryType = UITableViewCellAccessoryNone;
        }
    }
    else if (indexPath.section == 0 && indexPath.row == [self.consumer.paymentSubscriptions count]) {
        cell = [tableView dequeueReusableCellWithIdentifier:@"AddCardCell" forIndexPath:indexPath];
    }
    else if (indexPath.section == 1) {
        ZETextFieldLabelCell *textfieldCell = (ZETextFieldLabelCell *)[tableView dequeueReusableCellWithIdentifier:@"TextFieldCell" forIndexPath:indexPath];
        textfieldCell.inputField.delegate = self;
        textfieldCell.inputField.keyboardType = UIKeyboardTypeDefault;
        textfieldCell.inputField.tag = indexPath.row;
        textfieldCell.inputField.text = self.textfieldState[indexPath.row];
        switch (indexPath.row) {
            case 0:
                textfieldCell.titleLabel.text = NSLocalizedString(@"Email", @"Customer email");
                self.emailField = textfieldCell.inputField;
                break;
            case 1:
                textfieldCell.titleLabel.text = NSLocalizedString(@"Name", @"Customer name");
                self.nameField = textfieldCell.inputField;
                break;
            case 2:
                textfieldCell.titleLabel.text = NSLocalizedString(@"Address line 1", @"Customer address line 1");
                self.address1Field = textfieldCell.inputField;
                break;
            case 3:
                textfieldCell.titleLabel.text = NSLocalizedString(@"Address line 2", @"Customer address line 2");
                self.address2Field = textfieldCell.inputField;
                break;
            case 4:
                textfieldCell.titleLabel.text = NSLocalizedString(@"City/town", @"Customer city");
                self.cityField = textfieldCell.inputField;
                break;
            case 5:
                textfieldCell.titleLabel.text = NSLocalizedString(@"Postcode/zip", @"Customer postcode/zip");
                self.postcodeField = textfieldCell.inputField;
                break;
            case 6:
                textfieldCell.titleLabel.text = NSLocalizedString(@"Phone number", @"Customer phone no.");
                textfieldCell.inputField.keyboardType = UIKeyboardTypePhonePad;
                self.phoneField = textfieldCell.inputField;
                break;
        }
        cell = textfieldCell;
    }
    
    return cell;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return (section == 0) ? NSLocalizedString(@"Credit Cards", nil) : NSLocalizedString(@"Profile", nil);
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        ZEPaymentSubscription *card = [self.consumer.paymentSubscriptions objectAtIndex:indexPath.row];

        // Update data model and table view
        if (self.consumer.currentPaymentSubscription != nil && [self.consumer.currentPaymentSubscription isEqual:[self.consumer.paymentSubscriptions objectAtIndex:indexPath.row]]) {
            // Unset current card
            self.consumer.currentPaymentSubscription = nil;
        }
        // Remove from card list
        [self.consumer.paymentSubscriptions removeObjectAtIndex:indexPath.row];
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
        
        // Call server to delete card
        [SVProgressHUD showWithStatus:NSLocalizedString(@"Deleting", nil)];
        RKObjectManager *objectManager = [RKObjectManager sharedManager];
        [objectManager deleteObject:card path:[NSString stringWithFormat:@"paymentsubscriptions/%@", card.key] parameters:nil success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
            [SVProgressHUD dismiss];
        } failure:^(RKObjectRequestOperation *operation, NSError *error) {
            [self reload];
            [self handleRestFailure:operation error:error];
        }];
    }
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    if (indexPath.section == 1) {
        return;
    }
    // Add card row
    if (indexPath.section == 0 && indexPath.row == [self.consumer.paymentSubscriptions count]) {
        if ([self.tableView isEditing]) {
            [SVProgressHUD showErrorWithStatus:NSLocalizedString(@"Please save profile fields first", @"Error msg when trying to switch to add card screen when currently editing profile fields")];
        } else {
            //[self performSegueWithIdentifier:@"AddCard" sender:self];
        }
        return;
    }
    // If row already selected, don't do anything
    NSUInteger selectedIndex = [self.consumer.paymentSubscriptions indexOfObject:self.consumer.currentPaymentSubscription];
    if (selectedIndex == indexPath.row) {
        return;
    }
    
    // Deselect old row
    if (self.consumer.currentPaymentSubscription && selectedIndex != NSNotFound) {
        NSIndexPath *oldIndexPath = [NSIndexPath indexPathForRow:selectedIndex inSection:0];
        UITableViewCell *oldCell = [tableView cellForRowAtIndexPath:oldIndexPath];
        if (oldCell.accessoryType == UITableViewCellAccessoryCheckmark) {
            oldCell.accessoryType = UITableViewCellAccessoryNone;
            oldCell.editingAccessoryType = UITableViewCellAccessoryNone;
        }
    }
    
    // Select new row
    UITableViewCell *newCell = [tableView cellForRowAtIndexPath:indexPath];
    if (newCell.accessoryType == UITableViewCellAccessoryNone) {
        newCell.accessoryType = UITableViewCellAccessoryCheckmark;
        newCell.editingAccessoryType = UITableViewCellAccessoryCheckmark;
        // Update current card on server
        self.consumer.currentPaymentSubscription = [self.consumer.paymentSubscriptions objectAtIndex:indexPath.row];
        RKObjectManager *objectManager = [RKObjectManager sharedManager];
        [objectManager postObject:self.consumer path:[NSString stringWithFormat:@"consumers/%@", self.consumer.key] parameters:nil success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
            
        } failure:^(RKObjectRequestOperation *operation, NSError *error) {
            [self handleRestFailure:operation error:error];
        }];
        
    }
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0 && indexPath.row == [self.consumer.paymentSubscriptions count]) {
        return UITableViewCellEditingStyleNone;
    } else if (indexPath.section == 0) {
        return UITableViewCellEditingStyleDelete;
    } else {
        return UITableViewCellEditingStyleNone;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return (section == 0) ? 0 : 25;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    if (section == 1 ) {
        // Delete account button
        UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 25)];
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.frame = CGRectMake(10, 5, 300, 20);
        [button setTitle:NSLocalizedString(@"Delete Account", nil) forState:UIControlStateNormal];
        [button setTitleColor:[UIColor colorWithRed:50/255.0f green:79/255.0f blue:133/255.0f alpha:1.0f] forState:UIControlStateNormal];
        [button.titleLabel setFont:[UIFont boldSystemFontOfSize:12.0]];
        [button setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
        button.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
        [button addTarget:self action:@selector(deleteAccount) forControlEvents:UIControlEventTouchUpInside];
        [footerView addSubview:button];
        return footerView;
    }
    return nil;
}

#pragma mark - Text view delegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    [self endEditing];
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [self beginEditing];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    // Update local array of textfield state when text is changed
    NSString *finalString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    self.textfieldState[textField.tag] = finalString;
    return YES;
}

#pragma mark - UIAlertViewDelegate Methods

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 1 && buttonIndex == 1) {
        // Delete account
        ZEAppDelegate *delegate = [[UIApplication sharedApplication] delegate];
        ZEConsumerManager *consumerManager = delegate.consumer;
        consumerManager.delegate = self;
        [consumerManager deleteAccount];
        //self.loginCancelled = YES;
        [self reload];
    } else if (alertView.tag == 2 && buttonIndex == 1) {
        // Log in
		self.sessionExpiredAlertView.delegate = nil;
		self.sessionExpiredAlertView = nil;
        ZEConsumerManager *consumerManager = ((ZEAppDelegate *)[[UIApplication sharedApplication] delegate]).consumer;
        consumerManager.delegate = self;
        [consumerManager authenticateFromView:self];
    }
}

- (void)dealloc
{
	self.sessionExpiredAlertView.delegate = nil;
	self.sessionExpiredAlertView = nil;
}

- (void)epayViewControllerDidPlaceOrder:(ZEEpayViewController *)controller id:(NSString *)orderId key:(NSString *)orderKey fromTabIndex:(NSUInteger)tabIndex
{
    NSLog(@"save card payment placed ok");
    [self.navigationController popViewControllerAnimated:YES];
}

@end
