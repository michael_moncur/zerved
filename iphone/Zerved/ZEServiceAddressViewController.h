//
//  ZEServiceAddressViewController.h
//  Zerved
//
//  Created by Anders Rasmussen on 25/11/13.
//
//

#import <UIKit/UIKit.h>
#import <RestKit/RestKit.h>
#import "ZEServiceViewController.h"
#import "ZECart.h"
#import "ZEBar.h"
#import "ZEConsumerDelegate.h"
#import "ZEConsumer.h"

@interface ZEServiceAddressViewController : UITableViewController <ZEConsumerDelegate>

@property (weak, nonatomic) id<ZEServiceDelegate> delegate;
@property (nonatomic, strong) ZECart *cart;
@property (nonatomic, strong) ZEBar *bar;
@property (strong, nonatomic) ZEConsumer *consumer;
@property (weak, nonatomic) IBOutlet UITextField *nameField;
@property (weak, nonatomic) IBOutlet UITextField *address1Field;
@property (weak, nonatomic) IBOutlet UITextField *address2Field;
@property (weak, nonatomic) IBOutlet UITextField *cityField;
@property (weak, nonatomic) IBOutlet UITextField *postcodeField;
@property (weak, nonatomic) IBOutlet UITextField *phoneField;
@property (weak, nonatomic) IBOutlet UITextField *instructionsField;

- (IBAction)done:(id)sender;

@end
