//
//  ZEPlaceOrderResult.h
//  Zerved
//
//  Created by Anders Rasmussen on 11/06/13.
//
//

#import <Foundation/Foundation.h>

@interface ZEPlaceOrderResult : NSObject

@property (nonatomic, strong) NSString *orderKey;
@property (nonatomic, strong) NSString *orderId;
@property (nonatomic, strong) NSNumber *paymentNeeded;

@end
