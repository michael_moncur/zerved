//
//  ZELocationsAlphabeticViewController.h
//  Zerved
//
//  Created by Anders Rasmussen on 01/08/13.
//
//

#import <UIKit/UIKit.h>
#import "ZECountry.h"
#import "ZELocation.h"
#import <RestKit/RestKit.h>

@interface ZELocationsAlphabeticViewController : UITableViewController
{
    CLLocation *lastLocation;
}

@property (nonatomic, strong) ZECountry *country;
@property (strong, nonatomic) NSArray *locations;
@property (strong, nonatomic) CLLocationManager *locationManager;

@end
