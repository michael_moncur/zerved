//
//  ZEProductOptionsViewController.m
//  Zerved
//
//  Created by Anders Rasmussen on 06/06/13.
//
//  List all options and option values for a cart item.
//

#import "ZEProductOptionsViewController.h"
#import "ZEProductOption.h"
#import "ZEProductOptionValue.h"
#import "ZECartItemOptionValue.h"
#import "SVProgressHUD.h"

@interface ZEProductOptionsViewController ()

@end

@implementation ZEProductOptionsViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1) {
        self.navigationController.navigationBar.tintColor = [UIColor blackColor];
    }
    
    self.title = self.cartItem.productName;
    
    RKObjectManager *objectManager = [RKObjectManager sharedManager];
    [objectManager getObjectsAtPath:[NSString stringWithFormat:@"products/%@", self.productKey]
                         parameters:nil
                            success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
                                self.product = [mappingResult firstObject];
                                self.title = self.product.name;
                                [self.tableView reloadData];
                            }
                            failure:^(RKObjectRequestOperation *operation, NSError *error) {
                                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil)
                                                                                message:[error localizedDescription]
                                                                               delegate:nil
                                                                      cancelButtonTitle:NSLocalizedString(@"OK", nil)
                                                                      otherButtonTitles:nil];
                                [alert show];
                            }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)done:(id)sender {
    // Validate
    for (ZEProductOption *option in self.product.productOptions) {
        if ([option.required boolValue]) {
            if (![self.cartItem hasValueForOptionId:option.optionId]) {
                [SVProgressHUD showErrorWithStatus:[NSString stringWithFormat:NSLocalizedString(@"Please select %@", @"Error msg for required option selection"), option.name]];
                [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:[self.product.productOptions indexOfObject:option]] atScrollPosition:UITableViewScrollPositionTop animated:YES];
                return;
            }
        }
    }
    
    // Change of options may cause duplicate items in cart: Make sure they are merged
    [self.cart updateItem:self.cartItem atIndex:self.itemIndex];
    
    [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (self.product) {
        return [self.product.productOptions count];
    }
    return 0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    ZEProductOption *option = [self.product.productOptions objectAtIndex:section];
    return [option.values count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"OptionValueCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    // Configure the cell...
    ZEProductOption *option = [self.product.productOptions objectAtIndex:indexPath.section];
    ZEProductOptionValue *optionValue = [option.values objectAtIndex:indexPath.row];
    cell.textLabel.text = optionValue.label;
    cell.detailTextLabel.text = optionValue.priceLabel;
    if (self.cartItem && [self.cartItem hasValue:indexPath.row forOptionId:option.optionId]) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    } else {
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    ZEProductOption *productOption = [self.product.productOptions objectAtIndex:indexPath.section];
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    if ([productOption.selectionType isEqualToString:@"single"]) {
        // Single selection
        if (cell.accessoryType == UITableViewCellAccessoryNone) {
            // Update cart item
            [self.cartItem clearOptionId:productOption.optionId];
            [self.cartItem.options addObject:[[ZECartItemOptionValue alloc] initWithOptionId:productOption.optionId selectedIndex:indexPath.row]];
            // Remove old checkmarks
            for (int i=0; i<[self tableView:tableView numberOfRowsInSection:indexPath.section]; i++) {
                UITableViewCell *oldCell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:i inSection:indexPath.section]];
                oldCell.accessoryType = UITableViewCellAccessoryNone;
            }
            // Add new checkmarks
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
        } else if (cell.accessoryType == UITableViewCellAccessoryCheckmark && ![productOption.required boolValue]) {
            // Deselecting (only allowed if not required)
            // Remove option value
            cell.accessoryType = UITableViewCellAccessoryNone;
            [self.cartItem removeOptionWithOptionId:productOption.optionId value:indexPath.row];
        }
    } else if ([productOption.selectionType isEqualToString:@"multiple"]) {
        // Multiple selection
        if (cell.accessoryType == UITableViewCellAccessoryNone) {
            // Add option value
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
            [self.cartItem.options addObject:[[ZECartItemOptionValue alloc] initWithOptionId:productOption.optionId selectedIndex:indexPath.row]];
        } else if (cell.accessoryType == UITableViewCellAccessoryCheckmark) {
            // Remove option value
            cell.accessoryType = UITableViewCellAccessoryNone;
            [self.cartItem removeOptionWithOptionId:productOption.optionId value:indexPath.row];
        }
    }
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    ZEProductOption *option = (ZEProductOption *)[self.product.productOptions objectAtIndex:section];
    
    UILabel *label = [[UILabel alloc] init];
    label.frame = CGRectMake(20, 8, 300, 20);
    label.backgroundColor = [UIColor clearColor];
    label.textColor = [UIColor colorWithRed:0.298039 green:0.337255 blue:0.423529 alpha:1.0];
    label.shadowColor = [UIColor colorWithWhite:1.0 alpha:1.0];
    label.shadowOffset = CGSizeMake(0.0, 1.0);
    label.font = [UIFont boldSystemFontOfSize:17];
    label.text = option.name;
    
    UILabel *subLabel = [[UILabel alloc] init];
    subLabel.frame = CGRectMake(20, 30, 300, 16);
    subLabel.backgroundColor = [UIColor clearColor];
    subLabel.textColor = [UIColor colorWithRed:0.298039 green:0.337255 blue:0.423529 alpha:1.0];
    subLabel.shadowColor = [UIColor colorWithWhite:1.0 alpha:1.0];
    subLabel.shadowOffset = CGSizeMake(0.0, 1.0);
    subLabel.font = [UIFont boldSystemFontOfSize:13];
    if ([option.selectionType isEqualToString:@"single"]) {
        if ([option.required boolValue]) {
            subLabel.text = NSLocalizedString(@"Select one", @"Instructions for mandatory single-select option");
        } else {
            subLabel.text = NSLocalizedString(@"Select one (optional)", @"Instructions for optional single-select option");
        }
    } else {
        if ([option.required boolValue]) {
            subLabel.text = NSLocalizedString(@"Select at least one", @"Instructions for mandatory multi-select option");
        } else {
            subLabel.text = NSLocalizedString(@"Select one or more (optional)", @"Instructions for optinal multi-select option");
        }
    }
    
    UIView *view = [[UIView alloc] init];
    [view addSubview:label];
    [view addSubview:subLabel];
    
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 60.0;
}

@end
