//
//  ZEProductOptionToggleButton.m
//  Zerved
//
//  Created by Anders Rasmussen on 13/08/13.
//
//

#import "ZEProductOptionToggleButton.h"

@implementation ZEProductOptionToggleButton

@synthesize productOptionValue = _productOptionValue;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setProductOptionValue:(ZEProductOptionValue *)productOptionValue
{
    _productOptionValue = productOptionValue;
    
    [self setBackgroundImage:[UIImage imageNamed:@"chkbox_unchk.png"] forState:UIControlStateNormal];
    [self setBackgroundImage:[UIImage imageNamed:@"chkbox_chk.png"] forState:UIControlStateSelected];
    [self setTitle:[NSString stringWithFormat:@"%@ %@", productOptionValue.label, productOptionValue.priceLabel] forState:UIControlStateNormal];
    [self setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
    [self setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    [self setContentEdgeInsets:UIEdgeInsetsMake(0, 25, 0, 0)];
    [self.titleLabel setFont:[UIFont systemFontOfSize:12]];
    [self.titleLabel setLineBreakMode:NSLineBreakByWordWrapping];
    [self addTarget:self action:@selector(toggleOption:) forControlEvents:UIControlEventTouchUpInside];
    self.selected = productOptionValue.selected;
}

- (void)toggleOption:(UIButton *)sender
{
    self.selected = !self.selected;
    self.productOptionValue.selected = self.selected;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
