//
//  ZEEmptyView.h
//  Zerved
//
//  Created by Anders Rasmussen on 22/04/13.
//
//

#import <UIKit/UIKit.h>

@interface ZEEmptyView : UIView
@property (weak, nonatomic) IBOutlet UILabel *emptyLabel;
- (void)setupViewWithFrame:(CGRect)frame text:(NSString *)text;
- (void)setupViewForTableView:(UITableView *)parentView text:(NSString *)text;
@end
