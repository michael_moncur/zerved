//
//  ZEProductOptionDropdownButton.h
//  Zerved
//
//  Created by Anders Rasmussen on 13/08/13.
//
//

#import <UIKit/UIKit.h>
#import "ZEProductOption.h"

@interface ZEProductOptionDropdownButton : UIButton

@property (nonatomic, weak) ZEProductOption *productOption;

@end
