//
//  ZEFavouriteCell.m
//  Zerved
//
//  Created by Zerved Development Mac on 29/07/13.
//
//

#import "ZEFavouriteCell.h"

@implementation ZEFavouriteCell

@synthesize favourite = _favourite;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
