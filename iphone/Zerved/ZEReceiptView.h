//
//  ZEReceiptView.h
//  Zerved
//
//  Created by Zerved Development Mac on 19/08/13.
//
//

#import <UIKit/UIKit.h>

@interface ZEReceiptView : UIView

@property (nonatomic, retain) IBOutlet UIImageView *top;
@property (nonatomic, retain) IBOutlet UIImageView *bottom;
@property (nonatomic, retain) IBOutlet UIView *content;
@property (nonatomic, retain) NSArray * receiptLines; // ZEReceiptLineView

@end
