//
//  OrderItemOptionValue.h
//  Zerved
//
//  Created by Zerved Development Mac on 16/08/13.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@interface OrderItemOptionValue : NSObject

@property (nonatomic, strong) NSString * valueLabel;
@property (nonatomic, strong) NSObject * orderItemOption;
@end
