//
//  ZELocation.h
//  Zerved
//
//  Created by Anders Rasmussen on 15/04/13.
//
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import "ZEMerchant.h"
#import "ZEBar.h"

@interface ZELocation : NSObject
@property (nonatomic, copy) NSString *key;
@property (nonatomic, copy) NSString *locationId;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSNumber *latitude;
@property (nonatomic, copy) NSNumber *longitude;
@property (nonatomic, copy) NSString *country;
@property (nonatomic, strong) ZEMerchant *merchant;
@property (nonatomic, strong) NSArray *bars;
@property (nonatomic, strong) NSArray *entranceCategories;
@property (nonatomic, copy) NSArray *coverUrls;
@property (nonatomic, strong) NSArray *upcomingEvents;
@property (nonatomic, copy) NSNumber *infoPanelEnabled;
@property (nonatomic, copy) NSString *address;
@property (nonatomic, copy) NSString *websiteUrl;
@property (nonatomic, copy) NSString *phone;
@property (nonatomic, copy) NSString *phone2;
@property (nonatomic, copy) NSString *facebook;
@property (nonatomic, copy) NSString *twitter;
@property (nonatomic, copy) NSString *googlePlus;
@property (nonatomic, copy) NSString *linkedIn;
@property (nonatomic, copy) NSString *instagram;
@property (nonatomic, copy) NSString *pinterest;
@property (nonatomic, copy) NSString *flickr;
@property (nonatomic, copy) NSString *openingHoursMonday;
@property (nonatomic, copy) NSString *openingHoursTuesday;
@property (nonatomic, copy) NSString *openingHoursWednesday;
@property (nonatomic, copy) NSString *openingHoursThursday;
@property (nonatomic, copy) NSString *openingHoursFriday;
@property (nonatomic, copy) NSString *openingHoursSaturday;
@property (nonatomic, copy) NSString *openingHoursSunday;
- (NSString *)describeDistanceTo:(CLLocation *)location;
- (ZEBar *)getBarWithId:(NSString *)barId;
- (BOOL)hasOpenBars;
- (NSString *)onelineAddress;
@end
