//
//  ZEProductsViewController.h
//  Zerved
//
//  Created by Anders Rasmussen on 07/08/13.
//
//

#import <UIKit/UIKit.h>
#import <RestKit/RestKit.h>
#import "ZELocation.h"
#import "ZEBar.h"
#import "ZEMenu.h"
#import "ZEOptionViewController.h"
#import "ZEEvent.h"
#import "ZECartButton.h"
#import "ZEProductViewController.h"

@interface ZEProductsViewController : UITableViewController <UIActionSheetDelegate, ZESelectOptionDelegate, ZEProductViewDelegate>

@property (strong, nonatomic) NSString *group;
@property (strong, nonatomic) NSString *placeKey;
@property (nonatomic, strong) ZELocation *location;
@property (nonatomic, strong) ZEBar *bar;
@property (nonatomic, strong) ZEMenu *menu;
@property (nonatomic, strong) ZEEvent *event;
@property (strong, nonatomic) NSString *category;
@property (strong, nonatomic) NSString *categoryName;
@property (strong, nonatomic) NSArray *products;
@property BOOL openForOrders;

@property (strong, nonatomic) ZECartButton *cartButton;

- (void)attachQuantityStepper:(UIStepper *)stepper;

@end
