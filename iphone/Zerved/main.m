//
//  main.m
//  Zerved
//
//  Created by Anders Rasmussen on 11/04/12.
//  Copyright (c) 2012 Zerved. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ZEAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([ZEAppDelegate class]));
    }
}
