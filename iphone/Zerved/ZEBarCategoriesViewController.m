//
//  ZEBarCategoriesViewController.m
//  Zerved
//
//  Created by Anders Rasmussen on 07/08/13.
//
//  List categories for bar. The categories link to ZEProductsViewController.
//

#import "ZEBarCategoriesViewController.h"
#import "ZECategory.h"
#import "SVProgressHUD.h"
#import "ZEEmptyView.h"
#import "ZEProductsViewController.h"
#import "QuartzCore/CAGradientLayer.h"
#import "ZEAppDelegate.h"
#import "ZECartViewController.h"
#import "ZETabBarController.h"
#import "ZEEventSummaryView.h"

@interface ZEBarCategoriesViewController ()

@end

@implementation ZEBarCategoriesViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Set white background view
    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.opaque = NO;
    self.tableView.backgroundView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.tableView.frame.size.width, self.tableView.frame.size.height)];
    self.tableView.backgroundView.backgroundColor = [UIColor whiteColor];
    
    // Create custom cart button
    self.cartButton = [[ZECartButton alloc] initWithFrame:CGRectMake(0, 0, 100, 44)];
    [self.cartButton addTarget:self action:@selector(goToCart:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:self.cartButton];
    ZEAppDelegate *delegate = [[UIApplication sharedApplication] delegate];
    self.cartButton.cart = [delegate.cartManager getCartForLocation:self.location bar:self.bar group:@"menu" event:self.event];
    
    [self reload];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)reload
{
    [SVProgressHUD showWithStatus:NSLocalizedString(@"Loading", @"Progress indicator loading")];
    RKObjectManager *objectManager = [RKObjectManager sharedManager];
    [objectManager getObjectsAtPath:[NSString stringWithFormat:@"bars/%@/menus/%@/categories", self.bar.key, self.menu.key]
                         parameters:nil
                            success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
                                // Categories loaded
                                [SVProgressHUD dismiss];
                                self.categories = [mappingResult array];
                                [self reloadTableView];
                            }
                            failure:^(RKObjectRequestOperation *operation, NSError *error) {
                                // Failed to load products
                                [SVProgressHUD dismiss];
                                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil)
                                                                                message:[error localizedDescription]
                                                                               delegate:nil
                                                                      cancelButtonTitle:NSLocalizedString(@"OK", nil)
                                                                      otherButtonTitles:nil];
                                [alert show];
                            }];
}

- (void)reloadTableView
{
    if(self.isViewLoaded) {
        [self.tableView reloadData];
        if ([self.categories count]) {
            // Event summary
            if (self.event) {
                ZEEventSummaryView *eventSummaryView = [[[NSBundle mainBundle] loadNibNamed:@"EventSummaryView" owner:self options:nil] objectAtIndex:0];
                [eventSummaryView setEvent:self.event];
                [self.tableView setTableHeaderView:eventSummaryView];
            } else {
                [self.tableView setTableHeaderView:nil];
            }
        } else {
            ZEEmptyView *emptyView = [[[NSBundle mainBundle] loadNibNamed:@"EmptyTableView" owner:self options:nil] objectAtIndex:0];
            [emptyView setupViewForTableView:self.tableView text:NSLocalizedString(@"No categories found", nil)];
            [self.tableView setTableHeaderView:emptyView];
        }
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [self.cartButton updateAnimated:NO];
    [super viewWillAppear:animated];
}

- (void)goToCart:(id)sender
{
    [self performSegueWithIdentifier:@"ViewCart" sender:sender];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"ShowProducts"]) {
        ZEProductsViewController *detailViewController = [segue destinationViewController];
        detailViewController.placeKey = self.bar.key;
        ZECategory *category = [self.categories objectAtIndex:[self.tableView indexPathForSelectedRow].row];
        detailViewController.category = category.key;
        detailViewController.categoryName = category.name;
        detailViewController.group = @"menu";
        detailViewController.location = self.location;
        detailViewController.bar = self.bar;
        detailViewController.menu = self.menu;
        detailViewController.event = self.event;
    } else if ([segue.identifier isEqualToString:@"ViewCart"]) {
        ZECartViewController *cartViewController = [segue destinationViewController];
        cartViewController.location = self.location;
        cartViewController.bar = self.bar;
        cartViewController.group = @"menu";
        cartViewController.delegate = (ZETabBarController *)self.navigationController.tabBarController;
        cartViewController.event = self.event;
    }
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [self.categories count] > 0 ? 1 : 0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.categories count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"CategoryCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    // Configure the cell...
    ZECategory *category = [self.categories objectAtIndex:indexPath.row];
    cell.textLabel.text = category.name;
    
    return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

@end
