//
//  ZEPinViewController.h
//  Zerved
//
//  Created by Anders Rasmussen on 03/08/12.
//  Copyright (c) 2012 Zerved. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ZEPinViewControllerDelegate;

@interface ZEPinViewController : UIViewController <UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UILabel *labelOne;
@property (weak, nonatomic) IBOutlet UILabel *labelTwo;
@property (weak, nonatomic) IBOutlet UILabel *labelThree;
@property (weak, nonatomic) IBOutlet UILabel *labelFour;
@property (weak, nonatomic) IBOutlet UITextField *pinTextField;
@property (weak, nonatomic) IBOutlet UILabel *subtitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *helpLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerLabel;
@property (weak, nonatomic) IBOutlet UIButton *forgotButton;
@property (weak, nonatomic) id <ZEPinViewControllerDelegate> delegate;
@property BOOL dismissed;
@property (strong, nonatomic) NSString *header;
@property (strong, nonatomic) NSString *subtitle;
@property (strong, nonatomic) NSString *helpText;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *cancelButton;
@property (nonatomic, assign) BOOL displayForgotButton;

- (void)showError:(NSString *)message;
- (void)dismiss;
- (IBAction)cancel:(id)sender;
- (IBAction)forgotPin:(id)sender;
@end

@protocol ZEPinViewControllerDelegate <NSObject>
- (void)pinViewControllerDidEnterPin:(ZEPinViewController *)controller pin:(NSString *)pin;
- (void)pinViewControllerDidCancel:(ZEPinViewController *)controller;
- (void)pinViewControllerForgotPin:(ZEPinViewController *)controller;
@end