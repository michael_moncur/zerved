//
//  ZETextFieldLabelCell.m
//  Zerved
//
//  Created by Anders Rasmussen on 15/05/13.
//
//

#import "ZETextFieldLabelCell.h"

@implementation ZETextFieldLabelCell

@synthesize inputField;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
