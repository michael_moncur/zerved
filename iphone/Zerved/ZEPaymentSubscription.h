//
//  ZEPaymentSubscription.h
//  Zerved
//
//  Created by Anders Rasmussen on 15/05/13.
//
//

#import <Foundation/Foundation.h>

@interface ZEPaymentSubscription : NSObject
@property (nonatomic, copy) NSString *key;
@property (nonatomic, copy) NSString *description;
@end
