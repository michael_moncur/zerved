//
//  ZELocationsViewController.m
//  Zerved
//
//  Created by Anders Rasmussen on 01/08/13.
//
//  Container view for the location tabs (all locations, nearby, my locations, recommend)
//

#import "ZELocationsViewController.h"
#import "ZELocationsAlphabeticViewController.h"

@interface ZELocationsViewController ()

@end

@implementation ZELocationsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1) {
        self.navigationController.navigationBar.tintColor = [UIColor blackColor];
    }
    
    [self.switchViewControl setTitle:NSLocalizedString(@"Nearby", @"Button for nearby locations") forSegmentAtIndex:0];
    [self.switchViewControl setTitle:NSLocalizedString(@"All", @"Button to list all locations") forSegmentAtIndex:1];
    [self.switchViewControl setTitle:NSLocalizedString(@"Recommend", @"Button for recommend a venue") forSegmentAtIndex:2];
    
    UIViewController<ZELocationsViewProtocol> *vc = [self viewControllerForSegmentIndex:self.switchViewControl.selectedSegmentIndex];
    if (vc != nil) {
        [self addChildViewController:vc];
        vc.view.frame = self.view.bounds;
        [self.view addSubview:vc.view];
        self.currentViewController = vc;
        self.currentSegmentIndex = 0;
    }
        
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appDidEnterForeground:) name:UIApplicationWillEnterForegroundNotification object:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setSwitchViewControl:nil];
    [super viewDidUnload];
}

- (void)viewDidAppear:(BOOL)animated
{
    [self refreshSegments];
}

- (void)appDidEnterForeground:(NSNotification *)notification
{
    [self refreshSegments];
}

- (void)refreshSegments
{
    // Toggle segment button for merchant's own locations depending on merchant ID set in settings
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults synchronize];
    NSString *merchantID = [defaults stringForKey:@"merchant_id"];
    BOOL merchantIDset = merchantID != nil && ![merchantID isEqualToString:@""];
    NSString *mineLabel = NSLocalizedString(@"Mine", @"Button for locations owned by user");
    NSString *recommendLabel = NSLocalizedString(@"Recommend", @"Button for recommend a venue");
    if (merchantIDset && [[self.switchViewControl titleForSegmentAtIndex:2] isEqualToString:recommendLabel]) {
        // Switch recommend view to mine view
        [self.switchViewControl setTitle:mineLabel forSegmentAtIndex:2];
        // If view should change, move away from it
        if (self.switchViewControl.selectedSegmentIndex == 2) {
            [self.switchViewControl setSelectedSegmentIndex:0];
            [self segmentChanged:self.switchViewControl];
        }
    } else if (!merchantIDset && [[self.switchViewControl titleForSegmentAtIndex:2] isEqualToString:mineLabel]) {
        // Switch mine to recommend
        [self.switchViewControl setTitle:recommendLabel forSegmentAtIndex:2];
        // If view should change, move away from it
        if (self.switchViewControl.selectedSegmentIndex == 2) {
            [self.switchViewControl setSelectedSegmentIndex:0];
            [self segmentChanged:self.switchViewControl];
        }
    }
}

- (IBAction)segmentChanged:(UISegmentedControl *)sender {
    CGFloat width = self.view.frame.size.width;
    CGFloat height = self.view.frame.size.height;
    UIViewController<ZELocationsViewProtocol> *vc = [self viewControllerForSegmentIndex:sender.selectedSegmentIndex];
    if (vc == nil) {
        sender.selectedSegmentIndex = 0;
        return;
    }
    
    // Move from current view controller to vc
    if (sender.selectedSegmentIndex < self.currentSegmentIndex) {
        // Move left
        vc.view.frame = CGRectMake(0-width, 0, width, height);
    } else {
        // Move right
        vc.view.frame = CGRectMake(width, 0, width, height);
    }
    [self addChildViewController:vc];
    [self transitionFromViewController:self.currentViewController toViewController:vc duration:0.4 options:UIViewAnimationOptionTransitionNone animations:^{
        if (sender.selectedSegmentIndex < self.currentSegmentIndex) {
            self.currentViewController.view.frame = CGRectMake(width, 0, width, height);
        } else {
            self.currentViewController.view.frame = CGRectMake(0-width, 0, width, height);
        }
        vc.view.frame = CGRectMake(0, 0, width, height);
        //[self.currentViewController.view removeFromSuperview];
        //vc.view.frame = self.view.bounds;
        [self.view addSubview:vc.view];
    } completion:^(BOOL finished) {
        [vc didMoveToParentViewController:self];
        [self.currentViewController removeFromParentViewController];
        self.currentViewController = vc;
        self.currentSegmentIndex = sender.selectedSegmentIndex;
    }];
    self.navigationItem.title = vc.title;
}

- (IBAction)reload:(id)sender {
    [self refreshSegments];
    [self.currentViewController reload];
}

- (UIViewController<ZELocationsViewProtocol> *)viewControllerForSegmentIndex:(NSInteger)index {
    UIViewController<ZELocationsViewProtocol> *vc;
    
    switch (index) {
        case 0:
            vc = [self.storyboard instantiateViewControllerWithIdentifier:@"LocationsNearbyViewController"];
            break;
        case 1:
        {
            // Only Danish merchants are active at the moment. Show list of Danish merchants instead of countryselection.            
            //vc = [self.storyboard instantiateViewControllerWithIdentifier:@"CountriesViewController"];

            ZELocationsAlphabeticViewController *destinationController = [self.storyboard instantiateViewControllerWithIdentifier:@"ZELocationsAlphabeticViewController"];
            
            ZECountry *country = [[ZECountry alloc] init];
            country.name = @"Danmark";
            country.countryCode = @"DK";
            
            destinationController.country = country;
            [self.navigationController pushViewController:destinationController animated:YES];
        }
        return nil;
        case 2: {
            NSString *merchantID = [[NSUserDefaults standardUserDefaults] stringForKey:@"merchant_id"];
            BOOL merchantIDset = merchantID != nil && ![merchantID isEqualToString:@""];
            if (merchantIDset) {
                vc = [self.storyboard instantiateViewControllerWithIdentifier:@"MerchantLocationsViewController"];
            } else {
                vc = [self.storyboard instantiateViewControllerWithIdentifier:@"RecommendVenueController"];
            }
            break;
        }
    }
    return vc;
}

@end
