//
//  ZEWebViewController.m
//  Zerved
//
//  Created by Anders Rasmussen on 11/04/12.
//  Copyright (c) 2012 Zerved. All rights reserved.
//
//  Render a webpage and let webpages call app methods using special prefixed URL requests
//

#import "ZEAppDelegate.h"
#import "ZEWebViewController.h"
#import "SVProgressHUD.h"

@interface ZEWebViewController ()

@end

@implementation ZEWebViewController

@synthesize webView = _webView;
@synthesize loginCancelled = _loginCancelled;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.loginCancelled = NO;
    //self.webView.scrollView.contentInset = UIEdgeInsetsMake(64, 0, 0, 48);
}

- (void)viewDidUnload
{
    [super viewDidUnload];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (NSString *)getUrl:(NSString *)url
{
    return [NSString stringWithFormat:@"%@%@", SERVER_URL, url];
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    NSString *requestString = [[request URL] absoluteString];
    if ([requestString hasPrefix:@"js-frame:"]) {
        // App call from webpage
        
        NSArray *components = [requestString componentsSeparatedByString:@":"];
        
        NSString *function = (NSString*)[components objectAtIndex:1];
        int callbackId = [((NSString*)[components objectAtIndex:2]) intValue];
        NSString *argsAsString = [(NSString*)[components objectAtIndex:3] 
                                  stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSData *argsAsData = [argsAsString dataUsingEncoding:NSUTF8StringEncoding]; 
        
        NSError *e = nil;
        NSDictionary *jsonArgs = [NSJSONSerialization JSONObjectWithData:argsAsData options:NSJSONReadingMutableContainers error:&e];
        
        [self handleCall:function callbackId:callbackId args:jsonArgs];

        return NO;
    } else if ([requestString hasPrefix:SERVER_URL]) {
        // Internal link
        return YES;
    } else if ([requestString hasPrefix:BASE_SERVER_URL]) {
        // Internal link
        return YES;
#if defined(DEBUG) || defined(DEVELOPMENT)
#ifdef EMBEDDED_MODE
    } else if ([requestString containsString:@"www.zervedapp"]) {
        // Internal link
        return YES;
    } else if ([requestString hasPrefix:@"http://da.zervedapp"]) {
        // Internal link
        return YES;
#endif
#endif
    } else if ([requestString hasPrefix:@"http:"] || [requestString hasPrefix:@"https:"] || [requestString hasPrefix:@"itms:"]) {
        // Open external links in Safari app
        NSLog(@"Open external link");
        [[UIApplication sharedApplication] openURL:[request URL]];
        return NO;
    }
    return YES;
}

// Call this function when you have results to send back to javascript callbacks
// callbackId : int comes from handleCall function
// args: list of objects to send to the javascript callback
- (void)returnResult:(int)callbackId args:(id)arg, ...;
{
    va_list argsList;
    NSMutableArray *resultArray = [[NSMutableArray alloc] init];
    
    if(arg != nil){
        [resultArray addObject:arg];
        va_start(argsList, arg);
        while((arg = va_arg(argsList, id)) != nil)
            [resultArray addObject:arg];
        va_end(argsList);
    }
    
    //NSString *resultArrayString = [json stringWithObject:resultArray allowScalar:YES error:nil];
    
    //[self stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:@"NativeBridge.resultForCallback(%d,%@);",callbackId,resultArrayString]];
}

- (void)handleCall:(NSString*)functionName callbackId:(int)callbackId args:(NSDictionary*)args
{
    if ([functionName isEqualToString:@"reload"]) {
        [self reload];
        [self returnResult:callbackId args:nil];
    } else if ([functionName isEqualToString:@"resetLogin"]) {
        ZEAppDelegate *delegate = [[UIApplication sharedApplication] delegate];
        ZEConsumerManager *consumer = delegate.consumer;
        [consumer deleteAccount];
        [self reload];
        [self returnResult:callbackId args:nil];
    } else if ([functionName isEqualToString:@"deleteAccount"]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Warning", nil) message:NSLocalizedString(@"Your saved cards will be deleted and you will lose access to your receipts. The app will be reset with an empty Zerved account. Are you sure you want to delete your account?", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"Keep", nil) otherButtonTitles:NSLocalizedString(@"Delete", nil), nil];
        [alert setTag:1];
        [alert show];
    } else if ([functionName isEqualToString:@"loginNewAccount"]) {
        ZEAppDelegate *delegate = [[UIApplication sharedApplication] delegate];
        ZEConsumerManager *consumer = delegate.consumer;
        consumer.delegate = self;
        [consumer authenticateNewAccountFromView:self email:@""];
    } else if ([functionName isEqualToString:@"login"]) {
        // Login button clicked, open login
        //[self performSegueWithIdentifier:@"Login" sender:nil];
        ZEConsumerManager *consumer = ((ZEAppDelegate *)[[UIApplication sharedApplication] delegate]).consumer;
        consumer.delegate = self;
        [consumer authenticateFromView:self];
    } else if ([functionName isEqualToString:@"autoOpenLogin"]) {
        // On page load, only open login if it has not already been opened and then cancelled
        if (!self.loginCancelled) {
            //[self performSegueWithIdentifier:@"Login" sender:nil];
            ZEConsumerManager *consumer = ((ZEAppDelegate *)[[UIApplication sharedApplication] delegate]).consumer;
            consumer.delegate = self;
            [consumer authenticateFromView:self];
        }
    } else if ([functionName isEqualToString:@"logout"]) {
        //[self showErrorPageInWebView:self.webView withMessage:NSLocalizedString(@"Logged out", nil)];
        ZEConsumerManager *consumer = ((ZEAppDelegate *)[[UIApplication sharedApplication] delegate]).consumer;
        consumer.accessToken = @"";
        self.loginCancelled = YES;
        [self reload];
    } else {
        DLog(@"Unimplemented method '%@'",functionName);
    }
}

- (void)webViewDidStartLoad:(UIWebView *)webView
{
    [SVProgressHUD showWithStatus:NSLocalizedString(@"Loading", @"Progress indicator loading")];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [SVProgressHUD dismiss];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    // Ignore error for request made before previous request completed
    if ([error code] == NSURLErrorCancelled) {
        return;
    }
    [SVProgressHUD dismiss];
    // Ignore "Frame load interrupted" received after opening external URLs in Safari
    if (error.code == 102 && [error.domain isEqual:@"WebKitErrorDomain"]) return;
    [self showErrorPageInWebView:webView withMessage:[error localizedDescription]];
}

- (void)showErrorPageInWebView:(UIWebView *)webView withMessage:(NSString *)message
{
    // Load error page, insert error description, and display in webview
    NSData *errorData = [NSData dataWithContentsOfFile:[NSString stringWithFormat:@"%@/errorpage.html", [[NSBundle mainBundle] bundlePath]]];
    NSString *errorPageString = [[NSString alloc] initWithData:errorData encoding:NSUTF8StringEncoding];
    errorPageString = [errorPageString stringByReplacingOccurrencesOfString:@"{{ message }}" withString:message];
    [webView loadHTMLString:errorPageString baseURL:nil];
}

- (void)reload{
	// Reload data source
	[self.webView reload];
}

- (IBAction)reload:(id)sender
{
    [self reload];
}

- (void)addGreyBackground
{
    UIImageView *backgroundImage;
    if ([[UIScreen mainScreen] bounds].size.height == 568) {
        backgroundImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"bg_grey-568h.png"]];
    } else {
        backgroundImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"bg_grey.png"]];
    }
    [self.view addSubview:backgroundImage];
    [self.view sendSubviewToBack:backgroundImage];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    /*
    if ([[segue identifier] isEqualToString:@"Login"]) {
        UINavigationController *navController = [segue destinationViewController];
        self.loginController = [navController.viewControllers objectAtIndex:0];
        self.loginController.delegate = self;
    }
    */
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 1 && buttonIndex == 1) {
        // Delete account
        ZEAppDelegate *delegate = [[UIApplication sharedApplication] delegate];
        ZEConsumerManager *consumer = delegate.consumer;
        consumer.delegate = self;
        [consumer deleteAccount];
        self.loginCancelled = YES;
        [self reload];
    }
}

#pragma mark -
#pragma mark ZELoginViewControllerDelegate Methods

/*
- (void)loginViewControllerDidLogin
{
    [self dismissModalViewControllerAnimated:YES];
    [self reload];
}

- (void)loginViewControllerDidCancel
{
    [self dismissModalViewControllerAnimated:YES];
    self.loginCancelled = YES;
}
*/

#pragma mark -
#pragma mark ZEConsumerDelegate Methods

- (void)didAuthenticate
{
    DLog(@"Webview did authenticate");
    [self reload];
}

- (void)didFailToAuthenticate
{
    DLog(@"Webview did fail to authenticate");
    self.loginCancelled = YES;
}

@end
