//
//  ZEEventsViewController.h
//  Zerved
//
//  Created by Anders Rasmussen on 24/10/13.
//
//

#import <UIKit/UIKit.h>
#import <RestKit/RestKit.h>
#import "ZELocation.h"
#import "ZEEvent.h"

#define BASE_EVENT_CELL_HEIGHT 60
#define BASE_EVENT_TITLE_HEIGHT 21
#define EVENT_TITLE_WIDTH 272
#define ICON_TITLE_OFFSET_Y 6

@interface ZEEventsViewController : UITableViewController

@property (nonatomic, strong) ZELocation *location;
@property (nonatomic, strong) NSArray *events;
@property (nonatomic, strong) UIDatePicker *datePicker;
@property (nonatomic, strong) UILabel *headerLabel;
@property BOOL isLoaded;

@end
