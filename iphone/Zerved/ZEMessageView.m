
//
//  ZEMessageView.m
//  Zerved
//
//  Created by Zerved Development Mac on 20/08/13.
//
//

#import "ZEMessageView.h"

@implementation ZEMessageView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        self.title = [[UILabel alloc] initWithFrame:CGRectMake( 10,
                                                                5,
                                                               self.frame.size.width - 20,
                                                               25)];
        self.title.textColor = [UIColor whiteColor];
        self.title.textAlignment = NSTextAlignmentCenter;
        self.title.backgroundColor = [UIColor clearColor];
        self.title.font = [UIFont fontWithName:@"HelveticaNeue" size:18.0f];
        
        
        self.message = [[UILabel alloc] initWithFrame:CGRectMake(10,
                                                                 25,
                                                                 self.frame.size.width - 20,
                                                                 55)];
        self.message.textColor = [UIColor whiteColor];
        self.message.textAlignment = NSTextAlignmentCenter;
        self.message.backgroundColor = [UIColor clearColor];
        [self.message setFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:14.0f]];
        self.message.numberOfLines=0;
        self.message.lineBreakMode=NSLineBreakByWordWrapping;
        
        [self.layer insertSublayer:self.title.layer atIndex:1];
        [self.layer insertSublayer:self.message.layer atIndex:2];
    }
    return self;
}

- (void)setMessageTitle:(NSString *) title
{
    self.title.text = (title == nil) ? @"" : title;
}

- (void)setMessageContent:(NSString *) content
{
    self.message.text = (content == nil) ? @"" : content;
}

- (void) setCommentTheme:(NSString *)comment
{
    self.title.textColor = [UIColor blackColor];
    self.title.textAlignment = NSTextAlignmentLeft;
    self.message.textColor = [UIColor darkGrayColor];
    self.message.textAlignment = NSTextAlignmentLeft;
    self.backgroundColor = [UIColor colorWithRed:0.95 green:0.95 blue:0.95 alpha:1.0];
    [self setMessageTitle:NSLocalizedString(@"Comment", nil)];
    [self setMessageContent:comment];
}

/*
//Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
