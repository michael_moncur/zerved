//
//  ZEQuoteFee.h
//  Zerved
//
//  Created by Anders Rasmussen on 17/06/13.
//
//

#import <Foundation/Foundation.h>

@interface ZEQuoteFee : NSObject

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSNumber *price;
@property (nonatomic, strong) NSString *priceFormatted;

@end
