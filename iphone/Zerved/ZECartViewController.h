//
//  ZECartViewController.h
//  Zerved
//
//  Created by Anders Rasmussen on 30/05/13.
//
//

#import <UIKit/UIKit.h>
#import <RestKit/RestKit.h>
#import "ZELocation.h"
#import "ZEBar.h"
#import "ZEEvent.h"
#import "ZEQuote.h"
#import "ZEConsumer.h"
#import "ZEModalWebpageViewController.h"
#import "ZEConsumerDelegate.h"
#import "ZEPinViewController.h"
#import "ZEServiceViewController.h"

@protocol ZECartViewControllerDelegate;

@interface ZECartViewController : UITableViewController <UITextFieldDelegate, ZEModalWebpageViewControllerDelegate, ZEConsumerDelegate, ZEPinViewControllerDelegate, ZEServiceDelegate, UIAlertViewDelegate>

@property (strong, nonatomic) NSString *group;
@property (nonatomic, strong) ZELocation *location;
@property (nonatomic, strong) ZEBar *bar;
@property (nonatomic, strong) ZEEvent *event;
@property (nonatomic, strong) ZEQuote *quote;
@property (nonatomic, strong) ZEConsumer *consumer;
@property (weak, nonatomic) UITextField *tableNumberField;
@property (weak, nonatomic) IBOutlet UILabel *waitingTimeLabel;
@property (weak, nonatomic) UILabel *tipPercentageLabel;
@property (strong, nonatomic) ZEPinViewController *pinController;
@property (weak, nonatomic) id <ZECartViewControllerDelegate> delegate;
@property (nonatomic, strong) NSTimer *timer;
@property (nonatomic, strong) UIActivityIndicatorView *activityIndicator;
@property (nonatomic, strong) UIView *footerView;
@property (nonatomic, strong) UIButton *placeOrderButton;
@property (nonatomic, strong) NSURLConnection *addFavouriteConnection;
@property (retain, nonatomic) NSHTTPURLResponse *lastResponse;
@property (nonatomic) NSUInteger tabIndex;
@property (nonatomic, strong) NSMutableArray *section1Fields;
@property (nonatomic, strong) NSMutableArray *allowedPaymentMethodButtons;
- (IBAction)acceptTermsClicked:(UISwitch *)sender;
- (IBAction)placeOrder:(id)sender;
- (IBAction)toggleFavourite:(UIButton *)sender;
- (IBAction)addTip:(id)sender;

@end

@protocol ZECartViewControllerDelegate <NSObject>
- (void)cartViewControllerDidPlaceOrder:(ZECartViewController *)controller id:(NSString *)orderId key:(NSString *)orderKey fromTabIndex:(NSInteger)tabIndex;
@end