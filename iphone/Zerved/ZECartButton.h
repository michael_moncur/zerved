//
//  ZECartButton.h
//  Zerved
//
//  Created by Anders Rasmussen on 20/11/13.
//
//

#import <UIKit/UIKit.h>
#import "ZECart.h"

@interface ZECartButton : UIButton

@property (weak, nonatomic) ZECart *cart;

- (void)updateAnimated:(BOOL)animated;

@end
