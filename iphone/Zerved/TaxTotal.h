//
//  TaxTotal.h
//  Zerved
//
//  Created by Anders Rasmussen on 16/10/13.
//
//

#import <CoreData/CoreData.h>

@interface TaxTotal : NSObject

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSDecimalNumber *rate;
@property (nonatomic, strong) NSString *totalFormatted;

@end
