//
//  ZECountry.h
//  Zerved
//
//  Created by Anders Rasmussen on 01/08/13.
//
//

#import <Foundation/Foundation.h>

@interface ZECountry : NSObject

@property (nonatomic, strong) NSString *countryCode;
@property (nonatomic, strong) NSString *name;

@end
