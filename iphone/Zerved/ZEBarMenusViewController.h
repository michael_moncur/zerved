//
//  ZEBarMenusViewController.h
//  Zerved
//
//  Created by Zerved Development Mac on 28/08/13.
//
//

#import <UIKit/UIKit.h>
#import "ZEBar.h"
#import "ZEMenu.h"
#import "ZELocation.h"
#import "ZEEvent.h"

@interface ZEBarMenusViewController : UITableViewController

@property (nonatomic, retain) ZEBar *bar;
@property (nonatomic, retain) ZELocation *location;
@property (nonatomic, strong) ZEEvent *event;
@property (nonatomic, retain) NSArray *menus;
@end
