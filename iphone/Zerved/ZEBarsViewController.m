//
//  ZEBarsViewController.m
//  Zerved
//
//  Created by Anders Rasmussen on 24/10/13.
//
//  List bars available at location. The bars go to ZEBarMenusViewController
//  or directly to ZEBarCategoriesViewController if there's only one menu.
//

#import "ZEBarsViewController.h"
#import "ZEBarMenusViewController.h"
#import "ZEBarCategoriesViewController.h"
#import "SVProgressHUD.h"
#import "ZEEventSummaryView.h"

@interface ZEBarsViewController ()

@end

@implementation ZEBarsViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Event summary
    if (self.event) {
        if (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1) {
            // Disabled for iOS 6 because the autolayout with title height resizing does not layout correctly.
            // For some reason it works on other pages, but not this one. I have not been able to find
            // the bug, so I had to disable it for IOS6.
            ZEEventSummaryView *eventSummaryView = [[[NSBundle mainBundle] loadNibNamed:@"EventSummaryView" owner:self options:nil] objectAtIndex:0];
            [eventSummaryView setEvent:self.event];
            [self.tableView setTableHeaderView:eventSummaryView];
        }
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.bars count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"BarCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    ZEBar *bar = [self.bars objectAtIndex:indexPath.row];
    cell.textLabel.text = bar.name;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    ZEBar *bar = [self.bars objectAtIndex:indexPath.row];
    if ([bar.menus count] == 0) {
        // No menus
        [SVProgressHUD showErrorWithStatus:NSLocalizedString(@"Event is not taking orders", @"Error msg for event with no bars/menus")];
    } else if ([bar.menus count] > 1) {
        // List menus
        ZEBarMenusViewController *menusController = [self.storyboard instantiateViewControllerWithIdentifier:@"BarMenusViewController"];
        menusController.location = self.location;
        menusController.bar = bar;
        menusController.title = bar.name;
        menusController.event = self.event;
        [self.navigationController pushViewController:menusController animated:YES];
    } else {
        // One menu, list categories
        ZEMenu *menu = [bar.menus firstObject];
        ZEBarCategoriesViewController *categoriesController = [self.storyboard instantiateViewControllerWithIdentifier:@"BarCategoriesViewController"];
        categoriesController.location = self.location;
        categoriesController.title = bar.name;
        categoriesController.bar = bar;
        categoriesController.menu = menu;
        categoriesController.event = self.event;
        [self.navigationController pushViewController:categoriesController animated:YES];
    }
}

@end
