//
//  ZECountriesViewController.h
//  Zerved
//
//  Created by Anders Rasmussen on 01/08/13.
//
//

#import <UIKit/UIKit.h>
#import "ZELocationsViewController.h"
#import <RestKit/RestKit.h>

@interface ZECountriesViewController : UITableViewController<ZELocationsViewProtocol>

@property (strong, nonatomic) NSArray *countries;

@end
