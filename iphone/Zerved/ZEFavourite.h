//
//  ZEFavourite.h
//  Zerved
//
//  Created by Zerved Development Mac on 26/07/13.
//
//

#import <Foundation/Foundation.h>
#import "ZEBar.h"
#import "ZELocation.h"
#import "ZEProduct.h"
#import "ZECartItemOptionValue.h" 

@interface ZEFavourite : NSObject

@property (nonatomic, copy) NSNumber *favouriteId;
@property (nonatomic, copy) NSString *key;
@property (nonatomic, retain) ZEBar *bar;
@property (nonatomic, retain) ZEProduct *product;
@property (nonatomic, retain) ZELocation *location;
@property (nonatomic, strong) NSMutableArray *options;

@property NSString* optionsString;

-(id) initWithProduct:(ZEProduct*)product andOptions:(NSMutableArray*)opt;

- (BOOL)isEqual:(id)anObject;

@end
