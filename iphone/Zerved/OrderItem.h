//
//  OrderItem.h
//  Zerved
//
//  Created by Zerved Development Mac on 15/08/13.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@interface OrderItem : NSObject

@property (nonatomic, strong) NSString * key;
@property (nonatomic, strong) NSString * productName;
@property (nonatomic, strong) NSNumber * quantity;
@property (nonatomic, strong) NSString * totalExclTaxFormatted;
@property (nonatomic, strong) NSString * totalInclTaxFormatted;
@property (nonatomic, strong) NSString * totalTaxFormatted;
@property (nonatomic, strong) NSString * totalBeforeLoyaltyDiscountInclTaxFormatted;
@property (nonatomic, strong) NSArray * optionValues;
@property (nonatomic, strong) NSObject * order;

@end
