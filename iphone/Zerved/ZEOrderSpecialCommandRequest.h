//
//  ZEOrderSpecialCommandRequest.h
//  Zerved
//
//  Created by Henrik Karlsen on 09/07/16.
//
//

#import <Foundation/Foundation.h>

@interface ZEOrderSpecialCommandRequest : NSObject

@property (nonatomic, strong) NSString *special_command;

@end
