//
//  ZETermsViewController.m
//  Zerved
//
//  Created by Anders Rasmussen on 01/08/12.
//  Copyright (c) 2012 Zerved. All rights reserved.
//

#import "ZEModalWebpageViewController.h"

@interface ZEModalWebpageViewController ()

@end

@implementation ZEModalWebpageViewController

@synthesize delegate = _delegate;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1) {
        self.navigationController.navigationBar.tintColor = [UIColor blackColor];
    }
    [self reload];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(switchToOrder) name:@"switchToOrder" object:nil];
}

- (void)reload
{
    NSString *fullURL = [self getUrl:self.url];
    NSURL *url = [NSURL URLWithString:fullURL];
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
    [self.webView loadRequest:requestObj];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    self.delegate = nil;
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (IBAction)done:(id)sender
{
    [self.delegate modalWebpageViewControllerDone:self];
}

- (void)switchToOrder
{
    // Close view when switched to order from notification
    [self dismissViewControllerAnimated:NO completion:nil];
}

@end
