//
//  ZEProductOption.m
//  Zerved
//
//  Created by Anders Rasmussen on 06/06/13.
//
//

#import "ZEProductOption.h"
#import "ZEProductOptionValue.h"

@implementation ZEProductOption

- (NSInteger)selectedIndex
{
    for (NSInteger i=0; i<[self.values count]; i++) {
        if (((ZEProductOptionValue *)[self.values objectAtIndex:i]).selected) {
            return i;
        }
    }
    return -1;
}

- (void)setSelectedIndex:(NSInteger)index
{
    for (NSInteger i=0; i<[self.values count]; i++) {
        ZEProductOptionValue *value = [self.values objectAtIndex:i];
        value.selected = i == index;
    }
}

@end
