//
//  ZELocationsViewController.h
//  Zerved
//
//  Created by Anders Rasmussen on 01/08/13.
//
//

#import <UIKit/UIKit.h>

@protocol ZELocationsViewProtocol <NSObject>

- (void)reload;

@end

@interface ZELocationsViewController : UIViewController

@property (strong, nonatomic) UIViewController<ZELocationsViewProtocol> *currentViewController;
@property (weak, nonatomic) IBOutlet UISegmentedControl *switchViewControl;
@property (nonatomic) NSInteger currentSegmentIndex;
- (IBAction)segmentChanged:(UISegmentedControl *)sender;
- (IBAction)reload:(id)sender;
@end
