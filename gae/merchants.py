#!/usr/bin/env python
import webapp2
import sys
import os
import re
sys.path.append('app')
sys.path.append('lib')
#import app.views.core
from webapp2_extras import routes

# debug = os.environ.get('SERVER_SOFTWARE','').startswith('Development') or os.environ['CURRENT_VERSION_ID'].split(".")[0] == 'dev'
debug = os.environ.get('SERVER_SOFTWARE', '').startswith('Development') or re.split('\.|-dot-', os.environ['CURRENT_VERSION_ID'])[0] == 'dev'

def build_crud_routes(entity_name, entity_name_plural, class_name):
    return routes.PathPrefixRoute('/'+entity_name_plural, [
        webapp2.Route('', 'app.views.merchants.'+entity_name_plural+'.'+class_name+'ListHandler', entity_name+'-list'),
        webapp2.Route('/new', 'app.views.merchants.'+entity_name_plural+'.'+class_name+'EditHandler', entity_name+'-new'),
        webapp2.Route('/edit/<'+entity_name+'_id:\d+>', 'app.views.merchants.'+entity_name_plural+'.'+class_name+'EditHandler', entity_name+'-edit'),
        webapp2.Route('/save', 'app.views.merchants.'+entity_name_plural+'.'+class_name+'EditHandler', entity_name+'-save'),
        webapp2.Route('/delete/<'+entity_name+'_id:\d+>', 'app.views.merchants.'+entity_name_plural+'.'+class_name+'DeleteHandler', entity_name+'-delete'),
        ])
    
app = webapp2.WSGIApplication([
    # routes.DomainRoute(r'<:(zervedapp\.com|www\.zervedapp\.com|zervedapp\.appspot\.com|dev\.zervedapp\.appspot\.com|demo\.zervedapp\.appspot\.com|new\.zervedapp\.appspot\.com|dev\.latest\.zervedapp\.appspot\.com|demo\.latest\.zervedapp\.appspot\.com|new\.latest\.zervedapp\.appspot\.com|zerved\.dev|localhost|\d+\.\d+\.\d+\.\d+)>', [
    # routes.DomainRoute(r'<:([^.])+(\.|-dot-)?><:(zervedapp\.com|www(\.|-dot-)zervedapp\.com|zervedapp\.appspot\.com|dev(\.|-dot-)zervedapp\.appspot\.com|demo(\.|-dot-)zervedapp\.appspot\.com|new(\.|-dot-)zervedapp\.appspot\.com|dev(\.|-dot-)latest(\.|-dot-)zervedapp\.appspot\.com|demo(\.|-dot-)latest(\.|-dot-)zervedapp\.appspot\.com|new(\.|-dot-)latest(\.|-dot-)zervedapp\.appspot\.com|zerved\.dev|localhost|\d+\.\d+\.\d+\.\d+)>', [
    routes.DomainRoute(r'<:(zervedapp\.com|www(\.|-dot-)zervedapp\.com|zervedapp\.appspot\.com|dev(\.|-dot-)zervedapp\.appspot\.com|demo(\.|-dot-)zervedapp\.appspot\.com|new(\.|-dot-)zervedapp\.appspot\.com|dev(\.|-dot-)latest(\.|-dot-)zervedapp\.appspot\.com|demo(\.|-dot-)latest(\.|-dot-)zervedapp\.appspot\.com|new(\.|-dot-)latest(\.|-dot-)zervedapp\.appspot\.com|zerved\.dev|localhost|\d+\.\d+\.\d+\.\d+)>', [
        webapp2.Route('/_ah/channel/connected/', 'app.views.merchants.serve.ConnectedHandler'),
        webapp2.Route('/_ah/channel/disconnected/', 'app.views.merchants.serve.DisconnectedHandler'),
        routes.RedirectRoute('/merchants', redirect_to='/merchants/dashboard'),
        routes.PathPrefixRoute('/merchants', [
            routes.RedirectRoute('/', redirect_to='/merchants/dashboard'),
            webapp2.Route('/init', 'app.views.merchants.merchants.InitHandler'),
            webapp2.Route('/image/<entity_key:[^/]+>/<width:\d+>/<height:\d+>', 'app.views.core.ImageHandler', 'image'),
            webapp2.Route('/accounts', 'app.views.merchants.merchants.AccountListHandler', 'user-merchants'),
            routes.PathPrefixRoute('/products/<group:menu|entrance>', [
                webapp2.Route('', 'app.views.merchants.products.ProductListHandler', 'product-list'),
                webapp2.Route('/category/<category_id:[^/]+>', 'app.views.merchants.products.ProductListHandler', 'product-list-category'),
                webapp2.Route('/new', 'app.views.merchants.products.ProductEditHandler', 'product-new'),
                webapp2.Route('/edit/<product_id:\d+>', 'app.views.merchants.products.ProductEditHandler', 'product-edit'),
                webapp2.Route('/save', 'app.views.merchants.products.ProductEditHandler', 'product-save'),
                webapp2.Route('/delete/<product_id:\d+>', 'app.views.merchants.products.ProductDeleteHandler', 'product-delete'),
                webapp2.Route('/clone/<product_id:[^/]+>', 'app.views.merchants.products.ProductCloneHandler', 'product-clone'),
                webapp2.Route('/category/<category_id:[^/]+>/sort', 'app.views.merchants.products.ProductSortHandler', 'product-sort'),
            ]),
            routes.PathPrefixRoute('/categories/<group:menu|entrance>', [
                webapp2.Route('', 'app.views.merchants.categories.CategoryListHandler', 'category-list'),
                webapp2.Route('/new', 'app.views.merchants.categories.CategoryEditHandler', 'category-new'),
                webapp2.Route('/edit/<category_id:\d+>', 'app.views.merchants.categories.CategoryEditHandler', 'category-edit'),
                webapp2.Route('/save', 'app.views.merchants.categories.CategoryEditHandler', 'category-save'),
                webapp2.Route('/delete/<category_id:\d+>', 'app.views.merchants.categories.CategoryDeleteHandler', 'category-delete'),
                webapp2.Route('/sort', 'app.views.merchants.categories.CategorySortHandler', 'category-sort'),
            ]),
            webapp2.Route('/menupreview', 'app.views.merchants.products.MenuPreviewHandler', 'menupreview'),
            build_crud_routes('taxclass', 'taxclasses', 'TaxClass'),
            build_crud_routes('schedule', 'schedules', 'Schedule'),
            build_crud_routes('pricerule', 'pricerules', 'PriceRule'),
            build_crud_routes('merchant', 'merchants', 'Merchant'),
            build_crud_routes('location', 'locations', 'Location'),
          
            webapp2.Route('/locations/cover', 'app.views.merchants.locations.LocationCoverMainHandler', 'location-cover'),
            webapp2.Route('/locations/cover/upload', 'app.views.merchants.locations.LocationCoverUploadHandler', 'location-upload'),
            webapp2.Route('/locations/cover/all/<location_id:\d+>', 'app.views.merchants.locations.LocationCoverGetImagesHandler', 'location-cover-all'),
            webapp2.Route('/locations/cover/sort/<location_id:\d+>', 'app.views.merchants.locations.LocationCoverSortHandler', 'location-cover-sort'),
            webapp2.Route('/locations/cover/delete/<location_id:\d+>/<blob_key:[^/]+>', 'app.views.merchants.locations.LocationCoverDeleteHandler', 'location-cover-delete'),
            webapp2.Route('/locations/cover/serve/<blob_key:[^/]+>', 'app.views.merchants.locations.LocationCoverServeHandler', 'location-serve'),

            build_crud_routes('bar', 'bars', 'Bar'),
            build_crud_routes('menu', 'menus', 'Menu'),
            webapp2.Route('/menus/sort', 'app.views.merchants.menus.MenuSortHandler', 'menu-sort'),
            build_crud_routes('event', 'events', 'Event'),
            webapp2.Route('/events/clone/<event_id:[^/]+>', 'app.views.merchants.events.EventCloneHandler', 'event-clone'),
            webapp2.Route('/events/exclude/<recurring_event_id:\d+>/<event_id:\d+>', 'app.views.merchants.events.EventExcludeHandler', 'event-exclude'),

            webapp2.Route('/locations/<location_id:\d+>/newbar', 'app.views.merchants.bars.BarNewHandler', 'location-newbar'),
            webapp2.Route('/settlements', 'app.views.merchants.invoices.SettlementsHandler', 'settlements'),
            webapp2.Route('/settlements/new', 'app.views.merchants.invoices.InvoiceNewHandler', 'invoice-new'),
            webapp2.Route('/orders', 'app.views.merchants.orders.OrdersListHandler', 'list-orders'),
            webapp2.Route('/orders/cancel', 'app.views.merchants.orders.CancelOrderHandler', 'cancel-order'),
            webapp2.Route('/savesettlement', 'app.views.merchants.invoices.SaveSettlementHandler', 'save-settlement'),
            routes.PathPrefixRoute('/profile', [
                webapp2.Route('', 'app.views.merchants.merchants.ProfileHandler', 'profile-edit'),
                webapp2.Route('', 'app.views.merchants.merchants.ProfileHandler', 'profile-save'),
                ]),
            #webapp2.Route('/tools', 'app.views.merchants.tools.ToolsHandler', 'tools'),
            #webapp2.Route('/tools/copymerchantmenu', 'app.views.merchants.tools.CopyMerchantMenuHandler', 'copy-merchant-menu'),
            routes.PathPrefixRoute('/sales', [
                webapp2.Route('/revenue', 'app.views.merchants.revenue.RevenueHandler', 'revenue'),
                webapp2.Route('/productssold', 'app.views.merchants.productssold.ProductsSoldHandler', 'productssold'),
                webapp2.Route('/productsopen', 'app.views.merchants.productssold.ProductsOpenHandler', 'productsopen'),
                webapp2.Route('/invoices', 'app.views.merchants.invoices.InvoiceListHandler', 'invoice-list'),
                webapp2.Route('/invoices/<invoice_id:\d+>', 'app.views.merchants.invoices.InvoiceViewHandler', 'invoice-view'),
                webapp2.Route('/invoices/<invoice_id:\d+>/orders', 'app.views.merchants.invoices.InvoiceOrdersHandler', 'invoice-orders'),
                webapp2.Route('/orders', 'app.views.merchants.orders.MerchantOrdersListHandler', 'list-merchant-orders'),
                ]),
            webapp2.Route('/serve/bars', 'app.views.merchants.serve.ServeBarsHandler', 'serve-bars'),
            webapp2.Route('/serve/<bar_id:\d+>', 'app.views.merchants.serve.ServeHandler', 'serve'),
            webapp2.Route('/serveipad/<bar_id:\d+>', 'app.views.merchants.serve.ServeIpadAppHandler', 'serve-ipad'),
            webapp2.Route('/startserve/<bar_id:\d+>', 'app.views.merchants.serve.StartServeHandler', 'serve-start'),
            webapp2.Route('/serve/<bar_id:\d+>/pull/event/<event_key:[^/]+>', 'app.views.merchants.serve.PullHandler', 'serve-pull'),
            webapp2.Route('/serve/<bar_id:\d+>/pull/changes/<since:[^/]+>', 'app.views.merchants.serve.PullChangesHandler', 'serve-pull-changes'),
            webapp2.Route('/serve/process', 'app.views.merchants.serve.ProcessOrderHandler', 'serve-process'),
            webapp2.Route('/serve/ping', 'app.views.merchants.serve.PingHandler', 'serve-ping'),
            webapp2.Route('/serve/end', 'app.views.merchants.serve.EndServeHandler', 'serve-end'),
            webapp2.Route('/serve/<bar_id:\d+>/settings', 'app.views.merchants.serve.BarSettingsHandler', 'serve-settings'),
            webapp2.Route('/updatedatastore', 'app.views.merchants.update.UpdateHandler'),
            webapp2.Route('/unknownuser', 'app.views.merchants.users.UnknownUserHandler', 'user-unknown'),
            webapp2.Route('/adminrestricted', 'app.views.merchants.users.AdminRestrictedHandler', 'admin-restricted'),
            webapp2.Route('/pricetiers', 'app.views.merchants.pricetiers.PriceTiersViewHandler', 'pricetiers-view'),
            webapp2.Route('/pricetiers/edit', 'app.views.merchants.pricetiers.PriceTiersEditHandler', 'pricetiers-edit'),
            webapp2.Route('/pricetiers/update', 'app.views.merchants.pricetiers.PriceTiersEditHandler', 'pricetiers-update'),
            webapp2.Route('/pricetiers/init', 'app.views.merchants.pricetiers.PriceTiersInitHandler', 'pricetiers-init'),
            webapp2.Route('/orders/view/<order_id:\d+>', 'app.views.merchants.orders.OrderViewHandler', 'order-view'),
            webapp2.Route('/consumers/<consumer_id:\d+>', 'app.views.merchants.consumers.ConsumerHandler', 'consumer-view'),
            #news
            webapp2.Route('/news', 'app.views.merchants.news.NewsListHandler', 'news-list'),
            webapp2.Route('/news/new', 'app.views.merchants.news.NewsEditHandler', 'news-new'),
            webapp2.Route('/news/edit/<news_id:\d+>', 'app.views.merchants.news.NewsEditHandler', 'news-edit'),
            webapp2.Route('/news/delete/<news_id:\d+>', 'app.views.merchants.news.NewsDeleteHandler', 'news-delete'),
            webapp2.Route('/news/image/<news_id:\d+>', 'app.views.merchants.news.NewsImageViewerHandler', 'news-image'),
            webapp2.Route('/news/upload', 'app.views.merchants.news.NewsImageUploadHandler', 'upload-image'),
            webapp2.Route('/news/image/view/<key:[^/]+>', 'app.views.merchants.news.NewsImageUploadHandler', 'view-image'),
            # Dashboard
            webapp2.Route('/dashboard', 'app.views.merchants.dashboard.DashboardHandler', 'dashboard'),
            webapp2.Route('/dashboard/update_news', 'app.views.merchants.dashboard.NewsReadHandler', 'read-news'),


            webapp2.Route('/tutorial/endpoint', 'app.views.merchants.tutorials.TutorialEndHandler', 'tutorial-end'),
            webapp2.Route('/tutorial/<page:[^/]+>', 'app.views.merchants.tutorials.TutorialHandler', 'tutorial-json'),


            ]),
        ]),
    # routes.DomainRoute(r'<subdomain:[^.]+>.<:(zervedapp\.com|www\.zervedapp\.com|zervedapp\.appspot\.com|dev\.zervedapp\.appspot\.com|zerved\.dev)>', [
    routes.DomainRoute(r'<subdomain:[^.]+>.<:(zervedapp\.com|www(\.|-dot-)zervedapp\.com|zervedapp\.appspot\.com|dev(\.|-dot-)zervedapp\.appspot\.com|zerved\.dev)>', [
        webapp2.Route('/merchants/', 'app.views.core.RemoveSubdomainHandler'),
        webapp2.Route('/merchants', 'app.views.core.RemoveSubdomainHandler')
        ]),
    webapp2.Route('/_ah/warmup', 'app.tasks.warmup.WarmupHandler'),
    ],
    debug=debug, config={'webapp2_extras.sessions': {'secret_key':'0f9s0f98s0df'}, 'webapp2_extras.i18n':{'default_timezone':'Europe/Copenhagen'}})
