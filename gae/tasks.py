#!/usr/bin/env python
import webapp2
import sys
sys.path.append('app')
sys.path.append('lib')
from webapp2_extras import routes


app = webapp2.WSGIApplication([
    routes.PathPrefixRoute('/tasks', [
        webapp2.Route('/cleanup', 'app.tasks.cleanup.CleanupHandler'),
        webapp2.Route('/closeinactivepersistingbars', 'app.tasks.cleanup.ClosePersistingBarsWithNoActiveMenus'),
        webapp2.Route('/closeinvoices', 'app.tasks.invoices.CloseInvoicesHandler'),
        webapp2.Route('/sendiphonemsg', 'app.tasks.notifications.SendIphoneMsgHandler'),
        webapp2.Route('/sendandroidmsg', 'app.tasks.notifications.SendAndroidMsgHandler'),
        webapp2.Route('/sendembeddedmsg', 'app.tasks.notifications.SendEmbeddedMsgHandler'),
        webapp2.Route('/update_datastore', 'app.tasks.update.UpdateHandler'),
        webapp2.Route('/deletecard', 'app.tasks.deletecard.DeleteCardHandler'),
        webapp2.Route('/recordanalytics', 'app.tasks.analytics.RecordOrderHandler'),
        webapp2.Route('/indexlocations', 'app.tasks.search.IndexLocationsHandler'),
        webapp2.Route('/monitorerrors', 'app.tasks.monitor.MonitorHandler'),
        webapp2.Route('/processorder', 'app.tasks.orders.ProcessOrderHandler'),
        webapp2.Route('/process_multi_orders', 
            'app.tasks.orders.ProcessMultipleOrderHandler'),
        webapp2.Route('/completeorder', 
            'app.tasks.orders.CompleteOrderHandler'),
        webapp2.Route('/clearproductcache', 'app.tasks.cache.ClearProductCacheHandler'),
        webapp2.Route('/closebar', 'app.tasks.cleanup.CloseDisconnectedBarHandler'),
        webapp2.Route('/updatesession', 'app.tasks.orders.UpdateSessionHandler'),
        webapp2.Route('/updatesession_multiple_orders',
            'app.tasks.orders.UpdateSessionHandlerMultipleOrders'),
        webapp2.Route('/createloyaltycodes', 'app.tasks.loyaltycodes.CreateLoyaltyCodesHandler'),
        webapp2.Route('/deleteloyaltycodes', 'app.tasks.loyaltycodes.DeleteLoyaltyCodesHandler'),
        webapp2.Route('/deletenordpaysubscriptions', 'app.tasks.deletenordpaysubscriptions.DeleteNordpaySubscriptionsHandler'),

        ])
    ],
    debug=True, config={'webapp2_extras.i18n':{'default_timezone':'Europe/Copenhagen'}})
