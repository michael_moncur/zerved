{% trans %}Software Licenses{% endtrans %}

{% trans %}The following sets forth attribution to third party software that may be contained in portions of the Zerved app.{% endtrans %}

--------------------

{% trans %}The following software may be included in this product:{% endtrans %} ActionBarSherlock. {% trans %}The software contains the following license and notice:{% endtrans %}

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

--------------------

{% trans %}The following software may be included in this product:{% endtrans %} SlidingMenu. {% trans %}The software contains the following license and notice:{% endtrans %}

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

--------------------

{% trans %}The following software may be included in this product:{% endtrans %} SwitchWidget. {% trans %}The software contains the following license and notice:{% endtrans %}

Copyright (C) 2012 Benoit 'BoD' Lubek (BoD@JRAF.org)
Copyright (C) 2006 The Android Open Source Project

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

--------------------

{% trans %}The following software may be included in this product:{% endtrans %} UrlImageViewHelper. {% trans %}The software contains the following license and notice:{% endtrans %}

Copyright 2012 Koushik Dutta

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

--------------------

{% trans %}The following software may be included in this product:{% endtrans %} CWAC Wakeful. {% trans %}The software contains the following license and notice:{% endtrans %}

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

--------------------

{% trans %}The following software may be included in this product:{% endtrans %} card.io. {% trans %}The software contains the following license and notice:{% endtrans %}

Summary

card.io's Android SDK uses code from the following libraries (with gratitude!):

Willow Garage's OpenCV
Loopj's Android Asnyc HTTP
Full licenses

Willow Garage's OpenCV, BSD license:

Actual license text not provided -- [the OpenCV website](http://opencv.willowgarage.com/wiki/) says "OpenCV is released under a BSD license, it is free for both academic and commercial use.", with links to http://opensource.org/licenses/bsd-license.php and http://en.wikipedia.org/wiki/BSD_licenses
Loopj's Android Async HTTP, Apache License 2.0:

Actual license text not provided -- [The loopj website](http://loopj.com/android-async-http/) states "The Android Asynchronous Http Client is released under the Android-friendly Apache License, Version 2.0. Read the full license here: http://www.apache.org/licenses/LICENSE-2.0"

--------------------

{% trans %}The following software may be included in this product:{% endtrans %} Android ViewPagerIndicator. {% trans %}The software contains the following license and notice:{% endtrans %}

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.