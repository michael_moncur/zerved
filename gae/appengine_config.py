import sys
sys.path.append('app')
sys.path.append('lib')
import os

appstats_CALC_RPC_COSTS = False

debug = os.environ.get('SERVER_SOFTWARE', '').startswith('Development') or os.environ['CURRENT_VERSION_ID'].split(".")[0] == 'dev'

def webapp_add_wsgi_middleware(app):
    if debug:
        from google.appengine.ext.appstats import recording
        app = recording.appstats_wsgi_middleware(app)
    return app
    
def namespace_manager_default_namespace_for_request():
    major_version = os.environ['CURRENT_VERSION_ID'].split(".")[0]
    if major_version == 'dev':
        return 'dev'
    elif major_version == 'demo' or major_version == 'new':
        return 'demo'
    else:
        return ''