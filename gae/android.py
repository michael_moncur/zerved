#!/usr/bin/env python
import webapp2
import sys
import os
sys.path.append('app')
sys.path.append('lib')
from webapp2_extras import routes

debug = os.environ.get('SERVER_SOFTWARE','').startswith('Development') or os.environ['CURRENT_VERSION_ID'].split(".")[0] == 'dev'

app = webapp2.WSGIApplication([
    routes.PathPrefixRoute('/android', [
        webapp2.Route('/info', 'app.views.android.info.InfoHandler'),
        webapp2.Route('/terms', 'app.views.android.info.TermsHandler'),
        webapp2.Route('/privacy', 'app.views.android.info.PrivacyHandler'),
        webapp2.Route('/epaypayment', 'app.views.android.payment.UpdateHandler', 'epay-payment-legacy'), # Old 2012-2013 Epay, removed
        webapp2.Route('/epayauthorize', 'app.views.android.payment.UpdateHandler', 'epay-payment'), # Old 2012-2013 Epay, removed
        webapp2.Route('/epayaccept', 'app.views.android.payment.UpdateHandler', 'epay-accept'), # Old 2012-2013 Epay, removed
        webapp2.Route('/epaycancel', 'app.views.android.payment.UpdateHandler', 'epay-cancel'), # Old 2012-2013 Epay, removed
        webapp2.Route('/epaycallback', 'app.views.android.payment.UpdateHandler', 'epay-callback'), # Old 2012-2013 Epay, removed
        webapp2.Route('/recommend', 'app.views.android.info.RecommendHandler', 'recommend'),
        webapp2.Route('/softwarelicenses', 'app.views.android.info.LicensesHandler', 'licenses'),
        ])
    ],
    debug=debug, config={'webapp2_extras.sessions': {'secret_key':'0f9s0f98s0df'}, 'webapp2_extras.i18n':{'default_timezone':'Europe/Copenhagen'}})
