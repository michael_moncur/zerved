function zeProgressHudShow(status, options) {
    //, errorState, errorDismissedCallback, ttl
    if (typeof options === 'undefined') {
        options = {};
    }
    var errorState = (typeof options.errorState === 'undefined') ? false : options.errorState;
    var ttl = (typeof options.ttl === 'undefined') ? 0 : options.ttl;
    var fadeTime= (typeof options.fadeTime === 'undefined') ? 200 : options.fadeTime;
    var hud = $('#ze-progress-hud');
    hud.toggleClass('ze-progress-error', errorState);
    hud.toggleClass('ze-progress-success', ttl > 0);
    if (status && status !== '') {
        $('#ze-progress-text').text(status).show();
    } else {
        $('#ze-progress-text').text('').hide();
    }
    if (errorState) {
       hud.on('click', function() {
           zeProgressHudDismiss();
           if (options.errorDismissedCallback) {
               options.errorDismissedCallback();
           }
       })
    }
    if (fadeTime > 0) {
        hud.fadeIn(fadeTime);
    } else {
        hud.show();
    }
    if (ttl > 0) {
        setTimeout(function() {
            zeProgressHudDismiss();
        }, ttl);
    }
}

function zeProgressHudDismiss() {
    $('#ze-progress-hud').fadeOut(100);
}
