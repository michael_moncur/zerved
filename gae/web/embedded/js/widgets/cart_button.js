function updateCartButtonQuantity(cart) {
    var items = 0;
    if (typeof cart !== 'undefined') {
        items = cart.count();
    }
    $('#ze-nav-bar-cart-button-amount').text(items);
    return items;
}

function updateCartButton(tab, cart, zeLocation, bar, group, event) {
    var items = updateCartButtonQuantity(cart);
    if (items > 0) {
        $('#ze-nav-bar-cart-button')
            .addClass('ze-has-content')
            .off('click')
            .on('click', function() {
                navigationManager.openCart(tab, zeLocation, bar, group, event);
                return false;
            }
        );
    } else {
        $('#ze-nav-bar-cart-button')
            .removeClass('ze-has-content')
            .off('click');
    }
}