function QuantityStepper(rootElement, minValue, defaultValue, notifyChange) {
    this.rootElement = rootElement;
    this.notifyChange = notifyChange;
    this.minValue = minValue;
    var self = this;
    this.minusButton = this.rootElement.find('.ze-minus');
    this.plusButton = this.rootElement.find('.ze-plus');
    this.label = this.rootElement.find('.ze-quantity-label');
    this.setValue(defaultValue);

    // Attach listeners
    this.minusButton
        .on('click', function(event) {
            event.stopPropagation();
            if (self.value > self.minValue) {
                self.setValue(self.value - 1);
            }
        });
    this.plusButton
        .on('click', function(event) {
            event.stopPropagation();
            self.setValue(self.value + 1);
        });
}

QuantityStepper.prototype.setValue = function(value) {
    this.value = value;
    if (this.value == this.minValue) {
        this.minusButton.prop('disabled', true);
    } else {
        this.minusButton.prop('disabled', false);
    }
    this.label.text(this.value);
    this.notifyChange(value);
};