//-------------------//
// ModalView         //
//-------------------//
function ModalView(rootElement, leftButtonHandler, rightButtonHandler) {
    this.rootElement = rootElement;
    this.rootElement.find('.ze-nav-bar-left-button').off('click').on('click', leftButtonHandler);
    this.rootElement.find('.ze-nav-bar-right-button').off('click').on('click', rightButtonHandler);
}

ModalView.prototype.present = function(animated, complete) {
    if (this.locked) {
        return;
    }
    if (typeof animated === 'undefined') {
        animated = true;
    }
    this.locked = true;
    if (animated) {
        if (typeof complete !== 'undefined') {
            this.rootElement.slideDown(400, complete);
        } else {
            this.rootElement.slideDown(400);
        }
    } else {
        this.rootElement.show();
        if (typeof complete !== 'undefined') {
            complete();
        }
    }
};


ModalView.prototype.dismiss = function(animated, complete) {
    if (typeof animated === 'undefined') {
        animated = true;
    }

    this.locked = false;
    if (animated) {
        if (typeof complete !== 'undefined') {
            this.rootElement.slideUp(400, complete);
        } else {
            this.rootElement.slideUp(400);
        }
    } else {
        this.rootElement.hide();
        if (typeof complete !== 'undefined') {
            complete();
        }
    }
};


//-------------------//
// OptionModalView   //
//-------------------//
function OptionModalView(productIndex, optionsIndex, goToNext, product, missingSelectionMessage, callback) {
    this.rootElement = $('#ze-modal-view-' + productIndex + '-' + optionsIndex);
    this.productIndex = productIndex;
    this.optionsIndex = optionsIndex;
    this.goToNext = goToNext;
    this.product = product;
    this.callback = callback;
    this.cartItem = null;

    var self = this;

    this.modalView = new ModalView(
        this.rootElement,
        function(evt) { // left button handler
            evt.stopPropagation();
            var type = $(this).data('type');
            if (type === 'cancel') {
                self.dismiss();
            } else {
                self.backToPrevious();
            }
        },
        function() { // right button handler
            // Validate
            var productOption = self.product.product_options[self.optionsIndex];
            if (productOption.required && Product.getSelectedIndexForOption(productOption) == -1) {
                zeProgressHudShow(missingSelectionMessage + ' ' + productOption.name, {errorState: true});
                return;
            }

            if (self.goToNext) {
                if (self.product.product_options.length > self.optionsIndex + 1) {
                    // More options, go to next
                    var optionsIndex = self.optionsIndex + 1;
                    var optionModal = new OptionModalView(self.productIndex, optionsIndex, true, product, '{% trans %}Please select{% endtrans %}', self.callback);
                    optionModal.cartItem = self.cartItem;
                    self.replaceWithModal(optionModal);
                } else {
                    // No more options, return to product list
                    self.callback(self.cartItem);
                    self.modalView.dismiss();
                }
            } else {
                self.callback();
                self.modalView.dismiss();
            }
        }
    );

    this.rootElement.find('.ze-table-view-cell').on('click', function() {
        var optionCell = $(this);
        var cellIndex = optionCell.data('row');
        var productOption = self.product.product_options[self.optionsIndex];
        var selected = productOption.values[cellIndex].selected;
        if (productOption.selection_type === 'single') {
            // Single selection
            if (!selected) {
                // Update cart item
                if (self.cartItem) {
                    self.cartItem.clearOptionId(productOption.id);
                    self.cartItem.options.push(new CartItemOptionValue({
                        'optionId': productOption.id,
                        'selectedIndex': cellIndex
                    }));
                }
                // Update product selection
                Product.setSelectedIndexForOption(productOption, cellIndex);
            } else if (selected && !productOption.required) {
                // Deselecting (only allowed if not required)
                // Remove option value
                if (self.cartItem) {
                    self.cartItem.removeOption(productOption.id, cellIndex);
                }
                Product.setSelectedIndexForOption(productOption, -1);
            }
        } else if (productOption.selection_type === 'multiple') {
            // Multiple selection
            if (!selected) {
                // Add option value
                if (self.cartItem) {
                    self.cartItem.options.push(new CartItemOptionValue({
                        'optionId': productOption.id,
                        'selectedIndex': cellIndex
                    }));
                }
                productOption.values[cellIndex].selected = true;
            } else {
                // Remove option value
                if (self.cartItem) {
                    self.cartItem.removeOption(productOption.id, cellIndex);
                }
                productOption.values[cellIndex].selected = false;
            }
        }
        self.refreshSelections();
    });

    this.refreshSelections();
}


OptionModalView.prototype.present = function() {
    this.modalView.present();
};

OptionModalView.prototype.dismiss = function() {
    this.modalView.dismiss();
};

OptionModalView.prototype.replaceWithModal = function(optionModalView) {
    optionModalView.previousModal = this;
    this.modalView.dismiss(false);
    optionModalView.modalView.present(false);
};

OptionModalView.prototype.backToPrevious = function() {
    this.modalView.dismiss(false);
    this.previousModal.modalView.present(false);
};

OptionModalView.prototype.refreshSelections = function() {
    var self = this;
    this.rootElement.find('.ze-table-view-cell').each(function(i, cell) {
        var selected = self.product.product_options[self.optionsIndex].values[i].selected === true;
        $(cell).toggleClass('ze-selected', selected);
    });
};


//----------------------//
// MultiOptionModalView //
//----------------------//
function MultiOptionModalView(rootElement, cartItem, product, strings, callback) {
    this.rootElement = rootElement;
    this.cartItem = cartItem;
    this.product = product;
    this.strings = strings;

    var self = this;
    this.modalView = new ModalView(
        this.rootElement,
        null, // left button handler
        function() { // right button handler
            // Validate
            for (var i = 0; i < self.product.product_options.length; i++) {
                var productOption = self.product.product_options[i];
                if (productOption.required && !self.cartItem.hasValueForOptionId(productOption.id)) {
                    zeProgressHudShow(strings.missingSelectionMessage + ' ' + productOption.name, {errorState: true});
                    return;
                }
            }
            self.modalView.dismiss();
            callback(self.cartItem);
        }
    );

    // Set nav bar title
    this.rootElement.find('.ze-nav-bar-title').text(this.cartItem.productName);

    // Build new content
    var tableView = this.rootElement.find('.ze-table-view-container');
    tableView.empty();

    this.product.product_options.forEach(function(productOption) {
        var sectionTitle;
        if (productOption.selection_type == 'single') {
            if (productOption.required) {
                sectionTitle = self.strings.single_title;
            } else {
                sectionTitle = self.strings.single_optional_title;
            }
        } else {
            if (productOption.required) {
                sectionTitle = self.strings.multi_title;
            } else {
                sectionTitle = self.strings.multi_optional_title;
            }
        }

        var section = $('<div class="ze-table-view-section"></div>').appendTo(tableView);
        var header = $('<div class="ze-table-view-section-header ze-with-sub-header">' + productOption.name + '<div class="ze-table-view-section-sub-header">' + sectionTitle + '</div></div>').appendTo(section);
        productOption.values.forEach(function(optionValue, cellIndex) {
            var cell = $('<div class="ze-table-view-cell ze-label-value-cell"></div>').appendTo(section);
            var cellTitle = $('<div class="ze-title">' + optionValue.label + '</div>').appendTo(cell);
            var cellValue = $('<div class="ze-value">' + optionValue.price_label + '</div>').appendTo(cell);
            var indicator = $('<span class="ze-selection-indicator fa fa-check"></span>').appendTo(cell);
            cell.append($('<hr>'));
            cell.on('click', function(e) {
                var selected = productOption.values[cellIndex].selected;
                if (productOption.selection_type === 'single') {
                    // Single selection
                    if (!selected) {
                        // Update cart item
                        if (self.cartItem) {
                            self.cartItem.clearOptionId(productOption.id);
                            self.cartItem.options.push(new CartItemOptionValue({
                                'optionId': productOption.id,
                                'selectedIndex': cellIndex
                            }));
                        }
                        // Update product selection
                        Product.setSelectedIndexForOption(productOption, cellIndex);
                    } else if (selected && !productOption.required) {
                        // Deselecting (only allowed if not required)
                        // Remove option value
                        if (self.cartItem) {
                            self.cartItem.removeOption(productOption.id, cellIndex);
                        }
                        Product.setSelectedIndexForOption(productOption, -1);
                    }
                } else if (productOption.selection_type === 'multiple') {
                    // Multiple selection
                    if (!selected) {
                        // Add option value
                        if (self.cartItem) {
                            self.cartItem.options.push(new CartItemOptionValue({
                                'optionId': productOption.id,
                                'selectedIndex': cellIndex
                            }));
                        }
                        productOption.values[cellIndex].selected = true;
                    } else {
                        // Remove option value
                        if (self.cartItem) {
                            self.cartItem.removeOption(productOption.id, cellIndex);
                        }
                        productOption.values[cellIndex].selected = false;
                    }
                }
                self.refreshSelections();
            })
        });
    });

    this.refreshSelections();
    this.modalView.present();
}


MultiOptionModalView.prototype.refreshSelections = function() {
    var self = this;
    this.rootElement.find('.ze-table-view-section').each(function(i, section) {
        var option = self.product.product_options[i];
        $(section).find('.ze-table-view-cell').each(function(j, cell) {
            var selected = self.cartItem.hasValueForOptionIdAndSelectedIndex(j, option.id);
            $(cell).toggleClass('ze-selected', selected);
        });
    });
};


//----------------------//
// ServiceModalView     //
//----------------------//
function ServiceModalView(rootElement, bar, cart, strings, callback, onCancel) {
    this.rootElement = rootElement;
    this.bar = bar;
    this.cart = cart;
    this.strings = strings;
    this.selectedMethod = this.cart.service;
    if (this.selectedMethod === 'counter' && Bar.hasToStayAndToGo(this.bar)) {
        if (this.cart.toGo) {
            this.selectedMethod = 'counter_togo';
        } else {
            this.selectedMethod = 'counter_tostay'
        }
    }

    var self = this;
    this.modalView = new ModalView(
        this.rootElement,
        function() { // left button handler
            self.modalView.dismiss();
            if (onCancel) {
                onCancel();
            }
        },
        function() { // right button handler
            // Validate
            if (!self.selectedMethod ||self.selectedMethod == '') {
                zeProgressHudShow(self.strings.missingSelectionMessage, {errorState: true});
                return;
            }
            if (self.selectedMethod === 'counter' || self.selectedMethod === 'counter_tostay') {
                self.cart.service = 'counter';
                self.cart.toGo = false;
            }
            if (self.selectedMethod === 'counter_togo') {
                self.cart.service = 'counter';
                self.cart.toGo = true;
            }
            if (self.selectedMethod === 'table') {
                var n = parseInt(self.tableNumberInput.val());
                if (isInt(n)) {
                    self.cart.service = 'table';
                    self.cart.tableNumber = n.toString();
                } else {
                    self.tableNumberInput.focus();
                    zeProgressHudShow(self.strings.missingTableMessage, {errorState: true});
                    return;
                }
            }
            if (self.selectedMethod === 'delivery') {
                // TODO: implement delivery method
            }
            self.modalView.dismiss();
            callback();
        }
    );

    // Build new content
    var tableView = this.rootElement.find('.ze-table-view-container');
    tableView.empty();
    var section = $('<div class="ze-table-view-section"></div>').appendTo(tableView);
    var header = $('<div class="ze-table-view-section-header"></div>').appendTo(section);
    var cell;

    if (Bar.hasCounterService(this.bar)) {
        if (Bar.hasToStayAndToGo(this.bar)) {
            // Counter to stay
            cell = $('<div class="ze-table-view-cell"></div>').appendTo(section);
            cell.data('service-method', 'counter_tostay');
            $('<div class="ze-title">' + this.strings.counterToStayTitle + '</div>').appendTo(cell);
            $('<span class="ze-selection-indicator fa fa-check"></span>').appendTo(cell);
            cell.append($('<hr>'));
            cell.on('click', function(e) {
                self.selectedMethod = 'counter_tostay';
                self.refreshSelections();
            });

            // counter toGo
            cell = $('<div class="ze-table-view-cell"></div>').appendTo(section);
            cell.data('service-method', 'counter_togo');
            $('<div class="ze-title">' + this.strings.counterToGoTitle + '</div>').appendTo(cell);
            $('<span class="ze-selection-indicator fa fa-check"></span>').appendTo(cell);
            cell.append($('<hr>'));
            cell.on('click', function(e) {
                self.selectedMethod = 'counter_togo';
                self.refreshSelections();
            });
        } else {
            // counter
            cell = $('<div class="ze-table-view-cell"></div>').appendTo(section);
            cell.data('service-method', 'counter');
            $('<div class="ze-title">' + this.strings.counterTitle + '</div>').appendTo(cell);
            $('<span class="ze-selection-indicator fa fa-check"></span>').appendTo(cell);
            cell.append($('<hr>'));
            cell.on('click', function(e) {
                self.selectedMethod = 'counter';
                self.toGo = false;
                self.refreshSelections();
            });
        }
    }
    if (this.bar.table_service) {
        // table
            cell = $('<div class="ze-table-view-cell ze-label-value-cell"></div>').appendTo(section);
            cell.data('service-method', 'table');
            $('<div class="ze-title">' + this.strings.tableTitle + '</div>').appendTo(cell);
            this.tableNumberInput = $('<input type="number" class="ze-value" placeholder="XX" value="' + this.cart.tableNumber + '">').appendTo(cell);
            $('<span class="ze-selection-indicator fa fa-check"></span>').appendTo(cell);
            cell.append($('<hr>'));
            cell.on('click', function(e) {
                self.selectedMethod = 'table';
                self.refreshSelections();
            });
            this.tableNumberInput.on('change', function(e) {
                self.refreshSelections();
            });
    }
    if (this.bar.delivery_service) {
        // TODO: Implement delivery service
    }

/*
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(switchToOrder) name:@"switchToOrder" object:nil];
 */

    this.refreshSelections();
    this.modalView.present();
}


ServiceModalView.prototype.refreshSelections = function() {
    var self = this;
    this.rootElement.find('.ze-table-view-section').each(function(i, section) {
        $(section).find('.ze-table-view-cell').each(function(j, element) {
            var cell = $(element);
            var serviceMethod = cell.data('service-method');
            var selected = self.selectedMethod === serviceMethod;
            cell.toggleClass('ze-selected', selected);
        });
    });
};

function isInt(n) {
    return n === +n && n === (n|0);
}


//----------------------//
// PaymentTypeModalView //
//----------------------//
function PaymentTypeModalView(rootElement, epayEnabled, mobilepayAppswitchEmbeddedEnabled, selectionCallback) {
    this.rootElement = rootElement;
    var self = this;
    this.modalView = new ModalView(
        this.rootElement,
        function() { // left button handler
            self.modalView.dismiss();
        }
    );
    var epayCell = this.rootElement.find('#ze-payment-credit-card-cell');
    if (epayEnabled) {
        epayCell.show();
        // Attach listeners
        epayCell.off('click').on('click', function() {
            self.modalView.dismiss();
            selectionCallback('credit_card');
        });
    }
    else {
        epayCell.hide();
    }

    /*this.rootElement.find('#ze-payment-paii-cell').off('click').on('click', function() {
        self.modalView.dismiss();
        selectionCallback('paii');
    });*/
    var mobilepay_native_embedded_cell = this.rootElement.find('#ze-payment-mobilepay_native_embedded-cell');
    if (mobilepayAppswitchEmbeddedEnabled) {
        mobilepay_native_embedded_cell.show();
        mobilepay_native_embedded_cell.off('click').on('click', function () {
            self.modalView.dismiss();
            selectionCallback('mobilepay_appswitch_embedded');
        });
    }
    else {
        mobilepay_native_embedded_cell.hide();
    }

    this.modalView.present();
}


//----------------------//
// PincodeModalView //
//----------------------//
function PincodeModalView(rootElement, callback, forgotHandler) {
    this.rootElement = rootElement;
    if (isEmbeddedAndroidApp() || (isEmbeddedIOSApp() && iOSAppOSVersion() < 8)) {
        this.hiddenInput = this.rootElement.find('textarea.ze-pincode-hidden-input');
        this.hiddenInput.toggleClass('ios', true);
    } else {
        this.hiddenInput = this.rootElement.find('input.ze-pincode-hidden-input');
    }
    this.hiddenInput.show();
    this.circles = this.rootElement.find('.ze-pincode-cicle');
    this.reset();

    var self = this;
    this.modalView = new ModalView(
        this.rootElement,
        function() { // left button handler
            if (self.disableInput) {
                return;
            }
            self.modalView.dismiss();
        }
    );

    // Attach listeners
    if (!isEmbeddedAndroidApp()) {
        this.circles.off('click').on('click', function (e) {
            if (self.disableInput) {
                return false;
            }
            e.stopPropagation();
            self.focus();
        });
    }
    this.rootElement.find('.ze-forgot-button').off('click').on('click', function() {
        if (self.disableInput) {
            return false;
        }
        forgotHandler();
    });

    this.hiddenInput
        .off('input')
        .on('input', function(inputEvent) {
            if (self.disableInput) {
                return false;
            }
            var pincode = self.hiddenInput.val();
            var invalidChars = /[^0-9]/gi;
            if (invalidChars.test(pincode)) {
                pincode = pincode.replace(invalidChars,"");
                self.hiddenInput.val(pincode);
                return;
            }
            var count = pincode.length;
            self.circles.each(function(i, circle) {
                $(circle).toggleClass('ze-contains-input', i < count);
            });
            if (count == 4) {
                callback(pincode);
            }
        });

    this.modalView.present(false, function() {
        self.focus();
    });
}

PincodeModalView.prototype.focus = function() {
    this.hiddenInput.get(0).focus();
};

PincodeModalView.prototype.dismiss = function() {
    this.hiddenInput.blur();
    this.modalView.dismiss();
};

PincodeModalView.prototype.reset = function() {
    this.disableInput = false;
    this.hiddenInput.val('');
    this.circles.toggleClass('ze-contains-input', false);
};
