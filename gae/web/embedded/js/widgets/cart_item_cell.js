function CartItemCell(data) {
    this.data = data;
    this.cartItem = data.cartItem;
    if (typeof this.cartItem.isExpanded === 'undefined') {
        this.cartItem.isExpanded = false;
    }

    // Variable initializations
    var titleClasses = 'ze-title';
    var titleLabel = this.getTitleString();
    var priceLabel;
    var optionsLabel;
    if (this.cartItem.quoteItem) {
        priceLabel = this.cartItem.quoteItem.price_formatted;
        optionsLabel = this.cartItem.quoteItem.options_summary;
    } else {
        priceLabel = '--';
        optionsLabel = '';
    }

    // Root view
    this.view = $('<div class="ze-table-view-cell ze-cart-item-cell"></div>')
        .off('click')
        .on('click', this, function(e) {
            e.data.toggleExpand();
        }
    );

    // Favourite button
    if (data.favouriteHandler) {
        titleClasses = titleClasses + ' ze-has-favourite';
        this.favouriteButton = $('<span class="ze-favourite-button fa"></span>')
            .off('click')
            .on('click', this, function(e) {
                e.stopPropagation();
                data.favouriteHandler();
            })
            .appendTo(this.view);
        this.toggleFavouriteButton(this.cartItem.isFavourite);
    }

    // Title
    this.title = $('<div class="' + titleClasses + '">' + titleLabel + '</div>').appendTo(this.view);

    // Price
    this.price = $('<div class="ze-price-label">' + priceLabel + '</div>').appendTo(this.view);

    // Chevron
    this.chevron = $('<span class="ze-disclosure fa fa-chevron-right"></span>').appendTo(this.view);

    if (this.cartItem.quoteItem.product_has_options) {
        // Options
        this.options = $('<div class="ze-options-summary">' + optionsLabel + '</div>').appendTo(this.view);

        // Edit button
        this.editButton = $('<div class="ze-edit-button">' + data.editButtonTitle + '</div>')
            .off('click')
            .on('click', this, function (e) {
                e.stopPropagation();
                data.editButtonHandler();
            })
            .appendTo(this.view);
    }

    // Quantity stepper
    var self = this;
    var quantityContainer = $('<div class="ze-quantity-container"><div class="ze-quantity-controls"><button class="ze-button ze-minus">-</button><button class="ze-button ze-plus">+</button></div></div>')
        .appendTo(this.view);
    new QuantityStepper(quantityContainer, 0, this.cartItem.quantity, function(value) {
        if (value == self.cartItem.quantity) {
            return;
        }
        self.price.text('--');
        data.quantityStepperHandler(value);
        self.title.text(self.getTitleString());
    });


    $('<hr>').appendTo(this.view);

    // Update the view according to expansion state
    this.updateViewExpansion(false);
}


CartItemCell.prototype.getTitleString = function() {
    if (this.cartItem.quoteItem) {
        return this.cartItem.quantity + ' x ' + this.cartItem.quoteItem.name;
    } else {
        return this.cartItem.quantity + ' x ' + this.cartItem.productName;
    }
};


CartItemCell.prototype.toggleFavouriteButton = function(on) {
    this.favouriteButton.toggleClass('fa-star-o', !on).toggleClass('fa-star', on);
};


CartItemCell.prototype.toggleExpand = function() {
    this.cartItem.isExpanded = !this.cartItem.isExpanded;
    this.data.expansionStateChangeHandler();
    this.updateViewExpansion(true);
};

CartItemCell.prototype.updateViewExpansion = function(animated) {
    var angle;
    var height;
    if (this.cartItem.isExpanded) {
        angle = 90;
        height = 110;
    } else {
        angle = 0;
        height = 48;
    }
    if (animated) {
        this.chevron.animate(
            {
                borderSpacing: angle
            }, {
                step: function (now, fx) {
                    $(this).css('transform', 'rotate(' + now + 'deg)');
                },
                duration: 100
            },
            'linear'
        );
        this.view.animate(
            {
                height: height
            },
            100,
            'linear'
        );
    } else {
        this.chevron.css('transform', 'rotate(' + angle + 'deg)');
        this.view.css('height', height + 'px');
    }
};
