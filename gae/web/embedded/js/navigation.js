function NavigationManager() {
    var navigationData = localStorage.navigationData;
    if (typeof navigationData === 'undefined') {
        this.dictionary = {};
        this.currentTab = null;
    } else {
        var navigationObject = JSON.parse(navigationData);
        for (var key in navigationObject) {
            this[key] = navigationObject[key];
        }
    }

    // Handle tab bar buttons
    $('.ze-tab-bar-item').on('click', function(e) {
        var tab = $(this).data('tab');
        if (tab === navigationManager.currentTab) {
            return true;
        } else {
            return navigationManager.switchToTab(tab);
        }
    });
}

NavigationManager.prototype.save = function() {
    localStorage.navigationData = JSON.stringify(this);
};

NavigationManager.prototype.pushPageInTab = function(tab, pageData) {
    tabData = this.dictionary[tab];
    if (typeof tabData === 'undefined') {
        this.popToTabRoot(tab);
        tabData = this.dictionary[tab];
    }
    tabData.push(pageData);
    this.currentTab = tab;
    this.save();
    this._doPageChange(pageData);
};

NavigationManager.prototype.popPageInTab = function(tab) {
    tabData = this.dictionary[tab];
    if (tabData && tabData.length > 1) {
        tabData.pop();
        var pageData = tabData[tabData.length - 1];
        this.save();
        this._doPageChange(pageData);
    }
};

NavigationManager.prototype.replacePageInTab = function(tab, pageData) {
    tabData = this.dictionary[tab];
    if (tabData && tabData.length > 1) {
        tabData.pop();
        tabData.push(pageData);
        this.currentTab = tab;
        this.save();
        this._doPageChange(pageData);
    }
};

NavigationManager.prototype.popToTabRoot = function(tab) {
    tabData = this.dictionary[tab] = [];
    tabData.push({
        type: 'tab_root',
        tab: tab
    });
    this.currentTab = tab;
    this.save();
};

NavigationManager.prototype.popToTypesInTab = function(tab, types, changePage) {
    if (typeof changePage === 'undefined') {
        changePage = true;
    }
    tabData = this.dictionary[tab];
    if (tabData) {
        var foundPage = false;
        var popCount = 0;
        for (var i = tabData.length - 1; i >= 0; i--) {
            var pageData = tabData[i];
            foundPage = $.inArray(pageData.type, types) > -1;
            if (foundPage) {
                break;
            }
            popCount++;
        }
        if (foundPage) {
            if (popCount > 0) {
                tabData.splice(-popCount);
            }
            this.save();
            var newPageData = tabData[tabData.length - 1];
            if (changePage) {
                this._doPageChange(newPageData);
            }
        } else {
            this.popToTabRoot(tab);
        }
    }
};

NavigationManager.prototype.switchToTab = function(tab) {
    tabData = this.dictionary[tab];
    if (tabData) {
        var pageData = tabData[tabData.length - 1];
        this.currentTab = tab;
        this.save();
        this._doPageChange(pageData);
        return false;
    }
    return true;
};

NavigationManager.prototype._doPageChange = function(pageData) {
    var message = '';
    if (globalPageChangingMessage) {
        message = globalPageChangingMessage;
    }
    if (!isEmbeddedAndroidApp()) {
        zeProgressHudShow(message, {fadeTime: 0});
    }
    //console.log(pageData);
    //alert(pageData.type);
    if (pageData.type === 'tab_root') {
        if (this.currentTab === pageData.tab) {
            window.location.href = '/embedded/tab/' + pageData.tab;
        }
    } else if (pageData.type === 'location') {
        window.location.href = '/embedded/locations/' + pageData.locationKey;
    } else if (pageData.type === 'bar_list') {
        window.location.href = '/embedded/locations/' + pageData.locationKey  + '/bars';
    } else if (pageData.type === 'menu_list') {
        window.location.href = '/embedded/bars/' + pageData.barKey  + '/menus';
    } else if (pageData.type === 'category_list') {
        localStorage.categoryListTitle = pageData.title;
        window.location.href = '/embedded/bars/' + pageData.barKey  + '/menus/' + pageData.menuKey + '/categories';
    } else if (pageData.type === 'product_list') {
        window.location.href = '/embedded/bars/' + pageData.barKey  + '/menus/' + pageData.menuKey + '/categories/' + pageData.categoryKey + '/products';
    } else if (pageData.type === 'product') {
        window.location.href = '/embedded/products/' + pageData.productKey;
    } else if (pageData.type === 'entrance_product_list') {
        window.location.href = '/embedded/locations/' + pageData.locationKey  + '/categories/' + pageData.categoryKey + '/products';
    } else if (pageData.type === 'more_events_list') {
        window.location.href = '/embedded/locations/' + pageData.locationKey  + '/events';
    } else if (pageData.type === 'cart') {
        if (pageData.group === 'menu') {
            if (typeof pageData.event !== 'undefined' && pageData.event != null) {
                window.location.href = '/embedded/groups/' + pageData.group + '/bars/' + pageData.barKey + '/events/' + pageData.eventKey + '/cart';
            } else {
                window.location.href = '/embedded/groups/' + pageData.group + '/bars/' + pageData.barKey + '/cart';
            }
        } else {
            window.location.href = '/embedded/groups/' + pageData.group + '/locations/' + pageData.locationKey + '/cart';
        }
    } else if (pageData.type === 'epay') {
        window.location.href = '/embedded/epay';
    } else if (pageData.type === 'mobilepaynativeembedded') {
        window.location.href = '/embedded/mobilepaynativeembedded';
    } else if (pageData.type === 'epaysavecard') {
        window.location.href = '/embedded/epaysavecard';
    } else if (pageData.type === 'profile') {
        window.location.href = '/embedded/tab/profile';
    }else if (pageData.type === 'order') {
        if (pageData.orderKey) {
            window.location.href = '/embedded/orders/' + pageData.orderKey;
        } else {
            window.location.href = '/embedded/orders/' + pageData.orderId;
        }
    }
};


NavigationManager.prototype.openLocation = function(tab, locationKey) {
    this.pushPageInTab(tab, {
        type: 'location',
        locationKey: locationKey
    });
};

NavigationManager.prototype.openBarList = function(tab, locationKey) {
    this.pushPageInTab(tab, {
        type: 'bar_list',
        locationKey: locationKey
    });
};

NavigationManager.prototype.openMenuList = function(tab, barKey) {
    this.pushPageInTab(tab, {
        type: 'menu_list',
        barKey: barKey
    });
};

NavigationManager.prototype.openCategoryList = function(tab, barKey, menuKey, title) {
    this.pushPageInTab(tab, {
        type: 'category_list',
        barKey: barKey,
        menuKey: menuKey,
        title: title
    });
};

NavigationManager.prototype.openProductList = function(tab, barKey, menuKey, categoryKey) {
    this.pushPageInTab(tab, {
        type: 'product_list',
        barKey: barKey,
        menuKey: menuKey,
        categoryKey: categoryKey
    });
};

NavigationManager.prototype.openProduct = function(tab, productKey) {
    this.pushPageInTab(tab, {
        type: 'product',
        productKey: productKey
    });
};

NavigationManager.prototype.openEntranceProductList = function(tab, locationKey, categoryKey) {
    this.pushPageInTab(tab, {
        type: 'entrance_product_list',
        locationKey: locationKey,
        categoryKey: categoryKey
    });
};

NavigationManager.prototype.openMoreEventsList = function(tab, locationKey) {
    this.pushPageInTab(tab, {
        type: 'more_events_list',
        locationKey: locationKey
    });
};

NavigationManager.prototype.openCart = function(tab, zeLocation, bar, group, event) {
    var data = {
        type: 'cart',
        group: group
    };
    if (group === 'menu') {
        data.barKey = bar.key;
        if (typeof event !== 'undefined' && event != null) {
            data.eventKey = event.key;
        }
    } else {
        data.locationKey = zeLocation.key;
    }
    this.pushPageInTab(tab, data);
};

NavigationManager.prototype.openEPay = function(tab) {
    this.pushPageInTab(tab, {
        type: 'epay'
    });
};

NavigationManager.prototype.openMobilePayNativeEmbedded = function(tab) {
    this.pushPageInTab(tab, {
        type: 'mobilepaynativeembedded'
    });
};

NavigationManager.prototype.openEPaySaveCard = function(tab) {
    this.pushPageInTab(tab, {
        type: 'epaysavecard'
    });
};

NavigationManager.prototype.openOrderByKey = function(tab, orderKey) {
    this.pushPageInTab(tab, {
        type: 'order',
        orderKey: orderKey,
        orderId: null
    });
};

NavigationManager.prototype.openOrderById = function(tab, orderId) {
    this.pushPageInTab(tab, {
        type: 'order',
        orderKey: null,
        orderId: orderId
    });
};

NavigationManager.prototype.loadEventForKey = function(tab, eventKey, loadingMessage, notTakingOrdersMessage) {
    url = '/api/v2/events/' + eventKey;
    zeProgressHudShow(loadingMessage);
    $.ajax({
        type: 'GET',
        url: url,
        contentType: "application/json",
        success: function (event, textStatus, jqXHR) {
            if (event.bars.length == 0) {
                // No bars
                zeProgressHudShow(notTakingOrdersMessage, {errorState: true});
            } else if (event.bars.length > 1) {
                // List bars
                zeProgressHudDismiss();
                ZEEvent.setEvent(event);
                var zeLocation = ZELocation.getLocation();
                navigationManager.openBarList(tab, zeLocation.key);
            } else {
                // One bar
                var bar = event.bars[0];
                if (bar.menus.length == 0) {
                    // NO menus
                    zeProgressHudShow(notTakingOrdersMessage, {errorState: true});
                } else if (bar.menus.length > 1) {
                    // List menus
                    zeProgressHudDismiss();
                    Bar.setBar(bar);
                    ZEEvent.setEvent(event);
                    navigationManager.openMenuList(tab, bar.key);
                } else {
                    // One menu, list categories
                    zeProgressHudDismiss();
                    var menu = bar.menus[0];
                    Bar.setBar(bar);
                    Menu.setMenu(menu);
                    ZEEvent.setEvent(event);
                    navigationManager.openCategoryList(tab, bar.key, menu.key, event.name);
                }
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert(errorThrown);
            zeProgressHudDismiss();
        }
    });
};

// Utility functions
function isEmbeddedApp() {
    return /ZervedEmbedded/.test(navigator.userAgent);
}

function isEmbeddedIOSApp() {
    // http://stackoverflow.com/questions/9038625/detect-if-device-is-ios
    var iOS = /iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream;
    return isEmbeddedApp() && iOS;
}

function iOSAppOSVersion() {
    var v = (navigator.appVersion).match(/OS (\d+)_(\d+)?/);
    var result = [parseInt(v[1], 10), parseInt(v[2], 10)];
    //console.log('version: ' + result);
    //console.log('float: ', parseFloat(result.join('.')));
    return parseFloat(result.join('.'));
}

function isEmbeddedAndroidApp() {
    return isEmbeddedApp() && /Android/.test(navigator.userAgent) && !window.MSStream;
}

function androidAppOSVersion() {
    var ua = navigator.userAgent;
    if( ua.indexOf("Android") >= 0 ) {
        return parseFloat(ua.slice(ua.indexOf("Android") + 8));
    }
    return 0;
}



// --- SINGLETON --- //
navigationManager = new NavigationManager();
