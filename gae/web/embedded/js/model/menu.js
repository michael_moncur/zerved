function Menu(data) {
    for (var key in data) {
        this[key] = data[key]
    }
}

Menu.getMenu = function() {
    var data = JSON.parse(localStorage.menu);
    return new Menu(data);
};

Menu.setMenu = function(data) {
    localStorage.menu = JSON.stringify(data);
};

Menu.remove = function() {
    localStorage.removeItem('menu');
};