function ZELocation(data) {
    for (var key in data) {
        this[key] = data[key]
    }
}

ZELocation.getLocation = function() {
    var storedData = localStorage.zeLocation;
    if (typeof storedData === 'undefined') {
        return null;
    } else {
        var data = JSON.parse(storedData);
        return new ZELocation(data);
    }
};

ZELocation.setLocation = function(zeLocation) {
    localStorage.zeLocation = JSON.stringify(zeLocation);
};

ZELocation.remove = function() {
    localStorage.removeItem('zeLocation');
};