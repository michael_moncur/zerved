//-----------------//
// ConsumerManager //
//-----------------//
function ConsumerManager() {
    var consumerManagerData = localStorage.consumerManager;
    if (typeof consumerManagerData !== 'undefined') {
        anomymousCM = JSON.parse(consumerManagerData);
        for (var key in anomymousCM) {
            this[key] = anomymousCM[key];
        }
    } else {
        this.accessToken = '';
        this.authValue = null;
        this.termsLastAccepted = null;
    }
}

ConsumerManager.prototype.save = function() {
    //localStorage.removeItem('consumerManager');
    localStorage.consumerManager = JSON.stringify(this);
};

ConsumerManager.prototype.login = function(consumerKey, password, authenticationCallback) {
    var self = this;
    var url = '/api/v2/consumers/login';
    var params = {'key': consumerKey, 'password': password};
    $.ajax({
        type: 'POST',
        url: url,
        data: JSON.stringify(params),
        contentType: "application/json",
        success: function (consumerData, textStatus, jqXHR) {
            self.accessToken = consumerData.access_token;
            self.setConsumerKey(consumerData.key);
            self.setConsumerId(consumerData.id);
            self.authValue = 'Basic ' + btoa(self.accessToken);
            self.save();
            console.log(self.authValue);
            authenticationCallback();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            if (jqXHR.status == 404) {
                // Account does not exist, delete the saved login
                localStorage.removeItem('consumerManager');
                localStorage.removeItem('consumerKey');
                localStorage.removeItem('consumerPassword');
            }
            zeProgressHudShow(errorThrown + ': ' + jqXHR.responseText, {errorState: true});
            console.log(textStatus);
            console.log(jqXHR);
        }
    });
};


ConsumerManager.prototype.createNewAccountInBackground = function(authenticationCallback) {
    var self = this;
    var url = '/api/v2/consumers/create';
    var params = {'device': getDeviceTypeFromUserAgent()};
    $.ajax({
        type: 'POST',
        url: url,
        data: JSON.stringify(params),
        contentType: "application/json",
        success: function (consumerData, textStatus, jqXHR) {
            console.log('Consumer created');
            console.log(consumerData);
            self.accessToken = consumerData.access_token;
            self.setConsumerKey(consumerData.key);
            self.setConsumerId(consumerData.id);
            localStorage.consumerPassword = consumerData.password;
            self.authValue = 'Basic ' + btoa(self.accessToken);
            self.save();
            console.log(self.authValue);
            authenticationCallback();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            zeProgressHudShow(errorThrown, {errorState: true});
        }
    });
};


ConsumerManager.prototype.authenticate = function(authenticationCallback) {
    // Check if consumer login is available in localStorage
    //localStorage.removeItem('consumerKey'); // Use this to reset authorization
    var consumerKey = localStorage.consumerKey;
    if (typeof consumerKey !== 'undefined') {
        console.log('Logging in with saved credentials:');
        console.log(localStorage.consumerKey + ' / ' + localStorage.consumerPassword);
        var password = localStorage.consumerPassword;
        this.login(consumerKey, password, authenticationCallback);
    } else {
        this.createNewAccountInBackground(authenticationCallback);
    }
};


ConsumerManager.prototype.setConsumerKey = function(key) {
    if (key) {
        localStorage.consumerKey = key;
    } else {
        localStorage.removeItem('consumerKey');
    }
};

ConsumerManager.prototype.getConsumerKey = function() {
    if (localStorage.consumerKey !== 'undefined') {
        return localStorage.consumerKey;
    }
    return null;
};


ConsumerManager.prototype.setConsumerId = function(id) {
    if (id) {
        localStorage.consumerId = id;
    } else {
        localStorage.removeItem('consumerId');
    }
};

ConsumerManager.prototype.getConsumerId = function() {
    if (localStorage.consumerId !== 'undefined') {
        return localStorage.consumerId;
    }
    return null;
};

ConsumerManager.prototype.setTermsAccepted = function(accepted) {
    if (accepted) {
        this.termsLastAccepted = new Date().getTime();
    } else {
        this.termsLastAccepted = null;
    }
    this.save();
};

ConsumerManager.prototype.termsAcceptedRecently = function() {
    if (!this.termsLastAccepted) {
        return false;
    }
    // Is terms accepted within the last 24 hours
    var nowUtc = new Date().getTime();
    //console.log('then:' + this.termsLastAccepted);
    //console.log('now: ' + nowUtc);
    //console.log('diff: ' + Math.floor(nowUtc - this.termsLastAccepted) + ' < ' + (24*60*60*1000) + '? ' + (Math.floor(nowUtc - this.termsLastAccepted) < 24*60*60*1000));
    return Math.floor(nowUtc - this.termsLastAccepted) < 24*60*60*1000;
};

consumerManager = null;
ConsumerManager.getConsumerManager = function() {
    if (!consumerManager) {
        consumerManager = new ConsumerManager();
    }
    return consumerManager;
};

function getDeviceTypeFromUserAgent() {
    return 'embedded';
}
/*
function getDeviceTypeFromUserAgent() {
    // IMPORTANT
    // ---------
    // Make sure the webview displaying this client adds the proper data to
    // the user agent header. For an app called SomeAppName with version 1.0
    // using embedded client version 1, this would be one of these two:
    //
    // SomeAppName/1.0 (iOS_embedded 1)
    // SomeAppName/1.0 (android_embedded 1)
    //
    // How to set them:
    // http://stackoverflow.com/questions/8487581/uiwebview-ios5-changing-user-agent
    // http://stackoverflow.com/questions/6783185/detect-inside-android-browser-or-webview

    if (/\(iOS_embedded [0-9]+\)/.test(navigator.userAgent)) {
        return 'iphone';
    } else if (/\(android_embedded [0-9]+\)/.test(navigator.userAgent)) {
        return 'android';
    } else {
        // We really shouldn't get here if webview is configured properly,
        // but for browser testing purposes it returns a valid value.
        return 'iphone';
    }
}
*/