function Product(data) {
    for (var key in data) {
        this[key] = data[key]
    }
}

Product.createCartItem = function(product) {
    var cartOptions = [];
    product.product_options.forEach(function(option) {
        option.values.forEach(function(value) {
            if (value.selected) {
                cartOptions.push(CartItemOptionValue.createNew(option.id, value.index));
            }
        });
    });
    return CartItem.createNew(product.key, product.quantity, cartOptions);
};

Product.getSelectedIndexForOption = function(option) {
    for (var i = 0; i < option.values.length; i++) {
        if (option.values[i].selected) {
            return i;
        }
    }
    return -1;
};

Product.setSelectedIndexForOption = function(option, index) {
    for (var i = 0; i < option.values.length; i++) {
        var value = option.values[i];
        value.selected = i == index;
    }
};

Product.getLabelWithPriceForOptionValue = function(optionValue) {
    if ((typeof optionValue.price_label === 'undefined') || (optionValue.price_label === '')) {
        return optionValue.label;
    } else {
        return optionValue.label + ' (' + optionValue.price_label + ')';
    }
};

Product.productsFromDictionary = function(data) {
    var products = [];
    data.forEach(function(entry) {
        products.push(new Product(entry));
    });
    return products;
};

Product.getProduct = function() {
    return JSON.parse(localStorage.product);
};

Product.setProduct = function(data) {
    localStorage.product = JSON.stringify(data);
};

Product.remove = function() {
    localStorage.removeItem('product');
};



//------------------------------//
// Product controller functions //
//------------------------------//
activeQuoteRequests = [];
function loadQuoteAndUpdateItems(cart, completedCallback) {
    var zeLocation = ZELocation.getLocation();
    var bar = Bar.getBar();
    var group = localStorage.group;
    var event = ZEEvent.getEvent();

    // Cancel existing requests
    activeQuoteRequests.forEach(function(request) {
        request.abort();
    });

    // Load quote from server
    var url = '/api/v2/quote';
    var data = cart.serialize();
    $.ajax({
        type: 'POST',
        url: url,
        data: data,
        contentType: "application/json",
        success: function (quote, textStatus, jqXHR) {
            zeProgressHudDismiss();
            cart.attachQuote(quote);
            cart.validateQuoteItems();
            updateCartButtonQuantity(cart);
            cart.save();
            if (typeof completedCallback !== 'undefined') {
                completedCallback();
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            zeProgressHudDismiss();
            alert(errorThrown);
        }
    });

}
