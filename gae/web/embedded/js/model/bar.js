function Bar(data) {
    for (var key in data) {
        this[key] = data[key]
    }
}

Bar.hasCounterService = function(bar ) {
    return bar.counter_service_to_stay || bar.counter_service_to_go;
};

Bar.hasToStayAndToGo = function(bar) {
    return bar.counter_service_to_stay && bar.counter_service_to_go;
};

Bar.getServiceMethodsCount = function(bar) {
    var count = 0;
    if (this.hasCounterService(bar)) {
        if (this.hasToStayAndToGo(bar)) {
            count += 2;
        }
        else {
            count++;
        }
    }

    if (bar.table_service) {
        count++;
    }

    if (bar.delivery_service) {
        count++;
    }

    return count;
};

Bar.getBar = function() {
    var rawData = localStorage.bar;
    if (typeof rawData !== 'undefined') {
        var data = JSON.parse(localStorage.bar);
        return new Bar(data);
    } else {
        return null;
    }
};

Bar.setBar = function(data) {
    localStorage.bar = JSON.stringify(data);
};

Bar.remove = function() {
    localStorage.removeItem('bar');
};