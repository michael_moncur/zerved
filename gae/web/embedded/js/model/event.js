function ZEEvent(data) {
    for (var key in data) {
        this[key] = data[key]
    }
}

ZEEvent.getEvent = function() {
    var rawData = localStorage.event;
    if (rawData) {
        var data = JSON.parse(rawData);
        return new ZEEvent(data);
    } else {
        return null;
    }
};

ZEEvent.setEvent = function(data) {
    localStorage.event = JSON.stringify(data);
};

ZEEvent.remove = function() {
    localStorage.removeItem('event');
};

ZEEvent.handleEventSummary = function(element) {
    console.log(element);
};