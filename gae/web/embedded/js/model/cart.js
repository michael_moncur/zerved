//----------//
// Cart     //
//----------//
function Cart(location, bar, group, event) {
    this.cartId = Cart.getCartIdForLocation(location, bar, group, event);
    var anonymousCart = null;
    var allCarts = null;
    var cartsData = localStorage.carts;
    if (typeof cartsData !== 'undefined') {
        allCarts = JSON.parse(cartsData);
        if (typeof allCarts[this.cartId] !== 'undefined') {
            anonymousCart = allCarts[this.cartId];
        }
    }
    if (anonymousCart) {
        for (var key in anonymousCart) {
            if (key === 'items') {
                var items = [];
                var anonymousItems = anonymousCart[key];
                anonymousItems.forEach(function(anonymousItem) {
                    items.push(new CartItem(anonymousItem));
                });
                this.items = items;
            } else {
                this[key] = anonymousCart[key];
            }
        }
    } else {
        this.location = location;
        this.bar = bar;
        this.group = group;
        this.event = event;
        this.clear();

        if (allCarts === null) {
            allCarts = {};
        }
        allCarts[this.cartId] = this;
        localStorage.carts = JSON.stringify(allCarts);
    }
}

Cart.prototype.save = function() {
    var allCarts = {};
    var cartsData = localStorage.carts;
    if (typeof cartsData !== 'undefined') {
        allCarts = JSON.parse(cartsData);
    }
    allCarts[this.cartId] = this;
    localStorage.carts = JSON.stringify(allCarts);
};

Cart.prototype.setLocation = function(location) {
    this.location = location;
    if (this.group === 'entrance') {
        this.service = 'entrance'
    }
};

Cart.prototype.setBar = function(bar) {
    this.bar = bar;
    // Preselect a service type if only one is available
    if (bar && bar.hasCounterService() && !bar.tableService && !bar.hasToStayAndToGo() && !bar.deliveryService) {
        this.service = 'counter';
        if (bar.counter_service_to_go) {
            this.toGo = true;
        } else if (bar.counter_service_to_stay) {
            this.toGo = false;
        }
    }
    if (bar && !bar.hasCounterService() && bar.tableService && !bar.deliveryService) {
        this.service = 'table';
    }
};

Cart.prototype.count = function() {
    var items = 0;
    this.items.forEach(function(item) {
        items += item.quantity;
    });
    return items;
};

Cart.prototype.countRows = function() {
    return this.items.length;
};


Cart.prototype.clear = function() {
    this.items = [];
    this.service = '';
    this.tableNumber = '';
    this.tip_percent = 0;
    this.card = null;
    this.pincode = null;
    this.nordpayRegistration = null;
    this.nordpayAuthorization = null;
    this.nordpayMethod = null;
    this.obscuredCardNumber = null;
    this.quote = null;
    this.termsAccepted = ConsumerManager.getConsumerManager().termsAcceptedRecently();
    this.deliveryName = '';
    this.deliveryPhone = '';
    this.deliveryAddress1 = '';
    this.deliveryAddress2 = '';
    this.deliveryCity = '';
    this.deliveryPostcode = '';
    this.deliveryInstructions = '';
    this.loyaltyCode = null;
};

Cart.prototype.addItem = function(item) {
    this.addItemAtIndex(item, this.items.length);
};

Cart.prototype.addItemAtIndex = function(item, index) {
    var existingIndex = -1;
    this.items.forEach(function(existingItem, i) {
        if (existingItem.isEqual(item)) {
            existingIndex = i;
        }
    });
    if (existingIndex == -1) {
        this.items.splice(index, 0, item);
    } else {
        this.items[existingIndex].addQuantity(item.quantity);
    }
};

Cart.prototype.removeItem = function(item) {
    var index = this.items.indexOf(item);
    this.items.splice(index, 1);

    // Since local storage does not reset like App memory on application termination,
    // do this to allow the cart to be cleared.
    if (this.items.length == 0) {
        this.clear();
    }
};

Cart.prototype.updateItem = function(item, index) {
    // Remove and insert again to avoid two rows with same options
    this.items.splice(index, 1);
    this.addItemAtIndex(item, index);
};

Cart.prototype.attachQuote = function(quote) {
    this.quote = quote;
    // Update location/bar objects
    if (quote.location) {
        this.location = quote.location;
    }
    if (quote.bar) {
        this.bar = quote.bar;
    }
    // Attach quote items to cart items
    for (var i = 0; i < quote.items.length; i++) {
        if (this.items.length > i) {
            this.items[i].quoteItem = quote.items[i];
        }
    }
    // Update service
    if (this.service === '') {
        if (this.group === 'entrance') {
            this.service = 'entrance';
        } else if (this.bar && Bar.hasCounterService(this.bar) && !this.bar.table_service && !Bar.hasToStayAndToGo(this.bar) && !this.bar.delivery_service) {
            this.service = 'counter';
            if (this.bar.counter_service_to_go) {
                this.toGo = true;
            } else if (this.bar.counter_service_to_stay) {
                this.toGo = false;
            }
        } else if (this.bar && this.bar.table_service && !Bar.hasCounterService(this.bar) && !this.bar.delivery_service) {
            this.service = 'table';
        }
    } else if (this.service === 'counter' && this.bar && !Bar.hasCounterService(this.bar)) {
        this.service = '';
    } else if (this.service === 'table' && this.bar && !this.bar.table_service) {
        this.service = '';
    } else if (this.service === 'delivery' && this.bar && !this.bar.delivery_service) {
        this.service = '';
    }
    // Loyalty code
    if (this.loyaltyCode == null && quote.loyalty_code !== '') {
        this.loyaltyCode = quote.loyalty_code;
    }
    this.save();
};

Cart.prototype.validateQuoteItems = function() {
    var invalidItems = [];
    var validItems = [];
    this.items.forEach(function(item) {
        if (!item.quoteItem.valid) {
            invalidItems.push(item);
        } else {
            validItems.push(item);
        }
    });
    this.items = validItems;
    return invalidItems.length;
};

Cart.prototype.estimatedWaitingTime = function() {
    if (this.event) {
        return 0; // No waiting time for event orders
    }
    if ((this.service === 'counter' || this.service === 'table') && this.bar.estimated_waiting_time > 0) {
        return this.bar.estimated_waiting_time;
    }
    if (this.service === 'delivery' && this.bar.estimated_waiting_time_delivery > 0) {
        return this.bar.estimated_waiting_time_delivery;
    }
    return 0;
};


Cart.prototype.itemsForProduct = function(productKey) {
    var result = [];
    this.items.forEach(function(item) {
        if (item.productKey === productKey) {
            result.push(item);
        }
    });
    return result;
};

Cart.prototype.jsonObject = function() {
    var data = {
        'group': this.group,
        'service': this.service,
        'table_number': this.tableNumber,
        'termsAccepted': this.termsAccepted,
        'to_go': typeof this.toGo !== 'undefined' ? this.toGo : false
    };
    if (this.group === 'menu') {
        data.bar = Bar.getBar().key;
    } else {
        data.location = ZELocation.getLocation().key;
    }
    var items = [];
    this.items.forEach(function(item) {
        items.push(item.jsonObject());
    });
    data.items = items;

    return data;
};

Cart.prototype.serialize = function() {
    var data = this.jsonObject();
    if (this.location) data.location = this.location.key;
    if (this.bar) data.bar = this.bar.key;
    data.group = this.group;
    data.service = this.service;
    if (this.tableNumber != null) data.table_number = this.tableNumber;
    data.to_go = this.toGo;
    data.terms_accepted = this.termsAccepted;
    data.tip_percent = this.tip_percent;
    if (this.event) data.event = this.event.key;
    data.delivery_name = this.deliveryName;
    data.delivery_address1 = this.deliveryAddress1;
    data.delivery_address2 = this.deliveryAddress2;
    data.delivery_postcode = this.deliveryPostcode;
    data.delivery_city = this.deliveryCity;
    data.delivery_phone = this.deliveryPhone;
    data.delivery_instructions = this.deliveryInstructions;
    if (this.loyaltyCode !== null) data.loyalty_code = this.loyaltyCode;
    data.card = this.card;
    data.pincode = this.pincode;
    data.obscured_card_number = this.obscuredCardNumber;
    data.nordpay_authorization = this.nordpayAuthorization;
    data.nordpay_registration = this.nordpayRegistration;
    data.nordpay_method = this.nordpayMethod;
    console.log(data);
    return JSON.stringify(data);
};

Cart.getCartForLocation = function(location, bar, group, event) {
    return new Cart(location, bar, group, event);
};

Cart.getCartIdForLocation = function(location, bar, group, event) {
     if (group === 'menu') {
        if (event != null) {
            return bar.key + '_' + group + '_' + event.key;
        } else {
            return bar.key + '_' + group;
        }
    } else {
        return location.key + '_' + group;
    }
};


//----------//
// CartItem //
//----------//
function CartItem(anonymousCartItem) {
    for (var key in anonymousCartItem) {
        if (key === 'options') {
            var options = [];
            var anonymousOptions= anonymousCartItem[key];
            anonymousOptions.forEach(function(anonymousOption) {
                options.push(new CartItemOptionValue(anonymousOption));
            });
            this.options = options;
        } else {
            this[key] = anonymousCartItem[key];
        }
    }
}

CartItem.createNew = function(productKey, quantity, options) {
    return new CartItem({
        'productKey': productKey,
        'quantity': quantity,
        'options': options
    });
};

CartItem.prototype.addQuantity = function(quantity) {
    this.quantity += quantity;
};

CartItem.prototype.isEqual = function(anObject) {
    if (anObject === this) {
        return true;
    } else if (typeof anObject === 'undefined' || !anObject || typeof anObject.options === 'undefined' || typeof anObject.productKey === 'undefined') {
        return false;
    }
    if (this.options.length != anObject.options.length) {
        return false;
    }
    for (var i = 0; i < this.options.length; i++) {
        if (!(this.options[i].isEqual(anObject.options[i]))) {
            return false;
        }
    }
    return this.productKey === anObject.productKey;
};

CartItem.prototype.jsonObject = function() {
    var data = {
        'product': this.productKey,
        'qty': this.quantity
    };
    var options = [];
    this.options.forEach(function(optionValue) {
        options.push(optionValue.jsonObject())
    });
    data.options = options;
    return data;
};


CartItem.prototype.hasValueForOptionIdAndSelectedIndex = function(selectedIndex, optionId) {
    for (var i = 0; i < this.options.length; i++) {
        var optionValue = this.options[i];
        if (optionValue.optionId === optionId && optionValue.selectedIndex === selectedIndex) {
            return true;
        }
    }
    return false;
};

CartItem.prototype.hasValueForOptionId = function(optionId) {
    for (var i = 0; i < this.options.length; i++) {
        var optionValue = this.options[i];
        if (optionValue.optionId === optionId) {
            return true;
        }
    }
    return false;
};


CartItem.prototype.clearOptionId = function(optionId) {
    this.options = this.options.filter(function(optionValue) {
        return optionValue.optionId !== optionId;
    });
};


CartItem.prototype.removeOption = function(optionId, selectedIndex) {
    this.options = this.options.filter(function(optionValue) {
        return !(optionValue.optionId === optionId && optionValue.selectedIndex == selectedIndex);
    });
};


//---------------------//
// CartItemOptionValue //
//---------------------//
function CartItemOptionValue(anonymousCartItemOptionValue) {
    for (var key in anonymousCartItemOptionValue) {
        this[key] = anonymousCartItemOptionValue[key];
    }
}

CartItemOptionValue.createNew = function(optionId, selectedIndex) {
    return new CartItemOptionValue({
        'optionId': optionId,
        'selectedIndex': selectedIndex
    });
};

CartItemOptionValue.prototype.jsonObject = function() {
    return {
        'option_id': this.optionId,
        'option_value': this.selectedIndex
    };
};

CartItemOptionValue.prototype.isEqual = function(anObject) {
    if (anObject === this) {
        return true;
    } else if (typeof anObject === 'undefined' || !anObject || typeof anObject.optionId === 'undefined' || typeof anObject.selectedIndex === 'undefined') {
        return false;
    }
    return this.optionId === anObject.optionId && this.selectedIndex === anObject.selectedIndex;
};
