function app(action, params) {
    var iframe = document.createElement("IFRAME");
    iframe.setAttribute("src", "js-frame:"+action+":0:"+encodeURIComponent(JSON.stringify(params)));
    document.documentElement.appendChild(iframe);
    iframe.parentNode.removeChild(iframe);
    iframe = null;
    return false;
}

$(document).ready(function() {
    // Fold out product data in product list
    $(".menu .menuitem .product_header").click(function() {
        $(this).next().slideToggle(200);
    });
    
    // Products
    $(".menu li").each(function (i) {
        var li = $(this);
        
        // Quantity control
        li.find('.plus, .minus').click(function() {
            var add = ($(this).hasClass('plus')) ? 1 : -1;
            var qtyinput = li.find('input[name="qty"]');
            var newQty = parseInt(qtyinput.val())+add;
            if (newQty < 1) {
                return;
            }
            qtyinput.val(newQty);
            li.find('.number').html('<p>'+newQty+'</p>');
            return false;
        });
        
        // Add to cart
        /*li.find('.btn').click(function() {
            var formdata = li.find('form').serialize();
            app('addToCart', {'addData': formdata});
        });*/
    });
    
    // Cart form
    $('.cartform').submit(function() {
        return validateOptions($(this));
    });
    
    // Cart lines
    $(".checkout_lines .open_orderline").click(function() {
        $(this).parents('li').toggleClass('open');
    });
    
    // Cart quantity controls
    $(".checkout_lines li").each(function (i) {
        var li = $(this);
        li.find('.plus, .minus').click(function() {
            var add = ($(this).hasClass('plus')) ? 1 : -1;
            var qtyinput = li.find('input[name="qty"]');
            var newQty = parseInt(qtyinput.val())+add;
            if (newQty < 1) {
                return;
            }
            qtyinput.val(newQty);
            li.find('.number').html('<p>'+newQty+'</p>');
            updateItem($(this).closest('form'));
            return false;
        });
    });
    
    // Cart service selection
    $("input[type=radio].select-service").click(function() {
        var service = $(this).val();
        // Show table number if table service is selected
        $("input[name=table_number]").toggle((service == "table"));
        $("input[name=service]").val(service);
    });
});

function validateOptions(container) {
    var allOptionsValid = true;
    container.find('.product-option').each(function () {
        var optionValid = false;
        if ($(this).hasClass('required')) {
            if ($(this).find('select').length) {
                optionValid = ($(this).find('select').val() != '');
            } else if ($(this).find(':checkbox').length) {
                optionValid = ($(this).find('input:checkbox:checked').length > 0);
            }
        } else {
            optionValid = true;
        }
        if (!optionValid) {
            allOptionsValid = false;
        }
        $(this).find('.option-failed-validation').toggle(!optionValid);
    });
    return allOptionsValid;
}

function updateItem(itemForm) {
    expandedItems = new Array();
    $('li.checkout_line').each(function (index) {
        if ($(this).hasClass('open')) {
            expandedItems.push(index);
        }
    });
    $('input[name=expanded_items]').val(expandedItems.join());
    itemForm.submit();
}

$.fn.serializeObject = function()
{
    var o = {};
    var a = this.serializeArray();
    $.each(a, function() {
        if (o[this.name] !== undefined) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};