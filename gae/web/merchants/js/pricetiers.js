function add_price_tier(currency, mtv, fee ){          
    return $('<tr><td><div class="input input-prepend"><span class="add-on">' +  currency + '</span><input name="min_values[]" class="span2" type="text" value="'+mtv+'" pattern="\\d{1,3}(\\,?\\d{3})*(\\.\\d+)?" required /></div></td><td><div class="input input-prepend"><span class="add-on">%</span><input name="fees[]" class="span1" type="text" value="'+fee+'" pattern="\\d{1,3}(\\,?\\d{3})*(\\.\\d+)?" required/></div></td><td><button type="button" class="btn btn-small pull-right" onclick="$(this).closest(\'tr\').remove();"><i class="icon-minus-sign"></i></button></td></tr>');
}

function add_price_tier_flat_fee(currency, mtv, flat_fee ){
    return $('<tr><td><div class="input input-prepend"><span class="add-on">' +  currency + '</span><input name="flat_fee_min_values[]" class="span2" type="text" value="'+mtv+'" pattern="\\d{1,3}(\\,?\\d{3})*(\\.\\d+)?" required /></div></td><td><div class="input input-prepend"><span class="add-on">' +  currency + '</span><input name="flat_fees[]" class="span1" type="text" value="'+flat_fee+'" pattern="\\d{1,3}(\\,?\\d{3})*(\\.\\d+)?" required/></div></td><td><button type="button" class="btn btn-small pull-right" onclick="$(this).closest(\'tr\').remove();"><i class="icon-minus-sign"></i></button></td></tr>');
}

function add_discount(tr, discount){
    return $('<tr><td><input name="discount_min_values[]" class="span2" type="text" value="'+tr+'" pattern="\\d{1,3}(\\,?\\d{3})*(\\.\\d+)?" required/></td><td><div class="input input-prepend"><span class="add-on">%</span><input name="discount_percentages[]" class="span1" type="text" value="'+ discount +'" pattern="\\d{1,3}(\\,?\\d{3})*(\\.\\d+)?" required/></div></td><td><button type="button" class="btn btn-small pull-right" onclick="$(this).closest(\'tr\').remove();"><i class="icon-minus-sign"></i></button></td></tr>');
}

function show_price_tier(currency, mtv, fee){
 	return $("<tr><td>From <strong>"+ mtv +"</strong> "+ currency +"</td><td><strong>" + fee + "</strong> %</td></tr>");
}

function show_price_tier_flat_fee(currency, mtv, flat_fee) {
 	return $("<tr><td>From <strong>"+ mtv +"</strong> "+ currency +"</td><td><strong>" + flat_fee + "</strong> " + currency + "</td></tr>");
}

function show_price_tier_fixed_fee(currency, text, fixed_fee) {
	var s = (fixed_fee > 0) ? " + <strong>" +fixed_fee+"</strong> " + currency : "None";
 	return $("<tr><td>" + text + "</td><td>" + s + "</td></tr>");
}

function show_discount(tr, discount){            
    return $("<tr><td>From <strong>" + tr + "</strong></td><td><strong>" + discount + "</strong> %</td></tr>");
}



