var fullRefreshInterval = 60; // Seconds between full refresh of order lists
var fullPageRefreshInterval = 60*60; // Seconds between page refresh
var pingInterval = 20;
var inactiveNotificationDelay = 60;

var numberOfMissedPingsBeforeReconnect = 4;
var numberOfMissedPingsBeforePull = 3;

var lastUpdate; // Timestamp for last pull
var xhrPull; // AJAX object for full refresh pulls
var lastPing = new Date();
var lastUserInteraction = new Date();

var lastBigPull = null;
var lastSmallPull = new Date().getTime()/1000.0;

var expandedOrders = [];
var selectedOrders = [];

var groupedProducts = []; // for product summary

var soundPlayed = [];

var channel;

var status; // open, paused, closed

var isiPad;

// Call method in iOS app
function app(action, params) {
    var iframe = document.createElement("IFRAME");
    iframe.setAttribute("src", "js-frame:"+action+":0:"+encodeURIComponent(JSON.stringify(params)));
    document.documentElement.appendChild(iframe);
    iframe.parentNode.removeChild(iframe);
    iframe = null;
    return false;
}

$(document).ready(function() {
    // Init templates for dynamic content insertion using javascript
    $.template('order-template', $("#order-template"));
    $.template('order-item-template', $("#order-item-template"));
    $.template('empty-table-template', $("#empty-table-template"));
    $.template('order-complete-template', $("#order-complete-template"));
    $.template('empty-pending-open-template', $("#empty-pending-open-template"));
    $.template('empty-pending-closed-template', $("#empty-pending-closed-template"));
    $.template('waitingtime-description-template', $("#waitingtime-description-template"));
    $.template('product-summary-row-template', $("#product-summary-row-template"));
    // Connect buttons
    $('.btn-confirm-reject').click(orderClick);
    $('.btn-confirm-complete').click(orderClick);
    $('.btn-cancel-complete').click(orderClick);
    $('#order_warning').click(notificationClick);
    $('#btn-open').click(startClick);
    $('#btn-closed').click(stopClick);
    $('#btn-save-paused-msg').click(savePausedMsgClick);
    $('#btn-save-waitingtime').click(saveWaitingTimeClick);
    $('.btn-select-all').click(selectBtnClick);
    $('.btn-deselect-all').click(selectBtnClick);
    $('#btn-prepare-selected').click(multipleOrdersActionClick);
    $('#btn-cancel-selected').click(multipleOrdersActionClick);
    $('#btn-complete-selected').click(multipleOrdersActionClick);
    $('#btn-confirm-complete-selected').click(multipleOrdersActionClick);
    $("tr.order-row.incomplete").live('touchend click', selectRowClick);

    isiPad = DEVICE == "ipad";
    $('#btn-confirm-quit-keep-open-computer').toggle(!isiPad).click(quitKeepOpenConfirmClick);
    $('#btn-confirm-quit-stop-computer').toggle(!isiPad).click(quitStopConfirmClick);
    $('#btn-confirm-quit-keep-open-ipad').toggle(isiPad).click(quitKeepOpenConfirmClick);
    $('#btn-confirm-quit-stop-ipad').toggle(isiPad).click(quitStopConfirmClick);
    $('#btn-quit').click(quitClick);
    // Ping to check connection
    setInterval("ping()", pingInterval*1000);
    // Full order list refresh
    setInterval("pull()", fullRefreshInterval*1000);
    // Refresh the whole page to recreate connection
    setTimeout("reloadWithoutGuard()", fullPageRefreshInterval*1000);
    
    // set default counters and enable realtime buttons
    $("#n-rl-pending").show();
    $("#n-rl-preparing").show();
    $("#btn-group-status").show();

    //pull();
	var event_key = window.location.hash;
	if (event_key)
		event_key = event_key.slice(1);
	set_pull_filter_key(event_key);

    // Connect to channel for instant order update messages
    connect();
});

function reloadWithoutGuard() {
    window.onbeforeunload = null;
    window.location.reload();
}

function handleMessageReceivedFromChannelOk(message) {
    console.log('handleMessageReceivedFromChannelOk: ' + message);
        // Message JSON data received
        data = $.parseJSON(message);

    if (data['remove'] || data['remove_orders'] || data['insert']) {
        // Michael and Christian have reported some strange errors after the switch to firebase
        // Do a big pull for now.
        doBigPull();
    }
    else {
        refreshInterface(data);
    }

    $(".fa-spin").hide();
    lastSmallPull = new Date().getTime() / 1000.0;
}

function handleMessageReceivedFromChannelError(error) {
        console.log(error);
        // Reload page when device token expires (default two hours) or server is down
        if (error.code == 401 || error.code == 0) {
            reloadWithoutGuard();
            return;
        }
        if (error.description) {
            alert(error.description);
        }
}


function connect() {
/*    console.log('Connecting ' + new Date());
    channel = new goog.appengine.Channel(CHANNEL_TOKEN);
    socket = channel.open();
    socket.onopen = function() {
        // console.log('Socket opened');
        pull();
    };
    socket.onmessage = function(message) {
        handleMessageReceivedFromChannelOk(message.data);
    };
    socket.onerror = function(error) {
        handleMessageReceivedFromChannelError(error);
    };
    socket.onclose = function() {
        // Reload page if socket is closed
        reloadWithoutGuard();
    };*/
}

// set the order pull filter
function set_pull_filter(li) {
    var event_key = li.attr('data-key');
	set_pull_filter_key(event_key);
}

function set_pull_filter_key(event_key) {
	if (!event_key)
		event_key = 'realtime';
	else
		window.location.hash = event_key;

	var li = $('#filter-pull-orders [data-key='+event_key+']');
    /*if ($(this).hasClass("active"))
        return;*/

    $("#filter-pull-orders li").each(function(){ 
        $(this).removeClass("active");
    });

    li.addClass("active");

    var max_chars = 20;
    var filter_name = li.find("a").html();
    filter_name = ((filter_name.length > max_chars) ? (filter_name.substr(0,max_chars - 3) + "...") : filter_name);

    $("#filter-pull-orders-description").html(filter_name);
    $("#filter-pull-orders-description").data("data-key", event_key);

    if (event_key == "realtime") {
        $("#n-po-pending").hide();
        $("#n-po-preparing").hide();
        $("#n-rl-pending").show();
        $("#n-rl-preparing").show();
        $("#btn-group-status").show();
    } else {
        $("#n-rl-pending").hide();
        $("#n-rl-preparing").hide();
        $("#btn-group-status").hide();
        $("#n-po-pending").show();
        $("#n-po-preparing").show();
    }
    //invalidate pull
    lastBigPull = null;
    // get new orders
    pull();
}

function doBigPull() {
    lastBigPull = null;
    pull();
}

// Pull complete refresh
function pull() {


    // pull is disabled by default
    var do_pull = false; 
    var now = new Date();

    $("#load_layer_text_loading").show();

    // conditions for enabeling pull
    if(!lastBigPull)
        // If we didn't do a pull yet or have changed filters
        do_pull = true;

    else if ((now - lastPing)/1000 > numberOfMissedPingsBeforePull*pingInterval){
        // If we haven't head from the server for a while do the full pull 

      $("#load_layer_text_lost_connection").show();
        do_pull = true;
    }

    // Don't do a big pull now
    if (!do_pull)
        return;

	$("#load_layer").show();
	$(".fa-spin").hide();

    var event_key = $("#filter-pull-orders-description").data("data-key");
    xhrPull = $.getJSON(
            '/merchants/serve/'+BAR_ID+'/pull/event/'+event_key, 
            refreshInterface
            );

    // update timestamp
    xhrPull.done(function(){lastBigPull = new Date();});

    // write fail message
    xhrPull.fail(function(){
	    $("#load_layer").hide();
	    $(".fa-spin").hide();
	    $("#error500_layer").show();
    })
    
}

// Ping server (server will send a small ping channel msg back), seems to help keeping the connection alive
function ping() {
    console.log('pinging server ' + CHANNEL_CLIENT_ID);
    updatePingStatus();
    var data = {'client_id': CHANNEL_CLIENT_ID };
    $.post('/merchants/serve/ping', data);
}

function updatePingStatus() {
    var now = new Date();

    if (lastPing) {
        var diff = (now-lastPing)/1000;
        if (diff > numberOfMissedPingsBeforeReconnect*pingInterval) {
            //$("#lastmessagedate").html(lastPing.toLocaleTimeString() + " Connection may be lost");
            console.log(lastPing.toLocaleTimeString() + " Connection may be lost");
            reloadWithoutGuard();
        } else if (diff > 2*pingInterval) {
            //$("#lastmessagedate").html(lastPing.toLocaleTimeString() + " Connection unstable");
            console.log(lastPing.toLocaleTimeString() + " Connection unstable");
        } else {
            //$("#lastmessagedate").html(lastPing.toLocaleTimeString() + ' Connection OK');
            console.log(lastPing.toLocaleTimeString() + ' Connection OK');
        }
    }
}

// returns the current event/realtime orderfilter
function get_current_filter(){
    var current_filter = null;
    $("#filter-pull-orders li").each(function(){ 
        if ($(this).hasClass("active")) {
            // get filter name, see template
            current_filter = $(this).attr('data-name')
        }});
    return current_filter;
}

// matches an order againsts the order filter
function order_filter(filter_name, order) {

    // don't mix realtime with pre-orders
    if (filter_name == "realtime"){
        if (order['event_name'] == '')
            return true;
        else
            return false;
    }

    var event_name = order['event_name']+'_'+order['event_time_label']+'_'+order['delivery_date'];
    if (event_name != filter_name) {
        return false;
    }

    return true;
}


// Update order lists with received data
function refreshInterface(data) {

    console.log(data);
    var now = new Date();
    // Ping response
    if (data['ping']) {
        lastPing = now;
        updatePingStatus();
    }
    
    // Refresh command
    if (data['reload']) {
        reloadWithoutGuard();
    }
    
    // Start/stop button
    if ('status' in data) {
        status = data['status'];
        refreshStatus();
        if (status == 'open') {
            window.onbeforeunload = confirmOnPageExit;
        } else {
            window.onbeforeunload = null;
        }
    }
    
    // Pause msg
    if ('message_when_paused' in data) {
        $("#input-paused-msg").val(data['message_when_paused']);
    }
    
    // Estimated waiting time
    if ('estimated_waiting_time' in data && 'estimated_waiting_time_delivery' in data) {
        refreshWaitingTime(data['estimated_waiting_time'], data['estimated_waiting_time_delivery']);
    }

    // Remove these orders
    if (data['remove']) {

        if(data['remove'][1] == 'cancelled' && isiPad){
            console.log('Order #' + data['remove'][2] + ' cancelled');
            app('printCancelledOrder', { queue_number : data['remove'][2] } );   
        }
        $.each(data['remove'], function (key, orderkey) {
            $('#'+orderkey).attr('id', orderkey+'out');
            $('#'+orderkey+'out').fadeOut(function() { $(this).remove(); refreshEmptyRows(); updateStatusBar();});
        });
    }
    
    // Remove these orders
    if (data['remove_orders']) {
        $.each(data['remove_orders'], function (
                    key,
                    order_data
                    ) {
       
            var order_key = order_data[0];
            var order_status = order_data[1];
            var order_qno = order_data[2];
            if(order_status == 'cancelled' && isiPad){
                console.log('Order #' + order_qno + ' cancelled');
                app('printCancelledOrder', { queue_number : order_qno } );   
            }

            $('#'+order_key).attr('id', order_key+'out');
            // elias: remove animation for performance
			/*$('#'+order_key+'out').fadeOut(function() { 
                $(this).remove(); 
                refreshEmptyRows();
                updateStatusBar();
                });*/
            $('#'+order_key+'out').remove()
			refreshEmptyRows();
			updateStatusBar();
        });
    }

    // get current pull filter
    var current_filter = get_current_filter();

	var suppressSound;
    // Insert these orders
    if (data['insert']) {
        $.each(data['insert'], function (key, order) {

            // don't allow mixing order from different events / realtime
            if (!order_filter(current_filter, order))
                return; //emulate continue in jquery

            suppressSound = addRow(order, suppressSound, true);
            var tableId = 'table-' + order['status'];
            sortRows($('#'+tableId+' tbody'));
			// elias: remove animation for performance
            //$("#"+order['key']).fadeIn();
            //$("#"+order['key']).show();
        });
        refreshEmptyRows();
        updateStatusBar();
        getProductSummary();
    }

    var big_pull = false;
    // Refresh lists completely
    if (data['refresh']) {
        $("table.serve-orders tbody").empty();
        $.each(data['refresh'], function (key, order) {

            // don't allow mixing order from different events / realtime
            if (!order_filter(current_filter, order))
                return; //emulate continue in jquery

            suppressSound = addRow(order, suppressSound);
        });
        $("table.serve-orders tbody").each(function() { sortRows($(this)); });
        $("table.serve-orders tbody tr").show();
        refreshEmptyRows();
        getProductSummary();
        updateTableButtons($("table.serve-orders")); 
        big_pull = true;
    }
    // Show received error
    if (data['error']) {
        alert(data['error'])
    }
    // Update clock
    if (data['datetime']) {
        $("#datetime").html(data['datetime']);
    }
    // Remember timestamp for last update
    if (data['timestamp']) {
        lastUpdate = data['timestamp'];
    }
    
    // Show notification overlay if there are pending orders to deliver now and time since last user interaction is long
    var pendingOrders = $('#table-pending tbody tr').not('.no-orders-row').filter(function () { return $(this).attr('data-age') > 0; });
    var showWarning = (pendingOrders.length > 0 && (now-lastUserInteraction)/1000 > inactiveNotificationDelay);
    $("#order_warning").toggle(showWarning);
    playWarningSound(showWarning);

    getProductSummary();
    updateStatusBar();

    //
    // we need the loading layer to work for the big data imports
    if(big_pull){ 
	    $("#load_layer").hide();
	    $(".fa-spin").hide();
    }

}

function getPreOrderId(event_name, event_time_label, date){
    return (event_name.replace(" ", "_") +"_"+event_time_label.replace(" ", "_") + "_" + date.replace(/[^0-9]+/g, '')).toLowerCase();
}

// Insert order row into table
function addRow(order, suppressSound, showImmediately) {

    if (isiPad 
            && (order['status'] == 'preparing' || order['status'] == 'pending')
            ) {
        // Inform ipad app
        app('orderDidArrive', { 'order' : order } );
    }

    // Generate HTML table row from template
    var row;
    if (order['status'] == 'complete') {
        row = $.tmpl("order-complete-template", order);
    } else {
        row = $.tmpl("order-template", order);
		if (!suppressSound) {
	        playOrderSound(row);
			suppressSound = true;
		}
    }
    row.data("order-data", order);
    var tableId = 'table-' + order['status'];
    var tfoot = $('#'+tableId +' tfoot');

    // Pre-order
    if(order['event_name'] 
            && order['event_name'].length > 0 
            && order['event_time_label'] 
            && order['event_time_label'].length > 0) {

                var preorder_id = getPreOrderId(
                        order['event_name'],
                        order['event_time_label'],
                        order['delivery_date']),

                    preorder_label_short = order['event_name'] +
                        " ("+order['event_time_label'] + ")";

                row.data({type: "preorder", id : preorder_id});
                row.find(".event-name").html(preorder_label_short);
            }
    //Realtime order
    else {
        row.data({type : "realtime", id : "realtime"});      
    }

	if (showImmediately)
		row.show();
    row.appendTo($('#'+tableId+' tbody'));
    //updateStatusBar();

    $(".btn", row).click(orderClick);
    $("a.btn-reject", row).click(orderClick);
    $("a.btn-cancel", row).click(orderClick);
    if(isiPad){
        $(".btn-group.computer", row).remove();
        $(".btn-print", row).click(function(){ orderPrintClick(order); });
    }
    else { 
        $(".btn-group.ipad", row).remove();
        $(".btn-print", row).remove();
    }
    $('.btn-complete-withpin', row).click(orderClick);
    $('.address-badge', row).click(orderClick);
    $('.address-badge', row).data('address', order['delivery_address_html']);
    
    tfoot.show();

    setRowExpanded(row, $.inArray(order['key'], expandedOrders) != -1);
    setRowSelected(row, $.inArray(order['key'], selectedOrders) != -1);
	return suppressSound;
}


function sortRows(tbody) {
    var rowData = [];
    // Collect ages from rows
    $('tr', tbody).each(function(key, row) {
        rowData.push([$(row).attr('data-age'), $(row)]);
    });
    // Sort by age
    rowData.sort(function(a, b) { return b[0]-a[0] });
    // Reverse direction for completed orders
    if (tbody.closest('table').attr('id') == 'table-complete') {
        rowData.reverse();
    }
    // Re-append rows to table in sorted order
    $.each(rowData, function (i, row) {
        row[1].appendTo(tbody);
    });
}

// If there are no order rows, show placeholder rows instead
function refreshEmptyRows()
{
    $("table.serve-orders tbody").each(function (i, tbody) {
        var hasPlaceholder = ($('tr.no-orders-row', tbody).length > 0);
        var table = $(tbody).parent('table');
        if ($('tr', tbody).length == 0) {
            var tableId = table.attr('id');
            var emptyRow;
            if (tableId == 'table-pending') {
                if (status == 'open') {
                    emptyRow = $.tmpl("empty-pending-open-template");
                } else {
                    emptyRow = $.tmpl("empty-pending-closed-template", {});
                }
            } else {
                emptyRow = $.tmpl("empty-table-template", {msg: emptyTableMessages[tableId]});
            }
            emptyRow.appendTo(tbody);
            table.find("tfoot").hide();
        } else if ($('tr', tbody).length > 1 && hasPlaceholder) {
            $("tr.no-orders-row", tbody).remove(); 
        }
    });
}

// When an order button is clicked
function orderClick() {
    lastUserInteraction = new Date();
    if($(this).hasClass("btn-cancel-complete")) {
        updateTableButtons($("#table-preparing"));
        return;
    }
    if ($(this).hasClass('btn-reject')) {
        $("#confirmReject .btn-confirm-reject").attr('data-order', $(this).attr('data-order'));
        $("#confirmReject").modal();
        return;
    }
    if ($(this).hasClass('btn-more')) {
        setRowExpanded($(this).closest('tr'), true);
        return;
    }
    if ($(this).hasClass('btn-less')) {
        setRowExpanded($(this).closest('tr'), false);
        return;
    }
    if ($(this).hasClass('btn-confirm-complete')) {
        if($(this).attr("id") == "btn-confirm-complete-selected") {
            multipleOrdersActionClick($(this));
            return;    
        }
        // Complete order with PIN
        $("#staffpin-error").hide();
        $("#confirmComplete").modal('hide');
        var confirmCompleteButton = $(this);
        var data = {'client_id': CHANNEL_CLIENT_ID, 'order_key': $(this).attr('data-order'), 'status': 'complete', 'pin': $("#staffpin").val()};
        var jqxhr = $.post('/merchants/serve/process', data)
        .fail(function() { $("#staffpin-error").show(); $("#staffpin").val(''); $("#confirmComplete").modal(); }); // Pin error, show modal again
        return;
    }
    if ($(this).hasClass('address-badge')) {
        $("#address-summary").html($(this).data('address'));
        $("#address-info").modal();
        return;
    }
    var status;
    if ($(this).hasClass('btn-startpreparing')) {
        status = 'preparing';
        $(this).attr('disabled', 'disabled');
    } else if ($(this).hasClass('btn-complete')) {
        status = 'complete';
        $(this).attr('disabled', 'disabled');
    } else if ($(this).hasClass('btn-cancel')) {
        status = 'pending';
        $(this).attr('disabled', 'disabled');
    } else if ($(this).hasClass('btn-confirm-reject')) {
        status = 'cancelled';
        $("#confirmReject").modal('hide');
    } else if ($(this).hasClass('btn-complete-withpin')) {
        $("#confirmComplete .btn-confirm-complete").attr('data-order', $(this).attr('data-order'));
        $("#confirmComplete .btn-confirm-complete").attr("id", "");
        $("#staffpin-error").hide();
        $("#staffpin").val('');
        $("#confirmComplete").modal();
        return;
    }
    if (status) {
        var data = {
            'client_id': CHANNEL_CLIENT_ID,
            'order_key': $(this).attr('data-order'),
            'status': status
        };
        $.post('/merchants/serve/process', data);
    }
}

function orderPrintClick(order) {
    lastUserInteraction = new Date();
    app('printOrder', { 'order' : order } );
}

function notificationClick() {
    lastUserInteraction = new Date();
    $("#order_warning").hide();
}

function selectRowClick() {
    lastUserInteraction = new Date();
    setRowSelected($(this), !$(this).hasClass("selected"));
    updateTableButtons($(this).parents().eq(1)); 
}

function selectBtnClick() {
    var btn = $(this);
    lastUserInteraction = new Date();
    var table = btn.parents().eq(4);
    var rows = table.find("tbody").children("tr");
    rows.each(function(){
        if($(this).css("display") != "none") {
            setRowSelected($(this), btn.hasClass("btn-select-all")); 
        }
    });
	updateTableButtons(table); 
}

// Batch actions (process multiple selected orders)
function multipleOrdersActionClick(e) {
    lastUserInteraction = new Date();
    
    $(this).prop("disabled", true);
    $(this).children("i.fa-spin").show();
    $(this).parents().eq(1).find("button.btn-deselect-all").prop("disabled", true);

    var status;
    if($(this).attr("id") == "btn-cancel-selected")
        status = "pending";

    else if($(this).attr("id") == "btn-prepare-selected")
        status = "preparing";

    else if($(this).attr("id") == "btn-complete-selected"){
        status = 'complete';
        if($(this).parents().eq(3).find("a.btn-complete-withpin").length > 0) {
            $("#staffpin-error").hide();
            $("#staffpin").val('');
            $("#confirmComplete").modal();
            $("#confirmComplete .btn-confirm-complete").attr(
                    "id", 
                    "btn-confirm-complete-selected");
            return;
        }
    }

    var confirmWithPin = false;
    if (e.length > 0 
            && typeof e.attr('id') !== 'undefined' 
            && e.attr('id') == "btn-confirm-complete-selected") {
       confirmWithPin = true;
       status = 'complete';
    }

    var btn = confirmWithPin ? e : $(this);

    if(status) {
        selectedOrdersInTable = [];
        btn.parents().eq(3).find('tbody tr').each(function(){
            var orderKey = $(this).attr("id");
            var index = $.inArray(orderKey, selectedOrders);
            if(index >= 0) {
                selectedOrdersInTable.push(orderKey);
                if(status == 'complete') {
                    $(this).removeClass("selected");
                    selectedOrders.splice(index, 1);
                }
            }
        });

        if(confirmWithPin) {
            // Complete orders with PIN
            $("#staffpin-error").hide();
            $("#confirmComplete").modal('hide');
            var data = {
                'client_id': CHANNEL_CLIENT_ID,
                'order_keys': selectedOrdersInTable,
                'status': 'complete',
                'pin': $("#staffpin").val()
            };
            var jqxhr = $.post('/merchants/serve/process', data)
            .fail(function() {  // Pin error, show modal again
                $("#staffpin-error").show(); 
                $("#staffpin").val(''); 
                $("#confirmComplete").modal(); 
                for(var i=0; i < selectedOrdersInTable.length; i++) {
                    selectedOrders.push(selectedOrdersInTable[i]);
                    if(!$("#" + selectedOrdersInTable[i]).hasClass("selected"))
                        $("#" + selectedOrdersInTable[i]).addClass("selected");
                }
            });
            return;
        }

        var data = {
            'client_id': CHANNEL_CLIENT_ID,
            'order_keys': selectedOrdersInTable,
            'status': status
        };

        
        $.post('/merchants/serve/process', data);
    }
}

function refreshStatus() {
    // Update start/pause/stop buttons
    $("#btn-group-status button").removeClass('active');
    $("#btn-"+status).addClass('active');
    $("#current-status").html(STATUS_LABELS[status]);
    $("#current-status").attr('class', STATUS_CLASSES[status]);
    // Force refresh of the empty msg in the pending table
    $("#table-pending tbody tr.no-orders-row").remove();
    refreshEmptyRows();
}

// type : 'all', 'realtime' or 'preorder'
function getNumberOfOrders(type, status){
    var n = 0;
    $("#table-" + status + " tbody tr.order-row").each(function(){
        if(type == 'all' || $(this).data("type") == type)
            n++;
    });
    return n;
}

function getNumberOfEvents() {
    var n = 0;
    var ids = [];
    $("#table-pending tbody tr.order-row").each(function(){
        if($(this).data("type") == 'preorder' && $(this).data("id") != 'preorder'){
            var i = $.inArray($(this).data("id"), ids);
            if(i == -1){
                ids.push($(this).data("id"));
                n++;
            }
        }
    });
    return n;
}

function updateStatusBar(){
   $("#n-rl-pending").html("(" + getNumberOfOrders('realtime', 'pending') + ")");
   $("#n-po-pending").html("(" + getNumberOfOrders('preorder', 'pending') + ")");
   $("#n-rl-preparing").html("(" + getNumberOfOrders('realtime', 'preparing') + ")");
   $("#n-po-preparing").html("(" + getNumberOfOrders('preorder', 'preparing') + ")");

   $("#n-events").html(getNumberOfEvents());
   $("#status-po").css("display", getNumberOfEvents() == 0 ? 'none':'inline');
}

function startClick() {
    // Start service
    var data = {'client_id': CHANNEL_CLIENT_ID};
    $.post('/merchants/serve/'+BAR_ID+'/settings', {'status': 'open', 'persist_status': false});
}

function stopClick() {
    stopService();
}

function stopService() {
    var data = {'client_id': CHANNEL_CLIENT_ID};
    $.post('/merchants/serve/'+BAR_ID+'/settings', {'status': 'closed', 'persist_status': false});
}

function savePausedMsgClick() {
    // Save msg and pause service
    $.post('/merchants/serve/'+BAR_ID+'/settings', {'status': 'paused', 'persist_status': false, 'message_when_paused': $("#input-paused-msg").val()});
    $("#edit-paused-msg").modal('hide');
}

function confirmOnPageExit(e) {
    e = e || window.event;

    // NB: Custom messages are no longer supported in new browsers.
    var message = 'Please close the bar or use the admin button to keep it open.';

    // For IE6-8 and Firefox prior to version 4
    if (e) {
        e.returnValue = message;
    }

    return message;
}

function refreshWaitingTime(min, min_delivery) {
    // Update button and dropdown
    $("#btn-waitingtime").removeClass("disabled");
    $("#waitingtime-description").html($.tmpl("waitingtime-description-template", {'min': min, 'min_delivery': min_delivery}));
    //$("#waitingtime-options li").removeClass("active");
    //$("#waitingtime-options .waitingtime-option-"+min).addClass("active");
    $("#input-custom-waitingtime").val(min > 0 ? min : "");
    $("#input-custom-waitingtime-delivery").val(min_delivery > 0 ? min_delivery : "");
}

function saveWaitingTime(min, min_delivery) {
    if (min == "") {
        min = "0";
    }
    if (min_delivery == "") {
        min_delivery = "0";
    }
    // Submit minutes to server
    $("#btn-waitingtime").addClass("disabled");
    $.post('/merchants/serve/'+BAR_ID+'/settings', {'estimated_waiting_time': min, 'estimated_waiting_time_delivery': min_delivery}, function(data){
        pull();
    });
}

function saveWaitingTimeClick() {
    if (document.getElementById('input-custom-waitingtime').validity.valid === true && document.getElementById('input-custom-waitingtime-delivery').validity.valid === true) {
        saveWaitingTime($('#input-custom-waitingtime').val(), $('#input-custom-waitingtime-delivery').val());
        $('#custom-waitingtime').modal('hide');
    } else {
        return false;
    }
}

function quitClick() {
    if (status == 'open') {
        $("#confirm-quit").modal();
    } else {
        navigateBackToAdmin();
    }
}

function quitStopConfirmClick() {
    stopService();
    window.onbeforeunload = null;
    return true;
}

function quitKeepOpenConfirmClick() {
    var data = {'client_id': CHANNEL_CLIENT_ID};
    $.post('/merchants/serve/'+BAR_ID+'/settings', {'persist_status': true})
        .done(function( data ) {
            if (data == 'INVALID_SCHEDULE') {
                $('#noValidSchedulesErrorMessage').show();
            } else if (data == '') {
                navigateBackToAdmin();
            } else {
                $('#unknownErrorMessage').show();
            }
        })
        .fail(function() {
            $('#unknownErrorMessage').show();
        });
    return false;
}

function navigateBackToAdmin() {
    window.onbeforeunload = null;
    if (isiPad) {
        window.location.href=$("#btn-confirm-quit-stop-ipad").attr('href');
    } else {
        window.location.href=$("#btn-confirm-quit-stop-computer").attr('href');
    }
}

function setRowExpanded(row, isExpanded) {
    var orderKey = row.attr('id');
    var orderExpandedIndex = $.inArray(orderKey, expandedOrders);
    // Remember order is expanded
    if (isExpanded && orderExpandedIndex == -1) {
        expandedOrders.push(orderKey);
    }
    // Remove order from list of expanded orders
    if (!isExpanded && orderExpandedIndex != -1) {
        expandedOrders.splice(orderExpandedIndex, 1);
    }
    // Show the full order data if expanded, or the summmary data if not expanded
    $(".summary", row).toggle(!isExpanded);
    $(".full", row).toggle(isExpanded);
}

function setRowSelected(row, isSelected) {
    var orderKey = row.attr('id');
    var orderSelectedIndex = $.inArray(orderKey, selectedOrders);
    // Remember order is selected
    if (isSelected && orderSelectedIndex == -1) {
        selectedOrders.push(orderKey);
    }
    // Remove order from list of selected orders
    if (!isSelected && orderSelectedIndex != -1) {
        selectedOrders.splice(orderSelectedIndex, 1);
    }
    // Change row style
    if(isSelected){
        row.addClass("selected");
        row.removeClass("important");        
        row.removeClass("very-important");        
    }
    else {
        row.removeClass("selected");
        if(row.parents().eq(1).attr("id") == "table-pending") {
            if(parseInt(row.attr("data-age")) > 300)
                row.addClass("very-important");
            else if(parseInt(row.attr("data-age")) > 180)
                row.addClass("important");
        }
    }
}

function updateTableButtons(table) {

  // Update select/action buttons
    selectedOrdersInTable = [];
    var rowsInTable = table.children("tbody").find('tr'); 
    rowsInTable.each(function(){
        var key = $(this).attr("id");
        if($.inArray(key, selectedOrders) >= 0)
            selectedOrdersInTable.push(key);
    });

    var numberOfOrdersInTable = rowsInTable.length;
    var numberOfSelectedOrdersInTable = selectedOrdersInTable.length;
    var thead = table.find("thead");
    var selectAllBtn = thead.find("button.btn-select-all"); 
    var deselectAllBtn = thead.find("button.btn-deselect-all"); 
    var allBtns = thead.find("button"); 

    allBtns.prop("disabled", numberOfSelectedOrdersInTable == 0);
    selectAllBtn.prop("disabled", numberOfOrdersInTable == numberOfSelectedOrdersInTable); 
    deselectAllBtn.prop("disabled", numberOfSelectedOrdersInTable == 0);

    //table.find("thead button.pull-right").prop("disabled", !isBulkAllowed());
    table.find("thead p.text-error").css("display", selectedOrders.length == 0);
} 

function isBulkAllowed() {
    if(selectedOrders.length == 0)
        return false;
    // If the user has selected a mix of preorders and realtime orders, it is most likely a mistake, so we don't allow it
    var order_type = null;
    var event_id = null;
    for(var i = 0; i < selectedOrders.length; i++){
        var row = $("#" + selectedOrders[i]);

        var row_type = row.data('type');
        var row_event_id = row.data('id');

        // don't know what to do in this situation, FIXME
        if (! (row_type == "preorder" || row_type == "realtime") )
            continue;

        if(order_type == null) {
            order_type = row_type;
            event_id = row_event_id;
            continue;
        }

        // dont allow mixing pre order and realtime in multiple selections
        if(row_type != order_type) {
            return false;
        }

        // dont allow mixing of orders from different events in multiple
        // selection
        if(event_id != row_event_id){
            return false
        }

    }
    return true;
}

function selectionContainsOrderOfType(type) {
    for(var i = 0; i < selectedOrders.length; i++){
        var row = $("#" + selectedOrders[i]);
        if(row.data('type') == type)
            return true;
    }
    return false;
}

function selectionContainsOrderFromOtherEvent(event_id) {
    for(var i = 0; i < selectedOrders.length; i++){
        var row = $("#" + selectedOrders[i]);
        if(row.data('id') != event_id)
            return true;
    }
    return false;
}

function getProductSummary() {
    groupedProducts = [];
    // Collect items from all visible order rows, grouped by product
    var domelement = $("table tbody tr.order-row:visible");
    if (domelement !== undefined && 'length' in domelement) {
        domelement.each(function(){
            order_data = $(this).data("order-data");
            order_data_num_items = 0;
            if (order_data['items'] !== undefined && 'length' in order_data['items']) {
                order_data_num_items = order_data['items'].length;
            }

            for(var i = 0; i < order_data_num_items; i++) {
                if(order_data['items'][i]['product_key'] != ''){ // Tips have no product key
                    insert(groupedProducts, {
                        key : order_data['items'][i]['product_key'],
                        name : order_data['items'][i]['name'],
                        qty_pending : order_data['status'] == "pending" ? order_data['items'][i]['quantity'] : 0,
                        qty_preparing : order_data['status'] == "preparing" ? order_data['items'][i]['quantity'] : 0,
                        qty_complete : order_data['status'] == "complete" ? order_data['items'][i]['quantity'] : 0,
                        options : order_data['items'][i]['options']
                    });
                }
            }
        });
    }
    groupedProducts.sort(sortProductByNames);
    displaySummary();
}

/* Insert a new product or update quantities of exsiting one */
function insert(products, product) {
    var p=-1;
    for(var i=0; i<products.length; i++) {
        if(products[i]['key'] == product['key'] && haveSameOptions(products[i], product)){
            p=i;
            break;
        }
    }
    if(p > -1){
        products[p]['qty_pending'] += product['qty_pending'];
        products[p]['qty_preparing'] += product['qty_preparing'];
        products[p]['qty_complete'] += product['qty_complete'];
    }
    else products.push(product);
}

/* Returns true if products are the same (product key and options)*/
function haveSameOptions(product1, product2) {
    product1haslength = product1['options'] !== undefined && 'length' in product1['options'];
    product2haslength = product2['options'] !== undefined && 'length' in product2['options'];

    if (!product1haslength && !product2haslength) {
        return true;
    }

    if (!product1haslength || !product2haslength) {
        return false;
    }

    if(product1['options'].length != product2['options'].length)
        return false;

    if(product1['options'].length == 0)
        return true;

    for(var i=0; i < product1['options'].length; i++){
        found = false;
        for(var j=0; j < product2['options'].length; j++){
            if(product1['options'][i]['option_id'] == product2['options'][j]['option_id']) {
                if(arraysEqual(product1['options'][i]['values'], product2['options'][j]['values'])){
                    found=true;
                    break;
                }
            }
        }   
        if(!found)
            return false;
    }
    return true;
}

function arraysEqual(a, b) {
    if (a === b) return true;
    if (a == null || b == null) return false;
    if (a.length != b.length) return false;

    a.sort();
    b.sort();

    for (var i = 0; i < a.length; ++i) {
        if (a[i] !== b[i]) return false;
    }
    return true;
}

/* Sort products by names before displaying the table */
function sortProductByNames(a,b) {
    if (a.name < b.name)
        return -1;
    if (a.name > b.name)
        return 1;
    return 0;
}

/* Appends rows to table based on the products array */
function displaySummary() {
    $("#table-product-summary tbody").html("");
    for(var i = 0; i < groupedProducts.length; i++) {
        row = $.tmpl("product-summary-row-template", groupedProducts[i]);
        $("#table-product-summary tbody").append(row);
    }
}

function playOrderSound(row) {
    var orderKey = row.attr('id');
    var orderIndex = $.inArray(orderKey, soundPlayed);
    if (orderIndex == -1) {
        var audio = document.getElementById('order-sound');
        if(audio) {
            soundPlayed.push(orderKey);
            if (lastUpdate !== undefined) { // don't play sound on first launch
                if(!isiPad)
                    audio.play();
                else app('playOrderSound', {});
            }
        }
    }
}
function playWarningSound(bool) {        
    var audio = document.getElementById('warning-sound');
    if(audio) {
        if (bool) {
            if(!isiPad)
                audio.play();
            else app('playSplashSound', {});
        } else {
            audio.pause();
        }
    }
}
