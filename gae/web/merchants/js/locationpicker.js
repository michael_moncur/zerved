(function( $ ) {
    $.fn.locationPicker = function(options) {
        var defaults = {
            labels: {
                search: 'Search',
                drag_me: 'Drag me'
            }
        };
        var settings = $.extend(true, {}, defaults, options);
        var locationfield = settings.locationfield;
        var mapbutton = this;
        var initLat = 55.4;
        var initLng = 10.4;
        var geocoder = new google.maps.Geocoder();
        var map;
        var marker;
        
        // Show map when button clicked
        mapbutton.click(initMap);
        
        if(locationfield.val() != ""){
            // Geocode position
            var posArr = locationfield.val().split(",");
            if(posArr.length == 2){
                initLat = $.trim(posArr[0]);
                initLng = $.trim(posArr[1]);
            }
            var latlng = new google.maps.LatLng(initLat, initLng);
            geocoder.geocode({'latLng': latlng}, function(results, status) {
              if (status == google.maps.GeocoderStatus.OK) {
                  // Multiple results are returned with various accuracy - use the locality (city) version
                  $.each(results, function(index, result) {
                      if (result.types[0] == 'locality') {
                          mapbutton.find(".location-label").html(result.formatted_address);
                          return false;
                      }
                  });
              } else {
                // Geocoder failed
              }
            });
        } else {
            // No location - show map
            initMap();
        }
        
        function initMap() {
            // Hide map button
            mapbutton.hide();
            
            // Create map
            var myLatlng = new google.maps.LatLng(initLat, initLng);
            var myOptions = {
                zoom: 15,
                center: myLatlng,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                mapTypeControl: true,
                disableDoubleClickZoom: true,
                streetViewControl: false
            }
            mapbutton.after('<div class="input-append"><input type="text" id="map-search" placeholder="'+settings.labels.search+'" style="width: 500px"/><button type="button" id="map-search-btn" class="btn" style="width: 86px">'+settings.labels.search+'</button></div><div id="map_canvas" style="width: 600px; height: 400px;"></div>');
            map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
            marker = new google.maps.Marker({
                position: myLatlng, 
                map: map, 
                title: settings.labels.drag_me,
                draggable: true
            });
            
            // Search field
            $("#map-search").keypress(function (event) {
                if (event.which == 13) {
                    findAddress($(this).val());
                    event.stopPropagation();
                    return false;
                }
            });
            
            // Search btn
            $("#map-search-btn").click(function() {
                findAddress($("#map-search").val());
            });
            
            // Map callbacks
            google.maps.event.addListener(map, 'dblclick', function(event) {
                setPosition(event.latLng);
            });
            google.maps.event.addListener(marker, 'dragend', function(event) {
                setPosition(marker.position);
            });
        }
 
        function findAddress(address){
            if(address == ""){
                alert("Please enter an address or Lng/Lat position.");
            }else{
                if(isLngLat(address)){
                    setPositionFromSearchField();
                    map.setCenter(marker.position);
                }else{
                    geocoder.geocode( {'address': address}, function(results, status) {
                        if (status == google.maps.GeocoderStatus.OK) {
                            setPosition(
                                results[0].geometry.location,
                                results[0].geometry.viewport
                            );
                            map.setCenter(marker.position);
                        } else {
                            alert("Geocode was not successful for the following reason: " + status);
                        }
                    });
                }
            }
        }
        
        function isLngLat(val){
            var lngLatArr = val.split(",");
            if(lngLatArr.length == 2){
                if(isNaN(lngLatArr[0]) || isNaN(lngLatArr[1])){
                    return false;
                }else{
                    return true;
                }
            }
            return false;
        }
        
        function RoundDecimal(num, decimals){
            var mag = Math.pow(10, decimals);
            return Math.round(num * mag)/mag;
        }
        
        function setPosition(latLng, viewport){
            var lat = RoundDecimal(latLng.lat(), 6);
            var lng = RoundDecimal(latLng.lng(), 6);
            marker.setPosition(latLng);
            if(viewport){
                map.fitBounds(viewport);
                map.setZoom(map.getZoom() + 2);
            }else{
                map.panTo(latLng);
            }
            locationfield.val(lat + "," + lng);
        }
        
        function setPositionFromSearchField() {
            var posStr = $("#map-search").val();
            if(posStr != ""){
                var posArr = posStr.split(",");
                if(posArr.length == 2){
                    var lat = $.trim(posArr[0]);
                    var lng = $.trim(posArr[1]);
                    var latlng = new google.maps.LatLng(lat, lng);
                    setPosition(latlng);
                }
            }
        }

    };
})( jQuery );
