(function( $ ) {
    $.fn.productOptions = function(options) {
        var option_index = 0;
        var option_container = this;
        var option_form_field;
        var form;
        
        $.template('options-template', options.optionTemplate);
        $.template('option-value-template', options.optionValueTemplate);
        option_forms_field = options.optionFormsField;
        option_index = option_forms_field.val();
        form = options.form;
        
        options.addButton.click(addEmptyOption);
        $("button.delete-option", option_container).click(function(event) { event.data = {'optionIndex': $(this).attr('data-option-index')}; deleteOption(event); });
        $("tbody", option_container).sortable({ handle: ".sort-option-value"});
        
        form.submit(validateAndPrepareOptionValues);
        
        function addEmptyOption() {
            var option = $.tmpl("options-template", {'option_index':option_index});
            option.appendTo(option_container);
            $("button.delete-option", option).click({'optionIndex': option_index}, deleteOption);
            $("input.option-name").last().focus();
            $("#option-"+option_index+"-values").sortable({ handle: ".sort-option-value"});
            option_index++;
            option_forms_field.val(option_index);
        }
        
        /*function addOption(data) {
            $.extend(data, {'option_index':option_index})
            var option = $.tmpl("options-template", data);
            option.appendTo(option_container);
            $("button.delete-option", option).click({'optionIndex': option_index}, deleteOption);
            option_index++;
            option_forms_field.val(option_index);
        }*/
        
        function deleteOption(event) {
            var idx = event.data.optionIndex;
            $("#option-"+idx+"-deleted").val('1');
            $("#option-"+idx).fadeOut();
        }
        
        function validateAndPrepareOptionValues(event) {
            var valid = true;
            // For each option
            for (var i=0; i<option_index; i++) {
                if ($("#option-"+i+"-deleted").val() == '1') {
                    continue;
                }
                // Collect labels
                var labelsArray = $("input.option-"+i+"-value-label").map(function() { return $(this).val(); }).get();
                // Collect prices
                var pricesArray = $("input.option-"+i+"-value-price").map(function() {
                    var val = $(this).val();
                    if (val == '') {
                        val = 0;
                    }
                    val = parseFloat(val);
                    if (isNaN(val)) {
                        return 0;
                    }
                    return val;
                }).get();
                // Validate
                var j=labelsArray.length;
                while (j--) {
                    // Remove element if label is empty
                    if (labelsArray[j] == "") {
                        labelsArray.splice(j, 1);
                        pricesArray.splice(j, 1);
                    }
                }
                // Collect as strings
                $("#option-"+i+"-value_labels").val(labelsArray.join("\n"));
                $("#option-"+i+"-value_prices").val(pricesArray.join("\n"));
            }
            return valid;
        }
    };
})( jQuery );
