#!/usr/bin/env python
import webapp2
import sys
import os
import re
sys.path.append('app')
sys.path.append('lib')
from webapp2_extras import routes

# debug = os.environ.get('SERVER_SOFTWARE', '').startswith('Development') or os.environ['CURRENT_VERSION_ID'].split(".")[0] == 'dev'
debug = os.environ.get('SERVER_SOFTWARE', '').startswith('Development') or re.split('\.|-dot-', os.environ['CURRENT_VERSION_ID'])[0] == 'dev'

app = webapp2.WSGIApplication([
    # Match subdomain or no subdomain, but not sub-subdomains
    # routes.DomainRoute(r'<:([^.]+\.)|([^.]+-dot-)?><:zervedapp\.com|dev\.zervedapp\.appspot.com|dev-dot-zervedapp\.appspot.com|demo\.zervedapp\.appspot.com|zervedapp\.appspot\.com|10\.0\.0\.82|zerved\.dev|new\.zervedapp\.appspot\.com>', [
    routes.DomainRoute(r'<:([^.])+(\.|-dot-)?><:zervedapp\.com|dev(\.|-dot-)zervedapp\.appspot.com|demo(\.|-dot-)zervedapp\.appspot.com|zervedapp\.appspot\.com|zerved\.dev|new(\.|-dot-)zervedapp\.appspot\.com>', [
        webapp2.Route('/image/<entity_key:[^/]+>/<width:\d+>/<height:\d+>', 'app.views.core.ImageHandler', name='image'),
        webapp2.Route('/embedded/merchant/<merchant_id:[^/]+>', 'app.views.embedded.main.EmbeddedHomeHandler', name='embedded-home'),
        webapp2.Route('/embedded/tab/<tab:[^/]+>', 'app.views.embedded.main.EmbeddedTabHandler', name='embedded-tab'),
        webapp2.Route('/embedded/locations/<location_key:[^/]+>', 'app.views.embedded.main.EmbeddedLocationHandler'),
        webapp2.Route('/embedded/locations/<location_key:[^/]+>/bars', 'app.views.embedded.main.EmbeddedBarsHandler'),
        webapp2.Route('/embedded/locations/<location_key:[^/]+>/categories/<category_key:[^/]+>/products', 'app.views.embedded.main.ProductListHandler'),
        webapp2.Route('/embedded/locations/<location_key:[^/]+>/events', 'app.views.embedded.main.EventListHandler'),
        webapp2.Route('/embedded/bars/<bar_key:[^/]+>/menus', 'app.views.embedded.main.EmbeddedBarMenusHandler'),
        webapp2.Route('/embedded/bars/<bar_key:[^/]+>/menus/<menu_key:[^/]+>/categories', 'app.views.embedded.main.EmbeddedMenuCategoryListHandler'),
        webapp2.Route('/embedded/bars/<bar_key:[^/]+>/menus/<menu_key:[^/]+>/categories/<category_key:[^/]+>/products', 'app.views.embedded.main.ProductListHandler'),
        webapp2.Route('/embedded/products/<product_key:[^/]+>', 'app.views.embedded.main.ProductHandler'),
        webapp2.Route('/embedded/groups/<group:[^/]+>/bars/<bar_key:[^/]+>/events/<event_key:[^/]+>/cart', 'app.views.embedded.main.CartHandler'),
        webapp2.Route('/embedded/groups/<group:[^/]+>/bars/<bar_key:[^/]+>/cart', 'app.views.embedded.main.CartHandler'),
        webapp2.Route('/embedded/groups/<group:[^/]+>/locations/<location_key:[^/]+>/cart', 'app.views.embedded.main.CartHandler'),
        webapp2.Route('/embedded/epay', 'app.views.embedded.main.EPayHandler'),
        webapp2.Route('/embedded/mobilepaynativeembedded', 'app.views.embedded.main.MobilePayNativeEmbeddedHandler'),
        webapp2.Route('/embedded/epaysavecard', 'app.views.embedded.main.EPaySaveCardHandler'),
        webapp2.Route('/embedded/epay/accepted', 'app.views.embedded.main.EPayAcceptedHandler'),
        webapp2.Route('/embedded/epay/cancelled', 'app.views.embedded.main.EPayCancelledHandler'),
        webapp2.Route('/embedded/epaysavecard/cancelled', 'app.views.embedded.main.EPaySaveCardCancelledHandler'),
        webapp2.Route('/embedded/orders/<order_key_or_id:[^/]+>', 'app.views.embedded.main.EmbeddedOrderHandler'),
        ])
    ],
    debug=debug, config={'webapp2_extras.i18n': {'default_timezone': 'Europe/Copenhagen'}})
