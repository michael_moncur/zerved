#!/usr/bin/env python
import webapp2
import sys
import os
sys.path.append('app')
sys.path.append('lib')
from webapp2_extras import routes

debug = os.environ.get('SERVER_SOFTWARE', '').startswith('Development') or os.environ['CURRENT_VERSION_ID'].split(".")[0] == 'dev'

import app.views.core
import app.views.front.consumers
import app.views.front.facebookcontest
import app.views.front.main
#import app.views.embedded.main


app = webapp2.WSGIApplication([
    # Match subdomain or no subdomain, but not sub-subdomains
    routes.DomainRoute(r'<:([^.])+(\.|-dot-)?><:zervedapp\.com|dev(\.|-dot-)zervedapp\.appspot.com|demo(\.|-dot-)zervedapp\.appspot.com|zervedapp\.appspot\.com|zerved\.dev|new(\.|-dot-)zervedapp\.appspot\.com>', [
        webapp2.Route('/resetpassword/<token:[^/]+>', handler=app.views.front.consumers.ResetPasswordHandler, name='reset-password'),
        webapp2.Route('/contactform', handler=app.views.front.main.ContactFormHandler, name='contactform'),
        webapp2.Route('/signupform', handler=app.views.front.main.SignUpFormHandler, name='signupform'),
        webapp2.Route('/robots.txt', handler=app.views.front.main.RobotsTxtHandler, name='robots.txt'),
        webapp2.Route('/fb-contest-april', handler=app.views.front.facebookcontest.FacebookContestHandler, name='fb-contest-april'),
        webapp2.Route('/fb-contest-april/shared', handler=app.views.front.facebookcontest.FacebookContestSharedHandler, name='fb-contest-april-shared'),
        webapp2.Route('/fb-contest-april/drawrandom', handler=app.views.front.facebookcontest.FacebookContestDrawRandomHandler),
        webapp2.Route('/recommendform', handler=app.views.front.main.RecommendFormHandler, name='recommendform'),
        webapp2.Route('/recommend/sent', handler=app.views.front.main.RecommendSentHandler, name='recommendsent'),
        webapp2.Route('/news', handler=app.views.front.main.NewsListHandler, name='news-list'),
        webapp2.Route('/news/image/<news_id:\d+>', handler=app.views.front.main.NewsImageViewerHandler, name='imageviewer'),
        webapp2.Route('/news/view/<slug:.*>', handler=app.views.front.main.NewsViewHandler, name='news-view'),
        webapp2.Route('/image/<entity_key:[^/]+>/<width:\d+>/<height:\d+>', handler=app.views.core.ImageHandler, name='image'),

        webapp2.Route('/<template:.*>', handler=app.views.front.main.MainHandler, name='main'),
        ])
    ],
    debug=debug, config={'webapp2_extras.i18n': {'default_timezone': 'Europe/Copenhagen'}})