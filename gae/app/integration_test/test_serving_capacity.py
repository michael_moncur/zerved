import os
from sys import getsizeof
import unittest
import datetime, pytz
import logging
import json
import resource

import webtest
import webapp2
import mock

from google.appengine.api import memcache
from google.appengine.api import urlfetch_stub
from google.appengine.ext import db, ndb
from google.appengine.ext import testbed

from models.merchants import Merchant, Location, Bar
from models.consumers import Consumer
from models.sales import Order, OrderItem
from models.events import Event, EventSchedule
from models.catalog import Product, PriceTier

from decimal import Decimal

NOW = datetime.datetime.now() - datetime.timedelta(days=1)

MERCHANT = "Test merchant"
LOCATION = "Test location"
BAR = "Test bar"
EVENT = "Test event"
CONSUMER = "Test consumer"
PRODUCT = "Test beer"
ORDER_ITEM = "Test item"
NUMBER_OF_ORDERS = 10

class ServingCapacityTestEvent(unittest.TestCase):
    def setUp(self):

        os.environ['USER_EMAIL'] = "info@gmail.com"
        os.environ['USER_IS_ADMIN'] = '1'

        logging.debug("Setting up servings capacity test")
        # First, create an instance of the Testbed class.
        self.testbed = testbed.Testbed()
        # Then activate the testbed, which prepares the service stubs for use.
        self.testbed.activate()
        # Next, declare which service stubs you want to use.
        self.testbed.init_datastore_v3_stub()
        self.testbed.init_memcache_stub()
        self.testbed.init_search_stub()
        self.testbed.init_urlfetch_stub()
        self.testbed.init_user_stub()

        ##self.testbed.init_all_stubs()

        logging.debug("creating objects")
        #
        # Create price tier
        price_tier = PriceTier(currency='DKK', min_value=[100,], fee=[5,] )
        price_tier.put()
       
        # Create a merchant, location and a bar
        merchant = Merchant(
                name=MERCHANT,
                location=db.GeoPt('55.4,10.4'),
                pricetier=price_tier)
        merchant.put()
        
        location = Location(
                merchant=merchant,
                name=LOCATION,
                location=db.GeoPt('55.4,10.4')
                )
        location.put()
        
        self.bar = Bar(name=BAR, merchant=merchant, location=location,)
        self.bar.put()
       
        
        # Create a consumer
        consumer = Consumer(name=CONSUMER)
        consumer.put()
        
       
        # Create and event
        event = EventSchedule(
                name=EVENT,
                merchant=merchant,
                location=location,
                enabled_bars=[self.bar.key(), ],
                first_date=NOW,
                last_date=NOW+datetime.timedelta(days=1),
                date_start=NOW,
                date_delivery=NOW,
                close_offset_minutes=-0,
                times = ['20:00',],
                labels = ['break',]
        
                )
        event.put()

        self.events = [e for e in Event.all().ancestor(event).fetch(10)]
        
        # Create a product  
        product = Product(merchant=merchant,
                name=PRODUCT,
                price=Decimal('30.0'))
        product.put()
       
        # Create 250 orders
        for i in range(NUMBER_OF_ORDERS):

            order = Order(
                    event_name=self.events[0].name,
                    event_time_label = self.events[0].time_label,
                    merchant=merchant,
                    consumer=consumer,
                    location=location,
                    bar=self.bar,
                    service='counter',
                    status='pending',
                    currency='Dkr',
                    order_number=merchant.reserve_order_number(),
                    merchant_name=merchant.name,
                    location_name=location.name,
                    bar_name=self.bar.name,
                    timezone=merchant.timezone,
                    )
            
            order.put()    

            for i in range(4):
                order_item = OrderItem(
                        parent=order,
                        order=order,
                        product=product,
                        name=ORDER_ITEM,
                        quantity=4,
                        base_price_excl_tax=Decimal('300.0'),
                        )
                order_item.set_options([])
                order_item.put()

        logging.debug("importing and staring app")
        from merchants import app
        self.testapp = webtest.TestApp(app)


        

    def tearDown(self):
        self.testbed.deactivate()
        os.environ['USER_EMAIL'] = ""
        os.environ['USER_IS_ADMIN'] = ""

   
    def testServingCapacity(self):
        """
        Tests the serving capacity of an event using above 200 orders.
        """
        rusage = resource.getrusage(resource.RUSAGE_SELF)
        print "system usage %s" % rusage.ru_maxrss


        from google.appengine.api import users
        with mock.patch.object(users.User, 'user_id') as id_mock:
            # We need to patch the default user_id method in order to convince
            # the system that the fake account is a google account.
            id_mock.return_value = "fake id"

            logging.debug("getting merchant servings page")

            servings_pull_url = "/merchants/serve/{barid}/pull/event/{event_key}".format(
                    barid=self.bar.key().id(),
                    event_key=self.events[0].key()
                    )

            response = self.testapp.get(servings_pull_url)
            self.assertEqual(response.status_int, 200)

            json_response = json.loads(response.body)


            self.assertTrue(len(json_response['refresh']) == NUMBER_OF_ORDERS)

            print "string size %s " % getsizeof(response.body)


            #
            # Make som clamp tests of the fist order
            first_order = json_response['refresh'][0]
            baseline_first_order = {
                    u'comment': None, 
                    u'total_loyalty_discount_incl_tax': u'0', 
                    u'delivery_postcode': None, 
                    u'time_created': u'18:39', 
                    u'items_length': 4, 
                    u'delivery_city': None, 
                    u'id': 9, 
                    u'sort_age': 7224, 
                    u'late': 120, 
                    u'time_completed': None, 
                    u'total_quantity': 16, 
                    u'service': u'counter', 
                    u'total_incl_tax': u'Dkr0.00', 
                    u'delivery_address2': None, 
                    u'event_name': u'Test event', 
                    u'loyalty_code': None, 
                    u'delivery_address1': None, 
                    u'table_number': None, 
                    u'event_time_label': None, 
                    u'delivery_address_formatted': 
                    u'', 
                    u'delivery_phone': None, 
                    u'status': u'pending', 
                    u'completed_age': 0, 
                    u'queue_number': u'001', 
                    u'total_loyalty_discount_excl_tax_formatted': u'Dkr0.00', 
                    u'delivery_instructions': None, 
                    u'total_loyalty_discount_incl_tax_formatted': u'Dkr0.00', 
                    u'key': u'agx0ZXN0YmVkLXRlc3RyCwsSBU9yZGVyGAkM', 
                    u'delivery_name': None, 
                    u'bar': u'Test bar', 
                    u'items': [
                        {
                            u'name': u'Test item', 
                            u'total_incl_tax': u'Dkr0.00', 
                            u'product_key': u'agx0ZXN0YmVkLXRlc3RyDQsSB1Byb2R1Y3QYCAw', 
                            u'key': u'agx0ZXN0YmVkLXRlc3RyGgsSBU9yZGVyGAkMCxIJT3JkZXJJdGVtGAoM', 
                            u'quantity': 4, 
                            u'options': [], 
                            u'category_name': u''
                            }
                        ], 
                        u'age': 120,
                        u'delivery_address_html': u'',
                        u'to_go': False,
                        u'order_number': 1,
                        u'delivery_date': u'18:39'}

            # pick out a few keys and test against baseline order  
            test_keys = (
                    'total_quantity',
                    'event_name',
                    'service',
                    'items_length',
                    'status',
                    'queue_number',
                    'order_number'
                    )

            for test_key in test_keys:
                self.assertEqual(
                        first_order[test_key],
                        baseline_first_order[test_key]
                        )

            rusage = resource.getrusage(resource.RUSAGE_SELF)
            print "system usage end,  %s" % rusage.ru_maxrss
            print "User mode time %s" % rusage.ru_utime
            print "System mode time %s" % rusage.ru_stime


        
class ServingCapacityTestLive(unittest.TestCase):
    def setUp(self):

        os.environ['USER_EMAIL'] = "info@gmail.com"
        os.environ['USER_IS_ADMIN'] = '1'

        logging.debug("Setting up servings capacity test")
        # First, create an instance of the Testbed class.
        self.testbed = testbed.Testbed()
        # Then activate the testbed, which prepares the service stubs for use.
        self.testbed.activate()
        # Next, declare which service stubs you want to use.
        self.testbed.init_datastore_v3_stub()
        self.testbed.init_memcache_stub()
        self.testbed.init_search_stub()
        self.testbed.init_urlfetch_stub()
        self.testbed.init_user_stub()

        ##self.testbed.init_all_stubs()

        logging.debug("creating objects")
        #
        # Create price tier
        price_tier = PriceTier(currency='DKK', min_value=[100,], fee=[5,] )
        price_tier.put()
       
        # Create a merchant, location and a bar
        merchant = Merchant(
                name=MERCHANT,
                location=db.GeoPt('55.4,10.4'),
                pricetier=price_tier)
        merchant.put()
        
        location = Location(
                merchant=merchant,
                name=LOCATION,
                location=db.GeoPt('55.4,10.4')
                )
        location.put()
        
        self.bar = Bar(name=BAR, merchant=merchant, location=location,)
        self.bar.put()
       
        
        # Create a consumer
        consumer = Consumer(name=CONSUMER)
        consumer.put()
        
        
        # Create a product  
        product = Product(merchant=merchant,
                name=PRODUCT,
                price=Decimal('30.0'))
        product.put()
       
        # Create 250 orders
        for i in range(NUMBER_OF_ORDERS):

            order = Order(
                    merchant=merchant,
                    consumer=consumer,
                    location=location,
                    bar=self.bar,
                    service='counter',
                    status='pending',
                    currency='Dkr',
                    order_number=merchant.reserve_order_number(),
                    merchant_name=merchant.name,
                    location_name=location.name,
                    bar_name=self.bar.name,
                    timezone=merchant.timezone,
                    )
            
            order.put()    

            for i in range(4):
                order_item = OrderItem(
                        parent=order,
                        order=order,
                        product=product,
                        name=ORDER_ITEM,
                        quantity=4,
                        base_price_excl_tax=Decimal('300.0'),
                        )
                order_item.set_options([])
                order_item.put()

        logging.debug("importing and staring app")
        from merchants import app
        self.testapp = webtest.TestApp(app)


        

    def tearDown(self):
        self.testbed.deactivate()
        os.environ['USER_EMAIL'] = ""
        os.environ['USER_IS_ADMIN'] = ""

   
    def testServingCapacity(self):
        """
        Tests the serving capacity of an event using above 200 orders.
        """
        rusage = resource.getrusage(resource.RUSAGE_SELF)
        print "system usage %s" % rusage.ru_maxrss


        from google.appengine.api import users
        with mock.patch.object(users.User, 'user_id') as id_mock:
            # We need to patch the default user_id method in order to convince
            # the system that the fake account is a google account.
            id_mock.return_value = "fake id"

            logging.debug("getting merchant servings page")

            servings_pull_url = "/merchants/serve/{barid}/pull/event/{event_key}".format(
                    barid=self.bar.key().id(),
                    event_key="realtime"
                    )

            response = self.testapp.get(servings_pull_url)
            self.assertEqual(response.status_int, 200)

            json_response = json.loads(response.body)


            self.assertTrue(len(json_response['refresh']) == NUMBER_OF_ORDERS)

            print "string size %s " % getsizeof(response.body)


            #
            # Make som clamp tests of the fist order
            first_order = json_response['refresh'][0]
            baseline_first_order = {
                    u'comment': None, 
                    u'total_loyalty_discount_incl_tax': u'0', 
                    u'delivery_postcode': None, 
                    u'time_created': u'18:39', 
                    u'items_length': 4, 
                    u'delivery_city': None, 
                    u'id': 9, 
                    u'sort_age': 7224, 
                    u'late': 120, 
                    u'time_completed': None, 
                    u'total_quantity': 16, 
                    u'service': u'counter', 
                    u'total_incl_tax': u'Dkr0.00', 
                    u'delivery_address2': None, 
                    u'loyalty_code': None, 
                    u'delivery_address1': None, 
                    u'table_number': None, 
                    u'delivery_address_formatted': 
                    u'', 
                    u'delivery_phone': None, 
                    u'status': u'pending', 
                    u'completed_age': 0, 
                    u'queue_number': u'001', 
                    u'total_loyalty_discount_excl_tax_formatted': u'Dkr0.00', 
                    u'delivery_instructions': None, 
                    u'total_loyalty_discount_incl_tax_formatted': u'Dkr0.00', 
                    u'key': u'agx0ZXN0YmVkLXRlc3RyCwsSBU9yZGVyGAkM', 
                    u'delivery_name': None, 
                    u'bar': u'Test bar', 
                    u'items': [
                        {
                            u'name': u'Test item', 
                            u'total_incl_tax': u'Dkr0.00', 
                            u'product_key': u'agx0ZXN0YmVkLXRlc3RyDQsSB1Byb2R1Y3QYCAw', 
                            u'key': u'agx0ZXN0YmVkLXRlc3RyGgsSBU9yZGVyGAkMCxIJT3JkZXJJdGVtGAoM', 
                            u'quantity': 4, 
                            u'options': [], 
                            u'category_name': u''
                            }
                        ], 
                        u'age': 120,
                        u'delivery_address_html': u'',
                        u'to_go': False,
                        u'order_number': 1,
                        u'delivery_date': u'18:39'}

            # pick out a few keys and test against baseline order  
            test_keys = (
                    'total_quantity',
                    'service',
                    'items_length',
                    'status',
                    'queue_number',
                    'order_number'
                    )

            for test_key in test_keys:
                self.assertEqual(
                        first_order[test_key],
                        baseline_first_order[test_key]
                        )

            rusage = resource.getrusage(resource.RUSAGE_SELF)
            print "system usage end,  %s" % rusage.ru_maxrss
            print "User mode time %s" % rusage.ru_utime
            print "System mode time %s" % rusage.ru_stime


