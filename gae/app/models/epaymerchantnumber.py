from models.environment import get_environment

class EpayMerchantNumber(object):
    @classmethod
    def epay_merchantnumber(self):
        merchantnumber = {'development': 8009448, 'demo': 8009448, 'production': 1009448}[get_environment()]
        return merchantnumber
