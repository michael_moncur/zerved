from google.appengine.ext import db
from google.appengine.ext.db import polymodel
from google.appengine.api import app_identity
from decimalproperty import *
from decimal import Decimal
from models.epay import EpaySubscriptionClient, EpayPaymentClient
from models.mobilepayappswitch import MobilePayAppSwitchPaymentClient
from models.nordpay import NordpayClient
from models.fourt import FourTClient
from models.i18ndata import convert_currency
from models.environment import get_environment
from models.epaymerchantnumber import EpayMerchantNumber
from models.mobilepayappswitchmerchantnumber import MobilePayAppSwitchMerchantNumber
import urllib
import hashlib

EPAY_API_PASSWORD = "x89nbd13afd085h"

class PaymentException(Exception):
    pass

class PaymentSubscription(polymodel.PolyModel):
    """ A payment subscription is a saved credit card that can be used to make payments """
    
    date_created = db.DateTimeProperty(auto_now_add=True)
    status = db.StringProperty(default='active',choices=['pending','active'])
    password = db.ByteStringProperty(default="") # Stored encrypted
    pincode = db.ByteStringProperty(default="") # Encrypted
    
    def is_current(self):
        sub = self.parent().current_payment_subscription
        if sub:
            return self.key() == sub.key()
        return False
        
    def get_description(self):
        return ''
        
    @property
    def description(self):
        return self.get_description()
        
    def delete(self):
        if self.parent() and self.is_current():
            # Unset from consumer
            consumer = self.parent()
            consumer.current_payment_subscription = None
            consumer.pincode = None
            consumer.put()
        super(PaymentSubscription, self).delete()
        
    def initialize_payment(self, order, amount):
        raise Exception("Cannot initialize payment from abstract PaymentSubscription class")
        
    def set_password(self, password):
        self.password = hashlib.sha1(password.encode('utf-8')).digest()

    def verify_password(self, password):
        digest = hashlib.sha1(password.encode('utf-8')).digest()
        return digest == self.password
        
    def set_pincode(self, pincode):
        self.pincode = hashlib.sha1(pincode).digest()

    def verify_pincode(self, pincode):
        digest = hashlib.sha1(pincode).digest()
        return digest == self.pincode
    
class EpaySubscription(PaymentSubscription):
    """ Saved credit card in ePay """

    subscription_id = db.StringProperty()
    transaction_id = db.StringProperty()
    obscured_card_number = db.StringProperty()
    payment_type = db.StringProperty()
    PAYMENT_TYPE_DANKORT = '1'
    PAYMENT_TYPE_VISA = '3'
    PAYMENT_TYPE_MASTERCARD = '4'
    
    def get_description(self):
        return self.obscured_card_number
        
    def delete(self):
        if self.status == 'active' and self.subscription_id:
            epay = EpaySubscriptionClient()
            epay.deletesubscription(
                merchantnumber=EpayMerchantNumber.epay_merchantnumber(),
                subscriptionid=self.subscription_id,
                pwd=EPAY_API_PASSWORD)
        super(EpaySubscription, self).delete()
        
    def initialize_payment(self, order, amount):
        """ Create a payment with the saved credit card """
        payment = EpayPayment(
            subscription=self,
            order=order, 
            amount=amount, 
            subscription_id=self.subscription_id, 
            status='new')
        return payment
        

    def get_redirect_url(self, merchantnumber, callbackurl, accepturl, cancelurl,
        currency='DKK',
        mobile='2',
        language='0',
        subscriptionname='Zerved',
        paymentcollection='1',
        ownreceipt='1',
        mobilecssurl=None):
        
        #epayorderid = str(self.parent().key().id()) + '-' + str(self.key().id())
        params = [
            ('merchantnumber', merchantnumber),
            ('amount', '0'),
            #('orderid', epayorderid),
            ('currency', currency),
            ('mobile', mobile), # 2 = Force mobile interface
            ('language', language), # 0 = Auto detect
            ('callbackurl', callbackurl),
            ('accepturl', accepturl),
            ('cancelurl', cancelurl),
            ('subscription', '1'),
            ('subscriptionname', subscriptionname),
            ('paymentcollection', paymentcollection), # 1 = Payment cards
            ('ownreceipt', '1')
        ]
        if mobilecssurl:
            params.append(('mobilecssurl', mobilecssurl))
            
        # Add hash string
        paramsstring = ''.join([p[1] for p in params]) + 'b80d9f7t83d45f'
        params.append(('hash', hashlib.md5(paramsstring).hexdigest()))

        # Redirect URL
        epayurl = 'https://ssl.ditonlinebetalingssystem.dk/integration/ewindow/Default.aspx?' + urllib.urlencode(params)
        return epayurl


class NordpaySubscription(PaymentSubscription):
    """ Credit card saved with Nordpay """
    identifier = db.StringProperty()
    obscured_card_number = db.StringProperty()
    method = db.StringProperty(default="CC", choices=["CC", "DC"]) # Credit or debit card
    
    def get_description(self):
        return self.obscured_card_number
        
    def initialize_payment(self, order, amount):
        """ Create a payment with the saved credit card """
        payment = NordpayPayment(
            subscription=self,
            order=order, 
            amount=amount, 
            registration_id=self.identifier, 
            method=self.method,
            status='new')
        return payment
        
    def delete(self):
        if self.status == 'active':
            nordpay = NordpayClient(live=(get_environment()=='production'))
            nordpay.deregister(method=self.method, registration=self.identifier)
        super(NordpaySubscription, self).delete()
        
class Payment(polymodel.PolyModel):
    """
    A payment is a one-time payment for an order, created from a saved credit card
    This base class should be extended by payment methods below.
    Payments are first authorized (amount reserved on card), then captured (amount 
    transferred from card to Zerved). Void is when a payment is cancelled before 
    capture. Refund is when a payment is cancelled after capture (currently not
    supported).
    """
    
    subscription = db.ReferenceProperty(PaymentSubscription)
    order = db.ReferenceProperty(collection_name='order_payments')
    date_created = db.DateTimeProperty(auto_now_add=True)
    amount = DecimalProperty(2, default=Decimal('0.00'))
    status = db.StringProperty(default='new',choices=['new', 'authorized', 'captured', 'void', 'refunded'])
    fee = DecimalProperty(2, default=Decimal('0.00'))
    
    def can_authorize(self):
        return self.status == 'new'
        
    def authorize(self):
        return
        
    def can_void(self):
        return self.status == 'authorized'
        
    def void(self):
        return
        
    def can_capture(self):
        return self.status == 'authorized'
        
    def capture(self):
        return
        
    def can_refund(self):
        return self.status == 'captured'
        
    def refund(self):
        return
    
class EpayPayment(Payment):
    """ Epay payment """
    
    subscription_id = db.StringProperty()
    transaction_id = db.StringProperty()
    is_mobilepay = db.BooleanProperty(default=False)

    # The authorize method is only called when authorizing a payment through a subscription. In case of a new epay
    # card, the authorization is done through the payment window (we get a call from epay to our EpayCallbackHandler
    # when authorize is ok).
    def authorize(self):
        if not self.can_authorize():
            raise Exception("Payment cannot be authorized")
        currency = self.order.currency
        epay = EpaySubscriptionClient()
        result = epay.authorize(
            merchantnumber=EpayMerchantNumber.epay_merchantnumber(),
            subscriptionid=self.subscription_id, 
            orderid=self.order.key().id(), 
            amount=self.amount,
            currency=str(currency),
            instantcapture=0,
            pwd=EPAY_API_PASSWORD)
        if result.get('authorizeResult') == 'true':
            logging.info("epay authorizeResult == true")
            self.transaction_id = result.get('transactionid')
            self.status = 'authorized'
            self.fee = self.calculate_fee(self.amount, currency)
            self.put()
            return True
        else:
            raise Exception("Payment authorization failed with PBS error code %s and ePay error code %s" % (result.get('pbsresponse'), result.get('epayresponse')))
            
    def void(self):
        if not self.can_void():
            raise Exception("Payment cannot be voided")
        epay = EpayPaymentClient()
        result = epay.delete(
            merchantnumber=EpayMerchantNumber.epay_merchantnumber(),
            transactionid=self.transaction_id,
            pwd=EPAY_API_PASSWORD)
        if result.get('deleteResult') == 'true':
            self.status = 'void'
            self.put()
            return True
        else:
            raise Exception("Payment void failed with ePay error code %s" % result.get('epayresponse'))
            
    def capture(self):
        logging.info("epay capture called")
        if not self.can_capture():
            raise Exception("Payment cannot be captured")
        epay = EpayPaymentClient()
        result = epay.capture(
            merchantnumber=EpayMerchantNumber.epay_merchantnumber(),
            transactionid=self.transaction_id,
            amount=self.amount,
            pwd=EPAY_API_PASSWORD)
        if result.get('captureResult') == 'true':
            self.status = 'captured'
            self.put()
            return True
        else:
            raise Exception("Payment capture failed with PBS error code %s and ePay error code %s" % (result.get('pbsresponse'), result.get('epayresponse')))
            
    def calculate_fee(self, amount, currency):
        return Decimal('0') # We are no longer charging payment fees

class MobilePayAppSwitchPayment(Payment):
    """ MobilePay AppSwitch payment """

    def void(self):
        if not self.can_void():
            raise Exception("Payment cannot be voided")
        client = MobilePayAppSwitchPaymentClient()
        (success, result) = client.delete(orderId=self.order.key().id())
        logging.info(result)
        if success:
            self.status = 'void'
            self.put()
            return True
        else:
            raise Exception("Payment void failed with MobilePay AppSwitch error code %s" % result.status_code)

    def capture(self):
        logging.info("MobilePay appswitch capture called")
        if not self.can_capture():
            raise Exception("Payment cannot be captured")
        client = MobilePayAppSwitchPaymentClient()
        (success, result) = client.capture(orderId=self.order.key().id())
        if success:
            self.status = 'captured'
            self.put()
            return True
        else:
            raise Exception("Payment capture failed with MobilePay AppSwitch error code %s" % result.status_code)

    def calculate_fee(self, amount, currency):
        return Decimal('0') # We are no longer charging payment fees

class NordpayPayment(Payment):
    """ Nordpay payment """
    registration_id = db.StringProperty()
    preauthorization_id = db.StringProperty()
    method = db.StringProperty(default="CC", choices=["CC", "DC"]) # Credit or debit card
    
    def authorize(self):
        if not self.can_authorize():
            raise Exception("Payment cannot be authorized")
        currency = self.order.currency
        nordpay = NordpayClient(live=(get_environment()=='production'))
        response = nordpay.preauthorize(
            method=self.method,
            amount=self.amount,
            currency=currency,
            orderid=self.order.key().id(),
            registration=self.registration_id)
        if response.is_success():
            self.set_authorized(response.find_text('Transaction/Identification/UniqueID'), self.amount, currency)
            return True
        else:
            error = response.get_error()
            if error is None:
                error = "Payment authorization failed"
            raise PaymentException(error)
            
    def set_authorized(self, preauthorization_id, amount, currency):
        self.preauthorization_id = preauthorization_id
        self.status = 'authorized'
        self.fee = self.calculate_fee(amount, currency)
        self.put()
        
    def void(self):
        if not self.can_void():
            raise Exception("Payment cannot be voided")
        nordpay = NordpayClient(live=(get_environment()=='production'))
        response = nordpay.reverse(method=self.method, identifier=self.preauthorization_id, orderid=self.order.key().id())
        if response.is_success():
            self.status = 'void'
            self.put()
            return True
        else:
            raise Exception("Payment void action failed")
            
    def capture(self):
        if not self.can_capture():
            raise Exception("Payment cannot be captured")
        nordpay = NordpayClient(live=(get_environment()=='production'))
        currency = self.order.currency
        response = nordpay.capture(
            method=self.method,
            amount=self.amount,
            currency=currency,
            orderid=self.order.key().id(),
            identifier=self.preauthorization_id,
            registration=self.registration_id)
        if response.is_success():
            self.status = 'captured'
            self.put()
            return True
        else:
            raise Exception("Payment capture failed")
            
    def calculate_fee(self, amount, currency):
        return Decimal('0') # We are no longer charging payment fees
        #return convert_currency(amount=Decimal('1.2'), base='DKK', target=currency) + amount * Decimal('0.0275') # 1.20 + 2.75 %

class FourTPayment(Payment):
    account_id = db.StringProperty()
    transaction_id = db.StringProperty()
    vat_amount = DecimalProperty(2, default=Decimal('0.00'))

    def authorize(self):
        if not self.can_authorize():
            raise Exception("Payment cannot be authorized")
        currency = self.order.currency
        fourt = FourTClient(live=(get_environment()=='production'))
        response = fourt.create_and_authorize(
            account_id=self.account_id,
            amount=self.amount,
            currency=currency,
            vat_amount=self.vat_amount,
            reference_id=self.order.key().id(),
            order_key=str(self.order.key()),
            order_number=self.order.order_number,
            location_name=self.order.location.name
            )
        if response.is_success():
            #self.transaction_id = response.get_transaction_id()
            #self.put()
            # Status is not yet updated - wait for confirmation on the callback URL
            return True
        else:
            error = response.get_error()
            if error is None:
                error = "Payment authorization failed"
            raise Exception(error)

    def void(self):
        if not self.can_void():
            raise Exception("Payment cannot be voided")
        fourt = FourTClient(live=(get_environment()=='production'))
        response = fourt.cancel(self.transaction_id, str(self.order.key()))
        if response.is_success():
            # Status is not yet updated - wait for confirmation on the callback URL
            return True
        else:
            raise Exception("Payment void action failed")

    def capture(self):
        if not self.can_capture():
            raise Exception("Payment cannot be captured")
        fourt = FourTClient(live=(get_environment()=='production'))
        response = fourt.capture(self.transaction_id, str(self.order.key()))
        if response.is_success():
            # Status is not yet updated - wait for confirmation on the callback URL
            return True
        else:
            raise Exception("Payment capture failed")