#!/usr/bin/python
# -*- coding: utf-8 -*-
from google.appengine.api import urlfetch
import urllib
import xml.etree.ElementTree as ET
from webapp2_extras import i18n
_ = i18n.gettext
import logging

NORDPAY_CONFIG = {
    'test': {
        'sender_id': 'ff8080813e175869013e3b3c57201a08',
        'user': 'ff8080813e175869013e3b3c57211a0c',
        'password': 'd6bMyzDq',
        'url': 'https://test.ctpe.io/payment/ctpe',
        'channel_id': '8a82941744d095850144dab08dd41f31',
        'response': 'SYNC',
        'mode': 'INTEGRATOR_TEST',
    },
    'live': {
        'sender_id': '8a8394c43ed17289013ed5d96dcb05ae',
        'user': '8a8394c43ed17289013ed5d96dcc05b2',
        'password': 'EAkan577',
        'url': 'https://ctpe.io/payment/ctpe',
        'channel_id': '8a8394c43ed17289013ed5d9d82205b6',
        'response': 'SYNC',
        'mode': 'LIVE',
    }
}

class NordpayClient:
    """ 
    This class is used to communicate with Nordpay using XML over HTTPS.
    The ElementTree Python library is used to construct the XML.
    """
    
    def __init__(self, live=False):
        self.config = NORDPAY_CONFIG['live'] if live else NORDPAY_CONFIG['test']
        
    def prepare_request(self):
        # XML header
        self.data = ET.Element('Request', {'version': '1.0'})
        header = ET.SubElement(self.data, 'Header')
        security = ET.SubElement(header, 'Security', {'sender': self.config['sender_id']})
        transaction = ET.SubElement(self.data, 'Transaction', {'mode': self.config['mode'], 'response': self.config['response'], 'channel': self.config['channel_id']})
        ET.SubElement(transaction, 'User', {'login': self.config['user'], 'pwd': self.config['password']})
        
        # Dummy customer data (not used, but required by the API)
        customer = ET.SubElement(transaction, 'Customer')
        name = ET.SubElement(customer, 'Name')
        given = ET.SubElement(name, 'Given')
        given.text = 'Zerved'
        family = ET.SubElement(name, 'Family')
        family.text = 'Customer'
        address = ET.SubElement(customer, 'Address')
        street = ET.SubElement(address, 'Street')
        street.text = 'Nordtoftevej 33'
        addresszip = ET.SubElement(address, 'Zip')
        addresszip.text = '2860'
        city = ET.SubElement(address, 'City')
        city.text = u'Søborg'
        country = ET.SubElement(address, 'Country')
        country.text = 'DK'
        contact = ET.SubElement(customer, 'Contact')
        email = ET.SubElement(contact, 'Email')
        email.text = 'info@zervedapp.com'
        ip = ET.SubElement(contact, 'Ip')
        ip.text = '127.0.0.1'
        
    def call(self):
        # Call Nordpay
        urlfetch.set_default_fetch_deadline(20) # Default is 5 seconds which is sometimes too short
        requestdata = ET.tostring(self.data, encoding='UTF-8')
        logging.info(requestdata)
        try:
            result = urlfetch.fetch(
                url=self.config['url'],
                payload=urllib.urlencode({'load': requestdata}),
                method=urlfetch.POST,
                validate_certificate=False,
                headers={'Content-Type': 'application/x-www-form-urlencoded'})
        except urlfetch.DownloadError:
            raise Exception(_("Payment service not responding, try again later."))
        except urlfetch.SSLCertificateError:
            raise Exception('Payment SSL failed')
        except urlfetch.ResponseTooLargeError:
            raise Exception('Payment response too large')
        if result.status_code == 200:
            logging.info(result.content.encode('utf-8'))
            return NordpayResponse(result.content)
        else:
            raise Exception('Payment transaction failed')
        
    def preauthorize(self, method, amount, currency, orderid, registration):
        self.prepare_request()
        
        # Payment data
        identification = ET.SubElement(self.data.find('Transaction'), 'Identification')
        transactionid = ET.SubElement(identification, 'TransactionID')
        transactionid.text = str(orderid)
        payment = ET.SubElement(self.data.find('Transaction'), 'Payment', {'code': method+'.PA'})
        presentation = ET.SubElement(payment, 'Presentation')
        amountdata = ET.SubElement(presentation, 'Amount')
        amountdata.text = str(amount)
        currencydata = ET.SubElement(presentation, 'Currency')
        currencydata.text = currency
        usage = ET.SubElement(presentation, 'Usage')
        usage.text = str(orderid)
        account = ET.SubElement(self.data.find('Transaction'), 'Account', {'registration': registration})
        recurrence = ET.SubElement(self.data.find('Transaction'), 'Recurrence', {'mode': 'REPEATED'})
        
        # Call Nordpay
        return self.call()
            
    def deregister(self, method, registration):
        self.prepare_request()
        
        # Registration data
        identification = ET.SubElement(self.data.find('Transaction'), 'Identification')
        referenceid = ET.SubElement(identification, 'ReferenceID')
        referenceid.text = registration
        payment = ET.SubElement(self.data.find('Transaction'), 'Payment', {'code': method+'.DR'})
        
        # Call Nordpay
        return self.call()
        
    def reverse(self, method, identifier, orderid):
        self.prepare_request()
        
        # Reverse preauthorized transaction
        identification = ET.SubElement(self.data.find('Transaction'), 'Identification')
        referenceid = ET.SubElement(identification, 'ReferenceID')
        referenceid.text = identifier
        transactionid = ET.SubElement(identification, 'TransactionID')
        transactionid.text = str(orderid)
        payment = ET.SubElement(self.data.find('Transaction'), 'Payment', {'code': method+'.RV'})
        
        # Call Nordpay
        return self.call()
        
    def capture(self, method, amount, currency, orderid, identifier, registration):
        self.prepare_request()
        
        # Capture preauthorized transaction
        payment = ET.SubElement(self.data.find('Transaction'), 'Payment', {'code': method+'.CP'})
        presentation = ET.SubElement(payment, 'Presentation')
        amountdata = ET.SubElement(presentation, 'Amount')
        amountdata.text = str(amount)
        currencydata = ET.SubElement(presentation, 'Currency')
        currencydata.text = currency
        usage = ET.SubElement(presentation, 'Usage')
        usage.text = str(orderid)
        identification = ET.SubElement(self.data.find('Transaction'), 'Identification')
        referenceid = ET.SubElement(identification, 'ReferenceID')
        referenceid.text = identifier
        transactionid = ET.SubElement(identification, 'TransactionID')
        transactionid.text = str(orderid)
        if registration:
            recurrence = ET.SubElement(self.data.find('Transaction'), 'Recurrence', {'mode': 'REPEATED'})
        
        # Call Nordpay
        return self.call()
            
class NordpayResponse:
    """ Wrapper class holding Nordpay response as an ElementTree """
    
    def __init__(self, xmlstring):
        self.response = ET.fromstring(xmlstring)
        
    def is_success(self):
        transactionresult = self.response.find('Transaction/Processing/Result')
        return transactionresult is not None and transactionresult.text == 'ACK'

    def get_error(self):
        status = self.find_text('Transaction/Processing/Status')
        if status == 'REJECTED_BANK':
            return _("Card rejected by bank")
        elif status == 'REJECTED_VALIDATION':
            return _("Invalid card details")
        else:
            return None
        
    def find_text(self, path):
        element = self.response.find(path)
        return None if element is None else element.text