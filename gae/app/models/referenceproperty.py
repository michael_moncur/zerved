from google.appengine.ext import db

class ReferenceProperty(db.ReferenceProperty):
    """A property that represents a many-to-one reference to another model.

    This acts as identical to google.appengine.ext.db.ReferenceProperty,
    except objects returns None when referenced entity does not exist
    instead of raising ReferencePropertyResolveError.
    """
    def __get__(self, *args, **kw):
        try:
            return super(ReferenceProperty, self).__get__(*args, **kw)
        except db.ReferencePropertyResolveError:
            return None