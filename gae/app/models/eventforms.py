from models.events import EventSchedule
from wtforms.ext.appengine.db import model_form
from wtforms import SelectField
from webapp2_extras import i18n
_ = i18n.lazy_gettext

def get_event_form():
    EventModelForm = model_form(EventSchedule, exclude=('merchant', 'location', 'first_date', 'last_date', 'close_offset_minutes', 'repeats'), field_args={
        'name': { 'label': _('Name') },
        'repeats': { 'label': _('Repeats') },
        'repeat_every': { 'label': _('Repeat every') },
    })
    class EventForm(EventModelForm):
        select_repeats = SelectField(label=_('Repeats'))

        def set(self):
            self.select_repeats.choices = []
            self.select_repeats.choices.append(('',_('Never')))
            self.select_repeats.choices.append(('daily',_('Daily')))
            self.select_repeats.choices.append(('weekly',_('Weekly')))
            self.select_repeats.choices.append(('monthly',_('Monthly')))
           
        def set_selected_repeats(self, v):
            self.select_repeats.data = v

    return EventForm