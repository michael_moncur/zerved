from elementsoap.ElementSOAP import *
from elementsoap.HTTPClientZerved import HTTPClient, HTTPError
import urllib
from google.appengine.api import urlfetch
import logging

NS_EPAY_SUBSCRIBE = "https://ssl.ditonlinebetalingssystem.dk/remote/subscription"
NS_EPAY_PAYMENT = "https://ssl.ditonlinebetalingssystem.dk/remote/payment"
CURRENCY_CODE_TO_NUMBER = {'DKK': 208, 'USD': 840, 'EUR': 978, 'GBP': 826, 'SEK': 752, 'NOK': 578}
# ZERVED_EPAY_PROXY_URL = "http://epay.zervedapp.com/index.php"
# ZERVED_EPAY_PROXY_KEY = "80s9b8fa51627ef"

class EpayClient(SoapService):
    """
    Base client class for Epay webservices
    It will attempt to call epay directly
    """
    
    service = ""
    namespace = ""

    @staticmethod
    def convert_currency_code_to_number(code):
        if code in CURRENCY_CODE_TO_NUMBER:
            return CURRENCY_CODE_TO_NUMBER[code]
        else:
            raise Exception('Currency code not defined in ePay')

    def call_epay(self, action, method, params):
        urlfetch.set_default_fetch_deadline(10) # Default is 5 seconds which is sometimes too short for ePay
        # Build SOAP request
        #logging.info(params)
        request = SoapRequest(method, None)
        request.set("xmlns", self.namespace)
        for name, type, text in params:
            SoapElement(request, name, type, text)
        # Try SOAP call
        response = self.call(action, request)
        epayresponse = EpayResponse(response, self.namespace)
        return epayresponse

    '''
    def call_proxy(self, action, request, params):
        # build SOAP envelope
        envelope = ET.Element(NS_SOAP_ENV + "Envelope")
        body = ET.SubElement(envelope, NS_SOAP_ENV + "Body")
        body.append(request)
        
        # Instead of SOAP, call proxy server with POST request
        try:
            response = HTTPClient(ZERVED_EPAY_PROXY_URL).do_request(
                body=urllib.urlencode(params),
                path=ZERVED_EPAY_PROXY_URL,
                method='POST',
                content_type='application/x-www-form-urlencoded',
                parser=namespace_parse
                )
        except HTTPError, v:
            if v[0] == 500:
                # might be a SOAP fault
                response = namespace_parse(v[3])
            elif v[0] in (301, 302):
                # redirect
                location = v[2].headers["location"]
                raise SoapFault(
                    '',
                    'HTTP Error %s: Location Changed' % v[0],
                    'Client',
                    'Service changed location to %r' % location,
                    )

        headers = response.findall(NS_SOAP_ENV + "Header")
        # FIXME: check mustunderstand attribute

        response = response.find(body.tag)[0]

        # fixup any XSI_type attributes
        # FIXME: only do this if envelope uses known soapencoding
        for elem in response.getiterator():
            type = elem.get(NS_XSI + "type")
            if type:
                elem.set(NS_XSI + "type", namespace_qname(elem, type))

        # look for fault descriptors
        if response.tag == NS_SOAP_ENV + "Fault":
            faultcode = response.find("faultcode")
            if faultcode is not None:
                raise SoapFault(
                    namespace_qname(faultcode, faultcode.text),
                    response.findtext("faultstring"),
                    response.findtext("faultactor"),
                    response.find("detail")
                    )
            else:
                # workaround: TGWebServices uses namespaced Fault sub-
                # element names
                faultcode = response.find(NS_SOAP_ENV + "faultcode")
                # TODO: manage the soapactor... :(
                raise SoapFault(
                    namespace_qname(faultcode, faultcode.text),
                    response.findtext(NS_SOAP_ENV + "faultstring"),
                    'Server', #response.findtext(NS_SOAP_ENV + "faultactor"),
                    response.findtext(NS_SOAP_ENV + "detail")
                    )

        response.tail = "\n\n" # nicer printouts

        return response
    '''
class EpaySubscriptionClient(EpayClient):
    """
    Epay client for subscriptions (saved credit cards)
    """
    
    url = "https://ssl.ditonlinebetalingssystem.dk/remote/subscription.asmx"
    service = "subscription"
    namespace = NS_EPAY_SUBSCRIBE
    
    def authorize(self, merchantnumber, subscriptionid, orderid, amount, currency, instantcapture, pwd):
        action = "https://ssl.ditonlinebetalingssystem.dk/remote/subscription/authorize"
        method = "authorize"
        if isinstance(currency, basestring):
            currency = EpayClient.convert_currency_code_to_number(currency)
        params = [
            ("merchantnumber", "int", str(merchantnumber)),
            ("subscriptionid", "long", str(subscriptionid)),
            ("orderid", "string", str(orderid)),
            ("amount", "int", str(int(amount*100))),
            ("currency", "int", str(currency)),
            ("instantcapture", "int", str(instantcapture)),
            ("pwd", "string", pwd)
        ]
        return self.call_epay(action, method, params)
        
    def deletesubscription(self, merchantnumber, subscriptionid, pwd):
        action = "https://ssl.ditonlinebetalingssystem.dk/remote/subscription/deletesubscription"
        method = "deletesubscription"
        params = [
            ("merchantnumber", "int", str(merchantnumber)),
            ("subscriptionid", "long", str(subscriptionid)),
            ("pwd", "string", pwd)
        ]
        return self.call_epay(action, method, params)
        
class EpayPaymentClient(EpayClient):
    """
    Epay client for payments
    """
    
    url = "https://ssl.ditonlinebetalingssystem.dk/remote/payment.asmx"
    service = "payment"
    namespace = NS_EPAY_PAYMENT
    
    def delete(self, merchantnumber, transactionid, pwd):
        action = "https://ssl.ditonlinebetalingssystem.dk/remote/payment/delete"
        method = "delete"
        params = [
            ("merchantnumber", "int", str(merchantnumber)),
            ("transactionid", "long", str(transactionid)),
            ("pwd", "string", pwd)
        ]
        return self.call_epay(action, method, params)
        
    def capture(self, merchantnumber, transactionid, amount, pwd):
        action = "https://ssl.ditonlinebetalingssystem.dk/remote/payment/capture"
        method = "capture"
        params = [
            ("merchantnumber", "int", str(merchantnumber)),
            ("transactionid", "long", str(transactionid)),
            ("amount", "int", str(int(amount*100))),
            ("pwd", "string", pwd)
        ]
        return self.call_epay(action, method, params)
        
    def credit(self, merchantnumber, transactionid, amount, pwd):
        action = "https://ssl.ditonlinebetalingssystem.dk/remote/payment/credit"
        method = "credit"
        params = [
            ("merchantnumber", "int", str(merchantnumber)),
            ("transactionid", "long", str(transactionid)),
            ("amount", "int", str(int(amount*100))),
            ("pwd", "string", pwd)
        ]
        return self.call_epay(action, method, params)

    def gettransaction(self, merchantnumber, transactionid, pwd):
        action = "https://ssl.ditonlinebetalingssystem.dk/remote/payment/gettransaction"
        method = "gettransaction"
        params = [
            ("merchantnumber", "int", str(merchantnumber)),
            ("transactionid", "long", str(transactionid)),
            ("pwd", "string", pwd)
        ]
        return self.call_epay(action, method, params)

class EpayResponse:
    """
    Wrapper class for epay webservice response
    """
    
    def __init__(self, response, namespace):
        self.response = response
        self.namespace = namespace
        
    def get(self, key):
        #for key in callback_result:
        #    logging.debug("key" + key)
        return self.response.findtext("{%s}%s" % (self.namespace, key))