from google.appengine.ext import db
from merchants import Merchant, Location
import datetime, logging, calendar, pytz
from webapp2_extras import i18n
_ = i18n.gettext

class EventSchedule(db.Model):
    """
    An event schedule is created by the merchant, and when saved, it generates 
    a number of Events based on the repetition settings
    The merchant defines the schedule in the merchant's own time zone, but the
    generated events are saved in UTC to be compatible with the rest of the DB

    Example schedule:
    Piano Concert
    Starts December 11, 2013 20:00
    Repeat every day until December 13, 2013
    Serve orders before show 19:30 and in break 21:15

    Resulting events:
    Piano Concert December 11 20:00 before show 19:30
    Piano Concert December 11 20:00 break 21:15
    Piano Concert December 12 20:00 before show 19:30
    Piano Concert December 12 20:00 break 21:15
    Piano Concert December 13 20:00 before show 19:30
    Piano Concert December 13 20:00 break 21:15
    """
    name = db.StringProperty(required=True)
    merchant = db.ReferenceProperty(Merchant, collection_name='recurring_events')
    location = db.ReferenceProperty(Location, collection_name='recurring_events')
    enabled_bars = db.ListProperty(db.Key)
    first_date = db.DateTimeProperty() # Datetime first show start merchant time zone
    labels = db.ListProperty(str) # Break, Before show
    times = db.ListProperty(str) # 20:00, 18:00 (merchant time zone)
    last_date = db.DateTimeProperty() # Datetime last show start merchant time zone
    close_offset_minutes = db.IntegerProperty() # How many minutes after event to close for orders (negative before)
    repeats = db.StringProperty(choices=['', 'daily', 'weekly', 'monthly']) # Use empty string for onetime events
    repeat_every = db.IntegerProperty(default=1) # Repeat every week, every second week, every 3rd day etc.
    excluded_dates = db.ListProperty(datetime.datetime) # Date and time using merchant time zone

    def get_diff_days(self):
        diff = self.last_date - self.first_date
        return divmod(diff.total_seconds(), 24*60*60)[0]
 
    def get_diff_weeks(self):
        return divmod(self.get_diff_days(), 7)[0]

    def get_diff_months(self):
        delta = 0
        d1 = self.first_date
        d2 = self.last_date
        while True:
            mdays = calendar.monthrange(d1.year, d1.month)[1]
            d1 += datetime.timedelta(days=mdays)
            if d1 <= d2:
                delta += 1
            else:
                break
        return delta

    def get_repetition_days(self):
        """
        Calculate how many days the event is repeated
        """
        if self.repeats == 'daily':
            return 1 + (int) (self.get_diff_days() / self.repeat_every)
        elif self.repeats == 'weekly':
            return 1 + (int) (self.get_diff_weeks() / self.repeat_every)
        elif self.repeats == 'monthly':
            return 1 + (int) (self.get_diff_months() / self.repeat_every)
        else:
            return 1

    def get_repetitions(self):
        """
        Calculate the total no. of events for the schedule
        """
        return self.get_repetition_days()*len(self.times) # No. of days * no. of times per day

    def put(self):
        super(EventSchedule, self).put()

        # Delete Events
        db.delete(Event.all().ancestor(self))

        # Create Events
        to_put = []
        date = self.first_date

        # Loop days
        i = 0            
        while i < self.get_repetition_days():
            event_start_utc = pytz.timezone(self.merchant.timezone).localize(date).astimezone(pytz.utc)
            # Loop times per day
            t = 0
            while t < len(self.times):
                hours = int(self.times[t].split(":")[0])
                minutes = int(self.times[t].split(":")[1])
                date_delivery_merchant = date.replace(minute=minutes, hour=hours)
                date_delivery_utc = pytz.timezone(self.merchant.timezone).localize(date_delivery_merchant).astimezone(pytz.utc)

                # Skip time if excluded
                if not self.is_datetime_excluded(date_delivery_merchant):
                    # Save event
                    event = Event(parent=self, merchant=self.merchant, location=self.location, enabled_bars=self.enabled_bars, name=self.name, time_label=self.labels[t], date_start=event_start_utc, date_delivery=date_delivery_utc, close_offset_minutes=self.close_offset_minutes)
                    to_put.append(event)
                t += 1
            date = self.next_event_date(date)
            i += 1
        db.put(to_put)

    def delete(self):
        # Delete Events
        db.delete(Event.all().ancestor(self))
        super(EventSchedule, self).delete()

    def next_event_date(self, date):
        if self.repeats == 'daily':
            return date + datetime.timedelta(days=1*self.repeat_every)
        elif self.repeats == 'weekly':
            return date + datetime.timedelta(days=7*self.repeat_every)
        elif self.repeats == 'monthly':
            return self.add_months(date, 1*self.repeat_every)
        return None

    def add_months(self, sourcedate, months):
        month = sourcedate.month - 1 + months
        year = sourcedate.year + month / 12
        month = month % 12 + 1
        day = min(sourcedate.day, calendar.monthrange(year, month)[1])
        return datetime.datetime(year, month, day, sourcedate.hour, sourcedate.minute)

    def add_bar(self, bar):
        if not bar in self.enabled_bars:
            self.enabled_bars.append(bar)
            return True
        return False

    def remove_bar(self, bar):
        if bar in self.enabled_bars:
            self.enabled_bars.remove(bar)
            return True
        return False

    def is_datetime_excluded(self, eventdate):
        for d in self.excluded_dates:
            if d.year == eventdate.year and d.month == eventdate.month and d.day == eventdate.day and d.hour == eventdate.hour and d.minute == eventdate.minute:
                return True
        return False

class Event(db.Model):
    """
    Event has a parent EventSchedule
    Events are generated when a EventSchedule is saved/updated
    """
    merchant = db.ReferenceProperty(Merchant, collection_name='events')
    location = db.ReferenceProperty(Location, collection_name='events')
    enabled_bars = db.ListProperty(db.Key)
    name = db.StringProperty(required=True) # Event name from EventSchedule
    date_start = db.DateTimeProperty(required=True) # UTC time for show start
    time_label = db.StringProperty() # Label for the time where orders are served (e.g. interval)
    date_delivery = db.DateTimeProperty(required=True) # UTC time when orders are served
    close_offset_minutes = db.IntegerProperty() # How many minutes before event to close for orders

    API_PROPERTIES = ['name', 'time_label', 'date_start_formatted', 'date_delivery_formatted', 'date_start_date_formatted', 'date_delivery_date_formatted', 'date_start_time_formatted', 'date_delivery_time_formatted', 'open_for_orders']

    @property
    def bars(self):
        return [b for b in db.get(self.enabled_bars) if b is not None]

    @property
    def date_start_formatted(self):
        return i18n.format_datetime(self.local_date_start, 'short', rebase=False)

    @property
    def date_delivery_formatted(self):
        return i18n.format_datetime(self.local_date_delivery, 'short', rebase=False)

    @property
    def date_start_time_formatted(self):
        return i18n.format_time(self.local_date_start, 'short', rebase=False)

    @property
    def date_delivery_time_formatted(self):
        return i18n.format_time(self.local_date_delivery, 'short', rebase=False)

    @property
    def date_start_date_formatted(self):
        return i18n.format_date(self.local_date_start, 'short', rebase=False)

    @property
    def date_delivery_date_formatted(self):
        return i18n.format_date(self.local_date_delivery, 'short', rebase=False)

    @property
    def local_date_start(self):
        # Date in merchant's time zone
        return self.date_start.replace(tzinfo=pytz.utc).astimezone(pytz.timezone(self.merchant.timezone))

    @property
    def local_date_delivery(self):
        # Date in merchant's time zone
        return self.date_delivery.replace(tzinfo=pytz.utc).astimezone(pytz.timezone(self.merchant.timezone))

    def format_date_with_timezone(self, date, timezone):
        # Date in merchant's time zone
        local_date = date.replace(tzinfo=pytz.utc).astimezone(pytz.timezone(timezone))
        return i18n.format_datetime(local_date, 'short', rebase=False)

    @property
    def order_end_date(self):
        return self.date_delivery + datetime.timedelta(minutes=self.close_offset_minutes)

    @property
    def open_for_orders(self):
        now = datetime.datetime.now()
        return now < self.order_end_date
