#!/usr/bin/python
# -*- coding: utf-8 -*-
from decimal import Decimal
import collections

ALLOWED_LOCALES = ['en_GB', 'da_DK', 'es_ES']
LANGUAGE_LOCALES = collections.OrderedDict([('en', 'en_GB'), ('da', 'da_DK'), ('es', 'es_ES')])

ALLOWED_COUNTRIES = ['DK', 'ES', 'GB', 'ZA']

ALLOWED_CURRENCIES = ['DKK', 'EUR', 'GBP', 'USD', 'ZAR']

COMPANY_DATA = {
    'DK': {
        'name': "Zerved DK ApS",
        'address': u"Nordtoftevej 33\n2860 Søborg\nDenmark",
        'vat': "DK35484639"
    },
    'ES': {
        'name': "Zerved España",
        'address': u"Urbanizacion La Torrecilla\n49 Adosada Lomas Bellas\nMarbella\nSpain",
        'vat': "N0082201E"
    },
    'GB': {
        'name': "Zerved UK Ltd",
        'address': u"422 The Beaux-Arts Building\n10-18 Manor Gardens\nLondon, N7 6JS",
        'vat': "GB165946861"
    },
    'ZA': {
        'name': "Zerved South Africa (PTY) LTD",
        'address': u"4 Wilden Place\nLa Lucia\nDurban\n4051\nSouth Africa",
        'vat': "N/A"
    }
}

# VAT rate for Zerved fees
VAT_RATES = {
    'DK': Decimal('0.25'),
    'ES': Decimal('0.21'),
    'GB': Decimal('0.20'),
    'ZA': Decimal('0.14')
}

# Default product VAT rates
PRODUCT_VAT_RATES = {
    'DK': Decimal('0.25'),
    'ES': Decimal('0.21'),
    'GB': Decimal('0.20'),
    'ZA': Decimal('0.14')
}

CURRENCY_RATES = {
    'DKK': {
        'DKK': 1.0,
        'USD': 0.1744850956,
        'EUR': 0.1341618496,
        'GBP': 0.1140308390,
        'ZAR': 1.79317,
    },
    'USD': {
        'DKK': 5.7224548134,
        'USD': 1.0,
        'EUR': 0.7676612884,
        'GBP': 0.6533182816,
        'ZAR': 9.98655
    },
    'EUR': {
        'DKK': 7.4544452443,
        'USD': 1.3025115009,
        'EUR': 1.0,
        'GBP': 0.8510498600,
        'ZAR': 13.3707
    },
    'GBP': {
        'DKK': 8.7612740334,
        'USD': 1.5307328476,
        'EUR': 1.1753158225,
        'GBP': 1.0,
        'ZAR': 15.5908
    },
    'ZAR': {
        'DKK': 0.557671,
        'USD': 0.100101,
        'EUR': 0.0747693,
        'GBP': 0.0641403,
        'ZAR': 1.0
    }
}

def convert_currency(amount, base, target):
    return Decimal(amount)*Decimal(CURRENCY_RATES[base][target])

ADDRESS_FORMATS = {
    'DK': "$name\n$address1\n$address2\n$postcode $city\nTlf. $phone",
    'ES': "$name\n$address1\n$address2\n$postcode $city\nTel. $phone",
    'GB': "$name\n$address1\n$address2\n$city\n$postcode\nTel. $phone",
    'ZA': "$name\n$address1\n$address2\n$city\n$postcode\nTel. $phone"
}