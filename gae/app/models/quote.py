from models.merchants import Merchant, Location, Bar
from models.catalog import Product, ProductOption, Menu
from models.sales import Order, OrderItem
from models.payment import NordpayPayment, EpayPayment
from google.appengine.ext import db
from models import i18ndata
from webapp2_extras import i18n
from decimal import Decimal
import datetime
_ = i18n.gettext
import logging

class CartItemOutdatedError(Exception):
    pass
    
class Quote:
    """
    A quote is a calulation of prices and totals based on shopping cart items
    submitted by the client.
    Quote is used to return price information for a user's shopping cart, and
    it is used to place orders.
    """
    
    def __init__(self, api_handler, location=None, bar=None):
        self.api_handler = api_handler
        if bar:
            # Bar quote
            self.bar = bar
            self.location = bar.location
            self.merchant = bar.merchant
        elif location:
            # Location quote
            self.bar = None
            self.location = location
            self.merchant = location.merchant
        else:
            raise Exception("Location or bar required for quote model")
        self.cart_items = []
        self.quote_items = None
        self.tip_percent = Decimal('0.00')
        self.service = ""
        self.table_number = ""
        self.to_go = False
        self.event = None
        self.delivery_name = ""
        self.delivery_phone = ""
        self.delivery_address1 = ""
        self.delivery_address2 = ""
        self.delivery_postcode = ""
        self.delivery_city = ""
        self.loyalty_code = ""
        self.delivery_instructions = ""
        self.custom_allowed_payment_methods = ""
            
    def set_cart_items(self, items):
        """
        Set cart items
        items = [{'product': productkey, 'qty': quantity, 'options': [{'option_key': optionkey, 'option_id: optionid, 'option_value': optionvalue}]}]
        """
        self.cart_items = []
        self.quote_items = None
        for item in items:
            # Convert options dictionary to list of tuples (option id, option value)
            options = []
            if 'options' in item:
                for option in item['options']:
                    if 'option_key' in option:
                        product_option = ProductOption.get(option['option_key'])
                    elif 'option_id' in option:
                        product_option = ProductOption.get(db.Key.from_path('ProductOption', int(option['option_id'])))
                    if product_option:
                        options.append((int(product_option.key().id()), int(option['option_value'])))
                    else:
                        options.append(0, 9999)
            item['options'] = options
            self.cart_items.append(item)
        
    def get_quote_items(self):
        """
        Return list of quote item objects (prices for cart items)
        """
        # Return if already loaded
        if self.quote_items:
            logging.info('Quote.get_quote_items: self.quote_items exist!')
            return self.quote_items
        else:
            logging.info('Quote.get_quote_items: building self.quote_items!')
            
        self.quote_items = []
        products = Product.get([item['product'] for item in self.cart_items])
        # Preload price rules
        products = Product.prefetch_merchant_price_rules(products, self.merchant)
        # Preload categories
        products = Product.prefetch_categories(products)
        # Preload merchant
        for p in products:
            logging.info('Quote.get_quote_items: p=%s' % p)
            p.merchant = self.merchant

        # Get a list of active menu keys
        if self.bar:
            deliverytime = self.get_date_delivery()
            active_menus = set([str(m.key()) for m in Menu.get_active_menus_at(self.bar, deliverytime).fetch(100)])

        for item in self.cart_items:
            try:
                product = next((p for p in products if p and str(p.key()) == item['product']), None)
                # Validate existence of product and options
                if not product:
                    raise CartItemOutdatedError("Cart item outdated")

                # Checks if the product is in an active menu card
                if self.bar:
                    product_active_menus = set([str(m) for m in product.menus]).intersection(active_menus)
                    if len(product_active_menus) == 0:
                        raise CartItemOutdatedError("Cart item outdated")
                
                for (option_id, value) in item['options']:
                    option = next((o for o in product.get_options() if o and o.key().id() == option_id), None)
                    if not option:
                        raise CartItemOutdatedError("Cart item outdated")
                    if len(option.value_prices)-1 < value:
                        raise CartItemOutdatedError("Cart item outdated")

                # Remove tax from onpremises-only taxed products if service is takeaway (UK tax rules)
                if self.bar and self.bar.is_service_takeaway(self.service, self.to_go) and product.get_tax_class() and product.get_tax_class().on_premises_only:
                    product.tax_class = None

                base_price_excl_tax = product.get_base_price(incl_tax=False)
                base_price_incl_tax = product.get_base_price(incl_tax=True)
                subtotal_excl_tax = product.get_price(quantity=item['qty'],incl_tax=False)
                subtotal_incl_tax = product.get_price(quantity=item['qty'],incl_tax=True)
                loyaltydiscount_excl_tax = product.get_loyalty_discount(price=subtotal_excl_tax, loyalty_code=self.loyalty_code)
                loyaltydiscount_incl_tax = product.get_loyalty_discount(price=subtotal_incl_tax, loyalty_code=self.loyalty_code) 
#                logging.info('Quote.get_quote_items: loyaltydiscount_excl_tax: %d for item:%s product:%s loyaltycode:%s' % (loyaltydiscount_excl_tax,item,product,self.loyalty_code))

                preloyalty_surcharge_excl_tax = product.get_total_surcharge(options=item['options'], quantity=item['qty'], incl_tax=False)
                preloyalty_surcharge_incl_tax = product.get_total_surcharge(options=item['options'], quantity=item['qty'], incl_tax=True)
                surcharge_loyaltydiscount_excl_tax = product.get_loyalty_discount(price=preloyalty_surcharge_excl_tax, loyalty_code=self.loyalty_code)
                surcharge_loyaltydiscount_incl_tax = product.get_loyalty_discount(price=preloyalty_surcharge_incl_tax, loyalty_code=self.loyalty_code)
                total_surcharge_excl_tax = preloyalty_surcharge_excl_tax - surcharge_loyaltydiscount_excl_tax
                total_surcharge_incl_tax = preloyalty_surcharge_incl_tax - surcharge_loyaltydiscount_incl_tax

                total_loyaltydiscount_incl_tax = loyaltydiscount_incl_tax + surcharge_loyaltydiscount_incl_tax
                total_loyaltydiscount_excl_tax = loyaltydiscount_excl_tax + surcharge_loyaltydiscount_excl_tax
                tax_independent_price = product.get_price(quantity=item['qty'])
                price = tax_independent_price+product.get_total_surcharge(options=item['options'], quantity=item['qty']) # Price to display (may or may not contain tax)
                total_excl_tax = subtotal_excl_tax + total_surcharge_excl_tax - loyaltydiscount_excl_tax
                total_incl_tax = subtotal_incl_tax + total_surcharge_incl_tax - loyaltydiscount_incl_tax
                optiondata = product.get_optiondata(item['options'])
                currency = self.merchant.currency
                self.quote_items.append({
                    'qty': item['qty'],
                    'valid': True,
                    'category_name': product.category.name,
                    'product_key': str(product.key()),
                    'price': price,
                    'loyaltydiscount_excl_tax': total_loyaltydiscount_excl_tax,
                    'loyaltydiscount_incl_tax': total_loyaltydiscount_incl_tax,
                    'optiondata': optiondata,
                    'base_price_excl_tax': base_price_excl_tax,
                    'base_price_incl_tax': base_price_incl_tax,
                    'total_discount_excl_tax': (item['qty']*base_price_excl_tax) - (-total_loyaltydiscount_excl_tax) - subtotal_excl_tax,
                    'total_discount_incl_tax': (item['qty']*base_price_incl_tax) - (-total_loyaltydiscount_incl_tax) - subtotal_incl_tax,
                    'total_surcharge_excl_tax': total_surcharge_excl_tax,
                    'total_surcharge_incl_tax': total_surcharge_incl_tax,
                    'total_excl_tax': total_excl_tax,
                    'total_incl_tax': total_incl_tax,
                    'total_tax': total_incl_tax-total_excl_tax,
                    'tax_rate': product.get_tax_rate(),
                    'tax_class_key': (str(product.tax_class.key()) if product.get_tax_class() else None),
                    'tax_name': (product.tax_class.name if product.get_tax_class() else ''),
                    'name': product.name,
                    'price_formatted': i18n.format_currency(price, currency),
                    'options_summary': ", ".join([o['option_name']+": "+", ".join([v['value_label'] for v in o['values']]) for o in optiondata]),
                    'product_has_options': len(product.get_options()) > 0,
                    })
            except CartItemOutdatedError:
                self.quote_items.append({'valid': False, 'price': Decimal('0'), 'total_incl_tax': Decimal('0'), 'total_excl_tax': Decimal('0'), 'total_tax': Decimal('0') })
                
        return self.quote_items
        
    def get_subtotal(self, incl_tax=None):    
        # Sum of items
        if incl_tax == None:
            return sum(item['price'] for item in self.get_quote_items())
        elif incl_tax == True:
            return sum(item['total_incl_tax'] for item in self.get_quote_items())
        elif incl_tax == False:
            return sum(item['total_excl_tax'] for item in self.get_quote_items())
        
    def get_total_loyalty_discount(self, incl_tax=None):    
        # Sum of items
        if incl_tax == True:
            return sum(item['loyaltydiscount_incl_tax'] for item in self.get_quote_items())
        elif incl_tax == False:
            return sum(item['loyaltydiscount_excl_tax'] for item in self.get_quote_items())
        return sum(item['loyaltydiscount_incl_tax'] for item in self.get_quote_items()) #TODO: hacksy
        
    def get_tip(self):
        # Tip is always VAT free
        if self.merchant.tip_enabled and self.tip_percent > 0:
            return self.get_subtotal(incl_tax=True)*self.tip_percent/100
        else:
            return Decimal('0.00')
        
    def get_total(self, incl_tax=None):
        # Subtotal + tip + fees
        total = self.get_subtotal(incl_tax) + self.get_tip()
        if incl_tax == None:
            total += sum(fee['price'] for fee in self.get_fees())
        elif incl_tax == True:
            total += sum(fee['price_incl_tax'] for fee in self.get_fees())
        elif incl_tax == False:
            total += sum(fee['price_excl_tax'] for fee in self.get_fees())
        return total
        
    def get_total_tax(self):
        return sum(item['total_tax'] for item in self.get_quote_items()) + sum(fee['tax'] for fee in self.get_fees())
        
    def get_fees(self):
        fees = []
        if self.merchant.service_charge > 0:
            # Calculate service charge
            vat_rate = i18ndata.VAT_RATES[self.merchant.country]
            price_incl_tax = self.get_subtotal(incl_tax=True)*self.merchant.service_charge/100
            price_excl_tax = price_incl_tax/(1+vat_rate)
            tax = price_excl_tax*vat_rate
            price = price_incl_tax if self.merchant.prices_include_tax else price_excl_tax
            fees.append({
                'name': _('Service Charge'),
                'price': price,
                'price_formatted': i18n.format_currency(price, self.merchant.currency),
                'price_incl_tax': price_incl_tax,
                'price_excl_tax': price_excl_tax,
                'tax': tax,
                'tax_rate': i18ndata.VAT_RATES[self.merchant.country]*100
            })
        if self.service == 'delivery' and self.bar.delivery_fee > 0:
            # Delivery fee
            price_incl_tax = self.bar.get_delivery_fee(incl_tax=True)
            price_excl_tax = self.bar.get_delivery_fee(incl_tax=False)
            tax_rate = self.bar.delivery_fee_tax_class.rate if self.bar.delivery_fee_tax_class else Decimal(0)
            fees.append({
                'name': _('Delivery fee'),
                'price': self.bar.delivery_fee,
                'price_formatted': i18n.format_currency(self.bar.delivery_fee, self.merchant.currency),
                'price_incl_tax': price_incl_tax,
                'price_excl_tax': price_excl_tax,
                'tax': self.bar.get_delivery_tax(),
                'tax_rate': tax_rate
            })
        # Other fees may be added here...
        return fees
        
    def get_date_delivery(self):
        if self.event:
            return self.event.date_delivery
        else:
            return datetime.datetime.now()

    def to_json(self):
        """ Return quote information as JSON to consumer """
        currency = self.merchant.currency
        data = {
            'items': self.get_quote_items(), 
            'total': self.get_total(), 
            'total_formatted': i18n.format_currency(self.get_total(), currency), 
            'subtotal': self.get_subtotal(), 
            'subtotal_formatted': i18n.format_currency(self.get_subtotal(), currency), 
            'total_incl_tax': self.get_total(incl_tax=True), 
            'total_incl_tax_formatted': i18n.format_currency(self.get_total(incl_tax=True), currency),
            'total_loyalty_discount_incl_tax': -self.get_total_loyalty_discount(incl_tax=True),
            'total_loyalty_discount_excl_tax': -self.get_total_loyalty_discount(incl_tax=False), 
            'total_loyalty_discount_incl_tax_formatted': i18n.format_currency(-self.get_total_loyalty_discount(incl_tax=True), currency), 
            'total_loyalty_discount_excl_tax_formatted': i18n.format_currency(-self.get_total_loyalty_discount(incl_tax=False), currency), 
            'total_tax': self.get_total_tax(),
            'total_tax_formatted': i18n.format_currency(self.get_total_tax(), currency),
            'tip': self.get_tip(), 
            'tip_formatted': i18n.format_currency(self.get_tip(), currency),
            'currency': currency,
            'fees': self.get_fees(),
            'tip_enabled': self.merchant.tip_enabled,
            'has_loyalty_program': self.merchant.has_loyalty_program,
            'loyalty_code': self.loyalty_code,
            'loyalty_program_name': self.merchant.loyalty_program_name,
            'loyalty_code_valid': self.merchant.is_loyaltycode_valid(self.loyalty_code)
        }
        custom_allowed_payment_methods = self.get_custom_allowed_payment_methods_string(self.merchant)
        #if self.merchant.custom_allowed_payment_methods_enabled:
        data.update({'custom_allowed_payment_methods':custom_allowed_payment_methods})

        if self.bar:
            data.update({'bar': self.api_handler.get_object(self.bar, Bar.API_PROPERTIES) })
        else:
            data.update({'location': self.api_handler.get_object(self.location, Location.API_PROPERTIES_SPARSE) })
        return data

    def get_custom_allowed_payment_methods_string(self, merchant):
        if merchant.custom_allowed_payment_methods_enabled:
            enabled_methods = []
            if merchant.custom_allowed_payment_method_epay:
                enabled_methods.append("epay")
            if merchant.custom_allowed_payment_method_swipp:
                enabled_methods.append("swipp")
            if merchant.custom_allowed_payment_method_mobilepay_appswitch:
                enabled_methods.append("mobilepay_appswitch")
            if merchant.custom_allowed_payment_method_mobilepay_appswitch_embedded:
                enabled_methods.append("mobilepay_appswitch_embedded")
            return ",".join(enabled_methods)
        else:
            enabled_methods = []
            enabled_methods.append("epay")
            enabled_methods.append("mobilepay_appswitch")
            return ",".join(enabled_methods)
        #return ""

    def place_order(self, consumer, payment_method, nordpay_authorization=None, nordpay_method=None):
        """
        Place order from shopping cart data
        Order object and order item objects will be created, prices and totals calculated,
        and payment will be authorized (if not already authorized by client).
        The order will be sent to the merchant staff through the channel API.
        """

#        logging.info('Quote.place_order: quote.loyalty_code:%s' % self.loyalty_code)
        if(self.loyalty_code):
            if(self.merchant.is_loyaltycode_valid(code=self.loyalty_code)):
                if(self.merchant.register_loyaltycode_with_consumer(loyalty_code=self.loyalty_code, consumer=consumer)):
                    logging.debug('PLACE_ORDER: loyalty_code valid and not fraudulent!')
                else:
                    logging.debug('PLACE_ORDER: loyalty_code rejected, no loyalty discounts given!')
                    self.loyalty_code = ''
                    raise Exception(_("The loyalty number is already in use by another user"))
            else:
                logging.debug('PLACE_ORDER: loyalty_code is not valid, no loyalty discounts given!')
                self.loyalty_code = ''
                raise Exception(_("The loyalty number does not exist"))
        
        # Initialize order data
        try:
            locale = i18n.get_i18n().locale
        except AssertionError: # If request not set (when run from unit test script)
            locale = 'en'
        bar_name = self.bar.name if self.bar else None
        tip = self.get_tip()
        fees = self.get_fees()
        items = self.get_quote_items()

        # Validate min order amount
        if self.service == 'delivery' and self.get_subtotal() < self.bar.delivery_min_order_amount:
            raise Exception(_("The minimum order amount for delivery is %s" % self.bar.delivery_min_order_amount_formatted))
        
        # Validate items
        for item in items:
            if not item['valid']:
                raise Exception("Cart contains outdated items")
                
        # Create order
        total_loyalty_discount_incl_tax = (sum(item['loyaltydiscount_incl_tax'] for item in items)) 
        total_loyalty_discount_excl_tax = (sum(item['loyaltydiscount_excl_tax'] for item in items)) 
        logging.debug("total_loyalty_discount_incl_tax:%s" % total_loyalty_discount_incl_tax)

        total_incl_tax = (tip + sum(fee['price_incl_tax'] for fee in fees) + sum(item['total_incl_tax'] for item in items)) 
        total_excl_tax = (tip + sum(fee['price_excl_tax'] for fee in fees) + sum(item['total_excl_tax'] for item in items))
        total_tax = (Decimal('0.0') + sum(fee['tax'] for fee in fees) + sum(item['total_tax'] for item in items))
        date_delivery = self.get_date_delivery()
        event_name = self.event.name if self.event else ''
        event_time_label = self.event.time_label if self.event else ''
        order = Order(
            merchant=self.merchant,
            consumer=consumer,
            location=self.location,
            bar=self.bar,
            currency=self.merchant.currency,
            order_number=self.merchant.reserve_order_number(),
            merchant_name=self.merchant.name,
            location_name=self.location.name,
            bar_name=bar_name,
            timezone=self.merchant.timezone,
            locale=locale,
            merchant_country=self.merchant.country,
            service=self.service,
            table_number=self.table_number,
            to_go=self.to_go,
            total_incl_tax=total_incl_tax,
            total_excl_tax=total_excl_tax,
            total_tax=total_tax,
            total_loyalty_discount_incl_tax = -total_loyalty_discount_incl_tax,
            total_loyalty_discount_excl_tax = -total_loyalty_discount_excl_tax, 
            date_delivery=date_delivery,
            event_name=event_name,
            event_time_label=event_time_label,
            delivery_name=self.delivery_name,
            delivery_phone=self.delivery_phone,
            delivery_address1=self.delivery_address1,
            delivery_address2=self.delivery_address2,
            delivery_city=self.delivery_city,
            delivery_postcode=self.delivery_postcode,
            delivery_country=self.merchant.country,
            delivery_instructions=self.delivery_instructions,
            loyalty_code=self.loyalty_code
        )
        order.put()
        
        # Create order items
        index = 0
        for index, item in enumerate(items):
            orderitem = OrderItem(parent=order, order=order,
                product=db.Key(item['product_key']),
                name=item['name'],
                category_name=item['category_name'],
                quantity=item['qty'],
                base_price_excl_tax=item['base_price_excl_tax'],
                base_price_incl_tax=item['base_price_incl_tax'],
                loyaltydiscount_excl_tax=item['loyaltydiscount_excl_tax'], 
                loyaltydiscount_incl_tax=item['loyaltydiscount_incl_tax'], 
                total_discount_excl_tax=item['total_discount_excl_tax'],
                total_discount_incl_tax=item['total_discount_incl_tax'],
                total_surcharge_excl_tax=item['total_surcharge_excl_tax'],
                total_surcharge_incl_tax=item['total_surcharge_incl_tax'],
                total_excl_tax=item['total_excl_tax'],
                total_incl_tax=item['total_incl_tax'],
                total_tax=item['total_tax'],
                tax_class=(db.Key(item['tax_class_key']) if item['tax_class_key'] else None),
                tax_rate=item['tax_rate'],
                tax_name=item['tax_name'],
                sort_order=index)
            orderitem.set_options(item['optiondata'])
            orderitem.put()
        # Create order item for tip
        index += 1
        if tip > 0:
            orderitem = OrderItem(parent=order, order=order,
                product=None,
                name=_('Tip'),
                category_name=_('Fees'),
                quantity=1,
                base_price_excl_tax=tip,
                base_price_incl_tax=tip,
                loyaltydiscount_excl_tax=Decimal('0'),
                loyaltydiscount_incl_tax=Decimal('0'),
                total_discount_excl_tax=Decimal('0'),
                total_discount_incl_tax=Decimal('0'),
                total_surcharge_excl_tax=Decimal('0'),
                total_surcharge_incl_tax=Decimal('0'),
                total_excl_tax=tip,
                total_incl_tax=tip,
                total_tax=Decimal('0'),
                tax_class=None,
                tax_rate=Decimal('0'),
                tax_name="",
                sort_order=index)
            orderitem.set_options([])
            orderitem.put()
        # Create order items for fees
        for fee in fees:
            index += 1
            orderitem = OrderItem(parent=order, order=order,
                product=None,
                name=fee['name'],
                category_name=_('Fees'),
                quantity=1,
                base_price_excl_tax=fee['price_excl_tax'],
                base_price_incl_tax=fee['price_incl_tax'],
                loyaltydiscount_excl_tax=Decimal('0'),
                loyaltydiscount_incl_tax=Decimal('0'),
                total_discount_excl_tax=Decimal('0'),
                total_discount_incl_tax=Decimal('0'),
                total_surcharge_excl_tax=Decimal('0'),
                total_surcharge_incl_tax=Decimal('0'),
                total_excl_tax=fee['price_excl_tax'],
                total_incl_tax=fee['price_incl_tax'],
                total_tax=fee['tax'],
                tax_class=None,
                tax_rate=fee['tax_rate'],
                tax_name=_("VAT"),
                sort_order=index)
            orderitem.set_options([])
            orderitem.put()

        # Create and authorize payment
        total_rounded = self.round_two_decimals(total_incl_tax)
        total_tax_rounded = self.round_two_decimals(total_tax)
        if payment_method == 'nordpay' and nordpay_authorization:
            payment = NordpayPayment(order=order, amount=total_rounded, method=nordpay_method)
            payment.set_authorized(nordpay_authorization, total_rounded, self.merchant.currency)
        elif payment_method == 'nordpay' or payment_method == 'epay_saved_card':
            payment = consumer.current_payment_subscription.initialize_payment(order, total_rounded)
            payment.authorize()
        elif payment_method == '4t':
            payment = consumer.initialize_4t_payment(order, total_rounded, total_tax_rounded)
            payment.authorize()
        elif payment_method == 'epay_new_card':
            payment = consumer.initialize_epay_payment(order, total_rounded, total_tax_rounded, False)
            # Don't authorize - authorization is done by epay callback to api/epay.py
        elif payment_method == 'epay_mobilepay':
            payment = consumer.initialize_epay_payment(order, total_rounded, total_tax_rounded, True)
            # Don't authorize - authorization is done by epay callback to api/epay.py
        elif payment_method == 'mobilepay_appswitch':
            payment = consumer.initialize_mobilepay_appswitch_payment(order, total_rounded, total_tax_rounded)
            # Don't authorize - authorization is done by callback

        # Nordpay is auhorized immediately, 4T and ePay authorization has to wait for confirmation
        if payment.status == 'authorized':
            order.status = 'pending'
        order.payment = payment

        # Auto prepare bar orders if enabled
        if order.status == 'pending' and self.bar and self.merchant.auto_prepare:
            order.status = 'preparing'
            
        order.put()
        
        # Update order module sessions
        Order.update_processing_sessions(order)

        # Update consumer address if given
        if self.service == 'delivery':
            for field in ['name', 'phone', 'address1', 'address2', 'postcode', 'city']:
                setattr(consumer, field, getattr(self, 'delivery_'+field))
                consumer.country = self.merchant.country
        # Update consumer loyalty code if loyalty program available
        if self.merchant.has_loyalty_program:
            consumer.set_loyalty_code(self.merchant.key(), self.loyalty_code)
        # Save consumer
        if self.service == 'delivery' or self.merchant.has_loyalty_program:
            consumer.put()
        
        return order
        
    def round_two_decimals(self, number):
        twoplaces = Decimal(10) ** -2
        return Decimal(number).quantize(twoplaces)