from decimal import Decimal
from google.appengine.ext import db
import wtforms
from webapp2_extras import i18n
import babel
import logging

class DecimalProperty(db.Property):
    """
    Allows Python's decimal.Decimal type to be stored in the datastore as an
    integer.  Takes care of putting the decimal point in the right place.
    """
    data_type = Decimal

    def __init__(self, dec_places, verbose_name=None, name=None, default=None,
                 required=False, validator=None, choices=None, indexed=True):
        super(DecimalProperty, self).__init__(verbose_name, name, default,
            required, validator, choices, indexed)
        self.__quantize_exp = Decimal('10') ** -dec_places
        self.__store_mul = Decimal('10') ** dec_places

    def get_value_for_datastore(self, model_inst):
        dec = super(DecimalProperty, self).get_value_for_datastore(model_inst)
        if dec is None:
            return None

        dec = dec.quantize(self.__quantize_exp)
        return int(dec * self.__store_mul)

    def make_value_from_datastore(self, value):
        if value is None:
            return None

        return Decimal(value) / self.__store_mul

    def validate(self, value):
        if value is not None and not isinstance(value, Decimal):
            raise db.BadValueError("Property %s must be a Decimal or string." % self.name)
        return super(DecimalProperty, self).validate(value)

    def empty(self, value):
        return (value is None)
        
class DecimalListPropertyField(wtforms.fields.TextAreaField):
    __store_mul = Decimal('10') ** 2
    __quantize_exp = Decimal('10') ** -2

    """
    A field for ``db.ListProperty(long)`` where long holds decimal values. 
    The list items are rendered in a textarea.
    """
    def _value(self):
        if self.raw_data:
            return self.raw_data[0]
        else:
            return self.data and unicode("\n".join(map(self.make_value_from_datastore, self.data))) or u''

    def process_formdata(self, valuelist):
        if valuelist:
            try:
                self.data = map(self.get_value_for_datastore, valuelist[0].splitlines())
            except ValueError:
                raise ValueError(self.gettext(u'Not a valid list'))

    def make_value_from_datastore(self, value):
        if value is None:
            return None
        return str(Decimal(value) / self.__store_mul)

    def get_value_for_datastore(self, value):
        dec = Decimal(value)
        dec = dec.quantize(self.__quantize_exp)
        return int(dec * self.__store_mul)
        
class LocalizedDecimalField(wtforms.fields.DecimalField):
    def _value(self):
        if self.data is not None:
            return i18n.format_decimal(self.data)
        else:
            return u''
            
    def process_formdata(self, valuelist):
        if valuelist:
            try:
                valuelist[0] = i18n.parse_decimal(valuelist[0])
            except babel.numbers.NumberFormatError:
                raise ValueError(self.gettext(u'Not a valid decimal value'))
            super(LocalizedDecimalField, self).process_formdata(valuelist)
            
    def get_pattern(self):
        locale = i18n.get_i18n().locale
        decimal_sep = babel.numbers.get_decimal_symbol(locale)
        thousands_sep = babel.numbers.get_group_symbol(locale)
        # 1-3 digits, followed by any number of 3-digit groups (optionally preceded by thousands separator),
        # then optionally a group of digits preceded by decimal separator
        return "\d{1,3}(\%s?\d{3})*(\%s\d+)?" % (thousands_sep, decimal_sep)