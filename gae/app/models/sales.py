#!/usr/bin/python
# -*- coding: utf-8 -*-
from google.appengine.ext import db
from google.appengine.api import channel, taskqueue
from decimalproperty import *
from referenceproperty import ReferenceProperty
from decimal import Decimal
from catalog import Product
from consumers import Consumer
from merchants import Merchant, MerchantInvoice, Location, Bar, TaxClass
from payment import Payment, EpayPayment, FourTPayment, MobilePayAppSwitchPayment
from models import i18ndata
import datetime
import json
import pickle
import cgi
import pytz
import string
import logging
import webapp2
from xml.sax.saxutils import escape, unescape
from webapp2_extras import i18n
_ = i18n.gettext
    
class Order(db.Model):
    """
    Orders are placed by consumers shopping for menu items at a bar,
    or tickets at a location. Menu orders are processed from the bar.
    """
    
    merchant = ReferenceProperty(Merchant, collection_name='orders')
    consumer = ReferenceProperty(Consumer, collection_name='orders')
    location = ReferenceProperty(Location, collection_name='orders')
    bar = ReferenceProperty(Bar, collection_name='orders')
    # Order statuses and the possible transitions between statuses
    status = db.StringProperty(default='pending_payment',choices=['pending_payment', 'pending', 'preparing', 'complete', 'cancelled'])
    transitions = {'pending_payment': ['pending', 'preparing', 'cancelled'], 'pending': ['preparing', 'cancelled', 'complete'], 'preparing' : ['pending', 'complete', 'cancelled'], 'complete': [], 'cancelled': []}
    date_created = db.DateTimeProperty(auto_now_add=True)
    date_completed = db.DateTimeProperty()
    date_modified = db.DateTimeProperty(auto_now=True)
    date_delivery = db.DateTimeProperty()
    order_number = db.IntegerProperty()
    comment = db.TextProperty()
    currency = db.StringProperty()
    payment = db.ReferenceProperty(Payment)
    service = db.StringProperty(choices=['counter', 'table', 'entrance', 'delivery'])
    table_number = db.StringProperty()
    to_go = db.BooleanProperty(default=False)
    total_excl_tax = DecimalProperty(2, default=Decimal('0.00'))
    total_incl_tax = DecimalProperty(2, default=Decimal('0.00'))
    total_loyalty_discount_incl_tax = DecimalProperty(2, default=Decimal('0.00'))
    total_loyalty_discount_excl_tax = DecimalProperty(2, default=Decimal('0.00'))
    total_tax = DecimalProperty(2, default=Decimal('0.00'))
    payment_fee = DecimalProperty(2, default=Decimal('0.00'))
    merchant_invoice = db.ReferenceProperty(MerchantInvoice, collection_name='orders')
    merchant_name = db.StringProperty()
    location_name = db.StringProperty()
    bar_name = db.StringProperty()
    timezone = db.StringProperty(choices=pytz.common_timezones,default='Europe/Copenhagen')
    locale = db.StringProperty()
    merchant_country = db.StringProperty(default='DK')
    event_name = db.StringProperty(default='')
    event_time_label = db.StringProperty()
    delivery_phone = db.StringProperty()
    delivery_name = db.StringProperty()
    delivery_address1 = db.StringProperty()
    delivery_address2 = db.StringProperty()
    delivery_postcode = db.StringProperty()
    delivery_city = db.StringProperty()
    delivery_country = db.StringProperty()
    loyalty_code = db.StringProperty()
    delivery_instructions = db.TextProperty()

    _order_items_cache = None
    
    API_PROPERTIES_SPARSE = ['id', 'order_number', 'queue_number', 'status', 'status_label', 'date_created', 'date_created_formatted', 'date_completed_formatted', 'date_delivery_formatted', 'merchant_name', 'location_name', 'bar_name', 'custom_allowed_payment_methods', ('merchant', ['image_url', 'entrance_pincode', 'serve_with_consumer_device'])]
    API_PROPERTIES_COMPLETE = API_PROPERTIES_SPARSE + ['consumer_has_email', 'comment', 'delivery_instructions', 'currency', 'service', 'table_number', 'total_excl_tax_formatted', 'total_incl_tax_formatted', 'total_tax_formatted', 'total_tax_grouped', 'total_loyalty_discount_incl_tax', 'total_loyalty_discount_incl_tax_formatted', 'status_title_for_consumer', 'status_description_for_consumer', 'company_name', 'company_vat', 'estimated_waiting_time', 'custom_allowed_payment_methods', 'event_name', 'event_time_label', 'date_delivery_formatted', ('items', ['name', 'quantity', 'total_excl_tax_formatted', 'total_incl_tax_formatted', 'total_before_loyalty_discount_incl_tax_formatted', 'total_tax_formatted', 'option_values'])]
        
    @property
    def id(self):
        return str(self.key().id())
    @property 
    def consumer_has_email(self):
        return self.consumer.email and len(self.consumer.email) > 0

    @property
    def total_loyalty_discount_incl_tax_formatted(self):
        return self.format_currency(self.total_loyalty_discount_incl_tax)

    @property
    def total_loyalty_discount_excl_tax_formatted(self):
        return self.format_currency(self.total_loyalty_discount_excl_tax)

    def get_age(self):
        diff = datetime.datetime.now() - self.date_created
        return int(diff.total_seconds()/60)
        
    def get_late(self):
        if self.date_delivery:
            diff = datetime.datetime.now() - self.date_delivery
        else:
            diff = datetime.datetime.now() - self.date_created
        return int(diff.total_seconds()/60)
        
    def is_real_time_order(self):
        return self.event_name == '' or self.event_name == None

    def get_completed_age(self):
        if (self.date_completed):
            diff = datetime.datetime.now() - self.date_completed
            return int(diff.total_seconds()/60)
        else:
            return 0
            
    def get_sort_age(self):
        date = self.date_delivery if self.date_delivery else self.date_created
        if (self.status == 'complete'):
            date = self.date_completed
        diff = datetime.datetime.now() - date
        return int(diff.total_seconds())
        
    def get_queue_number(self):
        # Last 3 digits of order number
        ordernumber = str(self.order_number)
        return ordernumber[-3:].zfill(3)
        
    @property
    def queue_number(self):
        return self.get_queue_number()
    
    # escape() and unescape() takes care of &, < and >.
    html_unescape_table = {
        "&aelig;" : u'æ',
        "&Aelig;" : u'Æ',
        "&oslash;" : u'ø',
        "&Oslash;" : u'Ø',
        "&aring;" : u'å',
        "&Aring;" : u'Å'
    }
    def html_unescape(self, text):
        return unescape(text, self.html_unescape_table)
        
    def to_json(self):
        """ Order data for the service module """        
        data = {
            'bar': self.bar_name, 
            'status': self.status, 
            'queue_number': self.get_queue_number(), 
            'to_go' : self.to_go , 
            'order_number': self.order_number, 
            'age': self.get_age(), 
            'late': self.get_late(), 
            'delivery_date': self.format_delivery_date(), 
            'completed_age': self.get_completed_age(), 
            'sort_age': self.get_sort_age(), 
            'time_created': self.format_time(self.date_created, format="short"), 
            'time_completed': self.format_time(self.date_completed, format="short") if self.date_completed else None, 
            'key': str(self.key()), 'id': self.key().id(), 
            'comment': (cgi.escape(self.comment) if self.comment else None), 
            'loyalty_code': (self.html_unescape(self.loyalty_code) if self.loyalty_code else None), 
            'service': self.service, 
            'table_number': self.table_number, 
            'total_incl_tax': self.format_currency(self.total_incl_tax), 
            'items_length': len(self.items), 
            'total_quantity': sum([item.quantity for item in self.items]), 
            'total_loyalty_discount_incl_tax': str(self.total_loyalty_discount_incl_tax),
            'total_loyalty_discount_incl_tax_formatted': self.format_currency(-self.total_loyalty_discount_incl_tax),
            'total_loyalty_discount_excl_tax_formatted': self.format_currency(-self.total_loyalty_discount_excl_tax),
            'event_name': self.event_name, 
            'event_time_label': self.event_time_label, 
            'delivery_name': self.delivery_name, 
            'delivery_address1': self.delivery_address1, 
            'delivery_address2': self.delivery_address2, 
            'delivery_postcode': self.delivery_postcode, 
            'delivery_city': self.delivery_city, 
            'delivery_phone': self.delivery_phone, 
            'delivery_address_formatted': self.delivery_address_formatted, 
            'delivery_address_html': self.delivery_address_html, 
            'delivery_instructions': (cgi.escape(self.delivery_instructions) if self.delivery_instructions else None), 
            'items': [] 
        }
        for item in self.items:
            options = []
            
            product_key = OrderItem.product.get_value_for_datastore(item)
            if product_key is None:
                product_key = ""
            else:
                product_key = str(product_key)


            for option in item.get_options():
                options.append({
                    'option_id' : option['option_id'], 
                    'option_name': cgi.escape(option['option_name']), 
                    'values': [cgi.escape(value['value_label']) for value in option['values']]
                })
            data['items'].append({
                'product_key' : product_key,
                'key' : str(item.key()), 
                'name' : cgi.escape(item.name), 
                'category_name' : cgi.escape(item.category_name), 
                'quantity': item.quantity, 
                'options': options, 
                'total_incl_tax': self.format_currency(item.total_incl_tax+item.loyaltydiscount_incl_tax)
            })
        return data
        
    def change_status_nosave(self, status, invoice):
        """
        This is the core order processing function. It will move the order to
        the given status if allowed, and will process payments if it is required
        by the transition.
        If the order is completed the order will be attached to the current
        open merchant invoice (which may be given as parameter), or a new
        merchant invoice will be generated if no open exists.
        """
        # If status transition is allowed
        if (status in self.transitions[self.status]):
            # Update order status
            self.status = status
            if (self.status == 'complete'):
                self.date_completed = datetime.datetime.now()
                self.merchant_invoice = invoice if invoice else self.merchant.get_open_invoice()
            return True
        else:
            return False

    def complete_status(self):
        payment = self.payment
        self.process_payment(payment, self.status)
        if (self.status == 'complete'):
            self.payment_fee = payment.fee
            for item in self.items:
                item.date_completed = self.date_completed
                item.merchant_invoice = Order.merchant_invoice.get_value_for_datastore(self)
                item.put()
            self.record_analytics()

    def change_status(self, status, invoice=None, update_session=True, is_consumer_request=False):
        """
        This is the core order processing function. It will move the order to
        the given status if allowed, and will process payments if it is required
        by the transition.
        If the order is completed the order will be attached to the current
        open merchant invoice (which may be given as parameter), or a new
        merchant invoice will be generated if no open exists.
        Completed orders will be recorded in Google Analytics.
        Service screens will be notified about the change though the channel API.
        """
        order = db.get(self.key())
        oldstatus = order.status
        logging.info("changestatus: oldstatus " + oldstatus)
        
        # If status transition is allowed
        if (status in self.transitions[oldstatus]):
            if is_consumer_request:
                if status == 'cancelled' and oldstatus == 'preparing':
                    return False
                if status == 'cancelled' and oldstatus == 'complete':
                    return False

            # Process payment
            self.process_payment(order.payment, status)

            # Update order status
            order.status = status
            if (status == 'complete'):
                order.date_completed = datetime.datetime.now()
                self.date_completed = order.date_completed
                order.payment_fee = order.payment.fee
                self.payment_fee = order.payment_fee
                order.merchant_invoice = invoice if invoice else order.merchant.get_open_invoice()
                self.merchant_invoice = order.merchant_invoice
                for item in order.items:
                    item.date_completed = order.date_completed
                    item.merchant_invoice = order.merchant_invoice
                    item.put()
            order.put()
            self.status = status

            if order.service != 'entrance' and update_session:
                Order.update_processing_sessions(order)
                
            if status == 'complete':
                order.record_analytics()
            
            return True
        else:
            return False

    def record_analytics(self):
        taskqueue.add(url='/tasks/recordanalytics', params={'order_key': str(self.key())}, retry_options=taskqueue.TaskRetryOptions(task_retry_limit=1))

    def process_payment(self, payment, status):
        try:
            if status == 'cancelled' and payment.can_void():
                payment.void()
            if status == 'complete' and payment.can_capture():
                payment.capture()
        except Exception as e:
            logging.warning(e)
            
    @staticmethod
    def update_processing_sessions(order):
        # Update serve page
        session_threshold = datetime.datetime.utcnow() - datetime.timedelta(hours=3)
        if order.bar:
            sessions = order.bar.order_processing_sessions.\
                    filter('connected', True).\
                    filter("date_created >", session_threshold).\
                    order('-date_created').fetch(limit=20)

            for session in sessions:
                taskqueue.add(
                        url='/tasks/updatesession',
                        params={'order_key': str(order.key()),
                                'client_id': str(session.key().name()) },
                        retry_options=taskqueue.TaskRetryOptions(task_retry_limit=1))
    
    @staticmethod
    def update_processing_sessions_multiple_orders(orders, bar):
        # Update serve page
        session_threshold = datetime.datetime.utcnow() - datetime.timedelta(hours=3)
        if bar is not None:
            session_keys = bar.order_processing_sessions.\
                    filter('connected', True).\
                    filter("date_created >", session_threshold).\
                    order('-date_created').fetch(limit=20, keys_only=True)

            data = {
                    'remove_orders' : [],
                    'insert' : [],
                    }
            for order in orders:
                data['remove_orders'].append([
                    str(order.key()),
                    str(order.status),
                    str(order.queue_number)])

                if (order.status in ['pending', 'preparing', 'complete'] and \
                        order.service in ['counter', 'table', 'delivery']):
                    data['insert'].append(order.to_json())

            taskqueue.add(
                    queue_name='channelmsg',
                    url='/tasks/updatesession_multiple_orders',
                    params={'data': json.dumps(data),
                            'session_keys': json.dumps([str(session_key) for session_key in session_keys])},
                    retry_options=taskqueue.TaskRetryOptions(task_retry_limit=1))
            
    def get_status_label(self):
        status_labels = {
            'pending_payment': _('Pending payment'),
            'pending': _('Pending'), 
            'preparing': _('Preparing'), 
            'complete': _('Complete'), 
            'cancelled': _('Cancelled')}
        return status_labels[self.status]
        
    @property
    def status_label(self):
        return self.get_status_label()
        
    def get_total(self, incl_tax=None):
        if (incl_tax == None):
            incl_tax = self.merchant_prices_include_tax
        total = 0
        for item in self.items:
            total += item.total_incl_tax if incl_tax else item.total_excl_tax
        return total

    def get_total_tax(self):
        total = 0
        for item in self.items:
            total += item.total_tax
        return total

    def get_total_tax_grouped(self):
        tax_totals = {}
        for item in self.items:
            if item.tax_rate > 0:
                tax_class_key = str(item.properties()['tax_class'].get_value_for_datastore(item))
                if tax_class_key and tax_class_key not in tax_totals:
                    tax_totals[tax_class_key] = { 'name': item.tax_name, 'rate': item.tax_rate, 'total': Decimal('0') }
                tax_totals[tax_class_key]['total'] += item.total_tax
        return tax_totals
        
    def format_date(self, datetime, format=None):
        if self.timezone:
            datetime = datetime.replace(tzinfo=pytz.utc).astimezone(pytz.timezone(self.timezone))
            return i18n.format_date(date=datetime, format=format, rebase=False)
        else:
            return i18n.format_date(date=datetime, format=format, rebase=True)
            
    def format_datetime(self, datetime, format=None):
        if self.timezone:
            datetime = datetime.replace(tzinfo=pytz.utc).astimezone(pytz.timezone(self.timezone))
            return i18n.format_datetime(datetime=datetime, format=format, rebase=False)
        else:
            return i18n.format_datetime(datetime=datetime, format=format, rebase=True)
            
    def format_time(self, datetime, format=None):
        if self.timezone:
            datetime = datetime.replace(tzinfo=pytz.utc).astimezone(pytz.timezone(self.timezone))
            return i18n.format_time(time=datetime, format=format, rebase=False)
        else:
            return i18n.format_time(time=datetime, format=format, rebase=True)
            
    def format_delivery_date(self):
        now = datetime.datetime.now()
        delivery_date = self.date_delivery if self.date_delivery else self.date_created
        #if now.date() == delivery_date.date():
        #    return self.format_time(delivery_date, "short")
        #else:
        #    return self.format_datetime(delivery_date, "short")
        return self.format_datetime(delivery_date, "short")


    def format_currency(self, amount):
        return i18n.format_currency(amount, self.currency)
        
    @property
    def date_created_formatted(self):
        return self.format_datetime(self.date_created)
        
    @property
    def date_completed_formatted(self):
        return self.format_datetime(self.date_completed)

    @property
    def date_delivery_formatted(self):
        return self.format_datetime(self.date_delivery, "short")
        
    @property
    def status_title_for_consumer(self):
        if self.status == 'pending':
            return _("Thank you for your order")
        elif self.status == 'preparing':
            return _("Your order is being prepared")
        elif self.status == 'pending_payment' and self.payment and isinstance(self.payment, FourTPayment):
            return _("Approve payment in the Paii-app")
        elif self.status == 'pending_payment' and self.payment and isinstance(self.payment, EpayPayment):
            return _("Waiting for payment") # Waiting for ePay callback. The ePay callback will under normal circumstances be called very quickly
        # and then the user will not see this status.
        elif self.status == 'pending_payment' and self.payment and isinstance(self.payment, MobilePayAppSwitchPayment):
            return _("Waiting for payment") # Waiting for callback.
        elif self.status == 'pending_payment' and not self.payment:
            logging.error("Pending payment, no 'Payment' object!  Order key: " + str(self.key()))
            return _("Pending payment, no 'Payment' object!")
        return None
        
    @property
    def status_description_for_consumer(self):
        if self.service in ['table', 'counter', 'delivery'] and self.status == 'pending':
            return _("We will notify you when your order starts being prepared.")
        elif self.service == 'table' and self.status == 'preparing' and self.bar and self.bar.get_notification('table_preparing'):
            return self.bar.get_notification('table_preparing')
        elif self.service == 'counter' and self.status == 'preparing' and self.bar and self.bar.get_notification('counter_preparing'):
            return self.bar.get_notification('counter_preparing')
        elif self.service == 'delivery' and self.status == 'preparing' and self.bar and self.bar.get_notification('delivery_preparing'):
            return self.bar.get_notification('delivery_preparing')
        elif self.status == 'pending_payment' and self.payment and isinstance(self.payment, FourTPayment):
            return _("Open the Paii-app and approve the payment from Zerved")
        elif self.status == 'pending_payment' and self.payment and isinstance(self.payment, EpayPayment):
            return _("Your payment is being processed.") # Waiting for ePay callback. The ePay callback will under normal circumstances be called very quickly
        # and then the user will not see this status.
        elif self.status == 'pending_payment' and self.payment and isinstance(self.payment, MobilePayAppSwitchPayment):
            return _("Your payment is being processed.")
        elif self.status == 'pending_payment' and not self.payment:
            logging.error("The order is pending payment but there is no associated 'Payment' object! Order key: " + str(self.key()))
            return _("The order is pending payment but there is no associated 'Payment' object!")
        elif self.service == 'entrance':
            return _("Please show this screen to the entrance staff.")
        return None
        
    @property
    def total_excl_tax_formatted(self):
        return self.format_currency(self.total_excl_tax)
        
    @property
    def total_incl_tax_formatted(self):
        return self.format_currency(self.total_incl_tax)
        
    @property
    def total_loyalty_excl_tax_formatted(self):
        return self.format_currency(self.total_loyalty_excl_tax)
    
    @property    
    def total_loyalty_incl_tax_formatted(self):
        return self.format_currency(self.total_loyalty_incl_tax)
        
    @property
    def total_tax_formatted(self):
        return self.format_currency(self.total_tax)
        
    @property
    def total_tax_grouped(self):
        return [{'name': tax_total['name'], 'rate': tax_total['rate'], 'total_formatted': self.format_currency(tax_total['total'])} for key, tax_total in self.get_total_tax_grouped().items()]
        
    @property
    def items(self):
        """
        Get the order items from cache or query

        """
        if self._order_items_cache is None:
            self._order_items_cache = [
                    order_item for order_item in OrderItem.all().\
                    ancestor(self).order('sort_order').run(batch_size=10)
                    ]
        return self._order_items_cache

    @items.setter
    def items(self, order_items):
        self._order_items_cache = order_items
        
    def get_company_data(self, key):
        return i18ndata.COMPANY_DATA[self.merchant_country][key]
        
    @property
    def company_name(self):
        return self.get_company_data('name')
        
    @property
    def company_vat(self):
        return self.get_company_data('vat')

    @property
    def delivery_address_formatted(self):
        if self.delivery_country in i18ndata.ADDRESS_FORMATS:
            addressformat = i18ndata.ADDRESS_FORMATS[self.delivery_country]
            return string.Template(addressformat).substitute(name=self.delivery_name, address1=self.delivery_address1, address2=self.delivery_address2, city=self.delivery_city, postcode=self.delivery_postcode, phone=self.delivery_phone).replace("\n\n", "\n")
        return ""

    @property
    def delivery_address_html(self):
        return self.delivery_address_formatted.replace("\n", "<br/>")

    @property
    def estimated_waiting_time(self):
        if self.service in ['counter', 'table'] and self.status in ['pending', 'preparing'] and self.bar:
            return self.bar.estimated_waiting_time
        elif self.service == 'delivery' and self.status in ['pending', 'preparing'] and self.bar:
            return self.bar.estimated_waiting_time_delivery
        return 0

    @staticmethod
    def prefetch_merchants(orders):
        ref_keys = [Order.merchant.get_value_for_datastore(order) for order in orders]
        ref_keys = [r for r in ref_keys if r != None]
        merchantmap = dict((x.key(), x) for x in db.get(set(ref_keys)) if x)
        for order in orders:
            merchant_key = Order.merchant.get_value_for_datastore(order)
            if merchant_key and merchant_key in merchantmap:
                Order.merchant.__set__(order, merchantmap[merchant_key])
        return orders

    @property
    def processing_time(self):
        if self.date_created and self.date_completed:
            diff = (self.date_completed - self.date_created)
            days = diff.days
            hours, remainder = divmod(diff.seconds, 3600)
            minutes, seconds = divmod(remainder, 60)
            if days > 0:
                return '%sd %sh %sm %ss' % (days, hours, minutes, seconds)
            elif hours > 0:
                return '%sh %sm %ss' % (hours, minutes, seconds)
            elif minutes > 0:
                return '%sm %ss' % (minutes, seconds)
            else:
                return '%ss' % seconds
        else:
            return None
    
class OrderItem(db.Model):
    """ The order lines in an order """
    
    order = db.ReferenceProperty(Order)
    product = ReferenceProperty(Product)
    name = db.StringProperty()
    category_name = db.StringProperty(default='')
    quantity = db.IntegerProperty()
    base_price_excl_tax = DecimalProperty(2, default=Decimal('0.00'))
    base_price_incl_tax = DecimalProperty(2, default=Decimal('0.00'))
    loyaltydiscount_excl_tax = DecimalProperty(2, default=Decimal('0.00'))
    loyaltydiscount_incl_tax = DecimalProperty(2, default=Decimal('0.00'))
    total_discount_excl_tax = DecimalProperty(2, default=Decimal('0.00'))
    total_discount_incl_tax = DecimalProperty(2, default=Decimal('0.00'))
    total_surcharge_excl_tax = DecimalProperty(2, default=Decimal('0.00'))
    total_surcharge_incl_tax = DecimalProperty(2, default=Decimal('0.00'))
    total_excl_tax = DecimalProperty(2, default=Decimal('0.00')) # Total = Base price * quantity - discount + surcharge
    total_incl_tax = DecimalProperty(2, default=Decimal('0.00'))
    total_tax = DecimalProperty(2, default=Decimal('0.00'))
    tax_class = ReferenceProperty(TaxClass)
    tax_rate = DecimalProperty(4, default=Decimal('0.0000'))
    tax_name = db.StringProperty()
    options = db.BlobProperty()
    merchant_invoice = db.ReferenceProperty(MerchantInvoice, collection_name='order_items') # Copied from order
    date_completed = db.DateTimeProperty() # Copied from order
    sort_order = db.IntegerProperty(default=0)
    
    def get_options(self):
        return pickle.loads(str(self.options))
        
    def set_options(self, options):
        self.options = pickle.dumps(options, 2)
        
#    @property
#    def loyaltydiscount_excl_tax_formatted(self):
#        return self.order.format_currency(self.loyaltydiscount_excl_tax)
    
#    @property    
#    def loyaltydiscount_incl_tax_formatted(self):
#        return self.order.format_currency(self.loyaltydiscount_incl_tax)
        
    @property
    def total_excl_tax_formatted(self):
        return self.order.format_currency(self.total_excl_tax)
    
    @property    
    def total_incl_tax_formatted(self):
        return self.order.format_currency(self.total_incl_tax)
        
    @property
    def total_before_loyalty_discount_incl_tax_formatted(self):
        return self.order.format_currency(self.total_incl_tax + self.loyaltydiscount_incl_tax)

    @property
    def total_tax_formatted(self):
        return self.order.format_currency(self.total_tax)
        
    @property
    def option_values(self):
        return self.get_options();
        #option_values = []
        #for option in self.get_options():
        #    option_values.append({'name': option['option_name'], 'values': option['values'] })
        #return option_values
