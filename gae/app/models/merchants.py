from google.appengine.ext import db, blobstore
from google.appengine.api import memcache, channel, search, images, users, datastore_errors
from decimalproperty import *
from referenceproperty import ReferenceProperty
from geo.geomodel import GeoModel
from models import i18ndata
from models.front.news import News
from models.consumers import Consumer
from webapp2_extras import i18n
import pytz
import datetime
import counter
from itertools import groupby
from webapp2_extras import i18n
_ = i18n.gettext
import logging
import firezerved

from collections import defaultdict


class Merchant(GeoModel):
    """
    A merchant represents a seller in Zerved
    Extends GeoModel to store merchant's location
    All product and order data is associated with a merchant
    """
    
    name = db.StringProperty(required=True)
    #open = db.BooleanProperty(default=False) # Deprecated
    prices_include_tax = db.BooleanProperty(default=True)
    #default_tax_class = db.ReferenceProperty()
    users = db.StringListProperty()
    image = db.BlobProperty()
    image_type = db.StringProperty()
    currency = db.StringProperty(choices=i18ndata.ALLOWED_CURRENCIES)
    counter_service = db.BooleanProperty(default=True) # Deprecated
    table_service = db.BooleanProperty(default=False) # Deprecated
    address = db.TextProperty()
    vat_number = db.StringProperty()
    timezone = db.StringProperty(choices=pytz.common_timezones,default='Europe/Copenhagen')
    entrance_pincode = db.StringProperty()
    serve_with_consumer_device = db.BooleanProperty(default=False)
    bank_account = db.StringProperty()
    enabled = db.BooleanProperty(default=True) # Deprecated
    country = db.StringProperty(choices=i18ndata.ALLOWED_COUNTRIES)
    service_charge = DecimalProperty(2, default=Decimal('0.00'))
    auto_prepare = db.BooleanProperty(default=False)
    has_loyalty_program = db.BooleanProperty(default=False)
    loyalty_program_name = db.StringProperty()
    loyalty_program_rate = DecimalProperty(2, default=Decimal('0.00'))
    fee_rate = DecimalProperty(2, default=Decimal('0.00'))
    fee_rate_use_default = db.BooleanProperty(default=True)
    custom_allowed_payment_methods_enabled = db.BooleanProperty(default=False)
    custom_allowed_payment_method_epay = db.BooleanProperty(default=False)
    custom_allowed_payment_method_swipp = db.BooleanProperty(default=False)
    custom_allowed_payment_method_epaymobilepay = db.BooleanProperty(default=False)
    custom_allowed_payment_method_mobilepay_appswitch = db.BooleanProperty(default=False)
    custom_allowed_payment_method_mobilepay_appswitch_embedded = db.BooleanProperty(default=False)

    pricetier = db.ReferenceProperty()

    phonenr = db.StringProperty() # Merchant's phone number (contact purpose)
    email = db.StringProperty() # Email address to contact the merchant
    receive_news = db.BooleanProperty(default=True) # Boolean property to receive or not emails from administrator (Important news don't take this into account)

    DEFAULT_FEE_RATE = Decimal('10.50')
    
    # Notifications
    # Deprecated
    notif_counter_preparing_use_default = db.BooleanProperty(default=True)
    notif_counter_complete_use_default = db.BooleanProperty(default=True)
    notif_table_preparing_use_default = db.BooleanProperty(default=True)
    notif_counter_preparing = db.StringProperty()
    notif_counter_complete = db.StringProperty()
    notif_table_preparing = db.StringProperty()
    
    API_PROPERTIES = ['name', 'open', 'prices_include_tax', 'currency', 'counter_service', 'table_service', 'address', 'vat_number', 'timezone', 'latitude', 'longitude', 'image_url', 'has_entrance_categories', 'has_menu_categories', 'country', 'mandatory_service_charge', 'tip_enabled']
    
    def reserve_order_number(self):
        countername = "m_"+str(self.key().id())+"_order"
        return counter.increment_and_get_count(countername)
        
    def get_orders_with_status(self, status):
        return self.orders.filter('status =', status)
        
    def validate_permission(self, object):
        return object.merchant.key() == self.key()
        
    # Deprecated
    def is_open(self):
        """ Merchant is open for orders if one or more serve screens (processing sessions) are open """
        open = memcache.get('merchantopen_'+str(self.key().id()))
        if open is None:
            sessions = self.order_processing_sessions.filter('connected =', True).fetch(1)
            open = (self.enabled and (self.counter_service or self.table_service) and len(sessions) > 0)
            memcache.set('merchantopen_'+str(self.key().id()), open, 60*10)
        return open
        
    # Deprecated
    @property
    def open(self):
        return self.is_open()
        
    def format_date(self, datetime, format=None):
        if self.timezone:
            datetime = datetime.replace(tzinfo=pytz.utc).astimezone(pytz.timezone(self.timezone))
            return i18n.format_date(date=datetime, format=format, rebase=False)
        else:
            return i18n.format_date(date=datetime, format=format, rebase=True)
            
    def format_datetime(self, datetime, format=None):
        if self.timezone:
            datetime = datetime.replace(tzinfo=pytz.utc).astimezone(pytz.timezone(self.timezone))
            return i18n.format_datetime(datetime=datetime, format=format, rebase=False)
        else:
            return i18n.format_datetime(datetime=datetime, format=format, rebase=True)

    def format_time(self, datetime, format=None):
        if self.timezone:
            datetime = datetime.replace(tzinfo=pytz.utc).astimezone(pytz.timezone(self.timezone))
            return i18n.format_time(time=datetime.time(), format=format, rebase=False)
        else:
            return i18n.format_time(time=datetime.time(), format=format, rebase=True)
            
    def format_currency(self, amount):
        return i18n.format_currency(amount, self.currency)
        
    def get_open_invoice(self):
        invoices = self.invoices.filter('open = ', True).fetch(limit=1)
        if len(invoices) > 0:
            return invoices[0]
        invoice = MerchantInvoice(merchant=self, merchant_name=self.name, merchant_address=self.address, merchant_vat_number=self.vat_number, merchant_bank_account=self.bank_account, currency=self.currency, merchant_country=self.country)
        invoice.put()
        return invoice
        
    # Deprecated
    @property
    def latitude(self):
        return self.location.lat
        
    # Deprecated
    @property
    def longitude(self):
        return self.location.lon
        
    def clear_open_cache(self):
        memcache.delete('merchantopen_'+str(self.key().id()))
        
    @property
    def has_entrance_categories(self):
        return len(self.categories.filter('group =', 'entrance').fetch(1)) > 0
        
    @property
    def has_menu_categories(self):
        return len(self.categories.filter('group =', 'menu').fetch(1)) > 0

    @property
    def has_bars(self):
        return len(Bar.all().filter('merchant = ', self).fetch(1)) > 0
        
    def get_notification(self, notification_type):
        """ Returns default message or custom message depending on use_default setting (custom message may be empty) """
        use_default = getattr(self, 'notif_'+notification_type+'_use_default')
        custom_message = getattr(self, 'notif_'+notification_type)
        return custom_message if not use_default else Bar.get_default_notifications()[notification_type]
        
    def get_fee_rate(self):
        return Merchant.DEFAULT_FEE_RATE if self.fee_rate_use_default else self.fee_rate
       

    def get_pricetier(self):
        try:
            return self.loaded_pricetier
        except AttributeError:
            if self.fee_rate_use_default == False and self.pricetier:
                self.loaded_pricetier = self.pricetier
            else:
                pricetier_k = db.Key.from_path('PriceTier', self.currency)
                self.loaded_pricetier = db.get(pricetier_k)
            return self.loaded_pricetier

    def get_fee_for_order(self, order_revenue):       
        pricetier = self.get_pricetier()
        min_values = pricetier.get_list("min_value");
        fees = pricetier.get_list("fee")

        for index, value in enumerate(min_values):
            if order_revenue < value:
                return fees[index-1]
            
        return fees[-1] #last element

    def get_flat_fee_for_order(self, order_revenue):
        pricetier = self.get_pricetier()
        flat_fee_min_values = pricetier.get_list("flat_fee_min_value");
        flat_fees = pricetier.get_list("flat_fee")

        if len(flat_fees) == 0:
            return Decimal('0.00')

        for index, value in enumerate(flat_fee_min_values):
            if order_revenue < value:
                return flat_fees[index-1]

        return flat_fees[-1] #last element

    def get_discount_percentage(self, n_orders):
        pricetier = self.get_pricetier()
        discount_min_values = pricetier.get_list("discount_min_value");
        discount_percentages = pricetier.get_list("discount_percentage")

        for index, value in enumerate(discount_min_values):
            if n_orders < value:
                return discount_percentages[index-1]
            
        return discount_percentages[-1] #last element      


    def get_sort_order(self, fee):
        fees = self.get_pricetier().get_list('fee');
        i = 3
        if fee in fees:
            for f in fees:
                if fee == f:
                    return i;
                i = i + 1
            return i
        else:
            return 3;

    def delete(self):
        for product in self.products:
            product.delete() # Will also delete product options
        db.delete(self.price_rules)
        db.delete(self.price_rule_schedules)
        db.delete(self.tax_classes)
        db.delete(self.categories)
        for location in self.locations:
            location.search_index_delete()
            location.delete() # Will also delete bars
        self.search_index_delete()
        cache_key = "locationsbycountrykey" + str(self.country)
        memcache.delete(cache_key)
        super(Merchant, self).delete()
  
    def put(self):
        super(Merchant, self).put()

        cache_key = "locationsbycountrykey" + str(self.country)
        memcache.delete(cache_key)

        self.search_index_save()

    def search_index_save(self):
        document = search.Document(doc_id=str(self.key()), fields=[
            search.TextField(name="key", value=str(self.key())),
            search.TextField(name="id", value=str(self.key().id())),
            search.TextField(name="name", value=self.name)
            ])
        try:
            search.Index(name="merchants").put(document)
        except search.Error:
            logging.exception("Merchant search index put failed")

    def search_index_delete(self):
        try:
            search.Index(name="merchants").delete(str(self.key()))
        except search.Error:
            logging.exception("Merchant search index deletion failed") 

    def get_locations(self):
        try:
            return self.loaded_locations
        except AttributeError:
            self.loaded_locations = self.locations.order('name').fetch(100)
            return self.loaded_locations

    def get_menus(self):
        try:
            return self.loaded_menus
        except AttributeError:
            self.loaded_menus = self.menus.fetch(100)
            return self.loaded_menus
            
    @property
    def tip_enabled(self):
        # The tip feature is disabled as no merchants are using it at the moment.
        return False
        #return self.service_charge == 0

    """
      Grouped bars by products example
      
      [
        [ Bar 1, Bar 3 ], <- group of 2 bars
        [ Bar 2 ],
        [ Bar 3, Bar 1 ]
        [ Bar 4 ]
      ]

      Bar 1 and Bar 3 have the same products. They are therefore in the same group.
    """
    def grouped_bars_by_products(self):
        grouped_bars = []
        bars = Bar.all().filter('merchant = ', self)
        for bar1 in bars:
            group = [bar1]
            for bar2 in bars:
                if bar1.key() != bar2.key():
                    if bar1.has_same_products(bar2):
                        group.append(bar2)
            grouped_bars.append(group)
        return grouped_bars

    def has_any_loyaltycodes(self):
        merchant_loyalty_codes = LoyaltyCode.all().filter('merchant = ',self)
        if (merchant_loyalty_codes.count() > 0):
            return True
        else:
            return False

    def is_loyaltycode_valid(self,code):
        merchant_loyalty_codes = LoyaltyCode.all().filter('merchant = ',self)
        if (merchant_loyalty_codes.count() > 0 and self.has_loyalty_program):
            loyalty_code = merchant_loyalty_codes.filter('code =', code)
            if (loyalty_code.count() > 0):
                logging.debug('Found loyalty_code:[%s]' % code)
                return True
            else:
                logging.debug('Did not find loyalty_code:[%s]' % code)
                return False
        else:
            #if no uploaded loyaltycodes (from CSV) then all codes are valid
            return True

    def register_loyaltycode_with_consumer(self, loyalty_code, consumer):
        merchant_loyalty_codes = LoyaltyCode.all().filter('merchant = ',self)
        if (merchant_loyalty_codes.count() > 0 and self.has_loyalty_program and loyalty_code):
            loyalty_code_query = merchant_loyalty_codes.filter('code =', loyalty_code)
            if (loyalty_code_query.count() > 0):
                logging.debug('register_loyaltycode_with_consumer: Found loyalty_code:%s' % loyalty_code)
                loyalty_obj = loyalty_code_query.get()
                
                #h24 = datetime.timedelta(hours=24)
                d7 = datetime.timedelta(hours=24*7)
                if consumer:
                    try:
                        loyalty_consumer = loyalty_obj.consumer
                    except datastore_errors.Error:
                        loyalty_consumer = None

                    if (loyalty_consumer is None):
                        loyalty_obj.consumer = consumer
                        loyalty_obj.put() #auto-sets last_used timestamp
                        logging.debug('Merchant.register_loyaltycode_with_consumer: consumer found, claiming the code. :)')
                        return True
                    # elif (loyalty_obj.consumer and loyalty_obj.consumer.device_token == consumer.device_token):
                    elif (loyalty_obj.consumer and loyalty_obj.consumer.is_equal_to(consumer)):
                        loyalty_obj.put() #auto-sets last_used timestamp
                        logging.debug('Merchant.register_loyaltycode_with_consumer: consumer found, consumer already has claimed the code. :)')
                        return True
                    elif (loyalty_obj.consumer and datetime.datetime.now() > loyalty_obj.last_used + d7):
                        loyalty_obj.consumer = consumer
                        loyalty_obj.put() #auto-sets last_used timestamp
                        logging.debug('Merchant.register_loyaltycode_with_consumer: consumer found, another consumer already has claimed and used the code, but more than 7 days ago, grab it. :)')
                        return True
                    else:
                        logging.debug('Merchant.register_loyaltycode_with_consumer: consumer:%s(%s) found, another consumer:%s(%s) already has claimed and used the code:%s, less than 7 days ago:%s :(' %(consumer, consumer.device_token, loyalty_obj.consumer, loyalty_obj.consumer.device_token, loyalty_code, loyalty_obj.last_used))
                        return False
                else:
                    logging.error('Merchant.register_loyaltycode_with_consumer: consumer not found!')
                    return False
            else:
                logging.debug('register_loyaltycode_with_consumer: Did not find loyalty_code:%s' % loyalty_code)
                return False
        else:
            #if no uploaded loyaltycodes (from CSV) then all codes are valid
            return True

class LoyaltyCode(db.Model):    
    """
    Single loyaltycode associated with a merchant
    """
    merchant = db.ReferenceProperty(Merchant, collection_name='loyalty_codes')
    code = db.StringProperty()
    last_used = db.DateTimeProperty(auto_now=True)
    consumer = db.ReferenceProperty(Consumer)

    def __str__(self):
        return self.code
            
class UserPrefs(db.Model):
    """ Settings for merchant users, identified by user ID from Users API """
    news_read = db.ListProperty(db.Key) # List of the news (keys) that the merchant has read

class TaxClass(db.Model):
    """
    Tax class (name and rate), used for products
    """
    
    merchant = db.ReferenceProperty(Merchant, collection_name='tax_classes')
    name = db.StringProperty()
    rate = DecimalProperty(4, default=0.0000)
    is_default = db.BooleanProperty(default=False)
    on_premises_only = db.BooleanProperty(default=False) # Only apply VAT on premises, do not apply for takeaway

    @staticmethod
    def get_default_tax_class(merchant):
        return TaxClass.all().filter('merchant =', merchant).filter('is_default =', True).get()
        
    @staticmethod
    def get_list_of_tuples(merchant):
        tax_classes = TaxClass.all().filter('merchant =', merchant)
        tax_class_list = []
        for tax_class in tax_classes:
            tax_class_list.append((tax_class.key(),tax_class.name))
        return tax_class_list
        
    def __str__(self):
        return self.name
        
class Location(GeoModel):
    merchant = db.ReferenceProperty(Merchant, collection_name='locations')
    name = db.StringProperty(required=True)
    enabled = db.BooleanProperty(default=True)
    images = db.ListProperty(blobstore.BlobKey)

    # Additional info
    info_panel_enabled = db.BooleanProperty(default=False)
    address = db.TextProperty()
    website_url = db.StringProperty()
    phone = db.StringProperty()
    phone2 = db.StringProperty()
    facebook = db.StringProperty()
    twitter = db.StringProperty()
    google_plus = db.StringProperty()
    linked_in = db.StringProperty()
    instagram = db.StringProperty()
    pinterest = db.StringProperty()
    flickr = db.StringProperty()
    opening_hours_monday = db.StringProperty()
    opening_hours_tuesday = db.StringProperty()
    opening_hours_wednesday = db.StringProperty()
    opening_hours_thursday = db.StringProperty()
    opening_hours_friday = db.StringProperty()
    opening_hours_saturday = db.StringProperty()
    opening_hours_sunday = db.StringProperty()
    
    API_PROPERTIES_COMPLETE = ['id', 'name', 'latitude', 'longitude', 'country', 'has_entrance_categories', 'number_of_entrance_categories', 'cover_urls',
        'info_panel_enabled', 'address', 'website_url', 'phone', 'phone2',
        'facebook', 'twitter', 'google_plus', 'linked_in', 'instagram', 'pinterest', 'flickr',
        'opening_hours_monday', 'opening_hours_tuesday', 'opening_hours_wednesday', 'opening_hours_thursday', 'opening_hours_friday', 'opening_hours_saturday', 'opening_hours_sunday', 
        ('merchant', 
            ['image_url', 'currency']), 
        ('bars', 
            ['id', 'name', 'open', 'counter_service', 'table_service', 'delivery_service', 'to_go', 'counter_service_to_stay', 'counter_service_to_go', 'enabled', 'estimated_waiting_time', 'estimated_waiting_time_delivery', 'delivery_min_order_amount', 'delivery_min_order_amount_formatted', 
            ('menus', 
                ['id', 'name']) 
            ]), 
        ('upcoming_events',
            ['name', 'time_label', 'date_start_formatted', 'date_delivery_formatted', 'date_start_date_formatted', 'date_delivery_date_formatted', 'date_start_time_formatted', 'date_delivery_time_formatted', 'open_for_orders']
            )]
    API_PROPERTIES_SPARSE = ['id', 'name', 'latitude', 'longitude', 'country', ('merchant', ['image_url'])]
    
    @property
    def id(self):
        return str(self.key().id())
        
    @property
    def country(self):
        return self.merchant.country
        
    @property
    def latitude(self):
        return self.location.lat
        
    @property
    def longitude(self):
        return self.location.lon
        
    @property
    def products(self):
        return Product.all().filter('enabled_locations =', self.key())
        
    @property
    def has_entrance_categories(self):
        return self.number_of_entrance_categories > 0
                
    @property
    def number_of_entrance_categories(self):
        return len(Category.get_categories_for_location(self))
        
    def __str__(self):
        return self.name
        
    def delete(self):
        for bar in self.bars:
            bar.search_index_delete()
        db.delete(self.bars)

        cache_key = "locationsbycountrykey" + str(self.country)
        memcache.delete(cache_key)

        self.search_index_delete()
        super(Location, self).delete()

    def put(self):
        super(Location, self).put()

        cache_key = "locationsbycountrykey" + str(self.country)
        memcache.delete(cache_key)

        if self.enabled:
            self.search_index_save()
        else:
            self.search_index_delete()

    def search_index_save(self):
        document = search.Document(doc_id=str(self.key()), fields=[
            search.TextField(name="id", value=str(self.key().id())),
            search.GeoField(name="location", value=search.GeoPoint(self.latitude, self.longitude)),
            search.TextField(name="name", value=self.name),
            search.TextField(name="merchant_key", value=str(self.merchant.key())),
            search.TextField(name="merchant_name", value=self.merchant.name),
            ])
        try:
            search.Index(name="locations").put(document)
        except search.Error:
            logging.exception("Location search index put failed")

    def search_index_delete(self):
        try:
            search.Index(name="locations").delete(str(self.key()))
        except search.Error:
            logging.exception("Location search index deletion failed")
        
    def get_bars(self):
        try:
            return self.loaded_bars
        except AttributeError:
            self.loaded_bars = self.bars.order('name').fetch(100)
            return self.loaded_bars

    @property
    def upcoming_events(self):
        now = datetime.datetime.now()
        nextmonth = now + datetime.timedelta(days=60)
        return self.events.filter('date_delivery >', now).filter('date_delivery <', nextmonth).order('date_delivery')

    @property
    def cover_urls(self):
        # Get URLs from cache
        cache_keys = [self.get_cover_url_cache_key(i) for i in self.images]
        cached_urls = memcache.get_multi(cache_keys)
        new_images = {}
        urls = []
        for i in self.images:
            # If image is in cache, use the cached URL
            cache_key = self.get_cover_url_cache_key(i)
            if cache_key in cached_urls:
                urls.append(cached_urls[cache_key])
            else:
                # If image is not cached, generate the URL and cache it
                url = self.get_cover_url(i)
                urls.append(url)
                new_images[cache_key] = url
        # Cache all newly generated URLs
        if len(new_images) > 0:
            memcache.set_multi(new_images)
        return urls

    def get_cover_url_cache_key(self, blobkey):
        return 'image-'+str(blobkey)+'-s640'

    def get_cover_url(self, blobkey):
        return images.get_serving_url(blobkey, size=640,secure_url=True )
    
class Bar(db.Model):
    merchant = db.ReferenceProperty(Merchant)
    location = db.ReferenceProperty(Location, collection_name='bars')
    name = db.StringProperty(required=True)
    counter_service = db.BooleanProperty(default=True) # deprecated, replaced by counter_service_to_stay and counter_service_to_go
    to_go = db.BooleanProperty(default=False) # deprecated, replaced by counter_service_to_stay and counter_service_to_go
    counter_service_to_stay = db.BooleanProperty(default=False)
    counter_service_to_go = db.BooleanProperty(default=False)
    table_service = db.BooleanProperty(default=False)
    delivery_service = db.BooleanProperty(default=False)
    delivery_min_order_amount = DecimalProperty(2, default=Decimal('0.00'))
    delivery_fee = DecimalProperty(2, default=Decimal('0.00'))
    delivery_fee_tax_class = ReferenceProperty(TaxClass)
    enabled = db.BooleanProperty(default=True)
    #accepting_orders = db.BooleanProperty(default=False)
    status = db.StringProperty(default='closed', choices=['open', 'paused', 'closed'])
    persist_status = db.BooleanProperty(default=False)
    message_when_paused = db.StringProperty(default="")
    estimated_waiting_time = db.IntegerProperty(default=0)
    estimated_waiting_time_delivery = db.IntegerProperty(default=0)
    
    # Notifications
    notif_counter_preparing_use_default = db.BooleanProperty(default=True)
    notif_counter_complete_use_default = db.BooleanProperty(default=True)
    notif_table_preparing_use_default = db.BooleanProperty(default=True)
    notif_delivery_preparing_use_default = db.BooleanProperty(default=True)
    notif_delivery_complete_use_default = db.BooleanProperty(default=True)
    notif_counter_preparing = db.StringProperty()
    notif_counter_complete = db.StringProperty()
    notif_table_preparing = db.StringProperty()
    notif_delivery_preparing = db.StringProperty()
    notif_delivery_complete = db.StringProperty()
    
    API_PROPERTIES = ['name', 'open', 'counter_service', 'table_service', 'delivery_service',  'to_go', 'counter_service_to_stay', 'counter_service_to_go', 'message_when_closed', 'estimated_waiting_time', 'estimated_waiting_time_delivery', 'delivery_min_order_amount', 'delivery_min_order_amount_formatted']
    API_LEGACY_LOCATION_PROPERTIES = ['latitude', 'longitude']
    API_LEGACY_MERCHANT_PROPERTIES = ['prices_include_tax', 'currency', 'vat_number', 'timezone', 'image_url', 'has_entrance_categories', 'has_menu_categories', 'address']
    
    @property
    def id(self):
        return str(self.key().id())

    @property
    def accepting_orders(self):
        return self.status == 'open'
        
    @property
    def open(self):
        return self.status == 'open' and self.enabled and (self.counter_service or self.table_service or self.delivery_service)
        """ Bar is open for orders if one or more serve screens (processing sessions) are open """
        """
        open = memcache.get('baropen_'+str(self.key().id()))
        if open is None:
            sessions = self.order_processing_sessions.filter('connected =', True).fetch(1)
            open = (self.enabled and self.accepting_orders and (self.counter_service or self.table_service) and len(sessions) > 0)
            memcache.set('baropen_'+str(self.key().id()), open, 60*10)
        return open
        """
        
    @property
    def paused(self):
        return self.status == 'paused'
        """ Bar is paused if one or more serve screens are open but not accepting orders """
        """
        paused = memcache.get('barpaused_'+str(self.key().id()))
        if paused is None:
            sessions = self.order_processing_sessions.filter('connected =', True).fetch(1)
            paused = (self.enabled and not self.accepting_orders and (self.counter_service or self.table_service) and len(sessions) > 0)
            memcache.set('barpaused_'+str(self.key().id()), paused, 60*10)
        return paused
        """
    """
    def clear_open_cache(self):
        if self.is_saved():
            memcache.delete('baropen_'+str(self.key().id()))
            memcache.delete('barpaused_'+str(self.key().id()))
    """
        
    @property
    def message_when_closed(self):
        if self.paused and self.message_when_paused:
            return self.message_when_paused
        else:
            return _("Closed for orders")
        
    def get_orders_with_status(self, status):
           return self.orders.filter('status =', status)
        
    @staticmethod
    def get_default_notifications():
        return {
            'counter_preparing': _("Your order is now being prepared and will be zerved at the bar shortly."),
            'counter_complete': _("Your order is zerved at the bar. Please pick it up."),
            'table_preparing': _("Your order is being prepared. It will be zerved at your table shortly."),
            'delivery_preparing': _("Your order is now being prepared."),
            'delivery_complete': _("Your order has been sent out for delivery.")
        }

    def get_notification(self, notification_type):
        """ Returns default message or custom message depending on use_default setting (custom message may be empty) """
        use_default = getattr(self, 'notif_'+notification_type+'_use_default')
        custom_message = getattr(self, 'notif_'+notification_type)
        return custom_message if not use_default else Bar.get_default_notifications()[notification_type]
    
    @property
    def products(self):
        return Product.all().filter('enabled_bars =', self.key())
        
    def __str__(self):
        return self.name
        
    @property
    def name_with_location(self):
        return self.location.name + ": " + self.name

    def put(self):
        #self.clear_open_cache()
        super(Bar, self).put()
        if self.enabled:
            self.search_index_save()
        else:
            self.search_index_delete()
        
    def delete(self):
        self.search_index_delete()
        super(Bar, self).delete()

    def search_index_save(self):
        document = search.Document(doc_id=str(self.key()), fields=[
            search.TextField(name="id", value=str(self.key().id())),
            search.TextField(name="name", value=self.name),
            search.TextField(name="location_name", value=self.location.name),
            search.TextField(name="merchant_name", value=self.merchant.name),
            search.TextField(name="merchant_key", value=str(self.merchant.key())),
            ])
        try:
            search.Index(name="bars").put(document)
        except search.Error:
            logging.exception("Bar search index put failed")

    def search_index_delete(self):
        try:
            search.Index(name="bars").delete(str(self.key()))
        except search.Error:
            logging.exception("Bar search index deletion failed")
        
    def get_menus(self):
        return Menu.all().filter('bars = ', self.key()).order('sort_order')

    @property
    def menus(self):
        return self.get_menus()

    def has_same_products(self, bar):      
        products1 = self.products
        products2 = bar.products
        if products1.count() == 0 or products2.count() == 0:
            return False           
        if products1.count() != products2.count():
            return False
        for product1 in products1:
            if product1.key() not in [product2.key() for product2 in products2]:
                return False
        return True

    def get_delivery_fee(self, incl_tax):
        price_includes_tax = self.merchant.prices_include_tax
        tax_rate = self.delivery_fee_tax_class.rate if self.delivery_fee_tax_class else Decimal(0)
        price = self.delivery_fee
        if (price_includes_tax and not incl_tax):
            # Subtract tax
            price -= self.get_delivery_tax()
        if (not price_includes_tax and incl_tax):
            # Add tax
            price += self.get_delivery_tax()
        return price

    def get_delivery_tax(self):
        tax_rate = self.delivery_fee_tax_class.rate/100 if self.delivery_fee_tax_class else Decimal(0)
        if self.merchant.prices_include_tax:
            return self.delivery_fee*(1-1/(1+tax_rate))
        else:
            return self.delivery_fee*tax_rate

    @property
    def delivery_min_order_amount_formatted(self):
        return i18n.format_currency(self.delivery_min_order_amount, self.merchant.currency)

    @property
    def to_go(self):
        # Ask customer for to stay/to go if both options are enabled
        return self.counter_service_to_stay and self.counter_service_to_go
        
    @property
    def counter_service(self):
        return self.counter_service_to_stay or self.counter_service_to_go

    def is_service_takeaway(self, service, to_go):
        # Delivery and counter to go is always takeaway
        if service == 'delivery' or (service == 'counter' and to_go):
            return True
        # Counter without the to_go flag set may still be takeaway if the bar does not have to_stay enabled
        if service == 'counter' and not self.counter_service_to_stay:
            return True
        return False

    def has_service(self, service):
        if service == 'counter' and self.counter_service:
            return True
        if service == 'table' and self.table_service:
            return True
        if service == 'delivery' and self.delivery_service:
            return True
        return False

class OrderProcessingSession(db.Model):
    """
    Represents an open serve module screen
    connected (true/false) is updated by channel connections/disconnections
    After an hour, the session is deleted by cron, and the serve screen must reconnect
    """
    
    date_created = db.DateTimeProperty(auto_now_add=True)
    bar = db.ReferenceProperty(Bar, collection_name='order_processing_sessions')
    connected = db.BooleanProperty(default=True)
    locale = db.StringProperty(default='en')
    """
    def put(self):
        self.bar.clear_open_cache()
        super(OrderProcessingSession, self).put()
        
    def delete(self):
        self.bar.clear_open_cache()
        super(OrderProcessingSession, self).delete()
    """
    def send_message(self, message):
        # Key name is the channel ID
        if self.key().name():
            firezerved._send_firebase_message(self.key().name(), message=message)

class MerchantInvoice(db.Model):
    """
    Contains the settlement between Zerved and a merchant
    
    Zerved fees and payment fees are invoiced (positive amounts)
    Order revenue is credited (negative amounts)
    
    Every merchant has an open merchant invoice, where orders are attached
    as they are completed. After a week, the invoice is closed, totals are
    calculated, and money transferred to the merchant.
    """
    
    merchant = db.ReferenceProperty(Merchant, collection_name='invoices')
    # Merchant invoice should contain orders between date_created and invoice_date
    date_created = db.DateTimeProperty(auto_now_add=True)
    invoice_date = db.DateTimeProperty()
    invoice_number = db.IntegerProperty()
    open = db.BooleanProperty(default=True) # Is invoice open for new orders and invoice items to be added
    total_excl_tax = DecimalProperty(2, default=Decimal('0.00'))
    total_incl_tax = DecimalProperty(2, default=Decimal('0.00'))
    total_tax = DecimalProperty(2, default=Decimal('0.00'))
    total_positive = DecimalProperty(2, default=Decimal('0.00'))
    total_negative = DecimalProperty(2, default=Decimal('0.00'))
    total_discount = DecimalProperty(2, default=Decimal('0.00'))
    order_revenue = DecimalProperty(2, default=Decimal('0.00'))
    order_count = db.IntegerProperty()
    first_order_date_completed = db.DateTimeProperty()
    last_order_date_completed = db.DateTimeProperty()
    #total_settlement = DecimalProperty(2, default=Decimal('0.00')) # Amount to be transferred to merchant = order_revenue - total_incl_tax
    settled = db.BooleanProperty(default=False) # Payed or not
    currency = db.StringProperty(choices=i18ndata.ALLOWED_CURRENCIES, default='DKK')
    merchant_name = db.StringProperty()
    merchant_address = db.TextProperty()
    merchant_vat_number = db.StringProperty()
    merchant_bank_account = db.StringProperty()
    merchant_country = db.StringProperty(default='DK')
    fee_rate = DecimalProperty(2, default=Decimal('0.00'))
    
    def close(self):
        self.invoice_date = datetime.datetime.now()
        countername = "merchantinvoice"
        self.invoice_number = counter.increment_and_get_count(countername)
        self.open = False
        items = self.collect_orders()
        self.collect_totals(items)
        #self.total_settlement = self.order_revenue - self.total_incl_tax
        self.put()
        
    def collect_orders(self):
        orders = self.orders.order('date_completed')
        invoice_items = []
        fee_per_order = []
        flat_fee_per_order = []
        if orders.count(limit=1)>0:
            self.first_order_date_completed = orders.get().date_completed
            self.order_revenue = Decimal('0.00')
            paymentfees = Decimal('0.00')
            self.order_count = 0
            order = None
            
            discount_perc = 0
            try:
                discount_perc = self.merchant.get_discount_percentage(orders.count()) #gets the discount % depending on the number of transactions (as defined by the pricetier)
            except Exception:
                logging.error('Exception!!')
                return invoice_items
                 
            for order in orders:
                self.order_count += 1
                self.order_revenue += order.total_incl_tax
                #fee rate per order
                order_fee_rate = self.merchant.get_fee_for_order(order.total_incl_tax)
                order_flat_fee_rate = self.merchant.get_flat_fee_for_order(order.total_incl_tax)
                order_amt = order.total_incl_tax * order_fee_rate / 100
                order_flat_amt = order_flat_fee_rate
                self.total_discount -= order_amt * discount_perc / 100
                fee_per_order.append( {
                    'fee' : order_fee_rate,
                    'amt' : order_amt
                })
                flat_fee_per_order.append( {
                    'flat_fee' : order_flat_fee_rate,
                    'flat_amt' : order_flat_amt
                })
                paymentfees += order.payment_fee
            self.last_order_date_completed = order.date_completed
            
            # Invoice line for total payment fees in orders
            """
            # We are no longer charging payment fees
            invoice_items.append(MerchantInvoiceItem(
                invoice=self,
                sku='paymentfees',
                name=str(_('Payment fees')),
                quantity=1,
                unit_price_excl_tax=paymentfees,
                unit_price_incl_tax=paymentfees,
                total_excl_tax=paymentfees,
                total_incl_tax=paymentfees,
                sort_order=2
            ))
            """

            #groups orders by fee % in "c" and counts orders by fee % in "N" 
            c = defaultdict(int)
            N = defaultdict(int)
            for d in fee_per_order:
                c[d['fee']] += d['amt'] 
                N[str(d['fee'])] += 1

            #groups orders by flat fee rate in "flat_fee_c" and counts orders by flat fee rate % in "flat_fee_N"
            flat_fee_c = defaultdict(int)
            flat_fee_N = defaultdict(int)
            for d in flat_fee_per_order:
                flat_fee_c[d['flat_fee']] += d['flat_amt']
                flat_fee_N[str(d['flat_fee'])] += 1

            # Zerved order fees lines
            vat_rate = i18ndata.VAT_RATES[self.merchant_country]
            orderfees_incl_tax = 0
            orderflatfees_incl_tax = 0


            for fee, amt in c.items():
                if amt == 0:
                    continue
                orderfees_excl_tax = amt
                orderfees_incl_tax = orderfees_excl_tax * ( 1 + vat_rate )
                orderfees_tax = orderfees_incl_tax - orderfees_excl_tax
                invoice_items.append(MerchantInvoiceItem(
                    invoice=self,
                    sku='orderfees',
                    name=str('Fees'),
                    name_parameters=[str(fee), str(N[str(fee)])],
                    quantity=1,
                    unit_price_excl_tax=orderfees_excl_tax,
                    unit_price_incl_tax=orderfees_incl_tax,
                    total_excl_tax=orderfees_excl_tax,
                    total_incl_tax=orderfees_incl_tax,
                    total_tax=orderfees_tax,
                    tax_rate=vat_rate*100,
                    sort_order=self.merchant.get_sort_order(fee)
                ))

            for flat_fee, flat_fee_amt in flat_fee_c.items():
                if flat_fee_amt == 0:
                    continue

                orderflatfees_excl_tax = flat_fee_amt
                orderflatfees_incl_tax = orderflatfees_excl_tax * ( 1 + vat_rate )
                orderflatfees_tax = orderflatfees_incl_tax - orderflatfees_excl_tax
                invoice_items.append(MerchantInvoiceItem(
                    invoice=self,
                    sku='orderflatfees',
                    name=str('Flat fees'),
                    name_parameters=[str(flat_fee), str(self.currency), str(flat_fee_N[str(flat_fee)])],
                    quantity=1,
                    unit_price_excl_tax=orderflatfees_excl_tax,
                    unit_price_incl_tax=orderflatfees_incl_tax,
                    total_excl_tax=orderflatfees_excl_tax,
                    total_incl_tax=orderflatfees_incl_tax,
                    total_tax=orderflatfees_tax,
                    tax_rate=vat_rate*100,
                    sort_order=100+self.merchant.get_sort_order(flat_fee)
                ))

            # Discount line
            if self.total_discount < 0:
                discount_excl_tax = self.total_discount / ( 1 + vat_rate )
                discount_tax = vat_rate * discount_excl_tax
                invoice_items.append(MerchantInvoiceItem(
                    invoice=self,
                    sku='discount',
                    name= "Fees discount " + str(discount_perc) + " %",
                    name_parameters=[str(discount_perc)],
                    quantity=1,
                    unit_price_excl_tax=Decimal(discount_excl_tax),
                    unit_price_incl_tax=Decimal(self.total_discount),
                    total_excl_tax=Decimal(discount_excl_tax),
                    total_incl_tax=Decimal(self.total_discount),
                    total_tax=discount_tax,
                    tax_rate=vat_rate*100,
                    sort_order=100
                ))

            # Fixed fee
            fixed_fee = self.merchant.get_pricetier().fixed_fee
            if fixed_fee > 0:
                qty = self.order_count
                fixed_fee_excl_tax = fixed_fee
                fixed_fee_incl_tax = fixed_fee_excl_tax * ( 1 + vat_rate )
                fixed_fee_tax = fixed_fee_incl_tax - fixed_fee_excl_tax
                total_excl_tax = fixed_fee_excl_tax * qty
                total_incl_tax = fixed_fee_incl_tax * qty
                invoice_items.append(MerchantInvoiceItem(
                    invoice=self,
                    sku='fixed_fee',
                    name= "Fixed fee per order " + str(fixed_fee_excl_tax) + " " + str(self.currency),
                    name_parameters=[str(fixed_fee_excl_tax), str(self.currency)],
                    quantity=qty,
                    unit_price_excl_tax=Decimal(fixed_fee_excl_tax),
                    unit_price_incl_tax=Decimal(fixed_fee_incl_tax),
                    total_excl_tax=Decimal(total_excl_tax),
                    total_incl_tax=Decimal(total_incl_tax),
                    total_tax=fixed_fee_tax * qty,
                    tax_rate=vat_rate*100,
                    sort_order=1
                ))                

            # Invoice lines for order revenue, grouped by tax rate
            order_items = self.order_items.order('tax_rate')
            for tax_rate, group in groupby(order_items, lambda x: x.tax_rate):
                group_items = list(group)
                total_excl_tax = Decimal(sum([item.total_excl_tax for item in group_items]))
                total_incl_tax = Decimal(sum([item.total_incl_tax for item in group_items]))
                total_tax = Decimal(sum([item.total_tax for item in group_items]))
                invoice_items.append(MerchantInvoiceItem(
                    invoice=self,
                    sku='revenue',
                    name=str(_('Order revenue')),
                    quantity=-1,
                    unit_price_excl_tax=total_excl_tax,
                    unit_price_incl_tax=total_incl_tax,
                    total_excl_tax=-total_excl_tax,
                    total_incl_tax=-total_incl_tax,
                    total_tax=-total_tax,
                    tax_rate=Decimal(tax_rate),
                    sort_order=0
                ))               
                
            # Save items
            db.put(invoice_items)
        # Return items
        return invoice_items
        
    def collect_totals(self, items=None):
        if items is None:
            items = self.items
        self.total_excl_tax = Decimal(sum([item.total_excl_tax for item in items]))
        self.total_incl_tax = Decimal(sum([item.total_incl_tax for item in items]))
        self.total_tax = Decimal(sum([item.total_tax for item in items]))
        self.total_positive = Decimal(sum([item.total_incl_tax for item in items if item.total_incl_tax > 0])) + self.total_discount # substracts, total discount amount is negative
        self.total_negative = Decimal(sum([item.total_incl_tax for item in items if item.total_incl_tax < 0]))
        
    def get_company_data(self, key):
        return i18ndata.COMPANY_DATA[self.merchant_country][key]
            
class MerchantInvoiceItem(db.Model):
    """
    The invoice lines on a merchant invoice
    """
    
    invoice = db.ReferenceProperty(MerchantInvoice, collection_name='items')
    sku = db.StringProperty()
    name = db.StringProperty() # Saved in English
    name_parameters = db.StringListProperty() # Parameters used for name translation
    quantity = db.IntegerProperty(default=1)
    unit_price_excl_tax = DecimalProperty(2, default=Decimal('0.00'))
    unit_price_incl_tax = DecimalProperty(2, default=Decimal('0.00'))
    total_excl_tax = DecimalProperty(2, default=Decimal('0.00'))
    total_incl_tax = DecimalProperty(2, default=Decimal('0.00'))
    total_tax = DecimalProperty(2, default=Decimal('0.00'))
    tax_rate = DecimalProperty(4, default=Decimal('0.0000'))
    sort_order = db.IntegerProperty(default=0)

    @property
    def sku_name(self):
        if self.sku == 'orderfees' and len(self.name_parameters) == 2:
            return _('Fees %s %% - %s order(s)') % tuple(self.name_parameters)
        if self.sku == 'orderflatfees' and len(self.name_parameters) == 3:
            return _('Flat fees %s %s - %s order(s)') % tuple(self.name_parameters)
        elif self.sku == 'discount' and len(self.name_parameters) == 1:
            return _('Fees discount %s %%') % tuple(self.name_parameters)
        elif self.sku == 'fixed_fee' and len(self.name_parameters) == 2:
            return _('Fixed fee per order (%s %s)') % tuple(self.name_parameters)
        elif self.sku == 'custom':
            return '%s x %s' % (str(self.quantity), self.name)
        else:
            sku_names = {
                'paymentfees': _('Payment fees'),
                'orderfees': _('Fees'),
                'orderflatfees': _("Flat fees"),
                'revenue': _('Order revenue'),
                'discount' : _('Fees discount'),
            }
            return sku_names[self.sku] if self.sku in sku_names else ""

from sales import Order, OrderItem
from payment import Payment
from catalog import Product, Menu, Category
from events import Event
