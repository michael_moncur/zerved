from google.appengine.ext import db
from google.appengine.api import images, memcache
from decimalproperty import *
from referenceproperty import ReferenceProperty
from merchants import Merchant, TaxClass
import i18ndata
import calendar
import datetime
import pytz
from collections import OrderedDict
import logging
from webapp2_extras import i18n
_ = i18n.lazy_gettext
import webapp2

class Category(db.Model):
    """
    Category, used for grouping products
    """
    
    merchant = db.ReferenceProperty(Merchant, collection_name='categories')
    name = db.StringProperty(required=True)
    sort_order = db.IntegerProperty(default=0)
    group = db.StringProperty(default='menu', choices=['menu', 'entrance'])
    group_labels = {'menu': _('Menu'), 'entrance': _('Entrance') }
    enabled = db.BooleanProperty(default=True)
    enabled_bars = db.ListProperty(db.Key) # deprecated
    
    API_PROPERTIES = ['id', 'name']
    
    @staticmethod
    def construct_for_merchant(merchant, **kwds):
        category = Category(**kwds)
        category.merchant = merchant
        # Put category last
        lastcategory = Category.all().filter('merchant =', merchant).order('-sort_order').get()
        if lastcategory:
            category.sort_order = lastcategory.sort_order + 1
        return category
    
    @property
    def group_label(self):
        return self.group_labels[self.group]
        
    def has_products_menu(self, location_or_bar, menu):
        products = self.products
        if self.group == 'menu':
            products.filter("menus = ", menu)
        else:
            products.filter('enabled_locations =', location_or_bar)
        return products.count(limit=1)>0
         
    def has_products(self, location_or_bar):
        products = self.products
        if self.group == 'menu':
            products.filter('enabled_bars =', location_or_bar)
        else:
            products.filter('enabled_locations =', location_or_bar)
        return products.count(limit=1)>0     

    @property
    def id(self):
        return str(self.key().id())
        
    def __str__(self):
        return self.name

    def get_cached_products(self, location_or_menu_key):
        # Load product list from cache
        if self.group == 'menu':
            cache_key = "productlist_menu_%s_cat_%s" % (location_or_menu_key.id(), self.key().id())
        else:
            cache_key = "productlist_loc_%s_cat_%s" % (location_or_menu_key.id(), self.key().id())
        products = memcache.get(cache_key)

        if products is None:
            # Load product list from DB
            products = Product.all().filter('category =', self)
            if self.group == 'menu':
                products.filter('menus =', location_or_menu_key)
            else:
                products.filter('enabled_locations =', location_or_menu_key)
            products.order('sort_order')
            products = products.fetch(1000)
            # Preload product options
            [p.get_options() for p in products]
            # Save in cache
            try:
                memcache.add(cache_key, products, 60*60*24)
            except ValueError:
                # Product list too large to cache
                pass
        return products

    @staticmethod
    def get_categories_for_bar_and_menu(bar, menu):
        cache_key = "categories_menu_"+str(menu.key().id())
        categories = memcache.get(cache_key)
        if categories is None:
            categories = Category.all().filter('merchant =', bar.merchant).filter('group =', 'menu').filter('enabled =', True).order('sort_order')
            categories = [c for c in categories if c.has_products_menu(bar, menu)]
            # Save in cache
            memcache.add(cache_key, categories, 60*60*24)
        return categories

    @staticmethod
    def get_categories_for_location(location):
        cache_key = "categories_loc_"+str(location.key().id())
        categories = memcache.get(cache_key)
        if categories is None:
            categories = Category.all().filter('merchant =', location.merchant).filter('group =', 'entrance').filter('enabled =', True).order('sort_order')
            categories = [c for c in categories if c.has_products(location)]
            # Save in cache
            memcache.add(cache_key, categories, 60*60*24)
        return categories

class Product(db.Model):
    """
    Product model, contains simple data such as name, description, image etc.
    and more complicated calculations (price and product options) using referenced models
    """
    
    merchant = db.ReferenceProperty(Merchant, collection_name='products')
    name = db.StringProperty(required=True)
    description = db.TextProperty(default='')
    price = DecimalProperty(2, default=0.00)
    category = ReferenceProperty(Category, collection_name='products')
    enabled = db.BooleanProperty(default=True) # Deprecated
    tax_class = ReferenceProperty(TaxClass)
    price_rules = db.ListProperty(db.Key)
    image = db.BlobProperty()
    image_type = db.StringProperty()
    sort_order = db.IntegerProperty(default=0)
    enabled_locations = db.ListProperty(db.Key) # For entrance
    enabled_bars = db.ListProperty(db.Key) # For menu
    menus = db.ListProperty(db.Key)
    group = db.StringProperty(default='menu', choices=['menu', 'entrance'])
    exclude_from_loyalty = db.BooleanProperty(default=False) 
    
    API_PROPERTIES = ['name', 'description', 'base_price', 'base_price_formatted', 'final_price', 'final_price_formatted', 'tiered_price_descriptions', 'image_url', 'product_options', 'exclude_from_loyalty']
    
    @staticmethod
    def construct_for_merchant(merchant, **kwds):
        product = Product(**kwds)
        product.merchant = merchant
        # Put product last in category
        if product.category:
            lastproduct = Product.all().filter('merchant =', product.merchant).filter('category =', product.category).order('-sort_order').get()
            if lastproduct:
                product.sort_order = lastproduct.sort_order + 1
        return product
        
    def get_base_price(self, incl_tax=None):
        return self.get_tax_prepared_price(self.price, incl_tax)
        
    @property
    def base_price(self):
        return self.get_base_price()
        
    @property
    def base_price_formatted(self):
        return i18n.format_currency(self.get_base_price(), self.currency)
        
    @property
    def currency(self):
        try:
            merchant = self.loaded_merchant
        except AttributeError:
            self.loaded_merchant = self.merchant
            merchant = self.merchant
        return merchant.currency
        
    def get_tiered_price_descriptions(self):
        price_rules = sorted(self.get_current_price_rules(), key=lambda p: p.quantity)
        return [price_rule.summary(self.price) for price_rule in price_rules if price_rule.quantity > 0]
        
    @property
    def tiered_price_descriptions(self):
        return self.get_tiered_price_descriptions()

    def get_loyalty_discount(self, price=0, loyalty_code=None):
        logging.info('Product.get_loyalty_discount: price=%d loyalty_code=%s exclude_from_loyalty=%i' %(price, loyalty_code, self.exclude_from_loyalty))
        if (loyalty_code and not self.exclude_from_loyalty):
            if(self.merchant.has_loyalty_program and self.merchant.has_any_loyaltycodes() and self.merchant.is_loyaltycode_valid(loyalty_code)):
                return Decimal(price*(self.merchant.loyalty_program_rate/100))
            else:
                logging.info('Product.get_loyalty_discount: Loyaltycode:%s invalid!' % loyalty_code)
        else:
            logging.info('Product.get_loyalty_discount: no loyalty_code or self.exclude_from_loyalty')
                
        return Decimal('0.0')
        
    def get_price(self, quantity=1, incl_tax=None):
        base_price = self.get_base_price(incl_tax)
        prices = [base_price*quantity]
        prices.extend([self.get_tax_prepared_price(price_rule.apply_to(quantity, self.price), incl_tax) for price_rule in self.get_current_price_rules()])
        #logging.info('tax '+str(incl_tax))
        #logging.info(prices)

        return min(prices)
        
    @property
    def final_price(self):
        return self.get_price(quantity=1)
        
    @property
    def final_price_formatted(self):
        return i18n.format_currency(self.get_price(quantity=1), self.currency)
        
    def get_current_price_rules(self):
        # Merchant price rules may be preloaded in the following attribute to avoid multiple loads
        if not hasattr(self, 'current_merchant_price_rules'):
            self.current_merchant_price_rules = PriceRule.get_current_price_rules(self.merchant)
        # Return the merchant price rules that are valid for this product
        return [price_rule for price_rule in self.current_merchant_price_rules if (price_rule.key() in self.price_rules)]
        
    def get_tax_prepared_price(self, price, incl_tax=None):
        price_includes_tax = self.merchant.prices_include_tax
        if (incl_tax == None):
            incl_tax = price_includes_tax
        if (price_includes_tax and not incl_tax):
            # Subtract tax
            price -= self.get_tax(price)
        if (not price_includes_tax and incl_tax):
            # Add tax
            price += self.get_tax(price)
        return price
        
    def get_tax(self, price=None):
        if price == None:
            price = self.price
        tax_rate = self.get_tax_rate()/100
        if self.merchant.prices_include_tax:
            return price*(1-1/(1+tax_rate))
        else:
            return price*tax_rate
            
    def get_tax_rate(self):
        return self.get_tax_class().rate if self.get_tax_class() else Decimal(0)
        
    def get_tax_class(self):
        try:
            return self.tax_class
        except db.ReferencePropertyResolveError:
            return None
            
    def __str__(self):
        return self.name
        
    def get_price_rules(self):
        return db.get(self.price_rules)
        
    def add_price_rule(self, price_rule):
        if not price_rule in self.price_rules:
            self.price_rules.append(price_rule)
            return True
        return False
            
    def remove_price_rule(self, price_rule):
        if price_rule in self.price_rules:
            self.price_rules.remove(price_rule)
            return True
        return False
        
    def add_location(self, location):
        if not location in self.enabled_locations:
            self.enabled_locations.append(location)
            return True
        return False
        
    def remove_location(self, location):
        if location in self.enabled_locations:
            self.enabled_locations.remove(location)
            return True
        return False
        
    def add_bar(self, bar):
        if not bar in self.enabled_bars:
            self.enabled_bars.append(bar)
            return True
        return False
    
    def remove_bar(self, bar):
        if bar in self.enabled_bars:
            self.enabled_bars.remove(bar)
            return True
        return False

    def add_menu(self, menu):
        if not menu in self.menus:
            self.menus.append(menu)
            return True
        return False
            
    def remove_menu(self, menu):
        if menu in self.menus:
            self.menus.remove(menu)
            return True
        return False
        
    @property
    def product_options(self):
        options = []
        for option in self.get_options():
            options.append(option.to_json())
        return options
        
    def get_options(self):
        try:
            return self.loaded_options
        except AttributeError:
            self.loaded_options = self.options.order('sort_order').fetch(100)
            for option in self.loaded_options:
                ProductOption.product.__set__(option, self) # Preload product object
            return self.loaded_options
        
    def get_optiondata(self, selected_options):
        optiondata = []
        selected_option_ids = [o[0] for o in selected_options]
        for option in self.get_options():
            option_id = int(option.key().id())
            if option_id in selected_option_ids:
                data = {'option_id': option_id, 'option_name': option.name, 'values':[] }
                for value in [o[1] for o in selected_options if o[0]==option_id]:
                    data['values'].append({
                        'value_label': option.get_value_label(value), 
                        'value_price_excl_tax': self.get_tax_prepared_price(option.get_value_price(value), incl_tax=False),
                        'value_price_incl_tax': self.get_tax_prepared_price(option.get_value_price(value), incl_tax=True)
                    })
                optiondata.append(data)
        return optiondata
        
    def get_total_surcharge(self, options, quantity=1, incl_tax=None):
        total = Decimal('0.00')
        for (option_id, value) in options:
            option = next(o for o in self.get_options() if o.key().id() == option_id)
            total += option.get_value_price(value)
        return self.get_tax_prepared_price(quantity*total, incl_tax)
        
    def delete(self):
        db.delete(self.options)
        super(Product, self).delete()
        
    @staticmethod
    def prefetch_merchant_price_rules(productlist, merchant):
        price_rules = PriceRule.get_current_price_rules(merchant)
        for pr in price_rules:
            # Preload merchant object in price rules
            pr.merchant = merchant
        for p in productlist:
            # Preload price rules list in products
            p.current_merchant_price_rules = price_rules
        return productlist
        
    @staticmethod
    def prefetch_categories(products):
        ref_keys = [Product.category.get_value_for_datastore(product) for product in products]
        ref_keys = [r for r in ref_keys if r != None]
        categorymap = dict((x.key(), x) for x in db.get(set(ref_keys)) if x)
        for product in products:
            category_key = Product.category.get_value_for_datastore(product)
            if category_key and category_key in categorymap:
                Product.category.__set__(product, categorymap[category_key])
        return products

    @property
    def image_url(self):
        if self.image:
            return webapp2.uri_for('image', entity_key=self.key(), width=320, height=320, _full=True) # Resize to square that fits half the width of large resolution phone
        else:
            return None
        
class ProductOption(db.Model):
    """
    A product option belongs to a product
    Contains a list of choices, and a price for each choice
    Option may be single select or multiple select, required or optional
    """
    
    product = db.ReferenceProperty(Product, collection_name='options')
    name = db.StringProperty(required=True)
    sort_order = db.IntegerProperty(default=0)
    selection_type = db.StringProperty(default='single', required=True, choices=['single','multiple'])
    required = db.BooleanProperty(default=True)
    value_labels = db.StringListProperty()
    value_prices = db.ListProperty(long)
    __store_mul = Decimal('10') ** 2
    __quantize_exp = Decimal('10') ** -2
    default_value = db.IntegerProperty(default=0)
    
    def to_json(self):
        return { 'key': str(self.key()), 'id': str(self.key().id()), 'name': self.name, 'selection_type': self.selection_type, 'required': self.required, 'values': self.get_option_values() }
        
    def get_option_values(self):
        option_values = []
        value_prices = self.get_value_prices()
        for i in range(len(self.value_labels)):
            #option_values.append((i, self.value_labels[i] + ' ' + self.get_price_string(value_prices[i])))
            option_values.append({'index': i, 'label': self.value_labels[i], 'price_label': self.get_price_string(value_prices[i])})
        return option_values
        
    def get_value_prices(self):
        return [Decimal(value) / self.__store_mul for value in self.value_prices]
        
    def get_value_price(self, index):
        return Decimal(self.value_prices[index]) / self.__store_mul
        
    def get_value_label(self, index):
        return self.value_labels[index]
        
    def get_optiondata(self, index):
        return {
            'option_id': int(self.key().id()), 
            'option_name': self.name, 
            'value_label': self.get_value_label(index), 
            'value_price_excl_tax': self.product.get_tax_prepared_price(self.get_value_price(index), incl_tax=False),
            'value_price_incl_tax': self.product.get_tax_prepared_price(self.get_value_price(index), incl_tax=True)
        }
        
    def get_price_string(self, price):
        if price == Decimal('0.00'):
            return ''
        elif price < 0:
            return self.product.merchant.format_currency(price)
        else:
            return '+' + self.product.merchant.format_currency(price)

class PriceRuleSchedule(db.Model):
    """
    Schedules contain 24x7 hour slots to define a weekly schedule of valid hours
    Used to determine when price rules and menus are valid (the name was chosen before menus existed)
    """
    
    merchant = db.ReferenceProperty(Merchant, collection_name='price_rule_schedules')
    name = db.StringProperty()
    # Example: hours0 = [17,18,19] means monday 17:00:00-19:59:59
    hours0 = db.ListProperty(int) # Monday
    hours1 = db.ListProperty(int)
    hours2 = db.ListProperty(int)
    hours3 = db.ListProperty(int)
    hours4 = db.ListProperty(int)
    hours5 = db.ListProperty(int)
    hours6 = db.ListProperty(int) # Sunday
    
    def __str__(self):
        return self.name + ': ' + self.hours_summary
    
    @property
    def hours_summary(self):
        """ A written summary of the valid hours """
        daysummaries = []
        totalhours = 0
        for weekday in range(7):
            hours = getattr(self, 'hours'+str(weekday))
            totalhours += len(hours)
            if len(hours) > 0:
                daystring = calendar.day_abbr[weekday]+" "+self.day_hours_summary(weekday)
                daysummaries.append(daystring)
        if totalhours == 7*24:
            return _('Always valid')
        return ", ".join(daysummaries)
        
    def day_hours_summary(self, weekday):
        """ A written summary of the valid hours for a given weekday (0-6) """
        hours = getattr(self, 'hours'+str(weekday))
        intervals = []
        lastinterval = []
        lasthour = -1
        for hour in hours:
            if hour == lasthour + 1:
                lastinterval.append(hour)
            else:
                if len(lastinterval) > 0: intervals.append(lastinterval)
                lastinterval = [hour]
            lasthour = hour
        if len(lastinterval) > 0: intervals.append(lastinterval)
        intervalstrings = []
        for interval in intervals:
            start = interval[0]
            end = interval[len(interval)-1]+1
            intervalstrings.append("%02d-%02d" % (start, end))
        return " + ".join(intervalstrings)
        
    def valid_hour(self, weekday, hour):
        return (hour in getattr(self, 'hours'+str(weekday)))
        
    def set_hours_from_checkboxes(self, checkbox_values):
        # Build hour lists
        hours = [[],[],[],[],[],[],[]]
        for hourslot in checkbox_values:
            (day, sep, hour) = hourslot.partition(',')
            hours[int(day)].append(int(hour))
        for weekday in range(7):
            setattr(self, 'hours'+str(weekday), hours[weekday])
            
    @staticmethod
    def get_valid_schedules(merchant, when=None):
        """ Get all schedules for merchant that are valid at given time (default now) """
        if when is None:
            when = datetime.datetime.now()
        if merchant.timezone:
            when = when.replace(tzinfo=pytz.utc).astimezone(pytz.timezone(merchant.timezone))
        weekday = when.weekday()
        hour = when.hour
        return PriceRuleSchedule.all().filter('merchant =', merchant).filter('hours'+str(weekday)+' =', hour)
        
class PriceRule(db.Model):
    """
    Price rules define discounts for a product in a given quantity
    Price rule calculations can use various algorithms (percentage discount, 
    fixed discount, set price to fixed amount)
    """
    
    merchant = db.ReferenceProperty(Merchant, collection_name='price_rules')
    name = db.StringProperty(required=True)
    quantity = db.IntegerProperty(required=True)
    amount = DecimalProperty(2, default=Decimal('0.00'), required=True)
    calculation = db.StringProperty(default='setprice',choices=['setprice','setprice_interval','fixeddiscount','fixeddiscount_interval','percentdiscount','percentdiscount_interval'])
    schedule = ReferenceProperty(PriceRuleSchedule, collection_name='price_rules')

    @property
    def products(self):
        return Product.all().filter('price_rules =', self.key())
        
    @staticmethod
    def get_calculation_names():
        return OrderedDict([
            ('setprice_interval', _('Interval price')),
            ('setprice', _('Unit price')),
            ('fixeddiscount_interval', _('Interval discount')),
            ('fixeddiscount', _('Unit discount')),
            ('percentdiscount_interval', _('Interval percentage discount')),
            ('percentdiscount', _('Unit percentage discount')),
        ])
        
    @staticmethod
    def get_calculation_descriptions(quantity='X', amount='Y', currency=None, original_price='?'):
        currency = ' '+currency if currency else ''
        return OrderedDict([
            ('setprice_interval', _('Buy %s for %s%s') % (quantity, amount, currency)),
            ('setprice', _('Buy %s or more for %s%s each') % (quantity, amount, currency) if quantity > 1 else _('Special offer! Price before discount: %s%s') % (original_price, currency)),
            ('fixeddiscount_interval', _('Buy %s, save %s%s') % (quantity, amount, currency)),
            ('fixeddiscount', _('Buy %s or more, save %s%s on each') % (quantity, amount, currency)),
            ('percentdiscount_interval', _('Buy %s, save %s %%') % (quantity, amount)),
            ('percentdiscount', _('Buy %s or more, save %s %%') % (quantity, amount)),
        ])
        
    def summary(self, original_price='?'):
        amount = self.merchant.format_currency(self.amount) if self.calculation not in ['percentdiscount','percentdiscount_interval'] else str(self.amount)
        if original_price != '?':
            original_price = self.merchant.format_currency(original_price)
        return PriceRule.get_calculation_descriptions(quantity=self.quantity, amount=amount, original_price=original_price)[self.calculation]
        
    def __str__(self):
        return self.name + ': ' + self.summary()
        
    @staticmethod
    def get_current_price_rules(merchant):
        # Save price rules for current hour in memcache
        cache_key = "pricerules_"+str(merchant.key().id())
        price_rules = memcache.get(cache_key)
        if price_rules is None:
            price_rules = PriceRule.get_valid_price_rules(merchant).fetch(1000)
            end_of_hour = (datetime.datetime.now() + datetime.timedelta(hours=1)).replace(minute=0,second=0,microsecond=0)
            seconds_valid = int((end_of_hour - datetime.datetime.now()).total_seconds())
            memcache.add(cache_key, price_rules, seconds_valid)
        return price_rules
        
    @staticmethod
    def get_valid_price_rules(merchant, when=None):
        if when is None:
            when = datetime.datetime.now()
        schedules = PriceRuleSchedule.get_valid_schedules(merchant, when).fetch(limit=1000, keys_only=True)
        return PriceRule.all().filter('schedule IN', schedules)
        
    def apply_to(self, quantity, baseprice):
        """ Calculate discounted price for given base price and quantity """
        price = baseprice
        if quantity < self.quantity:
            return price*quantity
        if self.calculation in ['setprice_interval','fixeddiscount_interval','percentdiscount_interval']:
            intervals = int(quantity / self.quantity)
            remainder = quantity % self.quantity
            if self.calculation == 'setprice_interval':
                price = intervals * self.amount + remainder * price
            elif self.calculation == 'fixeddiscount_interval':
                price = quantity * price - intervals * self.amount
            elif self.calculation == 'percentdiscount_interval':
                intervaldiscount = self.quantity * price * self.amount / 100
                price = quantity * price - intervals * intervaldiscount
        elif self.calculation == 'setprice':
            price = quantity * self.amount
        elif self.calculation == 'fixeddiscount':
            price = quantity * price - quantity * self.amount
        elif self.calculation == 'percentdiscount':
            price = quantity * price * (100-self.amount)/100
        return price


class PriceTier(db.Model):
    """ 
        Price tiers define fees that are used for merchant invoices as default, 
        if the merchant does not have it's own negotiated price tiers.
    """

    currency = db.StringProperty(choices=i18ndata.ALLOWED_CURRENCIES) # or merchant's currency
    min_value = db.ListProperty(long)
    fee = db.ListProperty(long)
    flat_fee_min_value = db.ListProperty(long)
    flat_fee = db.ListProperty(long)
    discount_min_value = db.ListProperty(int)
    discount_percentage = db.ListProperty(long)
        
    fixed_fee = DecimalProperty(2, 0.00)

    __store_mul = Decimal('10') ** 2
    __quantize_exp = Decimal('10') ** -2
    
    def get_list(self, propertyname):
        if propertyname in ['min_value', 'fee', 'discount_percentage', 'flat_fee_min_value', 'flat_fee']:
            # Convert long to decimals
            return [Decimal(value) / self.__store_mul for value in getattr(self, propertyname)]
        else:
            return getattr(self, propertyname)
        
    def set_list(self, propertyname, values):
        if propertyname in ['min_value', 'fee', 'discount_percentage', 'flat_fee_min_value', 'flat_fee']:
            # Convert decimal to long
            setattr(self, propertyname, [int(value.quantize(self.__quantize_exp) * self.__store_mul) for value in values])
        else:
            setattr(self, propertyname, values)

    @property
    def has_discounts(self):
        for v in self.get_list('discount_percentage'):
            if v > 0:
                return True
        return False

class Menu(db.Model):
    """
    Menus define schedules where products are available, e.g. lunch menu, dinner menu
    """

    merchant = db.ReferenceProperty(Merchant, collection_name='menus')
    name = db.StringProperty(required=True)
    schedule = ReferenceProperty(PriceRuleSchedule, collection_name='menus')
    bars = db.ListProperty(db.Key)
    sort_order = db.IntegerProperty(default=0)

    API_PROPERTIES = ['id', 'name']

    @property
    def products(self):
        return Product.all().filter('menus =', self.key())

    @property
    def active(self):
        if self.schedule == None:
            return False
        when = datetime.datetime.now()
        if self.merchant.timezone:
            when = when.replace(tzinfo=pytz.utc).astimezone(pytz.timezone(self.merchant.timezone))
        weekday = when.weekday()
        hour = when.hour
        hours = getattr(self.schedule, 'hours'+str(weekday))
        return hour in hours

    def __str__(self):
        return self.name

    def add_bar(self, bar):
        if not bar in self.bars:
            self.bars.append(bar)
            return True
        return False

    def remove_bar(self, bar):
        if bar in self.bars:
            self.bars.remove(bar)
            return True
        return False

    @staticmethod
    def get_active_menus(bar):
        return Menu.get_active_menus_at(bar, datetime.datetime.now())

    @staticmethod
    def get_active_menus_at(bar, when):
        schedules = PriceRuleSchedule.get_valid_schedules(bar.merchant, when).fetch(limit=1000, keys_only=True)
        return Menu.all().filter('bars =', bar).filter('schedule IN', schedules)
