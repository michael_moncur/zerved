from google.appengine.ext import db
from google.appengine.api import users

class News(db.Model):
	slug = db.StringProperty(required=True)	# url
	title = db.StringProperty(required=True) # max. 60 characters
	content = db.TextProperty(required=True)
	precontent = db.TextProperty()
	description = db.StringProperty()  # max. 160 characters
	author = db.StringProperty(default="Zerved")
	image = db.BlobProperty()
	date = db.DateProperty(required=True)
	type = db.StringProperty(required=True, choices=["news", "merchantnews"])
	language = db.StringProperty(required=True)
	published = db.BooleanProperty(default=False)
	important = db.BooleanProperty(default=False)
	send_to_merchants = db.BooleanProperty(default=False)

	@staticmethod
	def get_author_name():
		return users.get_current_user().nickname()

class NewsImage(db.Model):
	data = db.BlobProperty(required=True)
	mimetype = db.StringProperty(required=True)