from models.front.news import News
from wtforms.ext.appengine.db import model_form
from wtforms import Form, SelectField, HiddenField, validators
from webapp2_extras import i18n
_ = i18n.lazy_gettext

class BooleanSelectField(SelectField):
    def _value(self):
        if self.data is not None:
            return 'y' if self.data else ''
        else:
            return u''
            
    def process_formdata(self, valuelist):
        self.data = bool(valuelist[0])
            
    def pre_validate(self, form):
        pass
    
def get_news_form():
    return model_form(News, exclude=('image'), field_args={
        'title': { 'label': _('Title') },
        'date': { 'label': _('Date') },
        'author': { 'label': _('Author') },
        'description': { 'label': _('Description (meta)') },
        'precontent': { 'label': _('Introduction') },
        'content': { 'label': _('Content') },
        'type': { 'label': _('Type') },
        'language': { 'label': _('Language') },
        'published': { 'label': _('Publish') },
        'slug' : { 'label': _('URL') },
    })