from models.environment import get_environment

class MobilePayAppSwitchMerchantNumber(object):
    @classmethod
    def mobilepayappswitch_merchantnumber(self):
        merchantnumber = {'development': 'APPDK7502734001', 'demo': 'APPDK0000000000', 'production': 'APPDK7502734001'}[get_environment()]
        return merchantnumber
