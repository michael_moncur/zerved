from merchants import *
from wtforms.ext.appengine.db import model_form
from wtforms import Form, SelectField, HiddenField, validators
from webapp2_extras import i18n
_ = i18n.lazy_gettext

class BooleanSelectField(SelectField):
    def _value(self):
        if self.data is not None:
            return 'y' if self.data else ''
        else:
            return u''
            
    def process_formdata(self, valuelist):
        self.data = bool(valuelist[0])
            
    def pre_validate(self, form):
        pass

def get_location_form():
    LocationModelForm = model_form(Location, exclude=('merchant', 'image'), field_args={
        'name': { 'label': _('Name') },
        'enabled': { 'label': _('Enabled') },
        'info_panel_enabled': { 'label': _('Enable Information Panel') },
        'address': { 'label': _('Address') },
        'website_url': { 'label': _('Website URL') },
        'phone': { 'label': _('Phone number 1') },
        'phone2': { 'label': _('Phone number 2') },
        'facebook': { 'label': _('Facebook URL') },
        'twitter': { 'label': _('Twitter handle') },
        'google_plus': { 'label': _('Google+ URL') },
        'linked_in': { 'label': _('LinkedIn URL') },
        'instagram': { 'label': _('Instagram URL') },
        'pinterest': { 'label': _('Pinterest URL') },
        'flickr': { 'label': _('Flickr URL') },
        'opening_hours_monday': { 'label': _('Monday') },
        'opening_hours_tuesday': { 'label': _('Tuesday') },
        'opening_hours_wednesday': { 'label': _('Wednesday') },
        'opening_hours_thursday': { 'label': _('Thursday') },
        'opening_hours_friday': { 'label': _('Friday') },
        'opening_hours_saturday': { 'label': _('Saturday') },
        'opening_hours_sunday': { 'label': _('Sunday') },
    })

    class LocationForm(LocationModelForm):
        location = HiddenField(validators=[validators.required()])

        def validate_info_panel(self):
            # If info panel is enabled, at least address and opening hours should be filled
            return not self.info_panel_enabled.data or (self.address.data and self.opening_hours_monday.data and self.opening_hours_tuesday.data and self.opening_hours_wednesday.data and self.opening_hours_thursday.data and self.opening_hours_friday.data and self.opening_hours_saturday.data and self.opening_hours_sunday.data)

    return LocationForm
    
def get_bar_form():
    BarModelForm = model_form(Bar, exclude=('merchant', 'location', 'status', 'message_when_paused', 'estimated_waiting_time', 'estimated_waiting_time_delivery', 'to_go', 'counter_service'), field_args={
        'name': { 'label': _('Name') },
        'enabled': { 'label': _('Enabled') },
        'table_service': { 'label': _('Table Service') },
        'delivery_service': { 'label': _('Address Delivery') },
        'counter_service_to_stay' : {'label' : _('Counter Service to stay') },
        'counter_service_to_go' : {'label' : _('Counter Service to go') },
        'notif_counter_preparing': { 'label': _('Message') },
        'notif_counter_complete': { 'label': _('Message') },
        'notif_table_preparing': { 'label': _('Message') },
        'notif_delivery_preparing': { 'label': _('Message') },
        'notif_delivery_complete': { 'label': _('Message') },
        'delivery_fee_tax_class': { 'label': _('Delivery Fee Tax Class') }
    })

    class BarForm(BarModelForm):
        notif_counter_preparing_use_default = BooleanSelectField(label=_('When preparation starts'), choices=[('y', _("Use Zerved's default")), ('', _('Use custom message'))], coerce=bool, default=True)
        notif_counter_complete_use_default = BooleanSelectField(label=_('When order is completed'), choices=[('y', _("Use Zerved's default")), ('', _('Use custom message'))], coerce=bool, default=True)
        notif_table_preparing_use_default = BooleanSelectField(label=_('When preparation starts'), choices=[('y', _("Use Zerved's default")), ('', _('Use custom message'))], coerce=bool, default=True)
        notif_delivery_preparing_use_default = BooleanSelectField(label=_('When preparation starts'), choices=[('y', _("Use Zerved's default")), ('', _('Use custom message'))], coerce=bool, default=True)
        notif_delivery_complete_use_default = BooleanSelectField(label=_('When order is completed'), choices=[('y', _("Use Zerved's default")), ('', _('Use custom message'))], coerce=bool, default=True)
        delivery_min_order_amount = LocalizedDecimalField(places=2, label=_('Minimum Order Amount for Delivery'))
        delivery_fee = LocalizedDecimalField(places=2, label=_('Delivery Fee'))
        select_delivery_fee_tax_class = SelectField(label=_('Delivery Fee Tax Class'))

        def set_merchant(self, merchant):
            self.select_delivery_fee_tax_class.choices = [('','')]
            self.select_delivery_fee_tax_class.choices.extend([(str(tax_class.key()), tax_class) for tax_class in TaxClass.all().filter('merchant =', merchant)])

        def set_selected_delivery_fee_tax_class(self, tax_class_key):
            self.select_delivery_fee_tax_class.data = tax_class_key
    return BarForm
    
def get_simple_bar_form():
    return model_form(Bar, exclude=('merchant', 'location', 'notif_counter_preparing', 'notif_counter_complete', 'notif_table_preparing', 'notif_delivery_preparing', 'notif_delivery_complete', 'notif_counter_preparing_use_default', 'notif_counter_complete_use_default', 'notif_table_preparing_use_default', 'notif_delivery_preparing_use_default', 'notif_delivery_complete_use_default', 'delivery_min_order_amount', 'delivery_fee', 'delivery_fee_tax_class', 'estimated_waiting_time', 'estimated_waiting_time_delivery', 'to_go', 'counter_service'), field_args={
        'name': { 'label': _('Name') },
        'enabled': { 'label': _('Enabled') },
        'table_service': { 'label': _('Table Service') },
        'delivery_service': { 'label': _('Address Delivery') },
        'counter_service_to_stay' : {'label' : _('Counter Service to stay') },
        'counter_service_to_go' : {'label' : _('Counter Service to go') },
    })