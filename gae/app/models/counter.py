from google.appengine.ext import ndb

class GeneralCounter(ndb.Model):
    count = ndb.IntegerProperty(default=0)
    
@ndb.transactional
def increment_and_get_count(countername):
    counter = GeneralCounter.get_or_insert(countername)
    counter.count += 1
    counter.put()
    return counter.count
    
@ndb.transactional
def get_count(countername):
    counter = GeneralCounter.get_or_insert(countername)
    return counter.count