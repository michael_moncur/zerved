from google.appengine.ext import db
from consumers import Consumer
from merchants import Bar, Location
from catalog import Product
import pickle

class Favourite(db.Model):
    """
    Favourite (starred) items for consumer.
    Can be added to cart immediately because it includes location/bar/product
    information and also selected product options.
    """
    bar = db.ReferenceProperty(Bar)
    location = db.ReferenceProperty(Location)
    consumer = db.ReferenceProperty(Consumer)
    product = db.ReferenceProperty(Product)
    opts = db.BlobProperty()

    API_PROPERTIES = ['id', 'consumer_id', ('product', ['id', 'name', 'product_options', 'final_price_formatted', 'image_url']), ('bar', Bar.API_PROPERTIES), ('location', ['id', 'name', 'country', 'latitude', 'longitude']), 'options']

    @property
    def id(self):
        return str(self.key().id())

    @property
    def options(self):
        return pickle.loads(str(self.opts))