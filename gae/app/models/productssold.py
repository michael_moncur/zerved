from models.sales import Order, OrderItem
from google.appengine.ext import db
from decimal import Decimal
import datetime
from collections import OrderedDict
import hashlib

class ProductsSoldReport:
    """ Report of a merchant's revenue grouped by product in a given period """
    
    def __init__(self, merchant, location, bar, status, startdate=None, enddate=None):
        self.products = OrderedDict()
        self.sums = { 'count_quantity': 0, 
            'total_incl_tax': Decimal('0.00'), 
            'total_excl_tax': Decimal('0.00'), 
            'total_tax': Decimal('0.00'),
            'total_discount_excl_tax': Decimal('0.00'),
            'total_discount_incl_tax': Decimal('0.00'),
            'total_surcharge_excl_tax': Decimal('0.00'),
            'total_surcharge_incl_tax': Decimal('0.00') }
        self.startdate = startdate
        self.enddate = enddate
        # Load all orders in the period
        orders = merchant.orders
        if type(status) is list:
            orders.filter('status IN', status)
        else:
            orders.filter('status =', status)
        if self.startdate:
            orders.filter('date_completed >', startdate)
        if self.enddate:
            orders.filter('date_completed <', enddate)
        orders.order('date_completed')
        if location:
            orders.filter('location =', db.Key.from_path('Location', int(location)))
        if bar:
            orders.filter('bar =', db.Key.from_path('Bar', int(bar)))
        # Go through all items in orders
        # NOTE: This is very inefficient/expensive, but should be OK for just one day's orders
        # Idea for future optimization: Create report structure in database, with an entity for each report row. Increment the entity numbers when orders are completed.
        for order in orders:
            for item in order.items:
                # Add item to product row
                key = self.prepare_product_row(item)
                self.products[key]['quantity'] += item.quantity
                self.products[key]['total_incl_tax'] += item.total_incl_tax
                self.products[key]['total_excl_tax'] += item.total_excl_tax
                self.products[key]['total_tax'] += item.total_tax
                self.products[key]['total_discount_excl_tax'] += item.total_discount_excl_tax
                self.products[key]['total_discount_incl_tax'] += item.total_discount_incl_tax
                self.products[key]['total_surcharge_excl_tax'] += item.total_surcharge_excl_tax
                self.products[key]['total_surcharge_incl_tax'] += item.total_surcharge_incl_tax
                # Accumulate totals
                self.sums['total_incl_tax'] += item.total_incl_tax
                self.sums['total_excl_tax'] += item.total_excl_tax
                self.sums['total_tax'] += item.total_tax
                self.sums['count_quantity'] += item.quantity
                self.sums['total_discount_excl_tax'] += item.total_discount_excl_tax
                self.sums['total_discount_incl_tax'] += item.total_discount_incl_tax
                self.sums['total_surcharge_excl_tax'] += item.total_surcharge_excl_tax
                self.sums['total_surcharge_incl_tax'] += item.total_surcharge_incl_tax
        
    def prepare_product_row(self, item):
        # Generate a unique key for each product variation
        productkey = OrderItem.product.get_value_for_datastore(item)
        productsignature = str(productkey) + "_" + str(item.options)
        key = hashlib.md5(productsignature).hexdigest()
        if key not in self.products:
            self.products[key] = {
                'name': item.name,
                'options': [],
                'quantity': 0, 
                'total_incl_tax': Decimal('0.00'), 
                'total_excl_tax': Decimal('0.00'), 
                'total_tax': Decimal('0.00'),
                'total_discount_excl_tax': Decimal('0.00'),
                'total_discount_incl_tax': Decimal('0.00'),
                'total_surcharge_excl_tax': Decimal('0.00'),
                'total_surcharge_incl_tax': Decimal('0.00') }
            for option in item.get_options():
                option = option['option_name'] + ": " + ", ".join([v['value_label'] for v in option['values']])
                self.products[key]['options'].append(option)
        return key
        
    def get_products(self):
        return self.products
        
    def get_sum(self, key):
        return self.sums[key]