from google.appengine.ext import db
from google.appengine.ext.db import polymodel
import hashlib
from uuid import uuid4
import urllib
from google.appengine.api import taskqueue, memcache, mail, app_identity
from models.payment import PaymentSubscription, FourTPayment, EpayPayment, MobilePayAppSwitchPayment
from referenceproperty import ReferenceProperty
from webapp2_extras import i18n
_ = i18n.gettext
import logging

LOGIN_EXPIRATION = 60*60*2 # Seconds
    
class Consumer(db.Model):
    """
    A consumer is a smartphone app user purchasing items with Zerved
    """
    
    pincode = db.ByteStringProperty() # Deprecated
    current_payment_subscription = ReferenceProperty(PaymentSubscription)
    device = db.StringProperty(default='iphone', choices=['iphone', 'android', 'embedded'])
    gcm_registration_id = db.StringProperty() # Google Cloud Messaging
    password = db.ByteStringProperty() # Stored encrypted
    device_token = db.StringProperty() # For push notifications
    apns_sandbox = db.BooleanProperty(default=False) # For push notifications, must be true for apps run from Xcode with dev certificate
    embedded_client_push_data = db.StringProperty(default='') # For push notifications to embedded clients. Contains device/merchant specific JSON.
    email = db.StringProperty()
    phone = db.StringProperty()
    name = db.StringProperty()
    address1 = db.StringProperty()
    address2 = db.StringProperty()
    postcode = db.StringProperty()
    city = db.StringProperty()
    country = db.StringProperty()
    loyalty_merchants = db.ListProperty(db.Key)
    loyalty_values = db.ListProperty(str) # loyalty codes for merchants in loyalty_merchants

    API_PROPERTIES = ['device', 'email', 'name', 'phone', 'address1', 'address2', 'postcode', 'city', 'country', ('current_payment_subscription', ['description']), 'loyalty_numbers']

    def set_pincode(self, pincode):
        self.pincode = hashlib.sha1(pincode).digest()
        
    def verify_pincode(self, pincode):
        digest = hashlib.sha1(pincode).digest()
        return digest == self.pincode
        
    def generate_password(self):
        password = str(uuid4())[:8]
        self.set_password(password)
        return password
        
    def set_password(self, password):
        self.password = hashlib.sha1(password).digest()
        
    def verify_password(self, password):
        digest = hashlib.sha1(password).digest()
        return digest == self.password

    def is_equal_to(self, other_consumer):
        if self.device == 'iphone':
            return self.device == other_consumer.device
        elif self.device == 'android':
            return self.gcm_registration_id == other_consumer.gcm_registration_id
        elif self.device == 'embedded':
            return self.embedded_client_push_data == other_consumer.embedded_client_push_data
        
    def send_notification(self, message, action, order):
        """
        Send push notification to consumer
        To avoid a delay for the initiating request, we place the notification in 
        a task queue, which will submit the message as soon as possible
        """
        #logging.info(message)
        if self.device == 'iphone' and self.device_token:
            params = { 'device_token': self.device_token, 'message': unicode(message).encode('utf-8'), 'action': unicode(action).encode('utf-8'), 'merchant_id': order.merchant.key().id(), 'order_id': order.key().id(), 'sandbox': '1' if self.apns_sandbox else '0' }
            taskqueue.add(url='/tasks/sendiphonemsg', params=params, retry_options=taskqueue.TaskRetryOptions(task_retry_limit=1))
        elif self.device == 'android':
            notification = ConsumerNotification(parent=self, order_key=str(order.key()), order_id=order.key().id(), message=message, merchant_name=order.merchant_name)
            notification.put()
            if self.gcm_registration_id:
                params = { 'registration_id': self.gcm_registration_id, 'message': unicode(message).encode('utf-8'), 'order_id': order.key().id(), 'order_key': str(order.key()), 'merchant_name': order.merchant_name.encode('utf-8'), 'notification_key': str(notification.key()) }
                taskqueue.add(url='/tasks/sendandroidmsg', params=params, retry_options=taskqueue.TaskRetryOptions(task_retry_limit=1))
        elif self.device == 'embedded' and self.embedded_client_push_data:
            params = {
                'push_data': self.embedded_client_push_data,
                'message': unicode(message).encode('utf-8'),
                'action': unicode(action).encode('utf-8'),
                'order_id': order.key().id(),
            }
            taskqueue.add(url='/tasks/sendembeddedmsg', params=params,
                          retry_options=taskqueue.TaskRetryOptions(task_retry_limit=1))
            
    def login(self, password):
        if self.verify_password(password):
            return self.save_login_session()
        else:
            return False
            
    def save_login_session(self):
        """ Remember a login key in memcache """
        key = self.generate_login_key()
        memcache.add('login_'+key, str(self.key()), LOGIN_EXPIRATION)
        return key

    def update_embedded_client_push_data(self, push_data):
        if push_data is None:
            push_data = ''
        if self.embedded_client_push_data != push_data:
            self.embedded_client_push_data = push_data
            self.put()

    def generate_login_key(self):
        key = str(uuid4())[:18]
        while (memcache.get('login_'+key)):
            key = str(uuid4())[:18]
        return key
        
    @staticmethod
    def authenticate(key):
        """ Login user based on login token saved in memcache """
        consumer_key = memcache.get('login_'+key)
        if consumer_key:
            memcache.set('login_'+key, consumer_key, LOGIN_EXPIRATION) # Extend login lifetime
            consumer = Consumer.get(consumer_key)
            return consumer if consumer else False
        else:
            return False
            
    def init_forgot_password(self, handler):
        token = self.generate_reset_password_token()
        memcache.add('resetpassword_'+token, self.email, 60*60*2)
        message = mail.EmailMessage()
        message.sender = "Zerved <info@zervedapp.com>"
        message.subject = _("Reset password")
        message.to = self.email
        try:
            locale = i18n.get_i18n().locale
        except AssertionError: # If request not set (when run from unit test script)
            locale = 'en'
        link = "http://%s.zervedapp.com/resetpassword/%s" % (locale, token)
        logging.info(link)
        message.html = handler.jinja2.render_template('emails/forgotpassword.html', reset_link=link)
        logging.info(token)
        message.send()
        
    def generate_reset_password_token(self):
        key = str(uuid4())[:18]
        while (memcache.get('resetpassword_'+key)):
            key = str(uuid4())[:18]
        return key
        
    def pull_notifications(self):
        notifications = ConsumerNotification.all().ancestor(self).order('date_created')
        notificationlist = notifications.fetch(100)
        db.delete(notifications)
        return notificationlist

    def initialize_4t_payment(self, order, amount, vat_amount):
        payment = FourTPayment(
            order=order, 
            amount=amount, 
            vat_amount=vat_amount,
            account_id=self.phone,
            status='new')
        payment.put()
        return payment

    def initialize_epay_payment(self, order, amount, vat_amount, is_mobilepay):
        logging.debug("initialize_epay_payment")
        payment = EpayPayment(
            order=order,
            amount=amount,
            status='new',
            is_mobilepay=is_mobilepay)
        payment.put()
        return payment

    def initialize_mobilepay_appswitch_payment(self, order, amount, vat_amount):
        logging.debug("initialize_mobilepay_appswitch_payment")
        payment = MobilePayAppSwitchPayment(
            order=order,
            amount=amount,
            status='new')
        payment.put()
        return payment

    @property
    def loyalty_codes(self):
        return [{'merchant_key': str(merchant_key), 'code': code} for merchant_key, code in zip(self.loyalty_merchants, self.loyalty_values)]

    def get_loyalty_code(self, merchant_key):
        if merchant_key in self.loyalty_merchants:
            return self.loyalty_values[self.loyalty_merchants.index(merchant_key)]
        return ""

    def set_loyalty_code(self, merchant_key, code):
        if merchant_key in self.loyalty_merchants:
            # Update existing
            index = self.loyalty_merchants.index(merchant_key)
            self.loyalty_values[index] = code
        else:
            # Append
            self.loyalty_merchants.append(merchant_key)
            self.loyalty_values.append(code)
            
class EmailConsumer(db.Model):
    """
    A wrapper model for consumer, using email address as the key name for quick fetching
    Allows us to change the consumer's email address by creating a new EmailConsumer
    and keeping the Consumer object
    """
    consumer = db.ReferenceProperty(Consumer)
    
    @staticmethod
    def get_from_password_token(token):
        email = memcache.get('resetpassword_'+token)
        return EmailConsumer.get_by_key_name(email) if email else None

class ConsumerNotification(db.Model):
    date_created = db.DateTimeProperty(auto_now_add=True)
    order_key = db.StringProperty()
    order_id = db.IntegerProperty()
    message = db.StringProperty()
    merchant_name = db.StringProperty()
    
    API_PROPERTIES = ['order_key', 'order_id', 'message', 'merchant_name']
    
class ApnsToken(db.Model):
    # key = Device token
    consumer = db.ReferenceProperty(Consumer)
    date_created = db.DateTimeProperty(auto_now_add=True)
    enabled = db.BooleanProperty(default=True)
