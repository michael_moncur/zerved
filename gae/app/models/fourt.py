from google.appengine.api import urlfetch
import urllib
import hashlib, hmac, base64
import json
from email.utils import formatdate
from decimal import Decimal
from webapp2_extras import i18n
_ = i18n.gettext
from models.environment import get_environment
import logging

FOURT_CONFIG = {
    'test': {
        'public_key': '92520d79-f5a5-40af-a01b-3522782bec76', #'5ed128e1-afa0-4684-b2b3-b8b6354068e6',
        'secret_key': 'ED7FFF020F462E4980001131A02C6BE5',#'22F4A98EBC1DCB1E2D9A87E9EC4268B1',
        'merchant_id': 119,
        'url': 'https://api.staging.4-t.dk',
        'category': 'SC22',
        'product_id': 'P03',
        'short_code': '',
        'account_verified': False,
    },
    'live': {
        'public_key': '85826e78-b200-4c67-90fa-03e0d1c3f2da',
        'secret_key': 'A6BF36058E19B5238D0C0ED5110C910B',
        'merchant_id': 67,
        'url': 'https://api.4-t.dk',
        'category': 'SC22', # Hotel, vacation, restaurant, cafes, bars, canteens, catering
        'product_id': 'P03', # Web
        'short_code': '',
        'account_verified': False,
    }
}

class FourTClient:
    """ Client class to send requests to the 4T payment API"""

    def __init__(self, live=False):
        self.config = FOURT_CONFIG['live'] if live else FOURT_CONFIG['test']

    def get_signature(self, string_to_sign):
        dig = hmac.new(self.config['secret_key'], msg=string_to_sign, digestmod=hashlib.sha256).digest()
        return base64.b64encode(dig).decode()

    def get_callback_url(self, order_key):
        return {'development': 'http://dev.zervedapp.appspot.com/api/v2/orders/%s/4tcallback',
            'demo': 'http://demo.zervedapp.appspot.com/api/v2/orders/%s/4tcallback',
            'production': 'http://zervedapp.appspot.com/api/v2/orders/%s/4tcallback'}[get_environment()] % order_key

    def call(self, path, parameters):
        urlfetch.set_default_fetch_deadline(20) # Default is 5 seconds
        logging.info(parameters)
        try:
            content = json.dumps(parameters, cls=FourTJSONEncoder, ensure_ascii=False)
            now = formatdate(timeval=None, localtime=False, usegmt=True)
            content_hash = hashlib.md5()
            content_hash.update(content)
            content_md5 = base64.b64encode(content_hash.digest()).decode()
            string_to_sign = "\n".join(['POST', path, now, 'application/json', content_md5])
            response = urlfetch.fetch(
                url=self.config['url'] + path,
                payload=content,
                method=urlfetch.POST,
                headers={
                    'Content-Type': 'application/json', 
                    'Content-MD5': content_md5,
                    'Date': now,
                    'Authorization': self.config['public_key']+':'+self.get_signature(string_to_sign)}
                )
            logging.info(response.status_code)
            logging.info(response.content)
        except urlfetch.DownloadError:
            raise Exception(_("Payment service not responding, try again later."))
        except urlfetch.SSLCertificateError:
            raise Exception('Payment SSL failed')
        except urlfetch.ResponseTooLargeError:
            raise Exception('Payment response too large')
        return FourTResponse(response)

    def create_and_authorize(self, account_id, amount, currency, vat_amount, reference_id, order_key, order_number, location_name):
        """ Start a new transaction and authorize it """
        uniRefTitle = unicode('Zerved #'+str(order_number)+' '+location_name)[:40]
        encRefTitle = uniRefTitle.encode('ascii','ignore') # should be utf-8, but 4T pukes when we do that, waiting for them to fix
        logging.debug('uniRefTitle:'+uniRefTitle)
        logging.debug('encRefTitle:'+encRefTitle)
        return self.call(path='/v1/payment/transaction',
            parameters={
                'merchantId': self.config['merchant_id'],
                'accountId': str('45')+account_id,
                'amount': str(amount),
                'callbackUrl': self.get_callback_url(order_key),
                'callbackVerbosity': ['ALL'],
                'currency': currency,
                'vatAmount': str(vat_amount),
                'category': self.config['category'],
                'referenceId': str(reference_id),
                'referenceTitle': encRefTitle,
                'feeCoveredBy': 'CUSTOMER',
                'firstOperation': 'AUTHORIZE',
                'productId': self.config['product_id'],
                'accountVerified': self.config['account_verified'],
                'shortCode': self.config['short_code']
            })

    def cancel(self, transaction_id, order_key):
        """ Cancel a transaction """
        path = '/v1/payment/transaction/%s/operation' % transaction_id
        return self.call(path=path,
            parameters={
                'type': 'CANCEL',
            })

    def capture(self, transaction_id, order_key):
        """ Capture a transaction """
        path = '/v1/payment/transaction/%s/operation' % transaction_id
        return self.call(path=path,
            parameters={
                'type': 'CAPTURE',
            })

class FourTResponse:
    """ Wrapper class for a 4T response """

    def __init__(self, response):
        self.response = response

    def is_success(self):
        return self.response.status_code == 201

    def get_error(self):
        error_data = json.loads(self.response.content)
        if error_data['reason'] == 4003:
            return _("Invalid account ID")
        elif error_data['reason'] == 40020:
            return _("Account ID needs to be verified")
        elif error_data['reason'] == 40041:
            return _("Invalid phone number")
        else:
            return None

    def get_transaction_id(self):
        transaction_url = self.response.headers['Location']
        (_, _, transaction_id) = transaction_url.rpartition('/')
        return transaction_id

class FourTJSONEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, Decimal):
            return float(obj)
        return json.JSONEncoder.default(self, obj)