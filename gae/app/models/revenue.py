from models.sales import Order
from decimal import Decimal
import datetime
from collections import OrderedDict
from google.appengine.ext import db

class RevenueReport:
    """ Report of a merchant's revenue in a given period """
    
    def __init__(self, merchant, location, bar, startdate, enddate):
        self.days = OrderedDict()
        self.sums = { 'count_orders': 0, 'total_incl_tax': Decimal('0.00'), 'total_excl_tax': Decimal('0.00'), 'total_tax': Decimal('0.00') }
        timesplit = startdate.time()
        self.startdate = startdate
        self.enddate = enddate
        # Load all orders in the period
        orders = merchant.get_orders_with_status('complete').filter('date_completed >', startdate).filter('date_completed <', enddate).order('date_completed')
        # Filter by location or bar
        if location:
            orders.filter('location =', db.Key.from_path('Location', int(location)))
        if bar:
            orders.filter('bar =', db.Key.from_path('Bar', int(bar)))
        for order in orders:
            # Put each order into a day group
            daystart = self.get_daygroup_startdate(order.date_completed, timesplit)
            dayend = daystart+datetime.timedelta(days=1)
            dayid = daystart.strftime("%Y-%m-%d")
            if dayid not in self.days:
                # Initialize day
                self.days[dayid] = {
                    'start': daystart, 
                    'end': dayend, 
                    'orders': [], 
                    'count_orders': 0, 
                    'total_incl_tax': Decimal('0.00'), 
                    'total_excl_tax': Decimal('0.00'),
                    'total_tax': Decimal('0.00')
                }
            # Add order to day group
            self.days[dayid]['orders'].append(order)
            
            # Accumulate totals
            self.sums['total_incl_tax'] += order.total_incl_tax
            self.sums['total_excl_tax'] += order.total_excl_tax
            self.sums['total_tax'] += order.total_tax
            self.sums['count_orders'] += 1
            self.days[dayid]['total_incl_tax'] += order.total_incl_tax
            self.days[dayid]['total_excl_tax'] += order.total_excl_tax
            self.days[dayid]['total_tax'] += order.total_tax
            self.days[dayid]['count_orders'] += 1
            
    def get_daygroup_startdate(self, date, timesplit):
        day = date.date()
        # If date is earlier than timesplit, use yesterday instead
        if date.time() < timesplit:
            day -= datetime.timedelta(days=1)
        return datetime.datetime.combine(day, timesplit)
        
    def get_days(self):
        return self.days
        
    def get_sum(self, key):
        return self.sums[key]