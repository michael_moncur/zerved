import os
import re

def get_environment():
    if re.match('^(localhost:8080|10.0.0.82:8080|dev.zervedapp.appspot.com|demo.zervedapp.appspot.com|dev-dot-zervedapp.appspot.com|new.zervedapp.appspot.com|zerved.dev:8080|192.168.\d+.\d+:8080|0.0.0.0:8080)$', os.environ.get('HTTP_HOST')):
        return 'development'
    elif os.environ.get('HTTP_HOST') == 'demo.zervedapp.appspot.com':
    	return 'demo'
    else:
        return 'production'