import urllib
from google.appengine.api import urlfetch
from jose import jws
from models.mobilepayappswitchmerchantnumber import MobilePayAppSwitchMerchantNumber
import hashlib
import base64
import string
import logging

class MobilePayAppSwitchClient():
    """
    Base client class for MobilePay AppSwitch webservices
    It will attempt to call MobilePay directly
    """
    service = "https://api.mobeco.dk"
    subscription_key = '32e813a05e1d47d498c0239be82a0379'

    privatekey = """-----BEGIN RSA PRIVATE KEY-----
MIIEowIBAAKCAQEA4r+6Yn411yRI/ooFWHUVY0x7RAlfMeix4EVNjU1irR11cQqb
00mxe048tqUPKEUp0pzeu8coG0WjzttFzWPVnBtHxV6c7sgW5Ib28wKZ4g2dbk2u
85olhC9kbTTiFTyFeyFQNVF3UPAUTUL7R56ohqjBdgVV1NhUxCybnFVGbZs+QfYU
VPF8nBsDooH6mxMrQXyWz190MlFR6LwVHz8CEtGQQiN+HrtX+2vaj1uIfQJC8RJe
EHG/te5POW3l5nwn3686DgUZNOyU62L1lDzOEzDgCb2PUEZWP3b+k91m0u+B5vwp
SkMFbiX85iEyH8gqzNUNBfaV9h91/+2drADU3wIDAQABAoIBADpyi1uqBrzPGJWs
BBa6D2pKI5f1Bnm57mfN1WCv6tiOuGAMCQCchUgwcYvu3gpWUaJ9Vxh2DIuDFudS
NBruOfiEO6rPaYpPPz9m3q7jI8FWGxd8pLUcLcF16eAQeppYwFttydwEhMnzKHiF
5HmglvYm978xPt+fZ7ItgWr5vC6yEjv4fnI/GIVUx8/Qo7QHfGEVS4ksxbRlzO1B
keeEsl1570gpS9hodErPzavt1RsWkBsXBE2eSHuValrHxAXS6aWWnvomn9OHJbiR
51gE3Bc5Up8rJA8BG2fO6Dq5PVMoW6WPy6TIMvOV2eqRzAfy1kJhUqGpWcFtKXHn
U8spyyECgYEA+nlro4cN8E7oNOVozO+FVKNwj1d/K/UZhWIyDep20x54rk/dP2X5
2z98FmzsCAGSbBIhTnfK6K39sxqiCRD5zJ4WJm1/xL4NJViLqwDcAeoa4shjh5zE
yyB02v/4ybg5MbPSywAYzhS3bG3emH5TkOxUU9hD+jlUhRjrvE3iCo8CgYEA58BQ
8HO6cC8c4JTPx1qc0dXN4zZYOsO0MeQjoPWcMUkdgxhB2aXLGXw2MhEylNnC/M6e
Y4ab/O8HJSRD4w2v071fmnUcLN01pPN0aVMi5/O4v28Pc+pJNYQsUAPzhlJRWAFn
VKxWE5a0Dc3HmXfErDm08c9c73AB8+bjumIz+LECgYApuyci5LJ+0ja/8WTnmBSH
yH9Nco9+nGR3I6ghR2oqvI3wxN+g23WogMhE2DrvDov9s3uXHkBRSgPV3l8W0Zw8
6i3Yky/RcLSx/zGt+QOu0flOxYJHqufiy/KMNk+WFDp7PHGnXsnDNBJnX1rR32I+
6BEYsxSIURry3fcQtY0uowKBgQDjLrbpkDlCwhyH17l7zssONI55XwvMx458ng2D
O2JbOjaRDZV3+7wGhqpGGG/CVVp9IztHi2yc3eDeOxfFm+QyKGFw876kCFIPGo2S
tmnwgg4os4Gtew07Rg9+fPEcStPs9qtJ6QsUdtaY9ZHmUu9cedF0fne3TNDNPRke
3CfgcQKBgApVsgR/lPKW18C9wc1l44dH+enAtdjm5wowQHwKY29QDF6YAQWCIhS0
vQCsgnShw70gKh28RDIrdns2fQlAZ+mbJmamKjDurGrRpiy77OJdt6uIL3qjd1u5
oaOS+AC41xRBGm66KRcnASNXdLk4JCCJY7tNN5i6ATTcCjXpcScX
-----END RSA PRIVATE KEY-----"""

    def get_url(self, method):
        method_replaced = string.replace(method, "{merchantId}", MobilePayAppSwitchMerchantNumber.mobilepayappswitch_merchantnumber())
        return self.service + method_replaced

    def call_mobilepay_backend(self, action, method, params, content_type_header=None):
        urlfetch.set_default_fetch_deadline(20) # Default is 5 seconds which is sometimes too short
        url = self.get_url(action)
        signature = self.calculate_signature(url, params)

        try:
            headers = {'Ocp-Apim-Subscription-Key': self.subscription_key,
                         'AuthenticationSignature': signature}
            if content_type_header != None:
                headers["Content-Type"] = content_type_header

            result = urlfetch.fetch(
                url=url,
                payload=params, #urllib.urlencode({'load': requestdata}),
                method=method,
                validate_certificate=True,
                headers=headers)
        except urlfetch.DownloadError:
            raise Exception(_("Payment service not responding, try again later."))
        except urlfetch.SSLCertificateError:
            raise Exception('Payment SSL failed')
        except urlfetch.ResponseTooLargeError:
            raise Exception('Payment response too large')

        if result.status_code == 200:
            return (True, result)
        else:
            logging.error("Status code: " + str(result.status_code))
            logging.error("Result content: " + str(result.content.encode('utf-8')))
            return (False, result)

    def calculate_signature(self, url, body):
        payload = url + body

        logging.info(payload)
        payload_utf8 = payload.encode("utf-8")
        payload_sha1 = hashlib.sha1(payload_utf8).digest()
        payload_base64 = base64.b64encode(payload_sha1)

        signed = jws.sign(payload_base64, self.privatekey, algorithm='RS256')
        logging.info("signed" + str(signed))
        return signed
        
class MobilePayAppSwitchPaymentClient(MobilePayAppSwitchClient):
    """
    MobilePay AppSwitch client for payments
    """
    def delete(self, orderId):
        method = urlfetch.DELETE
        action = "/appswitch/api/v1/reservations/merchants/{merchantId}/orders/" + str(orderId)
        return self.call_mobilepay_backend(action, method, "")

    def capture(self, orderId):
        method = urlfetch.PUT
        action = "/appswitch/api/v1/reservations/merchants/{merchantId}/orders/" + str(orderId)
        params = "{}"
        return self.call_mobilepay_backend(action, method, params, content_type_header="application/json")

    def getpaymentstatus(self, orderid):
        method = urlfetch.GET
        action = "/appswitch/api/v1/merchants/{merchantId}/orders/" + str(orderid)
        return self.call_mobilepay_backend(action, method, "")

