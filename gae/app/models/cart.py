from models.catalog import Product
from models.sales import Order, OrderItem
from models.payment import NordpayPayment
import copy
from decimal import Decimal
from google.appengine.ext import db
from webapp2_extras import i18n
import logging

class CartItemOutdatedError(Exception):
    pass

class Cart:
    """
    DEPRECATED: Use Quote instead
    
    Shopping cart model
    Contains items initialized from session, can calculate totals and place order
    Separate carts for separate locations and bars is possible by specifying a cart_id
    """
    
    def __init__(self, cartdata=None):
        self.cartdata = cartdata if cartdata else {} # Cartdata contains the shopping cart session data
        self.items = None # Cartdata + full information about the items in the cart
        
    def add_item(self, cart_id, itemdata):
        if cart_id not in self.cartdata:
            self.cartdata[cart_id] = {}
        if 'items' not in self.cartdata[cart_id]:
            self.cartdata[cart_id]['items'] = []
        item = self.get_item_representing_itemdata(cart_id, itemdata)
        item['qty'] += itemdata['qty']
        self.items = None
        
    def get_item_representing_itemdata(self, cart_id, itemdata):
        # If item already exists, return it
        for item in self.cartdata[cart_id]['items']:
            if self.item_represents_itemdata(item, itemdata):
                return item
        # If item not found, create a new one
        newitem = { 'product': itemdata['product'], 'qty': 0, 'options': itemdata['options']}
        self.cartdata[cart_id]['items'].append(newitem)
        return newitem
        
    def item_represents_itemdata(self, item, itemdata):
        # Check if the item is the same product with the same product options
        if item['product'] != itemdata['product']:
            return False
        if set(item['options']) != set(itemdata['options']):
            return False
        return True
        
    def update_item(self, cart_id, item_index, itemdata):
        self.cartdata[cart_id]['items'][item_index] = itemdata
        self.items = None
        
    def get_items(self, cart_id):
        if self.items:
            return (self.items, [])
            
        if cart_id not in self.cartdata or 'items' not in self.cartdata[cart_id]:
            return ([], [])
        #products = Product.get_by_id(self.cartdata[cart_id]['items'].keys())
        products = Product.get_by_id([item['product'] for item in self.cartdata[cart_id]['items']])
        items = copy.deepcopy(self.cartdata[cart_id]['items'])
        for item in items:
            try:
                #item = items[product.key().id()]
                product = next((p for p in products if p and p.key().id() == item['product']), None)
                # Validate existence of product and options
                if not product:
                    raise CartItemOutdatedError("Cart item outdated")
                for (option_id, value) in item['options']:
                    option = next((o for o in product.get_options() if o and o.key().id() == option_id), None)
                    if not option:
                        raise CartItemOutdatedError("Cart item outdated")
                    if len(option.value_prices)-1 < value:
                        raise CartItemOutdatedError("Cart item outdated")
                base_price_excl_tax = product.get_base_price(incl_tax=False)
                base_price_incl_tax = product.get_base_price(incl_tax=True)
                subtotal_excl_tax = product.get_price(quantity=item['qty'],incl_tax=False)
                subtotal_incl_tax = product.get_price(quantity=item['qty'],incl_tax=True)
                total_surcharge_excl_tax = product.get_total_surcharge(options=item['options'], quantity=item['qty'], incl_tax=False)
                total_surcharge_incl_tax = product.get_total_surcharge(options=item['options'], quantity=item['qty'], incl_tax=True)
                price = product.get_price(quantity=item['qty'])+product.get_total_surcharge(options=item['options'], quantity=item['qty']) # Price to display (may or may not contain tax)
                total_excl_tax = subtotal_excl_tax + total_surcharge_excl_tax
                total_incl_tax = subtotal_incl_tax + total_surcharge_incl_tax
                optiondata = product.get_optiondata(item['options'])
                item.update({
                    'valid': True,
                    'data': product.__dict__,
                    'category_name': product.category.name,
                    'product_key': product.key(),
                    'price': price,
                    'product_options': product.get_options(),
                    'optiondata': optiondata,
                    'base_price_excl_tax': base_price_excl_tax,
                    'base_price_incl_tax': base_price_incl_tax,
                    'total_discount_excl_tax': item['qty']*base_price_excl_tax - subtotal_excl_tax,
                    'total_discount_incl_tax': item['qty']*base_price_incl_tax - subtotal_incl_tax,
                    'total_surcharge_excl_tax': total_surcharge_excl_tax,
                    'total_surcharge_incl_tax': total_surcharge_incl_tax,
                    'total_excl_tax': total_excl_tax,
                    'total_incl_tax': total_incl_tax,
                    'total_tax': total_incl_tax-total_excl_tax,
                    'tax_rate': product.get_tax_rate(),
                    'tax_class_key': (str(product.tax_class.key()) if product.get_tax_class() else None),
                    'tax_name': (product.tax_class.name if product.get_tax_class() else '') })
            except CartItemOutdatedError:
                item.update({'valid': False})
        # Return valid items, and number of outdated invalid removed items
        self.items = [item for item in items if item['valid']]
        invaliditemindices = [i for i, item in enumerate(items) if not item['valid']]
        invaliditemindices.sort()
        invaliditemindices.reverse()
        for i in invaliditemindices:
            del self.cartdata[cart_id]['items'][i]
        return (self.items, invaliditemindices)
        
    def count_items(self, cart_id):
        qty = 0
        if cart_id not in self.cartdata or 'items' not in self.cartdata[cart_id]:
            return qty
        for item in self.cartdata[cart_id]['items']:
            qty += item['qty']
        return qty
    
    def clear_cart(self, cart_id):
        self.cartdata[cart_id] = {}
        self.items = None
        
    def remove_item(self, cart_id, item_index):
        if cart_id in self.cartdata and 'items' in self.cartdata[cart_id]:
            del self.cartdata[cart_id]['items'][item_index]
        self.items = None
            
    def get_subtotal(self, cart_id):
        total=0
        (items, _) = self.get_items(cart_id)
        for item in items:
            total += item['price']
        return total
        
    def get_total(self, cart_id):
        total=self.get_subtotal(cart_id)
        if cart_id in self.cartdata and 'tip' in self.cartdata[cart_id]:
            total += self.cartdata[cart_id]['tip']
        return total
        
    def set_comment(self, cart_id, comment):
        if cart_id not in self.cartdata:
            self.cartdata[cart_id] = {}
        self.cartdata[cart_id]['comment'] = comment
        
    def get_service(self, cart_id):
        if cart_id in self.cartdata and 'service' in self.cartdata[cart_id]:
            return self.cartdata[cart_id]['service']
        return None
        
    def set_service(self, cart_id, service):
        if cart_id not in self.cartdata:
            self.cartdata[cart_id] = {}
        self.cartdata[cart_id]['service'] = service
       
    def get_table_number(self, cart_id):
        if cart_id in self.cartdata and 'table_number' in self.cartdata[cart_id]:
            return self.cartdata[cart_id]['table_number']
        return None
        
    def set_table_number(self, cart_id, table_number):
        if cart_id not in self.cartdata:
            self.cartdata[cart_id] = {}
        self.cartdata[cart_id]['table_number'] = table_number
        
    def set_tip(self, cart_id, tip):
        if cart_id not in self.cartdata:
            self.cartdata[cart_id] = {}
        self.cartdata[cart_id]['tip'] = Decimal(tip)
        
    def set_tip_percent(self, cart_id, tip_percent):
        if cart_id not in self.cartdata:
            self.cartdata[cart_id] = {}
        self.cartdata[cart_id]['tip_percent'] = Decimal(tip_percent)
        self.set_tip(cart_id, self.get_subtotal(cart_id)*tip_percent/100)
        
    def to_json(self, cart_id, currency):    
        (items, _) = self.get_items(cart_id)
        subtotal = self.get_subtotal(cart_id)
        tip = self.cartdata[cart_id]['tip'] if 'tip' in self.cartdata[cart_id] else Decimal('0.00')
        total = self.get_total(cart_id)
        quote = {'items': [], 'total': total, 'total_formatted': i18n.format_currency(total, currency), 'subtotal': subtotal, 'subtotal_formatted': i18n.format_currency(subtotal, currency), 'tip': tip, 'tip_formatted': i18n.format_currency(tip, currency) }
        for item in items:
            item_json = { k: item[k] for k in [
                'qty',
                'price',
                'optiondata',
                'base_price_excl_tax',
                'base_price_incl_tax',
                'total_discount_excl_tax',
                'total_discount_incl_tax',
                'total_surcharge_excl_tax',
                'total_surcharge_incl_tax',
                'total_excl_tax',
                'total_incl_tax',
                'total_tax',
                'tax_rate',
                'tax_class_key',
                'tax_name'
            ] }
            item_json.update({
                'product_key': str(item['product_key']),
                'name': item['data']['_name'],
                'price_formatted': i18n.format_currency(item['price'], currency),
                'options_summary': ", ".join([o['option_name']+": "+", ".join([v['value_label'] for v in o['values']]) for o in item['optiondata']]),
                'product_has_options': len(item['product_options']) > 0,
                'valid': True
            })
            quote['items'].append(item_json)
        return quote
        
    def round_two_decimals(self, number):
        twoplaces = Decimal(10) ** -2
        return Decimal(number).quantize(twoplaces)
        
    def place_order(self, consumer, cart_id, bar=None, location=None, nordpay_authorization=None):
        raise Exception("Outdated app version. Please update.")
        
        # Initialize bar/location/merchant
        if bar==None and location==None:
            raise Exception('Bar or location needed to place order')
        if location==None:
            location=bar.location
        merchant = location.merchant
        bar_name = bar.name if bar else None
        
        # Initialize order
        try:
            locale = i18n.get_i18n().locale
        except AssertionError: # If request not set (when run from unit test script)
            locale = 'en'
        order = Order(merchant=merchant, consumer=consumer, location=location, bar=bar, currency=merchant.currency, order_number=merchant.reserve_order_number(), merchant_name=merchant.name, location_name=location.name, bar_name=bar_name, timezone=merchant.timezone, locale=locale, merchant_country=merchant.country)
        if 'comment' in self.cartdata[cart_id]:
            order.comment = self.cartdata[cart_id]['comment']
        if 'service' in self.cartdata[cart_id]:
            order.service = self.cartdata[cart_id]['service']
        if 'table_number' in self.cartdata[cart_id]:
            order.table_number = self.cartdata[cart_id]['table_number']
        order.put()

        # Create order items
        tip = self.cartdata[cart_id]['tip'] if 'tip' in self.cartdata[cart_id] else Decimal('0.00')
        total_incl_tax = tip
        total_excl_tax = tip
        total_tax = Decimal('0.0')
        (items, invaliditemindices) = self.get_items(cart_id)
        if len(invaliditemindices)>0:
            raise Exception("Cart contains outdated items")
        for item in items:
            orderitem = OrderItem(parent=order, order=order,
                product=item['product_key'],
                name=item['data']['_name'],
                category_name=item['category_name'],
                quantity=item['qty'],
                base_price_excl_tax=item['base_price_excl_tax'],
                base_price_incl_tax=item['base_price_incl_tax'],
                total_loyalty_incl_tax=item['total_loyalty_incl_tax'], 
                total_loyalty_excl_tax=item['total_loyalty_excl_tax'], 
                total_discount_excl_tax=item['total_discount_excl_tax'],
                total_discount_incl_tax=item['total_discount_incl_tax'],
                total_surcharge_excl_tax=item['total_surcharge_excl_tax'],
                total_surcharge_incl_tax=item['total_surcharge_incl_tax'],
                total_excl_tax=item['total_excl_tax'],
                total_incl_tax=item['total_incl_tax'],
                total_tax=item['total_tax'],
                tax_class=(db.Key(item['tax_class_key']) if item['tax_class_key'] else None),
                tax_rate=item['tax_rate'],
                tax_name=item['tax_name'])
            orderitem.set_options(item['optiondata'])
            orderitem.put()
            total_incl_tax += item['total_incl_tax']
            total_excl_tax += item['total_excl_tax']
            total_tax += item['total_tax']
        if tip > 0:
            orderitem = OrderItem(parent=order, order=order,
                product=None,
                name='Tip',
                category_name='Fees',
                quantity=1,
                base_price_excl_tax=tip,
                base_price_incl_tax=tip,
                total_loyalty_incl_tax=Decimal('0'),
                total_loyalty_excl_tax=Decimal('0'),
                total_discount_excl_tax=Decimal('0'),
                total_discount_incl_tax=Decimal('0'),
                total_surcharge_excl_tax=Decimal('0'),
                total_surcharge_incl_tax=Decimal('0'),
                total_excl_tax=tip,
                total_incl_tax=tip,
                total_tax=Decimal('0'),
                tax_class=None,
                tax_rate=Decimal('0'),
                tax_name="")
            orderitem.set_options([])
            orderitem.put()

        # Create and authorize payment
        total_rounded = self.round_two_decimals(total_incl_tax)
        if nordpay_authorization:
            payment = NordpayPayment(order=order, amount=total_rounded)
            payment.set_authorized(nordpay_authorization, total_rounded, merchant.currency)
        else:
            payment = consumer.current_payment_subscription.initialize_payment(order, total_rounded)
            payment.authorize()
        order.payment = payment
        order.status = 'pending'
        order.total_incl_tax = total_incl_tax
        order.total_excl_tax = total_excl_tax
        order.total_tax = total_tax
        order.put()
        
        # Update order module sessions
        Order.update_processing_sessions(order)
        
        return order