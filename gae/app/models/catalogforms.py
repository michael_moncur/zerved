from merchants import *
from catalog import *
from models.events import EventSchedule
from wtforms.ext.appengine.db import model_form
from wtforms import Form, DecimalField, SelectField, HiddenField, TextAreaField, BooleanField, FileField, validators
from views.utilities import MultiCheckboxField
from decimalproperty import *
from webapp2_extras import i18n
_ = i18n.lazy_gettext

def get_category_form():
    CategoryModelForm = model_form(Category, exclude=('merchant', 'sort_order', 'group'), field_args={
        'name': { 'label': _('Name') },
        'enabled': { 'label': _('Enabled') },
    })
    
    return CategoryModelForm

def get_tax_class_form():
    TaxClassModelForm = model_form(TaxClass, exclude=('merchant'), field_args={
        'name': { 'label': _('Name') },
        'rate': { 'label': _('Rate') },
        'is_default': { 'label': _('Is Default') },
        'on_premises_only': { 'label': _('On Premises Only') },
    })
    class TaxClassForm(TaxClassModelForm):
        rate = LocalizedDecimalField(places=4)
    return TaxClassForm

def get_product_form():
    ProductModelForm = model_form(Product, exclude=('merchant', 'price_rules', 'sort_order', 'enabled', 'group'), field_args={
        'name': { 'label': _('Name') },
        'description': { 'label': _('Description') },
        'category': { 'label': _('Category'), 'get_label': 'name' },
        #'enabled': { 'label': _('Enabled') },
        'tax_class': { 'label': _('Tax Class') },
        'exclude_from_loyalty': {'label': _('Exclude from loyalty program') },
        #'sort_order': { 'label': _('Sort Order') },
        })

    class ProductForm(ProductModelForm):
        price = LocalizedDecimalField(label=_('Base Price'))
        new_price_rule = BooleanField()
        exclude_from_loyalty = BooleanField()
        option_forms = HiddenField(default='0')
        select_tax_class = SelectField(label=_('Tax Class'))
        select_category = SelectField(label=_('Category'))
        clone_image_from_product = HiddenField()
        enabled_locations = MultiCheckboxField(label=_('Locations'))
        enabled_bars = MultiCheckboxField(label=_('Bars'))
        menus = MultiCheckboxField(label=_('Menus'))
        image = FileField(label=_('Image'), validators=[validators.length(max=1000000, message=_('The image file is too large, must be maximum 1 MB.'))])

        def set_merchant(self, merchant, group):
            self.select_tax_class.choices = [('','')]
            self.select_tax_class.choices.extend([(str(tax_class.key()), tax_class) for tax_class in TaxClass.all().filter('merchant =', merchant)])
            self.select_tax_class.choices.append(('new',_('Create new tax class...')))
            self.select_category.choices = []
            self.select_category.choices.extend([(str(category.key()), category) for category in Category.all().filter('merchant =', merchant).filter('group = ', group)])
            self.select_category.choices.append(('new',_('Create new category...')))
            self.enabled_locations.choices = [(location.key(), location) for location in merchant.locations]
            bars = []
            for location in merchant.locations:
                bars.extend([(bar.key(), location.name + ': ' + bar.name) for bar in location.bars])
            self.enabled_bars.choices = bars
            self.menus.choices = [(menu.key(), menu) for menu in merchant.menus]
        
        def set_selected_tax_class(self, tax_class_key):
            self.select_tax_class.data = tax_class_key
        
        def set_selected_category(self, category_key):
            self.select_category.data = category_key
    return ProductForm

def get_product_option_form():
    ProductOptionModelForm = model_form(ProductOption, field_args={
        'name': { 'label': _('Name') },
        'sort_order': { 'label': _('Sort Order') },
        'required': { 'label': _('Required') }
    })

    class ProductOptionForm(ProductOptionModelForm):
        option_id = HiddenField()
        deleted = HiddenField()
        #sort_order = HiddenField()
        value_prices = DecimalListPropertyField()
        selection_type = SelectField(label=_('Let Customer Select'), choices=[('single', _('One')), ('multiple', _('Multiple'))])
    
        def __init__(self, formdata=None, obj=None, prefix='', **kwargs):
            if not prefix:
                prefix = 'option-${option_index}'
            super(ProductOptionForm, self).__init__(formdata, obj, prefix, **kwargs)
            if not formdata and not obj:
                self.name.data = '${name}'
            if obj:
                self.option_id.data = obj.key().id()
            
        def get_option_values(self):
            option_values = []
            option_value_labels = self.value_labels.data
            option_value_prices = self.value_prices.data
            if option_value_prices:
                option_value_prices = map(self.value_prices.make_value_from_datastore, option_value_prices)
            for i in range(len(option_value_labels)):
                option_values.append({'label': option_value_labels[i], 'price': option_value_prices[i]})
            return option_values
    return ProductOptionForm
        
def get_price_rule_form():
    PriceRuleModelForm = model_form(PriceRule, exclude=('merchant', 'schedule'), field_args={
        'name': { 'label': _('Name') },
        'quantity': { 'label': _('Quantity') },
        'amount': { 'label': _('Amount') },
    })

    class PriceRuleForm(PriceRuleModelForm):
        amount = LocalizedDecimalField()
        calculation = SelectField(label=_('Calculation'))
        select_schedule = SelectField(label=_('Rule Effective In'))

        def set_merchant(self, merchant):
            self.calculation.choices = PriceRule.get_calculation_descriptions(currency=merchant.currency).items()
            self.select_schedule.choices = [('',_('Never'))]
            self.select_schedule.choices.extend([(str(schedule.key()), schedule) for schedule in PriceRuleSchedule.all().filter('merchant =', merchant)])
            self.select_schedule.choices.append(('new',_('Create new schedule...')))

        def set_selected_schedule(self, schedule_key):
            self.select_schedule.data = schedule_key
    return PriceRuleForm

def get_menu_form():
    MenuModelForm = model_form(Menu, exclude=('merchant', 'schedule'), field_args={
        'name': { 'label': _('Name') },
    })

    class MenuForm(MenuModelForm):
        select_schedule = SelectField(label=_('Menu Effective In'))

        def set_merchant(self, merchant):
            self.select_schedule.choices = [('',_('Never'))]
            self.select_schedule.choices.extend([(str(schedule.key()), schedule) for schedule in PriceRuleSchedule.all().filter('merchant =', merchant)])
            self.select_schedule.choices.append(('new',_('Create new schedule...')))

        def set_selected_schedule(self, schedule_key):
            self.select_schedule.data = schedule_key
    return MenuForm

