import unittest
from google.appengine.api import memcache
from google.appengine.ext import db
from google.appengine.ext import testbed
from models.catalog import Menu, PriceRuleSchedule
from models.merchants import Merchant, Location, Bar
import datetime, pytz

class MenuTestCase(unittest.TestCase):
    def setUp(self):
        # First, create an instance of the Testbed class.
        self.testbed = testbed.Testbed()
        # Then activate the testbed, which prepares the service stubs for use.
        self.testbed.activate()
        # Next, declare which service stubs you want to use.
        self.testbed.init_datastore_v3_stub()
        self.testbed.init_memcache_stub()  
        self.testbed.init_search_stub()
        # Data
        self.merchant = Merchant(name="Test merchant",location=db.GeoPt('55.4,10.4'))
        self.merchant.put()

        self.location = Location(merchant=self.merchant, name="Test location",location=db.GeoPt('55.4,10.4'), enabled=True)
        self.location.put()
        
        self.bar = Bar(merchant=self.merchant, location=self.location, name="Test bar", enabled=True, counter_service=True, table_service=True)
        self.bar.put()

        self.always = PriceRuleSchedule(merchant=self.merchant, name=str("Always"), hours0=range(24), hours1=range(24), hours2=range(24), hours3=range(24), hours4=range(24), hours5=range(24), hours6=range(24))
        self.always.put()

        self.never = PriceRuleSchedule(merchant=self.merchant, name=str("Never"), hours0=[], hours1=[], hours2=[], hours3=[], hours4=[], hours5=[], hours6=[])
        self.never.put()

        self.menu = Menu(merchant=self.merchant, name="Test menu")
        self.menu.put()
        
    def tearDown(self):
        self.testbed.deactivate()
        
    def testActive(self):
        self.menu.schedule = self.always
        self.menu.put()
        self.assertEqual(self.menu.active, True)

        self.menu.schedule = self.never
        self.menu.put()
        self.assertEqual(self.menu.active, False)

    def testAddBar(self):
        self.menu.bars = []

        self.menu.add_bar(self.bar)
        self.assertEqual(len(self.menu.bars), 1)

        self.menu.add_bar(self.bar)
        self.assertEqual(len(self.menu.bars), 1) 

    def testRemoveBar(self):
        self.menu.bars = []

        self.menu.add_bar(self.bar)
        self.assertEqual(len(self.menu.bars), 1)

        self.menu.remove_bar(self.bar)
        self.assertEqual(len(self.menu.bars), 0) 