import unittest
from google.appengine.api import memcache
from google.appengine.ext import db
from google.appengine.ext import testbed
from google.appengine.datastore import datastore_stub_util
import models.merchants
from models.merchants import *
from models.catalog import *
from models.sales import Order
from models.quote import Quote
from models.consumers import Consumer
import helpers
models.merchants._ = helpers.dummy_gettext
from helpers import DummyPaymentSubscription, DummyPayment
from decimal import Decimal
import datetime, pytz

class MerchantInvoiceTestCase(unittest.TestCase):
    def setUp(self):
        # First, create an instance of the Testbed class.
        self.testbed = testbed.Testbed()
        # Then activate the testbed, which prepares the service stubs for use.
        self.testbed.activate()
        # Next, declare which service stubs you want to use.
        #self.testbed.init_datastore_v3_stub()
        self.testbed.init_memcache_stub()
        self.testbed.init_taskqueue_stub()
        self.testbed.init_search_stub()
        
        # Data
        self.policy = datastore_stub_util.PseudoRandomHRConsistencyPolicy(probability=1)
        self.testbed.init_datastore_v3_stub(consistency_policy=self.policy)

        # default pricetier - needs to be in the datastore
        pt = PriceTier(key_name="DKK")
        pt.currency = "DKK"
        pt.set_list('min_value', [Decimal("0.00"), Decimal("90.00"), Decimal("225.00"), Decimal("885.00")])
        pt.set_list('fee', [Decimal("12.50"), Decimal("12.00"), Decimal("11.50"), Decimal("11.00")])
        pt.set_list('discount_min_value', [0, 25, 100, 200, 500])
        pt.set_list('discount_percentage', [Decimal("0.00"), Decimal("10.00"), Decimal("15.00"), Decimal("20.00"), Decimal("25.00")])
        pt.fixed_fee = Decimal('0.00')
        pt.put() 

        self.merchant = Merchant(name="Test merchant",location=db.GeoPt('55.4,10.4'), prices_include_tax=True, currency='DKK', fee_rate=Decimal('1.00'), fee_rate_use_default=True, country='DK', pricetier=pt)
        self.merchant.put()
        self.location = Location(merchant=self.merchant, name="Test location",location=db.GeoPt('55.4,10.4'), enabled=True)
        self.location.put()
        self.bar = Bar(merchant=self.merchant, location=self.location, name="Test bar", enabled=True, counter_service=True, table_service=True)
        self.bar.put()
        always = PriceRuleSchedule(merchant=self.merchant,name="Always", hours0=range(24), hours1=range(24), hours2=range(24), hours3=range(24), hours4=range(24), hours5=range(24), hours6=range(24))
        always.put()
        self.menu = Menu(merchant=self.merchant, name="All day menu", schedule=always, bars=[self.bar.key()])
        self.menu.put()
        self.category = Category.construct_for_merchant(self.merchant, name="Drinks", group="menu")
        self.category.put()
        self.tax_class = TaxClass(merchant=self.merchant, name="VAT",rate=Decimal("25.0"), is_default=True)
        self.tax_class.put()
        self.product1 = Product.construct_for_merchant(self.merchant, name="Drink 1", price=Decimal("10.0"), category=self.category, tax_class=self.tax_class, menus=[self.menu.key()])
        self.product1.put()
        self.product2 = Product.construct_for_merchant(self.merchant, name="Drink 2", price=Decimal("12.5"), category=self.category, tax_class=self.tax_class, menus=[self.menu.key()])
        self.product2.put()
        self.consumer = Consumer()
        self.consumer.put()
        subscrip = DummyPaymentSubscription(parent=self.consumer,status='active')
        subscrip.put()
        self.consumer.current_payment_subscription = subscrip
        self.consumer.put()

        
    def placeOrder(self):
        cart_items = [{
            'product': str(self.product1.key()),
            'qty': 1,
            'options': []
        },
        {
            'product': str(self.product2.key()),
            'qty': 2,
            'options': []
        }]
        quote = Quote(None, self.location, self.bar)
        quote.set_cart_items(cart_items)
        quote.service = 'table'
        quote.table_number = '12'
        order = quote.place_order(self.consumer)
        return order
        
    def testMerchantInvoice(self): #default pricetier
        num_orders = 10
        merchant_fee_rate = Decimal('1.00') # %
        order_value = Decimal('35.00') # DKK
        tax_rate = Decimal('25.00') # %
        # Place orders
        for i in range(num_orders):
            order = self.placeOrder()
            result = order.change_status('complete')
            self.assertTrue(result)
            self.assertEqual('complete', order.status)
        # Find merchant invoice
        openinvoices = MerchantInvoice.all().filter('open =', True)
        self.assertEqual(1, openinvoices.count())
        invoice = openinvoices.get()
        self.assertTrue(invoice.open)
        # Close merchant invoice
        invoice.close()
        self.assertFalse(invoice.open)
        # Fixed fee per order 
        self.assertEqual(self.merchant.get_pricetier().fixed_fee, Decimal('0.00'))
        self.assertEqual(Decimal('0.00'), num_orders * self.merchant.get_pricetier().fixed_fee)
        # Validate merchant invoice content
        self.assertEqual(num_orders, invoice.order_count)
        self.assertEqual(Decimal('35.0'), invoice.orders.get().total_incl_tax)
        self.assertEqual(num_orders*order_value, invoice.order_revenue)
        self.assertEqual(num_orders*(order_value*self.merchant.get_fee_for_order(order_value)/100-order_value) + num_orders*self.merchant.get_pricetier().fixed_fee, invoice.total_incl_tax)
        invoiceitems = invoice.items.fetch(10)
        orderfees = next(item for item in invoiceitems if item.sku == 'orderfees')
        self.assertEqual(num_orders*order_value*self.merchant.get_fee_for_order(order_value)/100, orderfees.total_incl_tax)
        revenues = [item for item in invoiceitems if item.sku == 'revenue']
        self.assertEqual(1, len(revenues))
        self.assertEqual(tax_rate, revenues[0].tax_rate)        



    def testMerchantInvoiceCustomPriceTier(self):
        self.merchant.fee_rate_use_default = False
        self.pricetier = PriceTier()
        self.pricetier.set_list('min_value', [Decimal('0.00')])
        self.pricetier.set_list('fee', [Decimal('0.00')])
        self.pricetier.set_list('discount_percentage', [Decimal('5.00')])
        self.pricetier.set_list('discount_min_value', [0])
        self.pricetier.fixed_fee = Decimal('1.50')
        self.pricetier.currency = "DKK"
        self.pricetier.put()
        self.merchant.pricetier = self.pricetier
        self.merchant.put()

        num_orders = 10
        merchant_fee_rate = Decimal('1.00') # %
        order_value = Decimal('35.00') # DKK
        tax_rate = Decimal('25.00') # %
        # Place orders
        for i in range(num_orders):
            order = self.placeOrder()
            result = order.change_status('complete')
            self.assertTrue(result)
            self.assertEqual('complete', order.status)
        # Find merchant invoice
        openinvoices = MerchantInvoice.all().filter('open =', True)
        self.assertEqual(1, openinvoices.count())
        invoice = openinvoices.get()
        self.assertTrue(invoice.open)
        # Close merchant invoice
        invoice.close()
        self.assertFalse(invoice.open)
        # Validate merchant invoice contents
        self.assertEqual(num_orders, invoice.order_count)
        self.assertEqual(Decimal('35.0'), invoice.orders.get().total_incl_tax)

        # Fixed fee per order 
        self.assertEqual(self.merchant.get_pricetier().fixed_fee, Decimal('1.50'))
        self.assertEqual(Decimal('15.0'), num_orders * self.merchant.get_pricetier().fixed_fee)

        self.assertEqual(num_orders*order_value, invoice.order_revenue)
        self.assertEqual(num_orders*(order_value*self.merchant.get_fee_for_order(order_value)/100-order_value) + num_orders*self.merchant.get_pricetier().fixed_fee, invoice.total_incl_tax)
        invoiceitems = invoice.items.fetch(10)
        orderfees = next(item for item in invoiceitems if item.sku == 'orderfees')
        self.assertEqual(num_orders*order_value*self.merchant.get_fee_for_order(order_value)/100, orderfees.total_incl_tax)
        revenues = [item for item in invoiceitems if item.sku == 'revenue']
        self.assertEqual(1, len(revenues))
        self.assertEqual(tax_rate, revenues[0].tax_rate)


    def testMerchantInvoiceOneOrder(self):
        self.merchant.fee_rate_use_default = False
        self.pricetier = PriceTier()
        self.pricetier.set_list('min_value', [Decimal('0.00'),Decimal('11.00')])
        self.pricetier.set_list('fee', [Decimal('5.00','15.00')])
        self.pricetier.set_list('discount_percentage', [Decimal('0.00')])
        self.pricetier.set_list('discount_min_value', [0])
        self.pricetier.currency = "DKK"
        self.pricetier.fixed_fee = Decimal('1.50')
        self.pricetier.put()
        self.merchant.pricetier = self.pricetier
        self.merchant.put()

        num_orders = 1
        order_values = [Decimal('10.00'),Decimal('12.50'),Decimal('12.50')]
        tax_rate = Decimal('25.00') # %

        # Place orders
        order = self.placeOrder()
        result = order.change_status('complete')
        self.assertTrue(result)
        self.assertEqual('complete', order.status)

        # Find merchant invoice
        openinvoices = MerchantInvoice.all().filter('open =', True)
        self.assertEqual(1, openinvoices.count())
        invoice = openinvoices.get()
        self.assertTrue(invoice.open)
        # Close merchant invoice
        invoice.close()
        self.assertFalse(invoice.open)
        # Validate merchant invoice contents

        self.assertEqual(num_orders, invoice.order_count)
        self.assertEqual(Decimal('35.0'), invoice.orders.get().total_incl_tax)
        self.assertEqual(sum(order_values), invoice.order_revenue)
        # Fixed fee
        self.assertEqual(self.merchant.get_pricetier().fixed_fee, Decimal('1.50'))

        s = 1 * Decimal('10.00')*self.merchant.get_fee_for_order(Decimal('10.00'))/100
        s = s + (2 * Decimal('12.50')*self.merchant.get_fee_for_order(Decimal('12.50'))/100)
        s = s + self.merchant.get_pricetier().fixed_fee
        self.assertEqual( s - invoice.orders.get().total_incl_tax , invoice.total_incl_tax)
        








