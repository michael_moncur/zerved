import unittest
from google.appengine.api import memcache
from google.appengine.ext import db
from google.appengine.ext import testbed
from google.appengine.datastore import datastore_stub_util
from models.merchants import *
from models.catalog import *
from models.sales import Order
from models.quote import Quote
from models.consumers import Consumer
import helpers
from helpers import DummyPaymentSubscription, DummyPayment
from decimal import Decimal
import datetime, pytz

class QuoteTestCase(unittest.TestCase):
    def setUp(self):
        # First, create an instance of the Testbed class.
        self.testbed = testbed.Testbed()
        # Then activate the testbed, which prepares the service stubs for use.
        self.testbed.activate()
        # Next, declare which service stubs you want to use.
        self.testbed.init_datastore_v3_stub()
        self.testbed.init_memcache_stub()
        self.testbed.init_search_stub()
        
        # Data
        self.merchant = Merchant(name="Test merchant",location=db.GeoPt('55.4,10.4'), prices_include_tax=True, currency='DKK', country='DK')
        self.merchant.put()
        self.location = Location(merchant=self.merchant, name="Test location",location=db.GeoPt('55.4,10.4'), enabled=True)
        self.location.put()
        self.bar = Bar(merchant=self.merchant, location=self.location, name="Test bar", enabled=True, counter_service=True, table_service=True)
        self.bar.put()
        always = PriceRuleSchedule(merchant=self.merchant,name="Always", hours0=range(24), hours1=range(24), hours2=range(24), hours3=range(24), hours4=range(24), hours5=range(24), hours6=range(24))
        always.put()
        self.menu = Menu(merchant=self.merchant, name="All day menu", schedule=always, bars=[self.bar.key()])
        self.menu.put()
        self.category = Category.construct_for_merchant(self.merchant, name="Drinks", group="menu")
        self.category.put()
        self.tax_class = TaxClass(merchant=self.merchant, name="VAT",rate=Decimal("25.0"), is_default=True)
        self.tax_class.put()
        self.product1 = Product.construct_for_merchant(self.merchant, name="Drink 1", price=Decimal("10.0"), category=self.category, tax_class=self.tax_class, menus=[self.menu.key()])
        self.product1.put()
        self.product2 = Product.construct_for_merchant(self.merchant, name="Drink 2", price=Decimal("12.5"), category=self.category, tax_class=self.tax_class, menus=[self.menu.key()])
        self.product2.put()
        self.consumer = Consumer()
        self.consumer.put()
        subscrip = DummyPaymentSubscription(parent=self.consumer,status='active')
        subscrip.put()
        self.consumer.current_payment_subscription = subscrip
        self.consumer.put()
        
    def tearDown(self):
        self.testbed.deactivate()
        
    def testQuote(self):
        # Add items
        cart_items = [{
            'product': str(self.product1.key()),
            'qty': 1,
            'options': []
        },
        {
            'product': str(self.product2.key()),
            'qty': 2,
            'options': []
        }]
        quote = Quote(None, self.location, self.bar)
        quote.set_cart_items(cart_items)
        quote_items = quote.get_quote_items()
        self.assertEqual(2, len(quote_items))
        self.assertEqual(Decimal('35'), quote.get_total())
        item1 = quote_items[0]
        self.assertEqual(1, item1['qty'])
        self.assertEqual(Decimal('10.0'), item1['price'])
        self.assertEqual(Decimal('2.0'), item1['total_tax'])
        self.assertEqual(Decimal('8.0'), item1['base_price_excl_tax'])
        self.assertEqual(Decimal('10.0'), item1['total_incl_tax'])
        item2 = quote_items[1]
        self.assertEqual(2, item2['qty'])
        self.assertEqual(Decimal('25.0'), item2['price'])
        self.assertEqual(Decimal('5.0'), item2['total_tax'])
        self.assertEqual(Decimal('10.0'), item2['base_price_excl_tax'])
        self.assertEqual(Decimal('25.0'), item2['total_incl_tax'])
        
    def testTip(self):
        quote = Quote(None, self.location, self.bar)
        quote.set_cart_items([{
            'product': str(self.product1.key()),
            'qty': 1,
            'options': []
        }])
        quote.tip_percent = Decimal('5')
        self.assertEqual(Decimal('10'), quote.get_subtotal())
        self.assertEqual(Decimal('0.5'), quote.get_tip())
        self.assertEqual(Decimal('10.5'), quote.get_total())
        self.assertEqual(Decimal('2'), quote.get_total_tax())
        
    def testFees(self):
        self.merchant.service_charge = Decimal('10')
        quote = Quote(None, self.location, self.bar)
        quote.set_cart_items([{
            'product': str(self.product1.key()),
            'qty': 1,
            'options': []
        }])
        self.assertEqual(Decimal('10'), quote.get_subtotal())
        self.assertEqual(Decimal('0'), quote.get_tip())
        self.assertEqual(Decimal('11'), quote.get_total())
        self.assertEqual(Decimal('2.2'), quote.get_total_tax())
        fee = quote.get_fees()[0]
        self.assertEqual(Decimal('1'), fee['price'])
        self.assertEqual(Decimal('1'), fee['price_incl_tax'])
        self.assertEqual(Decimal('0.8'), fee['price_excl_tax'])
        
    def testPlaceOrder(self):
        cart_items = [{
            'product': str(self.product1.key()),
            'qty': 1,
            'options': []
        },
        {
            'product': str(self.product2.key()),
            'qty': 2,
            'options': []
        }]
        quote = Quote(None, self.location, self.bar)
        quote.set_cart_items(cart_items)
        quote.service = 'table'
        quote.table_number = '12'
        order = quote.place_order(self.consumer)
        self.assertEqual('pending', order.status)
        self.assertEqual(2, order.items.count())
        self.assertEqual('table', order.service)
        self.assertEqual('12', order.table_number)
        self.assertEqual(Decimal('35.0'), order.total_incl_tax)
        self.assertEqual(Decimal('28.0'), order.total_excl_tax)
        self.assertEqual(Decimal('7.0'), order.total_tax)
        
    def testOrderNumber(self):
        no1 = self.merchant.reserve_order_number()
        no2 = self.merchant.reserve_order_number()
        self.assertEqual(no1+1, no2)
        