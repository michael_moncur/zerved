import unittest
from google.appengine.api import memcache
from google.appengine.ext import db
from google.appengine.ext import testbed
from models.merchants import Merchant, TaxClass
import models.catalog
from models.catalog import *
import helpers
from decimal import Decimal
import datetime, pytz

class TaxClassTestCase(unittest.TestCase):
    def setUp(self):
        # First, create an instance of the Testbed class.
        self.testbed = testbed.Testbed()
        # Then activate the testbed, which prepares the service stubs for use.
        self.testbed.activate()
        # Next, declare which service stubs you want to use.
        self.testbed.init_datastore_v3_stub()
        self.testbed.init_memcache_stub()
        self.testbed.init_search_stub()
        
        # Data
        self.merchant = Merchant(name="Test merchant",location=db.GeoPt('55.4,10.4'))
        self.merchant.put()
        TaxClass(merchant=self.merchant, name="No VAT", rate=Decimal("0.0"), is_default=False).put()
        TaxClass(merchant=self.merchant, name="VAT",rate=Decimal("25.0"), is_default=True).put()
        
    def tearDown(self):
        self.testbed.deactivate()
        
    def testDefaultVat(self):
        taxclass = TaxClass.get_default_tax_class(self.merchant)
        self.assertEqual("VAT", taxclass.name)
        
    def testVatList(self):
        taxclasses = TaxClass.get_list_of_tuples(self.merchant)
        self.assertEqual(2, len(taxclasses))
        
class CategoryTestCase(unittest.TestCase):
    def setUp(self):
        # First, create an instance of the Testbed class.
        self.testbed = testbed.Testbed()
        # Then activate the testbed, which prepares the service stubs for use.
        self.testbed.activate()
        # Next, declare which service stubs you want to use.
        self.testbed.init_datastore_v3_stub()
        self.testbed.init_memcache_stub()
        self.testbed.init_search_stub()
        
        # Data
        self.merchant = Merchant(name="Test merchant",location=db.GeoPt('55.4,10.4')).put()
        
    def tearDown(self):
        self.testbed.deactivate()
        
    def testMerchantCategory(self):
        Category.construct_for_merchant(self.merchant, name="Drinks", group="menu").put()
        Category.construct_for_merchant(self.merchant, name="Beer", group="menu").put()
        categories = Category.all().filter('merchant =', self.merchant).order('sort_order').fetch(10)
        self.assertEqual(2, len(categories))
        self.assertEqual(0, categories[0].sort_order)
        self.assertEqual(1, categories[1].sort_order)
        
class ProductTestCase(unittest.TestCase):
    def setUp(self):
        # First, create an instance of the Testbed class.
        self.testbed = testbed.Testbed()
        # Then activate the testbed, which prepares the service stubs for use.
        self.testbed.activate()
        # Next, declare which service stubs you want to use.
        self.testbed.init_datastore_v3_stub()
        self.testbed.init_memcache_stub()
        self.testbed.init_search_stub()

        # Data
        self.merchant = Merchant(name="Test merchant",location=db.GeoPt('55.4,10.4'), prices_include_tax=True)
        self.merchant.put()
        self.category = Category.construct_for_merchant(self.merchant, name="Drinks", group="menu").put()
        self.tax_class = TaxClass(merchant=self.merchant, name="VAT",rate=Decimal("25.0"), is_default=True).put()
        Product.construct_for_merchant(self.merchant, name="Drink 1", price=Decimal("10.0"), category=self.category, tax_class=self.tax_class).put()
        Product.construct_for_merchant(self.merchant, name="Drink 2", price=Decimal("12.5"), category=self.category, tax_class=self.tax_class).put()
        self.product = Product.all().filter('merchant =', self.merchant).filter('category =', self.category).order('sort_order').get()

    def tearDown(self):
        self.testbed.deactivate()

    def testMerchantProduct(self):
        products = Product.all().filter('merchant =', self.merchant).filter('category =', self.category).order('sort_order').fetch(10)
        self.assertEqual(2, len(products))
        self.assertEqual(0, products[0].sort_order)
        self.assertEqual(1, products[1].sort_order)
        
    def testBasePrice(self):
        self.assertEqual(Decimal('10.0'), self.product.get_base_price(incl_tax=True))
        self.assertEqual(Decimal('8.0'), self.product.get_base_price(incl_tax=False))
        self.assertEqual(Decimal('2.0'), self.product.get_tax())
    
    def testPrice(self):
        self.assertEqual(Decimal('10.0'), self.product.get_price())
        self.assertEqual(Decimal('10.0'), self.product.final_price)
        self.assertEqual(Decimal('20.0'), self.product.get_price(2))
        self.assertEqual(Decimal('16.0'), self.product.get_price(quantity=2, incl_tax=False))
        
    def testTaxClass(self):
        self.assertEqual("VAT", self.product.get_tax_class().name)
        
    def testTaxRate(self):
        self.assertEqual(Decimal('25.0'), self.product.get_tax_rate())
        
class PriceRuleTestCase(unittest.TestCase):
    def setUp(self):
        # First, create an instance of the Testbed class.
        self.testbed = testbed.Testbed()
        # Then activate the testbed, which prepares the service stubs for use.
        self.testbed.activate()
        # Next, declare which service stubs you want to use.
        self.testbed.init_datastore_v3_stub()
        self.testbed.init_memcache_stub()
        self.testbed.init_search_stub()

        # Data
        self.merchant = Merchant(name="Test merchant",location=db.GeoPt('55.4,10.4'), prices_include_tax=True, currency='DKK')
        self.merchant.put()
        self.category = Category.construct_for_merchant(self.merchant, name="Drinks", group="menu").put()
        self.tax_class = TaxClass(merchant=self.merchant, name="VAT",rate=Decimal("25.0"), is_default=True).put()
        # Schedules
        now = datetime.datetime.now().replace(tzinfo=pytz.utc).astimezone(pytz.timezone(self.merchant.timezone))
        weekday = now.weekday()
        hour = now.hour
        self.validschedule = PriceRuleSchedule(merchant=self.merchant, name="Valid schedule")
        setattr(self.validschedule, 'hours'+str(weekday), [hour])
        self.validschedule.put()
        self.invalidschedule = PriceRuleSchedule(merchant=self.merchant, name="Invalid schedule")
        hour = hour + 1
        if hour == 24:
            hour = 0
        setattr(self.invalidschedule, 'hours'+str(weekday), [hour])
        self.invalidschedule.put()
        # Price rules
        self.pricerule = PriceRule(merchant=self.merchant, name="10 pct. discount", quantity=1, amount=Decimal('10'), calculation='percentdiscount', schedule=self.validschedule)
        self.pricerule.put()
        self.intervalpricerule = PriceRule(merchant=self.merchant, name="Buy 3 save 8", quantity=3, amount=Decimal('8'), calculation='fixeddiscount_interval', schedule=self.validschedule)
        self.intervalpricerule.put()
        self.invalidpricerule = PriceRule(merchant=self.merchant, name="Not valid now", quantity=1, amount=Decimal('10'), calculation='percentdiscount', schedule=self.invalidschedule)
        self.invalidpricerule.put()
        # Products
        self.product = Product.construct_for_merchant(self.merchant, name="Drink 1", price=Decimal("10.0"), category=self.category, tax_class=self.tax_class)
        self.product.add_price_rule(self.pricerule.key())
        self.product.add_price_rule(self.intervalpricerule.key())
        self.product.add_price_rule(self.invalidpricerule.key())
        self.product.put()
        Product.construct_for_merchant(self.merchant, name="Drink 2", price=Decimal("12.5"), category=self.category, tax_class=self.tax_class).put()

    def tearDown(self):
        self.testbed.deactivate()
        
    def testSummary(self):
        schedule = PriceRuleSchedule(merchant=self.merchant, name="Schedule", hours0=[18,19], hours2=[9,10,18,19])
        schedule.put()
        self.assertEqual("Mon 18-20, Wed 09-11 + 18-20", schedule.hours_summary)
        
    def testValidHour(self):
        schedule = PriceRuleSchedule(merchant=self.merchant, name="Schedule", hours0=[18,19], hours2=[9,10,18,19])
        schedule.put()
        self.assertTrue(schedule.valid_hour(2, 10))
        self.assertFalse(schedule.valid_hour(2, 11))
        self.assertFalse(schedule.valid_hour(1, 10))
        
    def testValidSchedules(self):
        schedules = PriceRuleSchedule.get_valid_schedules(self.merchant).fetch(10)
        self.assertEqual(1, len(schedules))
        self.assertEqual("Valid schedule", schedules[0].name)
        
    def testCurrentPriceRules(self):
        pricerules = PriceRule.get_current_price_rules(self.merchant)
        pricerulenames = [p.name for p in pricerules]
        self.assertEqual(2, len(pricerules))
        self.assertIn("10 pct. discount", pricerulenames)
        self.assertIn("Buy 3 save 8", pricerulenames)
        self.assertNotIn("Not valid now", pricerulenames)
        
    def testPriceRuleTexts(self):
        self.assertEqual("Buy 1 or more, save 10 %", self.pricerule.summary())
        self.assertEqual("Buy 3, save 8 DKK", self.intervalpricerule.summary())
        
    def testPriceRuleCalculations(self):
        self.assertEqual(Decimal('9.0'), self.product.get_price(1)) # -10 %
        self.assertEqual(Decimal('18.0'), self.product.get_price(2)) # 2 x -10%
        self.assertEqual(Decimal('22.0'), self.product.get_price(3)) # Buy 3 save 8
        self.assertEqual(Decimal('32.0'), self.product.get_price(4)) # Buy 3 save 8 + normal price
        self.assertEqual(Decimal('44.0'), self.product.get_price(6)) # 2 x buy 3 save 8
