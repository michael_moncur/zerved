# Define dummy payment models to use instead of ePay so we don't have to call ePay
from models.payment import PaymentSubscription, Payment
from decimal import Decimal

class DummyPaymentSubscription(PaymentSubscription):
    def initialize_payment(self, order, amount):
        payment = DummyPayment(
            subscription=self,
            order=order, 
            amount=amount, 
            status='new')
        return payment
    
class DummyPayment(Payment):
    def authorize(self):
        self.status = 'authorized'
        self.fee = Decimal('1.00')
        self.put()
        
    def capture(self):
        self.status = 'captured'
        self.put()
        return True
        
# Override the i18n functions because they don't work without a HTTP request
from webapp2_extras import i18n

def dummy_format_currency(number, currency, format=None):
    return str(number) + ' ' + currency
i18n.format_currency = dummy_format_currency

def dummy_gettext(msg):
    return msg
i18n.gettext = dummy_gettext

def dummy_format_time(time, format, rebase):
    return str(time)
i18n.format_time = dummy_format_time