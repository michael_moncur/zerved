import unittest
from google.appengine.api import memcache
from google.appengine.ext import db
from google.appengine.ext import testbed
from models.merchants import Merchant
from decimal import Decimal
import datetime, pytz

class MerchantTestCase(unittest.TestCase):
    def setUp(self):
        # First, create an instance of the Testbed class.
        self.testbed = testbed.Testbed()
        # Then activate the testbed, which prepares the service stubs for use.
        self.testbed.activate()
        # Next, declare which service stubs you want to use.
        self.testbed.init_datastore_v3_stub()
        self.testbed.init_memcache_stub()
        self.testbed.init_search_stub()
        
        # Data
        self.merchant = Merchant(name="Test merchant",location=db.GeoPt('55.4,10.4'))
        self.merchant.put()
        
    def tearDown(self):
        self.testbed.deactivate()
        
    def testOrderNumber(self):
        id1 = self.merchant.reserve_order_number()
        id2 = self.merchant.reserve_order_number()
        self.assertEqual(id1+1, id2)
        
    def testFee(self):
        merchant2 = Merchant(name="Test merchant 2",location=db.GeoPt('55.4,10.4'), fee_rate=Decimal('4.00'), fee_rate_use_default=False)
        self.assertEqual(Merchant.DEFAULT_FEE_RATE, self.merchant.get_fee_rate())
        self.assertEqual(Decimal('4.00'), merchant2.get_fee_rate())
        