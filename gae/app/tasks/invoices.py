import webapp2
from google.appengine.ext import db
from models.merchants import MerchantInvoice
import datetime
import logging

class CloseInvoicesHandler(webapp2.RequestHandler):
    def get(self):
    	""" Close all open invoices to settle transactions with merchants (run this weekly) """
        invoices = MerchantInvoice.all().filter('open =', True).fetch(1000)
        for invoice in invoices:
            invoice.close()
        logging.info("Closed %s merchant invoices" % len(invoices))