import webapp2, csv
from google.appengine.ext import db
from models.merchants import Merchant, LoyaltyCode
import logging

class CreateLoyaltyCodesHandler(webapp2.RequestHandler):
    def post(self):
        #logging.info('Creating loyalty codes %s for merchant %s' % (self.request.get('csv_chunk'),self.request.get('merchant')))
        
        codes = self.request.get('csv_chunk').replace(' ','').replace('\'','').replace('[','').replace(']','').split(',')
        
        merchant = Merchant.get(self.request.get('merchant'))
        logging.info('Adding %i loyalty codes for merchant %s' % (len(codes), merchant.name))
        for code in codes:
            LoyaltyCode(merchant=merchant, code=code).put()
            logging.info('New loyalty code %s for merchant %s created' % (code, merchant.name))


class DeleteLoyaltyCodesHandler(webapp2.RequestHandler):
    def post(self):
        merchant = Merchant.get(self.request.get('merchant'))
        existing_loyalty_codes_for_merchant_query = LoyaltyCode.all().filter('merchant =', merchant)
                
        existing_loyalty_codes_for_merchant = existing_loyalty_codes_for_merchant_query.fetch(limit=100000,keys_only=True) 
#        while existing_loyalty_codes_for_merchant:
        logging.debug('Deleting %i loyalty codes for merchant %s' % (len(existing_loyalty_codes_for_merchant), merchant.name))
        for an_old_code in existing_loyalty_codes_for_merchant:
            db.delete(an_old_code)
#            existing_loyalty_codes_for_merchant = existing_loyalty_codes_for_merchant_query.fetch(limit=100,keys_only=True) 
