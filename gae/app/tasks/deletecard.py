import webapp2
from models.payment import PaymentSubscription

class DeleteCardHandler(webapp2.RequestHandler):
    def post(self):
        ps = PaymentSubscription.get(self.request.get('key'))
        if ps:
            ps.delete()
