import webapp2
import time
import logging
from google.appengine.api.logservice import logservice
from google.appengine.api import mail

class MonitorHandler(webapp2.RequestHandler):
	def get(self):
		""" Use this script to get notified when too many errors are logged """
		# Check the log for [max_errors] errors within the last [hours] hours
		hours = int(self.request.get('hours')) if self.request.get('hours') else None
		max_errors = int(self.request.get('max_errors')) if self.request.get('max_errors') else None
		if hours and max_errors:
			start_time=time.time()-60*60*hours
			end_time=time.time()
			requests = logservice.fetch(start_time=start_time, end_time=end_time, minimum_log_level=logservice.LOG_LEVEL_ERROR)
			count = 0
			for req_log in requests:
				count += 1
				if count >= max_errors:
					# Too many errors detected
					logging.info("High error rate detected during the last %s hour(s)" % hours)
					message = mail.EmailMessage()
					message.sender = "Zerved <info@zervedapp.com>"
					message.subject = "Increased error rate"
					message.to = "anders@zervedapp.com"
					message.body = "Zerved has detected a high error rate during the last %s hour(s)" % hours
					#message.send()
					break
