import webapp2
from google.appengine.ext import db
from models.catalog import Menu
from models.merchants import Merchant, Bar, OrderProcessingSession
from models.sales import Order
from models.payment import PaymentSubscription
from models.consumers import ConsumerNotification
from models.firezerved import _delete_all_firebase_channels
import datetime
import json
from webapp2_extras import i18n
_ = i18n.gettext
import logging

class CleanupHandler(webapp2.RequestHandler):
    def get(self):
        # Cancel orders that are pending_payment and more than an hour old
        onehourago = datetime.datetime.now() - datetime.timedelta(seconds=60*60)
        orders = Order.all().filter('status =', 'pending_payment').filter('date_created < ', onehourago).fetch(1000)
        for order in orders:
            order.status = 'cancelled'
        db.put(orders)
        if len(orders) > 0:
            logging.info("Cancelled %s orders that has been pending payment for more than an hour" % len(orders))
            
        # Cancel pending realtime menu orders more than 7 days old
        lastweek = datetime.datetime.now() - datetime.timedelta(days=7)
        orders = Order.all().filter('status =', 'pending').filter('event_name =', '').filter('date_created < ', lastweek).filter('service IN', ['counter', 'table', 'delivery']).fetch(1000)
        for order in orders:
            order.change_status('cancelled')
        if len(orders) > 0:
            logging.info("Cancelled %s orders that has been pending for more than 7 days" % len(orders))

        ## Cancel pending ticket orders more than 30 days old
        #lastmonth = datetime.datetime.now() - datetime.timedelta(days=30)
        #orders = Order.all().filter('status =', 'pending').filter('date_created < ', lastweek).filter('service =', 'entrance').fetch(1000)
        #for order in orders:
        #    order.change_status('cancelled')
        #if len(orders) > 0:
        #    logging.info("Cancelled %s ticket orders that has been pending for more than 30 days" % len(orders))
        
        # Delete payment subscriptions that are still pending after an hour
        subs = PaymentSubscription.all().filter('status =', 'pending').filter('date_created < ', onehourago)
        i=0
        for sub in subs:
            sub.delete()
            i+=1
        if i > 0:
            logging.info("Deleted %s uncompleted payment subscriptions more than an hour old" % i)
        
        # Delete inactive order processing sessions
        sessions = OrderProcessingSession.all().filter('date_created < ', onehourago).fetch(1000)
        for session in sessions:
            session.send_message(json.dumps({'reload': 1}))
        db.delete(sessions)
        if len(sessions) > 0:
            logging.info("Deleted %s inactive order processing sessions" % len(sessions))
        
        # Delete notifications that were not pulled within an hour
        notifications = ConsumerNotification.all().filter('date_created < ', onehourago).fetch(1000)
        db.delete(notifications)
        if len(notifications) > 0:
            logging.info("Deleted %s abandoned consumer notifications" % len(notifications))

        _delete_all_firebase_channels();


class CloseDisconnectedBarHandler(webapp2.RequestHandler):
    def post(self):
        # Close bar if no connected sessions
        bar = Bar.get(self.request.get('bar_key'))
        opensessions = bar.order_processing_sessions.filter('connected =', True).count(limit=1)
        logging.info("Checking for open sessions")
        if opensessions == 0:
            if not bar.persist_status:
                logging.info("No open sessions. Closing bar (current status = " + bar.status + ").")
                bar.status = 'closed'
                bar.put()
            else:
                logging.info("No open sessions, but bar must persist status, so leaving it untouched (current status = " + bar.status + ").")


class ClosePersistingBarsWithNoActiveMenus(webapp2.RequestHandler):
    def get(self):
        # Close bars with persist_status that no longer have any active menus
        bars = Bar.all().filter('persist_status = ', True)
        for bar in bars:
            menus = Menu.get_active_menus(bar)
            if menus.get() == None:
                logging.info('Disabling persistense in the bar "' + bar.name + '", and closing it.')
                bar.persist_status = False
                bar.status = 'closed'
                bar.put()
