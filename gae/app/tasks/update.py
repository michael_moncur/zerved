import webapp2
import update_schema
from google.appengine.ext import deferred

import logging
from webapp2_extras import i18n
_ = i18n.lazy_gettext

def clone_entity(e, **extra_args):
  """Clones an entity, adding or overriding constructor attributes.

  The cloned entity will have exactly the same property values as the original
  entity, except where overridden. By default it will have no parent entity or
  key name, unless supplied.

  Args:
    e: The entity to clone
    extra_args: Keyword arguments to override from the cloned entity and pass
      to the constructor.
  Returns:
    A cloned, possibly modified, copy of entity e.
  """
  klass = e.__class__
  props = dict((k, v.__get__(e, klass)) for k, v in klass.properties().iteritems())
  props.update(extra_args)
  return klass(**props)

class UpdateHandler(webapp2.RequestHandler):
    def get(self):
      deferred.defer(update_schema.UpdateSchema)
      self.response.out.write('Schema migration successfully initiated.')

