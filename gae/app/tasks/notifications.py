import os
import urllib
import webapp2
import os
from google.appengine.ext import db
from google.appengine.api import urlfetch
import apns
import urllib
import json
import logging

GCM_API_KEY = 'AIzaSyCtHpgsZxDZd5nxs3kZNq2xsoGzK04sihY'
GCM_TIME_TO_LIVE = 60*60 # Let undelivered messages die after an hour

class SendIphoneMsgHandler(webapp2.RequestHandler):
    def post(self):
        """ Send iphone push notification """
        sandbox = self.request.get('sandbox') == '1'
        if sandbox:
            service = apns.APNs(use_sandbox=True, cert_file='Zerved-Development.pem', key_file='Zerved-Development-key.pem')
        else:
            service = apns.APNs(use_sandbox=False, cert_file='Zerved-Production.pem', key_file='Zerved-Production-key.pem')
        payload = apns.Payload(alert={'body': self.request.get('message'), 'action-loc-key': self.request.get('action')}, sound="default", custom={'order_id': self.request.get('order_id')})
        logging.info(payload.json())
        service.gateway_server.send_notification(self.request.get('device_token'), payload)
        for (token_hex, fail_time) in service.feedback_server.items():
            logging.info("Processing APNS feedback")
            # TODO: Remove deregistered device tokens from consumer accounts if fail_time is older than registration time
            break
        
class SendAndroidMsgHandler(webapp2.RequestHandler):
    def post(self):
        """ Send Android GCM notification """
        url = 'https://android.googleapis.com/gcm/send'
        params = {
            'registration_ids': [self.request.get('registration_id')],
            'data': {
                'order_id': self.request.get('order_id'),
                'order_key': self.request.get('order_key'),
                'merchant_name': self.request.get('merchant_name').encode('utf-8'),
                'message': self.request.get('message').encode('utf-8'),
                'notification_key': self.request.get('notification_key')
            },
            'time_to_live': GCM_TIME_TO_LIVE
        }
        result = urlfetch.fetch(url, payload=json.dumps(params), method='POST', headers={'Content-Type': 'application/json', 'Authorization': 'key='+GCM_API_KEY}, validate_certificate=False)
        logging.info(result.status_code)
        logging.info(result.content)
        self.response.status = int(result.status_code)

class SendEmbeddedMsgHandler(webapp2.RequestHandler):
    def post(self):
        """ Call external API at merchant site """
        push_data = json.loads(self.request.get('push_data'))
        # logging.debug(self.request.get('push_data'))
        # logging.debug(self.request.get('message').encode('utf-8'))
        # print('-- push ---')
        # print('-- push ---')
        # print(push_data['merchant_id'])
        # print(push_data['external_user_id'])
        # print('-- end  ---')

        # TODO: Use push_data['merchant_id'] to setup the form fields and fetch header according to the needs of the specific merchant's API
        # The code below is hard coded to the Parken Live app


        form_fields = {
            'client': push_data['client'],
            'external_user_id': push_data['external_user_id'],
            'order_id': self.request.get('order_id'),
            'alert': self.request.get('message').encode('utf-8'),
            'action-loc-key': self.request.get('action')
        }
        debug = os.environ.get('SERVER_SOFTWARE', '').startswith('Development') or os.environ['CURRENT_VERSION_ID'].split(".")[0] == 'dev'
        if not debug:
            form_fields['live'] = 'true'
            url = 'http://teliaparkenlive.dk/service/zerved/push'
        else:
            url = 'http://parkenlive.c1.test.adapt.dk/service/zerved/push'

        form_data = urllib.urlencode(form_fields)
        logging.debug(form_data)
        result = urlfetch.fetch(
            url,
            payload=form_data,
            method='POST',
            headers={'Content-Type': 'application/x-www-form-urlencoded', "Authorization": "Basic YWRhcHQ6V2VsY29tZVRvQWRhcHQu", 'KEY': 'sewLbmaLVTXxWme25T6tYXKa', 'CLIENT': 'HXYcF79T9yaMZRpLL4Ka2fAv'}
        )
        logging.info(result.status_code)
        logging.info(result.content)
        self.response.status = int(result.status_code)
        