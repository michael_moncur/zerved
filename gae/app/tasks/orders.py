import webapp2
from models.merchants import Merchant, MerchantInvoice, OrderProcessingSession
from models.sales import Order
from google.appengine.ext import db
from google.appengine.api import taskqueue
import json
from webapp2_extras import i18n
_ = i18n.lazy_gettext

class ProcessOrderHandler(webapp2.RequestHandler):
    def post(self):
        """ Move order into new status """
        order = Order.get(self.request.get('key'))
        status = str(self.request.get('status'))
        invoice = None
        if status == 'complete' and self.request.get('invoice_key'):
            invoice = MerchantInvoice.get(self.request.get('invoice_key'))
        # Change status
        if (order and order.change_status(status, invoice)):
            bar = order.bar
            # Notify consumer
            consumer = order.consumer
            if consumer:
                if order.locale:
                    i18n.get_i18n().set_locale(order.locale)
                else:
                    i18n.get_i18n().set_locale('en')
                if status == 'preparing' and order.service == 'counter' and bar.get_notification('counter_preparing'):
                    consumer.send_notification(bar.get_notification('counter_preparing'), _("View Order"), order)
                elif status == 'preparing' and order.service == 'table' and bar.get_notification('table_preparing'):
                    consumer.send_notification(bar.get_notification('table_preparing'), _("View Order"), order)
                elif status == 'complete' and order.service == 'counter' and bar.get_notification('counter_complete'):
                    consumer.send_notification(bar.get_notification('counter_complete'), _("View Order"), order)
                elif status == 'preparing' and order.service == 'delivery' and bar.get_notification('delivery_preparing'):
                    consumer.send_notification(bar.get_notification('delivery_preparing'), _("View Order"), order)
                elif status == 'complete' and order.service == 'delivery' and bar.get_notification('delivery_complete'):
                    consumer.send_notification(bar.get_notification('delivery_complete'), _("View Order"), order)
                elif status == 'cancelled':
                    consumer.send_notification(_("Sorry, %s was not able to fulfill your order.") % order.location_name, _("View Cancellation"), order)

class ProcessMultipleOrderHandler(webapp2.RequestHandler):
    def post(self):
        """ Move order into new status """

        order_keys = json.loads(self.request.get('order_keys'))
        status = str(self.request.get('status'))
        invoice = None
        if status == 'complete' and self.request.get('invoice_key'):
            invoice = MerchantInvoice.get(self.request.get('invoice_key'))

        merchant = Merchant.get(str(self.request.get('merchant_key')))
        orders = []
        changed_orders = []
        orders_chunk = db.get(order_keys)
        for order in orders_chunk:
            if order is None or not merchant.validate_permission(order):
                continue
            orders.append(order)
            # Change status
            if (order.change_status_nosave(
                    status,
                    invoice)):
                changed_orders.append(order)
        bar_keys = list(frozenset([Order.bar.get_value_for_datastore(order) for order in changed_orders]))
        bars = db.get(bar_keys)
        bar_map = dict(zip(bar_keys, bars))
        completed_orders = []
        if len(changed_orders) > 0:
            consumers = db.get([Order.consumer.get_value_for_datastore(order) for order in changed_orders])
            db.put(changed_orders)
            for order, consumer in zip(changed_orders, consumers):
                if order.status in ['complete', 'cancelled']:
                    completed_orders.append(order)
                bar = bar_map[Order.bar.get_value_for_datastore(order)]
                # Notify consumer
                if consumer:
                    if order.locale:
                        i18n.get_i18n().set_locale(order.locale)
                    else:
                        i18n.get_i18n().set_locale('en')
                    if status == 'preparing' and order.service == 'counter' and bar.get_notification('counter_preparing'):
                        consumer.send_notification(bar.get_notification('counter_preparing'), _("View Order"), order)
                    elif status == 'preparing' and order.service == 'table' and bar.get_notification('table_preparing'):
                        consumer.send_notification(bar.get_notification('table_preparing'), _("View Order"), order)
                    elif status == 'complete' and order.service == 'counter' and bar.get_notification('counter_complete'):
                        consumer.send_notification(bar.get_notification('counter_complete'), _("View Order"), order)
                    elif status == 'preparing' and order.service == 'delivery' and bar.get_notification('delivery_preparing'):
                        consumer.send_notification(bar.get_notification('delivery_preparing'), _("View Order"), order)
                    elif status == 'complete' and order.service == 'delivery' and bar.get_notification('delivery_complete'):
                        consumer.send_notification(bar.get_notification('delivery_complete'), _("View Order"), order)
                    elif status == 'cancelled':
                        consumer.send_notification(_("Sorry, %s was not able to fulfill your order.") % order.location_name, _("View Cancellation"), order)

        # send changes to clients
        if len(bars) > 0:
            Order.update_processing_sessions_multiple_orders(orders, bars[0])
        for order in completed_orders:
            taskqueue.add(queue_name='orders', url='/tasks/completeorder', params={'order_key': str(order.key())})

class CompleteOrderHandler(webapp2.RequestHandler):
    def post(self):
        order_key = self.request.get('order_key')
        order = Order.get(order_key)
        order.complete_status()

class UpdateSessionHandlerMultipleOrders(webapp2.RequestHandler):
    def post(self):
        """ Send order to connected bar session """

        data = self.request.get('data')

        session_keys = json.loads(self.request.get('session_keys'))
        sessions = db.get(session_keys)
        for session in sessions:
            # Use the language of the connected order processing session
            i18n.get_i18n().set_locale(session.locale)
            session.send_message(data)


class UpdateSessionHandler(webapp2.RequestHandler):
    def post(self):
        """ Send order to connected bar session """
        session = OrderProcessingSession.get_by_key_name(self.request.get('client_id'))
        order = Order.get(self.request.get('order_key'))
        i18n.get_i18n().set_locale(session.locale) # Use the language of the connected order processing session
        data = { 'remove': [str(order.key()), str(order.status), str(order.queue_number)] }
        if (order.status in ['pending', 'preparing', 'complete'] and order.service in ['counter', 'table', 'delivery']):
            data['insert'] = [order.to_json()]
        session.send_message(json.dumps(data))
