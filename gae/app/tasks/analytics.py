import webapp2
from google.appengine.api import urlfetch
import urllib
from models.sales import Order, OrderItem
from models.environment import get_environment
import logging

def get_analytics_tracking_id():
    if get_environment() == 'production':
        return 'UA-34569146-3'
    else:
        return 'UA-34569146-2'

class RecordOrderHandler(webapp2.RequestHandler):
    def post(self):
        """ Track order in Google Analytics """
        order = Order.get(self.request.get('order_key'))
        if order:
            payload = {
                'v': '1',
                'tid': get_analytics_tracking_id(),
                'cid': str(order.consumer.key().id()),
                't': 'transaction',
                'ti': str(order.key().id()),
                'ta': order.location_name.encode('utf-8'),
                'tr': order.total_incl_tax,
                'tt': order.total_tax,
                'cu': order.currency
            }
            itemspayload = []
            for item in order.items:
                sku = str(item.product.key().id()) if item.product else ""
                itemspayload.append({
                    'v': '1',
                    'tid': get_analytics_tracking_id(),
                    'cid': str(order.consumer.key().id()),
                    't': 'item',
                    'ti': str(order.key().id()),
                    'in': item.name.encode('utf-8'),
                    'ip': item.base_price_incl_tax,
                    'iq': item.quantity,
                    'ic': sku,
                    'iv': item.category_name.encode('utf-8'),
                    'cu': order.currency
                })
            try:
                result = urlfetch.fetch(url='https://www.google-analytics.com/collect', payload=urllib.urlencode(payload), method='POST')
                for itempayload in itemspayload:
                    result = urlfetch.fetch(url='https://www.google-analytics.com/collect', payload=urllib.urlencode(itempayload), method='POST')
            except urlfetch.DownloadError as e:
                logging.error(e)