import webapp2
from models.merchants import Merchant
from google.appengine.api import memcache

class ClearProductCacheHandler(webapp2.RequestHandler):
	def post(self):
		# Clear all cached product lists for merchant
		merchant = Merchant.get(self.request.get('merchant'))
		if merchant:
			cache_keys = []
			for location in merchant.locations:
				cache_keys.append('categories_loc_'+str(location.key().id()))
				for category in merchant.categories:
					cache_keys.append('productlist_loc_'+str(location.key().id())+'_cat_'+str(category.key().id()))
			for menu in merchant.menus:
				cache_keys.append('categories_menu_'+str(menu.key().id()))
				for category in merchant.categories:
					cache_keys.append('productlist_menu_'+str(menu.key().id())+'_cat_'+str(category.key().id()))
			memcache.delete_multi(cache_keys)
