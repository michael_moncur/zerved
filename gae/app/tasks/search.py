import webapp2
from models.merchants import Merchant, Location

class IndexLocationsHandler(webapp2.RequestHandler):
    def get(self):
        for loc in Location.all():
            if loc.enabled:
                loc.search_index_save()
