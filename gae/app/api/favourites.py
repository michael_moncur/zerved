from core import ApiHandler
from models.favourite import Favourite
from models.catalog import Product
from models.consumers import Consumer
from models.merchants import Bar, Location
from app.api.data_collector import DataCollector
from google.appengine.ext import db
import logging
import pickle
from google.appengine.api.datastore import Key
from google.appengine.api import datastore_errors
from webapp2_extras import i18n
_ = i18n.gettext

class FavouriteListHandler(ApiHandler, DataCollector):
    def get(self, consumer_key):
        """ List favourites for consumer """
    	consumer = self.init_consumer(consumer_key)
        favourites = Favourite.all().ancestor(consumer).order("location").order("bar").order("product")
        fav = None
        for favourite in favourites:
            # Try fetching the product, location, and bar.
            # If they no longer exist, the favourite is invalid and should be deleted
            try:
                logging.info(favourite.product)
                logging.info(favourite.location)
                logging.info(favourite.bar)
            except datastore_errors.Error:
                favourite.delete()

        self.write_json(self.get_list(favourites, Favourite.API_PROPERTIES))

    def init_consumer(self, consumer_key):
        try:
            entity = Consumer.get(consumer_key)
        except Exception:
            self.abort(404)
        self.authenticate()
        self.authorize(entity)
        return entity

    def authorize(self, consumer):
        if not (self.user and consumer and str(self.user.key()) == str(consumer.key())):
            self.abort(401, comment=_("Unauthorized"))

class FavouriteHandler(ApiHandler, DataCollector):
    def post(self):
        """ Create favourite """
        self.authenticate()
    	postdata = self.read_json()
    	
        opts = []
        for option in postdata['options']:
            opts.append({"option_id" : str(option['option_id']), "option_value" : int(option['option_value'])})

        fav = Favourite(
            parent=self.user,
		    consumer = self.user,
		    product = Product.get(postdata['product']),
		    opts = pickle.dumps(opts, 2),
        	)
        
        if 'bar' in postdata:
            fav.bar = Bar.get(postdata['bar'])
            fav.location = fav.bar.location
       	if 'location' in postdata:
        	fav.location = Location.get(postdata['location'])

        fav.put()

    def delete(self, favourite_key):
        """ Delete favourite """
        self.authenticate()
        favourite = Favourite.get(favourite_key)
        if not favourite:
            self.abort(404)
        self.authorize(favourite)
    	favourite.delete()

    def authorize(self, favourite):
        if not (self.user and favourite and str(self.user.key()) == str(favourite.consumer.key())):
            self.abort(401, comment=_("Unauthorized"))