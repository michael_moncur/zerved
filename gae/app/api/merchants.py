from core import ApiHandler
from models.merchants import Merchant, Location, Bar
from app.api.data_collector import DataCollector
from geo.geomodel import geotypes

class MerchantLegacyHandler(ApiHandler, DataCollector):
    def get_legacy_bar_object(self, bar):
        # Create a bar object that looks like the old merchant object (including merchant and location data)
        data = self.get_object(bar.merchant, Bar.API_LEGACY_MERCHANT_PROPERTIES)
        data.update(self.get_object(bar.location, Bar.API_LEGACY_LOCATION_PROPERTIES))
        data.update(self.get_object(bar, Bar.API_PROPERTIES))
        return data
        
    def get_legacy_bar_list(self, locations):
        bars = []
        for location in locations:
            for bar in location.bars:
                if bar.enabled:
                    bars.append(self.get_legacy_bar_object(bar))
        return bars

class MerchantListLegacyHandler(MerchantLegacyHandler):
    def get(self):
        locations = Location.all().filter('enabled =', True)
        bars = self.get_legacy_bar_list(locations)
        self.write_json(bars)
        
class MerchantListNearbyLegacyHandler(MerchantLegacyHandler):
    def get(self, latitude, longitude):
        point = geotypes.Point(float(latitude), float(longitude))
        locations = Location.proximity_fetch(
            Location.all().filter('enabled =', True),
            point,
            max_results=20,
            max_distance=2000
        )
        bars = self.get_legacy_bar_list(locations)
        self.write_json(bars)
        
class ClosestMerchantLegacyHandler(MerchantLegacyHandler):
    def get(self, latitude, longitude):
        point = geotypes.Point(float(latitude), float(longitude))
        locations = Location.proximity_fetch(
            Location.all().filter('enabled =', True),
            point,
            max_results=1,
            max_distance=2000
        )
        for location in locations:
            for bar in location.bars:
                if bar.enabled:
                    self.write_json(self.get_legacy_bar_object(bar))
                    return
        self.write_json({})
        
class MerchantHandler(ApiHandler, DataCollector):
    def get(self, merchant_key):
        try:
            entity = Merchant.get(merchant_key)
        except Exception:
            self.abort(404)
        self.write_json(self.get_object(entity, Merchant.API_PROPERTIES))