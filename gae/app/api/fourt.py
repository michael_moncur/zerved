from core import ApiHandler
from google.appengine.ext import db
from models.sales import Order
from app.api.data_collector import DataCollector
import logging
from webapp2_extras import i18n
_ = i18n.gettext

class FourTCallbackHandler(ApiHandler, DataCollector):
    """ 
    When a transaction request is sent to 4T, the confirmation
    is sent to this callback URL, which will update the payment 
    and order status
    """
    def post(self, order_key):
        try:
            order = Order.get(order_key)
        except Exception:
            self.abort(404)
        postdata = self.read_json()
        logging.info(postdata)
        i18n.get_i18n().set_locale(order.locale)
        logging.info(self.request.headers)
        if 'state' in postdata:
            if postdata['state'] == 'AUTHORIZED':
                
                bar = order.bar
                eventname = order.event_name
                consumer = order.consumer

                if not eventname and bar and not bar.open:
                    logging.debug('not bar.open')
                    order.payment.status = 'void'
                    order.payment.put()
                    order.change_status('cancelled')
                    notif = bar.message_when_closed
                    consumer.send_notification(notif, _("Show Order"), order)
                    logging.debug('\'%s\' sent to %s',notif, consumer.phone)
                    self.abort(409, comment=bar.message_when_closed)

                order.payment.status = 'authorized'
                order.payment.transaction_id = str(postdata['transactionId'])
                order.payment.put()
                
                notif = _("Payment with Paii authorized")
                consumer.send_notification(notif, _("Show Order"), order)
                logging.debug('\'%s\' sent to %s',notif, consumer.phone)

                # Set order to pending, or auto prepare bar orders if enabled
                if order.bar and order.merchant.auto_prepare:
                    order.change_status('preparing')
                    logging.debug('auto_preparing %s', consumer.phone)
                else:
                    order.change_status('pending')
                    logging.debug('NOT auto_preparing %s', consumer.phone)
            elif postdata['state'] == 'CAPTURED':
                order.payment.status = 'captured'
                order.payment.put()
                order.change_status('complete')
            elif postdata['state'] == 'CANCELED':
                order.payment.status = 'void'
                order.payment.put()
                order.change_status('cancelled')
            elif postdata['state'] == 'REFUNDED':
                order.payment.status = 'refunded'
                order.payment.put()