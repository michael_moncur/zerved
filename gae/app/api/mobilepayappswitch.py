from core import ApiHandler
from google.appengine.ext import db
from consumers import Consumer
from models.sales import Order
from models.mobilepayappswitch import MobilePayAppSwitchClient, MobilePayAppSwitchPaymentClient
from models.payment import *
import logging
import json
import hashlib
from urlparse import urlparse, parse_qsl
from decimal import *
from app.api.data_collector import DataCollector
from webapp2_extras import i18n
_ = i18n.gettext

class MobilePayAppSwitchCallbackHandler(ApiHandler, DataCollector):
    # This handler is based on the logic in /api/fourt.py

    def post(self):
        # verify hash before proceeding
        #expected_hash = self.calculateExpectedHash()
        #callback_hash = self.request.get('hash')
        #if expected_hash != callback_hash:
        #    logging.error("Received epay MD5 hash (" + callback_hash + ") does not match expected hash (" + expected_hash + ")" )
        #    self.abort(404)


        #callback_transaction_id = str(self.request.get('txnid'))
        #logging.debug("callback_transaction_id " + callback_transaction_id)
        #if callback_transaction_id is None or callback_transaction_id == "0" or is_save_card_order:
            # Consumer is saving credit card without ordering.
        #    logging.info("Consumer is trying to save credit card without ordering.")
        #    consumerId = str(self.request.get('orderid'))[len(save_card_type_prefix_identifier):]
        #    consumer = Consumer.get_by_id(int(consumerId))

        #    callback_obscured_card_number = str(self.request.get('cardno'))
        #    self.saveSubscriptionIfEnabled(self.request, consumer, "-1", callback_obscured_card_number)
        #    return

        request_orderId = self.request.get('OrderId')
        request_transactionId = self.request.get('TransactionId')
        request_amount = self.request.get('Amount')
        request_paymentstatus = self.request.get('PaymentStatus')
        logging.info("amount " + request_amount)
        logging.info("status " + request_paymentstatus)
        logging.info(request_orderId)

        assert(request_orderId is not None), 'request_orderId is None'
        assert(request_transactionId is not None), 'request_transactionId is None'
        assert(request_amount is not None), 'request_amount is None'
        assert(request_paymentstatus is not None), 'request_paymentstatus is None'

        client = MobilePayAppSwitchPaymentClient()
        (success, result) = client.getpaymentstatus(request_orderId)
        if not success:
            logging.error("getpaymentstatus error")
            self.abort(404)

        logging.info("Success " + str(success) + "Result content: " + result.content.encode('utf-8'))
        request_body_json = json.loads(result.content.encode('utf-8'))
        getstatus_latestpaymentstatus = request_body_json["LatestPaymentStatus"]
        getstatus_originalamount = request_body_json["OriginalAmount"]

        try:
            order_key = db.Key.from_path('Order', int(request_orderId))
            order = Order.get(order_key)
        except Exception, e:
            logging.error(e)
            self.abort(404)
        i18n.get_i18n().set_locale(order.locale)

        error_cancel_order = False
        if not success:
            logging.error("MobilePay getpaymentstatus result was NOT True!")
            error_cancel_order = True

        if getstatus_latestpaymentstatus != "Reserved":
            logging.error("MobilePay getstatus_latestpaymentstatus is not reserved. Value is: " + getstatus_latestpaymentstatus)
            error_cancel_order = True

        # Verify amount and currency is ok
        order_amount = (int)((order.total_incl_tax * 100))
        mobilepay_original_amount = (int)((Decimal(getstatus_originalamount)) * 100)
        amount_ok = False
        amount_max_difference = 2
        if order_amount > (mobilepay_original_amount - amount_max_difference) and order_amount < (mobilepay_original_amount + amount_max_difference):
            amount_ok = True

        if not amount_ok:
            logging.error("Order amount (" + str(order_amount) + ") does not match mobilepay appswitch original amount (" + str(mobilepay_original_amount) + "). Max difference: " + str(amount_max_difference))
            error_cancel_order = True
        else:
            logging.info("amount ok " + str(Decimal(getstatus_originalamount)))

        bar = order.bar
        eventname = order.event_name
        consumer = order.consumer

        if error_cancel_order:
            order.change_status('cancelled')
            notif = _("Cancelled")
            consumer.send_notification(notif, _("Show Order"), order)
            logging.debug('\'%s\' sent to %s',notif, consumer.phone)
            self.abort(404)


        # Only proceed if payment status is 'new'. This check also helps avoiding fraud (reuse of an old call to change pincode etc)
        if order.payment.status != "new":
            logging.error("Received MobilePay callback for order with status != 'new'. Aborting call to avoid problems.")
            self.abort(404)


        if not eventname and bar and not bar.open:
            logging.debug('not bar.open')
            order.payment.status = 'void'
            order.payment.put()
            order.change_status('cancelled')
            notif = bar.message_when_closed
            consumer.send_notification(notif, _("Show Order"), order)
            logging.debug('\'%s\' sent to %s',notif, consumer.phone)
            self.abort(409, comment=bar.message_when_closed)

        order.payment.status = 'authorized'
        order.payment.transaction_id = request_transactionId
        order.payment.put()

        notif = _("Payment authorized")
        consumer.send_notification(notif, _("Show Order"), order)
        logging.debug('\'%s\' sent to %s',notif, consumer.phone)
        # Set order to pending, or auto prepare bar orders if enabled
        if order.bar and order.merchant.auto_prepare:
            order.change_status('preparing')
            logging.debug('auto_preparing %s', consumer.phone)
        else:
            order.change_status('pending')
            logging.debug('NOT auto_preparing %s', consumer.phone)

        return


