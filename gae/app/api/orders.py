from core import ApiHandler
import webapp2
from merchants import MerchantLegacyHandler
from models.merchants import Merchant, Location, Bar
from models.consumers import Consumer
from models.catalog import Product, ProductOption
from models.quote import Quote, CartItemOutdatedError
from models.sales import Order
from models.events import Event
from models.payment import NordpaySubscription, PaymentException
from google.appengine.ext import db
from google.appengine.api import mail, taskqueue
from decimal import Decimal
import datetime
from webapp2_extras import i18n
from app.api.data_collector import DataCollector
_ = i18n.gettext
import logging
import json
import base64
from models.mobilepayappswitch import MobilePayAppSwitchClient, MobilePayAppSwitchPaymentClient

class QuoteHandler(MerchantLegacyHandler):
    def post(self):
        """ Post shopping cart data to get a quote (prices, totals etc.) """
        self.authenticate()
        postdata = self.read_json()
        logging.info(postdata)
        useragent = self.request.headers.get("User-Agent", "")
        if 'Zerved/134' in useragent and 'iOS' in useragent:
            self.abort(410, comment=_("Outdated app version. Please update."))
        elif 'bar' in postdata:
            # Bar quote
            bar = Bar.get(postdata['bar'])
            if not bar:
                self.abort(404, comment=_("Bar not found"))
            quote = Quote(api_handler=self, bar=bar)
        elif 'location' in postdata:
            # Location quote
            location = Location.get(postdata['location'])
            if not location:
                self.abort(404, comment=_("Location not found"))
            quote = Quote(api_handler=self, location=location)
        else:
            self.abort(410, comment=_("Outdated app version. Please update."))
            
        quote.set_cart_items(postdata['items'])
        if 'tip_percent' in postdata:
            quote.tip_percent = Decimal(postdata['tip_percent'])
        if 'event' in postdata:
            event = Event.get(postdata['event'])
            if not event:
                self.abort(404, comment=_("Event not found"))
            quote.event = event
        if 'service' in postdata:
            quote.service = postdata['service']
            if 'to_go' in postdata:
                quote.to_go = bool(postdata['to_go'])
        if 'loyalty_code' in postdata: # and postdata['loyalty_code'] != "":
            quote.loyalty_code = postdata['loyalty_code']
            logging.debug('Loyalty code included in POST: %s' % quote.loyalty_code)
        elif self.user:
            # If no loyalty code submitted, get it from consumer account if available
            quote.loyalty_code = self.user.get_loyalty_code(quote.merchant.key())
            logging.debug('Loyalty code taken from consumer: %s' % quote.loyalty_code)
        if 'delivery_instructions' in postdata:
            quote.delivery_instructions = postdata['delivery_instructions']
        json = quote.to_json()
        logging.info(json)
        self.write_json(json)

    def authenticate(self):
        """ Optional authentication """
        self.user = None
        try:
            auth_header = self.request.headers["Authorization"]
            # Isolate the encoded access token and decode it
            auth_parts = auth_header.split(' ')
            access_token = base64.b64decode(auth_parts[1])
            self.user = Consumer.authenticate(access_token)
        except Exception as e:
            pass
        
class PlaceOrderHandler(ApiHandler, DataCollector):
    def post(self):
        """ Post shopping cart data to place an order """
        self.authenticate()
        postdata = self.read_json()
        logging.info(postdata)
        location = None
        bar = None

        if not self.user:
            self.abort(401, comment=_("User not found"))

        if 'bar' in postdata:
            bar = Bar.get(postdata['bar'])
            if not bar:
                self.abort(404, comment=_("Bar not found"))
            if not 'event' in postdata and not bar.open:
                self.abort(409, comment=bar.message_when_closed)
            quote = Quote(api_handler=self, bar=bar)
        elif 'location' in postdata:
            location = Location.get(postdata['location'])
            if not location:
                self.abort(404, comment=_("Location not found"))
            if not location.enabled:
                self.abort(409, comment=_("Sorry, the merchant is currently closed for orders."))
            quote = Quote(api_handler=self, location=location)
        else:
            self.abort(404)

        # Validate event
        if 'event' in postdata:
            event = Event.get(postdata['event'])
            if not event:
                self.abort(404, comment=_("Event not found"))
            now = datetime.datetime.now()
            #if event.order_start_date > now:
            #    self.abort(409, comment=_("The event is not yet open for orders"))
            if event.order_end_date < now:
                self.abort(409, comment=_("The event is no longer open for orders"))
            quote.event = event
            
        # Initialize cart
        if 'terms_accepted' not in postdata or postdata['terms_accepted'] != True:
            self.abort(409, comment=_("Please accept the Terms &amp; Conditions"))
        quote.set_cart_items(postdata['items'])
        if 'tip_percent' in postdata:
            quote.tip_percent = Decimal(postdata['tip_percent'])
        if 'service' in postdata:
            quote.service = postdata['service']
            if bar and not bar.has_service(quote.service):
                self.abort(409, comment=_("Selected service type is not available"))
            if postdata['service'] == 'table':
                if 'table_number' in postdata and postdata['table_number'] != '':
                    quote.table_number = postdata['table_number']
                else:
                    self.abort(409, comment=_("Please enter your table number"))
            elif postdata['service'] == 'counter':
                if "to_go" in postdata:
                    quote.to_go = bool(postdata["to_go"])
            elif postdata['service'] == 'delivery':
                for field in ['delivery_name', 'delivery_phone', 'delivery_address1', 'delivery_postcode', 'delivery_city']:
                    if field in postdata and postdata[field] != '':
                        setattr(quote, field, postdata[field])
                    else:
                        self.abort(409, comment=_("Address fields missing"))
                if 'delivery_address2' in postdata:
                    quote.delivery_address2 = postdata['delivery_address2']
        else:
            self.abort(409, comment=_("Please choose service type"))

        if 'loyalty_code' in postdata:
            quote.loyalty_code = postdata['loyalty_code']
            logging.info('postdata[loyalty_code]:%s' % postdata['loyalty_code'])
        elif self.user:
            quote.loyalty_code = self.user.get_loyalty_code(quote.merchant.key())
            logging.info('self.user.get_loyalty_code(quote.merchant.key()):%s' % self.user.get_loyalty_code(quote.merchant.key()))

        for item in quote.get_quote_items():
            if not item['valid']:
                self.abort(409, comment=_('The shopping cart is invalid because the menu has changed.'))
        if not 'card' in postdata:
            self.abort(409, comment=_("Credit card information missing"))

        if 'delivery_instructions' in postdata:
            quote.delivery_instructions = postdata['delivery_instructions']

        # Place order
        if postdata['card'] == 'current' and self.user.current_payment_subscription:
            # Use saved card, verify pincode
            pincodevalid = 'pincode' in postdata and self.user.current_payment_subscription.verify_pincode(postdata['pincode'])
            if not pincodevalid:
                self.abort(403, comment=_("Wrong payment code"))
            # Place order
            order = quote.place_order(consumer=self.user, payment_method='epay_saved_card')
        elif postdata['card'] == 'new' and 'nordpay_registration' in postdata and 'pincode' in postdata:
            nordpay_authorization = postdata['nordpay_authorization'] if 'nordpay_authorization' in postdata else None
            nordpay_method = postdata['nordpay_method'] if 'nordpay_method' in postdata else "CC"
            # Newly added card, save reference and pincode
            ps = NordpaySubscription(parent=self.user, 
                status='active',
                identifier=postdata['nordpay_registration'],
                method=nordpay_method)
            if 'obscured_card_number' in postdata:
                ps.obscured_card_number = postdata['obscured_card_number']
            ps.set_pincode(postdata['pincode'])
            ps.put()
            self.user.current_payment_subscription = ps
            self.user.put()
            # Place order
            order = quote.place_order(consumer=self.user, payment_method='nordpay', nordpay_authorization=nordpay_authorization, nordpay_method=nordpay_method)
        elif postdata['card'] == 'onetime' and 'nordpay_authorization' in postdata:
            # Place order with a onetime payment
            nordpay_method = postdata['nordpay_method'] if 'nordpay_method' in postdata else "CC"
            order = quote.place_order(consumer=self.user, payment_method='nordpay', nordpay_authorization=postdata['nordpay_authorization'], nordpay_method=nordpay_method)
        elif postdata['card'] == '4t' and self.user.phone:
            # Place order with 4T payment
            order = quote.place_order(consumer=self.user, payment_method='4t')
        elif postdata['card'] == 'epay_new_card':
            # Place order with ePay payment. Callback from ePay will tell if the user wants to save his card.
            order = quote.place_order(consumer=self.user, payment_method='epay_new_card')
            self.write_json({'payment_needed': True, 'order_key': str(order.key()), 'order_id': str(order.key().id()) })
            return
        elif postdata['card'] == 'mobilepay_appswitch':
            # Place order with mobilepay appswitch payment.
            order = quote.place_order(consumer=self.user, payment_method='mobilepay_appswitch')
            self.write_json({'payment_needed': True, 'order_key': str(order.key()), 'order_id': str(order.key().id()) })
            return
        elif postdata['card'] == 'epay_mobilepay':
            order = quote.place_order(consumer=self.user, payment_method='epay_mobilepay')
            self.write_json({'payment_needed': True, 'order_key': str(order.key()), 'order_id': str(order.key().id()) })
            return
        else:
            # No payment available
            self.write_json({'payment_needed': True})
            return
            
        # Return order key
        self.write_json({'order_key': str(order.key()), 'order_id': str(order.key().id()) })

    def handle_exception(self, exception, debug):
        """ Exception handler overriden to avoid logging rejected payments as errors """
        if isinstance(exception, webapp2.HTTPException):
            self.response.set_status(exception.code)
        else:
            self.response.set_status(500)
            if isinstance(exception, PaymentException):
                logging.info(exception)
            else:
                logging.error(exception)
        if hasattr(exception, 'comment'):
            self.response.write(exception.comment)
        else:
            self.response.write(exception)
        
class OrderListHandler(ApiHandler, DataCollector):
    def get(self, consumer_key):
        """ List orders for consumer grouped by open/closed """
        consumer = self.init_consumer(consumer_key)
        open_orders = consumer.orders.filter('status IN', ['pending', 'preparing']).order('-date_created')
        closed_orders = consumer.orders.filter('status =', 'complete').order('-date_created')
        orders = {}
        orders['open_orders'] = self.get_list(open_orders, Order.API_PROPERTIES_SPARSE)
        orders['closed_orders'] = self.get_list(closed_orders, Order.API_PROPERTIES_SPARSE)
        self.write_json(orders)
        
    def init_consumer(self, consumer_key):
        try:
            entity = Consumer.get(consumer_key)
        except Exception:
            self.abort(404)
        self.authenticate()
        self.authorize(entity)
        return entity

    def authorize(self, consumer):
        if not (self.user and consumer and str(self.user.key()) == str(consumer.key())):
            self.abort(401, comment=_("Unauthorized"))
            
class OrderListStateHandler(OrderListHandler, DataCollector):
    def get(self, consumer_key, state):
        """ List orders for consumer for given state (open, closed or both) """
        consumer = self.init_consumer(consumer_key)
        if state not in ['open', 'closed', 'open,closed']:
            self.abort(404)
        if state == 'open':
            orders = consumer.orders.filter('status IN', ['pending', 'preparing']).order('-date_created')
        elif state == 'closed':
            orders = consumer.orders.filter('status =', 'complete').order('-date_created')
        else:
            orders = consumer.orders.filter('status IN', ['pending', 'preparing', 'complete']).order('-date_created')
        # Fetch orders and preload merchant objects
        orders = Order.prefetch_merchants(orders.fetch(1000))
        self.write_json(self.get_list(orders, Order.API_PROPERTIES_SPARSE))
            
class OrderHandler(ApiHandler, DataCollector):
    def get(self, order_key_or_id):
        """ Get order data """
        order = self.init_order(order_key_or_id)
        orderdata = self.get_object(order, Order.API_PROPERTIES_COMPLETE)
        self.write_json(orderdata)
        
    def post(self, order_key_or_id):
        """ Update order """
        order = self.init_order(order_key_or_id)
        postdata = self.read_json()
        if 'special_command' in postdata:
            if postdata['special_command'] == 'mobilepay_handle_interrupted_payment':
                cancel_order = True
                try:
                    client = MobilePayAppSwitchPaymentClient()
                    (success, result) = client.getpaymentstatus(order.key().id())
                    if success:
                        logging.info("Success " + str(success) + ". Result content: " + result.content.encode('utf-8'))
                        request_body_json = json.loads(result.content.encode('utf-8'))
                        getstatus_latestpaymentstatus = request_body_json["LatestPaymentStatus"]
                        logging.info("getstatus_latestpaymentstatus " + getstatus_latestpaymentstatus)
                        if getstatus_latestpaymentstatus == "Reserved" and not order.status == 'pending_payment':
                            cancel_order = False
                            logging.info("Keeping order! Status is " + getstatus_latestpaymentstatus + " and order status is " + order.status)
                except Exception as exc:
                    logging.error("getpaymentstatus exception " + str(exc))

                if cancel_order:
                    logging.info("cancelling order.")
                    result = order.change_status('cancelled', is_consumer_request = True)
                    if not result:
                        self.abort(500, comment=_("The order cannot be cancelled because the merchant has begun processing it"))

        elif 'status' in postdata:
            # Change status if allowed
            if postdata['status'] == 'cancelled':
                result = order.change_status('cancelled', is_consumer_request = True)
                if not result:
                    self.abort(500, comment=_("The order cannot be cancelled because the merchant has begun processing it"))
            elif postdata['status'] == 'complete':
                if not postdata['entrance_pincode'] or postdata['entrance_pincode'] != order.merchant.entrance_pincode:
                    self.abort(409, comment=_("Wrong code"))
                completionallowed = order.service == 'entrance' or order.merchant.serve_with_consumer_device
                result = completionallowed and order.change_status('complete')
                if not result:
                    self.abort(500, comment=_("Completion failed"))
        # Return updated order
        self.write_json(self.get_object(order, Order.API_PROPERTIES_COMPLETE))
        
    def init_order(self, order_key_or_id):
        try:
            order_key = db.Key.from_path('Order', int(order_key_or_id)) if self.is_numeric(order_key_or_id) else order_key_or_id
            entity = Order.get(order_key)
        except Exception:
            self.abort(404)
        self.authenticate()
        self.authorize(entity)
        return entity

    def is_numeric(self, s):
        try:
            int(s)
            return True
        except ValueError:
            return False
        
    def authorize(self, order):
        if not (self.user and order and str(self.user.key()) == str(order.consumer.key())):
            self.abort(401, comment=_("Unauthorized"))
            
class SendReceiptHandler(ApiHandler, DataCollector):
    def post(self, order_key):
        """ Send email receipt for order """
        order = self.init_order(order_key)
        if not order.consumer.email:
            self.abort(409, comment=_('Missing email address. Please go to "Profile" to add it.'))
        message = mail.EmailMessage()
        message.sender = "Zerved <info@zervedapp.com>"
        message.subject = _("Zerved Receipt") + " #" + str(order.key().id())
        message.to = order.consumer.email
        message.body = self.jinja2.render_template('emails/receipt.txt', order=order)
        message.html = self.jinja2.render_template('emails/receipt.html', order=order)
        message.send()
        
    def init_order(self, order_key):
        try:
            entity = Order.get(order_key)
        except Exception:
            self.abort(404)
        self.authenticate()
        self.authorize(entity)
        return entity
        
    def authorize(self, order):
        if not (self.user and order and str(self.user.key()) == str(order.consumer.key())):
            self.abort(401, comment=_("Unauthorized"))