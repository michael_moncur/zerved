from core import ApiHandler
from google.appengine.ext import db
from consumers import Consumer
from models.sales import Order
from models.epay import EpayClient, EpayPaymentClient
from models.epaymerchantnumber import EpayMerchantNumber
from models.payment import *
import logging
import hashlib
from urlparse import urlparse, parse_qsl
from decimal import *
from app.api.data_collector import DataCollector
from webapp2_extras import i18n
_ = i18n.gettext

class EpayCallbackHandler(ApiHandler, DataCollector):
    # This handler is based on the logic in /api/fourt.py

    def get(self):
        # verify hash before proceeding
        expected_hash = self.calculateExpectedHash()
        callback_hash = self.request.get('hash')
        if expected_hash != callback_hash:
            logging.error("Received epay MD5 hash (" + callback_hash + ") does not match expected hash (" + expected_hash + ")" )
            self.abort(404)

        # Note: Regular orders have an "int" orderid. Special "save card info" orders starts with a special prefix.
        # The prefix is also specified in the client code (ios,android,embedded). If changed, it must also be changed in the clients.
        # The prefix length must not exceed 4 chars as the "epay order id field" is limited to 20 chars and the GAE id() field can
        # be up to 16 decimal digits long.
        save_card_type_prefix_identifier = 'save'

        is_save_card_order = str(self.request.get('orderid')).startswith(save_card_type_prefix_identifier)

        callback_transaction_id = str(self.request.get('txnid'))
        logging.debug("callback_transaction_id " + callback_transaction_id)
        if callback_transaction_id is None or callback_transaction_id == "0" or is_save_card_order:
            # Consumer is saving credit card without ordering.
            logging.info("Consumer is trying to save credit card without ordering.")
            consumerId = str(self.request.get('orderid'))[len(save_card_type_prefix_identifier):]
            logging.info("Consumer ID " + str(consumerId))
            consumer = Consumer.get_by_id(int(consumerId))
            if (consumer == None):
                logging.error("Consumer is None")
            callback_obscured_card_number = str(self.request.get('cardno'))
            self.saveSubscriptionIfEnabled(self.request, consumer, "-1", callback_obscured_card_number)
            return

        epay = EpayPaymentClient()
        callback_result = epay.gettransaction(
            merchantnumber=EpayMerchantNumber.epay_merchantnumber(),
            transactionid=callback_transaction_id,
            pwd=EPAY_API_PASSWORD)

        if callback_result.get('gettransactionResult') != 'true':
            logging.error("Epay gettransaction result was not true. Epayresponse code " + str(callback_result.get('epayresponse')))
            self.abort(404)

        namespace_prefix = '{https://ssl.ditonlinebetalingssystem.dk/remote/payment}'

        for elem in callback_result.response.getiterator():
            if elem.tag == namespace_prefix + 'authamount':
                gettransaction_auth_amount = elem.text
            elif elem.tag == namespace_prefix + 'orderid':
                gettransaction_order_id = elem.text
            elif elem.tag == namespace_prefix + 'currency':
                gettransaction_currency_code = elem.text
            elif elem.tag == namespace_prefix + 'tcardno':
                gettransaction_obscured_card_number = elem.text
            elif elem.tag == namespace_prefix + 'authdate':
                gettransaction_authdate = elem.text

        assert(gettransaction_auth_amount is not None), 'epay_auth_amount is None'
        assert(gettransaction_order_id is not None), 'order_id is None'
        assert(gettransaction_currency_code is not None), 'currency is None'
        assert(gettransaction_obscured_card_number is not None), 'tcardno is None'

        # To avoid fraud, check that the callback and gettranscation values match. If they match, we can also trust values from the callback not available in gettransaction (like subscription and pincode)
        #if gettransaction_auth_amount != str(self.request.get('amount')):
        #    logging.error("Posible fraud: callback and gettransaction value mismatch: amount");
        #    self.abort(404)

        if gettransaction_order_id != str(self.request.get('orderid')):
            logging.error("Posible fraud: callback and gettransaction value mismatch: orderid");
            self.abort(404)
        if gettransaction_currency_code != str(self.request.get('currency')):
            logging.error("Posible fraud: callback and gettransaction value mismatch: currency");
            self.abort(404)

        is_regular_order = not gettransaction_order_id.startswith(save_card_type_prefix_identifier)
        if is_regular_order:
            try:
                order = Order.get(db.Key.from_path('Order', int(gettransaction_order_id)))
            except Exception, e:
                self.abort(404)
            i18n.get_i18n().set_locale(order.locale)

            # Only proceed if payment status is 'new'. This check also helps avoiding fraud (reuse of an old call to change pincode etc)
            if order.payment.status != "new":
                logging.error("Received ePay callback for order with status != 'new'. Aborting call to avoid problems.")
                self.abort(404)

            bar = order.bar
            eventname = order.event_name
            consumer = order.consumer

            # Verify amount and currency is ok
            #order_amount = (int)(order.total_incl_tax * 100)
            #if order_amount != Decimal(gettransaction_auth_amount):
            #    logging.error("Order amount (" + str(order_amount) + ") does not match epay authamount (" + gettransaction_auth_amount + ")" );
            #    self.abort(404)

            order_currency_code = str(EpayClient.convert_currency_code_to_number(order.currency))
            if order_currency_code != gettransaction_currency_code:
                logging.error("Order currency code (" + order_currency_code + ") does not match epay currency code (" + gettransaction_currency_code + ")" );
                self.abort(404)

            if not eventname and bar and not bar.open:
                logging.debug('not bar.open')
                order.payment.status = 'void'
                order.payment.put()
                order.change_status('cancelled')
                notif = bar.message_when_closed
                consumer.send_notification(notif, _("Show Order"), order)
                logging.debug('\'%s\' sent to %s',notif, consumer.phone)
                self.abort(409, comment=bar.message_when_closed)

            order.payment.status = 'authorized'
            order.payment.transaction_id = callback_transaction_id
            order.payment.put()

            notif = _("Payment authorized")
            consumer.send_notification(notif, _("Show Order"), order)
            logging.debug('\'%s\' sent to %s',notif, consumer.phone)
            # Set order to pending, or auto prepare bar orders if enabled
            if order.bar and order.merchant.auto_prepare:
                order.change_status('preparing')
                logging.debug('auto_preparing %s', consumer.phone)
            else:
                order.change_status('pending')
                logging.debug('NOT auto_preparing %s', consumer.phone)

        self.saveSubscriptionIfEnabled(self.request, consumer, callback_transaction_id, gettransaction_obscured_card_number)
        return

    # Save consumers card info (the epay subscription id) if 'save card' is enabled in the app by the consumer
    # (then a subscriptionid GET parameter is received)
    def saveSubscriptionIfEnabled(self, request, consumer, transaction_id, obscured_card_number):
        callback_subscriptionid = request.get('subscriptionid')
        if callback_subscriptionid:
            callback_payment_type = request.get('paymenttype')
            callback_pincode = request.get('pincode')
            ps = EpaySubscription(parent=consumer,
                    status='active',
                    subscription_id=callback_subscriptionid,
                    transaction_id=transaction_id,
                    obscured_card_number=obscured_card_number,
                    payment_type=callback_payment_type)
            ps.set_pincode(callback_pincode)
            ps.put()

            consumer.current_payment_subscription = ps
            consumer.put()
            self.write_json({'key': str(ps.key())})
            logging.debug("subscription saved to database")


    def calculateExpectedHash(self):
        # We need the GET parameters in the order they appear in the query string. Unfortunately
        # self.request.arguments() does not give a ordered dictionary. Using urlparse module instead.

        received_parameter_values = ""
        for (argument, value) in parse_qsl(self.request.query_string):
            if argument != "hash":
                received_parameter_values += value

        md5key_from_epay_settings = "b80d9f7t83d45f"

        m = hashlib.md5()
        string_to_hash = received_parameter_values + md5key_from_epay_settings;
        m.update(string_to_hash)
        return m.hexdigest()

class EpayAcceptHandler(ApiHandler, DataCollector):
    def get(self):
        self.render('android/epaysuccess.html')
