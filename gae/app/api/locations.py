import os
from core import ApiHandler
from models.merchants import Merchant, Location, Bar
from models.catalog import Category, Menu
from geo.geomodel import geotypes
import webapp2
from models import i18ndata
from webapp2_extras import i18n
from app.api.data_collector import DataCollector
from google.appengine.api import search
from google.appengine.ext import db
_ = i18n.gettext
import urllib
import logging
from google.appengine.ext import blobstore
from google.appengine.ext.webapp import blobstore_handlers
from google.appengine.api import memcache

class CountryListCollector(DataCollector):
    def getData(self):
        """ List available countries """
        countries = [
            {'country_code': 'DK', 'name': _('Denmark')},
            {'country_code': 'GB', 'name': _('Great Britain')},
            {'country_code': 'ZA', 'name': _('South Africa')}
            # ,
            # {'country_code': 'ES', 'name': _('Spain')}
        ]
        return countries

class CountryListHandler(ApiHandler):
    def get(self):
        data_collector = CountryListCollector()
        self.write_json(data_collector.getData())

class LocationListBaseCollector(DataCollector):
    LOCATION_PROPERTIES = Location.API_PROPERTIES_COMPLETE

class LocationListCollector(LocationListBaseCollector):
    def getData(self):
        """ Get all locations """
        entities = self.get_list(Location.all().filter('enabled =', True), self.LOCATION_PROPERTIES)
        return entities

class LocationListHandler(ApiHandler):
    def get(self):
        data_collector = LocationListCollector()
        self.write_json(data_collector.getData())


class LocationListV2Collector(LocationListCollector):
    # New collector is faster, loads fewer properties
    LOCATION_PROPERTIES = Location.API_PROPERTIES_SPARSE
        
class LocationListV2Handler(LocationListHandler):
    def get(self):
        data_collector = LocationListV2Collector()
        self.write_json(data_collector.getData())


class LocationListNearbyCollector(LocationListBaseCollector):
    def getData(self, latitude, longitude):
        """ List max 20 locations within 400 km ordered by distance to latituted, longitude """
        max_results = 20
        max_distance = 400000 # meters

        # localhost server does not support distance queries in the search API, use the old geomodel instead
        if os.environ.get('SERVER_SOFTWARE','').startswith('Development'):
            location = geotypes.Point(float(latitude), float(longitude))
            entities = Location.proximity_fetch(
                Location.all().filter('enabled =', True),
                location,
                max_results=max_results,
                max_distance=max_distance
            )
            entities = self.get_list(entities, self.LOCATION_PROPERTIES)
            return entities

        # Find nearest locations using Search API
        index = search.Index("locations")
        query = "distance(location, geopoint(%s, %s)) < %s" % (latitude, longitude, max_distance)
        loc_expr = "distance(location, geopoint(%s, %s))" % (latitude, longitude)
        sortexpr = search.SortExpression(
            expression=loc_expr,
            direction=search.SortExpression.ASCENDING, default_value=max_distance+1)
        search_query = search.Query(
            query_string=query,
            options=search.QueryOptions(limit=max_results, sort_options=search.SortOptions(expressions=[sortexpr])))
        results = index.search(search_query)
        entities = []
        for doc in results:
            if doc["merchant_country"]:
                country = doc.field("merchant_country").value
            else: 
                country = 'DK' # same as default for this property, and only used for 4T
            entities.append({
                "key": doc.doc_id,
                "id": doc.field("id").value,
                "name": doc.field("name").value,
                "latitude": doc.field("location").value.latitude,
                "longitude": doc.field("location").value.longitude,
                "country": country, 
                "merchant": {
                    "key": doc.field("merchant_key").value, 
                    "image_url": webapp2.uri_for('image', entity_key=doc.field("merchant_key").value, width=120, height=120, _full=True)
                    }
                })
        return entities

class LocationListNearbyHandler(ApiHandler):
    def get(self, latitude, longitude):
        data_collector = LocationListNearbyCollector()
        self.write_json(data_collector.getData(latitude, longitude))

class LocationListNearbyV2Collector(LocationListNearbyCollector):
    # New handler is faster, loads fewer properties
    LOCATION_PROPERTIES = Location.API_PROPERTIES_SPARSE

class LocationListNearbyV2Handler(LocationListNearbyHandler):
    def get(self, latitude, longitude):
        data_collector = LocationListNearbyV2Collector()
        self.write_json(data_collector.getData(latitude, longitude))

class LocationsByCountryCollector(DataCollector):
    def getData(self, country):
        """ List all locations in country """
        if country not in i18ndata.ALLOWED_COUNTRIES:
            return None

        cache_key = "locationsbycountrykey"+str(country)
        location_entities = memcache.get(cache_key)
        if location_entities is None:
            # Locations don't have a country, but the merchant does
            # To avoid many DB calls, load everything in two queries and filter locations in memory
            merchants = Merchant.all().filter('country =', country)
            merchant_keys = [str(merchant.key()) for merchant in merchants]
            entities = Location.all().filter('enabled =', True).order('name')
            locations = [l for l in entities if str(Location.merchant.get_value_for_datastore(l)) in merchant_keys]
            # Save in cache
            location_entities = self.get_list(locations, Location.API_PROPERTIES_SPARSE)
            memcache.add(cache_key, location_entities, 60*60*24)
      
        return location_entities
        
class LocationsByCountryHandler(ApiHandler):
    def get(self, country):
        data_collector = LocationsByCountryCollector()
        data = data_collector.getData(country)
        if data is None:
            self.abort(404)
        else:
            self.write_json(data)


class ClosestLocationCollector(LocationListBaseCollector):
    def getData(self, latitude, longitude):
        """ Get the location closest to latitude, longitude """
        location = geotypes.Point(float(latitude), float(longitude))
        locations = Location.proximity_fetch(
            Location.all().filter('enabled =', True),
            location,
            max_results=1,
            max_distance=2000
        )
        data = {}
        if len(locations):
            location = locations[0]
            data = self.get_object(location, self.LOCATION_PROPERTIES)
        return data
        
class ClosestLocationHandler(ApiHandler):
    def get(self, latitude, longitude):
        data_collector = ClosestLocationCollector()
        self.write_json(data_collector.getData(latitude, longitude))


class LocationCollector(DataCollector):
    def getData(self, location_key, filter_bars=True):
        """
        Get location data, including entrance categories and list of bars.
        If there's only one bar, we also include the bar's menus.
        If there's only one bar with one menu, we also include the menu's categories.
        We filter for active bars by default, but this can be turned off to load inactive bars also (used for merchant preview).
        """
        try:
            entity = Location.get(location_key)
        except Exception:
            return None
        location = self.get_object(entity, Location.API_PROPERTIES_COMPLETE)
        if filter_bars:
            location['bars'] = [bar for bar in location['bars'] if bar['enabled']]
        # Add category list if there's only one bar with one menu
        if len(location['bars']) == 1:
            bar = Bar.get(location['bars'][0]['key'])
            if len(location['bars'][0]['menus']) == 1:
                menu = Menu.get(location['bars'][0]['menus'][0]['key'])
                categories = Category.get_categories_for_bar_and_menu(bar, menu)
                location['bars'][0]['menus'][0]['categories'] = self.get_list(categories, Category.API_PROPERTIES)
        # Add entrance categories
        location['entrance_categories'] = self.get_list(Category.get_categories_for_location(entity), Category.API_PROPERTIES)
        return location

class LocationHandler(ApiHandler):
    def get(self, location_key, filter_bars=True):
        data_collector = LocationCollector()
        data = data_collector.getData(location_key, filter_bars)
        if data is None:
            self.abort(404)
        else:
            self.write_json(data)

class LocationWithAllBarsHandler(LocationHandler):
    def get(self, location_key):
        # Show all bars in data to let the merchant preview them even if they are disabled
        super(LocationWithAllBarsHandler, self).get(location_key, filter_bars=False)

class MerchantLocationsCollector(DataCollector):
    def getData(self, merchant_key_or_id, require_enabled):
        """ Get all locations for merchant (used for merchant preview) """
        merchant_key = db.Key.from_path('Merchant', int(merchant_key_or_id)) if self.is_numeric(merchant_key_or_id) else merchant_key_or_id
        merchant = Merchant.get(merchant_key)
        if not merchant:
            return None

        if require_enabled:
            return self.get_list(merchant.locations.filter('enabled =', True).order('name'), Location.API_PROPERTIES_SPARSE)
        else:
            return self.get_list(merchant.locations.order('name'), Location.API_PROPERTIES_SPARSE)

    def is_numeric(self, s):
        try:
            int(s)
            return True
        except ValueError:
            return False

class MerchantLocationsHandler(ApiHandler):
    def get(self, merchant_key_or_id):
        data_collector = MerchantLocationsCollector()
        data = data_collector.getData(merchant_key_or_id, False)
        if data is None:
            self.abort(404, comment=_("Merchant not found"))
        else:
            self.write_json(data)


class LocationCoverServeHandler(ApiHandler, blobstore_handlers.BlobstoreDownloadHandler):
    def get(self, blob_key):
        print('###########------###########------###########------###########------###########------')
        """ Serve location picture from blobstore """
        resource = str(urllib.unquote(blob_key))
        blob_info = blobstore.BlobInfo.get(resource)
        self.send_blob(blob_info)
