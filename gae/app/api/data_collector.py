from collections import Iterable
import webapp2
from models.events import Event # Do no delete. This is needed for self.events lookup in merchants.py to work. Otherwise it might not be loaded in time and an AttributeError occurs.


class DataCollector():
    def get_list(self, entities, properties):
        """ Return list of entities as dictionary objects suitable for JSON API """
        return [self.get_object(entity, properties) for entity in entities]

    def get_object(self, entity, properties):
        """
        Return entity as a dictionary object suitable for JSON API.

        Example input:
        entity = product object
        properties = [name, price_formatted, (category, name)]

        Example result:
        {
            'key': 'abcd1234...',
            'name': 'Beer',
            'price_formatted': '35,00 kr.',
            'category': {
                'name': 'Beverages'
            }
        }
        """
        entitydata = {'key': str(entity.key())}
        # Fetch properties
        for prop in properties:
            # Basic property field
            if isinstance(prop, basestring):
                try:
                    attrdata = getattr(entity, prop)
                    entitydata[prop] = attrdata
                except AttributeError:
                    pass
                # For image property, use webapp2 to generate image URL
                if prop == 'image_url' and not hasattr(entity, 'image_url') and hasattr(entity, 'image') \
                        and entity.image:
                    entitydata[prop] = webapp2.uri_for('image', entity_key=entity.key(), width=120, height=120,
                                                       _full=True)
            # Child object property (tuple of child name and child properties)
            else:
                (reference, refproperties) = prop
                # If image url is the only property needed from the referenced object, we don't have to load the object
                if refproperties == ['image_url']:
                    # Fetch the key of the referenced object and construct an image URL with the key
                    referencekey = entity.properties()[reference].get_value_for_datastore(entity)
                    entitydata[reference] = {
                        'key': str(referencekey),
                        'image_url': webapp2.uri_for('image', entity_key=referencekey, width=120, height=120,
                                                     _full=True),
                        }
                # Load the referenced object(s)
                else:
                    referenceobject = getattr(entity, reference)
                    if referenceobject:
                        # List of child objects
                        if isinstance(referenceobject, Iterable):
                            entitydata[reference] = [self.get_object(child, refproperties) for child in referenceobject]
                        # Single child object
                        else:
                            entitydata[reference] = self.get_object(getattr(entity, reference), refproperties)
        return entitydata


