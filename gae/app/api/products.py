from core import ApiHandler
from models.merchants import Merchant, Location, Bar
from models.catalog import Category, Product, PriceRule, Menu
from google.appengine.ext import db
from webapp2_extras import i18n
from app.api.data_collector import DataCollector
_ = i18n.gettext

class ProductListLegacyCollector(DataCollector):
    def getData(self, category_key, location_or_bar_key):
        # Deprecated
        category = Category.get(category_key)
        products = Product.all().filter('category =', category)
        if category.group == 'menu':
            products.filter('enabled_bars =', db.Key(location_or_bar_key))
            bar = Bar.get(location_or_bar_key)
            merchant_open = bar.open
        else:
            products.filter('enabled_locations =', db.Key(location_or_bar_key))
            location = Location.get(location_or_bar_key)
            merchant_open = location.enabled
        products.order('sort_order')
        productlist = self.get_list(products, Product.API_PROPERTIES)
        for p in productlist:
            p['merchant_open'] = merchant_open
        return productlist
        
class ProductListLegacyHandler(ApiHandler):
    def get(self, category_key, location_or_bar_key):
        # Deprecated
        data_collector = ProductListLegacyCollector()
        self.write_json(data_collector.getData(category_key, location_or_bar_key))


class ProductListCollector(DataCollector):
    def getData(self, category_key, menu_key=None, location_key=None, bar_key=None):
        """ List all products in category available on menu in bar, or in location """
        # Load category
        try:
            category = Category.get(category_key)
            if menu_key:
                menu = Menu.get(menu_key)
        except Exception:
            return None

        bar = None
        # Load product list from cache
        if category.group == 'menu':
            bar = Bar.get(bar_key)
            if not menu_key:
                menu = Menu.get_active_menus(bar).fetch(1)[0]
                menu_key = str(menu.key())

            products = category.get_cached_products(db.Key(menu_key))

        else: 
            products = category.get_cached_products(db.Key(location_key))

        # Load location/bar info
        message_when_closed = _("Closed for orders")
        if category.group == 'menu':
            merchant_open = bar.open
            merchant = bar.merchant
            message_when_closed = bar.message_when_closed
            menu_active = menu.active
        else:
            location = Location.get(location_key)
            merchant_open = location.enabled
            merchant = location.merchant
            menu_active = True
        # Preload price rules
        products = Product.prefetch_merchant_price_rules(products, merchant)
        # Preload merchant
        for p in products:
            p.merchant = merchant
        productlist = self.get_list(products, Product.API_PROPERTIES)
        for p in productlist:
            p['merchant_open'] = merchant_open
            p['message_when_closed'] = message_when_closed
            p['menu_active'] = menu_active
        return productlist


class ProductListHandler(ApiHandler):
    def get(self, category_key, menu_key=None, location_key=None, bar_key=None):
        data_collector = ProductListCollector()
        data = data_collector.getData(category_key, menu_key, location_key, bar_key)
        if data is None:
            self.abort(404)
        else:
            self.write_json(data)
        
class ProductCollector(DataCollector):
    def getData(self, product_key):
        try:
            product = Product.get(product_key)
            return self.get_object(product, Product.API_PROPERTIES)
        except Exception:
            return None


class ProductHandler(ApiHandler):
    def get(self, product_key):
        data_collector = ProductCollector()
        data = data_collector.getData(product_key)
        if data is None:
            self.abort(404)
        else:
            self.write_json(data)