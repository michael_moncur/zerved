from core import ApiHandler
from models.merchants import Merchant, Location, Bar
from models.catalog import Category, Menu
from google.appengine.api import memcache
from app.api.data_collector import DataCollector
import logging

class CategoryListLegacyHandler(ApiHandler, DataCollector):
    def get(self, bar_key, group):
        """ List categories. Deprecated because locations/bars/menus have been added. """
        bar = Bar.get(bar_key)
        categories = Category.all().filter('merchant =', bar.merchant).filter('group =', group).filter('enabled =', True).order('sort_order')
        entities = []
        for category in categories:
            entity = self.get_object(category, Category.API_PROPERTIES)
            if category.group == 'menu':
                entity['key'] = entity['key'] + "/" + bar_key
            else:
                entity['key'] = entity['key'] + "/" + str(bar.location.key())
            entities.append(entity)
        self.write_json(entities)
        
class LocationCategoryListHandler(ApiHandler, DataCollector):
    def get(self, location_key):
        """ List categories for location """
        location = Location.get(location_key)
        categories = Category.get_categories_for_location(location)
        self.write_json(self.get_list(categories, Category.API_PROPERTIES))
        
class BarCategoryListHandler(ApiHandler, DataCollector):
    def get(self, bar_key):
        """ List categories for bar without specifying the menu (we use first available menu) """
        bar = Bar.get(bar_key)
        menus = Menu.get_active_menus(bar).fetch(1)
        if len(menus) == 0:
            self.response.headers['Content-Type'] = 'application/json'
            self.response.out.write('[]')
        else:
            cache_key = "categories_menu_"+str(menus[0].key().id())
            categories = None #memcache.get(cache_key)
            if categories is None:
                categories = Category.all().filter('merchant =', bar.merchant).filter('group =', 'menu').filter('enabled =', True).order('sort_order')
                categories = [c for c in categories if c.has_products_menu(bar, menus[0])]
                # Save in cache
                memcache.add(cache_key, categories, 60*60*24)
            self.write_json(self.get_list(categories, Category.API_PROPERTIES))
        
class CategoryHandler(ApiHandler, DataCollector):
    def get(self, category_key):
        """ Get category data """
        try:
            category = Category.get(category_key)
        except Exception:
            self.abort(404)
        self.write_json(self.get_object(category, Category.API_PROPERTIES))