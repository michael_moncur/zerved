from core import ApiHandler
from models.consumers import Consumer, EmailConsumer, ConsumerNotification
from models.payment import PaymentSubscription, NordpaySubscription
from google.appengine.ext import db
from webapp2_extras import i18n
_ = i18n.gettext
import logging
from app.api.data_collector import DataCollector

class ConsumerHandler(ApiHandler, DataCollector):
    def post(self, consumer_key):
        """ Update existing consumer """
        entitydata = {}
        entity = self.init_consumer(consumer_key)
        postdata = self.read_json()
        if 'current_payment_subscription' in postdata:
            key = postdata['current_payment_subscription']['key'] if type(postdata['current_payment_subscription']) is dict else postdata['current_payment_subscription']
            ps = PaymentSubscription.get(key)
            if not ps:
                self.abort(404, comment=_("Payment subscription not found"))
            if ps.parent().key() != entity.key():
                self.abort(401, comment=_("Unauthorized"))
            entity.current_payment_subscription = ps
        if 'gcm_registration_id' in postdata:
            entity.device = 'android'
            entity.gcm_registration_id = postdata['gcm_registration_id']
        for field in ['name', 'phone', 'address1', 'address2', 'postcode', 'city']:
            if field in postdata:
                setattr(entity, field, postdata[field])
        entity.put()
        if 'email' in postdata and postdata['email'] != entity.email:
            # Try change email
            oldemail = entity.email
            if postdata['email'] and EmailConsumer.get_by_key_name(postdata['email']):
                self.abort(409, comment=_("Email address is already registered"))
            if oldemail:
                oldemailconsumer = EmailConsumer.get_by_key_name(oldemail)
                if oldemailconsumer:
                    oldemailconsumer.delete()
            entity.email = postdata['email']
            entity.put()
            if postdata['email']:
                emailconsumer = EmailConsumer.get_or_insert(postdata['email'])
                emailconsumer.consumer = entity
                emailconsumer.put()
            entitydata['access_token'] = entity.save_login_session()
        # Return updated consumer
        payment_subscriptions = PaymentSubscription.all().ancestor(entity).filter('status =', 'active')
        entitydata.update(self.get_object(entity, Consumer.API_PROPERTIES))
        entitydata['payment_subscriptions'] = [self.get_object(p, ['description']) for p in payment_subscriptions]
        self.write_json(entitydata)
        
    def get(self, consumer_key):
        """ Get consumer data """
        entity = self.init_consumer(consumer_key)
        payment_subscriptions = PaymentSubscription.all().ancestor(entity).filter('status =', 'active')
        entitydata = self.get_object(entity, Consumer.API_PROPERTIES)
        entitydata['payment_subscriptions'] = [self.get_object(p, ['description']) for p in payment_subscriptions]
        logging.info(entitydata)
        self.write_json(entitydata)
        
    def delete(self, consumer_key):
        """ Delete consumer account """
        entity = self.init_consumer(consumer_key)
        payment_subscriptions = PaymentSubscription.all().ancestor(entity)
        for ps in payment_subscriptions:
            ps.delete()
        emailconsumer = EmailConsumer.get_by_key_name(entity.email) if entity.email else None
        entity.delete()
        if emailconsumer:
            emailconsumer.delete()
        
    def init_consumer(self, consumer_key):
        """ Load consumer and verify that it is the same as the logged in consumer """
        try:
            entity = Consumer.get(consumer_key)
        except Exception:
            self.abort(404)
        self.authenticate()
        self.authorize(entity)
        return entity
        
    def authorize(self, consumer):
        """ A consumer can only access his own account """
        if not (self.user and consumer and str(self.user.key()) == str(consumer.key())):
            self.abort(401, comment="Unauthorized")
            
class CreateConsumerHandler(ApiHandler, DataCollector):
    def post(self):
        """ Create new consumer account """
        postdata = self.read_json()
        if 'email' in postdata and 'password' in postdata and 'device' in postdata:
            if postdata['device'] not in ['iphone', 'android']:
                self.abort(400, comment=_("Device not recognized"))
            if EmailConsumer.get_by_key_name(postdata['email']):
                self.abort(401, comment=_("Email already exists"))
            consumer = Consumer(email=postdata['email'], device=postdata['device'])
            consumer.set_password(postdata['password'])
            consumer.put()
            emailconsumer = EmailConsumer.get_or_insert(postdata['email'], consumer=consumer)
            emailconsumer.put()
            consumer.update_embedded_client_push_data(self.request.cookies.get(u'embeddedClientPushData'))
            # Return consumer key and access token for the new consumer
            access_token = consumer.login(postdata['password'])
            self.write_json({'key': str(consumer.key()), 'access_token': access_token, 'id': str(consumer.key().id()) })
        else:
            consumer = Consumer()
            if 'device' in postdata:
                consumer.device = postdata['device']
            password = consumer.generate_password()
            consumer.put()
            consumer.update_embedded_client_push_data(self.request.cookies.get(u'embeddedClientPushData'))
            access_token = consumer.login(password)
            # Return consumer key, password and access token for the new consumer
            # Client should store consumer key and password for future logins.
            # Access token is used when logged in.
            self.write_json({'key': str(consumer.key()), 'password': password, 'access_token': access_token, 'id': str(consumer.key().id()) })
            
class LoginHandler(ApiHandler, DataCollector):
    def post(self):
        """ 
        Log in consumer based on consumer key+password or email+password
        Return an access token which should be used in the header of future requests
        """
        postdata = self.read_json()
        if ('email' in postdata or 'key' in postdata) and 'password' in postdata:
            if 'email' in postdata:
                emailconsumer = EmailConsumer.get_by_key_name(postdata['email'])
                if not emailconsumer:
                    self.abort(404, comment=_("User not found"))
                consumer = emailconsumer.consumer
            else:
                consumer = Consumer.get(postdata['key'])
            if not consumer:
                self.abort(404, comment=_("User not found"))
            result = consumer.login(postdata['password'])
            if result:
                push_data = self.request.cookies.get(u'embeddedClientPushData')
                consumer.update_embedded_client_push_data(push_data)
                self.write_json({ 'key': str(consumer.key()), 'access_token': result, 'id': str(consumer.key().id()) })
            else:
                self.abort(401, comment=_("Incorrect login"))
        else:
            self.abort(401, comment=_("Email and password, or consumer key and password is required"))
            
class PaymentSubscriptionHandler(ApiHandler, DataCollector):
    def delete(self, ps_key):
        """ Delete card """
        self.authenticate()
        ps = self.init_payment_subscription(ps_key)
        ps.delete()
        
    def post(self, ps_key):
        """ Update payment data (password) """
        self.authenticate()
        ps = self.init_payment_subscription(ps_key)
        postdata = self.read_json()
        if 'password' in postdata and 'old_password' in postdata:
            # Update payment password
            if ps.password != "" and not ps.verify_password(postdata['old_password']):
                self.abort(403, comment=_("Wrong payment password"))
            def txn():
                subscription = PaymentSubscription.get(ps.key())
                subscription.set_password(postdata['password'])
                subscription.put()
            db.run_in_transaction(txn)
            
    def init_payment_subscription(self, ps_key):
        """ Load payment subscription """
        if not self.user:
            self.abort(401, comment=_("Unauthorized"))
            return
        if ps_key == "current":
            ps = self.user.current_payment_subscription
        else:
            ps = PaymentSubscription.get(ps_key)
        if not ps:
            self.abort(404, comment=_("Payment subscription not found"))
        self.authorize(ps)
        return ps
        
    def authorize(self, payment_subscription):
        """ Payment subscription object can only be accessed by the owner """
        if str(payment_subscription.parent().key()) != str(self.user.key()):
            self.abort(401, comment=_("Unauthorized"))

#deprecated, not used anymore as nordpay support has been removed.
class PaymentSubscriptionCreateHandler(ApiHandler, DataCollector):
    def post(self):
        self.abort(410, comment=_("Outdated app version. Please update."))
        return
    '''
        """ Save a new payment subscription """
        self.authenticate()
        if not self.user:
            self.abort(401, comment=_("Login expired, please go back to profile and try again"))
            return
        postdata = self.read_json()
        logging.info(postdata)
        method = postdata['payment_method'] if 'payment_method' in postdata else 'nordpay'
        if method == 'nordpay':
            # For Nordpay, the client has saved the card to Nordpay directly and we just need to save the Nordpay identifier
            if 'identifier' in postdata and 'pincode' in postdata:
                card_method = postdata['nordpay_method'] if 'nordpay_method' in postdata else 'CC'
                ps = NordpaySubscription(parent=self.user, 
                    status='active',
                    identifier=postdata['identifier'],
                    obscured_card_number=postdata['obscured_card_number'],
                    method=card_method)
                ps.set_pincode(postdata['pincode'])
                ps.put()
                self.user.current_payment_subscription = ps
                self.user.put()
                self.write_json({'key': str(ps.key())})
    '''

class ResetPasswordHandler(ApiHandler, DataCollector):
    def post(self):
        postdata = self.read_json()
        if 'email' in postdata:
            emailconsumer = EmailConsumer.get_by_key_name(postdata['email'])
            if not emailconsumer:
                self.abort(404, comment=_("User not found"))
            consumer = emailconsumer.consumer
            if not consumer:
                self.abort(404, comment=_("User not found"))
            consumer.init_forgot_password(self)
        else:
            self.abort(401, comment=_("Email required"))

class PullNotificationsHandler(ApiHandler, DataCollector):
    def post(self, consumer_key):
        """
        Get all pending notifications for consumer.
        Used as a backup when Google Cloud Messaging delivery is delayed.
        Client can pull this URL repeatedly when an order has been placed but should stop when receiving HTTP 204.
        """
        # Authenticate
        try:
            consumer = Consumer.get(consumer_key)
        except Exception:
            self.abort(404)
        # Authorize
        postdata = self.read_json()
        if 'password' not in postdata or not consumer.verify_password(postdata['password']):
            self.abort(401)
        # Pull all pending notifications
        notifications = consumer.pull_notifications()
        # If there are notifications, return them
        if len(notifications) > 0:
            logging.info(self.get_list(notifications, ConsumerNotification.API_PROPERTIES))
            self.write_json(self.get_list(notifications, ConsumerNotification.API_PROPERTIES))
        else:
            # If no notifications, check for open orders
            has_open_orders = consumer.orders.filter('status IN', ['pending', 'preparing']).count(1) > 0
            # If no open orders, use status code 204 No Content to indicate the client does not have to call again
            if has_open_orders:
                self.response.write('[]')
            else:
                self.response.set_status(204)