from core import ApiHandler
from models.merchants import Location, Bar
from models.catalog import Menu, Category
from models.events import Event
from models.sales import Order
from app.api.data_collector import DataCollector
import datetime, pytz
import logging
from webapp2_extras import i18n
_ = i18n.gettext

class EventListByDateCollector(DataCollector):
    def getData(self, location_key, year, month, day):
        print(year)
        print(month)
        print(day)
        """ Get events for location on a certain day (in merchant timezone) """
        try:
            entity = Location.get(location_key)
        except Exception:
            return None
        # Convert merchant date to UTC
        startdate = datetime.datetime(int(year), int(month), int(day), 0, 0, 0, tzinfo=pytz.timezone(entity.merchant.timezone)).astimezone(pytz.utc)
        enddate = datetime.datetime(int(year), int(month), int(day), 23, 59, 59, tzinfo=pytz.timezone(entity.merchant.timezone)).astimezone(pytz.utc)
        logging.info(startdate)
        logging.info(enddate)
        events = entity.events.filter('date_delivery >=', startdate).filter('date_delivery <=', enddate).order('date_delivery')
        return self.get_list(events, Event.API_PROPERTIES)


class EventListByDateHandler(ApiHandler):
    def get(self, location_key, year, month, day):
        data_collector = EventListByDateCollector()
        data = data_collector.getData(location_key, year, month, day)
        if data is None:
            self.abort(404)
        else:
            self.write_json(data)

class EventHandler(ApiHandler, DataCollector):
    def get(self, event_key):
        """ Get event data """
        try:
            event = Event.get(event_key)
        except Exception:
            self.abort(404)
        eventdata = self.get_object(event, Event.API_PROPERTIES)
        eventdata['bars'] = []
        # Add bars
        for bar in event.bars:
            if bar.enabled:

                # we only want to include bars with less than 300 orders for this event
                # FIXME hardcoded limit !!!

                order_count = bar.orders.\
                        filter('event_name = ', event.name).\
                        filter('date_delivery =', event.date_delivery).\
                        filter('event_time_label  = ', event.time_label).\
                        filter('status IN', ['pending', 'preparing']).\
                        filter('service IN', ['counter', 'table', 'delivery']).\
                        count()

                if order_count > 300:
                    continue

                bardata = self.get_object(bar, Bar.API_PROPERTIES)
                # Load menus that are active at the bar at the event time
                bardata['menus'] = self.get_list(Menu.get_active_menus_at(bar, event.date_delivery), Menu.API_PROPERTIES)
                eventdata['bars'].append(bardata)

        # Add category list if there's only one bar with one menu
        if len(eventdata['bars']) == 1:
            bar = Bar.get(eventdata['bars'][0]['key'])
            if len(eventdata['bars'][0]['menus']) == 1:
                menu = Menu.get(eventdata['bars'][0]['menus'][0]['key'])
                categories = Category.get_categories_for_bar_and_menu(bar, menu)
                eventdata['bars'][0]['menus'][0]['categories'] = self.get_list(categories, Category.API_PROPERTIES)
        self.write_json(eventdata)
