from core import ApiHandler
from models.catalog import Menu
from models.merchants import Bar
from google.appengine.ext import db
import logging
from app.api.data_collector import DataCollector
from models.merchants import Merchant, Location, Bar
from models.catalog import Category
from google.appengine.api import memcache

class BarMenuListCollector(DataCollector):
    def getData(self, bar_key):
        """ List menus for bar """
    	menus = []
        query = Menu.all().order('sort_order')
        for menu in query:
        	if bar_key in [str(key) for key in menu.bars]:
        		menus.append(menu)

        logging.info(menus)
        return self.get_list(menus, Menu.API_PROPERTIES)

class BarMenusListHandler(ApiHandler):
    def get(self, bar_key):
        data_collector = BarMenuListCollector()
        self.write_json(data_collector.getData(bar_key))


class BarMenuCategoryListCollector(DataCollector):
    def getData(self, bar_key, menu_key):
        """ List categories on menu in bar """
        bar = Bar.get(bar_key)
        menu = Menu.get(menu_key)
        categories = Category.get_categories_for_bar_and_menu(bar, menu)
        return self.get_list(categories, Category.API_PROPERTIES)


class BarMenuCategoryListHandler(ApiHandler):
    def get(self, bar_key, menu_key):
        data_collector = BarMenuCategoryListCollector()
        self.write_json(data_collector.getData(bar_key, menu_key))