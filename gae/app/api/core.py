import webapp2
from webapp2_extras import i18n, jinja2
import json
import base64
import logging
from decimal import Decimal
import datetime
from google.appengine.ext import db
from models import i18ndata
from models.merchants import Merchant
from models.consumers import Consumer
from models.catalog import Product

def jinja2_factory(*args, **kwargs):
    jinja_config={
        'environment_args': {
            'extensions': [
                'jinja2.ext.i18n'
            ]
        },
        'globals': {
            'format_currency': i18n.format_currency,
            'format_date': i18n.format_date,
            'format_datetime': i18n.format_datetime,
        }
    }
    return jinja2.Jinja2(*args, config=jinja_config, **kwargs)
    
class ApiHandler(webapp2.RequestHandler):
    """
    Base handler for REST API requests.
    Language set from Accept-Language header.
    Authentication based on Authorization header.
    Contains functions for reading and writing JSON, and converting Zerved models to JSON.
    """
    @webapp2.cached_property
    def jinja2(self):
        # Returns a Jinja2 renderer cached in the app registry.
        return jinja2.get_jinja2(factory=jinja2_factory, app=self.app)
        
    def initialize(self, request, response):
        self.init_locale(request, response)

        super(ApiHandler, self).initialize(request, response)
        
    def init_locale(self, request, response):
        # Find locale in accept-language header
        header = request.headers.get('Accept-Language', '')
        locales = [locale.split(';')[0] for locale in header.split(',')]
        languages = [locale.split('-')[0] for locale in locales]
        for locale in locales:
            language = locale.split('-')[0]
            # Check for matching locale
            if locale in i18ndata.ALLOWED_LOCALES:
                i18n.get_i18n().set_locale(locale)
                break
            # Check for matching language (with different country)
            elif language in i18ndata.LANGUAGE_LOCALES:
                i18n.get_i18n().set_locale(i18ndata.LANGUAGE_LOCALES[language])
                break
        else:
            # if still no locale set, use the first available one
            i18n.get_i18n().set_locale('en_GB')
                
    # def get_list(self, entities, properties):
    #     """ Return list of entities as dictionary objects suitable for JSON API """
    #     return [self.get_object(entity, properties) for entity in entities]
    #
    # def get_object(self, entity, properties):
    #     """
    #     Return entity as a dictionary object suitable for JSON API.
    #
    #     Example input:
    #     entity = product object
    #     properties = [name, price_formatted, (category, name)]
    #
    #     Example result:
    #     {
    #         'key': 'abcd1234...',
    #         'name': 'Beer',
    #         'price_formatted': '35,00 kr.',
    #         'category': {
    #             'name': 'Beverages'
    #         }
    #     }
    #     """
    #     entitydata = {'key': str(entity.key())}
    #     # Fetch properties
    #     for prop in properties:
    #         # Basic property field
    #         if isinstance(prop, basestring):
    #             try:
    #                 attrdata = getattr(entity, prop)
    #                 entitydata[prop] = attrdata
    #             except AttributeError:
    #                 pass
    #             # For image property, use webapp2 to generate image URL
    #             if prop == 'image_url' and not hasattr(entity, 'image_url') and hasattr(entity, 'image') \
    #                     and entity.image:
    #                 entitydata[prop] = webapp2.uri_for('image', entity_key=entity.key(), width=120, height=120,
    #                                                    _full=True)
    #         # Child object property (tuple of child name and child properties)
    #         else:
    #             (reference, refproperties) = prop
    #             # If image url is the only property needed from the referenced object, we don't have to load the object
    #             if refproperties == ['image_url']:
    #                 # Fetch the key of the referenced object and construct an image URL with the key
    #                 referencekey = entity.properties()[reference].get_value_for_datastore(entity)
    #                 entitydata[reference] = {
    #                     'key': str(referencekey),
    #                     'image_url': webapp2.uri_for('image', entity_key=referencekey, width=120, height=120,
    #                                                  _full=True),
    #                     }
    #             # Load the referenced object(s)
    #             else:
    #                 referenceobject = getattr(entity, reference)
    #                 if referenceobject:
    #                     # List of child objects
    #                     if isinstance(referenceobject, Iterable):
    #                         entitydata[reference] = [self.get_object(child, refproperties) for child in referenceobject]
    #                     # Single child object
    #                     else:
    #                         entitydata[reference] = self.get_object(getattr(entity, reference), refproperties)
    #     return entitydata
        
    def write_json(self, data):
        """ Dump data as a JSON string to response output """
        self.response.headers['Content-Type'] = 'application/json'
        self.response.out.write(json.dumps(data, cls=ApiJSONEncoder, ensure_ascii=False))
        
    def read_json(self):
        """ Read request body as JSON string """
        return json.loads(self.request.body)
    
    def handle_exception(self, exception, debug):
        """ When an exception occurs, return the error message as the response body, and log the exception """
        if isinstance(exception, webapp2.HTTPException):
            self.response.set_status(exception.code)
        else:
            self.response.set_status(500)
            logging.error(exception)
        if hasattr(exception, 'comment'):
            self.response.write(exception.comment)
        else:
            self.response.write(exception)
    
    def authenticate(self):
        """ Authenticate consumer via HTTP Authorization header (see login API) """
        self.user = None
        try:
            auth_header = self.request.headers["Authorization"]
            # Isolate the encoded access token and decode it
            auth_parts = auth_header.split(' ')
            access_token = base64.b64decode(auth_parts[1])
            self.user = Consumer.authenticate(access_token)
        except Exception as e:
            logging.info(e)
            self.abort(401, comment=e)
            
class ApiJSONEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, Decimal):
            return float(obj)
        elif isinstance(obj, datetime.datetime):
            return obj.isoformat()
        return json.JSONEncoder.default(self, obj)
        
from google.appengine.api import images
class ImageHandler(webapp2.RequestHandler):
    def get(self, entity_key, width, height):

        """ Render resized image fetched from object defined by entity_key """
        entity = db.Model.get(entity_key)
        if not entity or not entity.image:
            self.abort(404)
        resizedimage = entity.image
        try:
            image = images.Image(entity.image)
            if int(width) > 0 and int(height) > 0:
                # Width and height specified, resize and put in white box
                image.resize(width=int(width), height=int(height))
                image = images.Image(image.execute_transforms(output_encoding=images.JPEG))
                offsetx = (int(width)-image.width)/2.0
                offsety = (int(height)-image.height)/2.0
                resizedimage = images.composite([(image, int(offsetx), int(offsety), 1.0, images.TOP_LEFT)], int(width), int(height), color=0xFFFFFFFF, output_encoding=images.JPEG)
            elif int(width) > 0:
                # Resize using auto height
                image.resize(width=int(width))
                resizedimage = image.execute_transforms(output_encoding=images.JPEG)
        except NotImplementedError:
            pass
        self.response.headers['Content-Type'] = str(entity.image_type)
        self.response.out.write(resizedimage)