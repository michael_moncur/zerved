import datetime
from google.appengine.ext import db
import hashlib
import json
from webapp2 import uri_for
from webapp2_extras import i18n
from app.api.core import ApiJSONEncoder
from app.api.locations import MerchantLocationsCollector, LocationCollector
from app.api.menus import BarMenuListCollector, BarMenuCategoryListCollector
from app.api.products import ProductListCollector, ProductCollector
from models.catalog import Category, Menu
from models.environment import get_environment
from models.merchants import Merchant, Location, Bar
from views.front.main import FrontHandler
from models.epaymerchantnumber import EpayMerchantNumber

md5key_from_epay_settings = "b80d9f7t83d45f"

class EmbeddedHomeHandler(FrontHandler):


    def get(self, merchant_id):
        # -----------------------------------------------------------------
        # TODO: Remove this HACK!
        # If the merchant is the old Telia Parken, replace it with the new.
        if merchant_id == '5821113057673216':
            merchant_id = '6067254737240064'
        # -----------------------------------------------------------------

        self.set_cookie('embeddedMerchantId', str(merchant_id), expires=(datetime.datetime.now() + datetime.timedelta(days=3650)))
        external_user_id = self.request.get('external_user_id', default_value=None) # default None ensures the cookie is deleted if no id is given
        client = self.request.get('client', default_value='TeliaParken') # TODO: Make this depend on merchant id.
        push_data = {
            'merchant_id': str(merchant_id),
            'client': client,
            'external_user_id': external_user_id
        }
        self.set_cookie('embeddedClientPushData', json.dumps(push_data, cls=ApiJSONEncoder, ensure_ascii=False), expires=(datetime.datetime.now() + datetime.timedelta(days=3650)))
        # print('--------')
        # print('--------')
        # print(client)
        # print(external_user_id)
        # print('-------.')
        self.redirect(uri_for('embedded-tab', tab='shop'))


class EmbeddedTabHandler(FrontHandler):
    def get(self, tab):
        # print('-----')
        # print(self.request.cookies.get(u'embeddedClientPushData'))
        # print('-----')
        tabs = ['shop', 'orders', 'favorites', 'profile', 'info']

        if tab not in tabs:
            self.abort(404)

        merchant_id = self.request.cookies.get(u'embeddedMerchantId')
        if merchant_id is None:
            self.abort(400) # Bad Request

        merchant = Merchant.get(db.Key.from_path('Merchant', int(merchant_id)))
        data_collector = MerchantLocationsCollector()
        data = data_collector.getData(merchant_id, True)

        title = ''
        if tab == tabs[0]:
            title = merchant.name
        elif tab == tabs[1]:
            title = i18n.gettext('Orders')
        elif tab == tabs[2]:
            title = i18n.gettext('Favourites')
        elif tab == tabs[3]:
            title = i18n.gettext('Profile')
        elif tab == tabs[4]:
            title = i18n.gettext('Info')

        self.render('embedded/'+i18n.get_i18n().locale.split('_')[0]+'/' + tab + '.html', tab=tab, data=data, disable_back_button=True, title=title)


class EmbeddedLocationHandler(FrontHandler):
    def get(self, location_key):

        data_collector = LocationCollector()
        data = data_collector.getData(location_key)

        has_open_bars = False
        for bar in data['bars']:
            if bar.get('open', False):
                has_open_bars = True

        number_of_sections = 0
        if len(data['entrance_categories']) > 0:
            number_of_sections = number_of_sections + 1
        if len(data['bars']) > 0 or len(data['upcoming_events']) > 0:
            number_of_sections = number_of_sections + 1

        section_types = []
        for i in range(number_of_sections):
            section = self.sectionTypeForSection(i, data, has_open_bars)
            section_types.append(section)

        json_data = json.dumps(data, cls=ApiJSONEncoder, ensure_ascii=False)

        self.render('embedded/'+i18n.get_i18n().locale.split('_')[0]+'/location.html', location=data, json_location=json_data, section_types=section_types, has_open_bars=has_open_bars, tab='shop', title=data['name'])


    def sectionTypeForSection(self, section, location, has_open_bars):
        if section == 0 and len(location['entrance_categories']) > 0:
            return 'entrance_categories'
        else:
            if len(location['upcoming_events']) > 0 and not has_open_bars:
                # Preorder only
                return 'events'
            elif len(location['upcoming_events']) == 0:
                # Real time orders only
                if len(location['bars']) > 1:
                    # Bars
                    return 'bars'
                else:
                    bar = location['bars'][0]
                    if len(bar['menus']) > 1:
                        # Menus
                        return 'menus'
                    elif len(bar['menus']) == 1:
                        # Categories
                        return 'menu_categories'
            else:
                # Both preorder and realtime
                return 'now_and_events'
        # Should not happen
        return None


class EmbeddedBarsHandler(FrontHandler):
    def get(self, location_key):
        data_collector = LocationCollector()
        data = data_collector.getData(location_key)
        bars = data['bars']
        self.render('embedded/'+i18n.get_i18n().locale.split('_')[0]+'/bars.html', data=bars, tab='shop', title='') # Title is set dynamically based on event state


class EmbeddedBarMenusHandler(FrontHandler):
    def get(self, bar_key):
        data_collector = BarMenuListCollector()
        data = data_collector.getData(bar_key)
        bar = Bar.get(bar_key)
        self.render('embedded/'+i18n.get_i18n().locale.split('_')[0]+'/barmenus.html', data=data, tab='shop', title=bar.name)


class EmbeddedMenuCategoryListHandler(FrontHandler):
    def get(self, bar_key, menu_key):
        data_collector = BarMenuCategoryListCollector()
        data = data_collector.getData(bar_key, menu_key)
        # menu = Menu.get(menu_key)
        json_data = json.dumps(data, cls=ApiJSONEncoder, ensure_ascii=False)
        self.render('embedded/'+i18n.get_i18n().locale.split('_')[0]+'/menucategories.html', data=data, json_data=json_data, tab='shop', cart_button=True, title='') # Title is set dynamically based on localStorage value


class ProductListHandler(FrontHandler):
    def get(self, category_key, menu_key=None, location_key=None, bar_key=None):
        data_collector = ProductListCollector()
        data = data_collector.getData(category_key, menu_key, location_key, bar_key)
        category = Category.get(category_key)

        json_data = json.dumps(data, cls=ApiJSONEncoder, ensure_ascii=False)

        self.render('embedded/'+i18n.get_i18n().locale.split('_')[0]+'/products.html', data=data, json_data=json_data, tab='shop', cart_button=True, title=category.name)


class ProductHandler(FrontHandler):
    def get(self, product_key):
        data_collector = ProductCollector()
        data = data_collector.getData(product_key)
        self.render('embedded/'+i18n.get_i18n().locale.split('_')[0]+'/product.html', product=data, tab='shop', cart_button=True, title=data['name'])


class EventListHandler(FrontHandler):
    def get(self, location_key):
        self.render('embedded/'+i18n.get_i18n().locale.split('_')[0]+'/events.html', location_key=location_key, tab='shop', title=i18n.gettext('Events'))


class CartHandler(FrontHandler):
    def get(self, group, location_key=None, bar_key=None, event_key=None):
        if location_key:
            location = Location.get(location_key)
        else:
            location = None

        if bar_key:
            bar = Bar.get(bar_key)
        else:
            bar = None

        self.render('embedded/'+i18n.get_i18n().locale.split('_')[0]+'/cart.html', group=group, location=location, bar=bar, event_key=event_key, tab='shop', title=i18n.gettext('Cart'))


class EPayHandler(FrontHandler):
    def get(self):
        epay_merchant_number = EpayMerchantNumber.epay_merchantnumber()
        self.render('embedded/'+i18n.get_i18n().locale.split('_')[0]+'/epay.html', epay_merchant_number=epay_merchant_number, epay_md5_hash_key=md5key_from_epay_settings, host=self.request.host, scheme=self.request.scheme, tab='shop', title=i18n.gettext('Payment'))

class MobilePayNativeEmbeddedHandler(FrontHandler):
    def get(self):
        epay_merchant_number = EpayMerchantNumber.epay_merchantnumber()
        self.render('embedded/'+i18n.get_i18n().locale.split('_')[0]+'/mobilepaynativeembedded.html', epay_merchant_number=epay_merchant_number, epay_md5_hash_key=md5key_from_epay_settings, host=self.request.host, scheme=self.request.scheme, tab='shop', title=i18n.gettext('Payment'))

class EPaySaveCardHandler(FrontHandler):
    def get(self):
        epay_merchant_number = EpayMerchantNumber.epay_merchantnumber()
        self.render('embedded/'+i18n.get_i18n().locale.split('_')[0]+'/epay.html', epay_merchant_number=epay_merchant_number, epay_md5_hash_key=md5key_from_epay_settings, host=self.request.host, scheme=self.request.scheme, tab='profile', title=i18n.gettext('Payment'), save_card_mode=True)

class EPayAcceptedHandler(FrontHandler):
    def get(self):
        string_to_hash = ''
        received_hash = ''
        for k, v in self.request.GET.items():
            if k == 'hash':
                received_hash = v
            else:
                string_to_hash += v
        string_to_hash += md5key_from_epay_settings
        calculated_hash = hashlib.md5(string_to_hash).hexdigest()
        valid = calculated_hash == received_hash
        if valid:
            title = ''
        else:
            title = 'Error'
        self.render('embedded/epay_accepted.html', valid=valid, parameters=self.request.GET.items(), tab='shop', title=title)


class EPayCancelledHandler(FrontHandler):
    def get(self):
        self.render('embedded/epay_cancelled.html', tab='shop', types='cart')

class EPaySaveCardCancelledHandler(FrontHandler):
    def get(self):
        self.render('embedded/epay_cancelled.html', tab='profile', types='tab_root')


class EmbeddedOrderHandler(FrontHandler):
    def get(self, order_key_or_id):
        self.render('embedded/'+i18n.get_i18n().locale.split('_')[0]+'/order.html', order_key_or_id=order_key_or_id, tab='orders', refresh_button=True, title='')
