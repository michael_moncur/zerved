from views.core import BaseHandler
from google.appengine.api import mail

class InfoHandler(BaseHandler):
    def get(self):
        self.render('android/info.html')
        
class TermsHandler(BaseHandler):
    def get(self):
        self.render('android/terms.html')

class PrivacyHandler(BaseHandler):
    def get(self):
        self.render('android/privacy.html')

class LicensesHandler(BaseHandler):
    def get(self):
        self.response.content_type = 'text/plain'
        self.render('android/licenses.txt')
        
class RecommendHandler(BaseHandler):
    def get(self):
        self.render('android/recommend.html')

    def post(self):
        message = mail.EmailMessage()
        message.sender = "Zerved <info@zervedapp.com>"
        message.subject = "Recommend a Venue"
        message.to = "contactform@zervedapp.com"
        message.reply_to = self.request.get("customer_email")
        message.body = self.jinja2.render_template('emails/recommend.txt', form=self.request.POST)
        message.send()
        self.render('android/recommend/sent.html')