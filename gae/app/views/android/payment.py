import webapp2
from views.core import BaseHandler
import views.iphone.payment
from models.merchants import Merchant
from models.consumers import Consumer
from models import payment
from models.payment import PaymentSubscription, EpaySubscription
from models.epaymerchantnumber import EpayMerchantNumber
from models.sales import Order
from google.appengine.ext import db
import base64
import os
import logging

class EpayPaymentHandler(BaseHandler):
    def get(self):
        # Authenticate consumer
        try:
            auth_header = self.request.headers["Authorization"]
            auth_parts = auth_header.split(' ')
            access_token = base64.b64decode(auth_parts[1])
            consumer = Consumer.authenticate(access_token)
            if not consumer:
                self.abort(401)
                return
        except Exception as e:
            self.abort(401, comment=e)
            return
            
        # Create subscription object
        subscription = self.create_subscription(consumer)
        
        # Redirect to ePay
        uriparameters = "?id1=%s&id2=%s" % (consumer.key().id(), subscription.key().id())
        epayurl = subscription.get_redirect_url(merchantnumber=str(EpayMerchantNumber.epay_merchantnumber()),
            callbackurl=webapp2.uri_for('epay-callback', _full=True)+uriparameters,
            accepturl=webapp2.uri_for('epay-accept', _full=True)+uriparameters,
            cancelurl=webapp2.uri_for('epay-cancel', _full=True),
            mobilecssurl='https://zervedapp.appspot.com/iphone/css/epay.css?v=5')
        logging.info(epayurl)
        self.redirect(epayurl)
        
    def create_subscription(self, consumer):
        subscription = EpaySubscription(parent=consumer,status='pending')
        subscription.put()
        return subscription

class UpdateHandler(BaseHandler):
    def get(self, *args):
        self.render('android/updateerror.html')
        
    def post(self, *args):
        self.get(*args)
