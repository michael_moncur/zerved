from views.core import AdminHandler
from models.catalog import Product, Menu, PriceRuleSchedule
from models.merchants import Bar
from models import catalogforms
from google.appengine.ext import db
from google.appengine.api import memcache, taskqueue
from webapp2_extras import i18n
_ = i18n.gettext
import logging

class MenuListHandler(AdminHandler):
    def get(self):
        menus = Menu.all().filter('merchant =', self.merchant).order('sort_order')
        bars = Bar.all().filter('merchant =', self.merchant).order('name')
        self.render('merchants/catalog/menus.html', menus=menus, bars=bars)
        
class MenuEditHandler(AdminHandler):
    def get(self, menu_id=None):
        menu = None
        if menu_id:
            menu = Menu.get(db.Key.from_path('Menu', int(menu_id)))
            if not self.validate_merchant_permission(menu):
                return
            form = catalogforms.get_menu_form()(self.request.POST, menu)
            if menu.schedule:
                form.set_selected_schedule(str(menu.schedule.key()))
        else:
            form = catalogforms.get_menu_form()()
        form.set_merchant(self.merchant)
        product_keys = [str(key) for key in menu.products.fetch(1000, keys_only=True)] if menu else []
        bar_keys = [str(key) for key in menu.bars] if menu else []
        allcategories = self.merchant.categories.order('sort_order').filter('group = ', 'menu')
        bars = Bar.all().filter("merchant = ", self.merchant).order('name') 
        self.render('merchants/catalog/editmenu.html', form=form, menu=menu, product_keys=product_keys, bar_keys=bar_keys, allcategories=allcategories, bars=bars)
        
    def post(self):
        id = self.request.get('_id')
        menu = None
        postdata = self.request.POST
        selected_products = postdata.getall('products')
        selected_bars = postdata.getall('bars')     
        if (id):
            menu = Menu.get(db.Key.from_path('Menu', int(id)))
            if not self.validate_merchant_permission(menu):
                return
            
        form = catalogforms.get_menu_form()(postdata)
        form.set_merchant(self.merchant)
        
        if form.validate():
            if (menu):
                form.populate_obj(menu)
                
                # Remove unselected products
                for product in menu.products:
                    if str(product.key()) not in selected_products:
                        product.remove_menu(menu.key())
                        product.put()
            else:
                menu = Menu(**form.data)
                menu.merchant = self.merchant
                
            # New schedule
            if postdata['select_schedule'] == 'new':
                schedule = PriceRuleSchedule(merchant=self.merchant)
                schedule.name = postdata['menu_schedule_name']
                schedule.set_hours_from_checkboxes(postdata.getall('menu_schedule_hours'))
                schedule.put()
                postdata['select_schedule'] = str(schedule.key())
                
            menu.schedule = db.Key(postdata['select_schedule']) if postdata['select_schedule'] else None
            menu.bars = []
            for barKey in selected_bars:
                menu.bars.append(db.Key(barKey))
            menu.put()
            
            # Add selected products
            products = db.get(selected_products)
            for product in products:
                product.add_menu(menu.key())
            db.put(products)

            # Clear product cache
            taskqueue.add(url='/tasks/clearproductcache', params={ 'merchant': str(self.merchant.key()) }, retry_options=taskqueue.TaskRetryOptions(task_retry_limit=1))
            
            link = '<a href="'+self.uri_for('menu-edit', menu_id=menu.key().id())+'">'+menu.name+'</a>'
            if id or self.request.get('continue_edit'):
                self.add_success(_('%s saved') % link)
            else:
                self.add_success((_('%s is being saved and should be available in the list and app within a few seconds') % link)+' &mdash; <a href="#" onclick="location.reload(); return false;">'+_('refresh list')+'</a>')
            if self.request.get('continue_edit'):
                self.redirect_to('menu-edit', menu_id=menu.key().id())
            else:
                if self.request.get('continue_tutorial'):
                    self.redirect_to('category-new', group='menu', tutorial='true', cont='true')
                else:    
                    self.redirect_to('menu-list')
        else:
            allcategories = self.merchant.categories.order('sort_order').filter('group = ', 'menu')
            bars = Bar.all().filter("merchant = ", self.merchant).order('name') 
            self.add_error(_('Please correct errors in your input and try save again.'))
            self.render('merchants/catalog/editmenu.html', form=form, menu=menu, product_keys=selected_products, bar_keys=selected_bars, allcategories=allcategories, bars=bars)
            
class MenuDeleteHandler(AdminHandler):
    def post(self, menu_id):
        if menu_id:
            menu = Menu.get(db.Key.from_path('Menu', int(menu_id)))
            if not self.validate_merchant_permission(menu):
                return
            self.add_success(_('%s is being deleted and should be removed from the list shortly.') % menu.name)
            menu.delete()
            # Clear product cache
            taskqueue.add(url='/tasks/clearproductcache', params={ 'merchant': str(self.merchant.key()) }, retry_options=taskqueue.TaskRetryOptions(task_retry_limit=1))
        self.redirect_to('menu-list')


class MenuSortHandler(AdminHandler):
    def get(self):
        # List all menus
        menus = Menu.all().filter('merchant =', self.merchant).order('sort_order')
        self.render('merchants/catalog/sortmenus.html', menus=menus)

    def post(self):
        menus = Menu.all().filter('merchant =', self.merchant).order('sort_order')
        # List of keys in newly sorted order
        sorted_keys = self.request.get_all('menu_id[]')
        # Menus that have changed position
        menus_to_save = []
        for menu in menus:
            # Fetch new index, update the menu if it has changed
            new_index = sorted_keys.index(str(menu.key()))
            if menu.sort_order != new_index:
                menu.sort_order = new_index
                menus_to_save.append(menu)
        # Save modified menus
        db.put(menus_to_save)
        # Return to menu list
        self.add_success(_('The new sort orders are being saved and should be reflected in the list shortly.'))
        self.redirect_to('menu-list')
