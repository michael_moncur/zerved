from views.core import AdminHandler
from models.sales import Order, OrderItem
import datetime
from views.utilities import Pager
from paging import *
from google.appengine.api import users, taskqueue
import logging


class OrdersBaseListHandler(AdminHandler):
    def get(self, merchant=None):
        status = 'all'
        if self.request.get('status') and self.request.get('status') in ['complete', 'preparing', 'pending', 'cancelled', 'all']:
            status = self.request.get('status')
        page = int(self.request.get('page')) if self.request.get('page') else 1
        query = Order.all()
        if merchant:
            query.filter('merchant =', merchant)
        if status != 'all':
            query.filter('status =', status)
        query = PagedQuery(query.order('-date_created'), 20)
        orders = query.fetch_page(page)
        pager = Pager(self, query, page, 'right')
        self.render('merchants/sales/orders.html', orders=orders, pager=pager.render(), merchant=merchant, status=status)

class OrdersListHandler(OrdersBaseListHandler):
    require_admin = True

class MerchantOrdersListHandler(OrdersBaseListHandler):
    def get(self):
        super(MerchantOrdersListHandler, self).get(merchant=self.merchant)


class OrderViewHandler(AdminHandler):
    def get(self, order_id):
        order = Order.get(db.Key.from_path('Order', int(order_id)))
        if not users.is_current_user_admin() and not self.validate_merchant_permission(order):
            return
        self.render('merchants/sales/vieworder.html', order=order)


class CancelOrderHandler(OrdersListHandler):
    csrf_protect = False
    
    def post(self):
        if 'order_key' in self.request.POST:
            # One order
            orders = [Order.get(self.request.get('order_key'))]
        elif 'order_keys[]' in self.request.POST:
            # Multiple orders
            orders = Order.get(self.request.POST.getall('order_keys[]'))
        else:
            self.abort(404)
        
        # Process orders
        for order in orders:
            if not self.validate_merchant_permission(order):
                self.abort(403)
                return
            # Queue order for status change
            taskqueue.add(queue_name='orders', url='/tasks/processorder', params={'key': str(order.key()), 'status': 'cancelled'})