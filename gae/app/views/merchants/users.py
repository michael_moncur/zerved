from views.core import BaseHandler
import webapp2
from google.appengine.api import users

class UnknownUserHandler(BaseHandler):
    def get(self):
        message = "User not found <a href=\"{0}\">Log Out</a>".format(users.create_logout_url(webapp2.uri_for('product-list', group='menu')))
        self.render('merchants/error.html', message=message)
        
class AdminRestrictedHandler(BaseHandler):
    def get(self):
        message = "The page you requested requires administrator permissions <a href=\"{0}\">Log Out</a>".format(users.create_logout_url(webapp2.uri_for('product-list', group='menu')))
        self.render('merchants/error.html', message=message)