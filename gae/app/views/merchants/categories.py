from views.core import AdminHandler
from models.merchants import Merchant
from models.catalog import Category
from models import catalogforms
from google.appengine.ext import db
from google.appengine.api import users, taskqueue
from webapp2_extras import i18n
_ = i18n.gettext

class CategoryListHandler(AdminHandler):
    def get(self, group):
        categories = Category.all().filter('merchant =', self.merchant).filter('group = ', group).order('sort_order')
        self.render('merchants/catalog/categories.html', categories=categories, group=group)
        
class CategoryEditHandler(AdminHandler):
    def get(self, group, category_id=None):
        category = None
        if category_id:
            category = Category.get(db.Key.from_path('Category', int(category_id)))
            if not self.validate_merchant_permission(category):
                return
            form = catalogforms.get_category_form()(self.request.POST, category)
        else:
            form = catalogforms.get_category_form()()
        self.render('merchants/catalog/editcategory.html', form=form, category=category, group=group)
        
    def post(self, group):
        id = self.request.get('_id')
        category = None
        form = catalogforms.get_category_form()(self.request.POST)
        
        if (id):
            category = Category.get(db.Key.from_path('Category', int(id)))
            if not self.validate_merchant_permission(category):
                return
        
        if form.validate():
            if (category):
                form.populate_obj(category)
            else:
                category = Category.construct_for_merchant(self.merchant, **form.data)
                category.group = group
            category.put()

            # Clear category/product cache
            taskqueue.add(url='/tasks/clearproductcache', params={ 'merchant': str(self.merchant.key()) }, retry_options=taskqueue.TaskRetryOptions(task_retry_limit=1))
            
            link = '<a href="'+self.uri_for('category-edit', group=group, category_id=category.key().id())+'">'+category.name+'</a>'
            if id or self.request.get('continue_edit'):
                self.add_success(_('%s saved') % link)
            else:
                self.add_success((_('%s is being saved and should be available in the list and app within a few seconds') % link)+' &mdash; <a href="#" onclick="location.reload(); return false;">'+_('refresh list')+'</a>')
            if self.request.get('continue_edit'):
                self.redirect_to('category-edit', group=group, category_id=category.key().id())
            else:
                if self.request.get('continue_tutorial'):
                    self.redirect_to('product-new', group='menu', cont='true', tutorial='true')
                else:
                    self.redirect_to('category-list', group=group)
        else:
            self.add_error(_('Please correct errors in your input and try save again.'))
            self.render('merchants/catalog/editcategory.html', form=form, category=category, group=group)
            
class CategoryDeleteHandler(AdminHandler):
    def post(self, group, category_id):
        if category_id:
            category = Category.get(db.Key.from_path('Category', int(category_id)))
            if not self.validate_merchant_permission(category):
                return
            if category.products.count(1) > 0:
                self.add_error(_('Cannot delete category because it contains products. Delete or move the products first.'))
                self.redirect_to('category-list', group=group)
                return
            self.add_success(_('%s is being deleted and should be removed from the list shortly.') % category.name)
            category.delete()
            # Clear category/product cache
            taskqueue.add(url='/tasks/clearproductcache', params={ 'merchant': str(self.merchant.key()) }, retry_options=taskqueue.TaskRetryOptions(task_retry_limit=1))
        self.redirect_to('category-list', group=group)

class CategorySortHandler(AdminHandler):
    def get(self, group):
        # List all categories
        categories = Category.all().filter('merchant =', self.merchant).filter('group = ', group).order('sort_order')
        self.render('merchants/catalog/sortcategories.html', categories=categories, group=group)

    def post(self, group):
        categories = Category.all().filter('merchant =', self.merchant).filter('group = ', group).order('sort_order')
        # List of keys in newly sorted order
        sorted_keys = self.request.get_all('category_id[]')
        # Categories that have changed position
        categories_to_save = []
        for category in categories:
            # Fetch new index, update the category if it has changed
            new_index = sorted_keys.index(str(category.key()))
            if category.sort_order != new_index:
                category.sort_order = new_index
                categories_to_save.append(category)
        # Save modified categories
        db.put(categories_to_save)
        # Clear category/product cache
        taskqueue.add(url='/tasks/clearproductcache', params={ 'merchant': str(self.merchant.key()) }, retry_options=taskqueue.TaskRetryOptions(task_retry_limit=1))
        # Return to category list
        self.add_success(_('The new sort orders are being saved and should be reflected in the list shortly.'))
        self.redirect_to('category-list', group=group)
