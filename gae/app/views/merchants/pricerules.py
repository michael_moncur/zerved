from views.core import AdminHandler
from models.catalog import Product, PriceRule, PriceRuleSchedule
from models import catalogforms
from google.appengine.ext import db
from google.appengine.api import memcache
from webapp2_extras import i18n
_ = i18n.gettext

class PriceRuleListHandler(AdminHandler):
    def get(self):
        pricerules = PriceRule.all().filter('merchant =', self.merchant)
        self.render('merchants/catalog/pricerules.html', pricerules=pricerules)
        
class PriceRuleEditHandler(AdminHandler):
    def get(self, pricerule_id=None):
        pricerule = None
        if pricerule_id:
            pricerule = PriceRule.get(db.Key.from_path('PriceRule', int(pricerule_id)))
            if not self.validate_merchant_permission(pricerule):
                return
            form = catalogforms.get_price_rule_form()(self.request.POST, pricerule)
            if pricerule.schedule:
                form.set_selected_schedule(str(pricerule.schedule.key()))
        else:
            form = catalogforms.get_price_rule_form()()
        form.set_merchant(self.merchant)
        product_keys = [str(key) for key in pricerule.products.fetch(1000, keys_only=True)] if pricerule else []
        allcategories = self.merchant.categories.order('sort_order')
        self.render('merchants/catalog/editpricerule.html', form=form, pricerule=pricerule, product_keys=product_keys, allcategories=allcategories)
        
    def post(self):
        id = self.request.get('_id')
        pricerule = None
        postdata = self.request.POST
        selected_products = postdata.getall('products')
        
        if (id):
            pricerule = PriceRule.get(db.Key.from_path('PriceRule', int(id)))
            if not self.validate_merchant_permission(pricerule):
                return
            
        form = catalogforms.get_price_rule_form()(postdata)
        form.set_merchant(self.merchant)
        
        if form.validate():
            if (pricerule):
                form.populate_obj(pricerule)
                
                # Remove unselected products
                for product in pricerule.products:
                    if str(product.key()) not in selected_products:
                        product.remove_price_rule(pricerule.key())
                        product.put()
            else:
                pricerule = PriceRule(**form.data)
                pricerule.merchant = self.merchant
                
            # New schedule
            if postdata['select_schedule'] == 'new':
                schedule = PriceRuleSchedule(merchant=self.merchant)
                schedule.name = postdata['schedule_name']
                schedule.set_hours_from_checkboxes(postdata.getall('schedule_hours'))
                schedule.put()
                postdata['select_schedule'] = str(schedule.key())
                
            pricerule.schedule = db.Key(postdata['select_schedule']) if postdata['select_schedule'] else None
            pricerule.put()
            
            # Add selected products
            products = db.get(selected_products)
            for product in products:
                product.add_price_rule(pricerule.key())
            db.put(products)
            
            memcache.delete("pricerules_"+str(self.merchant.key().id()))
            
            link = '<a href="'+self.uri_for('pricerule-edit', pricerule_id=pricerule.key().id())+'">'+pricerule.name+'</a>'
            if id or self.request.get('continue_edit'):
                self.add_success(_('%s saved') % link)
            else:
                self.add_success((_('%s is being saved and should be available in the list and app within a few seconds') % link)+' &mdash; <a href="#" onclick="location.reload(); return false;">'+_('refresh list')+'</a>')
            if self.request.get('continue_edit'):
                self.redirect_to('pricerule-edit', pricerule_id=pricerule.key().id())
            else:
                self.redirect_to('pricerule-list')
        else:
            allcategories = self.merchant.categories.order('sort_order')
            self.add_error(_('Please correct errors in your input and try save again.'))
            self.render('merchants/catalog/editpricerule.html', form=form, pricerule=pricerule, product_keys=selected_products, allcategories=allcategories)
            
class PriceRuleDeleteHandler(AdminHandler):
    def post(self, pricerule_id):
        if pricerule_id:
            pricerule = PriceRule.get(db.Key.from_path('PriceRule', int(pricerule_id)))
            if not self.validate_merchant_permission(pricerule):
                return
            self.add_success(_('%s is being deleted and should be removed from the list shortly.') % pricerule.name)
            pricerule.delete()
            memcache.delete("pricerules_"+str(self.merchant.key().id()))
        self.redirect_to('pricerule-list')
