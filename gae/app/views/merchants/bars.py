from views.core import AdminHandler
from models import merchantforms
from models.merchants import Bar, Location
from models.catalog import Menu, PriceRuleSchedule
from google.appengine.ext import db
from google.appengine.api import taskqueue
from webapp2_extras import i18n
from decimal import Decimal
_ = i18n.gettext
import logging

class BarListHandler(AdminHandler):
    def get(self):
        bars = self.merchant.bars
        self.render('merchants/merchants/bars.html', bars=bars)
        
class BarNewHandler(AdminHandler):
    def get(self, location_id):
        location = Location.get(db.Key.from_path('Location', int(location_id)))
        form = merchantforms.get_bar_form()()
        form.set_merchant(self.merchant)
        form.delivery_min_order_amount.data = Decimal('0.00')
        form.delivery_fee.data = Decimal('0.00')
        allmenus = Menu.all().filter('merchant = ', self.merchant)
        self.render('merchants/merchants/editbar.html', form=form, bar=None, default_notifications=Bar.get_default_notifications(), location=location, menu_keys=[], allmenus=allmenus)
        
class BarEditHandler(AdminHandler):
    def get(self, bar_id):
        bar = Bar.get(db.Key.from_path('Bar', int(bar_id)))
        if not self.validate_merchant_permission(bar):
            return
        form = merchantforms.get_bar_form()(self.request.POST, bar)
        form.set_merchant(self.merchant)
        if bar.delivery_fee_tax_class:
            form.set_selected_delivery_fee_tax_class(str(bar.delivery_fee_tax_class.key()))
        allmenus = Menu.all().filter('merchant = ', self.merchant)
        menu_keys = [str(key.key()) for key in bar.get_menus().fetch(1000)]
        self.render('merchants/merchants/editbar.html', form=form, bar=bar, default_notifications=Bar.get_default_notifications(), location=bar.location, menu_keys=menu_keys, allmenus=allmenus)

    def post(self):
        id = self.request.get('_id')
        bar = None
        postdata = self.request.POST
        form = merchantforms.get_bar_form()(postdata)
        form.set_merchant(self.merchant)
        selected_menus = self.request.POST.getall('menus')

        if (id):
            bar = Bar.get(db.Key.from_path('Bar', int(id)))
            if not self.validate_merchant_permission(bar):
                return
            location = bar.location
        else:
            location = Location.get(self.request.get('location_key'))

        if form.validate():
            if (bar):
                form.populate_obj(bar)
                # Remove unselected products
                for menu in bar.get_menus():
                    if str(menu.key()) not in selected_menus:
                        menu.remove_bar(bar.key())
                        menu.put()                
            else:
                bar = Bar(**form.data)
                bar.merchant = self.merchant
                bar.location = location

            # Update bar delivery fee tax class
            bar.delivery_fee_tax_class = db.Key(postdata['select_delivery_fee_tax_class']) if postdata['select_delivery_fee_tax_class'] else None

            bar.put()
            
            # Save selected menus
            menus = db.get(selected_menus)
            for menu in menus:
                menu.add_bar(bar.key())
            db.put(menus)

            # Creates a new menu card valid at any time if the merchant doesn't have one 
            menus = Menu.all().filter('merchant = ', self.merchant)
            if len(menus.fetch(1)) == 0:
                always = PriceRuleSchedule(merchant=self.merchant,name=str(_("Always")), hours0=range(24), hours1=range(24), hours2=range(24), hours3=range(24), hours4=range(24), hours5=range(24), hours6=range(24)).put()
                Menu(merchant=self.merchant, name=str(_("All day menu")), schedule=always, bars=[bar.key()]).put()

            # Clear product cache
            taskqueue.add(url='/tasks/clearproductcache', params={ 'merchant': str(self.merchant.key()) }, retry_options=taskqueue.TaskRetryOptions(task_retry_limit=1))

            link = '<a href="'+self.uri_for('bar-edit', bar_id=bar.key().id())+'">'+bar.name+'</a>'
            if id or self.request.get('continue_edit'):
                self.add_success(_('%s saved') % link)
            else:
                self.add_success((_('%s is being saved and should be available in the list and app within a few seconds') % link)+' &mdash; <a href="#" onclick="location.reload(); return false;">'+_('refresh list')+'</a>')
            if self.request.get('continue_edit'):
                self.redirect_to('bar-edit', bar_id=bar.key().id())
            else:
                self.redirect_to('location-list')
        else:
            allmenus = Menu.all().filter('merchant = ', self.merchant)
            self.add_error(_('Please correct errors in your input and try save again.'))
            self.render('merchants/merchants/editbar.html', form=form, bar=bar, default_notifications=Bar.get_default_notifications(), location=location, menu_keys=selected_menus, allmenus=allmenus)

class BarDeleteHandler(AdminHandler):
    def post(self, bar_id):
        if bar_id:
            bar = Bar.get(db.Key.from_path('Bar', int(bar_id)))
            if not self.validate_merchant_permission(bar):
                return
            self.add_success(_('%s is being deleted and should be removed from the list shortly.') % bar.name)
            bar.delete()
        self.redirect_to('location-list')