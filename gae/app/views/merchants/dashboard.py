from google.appengine.ext import db
from views.core import AdminHandler
from webapp2_extras import i18n
from models.front.news import News
from models.merchants import Location, Bar
from models.catalog import Category, Product, Menu
from views.utilities import Pager
from paging import *
import datetime
_ = i18n.lazy_gettext
gettext = i18n.gettext
from google.appengine.api import memcache
import logging

class DashboardHandler(AdminHandler):
	def get(self):
		# Merchant News
		page = int(self.request.get('page')) if self.request.get('page') else 1
		query = News.all().filter("type = ", "merchantnews").filter('published = ', True)
		query = PagedQuery(query.order('-date'), 5)
		news = query.fetch_page(page)
		pager = Pager(self, query, page)

		# Achievements
		achievements = {}
		locations = Location.all().filter("merchant =", self.merchant).fetch(1)
		achievements.setdefault('locations', (len(locations) > 0))
		bars = Bar.all().filter("merchant =", self.merchant).fetch(1)
		achievements.setdefault('bars', (len(bars) > 0))
		menus = Menu.all().filter("merchant = ", self.merchant).fetch(1)
		achievements.setdefault('menus', (len(menus) > 0))
		categories = Category.all().filter("merchant = ", self.merchant).fetch(1)
		achievements.setdefault('categories', (len(categories) > 0))
		products = Product.all().filter("merchant = ", self.merchant).fetch(1)
		achievements.setdefault('products', (len(products) > 0))

		self.render('merchants/dashboard/dashboard.html', merchant=self.merchant, news=news, pager=pager.render(), achievements=achievements, now=datetime.datetime.now())


# Set news as read for the current user
class NewsReadHandler(AdminHandler):
	def post(self):
		news_key = db.Key(self.request.POST['news_key']) if 'news_key' in self.request.POST else None
		user_prefs = self.get_user_prefs()
		if news_key not in user_prefs.news_read:
			cachekey = "user_prefs" + self.get_user_id()
			memcache.delete(cachekey)
			user_prefs.news_read.append(news_key)
			user_prefs.put()
