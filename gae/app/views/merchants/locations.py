from views.core import AdminHandler
from models import merchantforms
from models.merchants import Location, Bar
from models.catalog import Menu, PriceRuleSchedule
from google.appengine.ext import db
from google.appengine.api import memcache
from webapp2_extras import i18n
_ = i18n.gettext

import os, cgi
import urllib
import webapp2
import json
from google.appengine.ext import blobstore
from google.appengine.ext.webapp import blobstore_handlers

class LocationListHandler(AdminHandler, blobstore_handlers.BlobstoreUploadHandler):
    def get(self):
        locations = self.merchant.locations
        menus = Menu.all().filter('merchant = ', self.merchant).order('name')
        self.render('merchants/merchants/locations.html', locations=locations, menus=menus, form_action = blobstore.create_upload_url('/merchants/locations'))

    def post(self):
        if 'location_id' in self.request.POST:
            location = Location.get(db.Key.from_path('Location', int(self.request.POST['location_id'])))
        else:
            self.abort(404)

        file_fields = self.get_uploads('file')
        new_images = {}
        for field in file_fields:
            blobkey = field.key()
            # Save image blob key
            location.images.append(blobkey)
            # Generate and cache URL for image
            new_images[location.get_cover_url_cache_key(blobkey)] = location.get_cover_url(blobkey)
        location.put()
        memcache.set_multi(new_images)
        
        # Generates a new upload url
        self.response.headers['Content-Type'] = 'application/json'
        self.response.out.write(json.dumps(blobstore.create_upload_url('/merchants/locations')))
        
class LocationEditHandler(AdminHandler):
    def get(self, location_id=None):
        location = None
        if location_id:
            location = Location.get(db.Key.from_path('Location', int(location_id)))
            if not self.validate_merchant_permission(location):
                return
            form = merchantforms.get_location_form()(self.request.POST, location)
        else:
            form = merchantforms.get_location_form()()
        barform = merchantforms.get_simple_bar_form()(prefix='bar_')
        product_keys = [str(key) for key in location.products.fetch(1000, keys_only=True)] if location else []
        allcategories = self.merchant.categories.filter('group =', 'entrance').order('sort_order')

        self.render('merchants/merchants/editlocation.html', form=form, location=location, barform=barform, product_keys=product_keys, allcategories=allcategories)

    def post(self, location_id=None):
        id = self.request.get('_id')
        location = None
        form = merchantforms.get_location_form()(self.request.POST)
        selected_products = self.request.POST.getall('products')
        barform = merchantforms.get_simple_bar_form()(self.request.POST, prefix='bar_')
        barform_valid = id or barform.validate()

        if (id):
            location = Location.get(db.Key.from_path('Location', int(id)))
            if not self.validate_merchant_permission(location):
                return

        info_panel_valid = form.validate_info_panel()
        if not info_panel_valid:
            self.add_error(_('Address and opening hours are required for the information panel.'))

        if form.validate() and barform_valid and info_panel_valid:
            if (location):
                form.populate_obj(location)
                
                # Remove unselected products
                for product in location.products:
                    if str(product.key()) not in selected_products:
                        product.remove_location(location.key())
                        product.put()
            else:
                location = Location(**form.data)
                location.merchant = self.merchant
            location.update_location()
            location.put()
            
            # Add selected products
            products = db.get(selected_products)
            for product in products:
                product.add_location(location.key())
            db.put(products)
            
            if not id:
                bar = Bar(location=location, merchant=self.merchant, **barform.data)
                bar.put()
                # Creates a new menu card valid at any time if the merchant doesn't have one 
                menus = Menu.all().filter('merchant = ', self.merchant)
                if len(menus.fetch(1)) == 0:
                    always = PriceRuleSchedule(merchant=self.merchant,name=str(_("Always")), hours0=range(24), hours1=range(24), hours2=range(24), hours3=range(24), hours4=range(24), hours5=range(24), hours6=range(24)).put()
                    Menu(merchant=self.merchant, name=str(_("All day menu")), schedule=always, bars=[bar.key()]).put()

            link = '<a href="'+self.uri_for('location-edit', location_id=location.key().id())+'">'+location.name+'</a>'
            if id or self.request.get('continue_edit'):
                self.add_success(_('%s saved') % link)
            else:
                self.add_success((_('%s is being saved and should be available in the list and app within a few seconds') % link)+' &mdash; <a href="#" onclick="location.reload(); return false;">'+_('refresh list')+'</a>')
            if self.request.get('continue_edit'):
                self.redirect_to('location-edit', location_id=location.key().id())
            else:
                self.redirect_to('location-list')
        else:
            allcategories = self.merchant.categories.filter('group =', 'entrance').order('sort_order')
            self.add_error(_('Please correct errors in your input and try save again.'))
            self.render('merchants/merchants/editlocation.html', form=form, location=location, barform=barform, product_keys=selected_products, allcategories=allcategories)

class LocationDeleteHandler(AdminHandler):
    def post(self, location_id):
        if location_id:
            location = Location.get(db.Key.from_path('Location', int(location_id)))
            if not self.validate_merchant_permission(location):
                return
            self.add_success(_('%s is being deleted and should be removed from the list shortly.') % location.name)
            location.delete()
        self.redirect_to('location-list')

class LocationCoverServeHandler(blobstore_handlers.BlobstoreDownloadHandler):
    def get(self, blob_key):
        resource = str(urllib.unquote(blob_key))
        blob_info = blobstore.BlobInfo.get(resource)
        self.send_blob(blob_info)

class LocationCoverDeleteHandler(AdminHandler):
    def get(self, location_id, blob_key):
        if location_id and blob_key:
            location = Location.get(db.Key.from_path('Location', int(location_id)))
            resource = str(urllib.unquote(blob_key))
            # Remove reference from location
            location.images.remove(resource)
            memcache.delete(location.get_cover_url_cache_key(resource))
            blob_info = blobstore.BlobInfo.get(resource)
            location.put()
            # Remove from blobstore
            if blob_info:
                blob_info.delete()
            else:
                self.abort(404)
            self.redirect_to('location-list')
        else:
            self.abort(404)

class LocationCoverGetImagesHandler(AdminHandler):
    def get(self, location_id):
        if location_id:
            location = Location.get(db.Key.from_path('Location', int(location_id)))
            self.response.headers['Content-Type'] = 'application/json'
            self.response.out.write(json.dumps([str(i) for i in location.images]))
        else:
            self.abort(404)

class LocationCoverSortHandler(AdminHandler):
    def post(self, location_id):
        if location_id:
            location = Location.get(db.Key.from_path('Location', int(location_id)))
            if 'keys[]' in self.request.POST:
                location.images = []
                for k in self.request.POST.getall('keys[]'):
                    location.images.append(blobstore.BlobKey(k));
                location.put()
            else:
                self.abort(404)
        else:
            self.abort(404)