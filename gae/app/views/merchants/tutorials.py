from views.core import AdminHandler

class TutorialHandler(AdminHandler):
	def get(self, page):
		self.response.headers['Content-Type'] = 'application/json'
		self.render('merchants/tutorials/' + page + '.json')

class TutorialEndHandler(AdminHandler):
	def get(self):
		self.render('merchants/tutorials/endpoint.html')