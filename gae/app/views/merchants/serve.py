from views.core import BaseHandler, AdminHandler
from models.catalog import Menu
from models.merchants import Merchant, OrderProcessingSession, Bar
from models.events import Event
from models.sales import Order, OrderItem
from google.appengine.ext import db
from google.appengine.api import users, taskqueue, memcache
from models.firezerved import create_custom_token, _send_firebase_message
import webapp2
from webapp2_extras import i18n
_ = i18n.lazy_gettext
import json
import datetime, time
import uuid
import logging

class ServeBarsHandler(BaseHandler):
    def get(self):
        # Bar selected
        if self.request.get('bar'):
            bar = Bar.get(self.request.get('bar'))
            if not bar:
                self.abort(404)
            if users.get_current_user().email().lower() not in bar.merchant.users and not users.is_current_user_admin():
                self.abort(403)
            self.set_cookie('m', str(bar.merchant.key()), expires=(datetime.datetime.now() + datetime.timedelta(days=7)))
            self.redirect_to('serve-ipad', bar_id=str(bar.key().id()))
            return

        # Select bar
        bars=[]
        if users.is_current_user_admin():
            # Admin
            if self.request.get('merchant'):
                # Merchant selected, show bars
                merchant = Merchant.get(self.request.get('merchant'))
                if not merchant:
                    self.abort(404)
                for loc in merchant.get_locations():
                    bars.extend(loc.get_bars())
            else:
                # No merchant selected, show all merchants
                self.render('merchants/serve/servemerchants.html', merchants=Merchant.all(), logout_link=users.create_logout_url(webapp2.uri_for('serve-bars')))
                return
        else:
            # User, load all bars for all user merchants
            merchants = Merchant.all().filter('users =', users.get_current_user().email().lower())
            for m in merchants:
                for loc in m.get_locations():
                    bars.extend(loc.get_bars())
        self.render('merchants/serve/servemerchantbars.html', bars=bars, is_admin=users.is_current_user_admin(), logout_link=users.create_logout_url(webapp2.uri_for('serve-bars')))
        
class ServeHandler(AdminHandler):
    def get(self, bar_id, device='web'):
        bar = Bar.get(db.Key.from_path('Bar', int(bar_id)))
        client_id = uuid.uuid4().hex
        try:
            locale = i18n.get_i18n().locale
        except AssertionError: # If request not set (when run from unit test script)
            locale = 'en'

        merchant_key = Bar.merchant.get_value_for_datastore(bar)
        events = Event.all().\
                filter('merchant =', merchant_key).\
                filter('enabled_bars =', bar).\
                filter('date_delivery >=', datetime.datetime.now() - datetime.timedelta(days=7)).\
                order('date_delivery')

        session = OrderProcessingSession.get_or_insert(client_id, bar=bar, connected=True, locale=locale)
        session.put()
        channel_id = client_id
        client_auth_token = create_custom_token(channel_id)

        self.render(
                'merchants/serve/serve.html',
                client_id=client_id,
                bar=bar,
                device=device,
                events = events,
                client_auth_token = client_auth_token,
                channel_id = channel_id
                )

class ServeIpadAppHandler(ServeHandler):
    def get(self, bar_id):
        super(ServeIpadAppHandler, self).get(bar_id, device='ipad')
        
class StartServeHandler(AdminHandler):
    def get(self, bar_id):
        bar = Bar.get(db.Key.from_path('Bar', int(bar_id)))
        if not self.validate_merchant_permission(bar) and not users.is_current_user_admin():
            self.abort(403)
        bar.status = 'open'
        bar.put()
        self.redirect_to('serve', bar_id=bar_id)
        
class EndServeHandler(AdminHandler):
    def get(self):
        self.redirect_to('product-list', group="menu")
       

class PullHandler(AdminHandler):
    csrf_protect = False
    
    def get(self, bar_id, event_key):
        """ Get all orders for bar """

        # Get bar from cache
        bar = memcache.get("bar{barid}".format(barid=bar_id) )
        if bar is None:
            bar = Bar.get(db.Key.from_path('Bar', int(bar_id)))
            memcache.add("bar{barid}".format(barid=bar_id), bar, 60)

        # find active event
        active_event = None
        try:
            active_event = Event.get(event_key) 
        except db.BadKeyError:
            pass

        complete_orders = None
        unfinished_orders = None

        if active_event is None:
            # Get unfinished live orders
            unfinished_orders = bar.orders.\
                    filter('event_name = ', "").\
                    filter('status IN', ['pending', 'preparing']).\
                    filter('service IN', ['counter', 'table', 'delivery']).\
                    order('date_created').run(batch_size=50)
           
            # Get completed live orders
            complete_orders = bar.orders.\
                    filter('event_name = ', "").\
                    filter('status =', 'complete').\
                    filter('service IN', ['counter', 'table', 'delivery']).\
                    order('-date_completed').fetch(limit=10)
        else:
            # unfinished orders only get orders for specifiv event
            unfinished_orders = bar.orders.\
                    filter('event_name = ', active_event.name).\
                    filter('date_delivery =', active_event.date_delivery).\
                    filter('event_time_label = ', active_event.time_label).\
                    filter('status IN', ['pending', 'preparing']).\
                    filter('service IN', ['counter', 'table', 'delivery']).\
                    order('date_created').run(batch_size=1000)

            # completed orders only get orders for specifiv event
            complete_orders = bar.orders.\
                    filter('event_name = ', active_event.name).\
                    filter('date_delivery =', active_event.date_delivery).\
                    filter('event_time_label = ', active_event.time_label).\
                    filter('status =', 'complete').\
                    filter('service IN', ['counter', 'table', 'delivery']).\
                    order('-date_completed').fetch(limit=10)
        #
        # Append orders to json order list
        pending_orders_count = 0
        preparing_orders_count = 0
        if unfinished_orders is not None:
            orders_json = [] 
            if unfinished_orders is not None:
                for order in unfinished_orders:
                    if order.status == 'pending':
                        pending_orders_count += 1
                    elif order.status == 'preparing':
                        preparing_orders_count += 1
    
                    # save the order for the response
                    orders_json.append(order.to_json())

                    # clear order to save memory
                    del order


        completed_orders_count = 0
        if complete_orders is not None:
            for order in complete_orders:
                completed_orders_count += 1
                orders_json.append(order.to_json())
                
                # clear order to save memory
                del order

        data = {'refresh': orders_json, 
                'pending_orders_count': pending_orders_count,
                'preparing_orders_count': preparing_orders_count,
                'completed_orders_count': completed_orders_count,
                'status': bar.status,
                'message_when_paused': bar.message_when_paused,
                'estimated_waiting_time': bar.estimated_waiting_time,
                'estimated_waiting_time_delivery':\
                        bar.estimated_waiting_time_delivery,
                'timestamp': time.time() }

        self.response.write(json.dumps(data))

class PullChangesHandler(AdminHandler):
    def get(self, bar_id, since):
        bar = Bar.get(db.Key.from_path('Bar', int(bar_id)))
        timestamp_now = time.time()
        datetime_now = datetime.datetime.fromtimestamp(timestamp_now)
        since_datetime = datetime.datetime.fromtimestamp(float(since))
        data = { 'remove': [], 'insert': [], 'datetime': i18n.format_datetime(datetime_now), 'timestamp': timestamp_now }
        # Fetch modified orders since last update
        orders = bar.orders.filter('date_modified > ', since_datetime)
        yesterday = datetime.datetime.now() - datetime.timedelta(seconds=60*60*24)
        for order in orders:
            logging.info('Order '+str(order.key().id())+' changed')
            # Remove from screen
            data['remove'].append(str(order.key()))
            # Insert new order data on screen, if relevant
            if order.status == 'pending' or order.status == 'preparing' or (order.status == 'complete' and order.date_completed > yesterday):
                data['insert'].append(order.to_json())
        self.response.write(json.dumps(data))


def chunks(l, n):
    """
    Sublist iterator
     
    Yields successive n-sized chunks from l.
 
    Params:
        l : input list
        n : number of elements in sublist
 
    Returns:
        sublist iterator
     
    """
 
    for i in xrange(0, len(l), n):
        yield l[i:i+n]


class ProcessOrderHandler(AdminHandler):
    csrf_protect = False
    
    def post(self):
        """ Change status of one or more orders """
        if 'order_key' in self.request.POST:
            # One order
            order_keys = [db.Key(self.request.get('order_key'))]
        elif 'order_keys[]' in self.request.POST:
            # Multiple orders
            order_keys = [db.Key(encoded_key) for encoded_key in self.request.POST.getall('order_keys[]')]
        else:
            self.abort(404)
        status = str(self.request.get('status'))
        client_id = self.request.get('client_id')

        # If orders are normally completed from consumer device, require PIN to complete it from here
        if status == 'complete' and self.merchant.serve_with_consumer_device:
            if self.request.get('pin') != self.merchant.entrance_pincode:
                self.abort(403)
                return

        # Prepare an invoice so it's ready when orders are completed (bug #392)
        invoice_key = ''
        if status == 'complete':
            invoice = self.merchant.get_open_invoice()
            invoice_key = str(invoice.key())

        # Process orders
        for order_chunk in chunks(order_keys, 20):
            # Queue order for status change
            taskqueue.add(
                    queue_name='orders',
                    url='/tasks/process_multi_orders',
                    params={
                        'order_keys': json.dumps([str(order_key) \
                                for order_key in order_chunk]),
                        'status': status,
                        'merchant_key': str(self.merchant.key()),
                        'invoice_key': invoice_key}
                    )

class ConnectedHandler(BaseHandler):
    csrf_protect = False
    
    def post(self):
        client_id = self.request.get('from')
        session = OrderProcessingSession.get_by_key_name(client_id)
        if session:
            session.connected = True
            session.put()
        
class DisconnectedHandler(BaseHandler):
    csrf_protect = False

    def post(self):
        client_id = self.request.get('from')
        session = OrderProcessingSession.get_by_key_name(client_id)
        if session:
            # Close this session
            session.connected = False
            session.put()
            # Auto close bar (but wait 10 sec because this might just be a refresh)
            bar_key = OrderProcessingSession.bar.get_value_for_datastore(session)
            taskqueue.add(url='/tasks/closebar', params={'bar_key': str(bar_key) }, countdown=10, retry_options=taskqueue.TaskRetryOptions(task_retry_limit=1))
        
            
class PingHandler(BaseHandler):
    csrf_protect = False
    
    def post(self):
        client_id = self.request.get('client_id')
        random_value = uuid.uuid4().hex
        _send_firebase_message(client_id, message=json.dumps({ 'ping': random_value }))

class BarSettingsHandler(AdminHandler):
    csrf_protect = False

    def post(self, bar_id):
        """ Update settings for bar (waiting time, bar status etc.) """
        bar = Bar.get(db.Key.from_path('Bar', int(bar_id)))
        data = {}
        if not self.validate_merchant_permission(bar):
            self.abort(403)
        if self.request.get('status'):
            bar.status = self.request.get('status')
            data['status'] = bar.status
            #bar.clear_open_cache()
        if self.request.get('persist_status'):
            persist_status = self.request.get('persist_status') == 'true'
            if persist_status:
                menus = Menu.all().filter('bars =', bar)
                for menu in menus:
                    # The menu schedule "Always" must not be use for persistently open bars. This is to force merchants
                    # to actively make the decision on how long a bar should be able to be open without an active
                    # session, by creating their own schedule.
                    # Unfortunately, the only parameter to compare with is 'name', which is not unique. We just have
                    # to hope merchants don't use the same name for their own schedule, or it too will be invalid.
                    if menu.schedule is None or menu.schedule.name == 'Always':
                        self.response.write('INVALID_SCHEDULE')
                        return
            bar.persist_status = persist_status
            logging.info('Server bar.persist_status set to ' + str(bar.persist_status))
            data['persist_status'] = bar.persist_status
        if 'message_when_paused' in self.request.POST:
            bar.message_when_paused = self.request.get('message_when_paused')
            data['message_when_paused'] = bar.message_when_paused
        if self.request.get('estimated_waiting_time'):
            bar.estimated_waiting_time = int(self.request.get('estimated_waiting_time'))
            data['estimated_waiting_time'] = bar.estimated_waiting_time
        if self.request.get('estimated_waiting_time_delivery'):
            bar.estimated_waiting_time_delivery = int(self.request.get('estimated_waiting_time_delivery'))
            data['estimated_waiting_time_delivery'] = bar.estimated_waiting_time_delivery
        bar.put()
        for session in bar.order_processing_sessions:
            session.send_message(json.dumps(data))
