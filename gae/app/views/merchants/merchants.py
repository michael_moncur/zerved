# coding: utf-8
from views.core import BaseHandler, AdminHandler
from views.utilities import Pager
from paging import *
from models.merchants import Merchant, TaxClass, LoyaltyCode
from models.catalog import Product, PriceRuleSchedule, PriceRule, Menu, PriceTier
from models import i18ndata
from google.appengine.ext import db
from google.appengine.api import users, taskqueue, search
import wtforms, datetime, webapp2, logging, re, json, csv
from wtforms import Form, TextField, SelectField, BooleanField, RadioField, FileField, validators
from wtforms.ext.appengine.db import model_form
from views.utilities import RadioListWidget
from models.decimalproperty import *
from webapp2_extras import i18n
_ = i18n.lazy_gettext
gettext = i18n.gettext

class BooleanSelectField(SelectField):
    def _value(self):
        if self.data is not None:
            return 'y' if self.data else ''
        else:
            return u''
            
    def process_formdata(self, valuelist):
        self.data = bool(valuelist[0])
            
    def pre_validate(self, form):
        pass
        
class BooleanRadioField(BooleanSelectField):
    widget = RadioListWidget()
    option_widget = wtforms.widgets.RadioInput()
            
class InitHandler(BaseHandler):
    def get(self):
        merchant = Merchant(name='Test',location=db.GeoPt(30,51))
        merchant.put()
        self.response.write('ok')

class MerchantListHandler(AdminHandler):
    require_admin = True
    
    def get(self):
        search_query = self.request.get('search')        
        if search_query == '':
            page = int(self.request.get('page')) if self.request.get('page') else 1
            query = Merchant.all()
            query = PagedQuery(query.order('name'), 20)
            merchants = query.fetch_page(page)
            pager = Pager(self, query, page)
            self.render('merchants/merchants/merchants.html', merchants=merchants, pager=pager.render())
        else:
            # Search for merchants
            index = search.Index(name="merchants")
            results = index.search(search_query)
            _json = {}
            merchants = []  
            for result in results:                
                m = {}
                for field in result.fields:
                    m.setdefault(field.name, field.value)
                merchants.append(m)
                _json.setdefault("merchants", merchants)

            # Search for locations
            index = search.Index(name="locations")
            results = index.search(search_query)
            locations = []  
            for result in results:                
                location = {}
                for field in result.fields:
                    if isinstance(field, search.TextField): #Location index contains geo data, skip those
                        location.setdefault(field.name, field.value)
                locations.append(location)
                _json.setdefault("locations", locations)

            # Search for bars
            index = search.Index(name="bars")
            results = index.search(search_query)
            bars = []  
            for result in results:                
                bar = {}
                for field in result.fields:
                    bar.setdefault(field.name, field.value)
                bars.append(bar)
                _json.setdefault("bars", bars)

            self.response.headers['Content-Type'] = 'application/json'   
            self.response.out.write(json.dumps(_json))
        
    def post(self):
        merchant_key = self.request.get('merchant_key')
        self.set_cookie('m', str(merchant_key), expires=(datetime.datetime.now() + datetime.timedelta(days=7)))
        if self.request.get('redirect_url'):
            self.redirect(self.request.get('redirect_url'))
        else:
            self.redirect_to('merchant-list')

class MerchantEditHandler(AdminHandler):
    #require_admin = True
    
    def get_form(self):
        MerchantModelForm = model_form(Merchant, Form, exclude=['location', 'enabled'], field_args={
            'name': { 'label': _('Name') },
            'prices_include_tax': { 'label': _('Prices Include Tax') },
            'users': { 'label': _('Users') },
            'currency': { 'label': _('Currency') },
            'address': { 'label': _('Address') },
            'email': { 'label': _('Email Address') },
            'phonenr': { 'label': _('Phone Number') },
            'vat_number': { 'label': _('VAT Number') },
            'bank_account': { 'label': _('Bank Account') },
            'timezone': { 'label': _('Time Zone') },
            'entrance_pincode': { 'label': _('Staff PIN') },
            #'enabled': { 'label': _('Enabled') },
            'country': { 'label': _('Country') },
            'auto_prepare': { 'label': _('Auto Prepare Menu Orders') },
            'receive_news': { 'label': _('Receive Updates') },
            'has_loyalty_program': { 'label': _('Activate Loyalty Program') },
            'loyalty_program_name': { 'label': _('Specific Loyalty Program Name') },
            'loyalty_program_rate': { 'label': _('Loyalty Program Discount Rate') },
            'custom_allowed_payment_methods_enabled': { 'label': _('Activate Custom Payment Methods') },
            'custom_allowed_payment_method_epay': { 'label': _('Credit Cards (epay)') },
            'custom_allowed_payment_method_swipp': { 'label': _('Swipp') },
            'custom_allowed_payment_method_epaymobilepay': { 'label': _('MobilePay through epay') },
            'custom_allowed_payment_method_mobilepay_appswitch': { 'label': _('MobilePay AppSwitch (Native)') },
            'custom_allowed_payment_method_mobilepay_appswitch_embedded': { 'label': _('MobilePay AppSwitch (Native, through Zerved embedded client)') },
        })
        class MerchantForm(MerchantModelForm):
            #fee_rate = LocalizedDecimalField(places=2, label=_('Rate'), default=0)
            fee_rate_use_default = BooleanSelectField(label=_('Charge'), choices=[('y', _("Zerved's normal rate")), ('', _('Negotiated rate'))], coerce=bool, default=True)
            serve_with_consumer_device = BooleanRadioField(label=_("Serve menu orders using"), choices=[('', _("The service screen")), ('y', _("Staff PIN on the customer's phone"))], coerce=bool, default=False)
            service_charge = LocalizedDecimalField(places=2, label=_('Service Charge'), default=0)
            loyalty_program_rate = LocalizedDecimalField(places=2, label=_('Loyalty Program Rate'), default=0, validators=[validators.NumberRange(min=0, max=100, message=_('Loyalty discount cannot be higher then 100'))])
            image = FileField(label=_('Image'), validators=[validators.length(max=1000000, message=_('The image file is too large, must be maximum 1 MB.'))])
            loyalty_csv = FileField(label=_('CSV'), validators=[validators.length(max=1000000, message=_('The CSV file is too large, must be maximum 1 MB.'))])
            @property
            def default_fee_rate(self):
                return Merchant.DEFAULT_FEE_RATE
        return MerchantForm
    
    def get(self, merchant_id=None):
        merchant = None
        pricetiers = []
        loyalty_code_count = 0
        if merchant_id:
            merchant = Merchant.get(db.Key.from_path('Merchant', int(merchant_id)))
            if merchant.pricetier == None:
                merchant.pricetier = PriceTier.get_by_key_name(merchant.currency)
            pricetiers.append({merchant.currency: merchant.pricetier})
            form = self.get_form()(self.request.POST, merchant)
            
            count_limit = 100000
            loyalty_code_count = LoyaltyCode.all().filter('merchant =', merchant).count(limit=count_limit)
            # if loyalty_code_count == count_limit:
            #     while loyalty_code_count > 0:
            #         loyalty_code_count += LoyaltyCode.all().filter('merchant =', merchant).count(limit=count_limit,offset=loyalty_code_count)
        else:
            form = self.get_form()()
            for currency in i18ndata.ALLOWED_CURRENCIES:
                pricetiers.append({currency: PriceTier.get_by_key_name(currency)})  

        default_pricetiers = []
        for currency in i18ndata.ALLOWED_CURRENCIES:
            default_pricetiers.append({currency: PriceTier.get_by_key_name(currency)})  

        form.image.data = ""
        self.render('merchants/merchants/editmerchant.html', form=form, merchant=merchant, loyalty_code_count=loyalty_code_count, default_pricetiers=default_pricetiers, pricetiers=pricetiers, action=self.uri_for('merchant-save'), display_admin_fields=True)
        
    def delete_existing_loyalty_codes(self,merchant):                
        taskqueue.add(url='/tasks/deleteloyaltycodes', params={ 'merchant': str(merchant.key()) }, retry_options=taskqueue.TaskRetryOptions(task_retry_limit=1), transactional=True)

    def post(self):
        id = self.request.get('_id')
        merchant = None
        
        form = self.get_form()(self.request.POST)
        if (id):
            merchant = Merchant.get(db.Key.from_path('Merchant', int(id)))
            
        # If no image posted, use existing image
        if not self.request.get("image"):
            form.image.data = None
            form.image_type.data = None
            if id:
                form.image.data = merchant.image
                form.image_type.data = merchant.image_type
        # If delete image checked, remove image data
        if self.request.get("image-delete"):
            form.image.data = None
            form.image_type.data = None
        # If image posted, save it as blob
        if self.request.get("image"):
            form.image.data = db.Blob(self.request.get("image"))
            form.image_type.data = self.request.POST['image'].type

        # PriceTier : checks values
        min_values = []
        fees = []
        flat_fee_min_values = []
        flat_fees = []
        discount_min_values = []
        discount_percentages = []
        pt_error = False
        if self.request.POST["fee_rate_use_default"] != "y":  
            for min_value in self.request.POST.getall("min_values[]"):
                if re.match("^\d+\.?\d*$", min_value) != None:
                    min_values.append(Decimal(min_value))
                else:
                    pt_error = True
            for fee in self.request.POST.getall("fees[]"):
                if re.match("^\d+\.?\d*$", min_value) != None:
                    fees.append(Decimal(fee))
                else:
                    pt_error = True
            for flat_fee_min_value in self.request.POST.getall("flat_fee_min_values[]"):
                if re.match("^\d+\.?\d*$", flat_fee_min_value) != None:
                    flat_fee_min_values.append(Decimal(flat_fee_min_value))
                else:
                    pt_error = True
            for flat_fee in self.request.POST.getall("flat_fees[]"):
                if re.match("^\d+\.?\d*$", flat_fee_min_value) != None:
                    flat_fees.append(Decimal(flat_fee))
                else:
                    pt_error = True
            for min_value in self.request.POST.getall("discount_min_values[]"):
                if re.match("^\d+$", min_value) != None:
                    discount_min_values.append(int(min_value))
                else:
                    pt_error = True
            for discount_percentage in self.request.POST.getall("discount_percentages[]"):
                if re.match("^\d+\.?\d*$", min_value) != None:
                    discount_percentages.append(Decimal(discount_percentage))
                else:
                    pt_error = True
            if re.match("^\d+\.?\d*$", self.request.POST['fixed_fee']) == None:
                pt_error = True

        if merchant and merchant.pricetier:
            pt = merchant.pricetier
        else:
            pt = PriceTier()

        if pt_error == False:
            pt.currency = self.request.POST['currency']
            pt.fixed_fee = Decimal(self.request.POST['fixed_fee'], 2)
            pt.set_list('min_value', min_values)
            pt.set_list('fee', fees)
            pt.set_list('flat_fee_min_value', flat_fee_min_values)
            pt.set_list('flat_fee', flat_fees)
            pt.set_list('discount_min_value', discount_min_values)
            pt.set_list('discount_percentage', discount_percentages)  

        if form.validate() and pt_error == False:
            if merchant and merchant.pricetier and self.request.POST["fee_rate_use_default"] == "y":
                db.delete(merchant.pricetier)
            if (merchant):
                form.populate_obj(merchant)
            else:
                merchant = Merchant(location=db.GeoPt('55.4,10.4'), **form.data) # TODO: Remove dummy location after location data has been migrated
            merchant.users = [x.lower() for x in merchant.users]

            if self.request.POST["fee_rate_use_default"] != "y":
                pt.put()
                merchant.pricetier = pt

            merchant.put()

            # If new merchant, add a default tax class
            if not id:
                TaxClass(merchant=merchant, name=str(_("VAT")), rate=i18ndata.PRODUCT_VAT_RATES[merchant.country]*100, is_default=True).put()

            # If Loyalty CSV posted, delete existing LoyialtyCodes for this Merchant, then parse and save as new associated LoyaltyCode objects
            if self.request.get("loyalty_code-delete"):
                db.run_in_transaction(self.delete_existing_loyalty_codes,merchant)

            if self.request.get("loyalty_csv"):
                logging.info('Found loyalty_csv in form')

                # ADD NEW LOYALTYCODES
                reader = csv.reader(self.request.get("loyalty_csv").split('\n'), delimiter=';')
                chunk, chunksize = [], 100

                for i, line in enumerate(reader):
                    #logging.info('i=%i: line=%s' % (i, line))
                    if (i % chunksize == 0 and i > 0):
                        taskqueue.add(url='/tasks/createloyaltycodes', params={ 'merchant': str(merchant.key()), 'csv_chunk': str(chunk) }, retry_options=taskqueue.TaskRetryOptions(task_retry_limit=1))
                        del chunk[:]
                    dk_escaped = line[0].replace('æ','&aelig;').replace('Æ','&Aelig;').replace('ø','&oslash;').replace('Ø','&Oslash;').replace('å','&aring;').replace('Å','&Aring;')
                    chunk.append(dk_escaped)

                # process the remainder
                logging.info('process the remainder :%s',str(chunk))
                taskqueue.add(url='/tasks/createloyaltycodes', params={ 'merchant': str(merchant.key()), 'csv_chunk': str(chunk) }, retry_options=taskqueue.TaskRetryOptions(task_retry_limit=1))
            
            # Clear merchant open for orders cache
            merchant.clear_open_cache()
            # Clear product cache
            taskqueue.add(url='/tasks/clearproductcache', params={ 'merchant': str(merchant.key()) }, retry_options=taskqueue.TaskRetryOptions(task_retry_limit=1))
            
            self.add_success(gettext('Merchant saved'))
            if self.request.get('continue_edit'):
                self.redirect_to('merchant-edit', merchant_id=merchant.key().id())
            else:
                if merchant.locations.count():
                    self.redirect_to('merchant-list')
                else:
                    # For newly created merchant, switch to this merchant and go to create location
                    self.set_cookie('m', str(merchant.key()))
                    self.add_info(gettext('Please create the first location and bar for the merchant'))
                    self.redirect_to('location-new')
        else:
            form.image.data = ""
            pricetiers = []
            if self.request.POST["fee_rate_use_default"] != "y": 
                pricetiers.append({self.request.POST["currency"]:pt})
            else:
                for currency in i18ndata.ALLOWED_CURRENCIES:
                    pricetiers.append({currency: PriceTier.get_by_key_name(currency)}) 

            default_pricetiers = []
            for currency in i18ndata.ALLOWED_CURRENCIES:
                default_pricetiers.append({currency: PriceTier.get_by_key_name(currency)})  

            self.add_error(gettext('Please correct errors in your input and try save again.'))
            self.render('merchants/merchants/editmerchant.html', form=form, merchant=merchant, action=self.uri_for('merchant-save'), display_admin_fields=True, pricetier=pt, pricetiers=pricetiers, default_pricetiers=default_pricetiers )
            
class MerchantDeleteHandler(AdminHandler):
    def post(self, merchant_id):
        if merchant_id:
            merchant = Merchant.get(db.Key.from_path('Merchant', int(merchant_id)))
            # If merchant to delete is currently being managed, forget about the merchant in the cookie
            if self.merchant.key() == merchant.key():
                self.set_cookie('m', None)
            name = merchant.name
            merchant.delete()
            self.add_success(_('%s is being deleted and should be removed from the list shortly.') % name)
            logging.info('%s deleted' % name)
        self.redirect_to('merchant-list')

class ProfileHandler(AdminHandler):
    def get_form(self):
        excluded_fields = [
            'currency',
            'timezone',
            'location',
            'country',
            'pricetier',
            'custom_allowed_payment_methods_enabled',
            'custom_allowed_payment_method_epay',
            'custom_allowed_payment_method_swipp',
            'custom_allowed_payment_method_epaymobilepay',
            'custom_allowed_payment_method_mobilepay_appswitch',
            'custom_allowed_payment_method_mobilepay_appswitch_embedded'
        ]
        ProfileModelForm = model_form(Merchant, Form, exclude=excluded_fields, field_args={
            'name': { 'label': _('Name') },
            'prices_include_tax': { 'label': _('Prices Include Tax') },
            'users': { 'label': _('Users') },
            'address': { 'label': _('Address') },
            'email': { 'label': _('Email Address') },
            'phonenr': { 'label': _('Phone Number') },
            'vat_number': { 'label': _('VAT Number') },
            'bank_account': { 'label': _('Bank Account') },
            'entrance_pincode': { 'label': _('Staff PIN') },
            'auto_prepare': { 'label': _('Auto Prepare Menu Orders') },
            'receive_news': { 'label': _('Receive Updates') },
            'has_loyalty_program': { 'label': _('Activate Loyalty Program') },
            'loyalty_program_name': { 'label': _('Specific Loyalty Program Name') },
            'loyalty_program_rate': { 'label': _('Loyalty Program Discount Rate') }
        })
        class ProfileForm(ProfileModelForm):
            serve_with_consumer_device = BooleanRadioField(label=_("Serve menu orders using"), choices=[('', _("The service screen")), ('y', _("Staff PIN on the customer's phone"))], coerce=bool, default=False)
            service_charge = LocalizedDecimalField(places=2, label=_('Service Charge'), default=0)
            loyalty_program_rate = LocalizedDecimalField(places=2, label=_('Loyalty Program Rate'), default=0, validators=[validators.NumberRange(min=0, max=100, message=_('Loyalty discount cannot be higher then 100'))])
            image = FileField(label=_('Image'), validators=[validators.length(max=1000000, message=_('The image file is too large, must be maximum 1 MB.'))])
            loyalty_csv = FileField(label=_('CSV'), validators=[validators.length(max=1000000, message=_('The CSV file is too large, must be maximum 1 MB.'))])
        return ProfileForm
        
    def get(self):
        form = self.get_form()(self.request.POST, self.merchant)
        form.image.data = ""
        count_limit = 100000
        loyalty_code_count = LoyaltyCode.all().filter('merchant =', self.merchant).count(limit=count_limit)
        # if loyalty_code_count == count_limit:
        #     while loyalty_code_count > 0:
        #         loyalty_code_count += LoyaltyCode.all().filter('merchant =', merchant).count(limit=count_limit,offset=loyalty_code_count)
        self.render('merchants/merchants/editmerchant.html', form=form, merchant=self.merchant, loyalty_code_count=loyalty_code_count, action=self.uri_for('profile-save'), display_admin_fields=False)

    def delete_existing_loyalty_codes(self, merchant):                
        taskqueue.add(url='/tasks/deleteloyaltycodes', params={ 'merchant': str(merchant.key()) }, retry_options=taskqueue.TaskRetryOptions(task_retry_limit=1), transactional=True)
        
    def post(self):
        id = self.request.get('_id')
        merchant = self.merchant
        
        form = self.get_form()(self.request.POST)
            
        # If no image posted, use existing image
        if not self.request.get("image"):
            form.image.data = None
            form.image_type.data = None
            if id:
                form.image.data = merchant.image
                form.image_type.data = merchant.image_type
        # If delete image checked, remove image data
        if self.request.get("image-delete"):
            form.image.data = None
            form.image_type.data = None
        # If image posted, save it as blob
        if self.request.get("image"):
            form.image.data = db.Blob(self.request.get("image"))
            form.image_type.data = self.request.POST['image'].type
            
        if form.validate():
            if (merchant):
                form.populate_obj(merchant)
            else:
                merchant = Merchant(**form.data)
            merchant.users = [x.lower() for x in merchant.users]
            
            # If Loyalty CSV posted, delete existing LoyialtyCodes for this Merchant, then parse and save as new associated LoyaltyCode objects
            if self.request.get("loyalty_code-delete"):
                db.run_in_transaction(self.delete_existing_loyalty_codes,merchant)

            if self.request.get("loyalty_csv"):
                logging.info('Found loyalty_csv in form')

                # ADD NEW LOYALTYCODES
                reader = csv.reader(self.request.get("loyalty_csv").split('\n'), delimiter=';')
                chunk, chunksize = [], 100

                for i, line in enumerate(reader):
                    #logging.info('i=%i: line=%s' % (i, line))
                    if (i % chunksize == 0 and i > 0):
                        taskqueue.add(url='/tasks/createloyaltycodes', params={ 'merchant': str(merchant.key()), 'csv_chunk': unicode(chunk) }, retry_options=taskqueue.TaskRetryOptions(task_retry_limit=1))
                        del chunk[:]
                    dk_escaped = line[0].replace('æ','&aelig;').replace('Æ','&Aelig;').replace('ø','&oslash;').replace('Ø','&Oslash;').replace('å','&aring;').replace('Å','&Aring;')
                    chunk.append(dk_escaped)

                # process the remainder
                logging.info('process the remainder :%s',str(chunk))
                taskqueue.add(url='/tasks/createloyaltycodes', params={ 'merchant': str(merchant.key()), 'csv_chunk': unicode(chunk) }, retry_options=taskqueue.TaskRetryOptions(task_retry_limit=1))
            
            merchant.put()

            # Clear product cache
            taskqueue.add(url='/tasks/clearproductcache', params={ 'merchant': str(merchant.key()) }, retry_options=taskqueue.TaskRetryOptions(task_retry_limit=1))
            
            self.add_success(gettext('Profile saved'))
            self.redirect_to('profile-edit')
        else:
            form.image.data = ""
            self.add_error(gettext('Please correct errors in your input and try save again.'))
            self.render('merchants/merchants/editmerchant.html', form=form, merchant=merchant, action=self.uri_for('profile-save'), display_admin_fields=False)
            
class AccountListHandler(BaseHandler):
    csrf_protect = False
    
    def get(self):
        merchants = Merchant.all().filter('users =', users.get_current_user().email())
        self.render('merchants/merchants/accounts.html', merchants=merchants, logout_link=users.create_logout_url(webapp2.uri_for('product-list', group='menu')))
        
    def post(self):
        merchant_key = self.request.get('merchant_key')
        self.set_cookie('m', str(merchant_key), expires=(datetime.datetime.now() + datetime.timedelta(days=7)))
        self.redirect_to('product-list', group='menu')
