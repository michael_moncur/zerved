from views.core import AdminHandler
from views.utilities import Pager
from models.merchants import Merchant, MerchantInvoice, MerchantInvoiceItem
from models import i18ndata
from paging import *
from models.decimalproperty import *
import logging
from models import counter
from datetime import *
from webapp2_extras import i18n
_ = i18n.gettext


class InvoiceNewHandler(AdminHandler):
    require_admin = True

    def get(self):
        self.render('merchants/sales/editinvoice.html', merchants=Merchant.all(), rates=i18ndata.VAT_RATES)

    def post(self):
        postdata = self.request.POST
        items = postdata.getall('description[]')
        quantities = postdata.getall('quantity[]')
        unit_prices_excl_tax = postdata.getall('unit_price_excl_tax[]')
        tax_rates = postdata.getall('tax_rate[]')
        merchant = Merchant.get(db.Key(postdata['merchant']))

        # Create the invoice
        invoice = MerchantInvoice(
            merchant = merchant,
            date_created = datetime.now(),
            invoice_date = datetime.now(),
            invoice_number = counter.increment_and_get_count("merchantinvoice"),
            open = False,
            total_excl_tax = Decimal(0),
            total_incl_tax = Decimal(0),
            total_tax = Decimal(0),
            total_positive = Decimal(0),
            total_negative = Decimal(0),
            total_discount = Decimal(0),
            order_revenue = Decimal(0),
            order_count = 0,
            first_order_date_completed = datetime.now(),
            last_order_date_completed = datetime.now(),
            settled = False,
            currency = merchant.currency,
            merchant_name = merchant.name,
            merchant_address = merchant.address,
            merchant_vat_number = merchant.vat_number,
            merchant_bank_account = merchant.bank_account,
            merchant_country = merchant.country,
        )
        # Save it so that invoice items can be set with a reference to it
        invoice.put()

        invoice_items = []
        for i in range(len(items)):
            name = items[i]
            quantity = int(quantities[i])
            vat_rate = Decimal(tax_rates[i])
            unit_price_excl_tax = Decimal(unit_prices_excl_tax[i])
            unit_price_incl_tax = Decimal(unit_prices_excl_tax[i]) * (1 + vat_rate / 100)
            total_excl_tax = unit_price_excl_tax * quantity
            total_incl_tax = unit_price_incl_tax * quantity
            total_tax = total_incl_tax - total_excl_tax
            invoice_items.append(MerchantInvoiceItem(
                invoice=invoice,
                sku='custom',
                name=name,
                name_parameters=[],
                quantity=quantity,
                unit_price_excl_tax=unit_price_excl_tax,
                unit_price_incl_tax=unit_price_incl_tax,
                total_excl_tax=total_excl_tax,
                total_incl_tax=total_incl_tax,
                total_tax=total_tax,
                tax_rate=vat_rate
            ))

        # Update the invoice with the totals
        invoice.collect_totals(invoice_items)

        # Save the invoice items and update the invoice
        for i in invoice_items:
            i.put()
        invoice.put()

        self.add_success(_('The invoice has been created and should be available in the list within a few seconds'))
        self.redirect_to('settlements')

class InvoiceListHandler(AdminHandler):
    def get(self):
        page = int(self.request.get('page')) if self.request.get('page') else 1
        query = PagedQuery(MerchantInvoice.all().filter('merchant =', self.merchant).filter('open =', False).order('-invoice_date'), 20)
        invoices = query.fetch_page(page)
        pager = Pager(self, query, page)
        self.render('merchants/sales/invoices.html', invoices=invoices, pager=pager.render())
        
class InvoiceViewHandler(AdminHandler):
    def get(self, invoice_id):
        invoice = MerchantInvoice.get(db.Key.from_path('MerchantInvoice', int(invoice_id)))
        lines = invoice.items.order("sort_order")
        self.render('merchants/sales/viewinvoice.html', invoice=invoice, lines=lines)
        
class SettlementsHandler(AdminHandler):
    require_admin = True
    
    def get(self):
        page = int(self.request.get('page')) if self.request.get('page') else 1
        query = PagedQuery(MerchantInvoice.all().filter('open =', False).order('-invoice_date'), 20)
        invoices = query.fetch_page(page)
        pager = Pager(self, query, page)
        self.render('merchants/sales/settlements.html', invoices=invoices, pager=pager.render())
        
class SaveSettlementHandler(AdminHandler):
    require_admin = True
    
    def post(self):
        invoice_key = self.request.get('invoice_key')
        settled = self.request.get('settled') == '1'
        invoice = MerchantInvoice.get(invoice_key)
        invoice.settled = settled
        invoice.put()
        
class InvoiceOrdersHandler(AdminHandler):
    def get(self, invoice_id):
        invoice = MerchantInvoice.get(db.Key.from_path('MerchantInvoice', int(invoice_id)))
        orders = invoice.orders.order('date_completed')
        self.render('merchants/sales/invoiceorders.html', invoice=invoice, orders=orders)