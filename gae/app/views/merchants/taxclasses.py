from views.core import AdminHandler
from models.merchants import Merchant, TaxClass
from models import catalogforms
from google.appengine.ext import db
from webapp2_extras import i18n
_ = i18n.gettext

class TaxClassListHandler(AdminHandler):
    def get(self):
        tax_classes = TaxClass.all().filter('merchant =', self.merchant)
        self.render('merchants/catalog/taxclasses.html', tax_classes=tax_classes)
        
class TaxClassEditHandler(AdminHandler):
    def get(self, taxclass_id=None):
        tax_class = None
        if taxclass_id:
            tax_class = TaxClass.get(db.Key.from_path('TaxClass', int(taxclass_id)))
            if not self.validate_merchant_permission(tax_class):
                return
            form = catalogforms.get_tax_class_form()(self.request.POST, tax_class)
        else:
            form = catalogforms.get_tax_class_form()()
        self.render('merchants/catalog/edittaxclass.html', form=form, tax_class=tax_class)
        
    def post(self):
        id = self.request.get('_id')
        tax_class = None
        form = catalogforms.get_tax_class_form()(self.request.POST)
        if (id):
            tax_class = TaxClass.get(db.Key.from_path('TaxClass', int(id)))
            if not self.validate_merchant_permission(tax_class):
                return
            
        if form.validate():
            if (tax_class):
                form.populate_obj(tax_class)
            else:
                tax_class = TaxClass(**form.data)
                tax_class.merchant = self.merchant
                
            tax_class.put()
            
            if tax_class.is_default:
                tax_classes = TaxClass.all().filter('merchant =', self.merchant).filter('__key__ !=', tax_class.key())
                for tax_class in tax_classes:
                    tax_class.is_default = False
                    tax_class.put()
                    
            link = '<a href="'+self.uri_for('taxclass-edit', taxclass_id=tax_class.key().id())+'">'+tax_class.name+'</a>'
            if id or self.request.get('continue_edit'):
                self.add_success(_('%s saved') % link)
            else:
                self.add_success((_('%s is being saved and should be available in the list and app within a few seconds') % link)+' &mdash; <a href="#" onclick="location.reload(); return false;">'+_('refresh list')+'</a>')
            if self.request.get('continue_edit'):
                self.redirect_to('taxclass-edit', taxclass_id=tax_class.key().id())
            else:
                self.redirect_to('taxclass-list')
        else:
            self.add_error(_('Please correct errors in your input and try save again.'))
            self.render('merchants/catalog/edittaxclass.html', form=form, tax_class=tax_class)

class TaxClassDeleteHandler(AdminHandler):
    def post(self, taxclass_id):
        if taxclass_id:
            tax_class = TaxClass.get(db.Key.from_path('TaxClass', int(taxclass_id)))
            if not self.validate_merchant_permission(tax_class):
                return
            self.add_success(_('%s is being deleted and should be removed from the list shortly.') % tax_class.name)
            tax_class.delete()
        self.redirect_to('taxclass-list')
