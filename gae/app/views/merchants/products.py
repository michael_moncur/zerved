from views.core import AdminHandler
from views.utilities import Pager
from models.merchants import Merchant, Bar, TaxClass
from models.catalog import *
from models import catalogforms
from google.appengine.ext import db
from google.appengine.api import memcache, users, taskqueue
from paging import *
from webapp2_extras import i18n
_ = i18n.gettext
import json
import logging

class ProductListHandler(AdminHandler):
    def get(self, group, category_id=None):
        if not group or group not in ['menu', 'entrance']:
            self.abort(404)

        categories = self.merchant.categories.order('sort_order').filter('group = ', group)
        page = int(self.request.get('page')) if self.request.get('page') else 1
        query = Product.all().filter('merchant =', self.merchant).filter('group =', group)
        # Category id may be specified as URL parameter. If not, try to use cookie value instead. Category id 0 means all categories.
        if not category_id and self.request.cookies.get('category'):
            category_id = self.request.cookies.get('category')
        if category_id:
            self.set_cookie('category', str(category_id))
            if int(category_id) != 0:
                category = Category.get(db.Key.from_path('Category', int(category_id)))
                if category and category.merchant.key() == self.merchant.key():
                    query = query.filter('category =', category)
                else:
                    category_id = '0'
        # Temporarily disabled paging because it does not work with IN queries
        query = PagedQuery(query.order('sort_order'), 20)
        products = query.fetch_page(page)
        products = query.fetch(1000)
        products = Product.prefetch_categories(products)
        pager = Pager(self, query, page, 'right')
        self.render('merchants/catalog/products.html', products=products, pager=pager.render(), categories=categories, category_id=category_id, group=group)
        
class ProductEditHandler(AdminHandler):
    def get(self, group, product_id=None, clone=False):
        mform = catalogforms.get_menu_form()(prefix='menu_')
        mform.set_merchant(self.merchant)
        product = None
        if product_id:
            product = Product.get(db.Key.from_path('Product', int(product_id)))
            if not self.validate_merchant_permission(product):
                return
            form = catalogforms.get_product_form()(self.request.POST, product)
            if product.tax_class:
                form.set_selected_tax_class(str(product.tax_class.key()))
            if product.category:
                form.set_selected_category(str(product.category.key()))
            form.clone_image_from_product.data = product_id
        else:
            form = catalogforms.get_product_form()()
            default_tax_class = TaxClass.get_default_tax_class(self.merchant)
            if default_tax_class:
                form.set_selected_tax_class(str(default_tax_class.key()))
            if self.request.cookies.get('category'):
                category_id = int(self.request.cookies.get('category'))
                if category_id != 0:
                    form.set_selected_category(str(db.Key.from_path('Category', category_id)))
            form.enabled_locations.data = [str(location.key()) for location in self.merchant.locations]
            form.enabled_bars.data = [str(bar.key()) for bar in self.merchant.bar_set]
        form.set_merchant(self.merchant, group)
        form.image.data = ""
        allpricerules = self.merchant.price_rules
        priceruleform = catalogforms.get_price_rule_form()(prefix='pricerule_')
        priceruleform.set_merchant(self.merchant)
        
        js_optionform = catalogforms.get_product_option_form()()
        optionforms = []
        if product_id:
            for idx, option in enumerate(product.get_options()):
                optionform = catalogforms.get_product_option_form()(obj=option, prefix='option-'+str(idx)+'-')
                if clone:
                    optionform.option_id.data = ""
                optionforms.append(optionform)
        form.option_forms.data = len(optionforms)
        
        taxclassform = catalogforms.get_tax_class_form()(prefix='taxclass_')
        categoryform = catalogforms.get_category_form()(prefix='category_')
        
        bars = Bar.all().filter("merchant = ", self.merchant) 

        self.render('merchants/catalog/editproduct.html', form=form, mform=mform, product=product, allpricerules=allpricerules, pricerulewtform=priceruleform, optionforms=optionforms, js_optionform=js_optionform, taxclasswtform=taxclassform, categorywtform=categoryform, clone=clone, group=group, bars=bars)
        
    def post(self, group):
        id = self.request.get('_id')
        product = None
        postdata = self.request.POST
        # Product form
        form = catalogforms.get_product_form()(postdata)
        form.set_merchant(self.merchant, group)
        # Menu form
        newmenu = 'new_menu' in postdata and postdata['new_menu'] == 'new'
        menuform = catalogforms.get_menu_form()(postdata, prefix='menu_')
        menuform.set_merchant(self.merchant)
        # Price Rules form
        priceruleform = catalogforms.get_price_rule_form()(postdata, prefix='pricerule_')
        priceruleform.set_merchant(self.merchant)
        # Tax Classes form
        taxclassform = catalogforms.get_tax_class_form()(postdata, prefix='taxclass_')
        # Categories form
        categoryform = catalogforms.get_category_form()(postdata, prefix='category_')
        
        if (id):
            product = Product.get(db.Key.from_path('Product', int(id)))
            if not self.validate_merchant_permission(product):
                return
            
        # If no image posted, use existing image
        if not self.request.get("image"):
            form.image.data = None
            form.image_type.data = None
            if id:
                form.image.data = product.image
                form.image_type.data = product.image_type
            elif self.request.get("clone_image_from_product"):
                cloned_product = Product.get(db.Key.from_path('Product', int(self.request.get("clone_image_from_product"))))
                if cloned_product and cloned_product.image:
                    form.image.data = cloned_product.image
                    form.image_type.data = cloned_product.image_type
        # If delete image checked, remove image data
        if self.request.get("image-delete"):
            form.image.data = None
            form.image_type.data = None
        # If image posted, save it as blob
        if self.request.get("image"):
            form.image.data = db.Blob(self.request.get("image"))
            form.image_type.data = self.request.POST['image'].type
            
        # Validate price rule form if "create new price rule" is selected
        priceruleform_valid = (not self.request.get('new_price_rule') or priceruleform.validate())
        
        # Collect and validate option forms
        optionforms=[]
        optionforms_valid = True
        for i in range(int(self.request.get('option_forms'))):
            optionform = catalogforms.get_product_option_form()(postdata, prefix='option-'+str(i)+'-')
            optionforms.append(optionform)
            if not optionform.deleted.data == '1' and not optionform.validate():
                optionforms_valid = False
                
        # Validate tax class form if "create new tax class" is selected
        taxclassform_valid = (postdata['select_tax_class'] != 'new' or taxclassform.validate())
        
        # Validate new menu form if "new menu" is selected
        menuform_valid = (not newmenu or menuform.validate())
        
        # Validate category form if "create new category" is selected
        categoryform_valid = (postdata['select_category'] != 'new' or categoryform.validate())
        
        if form.validate() and priceruleform_valid and optionforms_valid and taxclassform_valid and categoryform_valid and menuform_valid:
            # Initialize product object
            if (product):
                form.populate_obj(product)
            else:
                newproductdata = form.data
                if postdata['select_category'] != 'new' and postdata['select_category'] != '':
                    newproductdata['category'] = db.Key(postdata['select_category'])
                product = Product.construct_for_merchant(merchant=self.merchant, **newproductdata)
                product.group = group
                
            # Set selected price rules
            product.price_rules = [db.Key(keystr) for keystr in postdata.getall('price_rules')]
            
            # Create new price rule
            if self.request.get('new_price_rule'):
                if postdata['pricerule_select_schedule'] == 'new':
                    # Create new schedule for price rule
                    schedule = PriceRuleSchedule(merchant=self.merchant)
                    schedule.name = postdata['schedule_name']
                    schedule.set_hours_from_checkboxes(postdata.getall('schedule_hours'))
                    schedule.put()
                    priceruleform.select_schedule.data = str(schedule.key())
                pricerule = PriceRule(**priceruleform.data)
                pricerule.merchant = self.merchant
                pricerule.schedule = db.Key(priceruleform.select_schedule.data) if priceruleform.select_schedule.data else None
                pricerule.put()
                product.price_rules.append(pricerule.key())

            # Update menus
            product.menus = []
            for menu in postdata.getall('menus'):
                product.add_menu(db.Key(menu))
                    
            # Create new menu
            if newmenu:
                logging.info("***** CREATING NEW MENU *****")
                menu = Menu(**menuform.data)
                menu.merchant = self.merchant
                # New schedule
                if postdata['menu_select_schedule'] == 'new':
                    schedule = PriceRuleSchedule(merchant=self.merchant)
                    schedule.name = postdata['menu_schedule_name']
                    schedule.set_hours_from_checkboxes(postdata.getall('menu_schedule_hours'))
                    schedule.put()
                    postdata['menu_select_schedule'] = str(schedule.key())

                menu.schedule = db.Key(postdata['menu_select_schedule']) if postdata['menu_select_schedule'] else None
                # Assign menu to selected bars
                for bar in postdata.getall('bars'):
                    menu.add_bar(db.Key(bar))

                menu.put()
                product.menus.append(menu.key())

            # Create new tax class
            if postdata['select_tax_class'] == 'new':
                taxclass = TaxClass(**taxclassform.data)
                taxclass.merchant = self.merchant
                taxclass.put()
                postdata['select_tax_class'] = str(taxclass.key())

            # Update product tax class
            product.tax_class = db.Key(postdata['select_tax_class']) if postdata['select_tax_class'] else None
            
            # Create new category
            if postdata['select_category'] == 'new':
                category = Category.construct_for_merchant(self.merchant, **categoryform.data)
                category.group = group
                category.put()
                postdata['select_category'] = str(category.key())
                
            # Update product category
            product.category = db.Key(postdata['select_category']) if postdata['select_category'] else None
            
            product.put()
            
            # Options
            for optionform in optionforms:
                option = ProductOption.get(db.Key.from_path('ProductOption', int(optionform.option_id.data))) if optionform.option_id.data else None 
                if optionform.deleted.data == '1':
                    if option:
                        # Delete
                        option.delete()
                else:
                    # Create/update
                    if option:
                        optionform.populate_obj(option)
                    else:
                        option = ProductOption(**optionform.data)
                    option.product = product
                    option.put()
                    
            memcache.delete("pricerules_"+str(self.merchant.key().id()))
            taskqueue.add(url='/tasks/clearproductcache', params={ 'merchant': str(self.merchant.key()) }, retry_options=taskqueue.TaskRetryOptions(task_retry_limit=1))
            
            link = '<a href="'+self.uri_for('product-edit', product_id=product.key().id(), group=group)+'">'+product.name+'</a>'
            if id or self.request.get('continue_edit'):
                self.add_success(_('%s saved') % link)
            else:
                self.add_success((_('%s is being saved and should be available in the list and app within a few seconds') % link)+' &mdash; <a href="#" onclick="location.reload(); return false;">'+_('refresh list')+'</a>')
            if self.request.get('continue_edit'):
                self.redirect_to('product-edit', product_id=product.key().id(), group=group)
            else:
                if self.request.get('continue_tutorial'):
                    self.redirect_to('tutorial-end')
                else:
                    self.redirect_to('product-list', group=group)
        else:
            form.image.data = ""
            allbars = Bar.all().filter("merchant = ", self.merchant) 
            allpricerules = self.merchant.price_rules
            js_optionform = catalogforms.get_product_option_form()()
            form.option_forms.data = len(optionforms)

            # Location/bar keys must be converted to strings
            form.enabled_locations.data = [str(v) for v in form.enabled_locations.data]
            form.enabled_bars.data = [str(v) for v in form.enabled_bars.data]
            self.add_error(_('Please correct errors in your input and try save again.'))
            self.render('merchants/catalog/editproduct.html', form=form, mform=menuform, product=product, allpricerules=allpricerules, pricerulewtform=priceruleform, js_optionform=js_optionform, optionforms=optionforms, taxclasswtform=taxclassform, categorywtform=categoryform, clone=bool(int(postdata['clone'])), group=group, bars=allbars)
            
class ProductCloneHandler(ProductEditHandler):
    def get(self, group, product_id=None, clone=False):
        self.add_info(_('Please review the product data and click Save Product to create the new product'))
        super(ProductCloneHandler, self).get(product_id=product_id, clone=True, group=group)
    
class ProductDeleteHandler(AdminHandler):
    def post(self, group, product_id):
        if product_id:
            product = Product.get(db.Key.from_path('Product', int(product_id)))
            if not self.validate_merchant_permission(product):
                return
            self.add_success(_('%s is being deleted and should be removed from the list shortly.') % product.name)
            product.delete()
            taskqueue.add(url='/tasks/clearproductcache', params={ 'merchant': str(self.merchant.key()) }, retry_options=taskqueue.TaskRetryOptions(task_retry_limit=1))
        self.redirect_to('product-list', group=group)

class ProductSortHandler(AdminHandler):
    def get(self, group, category_id):
        # List all products
        category = Category.get(db.Key.from_path('Category', int(category_id)))
        query = Product.all().filter('merchant =', self.merchant).filter('category =', category).order('sort_order')
        self.render('merchants/catalog/sortproducts.html', products=query, category=category, group=group)
        
    def post(self, group, category_id):
        category = Category.get(db.Key.from_path('Category', int(category_id)))
        # Product list
        query = Product.all().filter('merchant =', self.merchant).filter('category =', category).order('sort_order')
        # List of keys in newly sorted order
        sorted_keys = self.request.get_all('product_id[]')
        # Products that have changed position
        products_to_save = []
        for product in query:
            # Fetch new index, update the product if it has changed
            new_index = sorted_keys.index(str(product.key()))
            if product.sort_order != new_index:
                product.sort_order = new_index
                products_to_save.append(product)
        # Save modified products
        db.put(products_to_save)
        # Clear product cache
        taskqueue.add(url='/tasks/clearproductcache', params={ 'merchant': str(self.merchant.key()) }, retry_options=taskqueue.TaskRetryOptions(task_retry_limit=1))
        # Return to product list
        self.add_success(_('The new sort orders are being saved and should be reflected in the list shortly.'))
        self.redirect_to('product-list', group=group)

class MenuPreviewHandler(AdminHandler):
    def get(self):
        self.render('merchants/catalog/menupreview.html')
