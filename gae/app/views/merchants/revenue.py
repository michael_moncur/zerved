from views.core import AdminHandler
from models.revenue import RevenueReport
import datetime
import pytz

class RevenueHandler(AdminHandler):
    def get(self):
        report = None
        place = self.request.get('place') if self.request.get('place') else None
        (place_type, _, place_id) = place.partition("-") if place else (None, None, None)
        location = place_id if place_type == 'loc' else None
        bar = place_id if place_type == 'bar' else None
        # If parameters submitted, generate report
        if self.request.get('startdate') and self.request.get('enddate') and self.request.get('timesplit'):
            startdate = pytz.timezone(self.merchant.timezone).localize(datetime.datetime.strptime(self.request.get('startdate') + ' ' + self.request.get('timesplit'), "%Y-%m-%d %H:%M")).astimezone(pytz.utc)
            enddate = pytz.timezone(self.merchant.timezone).localize(datetime.datetime.strptime(self.request.get('enddate') + ' ' + self.request.get('timesplit'), "%Y-%m-%d %H:%M")).astimezone(pytz.utc)
            report = RevenueReport(self.merchant, location, bar, startdate, enddate)
        # Default values: Start yesterday, end today
        startdate = self.request.get('startdate') if self.request.get('startdate') else (datetime.datetime.now()-datetime.timedelta(days=1)).strftime("%Y-%m-%d")
        enddate = self.request.get('enddate') if self.request.get('enddate') else (datetime.datetime.now()).strftime("%Y-%m-%d")
        if self.request.get('csv') and self.request.get('csv') == '1':
            # CSV report
            self.response.headers.add('Content-Disposition', 'attachment; filename="report.csv"')
            self.response.content_type = 'application/octet-stream'
            self.render('merchants/csv/revenue.csv', report=report, startdate=startdate, enddate=enddate, timesplit=self.request.get('timesplit'), location=location, bar=bar)
        else:
            # HTML report
            self.render('merchants/sales/revenue.html', report=report, startdate=startdate, enddate=enddate, timesplit=self.request.get('timesplit'), location=location, bar=bar, csvlink=self.request.url+"&csv=1")