from views.core import AdminHandler
from models.sales import Order, OrderItem
from models.consumers import Consumer
import datetime
from views.utilities import Pager
from paging import *
from google.appengine.api import users, taskqueue
import logging

class ConsumerHandler(AdminHandler):
	def get(self, consumer_id):
		consumer = Consumer.get(db.Key.from_path('Consumer', int(consumer_id)))
		if not consumer:
			self.abort(404)
		orders = self.merchant.orders.filter('consumer =', consumer).order('-date_created')
		self.render('merchants/sales/consumer.html', consumer=consumer, orders=orders)