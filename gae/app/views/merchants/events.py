from views.core import AdminHandler
from models.merchants import Merchant, Location, Bar
from models.events import EventSchedule, Event
from models import eventforms
from google.appengine.ext import db
from webapp2_extras import i18n
from datetime import datetime, date, timedelta
from time import mktime
from models import i18ndata
_ = i18n.gettext
from babel import Locale

import logging, calendar, time

class EventListHandler(AdminHandler):
    def get(self):
        # Load events for the month
        now = datetime.now()
        month = int(self.request.get('m')) if 'm' in self.request.GET and 1 <= int(self.request.get('m')) <= 12 else now.month
        year = int(self.request.get('y')) if 'y' in self.request.GET and (now.year - 10) <= int(self.request.get('y')) <= (now.year + 10)  else now.year
        events = self.merchant.events.filter('date_delivery >=', datetime(year, month, 1)).filter('date_delivery <', datetime( (year+1) if (month+1) > 12 else year, (month+1) if (month < 12) else 1, 1)).fetch(1000)
        # Prefetch merchant object for performance
        for e in events:
            e.merchant = self.merchant
        # Calendar
        cal = calendar.monthcalendar(year, month)
        self.render('merchants/events/events.html', events=events, calendar=cal, months=self.get_locale_object().months['format']['wide'] , today=now, month=month, year=year)


class EventEditHandler(AdminHandler):
    def get(self, event_id=None, clone=False):
        event = None
        if event_id:
            event = EventSchedule.get(db.Key.from_path('EventSchedule', int(event_id)))
            if not self.validate_merchant_permission(event):
                return
            form = eventforms.get_event_form()(self.request.POST, event)
        else:
            form = eventforms.get_event_form()()
       
        form.set()
        if event:
            form.set_selected_repeats(event.repeats)

        bars = Bar.all().filter('merchant = ', self.merchant)
        bar_keys = [str(key) for key in event.enabled_bars] if event else []

        now = self.request.GET['date'] if 'date' in self.request.GET else date.today() 
        self.render('merchants/events/editevent.html', form=form, event=event, merchant=self.merchant, now=now, bars=bars, bar_keys=bar_keys, clone=clone)
        
    def post(self):    	
        id = self.request.get('_id')
        event = None
        form = eventforms.get_event_form()(self.request.POST)
        form.set()
        if (id):
            event = EventSchedule.get(db.Key.from_path('EventSchedule', int(id)))
            if not self.validate_merchant_permission(event):
                return
        
        location = Location.get(db.Key(self.request.POST['location-key']))
        error = (location == None)

        if form.validate() and error == False:
            if (event):
                form.populate_obj(event)
            else:
                event = EventSchedule(**form.data)

            event.merchant = self.merchant
            event.location = location

            event.enabled_bars = []
            for bar_key in self.request.POST.getall("bars"):
                bar = Bar.get(bar_key)
                if bar and bar.location.key() == event.location.key():
                    event.add_bar(bar.key())

            event.first_date = datetime.fromtimestamp(mktime(time.strptime(self.request.POST['start-date'] + ' ' + self.request.POST['start-time-hours'] + ':' + self.request.POST['start-time-minutes'], "%Y-%m-%d %H:%M")))            

            event.close_offset_minutes = -((int(self.request.POST['ordering-end-days']) * 24 * 60) + (int(self.request.POST['ordering-end-hours']) * 60) + (int(self.request.POST['ordering-end-minutes'])))

            event.last_date = datetime.fromtimestamp(mktime(time.strptime(self.request.POST['end-date'] + ' ' + self.request.POST['start-time-hours'] + ':' + self.request.POST['start-time-minutes'], "%Y-%m-%d %H:%M")))

            event.labels = self.request.POST.getall("event-labels");

            i = 0
            times = []
            while i < len(self.request.POST.getall("event-time-hours")):
                times.append(self.request.POST.getall("event-time-hours")[i] + ":" + self.request.POST.getall("event-time-minutes")[i])
                i += 1
            event.times = times

            event.excluded_dates = []
            event.repeats = self.request.POST['select_repeats'] if 'select_repeats' in self.request.POST else ''
            if event.repeats != '':
                event.repeat_every = int(self.request.POST['repeat_every']) if 'repeat_every' in self.request.POST else 1
                # Excluding dates
                i = 0
                while i < len(self.request.POST.getall('excluded-dates')):
                    d = self.request.POST.getall('excluded-dates')[i];
                    t = self.request.POST.getall('excluded-time-hours')[i] + ":" + self.request.POST.getall('excluded-time-minutes')[i];
                    excl_date = datetime.fromtimestamp(mktime(time.strptime(d + ' ' + t, "%Y-%m-%d %H:%M")))
                    event.excluded_dates.append(excl_date)
                    i += 1


            if event.get_repetitions() > 100 or len(event.enabled_bars) == 0:
                form.set()
                form.set_selected_repeats(event.repeats)
                if len(event.enabled_bars) == 0:
                    self.add_error(_('The event should be assigned to at least one bar.'))                    
                else:
                    self.add_error(_('The number of repetitions is limited to %s.') % 100)
                bars = Bar.all().filter('merchant = ', self.merchant)
                bar_keys = [str(key) for key in event.enabled_bars] if event else []
                self.render('merchants/events/editevent.html', form=form, event=event, error=True, merchant=self.merchant, now=date.today(), bars=bars, bar_keys=bar_keys)
                return

            else:
                event.put()
                
                link = '<a href="'+self.uri_for('event-edit', event_id=event.key().id())+'">'+event.name+'</a>'
                if id or self.request.get('continue_edit'):
                    self.add_success(_('%s saved') % link)
                else:
                    self.add_success((_('%s is being saved and should be available in the calendar and app within a few seconds') % link)+' &mdash; <a href="#" onclick="location.reload(); return false;">'+_('refresh list')+'</a>')
                if self.request.get('continue_edit'):
                    self.redirect_to('event-edit', event_id=event.key().id())
                else:
                    self.redirect_to('event-list', m=event.first_date.month, y=event.first_date.year)
        else:
            form.set()
            if event:
                form.set_selected_repeats(event.repeats)
            self.add_error(_('Please correct errors in your input and try save again.'))
            bars = Bar.all().filter('merchant = ', self.merchant)
            bar_keys = [str(key) for key in event.enabled_bars] if event else []
            self.render('merchants/catalog/editevent.html', form=form, event=event, merchant=self.merchant, now=date.today(), bars=bars, bar_keys=bar_keys)


class EventCloneHandler(EventEditHandler):
    def get(self, event_id):
        self.add_info(_('Please review the event data and click Save Event to create the new event'))
        super(EventCloneHandler, self).get(event_id=event_id, clone=True)

class EventExcludeHandler(AdminHandler):
    def post(self, recurring_event_id, event_id):
        if recurring_event_id and event_id:
            event = Event.get(db.Key.from_path('EventSchedule', int(recurring_event_id), 'Event', int(event_id)))
            if event == None:
                self.redirect_to('event-list', _abort=True)
            if not self.validate_merchant_permission(event):
                return
            self.add_success(_('%s is being modified and should be updated in the calendar shortly.') % event.name)
            recurring_event = event.parent()
            recurring_event.excluded_dates.append(event.local_date_delivery.replace(tzinfo=None))
            recurring_event.put()
            self.redirect_to('event-list', m=event.local_date_delivery.month, y=event.local_date_delivery.year)
        else:
            self.redirect_to('event-list')

class EventDeleteHandler(AdminHandler):
    def post(self, event_id):
        if event_id:
            event = EventSchedule.get(db.Key.from_path('EventSchedule', int(event_id)))
            if event == None:
                self.redirect_to('event-list', _abort=True)
            if not self.validate_merchant_permission(event):
                return
            self.add_success(_('%s is being deleted and should be removed from the calendar shortly.') % event.name)
            event.delete()
            self.redirect_to('event-list', m=event.first_date.month, y=event.first_date.year)
        else:
            self.redirect_to('event-list')