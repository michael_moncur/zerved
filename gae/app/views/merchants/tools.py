from views.core import BaseHandler, AdminHandler
from models.merchants import Merchant, TaxClass
from models.catalog import Product
from google.appengine.ext import db
from webapp2_extras import i18n
_ = i18n.lazy_gettext
gettext = i18n.gettext
import logging

class ToolsHandler(AdminHandler):
    require_admin = True
    
    def get(self):
        raise Exception("Deprecated feature")
        self.render('merchants/tools.html')
        
# Deprecated - Merchant copies are no longer needed. Merchants may now create multiple bars with shared products instead.
class CopyMerchantMenuHandler(AdminHandler):
    require_admin = True
    
    def post(self):
        raise Exception("Deprecated feature")
        from_merchant_id = self.request.get('from_merchant')
        to_merchant_id = self.request.get('to_merchant')
        from_merchant = Merchant.get(db.Key.from_path('Merchant', int(from_merchant_id)))
        to_merchant = Merchant.get(db.Key.from_path('Merchant', int(to_merchant_id)))
        if from_merchant and to_merchant:
            # Copy tax classes
            tax_class_map = {}
            for tax_class in TaxClass.all().filter('merchant =', from_merchant):
                new_tax_class = self.clone_entity(tax_class, merchant=to_merchant).put()
                tax_class_map[str(tax_class.key())] = new_tax_class
            # Copy categories
            for category in from_merchant.categories:
                new_category = self.clone_entity(category, merchant=to_merchant).put()
                # Copy products
                for product in category.products:
                    # Update product fields: merchant, category, tax_class. Remove price rules.
                    tax_class_key = Product.tax_class.get_value_for_datastore(product)
                    new_tax_class = tax_class_map[str(tax_class_key)] if tax_class_key else None
                    new_product = self.clone_entity(product, merchant=to_merchant, category=new_category, tax_class=new_tax_class, price_rules=[]).put()
                    # Copy product options
                    for option in product.options:
                        new_option = self.clone_entity(option, product=new_product).put()
            self.add_success(gettext('Merchant menu copied'))
        else:
            self.add_error(gettext('Merchants not found'))
        self.redirect_to('tools')
        
    def clone_entity(self, e, skip_auto_now=False, skip_auto_now_add=False, **extra_args):
      """Clones an entity, adding or overriding constructor attributes.

      The cloned entity will have exactly the same property values as the original
      entity, except where overridden. By default it will have no parent entity or
      key name, unless supplied.

      Args:
        e: The entity to clone
        skip_auto_now: If True then all DateTimeProperty propertes will be skipped which have the 'auto_now' flag set to True
        skip_auto_now_add: If True then all DateTimeProperty propertes will be skipped which have the 'auto_now_add' flag set to True
        extra_args: Keyword arguments to override from the cloned entity and pass
          to the constructor.
      Returns:
        A cloned, possibly modified, copy of entity e.
      """

      klass = e.__class__
      props = {}
      for k, v in klass.properties().iteritems():
        if not (type(v) == db.DateTimeProperty and ((skip_auto_now and getattr(v, 'auto_now')) or (skip_auto_now_add and getattr(v, 'auto_now_add')))):
          if type(v) == db.ReferenceProperty:
            value = getattr(klass, k).get_value_for_datastore(e)
          else:
            value = v.__get__(e, klass)
          props[k] = value
      props.update(extra_args)
      return klass(**props)