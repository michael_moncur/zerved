from views.core import AdminHandler
from models.catalog import PriceRuleSchedule
from google.appengine.ext import db
from google.appengine.api import memcache
from webapp2_extras import i18n
_ = i18n.gettext

class ScheduleListHandler(AdminHandler):
    def get(self):
        schedules = PriceRuleSchedule.all().filter('merchant =', self.merchant)
        self.render('merchants/catalog/schedules.html', schedules=schedules)
        
class ScheduleEditHandler(AdminHandler):
    def get(self, schedule_id=None):
        schedule = None
        if schedule_id:
            schedule = PriceRuleSchedule.get(db.Key.from_path('PriceRuleSchedule', int(schedule_id)))
            if not self.validate_merchant_permission(schedule):
                return
        self.render('merchants/catalog/editschedule.html', schedule=schedule)
        
    def post(self):
        id = self.request.get('_id')
        schedule = None
        #form = ScheduleForm(self.request.POST)
        if (id):
            schedule = PriceRuleSchedule.get(db.Key.from_path('PriceRuleSchedule', int(id)))
            if not self.validate_merchant_permission(schedule):
                return
        else:
            schedule = PriceRuleSchedule(merchant=self.merchant)
            
        data = self.request.POST
        schedule.name = data['name']
        schedule.set_hours_from_checkboxes(data.getall('hours'))
            
        schedule.put()
        
        memcache.delete("pricerules_"+str(self.merchant.key().id()))
        
        link = '<a href="'+self.uri_for('schedule-edit', schedule_id=schedule.key().id())+'">'+schedule.name+'</a>'
        if id or self.request.get('continue_edit'):
            self.add_success(_('%s saved') % link)
        else:
            self.add_success((_('%s is being saved and should be available in the list and app within a few seconds') % link)+' &mdash; <a href="#" onclick="location.reload(); return false;">'+_('refresh list')+'</a>')
        if self.request.get('continue_edit'):
            self.redirect_to('schedule-edit', schedule_id=schedule.key().id())
        else:
            self.redirect_to('schedule-list')
        
class ScheduleDeleteHandler(AdminHandler):
    def post(self, schedule_id):
        if schedule_id:
            schedule = PriceRuleSchedule.get(db.Key.from_path('PriceRuleSchedule', int(schedule_id)))
            if not self.validate_merchant_permission(schedule):
                return
            self.add_success(_('%s is being deleted and should be removed from the list shortly.') % schedule.name)
            schedule.delete()
            memcache.delete("pricerules_"+str(self.merchant.key().id()))
        self.redirect_to('schedule-list')