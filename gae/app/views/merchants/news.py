#!/usr/bin/python
# -*- coding: utf-8 -*-
from models.front.news import News, NewsImage
from models.front import newsforms
from models.merchants import Merchant
from views.core import BaseHandler, AdminHandler
from views.utilities import Pager
from paging import *
from google.appengine.ext import db
from google.appengine.api import mail, images, users
import wtforms
from wtforms import Form, TextField, SelectField, BooleanField, RadioField, FileField, validators
from wtforms.ext.appengine.db import model_form
from views.utilities import RadioListWidget
from models.decimalproperty import *
import datetime
import webapp2
from webapp2_extras import i18n
_ = i18n.lazy_gettext
gettext = i18n.gettext
import logging, re

class NewsListHandler(AdminHandler):
    require_admin = True
    def get(self):
        page = int(self.request.get('page')) if self.request.get('page') else 1
        query = News.all()
        query = PagedQuery(query.order('-date'), 20)
        news = query.fetch_page(page)
        pager = Pager(self, query, page)
        self.render('merchants/admin/news.html', news=news, pager=pager.render())
     
class NewsEditHandler(AdminHandler):
    require_admin = True
    def get(self, news_id=None):
        if news_id:
            news = News.get(db.Key.from_path('News', int(news_id)))
            form = newsforms.get_news_form()(self.request.POST, news)
            self.render('merchants/admin/editnews.html', form=form, news=news, username=News.get_author_name())
        else:
            form = newsforms.get_news_form()()
            self.render('merchants/admin/editnews.html', form=form, username=News.get_author_name())

    def post(self, news_id=None):
        if news_id:
            news = News.get(db.Key.from_path('News', int(news_id)))

        form = newsforms.get_news_form()(self.request.POST)
        if form.validate():
            if news_id and news:
                form.populate_obj(news)           
            else:
                news = News(**form.data)  

            if re.match("^([a-zA-Z0-9-]+)$", news.slug) == None:
                news.slug = re.sub(r'[^a-zA-Z0-9-]', '-', news.slug)
                self.add_error('The unique identifier has been modified automatically to match the required pattern.')

            if not news_id and News.all().filter('slug =', news.slug).count() > 0: # URL already used by another news
                self.add_error(_('The unique identifier is already used, please change it.'))
                self.render('merchants/admin/editnews.html', form=form, username=News.get_author_name())
                return
            
            if self.request.POST["draft"] == "0":
                news.published = True
      
            if self.request.POST["type"] == "merchantnews":
                news.precontent = ""
                news.description = ""
                news.language = "en"
                news.image = None
                news.important = 'important' in self.request.POST and self.request.POST['important'] == 'on' 
                
                # Send email to merchants
                if 'send-email' in self.request.POST and self.request.POST['send-email'] == 'on' and news.published == True:
                    news.send_to_merchants = True
                    if news.important: # Important news, send to ALL merchants
                        merchants = Merchant.all()
                    else:
                        merchants = Merchant.all().filter("receive_news = ", True)
                    for m in merchants:
                        if m.email:
                            logging.info("Sending email to merchant %s (%s)" % (m.name, m.email))
                            message = mail.EmailMessage()
                            message.sender = "Zerved <info@zervedapp.com>"
                            message.subject = 'News from Zerved - ' + self.request.POST["title"]
                            message.to = m.email
                            message.reply_to = 'info@zervedapp.com'
                            message.html = self.jinja2.render_template('emails/merchantnews.html', news=news)
                            message.send() 

            else:
                news.important = False
                news.send_to_merchants = False

                # News image
                image = news.image
                if self.request.get('image'):
                    image = self.request.get('image')

                if 'delete-image' in self.request.POST and self.request.POST['delete-image'] == 'on':
                    image = None

                news.image = db.Blob(image)

            news.put()
            link = '<a href="'+self.uri_for('news-edit', news_id=news.key().id())+'">'+news.title+'</a>'
            if news_id or self.request.get('continue_edit'):
                self.add_success(_('%s saved') % link)
            else:
                self.add_success((_('%s is being saved and should be available in the list and app within a few seconds') % link)+' &mdash; <a href="#" onclick="location.reload(); return false;">'+_('refresh list')+'</a>')
            if self.request.get('continue_edit'):
                self.redirect_to('news-edit', news_id=news.key().id())
            else:
                self.redirect_to('news-list')
        else:
            self.add_error(_('Please correct errors in your input and try to save again.'))
            if news_id and news:
                self.render('merchants/admin/editnews.html', form=form, news=news, username=News.get_author_name())
            else:
                self.render('merchants/admin/editnews.html', form=form, username=News.get_author_name())
        

class NewsDeleteHandler(AdminHandler):
    require_admin = True
    def post(self, news_id):
        if news_id:
            news = News.get(db.Key.from_path('News', int(news_id)))
            self.add_success(_('%s is being deleted and should be removed from the list shortly.') % news.title)
            news.delete()
        self.redirect_to('news-list')


class NewsImageViewerHandler(AdminHandler):
    def get(self, news_id):
        if news_id:
            news = News.get(db.Key.from_path('News', int(news_id)))
            if news.image:
                self.response.headers['Content-Type'] = "image/png"
                self.response.out.write(news.image)
            else:
                self.error(404)

class NewsImageUploadHandler(AdminHandler):
    def get(self, key):
        if key:
            ni = NewsImage.get(key)
            if ni:
                self.response.headers['Content-Type'] = str(ni.mimetype)
                self.response.out.write(ni.data)
            else:
                self.error(404)
        else:
            self.error(404)

    def post(self):
        file = self.request.POST['image']
        image = NewsImage(data=file.value, mimetype=file.type)
        image.put()
        self.response.headers['Content-Type'] = "application/json"
        self.response.out.write('{"upload" : { "id" : "' + str(image.key().id()) + '", "key" : "' + str(image.key()) + '", "src" : "/merchants/news/image/view/' + str(image.key())  + '", "filetype" : "' + str(file.type) + '"}}')
