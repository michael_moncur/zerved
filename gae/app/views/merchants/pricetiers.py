from views.core import AdminHandler
from models.catalog import *
from models import i18ndata
from models.merchants import MerchantInvoice
import logging
import re


class PriceTiersViewHandler(AdminHandler): 
    """ Displays the pricing model to the merchant """
    def get(self):
        pricetier = self.merchant.get_pricetier()

        self.render("merchants/admin/pricetiers.html", pricetier=pricetier, negociated=not self.merchant.fee_rate_use_default, currency=self.merchant.currency) 


class PriceTiersEditHandler(AdminHandler):
    require_admin = True
    def get(self):
        currencies = i18ndata.ALLOWED_CURRENCIES
        currency = "DKK"     
        if self.request.get("currency") in currencies:   
            currency = self.request.get("currency")
        
        pricetier_k = db.Key.from_path('PriceTier', currency)
        pricetier = db.get(pricetier_k)

        self.render("merchants/admin/editpricetiers.html", pricetier=pricetier, currencies=currencies, current_currency=currency)

    def post(self): 

        currency = self.request.POST['currency']
        pricetier = PriceTier.get_or_insert(currency)
        pricetier.currency = currency

        min_values = []
        for min_value in self.request.POST.getall("min_values[]"):
            if re.match("^\d+\.?\d*$", min_value) == None:
                self.add_error('Incorrect format.')
                self.redirect('/merchants/pricetiers/edit?currency=' + currency)
                return
            min_values.append(Decimal(min_value))

        fees = []
        for fee in self.request.POST.getall("fees[]"):
            if re.match("^\d+\.?\d*$", fee) == None:
                self.add_error('Incorrect format.')
                self.redirect('/merchants/pricetiers/edit?currency=' + currency)
                return
            fees.append(Decimal(fee))

        flat_fee_min_values = []
        for flat_fee_min_value in self.request.POST.getall("flat_fee_min_values[]"):
            if re.match("^\d+\.?\d*$", flat_fee_min_value) == None:
                self.add_error('Incorrect format.')
                self.redirect('/merchants/pricetiers/edit?currency=' + currency)
                return
            flat_fee_min_values.append(Decimal(flat_fee_min_value))

        flat_fees = []
        for flat_fee in self.request.POST.getall("flat_fees[]"):
            if re.match("^\d+\.?\d*$", flat_fee) == None:
                self.add_error('Incorrect format.')
                self.redirect('/merchants/pricetiers/edit?currency=' + currency)
                return
            flat_fees.append(Decimal(flat_fee))

        discount_min_values = []
        for min_value in self.request.POST.getall("discount_min_values[]"):
            if re.match("^\d+$", min_value) == None:
                self.add_error('Incorrect format.')
                self.redirect('/merchants/pricetiers/edit?currency=' + currency)
                return
            discount_min_values.append(int(min_value))

        discount_percentages = []
        for discount_percentage in self.request.POST.getall("discount_percentages[]"):
            if re.match("^\d+\.?\d*$", discount_percentage) == None:
                self.add_error('Incorrect format.')
                self.redirect('/merchants/pricetiers/edit?currency=' + currency)
                return
            discount_percentages.append(Decimal(discount_percentage))

        if pricetier == None:
            pricetier = PriceTier()
            pricetier.currency = currency

        pricetier.set_list('min_value', min_values)
        pricetier.set_list('fee', fees)
        pricetier.set_list('flat_fee_min_value', flat_fee_min_values)
        pricetier.set_list('flat_fee', flat_fees)
        pricetier.set_list('discount_min_value', discount_min_values)
        pricetier.set_list('discount_percentage', discount_percentages)
        pricetier.fixed_fee = Decimal(self.request.POST.get("fixed_fee"), 0.00)

        pricetier.put() 
        self.add_success('Successfully saved')
        self.redirect('/merchants/pricetiers/edit?currency=' + currency)

def init_default_price_tiers():
    """
        Insert default price tiers for each currency
        Note: USD's default price tiers are mostly 0's (not defined)
    """
    
    if not PriceTier.get_by_key_name("GBP"):
        pt = PriceTier(key_name="GBP")
        pt.currency = "GBP"
        pt.set_list('min_value', [Decimal("0.00"), Decimal("10.00"), Decimal("25.00"), Decimal("100.00")])
        pt.set_list('fee', [Decimal("12.50"), Decimal("12.00"), Decimal("11.50"), Decimal("11.00")])
        pt.set_list('discount_min_value', [0, 25, 100, 200, 500])
        pt.set_list('discount_percentage', [Decimal("0.00"), Decimal("10.00"), Decimal("15.00"), Decimal("20.00"), Decimal("25.00")])
        pt.set_list('flat_fee_min_value', [0])
        pt.set_list('flat_fee', [Decimal("0.00")])
        pt.fixed_fee = Decimal('0')
        pt.put() 

    if not PriceTier.get_by_key_name("EUR"):
        pt = PriceTier(key_name="EUR")
        pt.currency = "EUR"
        pt.set_list('min_value', [Decimal("0.00"), Decimal("12.00"), Decimal("30.00"), Decimal("120.00")])
        pt.set_list('fee', [Decimal("12.50"), Decimal("12.00"), Decimal("11.50"), Decimal("11.00")])
        pt.set_list('discount_min_value', [0, 25, 100, 200, 500])
        pt.set_list('discount_percentage', [Decimal("0.00"), Decimal("10.00"), Decimal("15.00"), Decimal("20.00"), Decimal("25.00")])
        pt.set_list('flat_fee_min_value', [0])
        pt.set_list('flat_fee', [Decimal("0.00")])
        pt.fixed_fee = Decimal('0')
        pt.put() 

    if not PriceTier.get_by_key_name("DKK"):
        pt = PriceTier(key_name="DKK")
        pt.currency = "DKK"
        pt.set_list('min_value', [Decimal("0.00"), Decimal("90.00"), Decimal("225.00"), Decimal("885.00")])
        pt.set_list('fee', [Decimal("12.50"), Decimal("12.00"), Decimal("11.50"), Decimal("11.00")])
        pt.set_list('discount_min_value', [0, 25, 100, 200, 500])
        pt.set_list('discount_percentage', [Decimal("0.00"), Decimal("10.00"), Decimal("15.00"), Decimal("20.00"), Decimal("25.00")])
        pt.set_list('flat_fee_min_value', [0])
        pt.set_list('flat_fee', [Decimal("0.00")])
        pt.fixed_fee = Decimal('0')
        pt.put() 

    if not PriceTier.get_by_key_name("ZAR"):
        pt = PriceTier(key_name="ZAR")
        pt.currency = "ZAR"
        pt.set_list('min_value', [Decimal("0.00"), Decimal("155.00"), Decimal("390.00"), Decimal("1560.00")])
        pt.set_list('fee', [Decimal("12.50"), Decimal("12.00"), Decimal("11.50"), Decimal("11.00")])
        pt.set_list('discount_min_value', [0, 25, 100, 200, 500])
        pt.set_list('discount_percentage', [Decimal("0.00"), Decimal("10.00"), Decimal("15.00"), Decimal("20.00"), Decimal("25.00")])
        pt.set_list('flat_fee_min_value', [0])
        pt.set_list('flat_fee', [Decimal("0.00")])
        pt.fixed_fee = Decimal('0')
        pt.put() 

    if not PriceTier.get_by_key_name("USD"):
        pt = PriceTier(key_name="USD")
        pt.currency = "USD"
        pt.set_list('min_value', [Decimal("0.00"), Decimal("0.00"), Decimal("0.00"), Decimal("0.00")])
        pt.set_list('fee', [Decimal("0.00"), Decimal("0.00"), Decimal("0.00"), Decimal("0.00")])
        pt.set_list('discount_min_value', [0, 25, 100, 200, 500])
        pt.set_list('discount_percentage', [Decimal("0.00"), Decimal("10.00"), Decimal("15.00"), Decimal("20.00"), Decimal("25.00")])
        pt.set_list('flat_fee_min_value', [0])
        pt.set_list('flat_fee', [Decimal("0.00")])
        pt.fixed_fee = Decimal('0')
        pt.put() 

class PriceTiersInitHandler(AdminHandler):
    require_admin = True
    def get(self):
        init_default_price_tiers()
