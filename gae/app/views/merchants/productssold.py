from views.core import AdminHandler
from models.productssold import ProductsSoldReport
import datetime
import pytz
import logging

class ProductsSoldHandler(AdminHandler):
    def get(self):
        report = None
        place = self.request.get('place') if self.request.get('place') else None
        (place_type, _, place_id) = place.partition("-") if place else (None, None, None)
        location = place_id if place_type == 'loc' else None
        bar = place_id if place_type == 'bar' else None
        # If parameters submitted, generate report
        if self.request.get('startdate') and self.request.get('timesplit'):
            startdate = pytz.timezone(self.merchant.timezone).localize(datetime.datetime.strptime(self.request.get('startdate') + ' ' + self.request.get('timesplit'), "%Y-%m-%d %H:%M")).astimezone(pytz.utc)
            enddate = pytz.timezone(self.merchant.timezone).localize(datetime.datetime.strptime(self.request.get('enddate') + ' ' + self.request.get('timesplit'), "%Y-%m-%d %H:%M")).astimezone(pytz.utc)
            # enddate = startdate + datetime.timedelta(days=1)
            report = ProductsSoldReport(self.merchant, location, bar, 'complete', startdate, enddate)
        # Default values: Start yesterday, end today
        startdate = self.request.get('startdate') if self.request.get('startdate') else (datetime.datetime.now()-datetime.timedelta(days=1)).strftime("%Y-%m-%d")
        enddate = self.request.get('enddate') if self.request.get('enddate') else (datetime.datetime.now()).strftime("%Y-%m-%d")
        if self.request.get('csv') and self.request.get('csv') == '1':
            # CSV report
            self.response.headers.add('Content-Disposition', 'attachment; filename="report.csv"')
            self.response.content_type = 'application/octet-stream'
            self.render('merchants/csv/productssold.csv', report=report, startdate=startdate, enddate=enddate, timesplit=self.request.get('timesplit'), location=location, bar=bar)
        else:
            # HTML report
            self.render('merchants/sales/productssold.html', report=report, startdate=startdate, enddate=enddate, timesplit=self.request.get('timesplit'), location=location, bar=bar, csvlink=self.request.url+"&csv=1")

class ProductsOpenHandler(AdminHandler):
    def get(self):
        report = None
        place = self.request.get('place') if self.request.get('place') else None
        (place_type, _, place_id) = place.partition("-") if place else (None, None, None)
        location = place_id if place_type == 'loc' else None
        bar = place_id if place_type == 'bar' else None
        if 'place' in self.request.GET:
            report = ProductsSoldReport(self.merchant, location, bar, ['pending', 'preparing'])
        if self.request.get('csv') and self.request.get('csv') == '1':
            # CSV report
            self.response.headers.add('Content-Disposition', 'attachment; filename="report.csv"')
            self.response.content_type = 'application/octet-stream'
            self.render('merchants/csv/productssold.csv', report=report, location=location, bar=bar, csvlink=self.request.url+"&csv=1")
        else:
            # HTML report
            self.render('merchants/sales/productsopen.html', report=report, location=location, bar=bar, csvlink=self.request.url+"&csv=1")