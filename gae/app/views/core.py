#!/usr/bin/python
# -*- coding: utf-8 -*-
import webapp2
from webapp2_extras import sessions
from webapp2_extras import sessions_memcache
import os
from google.appengine.ext import db
os.environ['DJANGO_SETTINGS_MODULE'] = 'app.settings'
from uuid import uuid4
from google.appengine.api import users, memcache
from webapp2_extras import i18n, jinja2
import re
import json
from geo.geomodel import geomath
import datetime
import collections
import logging
from models import i18ndata
from babel import Locale

def jinja2_factory(*args, **kwargs):
    jinja_config={
        'environment_args': {
            'extensions': [
                'jinja2.ext.i18n'
            ]
        },
        # Global template functions
        'globals': {
            'uri_for': webapp2.uri_for,
            'format_currency': i18n.format_currency,
            'format_date': i18n.format_date,
            'format_datetime': i18n.format_datetime,
            'format_number': i18n.format_number,
            'format_decimal': i18n.format_decimal,
            'geo_distance': geomath.distance,
        },
        'filters': {
            'tojson': json.dumps,
            'quoted_json': (lambda x: json.dumps(x).replace('"', '&quot;')),
            'str': str,
            'replace_subdomain': (lambda host, newsub: newsub+host[host.find("."):] if host.count(".") > 1 else newsub+"."+host),
        }
    }
    return jinja2.Jinja2(*args, config=jinja_config, **kwargs)

class BaseHandler(webapp2.RequestHandler):
    csrf_protect = True
    require_login = False
    require_admin = False
    
    @webapp2.cached_property
    def jinja2(self):
        # Returns a Jinja2 renderer cached in the app registry.
        return jinja2.get_jinja2(factory=jinja2_factory, app=self.app)
    
    def initialize(self, request, response):    
        self.init_locale(request, response)
        
        """General initialization for every request"""
        super(BaseHandler, self).initialize(request, response)
        
        self.init_csrf()
        if self.require_admin and not users.is_current_user_admin():
            self.redirect_to('admin-restricted', _abort=True)
            return
        if self.require_login:
            self.check_login()
        
    def set_cookie(self, name, value, expires=None):
        """Set a cookie, or delete cookie if value is None"""
        if value is None:
            self.response.delete_cookie(name)
        else:
            self.response.set_cookie(name, value, None, '/', None, None, False, None, expires)
        
    def init_csrf(self):
        """Issue and handle CSRF token as necessary"""
        self.csrf_token = self.request.cookies.get(u'c')
        if not self.csrf_token:
            self.csrf_token = str(uuid4())[:8]
            self.set_cookie('c', self.csrf_token)
        if self.request.method == u'POST' and self.csrf_protect and \
                self.csrf_token != self.request.POST.get(u'_csrf_token'):
            raise CsrfException(u'Missing or invalid CSRF token.')
            
    def render(self, name, **data):
        """Render a template"""
        if not data:
            data = {}
        data[u'csrf_token'] = self.csrf_token
        data['users'] = users
        data['current_locale'] = i18n.get_i18n().locale
        data['current_language'] = i18n.get_i18n().locale.split('_')[0]
        # Language codes and language names
        data['available_languages'] = collections.OrderedDict([(lang, Locale(lang).languages[lang].title()) for lang in i18ndata.LANGUAGE_LOCALES.keys()])
        rv = self.jinja2.render_template(name, **data)
        self.response.write(rv)
        
    def check_login(self):
        """Authorize user for merchant"""
        self.merchant = None
        merchant_key = self.request.cookies.get(u'm')
        user = users.get_current_user()
        if (user):
            if users.is_current_user_admin():
                # Administrator: Allow all merchants and remember current merchant in session
                if merchant_key:
                    self.merchant = Merchant.get(merchant_key)
                else:
                    self.merchant = Merchant.all().get()
                    if self.merchant:
                        self.set_cookie('m', str(self.merchant.key()))
            else:
                # Find merchant for user
                if merchant_key:
                    # Load from cookie and check permission
                    merchant = Merchant.get(merchant_key)
                    if merchant and user.email().lower() in merchant.users:
                        self.merchant = merchant
                        # Extend cookie
                        self.set_cookie('m', str(self.merchant.key()), expires=(datetime.datetime.now() + datetime.timedelta(days=7)))
                if not self.merchant:
                    # Find associated accounts
                    user_merchants = Merchant.all(keys_only=True).filter('users =', user.email().lower())
                    if user_merchants.count() == 0:
                        self.redirect_to('user-unknown', False, True)
                    elif user_merchants.count() == 1:
                        logging.info(user_merchants[0])
                        self.merchant = Merchant.get(user_merchants[0])
                        self.set_cookie('m', str(self.merchant.key()), expires=(datetime.datetime.now() + datetime.timedelta(days=7)))
                    else:
                        self.redirect_to('user-merchants', False, True)
            if self.merchant and self.merchant.timezone:
                i18n.get_i18n().set_timezone(self.merchant.timezone)
        else:
            self.redirect(users.create_login_url(self.request.uri), False, True)
            
    def init_locale(self, request, response):
        # first, try and set locale from cookie
        locale = request.cookies.get('locale')
        if locale in i18ndata.ALLOWED_LOCALES:
            i18n.get_i18n().set_locale(locale)
        else:
            # Find locale in accept-language header
            header = request.headers.get('Accept-Language', '')
            locales = [locale.split(';')[0] for locale in header.split(',')]
            languages = [locale.split('-')[0] for locale in locales]
            for locale in locales:
                language = locale.split('-')[0]
                # Check for matching locale
                if locale in i18ndata.ALLOWED_LOCALES:
                    i18n.get_i18n().set_locale(locale)
                    break
                # Check for matching language (with different country)
                elif language in i18ndata.LANGUAGE_LOCALES:
                    i18n.get_i18n().set_locale(i18ndata.LANGUAGE_LOCALES[language])
                    break
            else:
                # if still no locale set, use the first available one
                i18n.get_i18n().set_locale('en_GB')

    def get_locale_object(self):
        locale = i18n.get_i18n().locale
        (lang, country) = locale.split('_')
        return Locale(lang, country)

    def get_user_prefs(self):
        user = users.get_current_user()

        cachekey = "user_prefs" + user.user_id()
        user_prefs = memcache.get(cachekey)
        if user_prefs is None:
            user_prefs = UserPrefs.get_or_insert(user.user_id())
            memcache.add(cachekey, user_prefs, 60*60*24)
            logging.debug("saved userprefs to memcache")
        return user_prefs

    def get_user_id(self):
        return users.get_current_user().user_id()

class CsrfException(Exception):
    pass
    
class AdminHandler(BaseHandler):
    require_login = True
    
    def initialize(self, request, response):
        """General initialization for every request"""
        super(AdminHandler, self).initialize(request, response)
        
        # If lang parameter is set, switch language
        if request.GET.get('lang') and str(request.GET.get('lang')):
            lang = str(request.GET.get('lang'))
            if lang in i18ndata.ALLOWED_LOCALES:
                self.set_cookie('locale', lang)
                i18n.get_i18n().set_locale(lang)
            elif lang in i18ndata.LANGUAGE_LOCALES:
                self.set_cookie('locale', i18ndata.LANGUAGE_LOCALES[lang])
                i18n.get_i18n().set_locale(i18ndata.LANGUAGE_LOCALES[lang])
            
    def dispatch(self):
        # Get a session store for this request.
        self.session_store = sessions.get_store(request=self.request)

        try:
            # Dispatch the request.
            webapp2.RequestHandler.dispatch(self)
        finally:
            # Save all sessions.
            self.session_store.save_sessions(self.response)
            
    @webapp2.cached_property
    def session(self):
        # Returns a session using the default cookie key.
        return self.session_store.get_session(factory=sessions_memcache.MemcacheSessionFactory)
    
    def render(self, name, **data):
        if not data:
            data = {}
        data['active_module'] = self.get_active_module()
        data['user_merchant'] = self.merchant
        data['user_prefs'] = self.get_user_prefs()
        data['flashes'] = self.get_flashes()
        super(AdminHandler, self).render(name, **data)
        
    def get_active_module(self):
        handler_name = self.request.route.name
        if (handler_name):
            return handler_name.partition('-')[0]
        else:
            return ''
            
    def validate_merchant_permission(self, object):
        # Validate object owned by merchant
        if not self.merchant.validate_permission(object):
            self.render('merchants/error.html', message='Current merchant does not have access to this object')
            return False
        return True
        
    def get_flashes(self, key='_flash'):
        return self.session.get_flashes(key)
        
    def add_flash(self, value, level=None, key='_flash'):
        self.session.add_flash(value, level, key)
        
    def add_success(self, value):
        self.add_flash(value, 'success')
        
    def add_error(self, value):
        self.add_flash(value, 'error')
        
    def add_info(self, value):
        self.add_flash(value, 'info')

class RemoveSubdomainHandler(webapp2.RequestHandler):
    def get(self, subdomain):
        logging.info('redirect from subdomain')
        scheme = self.request.scheme + "://"
        url = self.request.url.replace(scheme+subdomain+".", scheme)
        self.redirect(uri=url, permanent=True, abort=True)

from google.appengine.api import images
class ImageHandler(BaseHandler):
    def get(self, entity_key, width, height):
        entity = db.Model.get(entity_key)
        resizedimage = entity.image
        try:
            image = images.Image(entity.image)
            image.resize(width=int(width), height=int(height))
            image = images.Image(image.execute_transforms(output_encoding=images.JPEG))
            offsetx = (int(width)-image.width)/2.0
            offsety = (int(height)-image.height)/2.0
            resizedimage = images.composite([(image, int(offsetx), int(offsety), 1.0, images.TOP_LEFT)], int(width), int(height), color=0xFFFFFFFF, output_encoding=images.JPEG)
        except NotImplementedError:
            pass
        except images.NotImageError:
            # Prevent internal server errors when testing on localhost
            pass
        self.response.headers['Content-Type'] = str(entity.image_type)
        self.response.out.write(resizedimage)
        
class AppHandler(BaseHandler):
    def dispatch(self):
        # Get a session store for this request.
        self.session_store = sessions.get_store(request=self.request)
        self.cart = Cart(self.session.get('cart'))
        
        try:
            # Dispatch the request.
            webapp2.RequestHandler.dispatch(self)
        finally:
            # Save all sessions.
            self.session['cart'] = self.cart.cartdata
            self.session_store.save_sessions(self.response)
            
    @webapp2.cached_property
    def session(self):
        # Returns a session using the default cookie key.
        return self.session_store.get_session(factory=sessions_memcache.MemcacheSessionFactory)
    
class ConsumerHandler(AppHandler):
    csrf_protect = False
    
    def dispatch(self):
        # Verify consumer account and password
        access_token = self.request.get('access_token')
        #consumer_key = self.request.get('consumer_key')
        #password = self.request.get('password')
        #if not consumer_key or not password:
        if not access_token:
            self.render('iphone/consumererror.html')
            return
        #self.consumer = Consumer.get(self.request.get('consumer_key'))
        self.consumer = Consumer.authenticate(access_token)
        #if (not self.consumer or not self.consumer.verify_password(password)):
        if not self.consumer:
            self.render('iphone/consumererror.html')
            return
        super(ConsumerHandler, self).dispatch()
        
class UpdateIphoneHandler(AppHandler):
    def get(self, *args):
        self.render('iphone/updateerror.html')
        
    def post(self, *args):
        self.get(*args)




from models.merchants import Merchant, UserPrefs
from models.consumers import Consumer
from models.cart import Cart