import wtforms
from google.appengine.ext import db

class Pager:
    template = 'merchants/pager.html'
    
    def __init__(self, handler, query, page, alignment='centered'):
        self.handler = handler
        self.query = query
        self.page = page
        self.alignment = alignment
        
    def calculate_page_range(self):
        pagecount = self.query.page_count()
        if (self.page < 3):
            startpage = 1
            endpage = min(5, pagecount)
        elif (self.page > pagecount - 2):
            endpage = pagecount
            startpage = max(pagecount - 4, 1)
        else:
            startpage = max(self.page - 2, 1)
            endpage = min(startpage + 4, pagecount)
        return range(startpage, endpage+1)
        
    def render(self):
        pages = self.calculate_page_range()
        return self.handler.jinja2.render_template(self.template, query=self.query, page=self.page, pages=pages, alignment=self.alignment)
            
class CheckboxListWidget(wtforms.widgets.ListWidget):
    def __call__(self, field, **kwargs):
        kwargs.setdefault('id', field.id)
        html = [u'<%s %s>' % (self.html_tag, wtforms.widgets.html_params(**kwargs))]
        for subfield in field:
            html.append(u'<li><label class="checkbox">%s %s</li></label>' % (subfield(), subfield.label))
        html.append(u'</%s>' % self.html_tag)
        return wtforms.widgets.HTMLString(u''.join(html))
        
class RadioListWidget(wtforms.widgets.ListWidget):
    def __call__(self, field, **kwargs):
        kwargs.setdefault('id', field.id)
        html = [u'<%s %s>' % (self.html_tag, wtforms.widgets.html_params(**kwargs))]
        for subfield in field:
            html.append(u'<li><label class="radio">%s %s</li></label>' % (subfield(), subfield.label))
        html.append(u'</%s>' % self.html_tag)
        return wtforms.widgets.HTMLString(u''.join(html))
        
class MultiCheckboxField(wtforms.SelectMultipleField):
    """
    A multiple-select, except displays a list of checkboxes.

    Iterating the field will produce subfields, allowing custom rendering of
    the enclosed checkbox fields.
    """
    widget = CheckboxListWidget()
    option_widget = wtforms.widgets.CheckboxInput()
    
    def process_formdata(self, valuelist):
        try:
            self.data = list(db.Key(x) for x in valuelist)
        except ValueError:
            raise ValueError(self.gettext(u'Invalid choice(s): one or more data inputs could not be coerced'))