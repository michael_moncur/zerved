import facebook
from views.core import BaseHandler
from webapp2_extras import i18n, jinja2, users
from google.appengine.ext import db
import random
import logging

FACEBOOK_APP_ID = "458661650869137"
FACEBOOK_APP_SECRET = "ebe0b027099fbbd127a011b68a2733d7"

class FacebookContestApril(db.Model):
    pass

class FacebookContestHandler(BaseHandler):
    csrf_protect = False
    
    def post(self):
        if self.request.get('email') and self.request.get('answer'):
            if self.request.get('answer') == '1':
                FacebookContestApril.get_or_insert(self.request.get('email'))
            self.render('front/fb-contest-april.html', liked=True, participated=True, shared=False, scheme=self.request.scheme)
        elif self.request.get('signed_request'):
            signed_request = facebook.parse_signed_request(self.request.get('signed_request'), FACEBOOK_APP_SECRET)
            liked=signed_request['page']['liked']
            self.render('front/fb-contest-april.html', liked=liked, participated=False, shared=False, scheme=self.request.scheme)
            
class FacebookContestSharedHandler(BaseHandler):
    csrf_protect = False
    
    def post(self):
        self.get()
        
    def get(self):
        self.render('front/fb-contest-april.html', liked=True, participated=True, shared=True, scheme=self.request.scheme)
        
class FacebookContestDrawRandomHandler(BaseHandler):
    @users.admin_required
    def get(self):
        participants = FacebookContestApril.all().count(limit=1000000)
        winner = FacebookContestApril.all().get(offset=random.randint(0,participants-1))
        self.response.write("Random participant from all %s participants: %s" % (participants, winner.key().name()))