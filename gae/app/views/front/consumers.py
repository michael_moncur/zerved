from main import MainHandler
from models.consumers import Consumer, EmailConsumer

class ResetPasswordHandler(MainHandler):
    require_login = False
    
    def get(self, token):
        emailconsumer = EmailConsumer.get_from_password_token(token)
        consumer = emailconsumer.consumer if emailconsumer else None
        self.render('front/consumers/resetpassword.html', path="", token=token, consumer=consumer)
        
    def post(self, token):
        password = self.request.get('password')
        emailconsumer = EmailConsumer.get_from_password_token(token)
        consumer = emailconsumer.consumer if emailconsumer else None
        if consumer:
            consumer.set_password(password)
            consumer.put()
        # If a phone thief changes the email address and then uses the forgot password page, he can gain access to the account, so we'll delete the credit card
        if consumer.current_payment_subscription:
            consumer.current_payment_subscription.delete()
        self.render('front/consumers/resetpassword/done.html', path="", consumer=consumer)