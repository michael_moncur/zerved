import datetime
from webapp2 import uri_for
from views.core import BaseHandler
from webapp2_extras import i18n, jinja2
from google.appengine.api import mail
from google.appengine.ext import db
from models.front.news import News
from models.merchants import Merchant
import google.appengine.api.namespace_manager
import logging
import re
from models import i18ndata

class FrontHandler(BaseHandler):
    def init_locale(self, request, response):
        domainparts = re.split('\.|-dot-', request.host)
        # domainparts = request.host.split(".")
        locale = domainparts[0]
        if len(domainparts) > 1 and locale in i18ndata.ALLOWED_LOCALES:
            # Set locale from subdomain
            i18n.get_i18n().set_locale(locale)
        elif len(domainparts) > 1 and locale in i18ndata.LANGUAGE_LOCALES:
            # Language subdomains
            if locale == 'da':
                # Redirect da to www
                scheme = request.scheme + "://"
                url = request.url.replace(scheme+"da.", scheme+"www.").replace(scheme+"da-dot-", scheme+"www-dot-")
                self.redirect(uri=url, permanent=True, abort=True)
            else:
                # Set locale from language subdomain
                i18n.get_i18n().set_locale(i18ndata.LANGUAGE_LOCALES[locale])
        elif request.get('lang') and request.get('lang') in i18ndata.ALLOWED_LOCALES:
            # Set locale from query string locale
            i18n.get_i18n().set_locale(request.get('lang'))
        elif request.get('lang') and request.get('lang') in i18ndata.LANGUAGE_LOCALES:
            # Set locale from query string language
            i18n.get_i18n().set_locale(i18ndata.LANGUAGE_LOCALES[request.get('lang')])
        else:
            # No language subdomain, use danish
            i18n.get_i18n().set_locale('da_DK')
            
    def render(self, name, **data):
        if not data:
            data = {}
        data['hostname'] = self.request.host

        data['fb_link'] = 'https://www.facebook.com/ZervedUK'
        data['tw_link'] = 'https://twitter.com/ZervedUK' 
        
        if i18n.get_i18n().locale == 'da':
            data['fb_link'] = 'https://www.facebook.com/Zerved'
            data['tw_link'] = 'https://twitter.com/zervedapp'
        elif i18n.get_i18n().locale == 'es':
            data['fb_link'] = 'https://www.facebook.com/ZervedEspana'
            data['tw_link'] = 'https://twitter.com/ZervedUK'

        super(FrontHandler, self).render(name, **data)

class MainHandler(FrontHandler):
    csrf_protect = False
    require_login = False
    
    def get(self, template):
        images = []
        """ Render HTML template with name as in given in the URL """
        if (template == ''):
            template = 'index'
            images = []
            for m in Merchant.all():
                if m.image:
                    images.append("/image/%s/120/120" % str(m.key()))
        try:
            self.render('front/'+i18n.get_i18n().locale.split('_')[0]+'/'+template+'.html', path=template, scheme=self.request.scheme, images=images)
        except jinja2.jinja2.TemplateNotFound:
            self.abort(404)
    
    def post(self, template):
        self.get(template)

class ContactFormHandler(FrontHandler):
    def post(self):
        """ Send contact form email """
        message = mail.EmailMessage()
        message.sender = "Zerved <info@zervedapp.com>"
        message.subject = self.request.get("subject") + ' - ' + self.request.get("sender_segment")
        message.to = "contactform@zervedapp.com"
        message.reply_to = self.request.get("email")
        message.body = self.jinja2.render_template('emails/contact.txt', form=self.request.POST)
        message.send()
        self.redirect('/contact/sent')    

class SignUpFormHandler(FrontHandler):
    def post(self):
        message = mail.EmailMessage()
        message.sender = "Zerved <info@zervedapp.com>"
        message.subject = "Sign up request"
        message.to = "contactform@zervedapp.com"
        message.reply_to = self.request.get("email")
        message.body = self.jinja2.render_template('emails/signup.txt', form=self.request.POST)
        message.send()
        self.redirect('/contact/sent')    


class RecommendFormHandler(FrontHandler):
    def post(self):
        """ Send recommendation form email """
        message = mail.EmailMessage()
        message.sender = "Zerved <info@zervedapp.com>"
        message.subject = "Recommend a Venue"
        message.to = "contactform@zervedapp.com"
        message.reply_to = self.request.get("customer_email")
        message.body = self.jinja2.render_template('emails/recommend.txt', form=self.request.POST)
        message.send()
        self.redirect('/recommend/sent?lang='+i18n.get_i18n().locale)

class RecommendSentHandler(FrontHandler):
    def get(self):
        self.render('front/recommend/sent.html', scheme=self.request.scheme)
        
class RobotsTxtHandler(FrontHandler):
    def get(self):
        """ Return a robots.txt with index OK for the production server, and no indexing for other servers """
        namespace = google.appengine.api.namespace_manager.get_namespace()
        filename = "robots.dev.txt" if namespace == "dev" or namespace == "demo" else "robots.prod.txt"
        self.response.content_type = 'text/plain'
        self.render('front/'+filename)

class NewsListHandler(FrontHandler):
    def get(self):
        """ List of news """
        locale = i18n.get_i18n().locale
        (lang, country) = locale.split('_')
        news_list = News.all().filter('language = ', lang).order("-date").filter('published = ', True).filter("type = ", "news")
        self.render('front/news.html', news_list=news_list, path="news")

class NewsViewHandler(FrontHandler):
    def get(self, slug):
        """ News article """
        if slug:
            news = News.all().filter("slug = ", slug).filter('published = ', True).filter("type = ", "news")[0]
            self.render('front/news-view.html', news=news, path="news")
        else:
            self.error(404)

class NewsImageViewerHandler(FrontHandler):
    def get(self, news_id):
        if news_id:
            news = News.get(db.Key.from_path('News', int(news_id)))
            if news.image:
                self.response.headers['Content-Type'] = "image/png"
                self.response.out.write(news.image)
            else:
                self.error(404)
