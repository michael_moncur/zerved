from views.core import AppHandler, ConsumerHandler
from google.appengine.ext import db
from models.consumers import Consumer, EmailConsumer, ApnsToken
from models.payment import PaymentSubscription
from models.merchants import Merchant
import json
import logging

class SaveDeviceTokenHandler(ConsumerHandler):
    def post(self):
        device_token = self.request.get('device_token')
        sandbox = self.request.get('sandbox') and self.request.get('sandbox') == '1'
        self.consumer.device_token = device_token
        self.consumer.apns_sandbox = sandbox
        self.consumer.put()
        apnstoken = ApnsToken.get_or_insert(device_token)
        apnstoken.consumer = self.consumer
        apnstoken.enabled = True
        apnstoken.put()
        
class LoginInfoHandler(AppHandler):
    def get(self):
        self.render('iphone/logininfo.html')