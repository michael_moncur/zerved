from views.core import AppHandler
from models.merchants import Location, Bar
from models.catalog import Category
from models.cart import Cart
from google.appengine.ext import db

class CategoryListLegacyHandler(AppHandler):
    def get(self, bar_id, group):
        bar = Bar.get(db.Key.from_path('Bar', int(bar_id)))
        merchant = bar.merchant
        categories = Category.all().filter('merchant =', merchant).filter('group =', group).filter('enabled =', True).order('sort_order')
        cart_id = bar_id+'_'+group
        self.render('iphone/legacy/categories.html', merchant=merchant, bar=bar, categories=categories, count_items=self.cart.count_items(cart_id), group=group)
        
class LocationCategoryListHandler(AppHandler):
    def get(self, location_id, group):
        if group == 'menu':
            bars = Bar.all().filter('location =', db.Key.from_path('Location', int(location_id))).filter('enabled =', True)
            if bars.count() == 1:
                self.redirect_to('bar-category-list', bar_id=bars[0].key().id())
            else:
                self.redirect_to('bar-list', location_id=location_id)
        else:
            location = Location.get(db.Key.from_path('Location', int(location_id)))
            categories = Category.all().filter('merchant =', location.merchant).filter('group =', group).filter('enabled =', True).order('sort_order')
            categories = [c for c in categories if c.has_products(location)]
            cart_id = location_id+'_'+group
            self.render('iphone/categories.html', place_id=location.key().id(), place_key=location.key(), categories=categories, count_items=self.cart.count_items(cart_id), group=group)
            
class BarCategoryListHandler(AppHandler):
    def get(self, bar_id):
        bar = Bar.get(db.Key.from_path('Bar', int(bar_id)))
        categories = Category.all().filter('merchant =', bar.merchant).filter('group =', 'menu').filter('enabled =', True).order('sort_order')
        categories = [c for c in categories if c.has_products(bar)]
        cart_id = bar_id+'_menu'
        self.render('iphone/categories.html', place_id=bar.key().id(), place_key=bar.key(), categories=categories, count_items=self.cart.count_items(cart_id), group='menu')
        