from views.core import AppHandler
from models.merchants import Merchant, Location, Bar
from models.catalog import Product, Category, Menu
from models.cart import Cart
from google.appengine.ext import db
from google.appengine.api import memcache
from webapp2_extras import i18n
_ = i18n.lazy_gettext

class ProductListLegacyHandler(AppHandler):
    def get(self, bar_id, category_id):
        bar = Bar.get(db.Key.from_path('Bar', int(bar_id)))
        merchant = bar.merchant
        category = Category.get(db.Key.from_path('Category', int(category_id)))
        products = merchant.products.filter('category = ', category)
        if category.group == 'menu':
            products.filter('enabled_bars =', bar.key())
        else:
            products.filter('enabled_locations =', bar.location.key())
        products.order('sort_order')
        cart_id = bar_id+'_'+category.group
        self.render('iphone/legacy/products.html', category=category, products=products, merchant=merchant, bar=bar, count_items=self.cart.count_items(cart_id))

class ProductListHandler(AppHandler):
    def get(self, category_id, location_id=None, bar_id=None):
        # Load category
        try:
            category = Category.get(db.Key.from_path('Category', int(category_id)))
        except Exception:
            self.abort(404)

        # Load product list from cache
        if category.group == "menu":
            bar = Bar.get(db.Key.from_path('Bar', int(bar_id)))
            products = category.get_cached_products(Menu.get_active_menus(bar).fetch(1)[0].key())
        else:
            products = category.get_cached_products(db.Key.from_path('Location', int(location_id)))

        # Load location/bar info
        message_when_closed = _("Closed for orders")
        if category.group == 'menu':
            message_when_closed = bar.message_when_closed
            is_open = bar.open
            cart_id = bar_id + '_' + category.group
            merchant = bar.merchant
        else:
            location = Location.get(db.Key.from_path('Location', int(location_id)))
            is_open = location.enabled
            cart_id = location_id + '_' + category.group
            merchant = location.merchant
        # Preload price rules
        products = Product.prefetch_merchant_price_rules(products, merchant)
        # Preload merchant
        for p in products:
            p.merchant = merchant
        self.render('iphone/products.html', merchant=merchant, category=category, products=products, count_items=self.cart.count_items(cart_id), is_open=is_open, cart_id=cart_id, message_when_closed=message_when_closed)

class ProductViewImageHandler(AppHandler):
    def get(self, product_key):
        if product_key:
            product = Product.get(product_key)
            if product.image:
                self.response.headers['Content-Type'] = "image/png"
                self.response.out.write(product.image)
            else:
                self.error(404)
        else:
            self.error(404)

