from views.core import ConsumerHandler
from models.merchants import Merchant
from models.sales import Order, OrderItem
from models.consumers import Consumer
from google.appengine.ext import db
from google.appengine.api import mail
from webapp2_extras import i18n
_ = i18n.lazy_gettext

class OrderListHandler(ConsumerHandler):
    def post(self):
        self.render('iphone/updateerror.html')
        
    def get(self):
        open_orders = self.consumer.orders.filter('status IN', ['pending', 'preparing']).order('-date_created')
        closed_orders = self.consumer.orders.filter('status =', 'complete').order('-date_created')
        self.render('iphone/orders.html', open_orders=open_orders, closed_orders=closed_orders)

class OrderHandler(ConsumerHandler):
    def post(self):
        self.render('iphone/updateerror.html')
        
    def get(self, order_id, merchant_id=None):
        order = Order.get(db.Key.from_path('Order', int(order_id)))
        if (order.consumer.key() != self.consumer.key()):
            return
        if order.status == 'pending_payment':
            self.render('iphone/orderpendingpayment.html', order=order)
        else:
            self.render('iphone/order.html', order=order)
        
class CancelOrderHandler(ConsumerHandler):
    def post(self):
        order = Order.get(self.request.get('order_key'))
        if (order.consumer.key() != self.consumer.key()):
            self.response.write('accessdenied')
            return
        result = order.change_status('cancelled')
        if not result:
            self.response.write('cancelfailed')
            return
        self.response.write('ordercancelled')

class SendReceiptHandler(ConsumerHandler):
    def post(self):
        order = Order.get(self.request.get('order_key'))
        if (order.consumer.key() != self.consumer.key()):
            self.response.write('accessdenied')
            return
        message = mail.EmailMessage()
        message.sender = "Zerved <info@zervedapp.com>"
        message.subject = _("Zerved Receipt") + " #" + str(order.key().id())
        message.to = order.consumer.email
        message.body = self.jinja2.render_template('emails/receipt.txt', order=order)
        message.html = self.jinja2.render_template('emails/receipt.html', order=order)
        message.send()
        self.response.write('receiptsent')
        
class CompleteOrderHandler(ConsumerHandler):
    def dispatch(self):
        # Consumer validation overridden to return an error code instead of rendering an HTML error page
        access_token = self.request.get('access_token')
        if not Consumer.authenticate(access_token):
            self.response.write('login')
            return
        super(CompleteOrderHandler, self).dispatch()
            
    def post(self):
        order = Order.get(self.request.get('order_key'))
        if (order.consumer.key() != self.consumer.key()):
            self.response.write('accessdenied')
            return
        completionallowed = order.service == 'entrance' or order.merchant.serve_with_consumer_device
        result = completionallowed and order.change_status('complete')
        if not result:
            self.response.write('completionfailed')
            return
        self.response.write('ordercompleted')