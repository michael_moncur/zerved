from views.core import AppHandler
from google.appengine.api import mail

class InfoHandler(AppHandler):
    def get(self):
        self.render('iphone/info.html')

class TermsHandler(AppHandler):
    def get(self):
        self.render('iphone/terms.html')

class PrivacyHandler(AppHandler):
    def get(self):
        self.render('iphone/privacy.html')

class LicensesHandler(AppHandler):
    def get(self):
        self.response.content_type = 'text/plain'
        self.render('iphone/licenses.txt')

class RecommendHandler(AppHandler):
    def get(self):
        self.render('iphone/recommend.html')
        
    def post(self):
        message = mail.EmailMessage()
        message.sender = "Zerved <info@zervedapp.com>"
        message.subject = "Recommend a Venue"
        message.to = "contactform@zervedapp.com"
        message.reply_to = self.request.get("customer_email")
        message.body = self.jinja2.render_template('emails/recommend.txt', form=self.request.POST)
        message.send()
        self.render('iphone/recommend/sent.html')