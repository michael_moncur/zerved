#!/usr/bin/env python
import webapp2
import sys
sys.path.append('app')
sys.path.append('lib')
from webapp2_extras import routes

"""
Definition of API routes.
Version numbers are used for backwards compatibility. If we change an API URL
in a way that the app will no longer work, we make it as a new version, and the
updated app uses the new API version URLs. If an API change does not conflict
with the old app (adding a property etc.) a version change is not necessary.
"""

app = webapp2.WSGIApplication([
    routes.PathPrefixRoute('/api/v2', [
        webapp2.Route('/image/<entity_key:[^/]+>/<width:\d+>/<height:\d+>', 'app.api.core.ImageHandler', 'image'),
        webapp2.Route('/countries', 'app.api.locations.CountryListHandler'),
        webapp2.Route('/locations/nearby/<latitude:[^/]+>/<longitude:[^/]+>', 'app.api.locations.LocationListNearbyV2Handler'),
        webapp2.Route('/locations/closest/<latitude:[^/]+>/<longitude:[^/]+>', 'app.api.locations.ClosestLocationHandler'),
        webapp2.Route('/locations/country/<country:[^/]+>', 'app.api.locations.LocationsByCountryHandler'),
        webapp2.Route('/locations/<location_key:[^/]+>/bars', 'app.api.bars.BarListHandler'),
        webapp2.Route('/locations/<location_key:[^/]+>/categories', 'app.api.categories.LocationCategoryListHandler'),
        webapp2.Route('/locations/<location_key:[^/]+>/categories/<category_key:[^/]+>/products', 'app.api.products.ProductListHandler'),
        webapp2.Route('/locations/<location_key:[^/]+>', 'app.api.locations.LocationHandler'),
        webapp2.Route('/locations/withallbars/<location_key:[^/]+>', 'app.api.locations.LocationWithAllBarsHandler'),
        webapp2.Route('/locations/cover/<blob_key:[^/]+>', 'app.api.locations.LocationCoverServeHandler'),
        webapp2.Route('/locations', 'app.api.locations.LocationListV2Handler'),
        # menus
        webapp2.Route('/bars/<bar_key:[^/]+>', 'app.api.bars.BarHandler'),
        webapp2.Route('/bars/<bar_key:[^/]+>/menus', 'app.api.menus.BarMenusListHandler'),
        webapp2.Route('/bars/<bar_key:[^/]+>/menus/<menu_key:[^/]+>/categories', 'app.api.menus.BarMenuCategoryListHandler'),
        webapp2.Route('/bars/<bar_key:[^/]+>/menus/<menu_key:[^/]+>/categories/<category_key:[^/]+>/products', 'app.api.products.ProductListHandler'),
        # backwards compatibility
        webapp2.Route('/bars/<bar_key:[^/]+>/categories', 'app.api.categories.BarCategoryListHandler'),
        webapp2.Route('/bars/<bar_key:[^/]+>/categories/<category_key:[^/]+>/products', 'app.api.products.ProductListHandler'),
        #
        webapp2.Route('/consumers/login', 'app.api.consumers.LoginHandler'),
        webapp2.Route('/consumers/create', 'app.api.consumers.CreateConsumerHandler'),
        webapp2.Route('/consumers/resetpassword', 'app.api.consumers.ResetPasswordHandler'),
        webapp2.Route('/consumers/<consumer_key:[^/]+>/orders', 'app.api.orders.OrderListHandler'),
        webapp2.Route('/consumers/<consumer_key:[^/]+>/orders/<state:open|closed|open,closed>', 'app.api.orders.OrderListStateHandler'),
        webapp2.Route('/consumers/<consumer_key:[^/]+>/pullnotifications', 'app.api.consumers.PullNotificationsHandler'),
        webapp2.Route('/consumers/<consumer_key:[^/]+>', 'app.api.consumers.ConsumerHandler'),
        webapp2.Route('/paymentsubscriptions/create', 'app.api.consumers.PaymentSubscriptionCreateHandler'), # Deprecated
        webapp2.Route('/paymentsubscriptions/<ps_key:[^/]+>', 'app.api.consumers.PaymentSubscriptionHandler'),
        webapp2.Route('/products/<product_key:[^/]+>', 'app.api.products.ProductHandler'),
        webapp2.Route('/quote', 'app.api.orders.QuoteHandler'),
        webapp2.Route('/placeorder', 'app.api.orders.PlaceOrderHandler'),
        webapp2.Route('/orders/<order_key:[^/]+>/sendreceipt', 'app.api.orders.SendReceiptHandler'),
        webapp2.Route('/orders/<order_key:[^/]+>/4tcallback', 'app.api.fourt.FourTCallbackHandler'),
        webapp2.Route('/orders/mobilepayappswitchcallback', 'app.api.mobilepayappswitch.MobilePayAppSwitchCallbackHandler'),
        webapp2.Route('/orders/epaycallback_v2', 'app.api.epay.EpayCallbackHandler'), # New 2014- Epay
        webapp2.Route('/orders/epayaccept_v2', 'app.api.epay.EpayAcceptHandler'), # New 2014- Epay
        webapp2.Route('/orders/<order_key_or_id:[^/]+>', 'app.api.orders.OrderHandler'),
        webapp2.Route('/favourites', 'app.api.favourites.FavouriteHandler'),
        webapp2.Route('/favourites/<favourite_key:[^/]+>', 'app.api.favourites.FavouriteHandler'),
        webapp2.Route('/consumers/<consumer_key:[^/]+>/favourites', 'app.api.favourites.FavouriteListHandler'),
        webapp2.Route('/orders/<order_key:[^/]+>', 'app.api.orders.OrderHandler'),
        webapp2.Route('/merchants/<merchant_key_or_id:[^/]+>/locations', 'app.api.locations.MerchantLocationsHandler'),
        # Events
        webapp2.Route('/locations/<location_key:[^/]+>/events/<year:[^/]+>/<month:[^/]+>/<day:[^/]+>', 'app.api.events.EventListByDateHandler'),
        webapp2.Route('/events/<event_key:[^/]+>', 'app.api.events.EventHandler'),
        ]),
    routes.PathPrefixRoute('/api', [
        webapp2.Route('/image/<entity_key:[^/]+>/<width:\d+>/<height:\d+>', 'app.api.core.ImageHandler'),
        webapp2.Route('/merchants/nearby/<latitude:[^/]+>/<longitude:[^/]+>', 'app.api.merchants.MerchantListNearbyLegacyHandler'), # Legacy
        webapp2.Route('/merchants/closest/<latitude:[^/]+>/<longitude:[^/]+>', 'app.api.merchants.ClosestMerchantLegacyHandler'), # Legacy
        webapp2.Route('/merchants/<bar_key:[^/]+>/categories/<group:menu|entrance>', 'app.api.categories.CategoryListLegacyHandler'), # Legacy
        webapp2.Route('/merchants/<merchant_key:[^/]+>', 'app.api.merchants.MerchantHandler'),
        webapp2.Route('/merchants', 'app.api.merchants.MerchantListLegacyHandler'), # Legacy
        webapp2.Route('/locations/nearby/<latitude:[^/]+>/<longitude:[^/]+>', 'app.api.locations.LocationListNearbyHandler'),
        webapp2.Route('/locations/closest/<latitude:[^/]+>/<longitude:[^/]+>', 'app.api.locations.ClosestLocationHandler'),
        webapp2.Route('/locations/<location_key:[^/]+>/bars', 'app.api.bars.BarListHandler'),
        webapp2.Route('/locations/<location_key:[^/]+>/categories', 'app.api.categories.LocationCategoryListHandler'),
        webapp2.Route('/locations/<location_key:[^/]+>/categories/<category_key:[^/]+>/products', 'app.api.products.ProductListHandler'),
        webapp2.Route('/locations/<location_key:[^/]+>', 'app.api.locations.LocationHandler'),
        webapp2.Route('/locations', 'app.api.locations.LocationListHandler'),
        webapp2.Route('/bars/<bar_key:[^/]+>', 'app.api.bars.BarHandler'),
        webapp2.Route('/bars/<bar_key:[^/]+>/categories', 'app.api.categories.BarCategoryListHandler'),
        webapp2.Route('/bars/<bar_key:[^/]+>/categories/<category_key:[^/]+>/products', 'app.api.products.ProductListHandler'),
        webapp2.Route('/consumers/login', 'app.api.consumers.LoginHandler'),
        webapp2.Route('/consumers/create', 'app.api.consumers.CreateConsumerHandler'),
        webapp2.Route('/consumers/resetpassword', 'app.api.consumers.ResetPasswordHandler'),
        webapp2.Route('/consumers/<consumer_key:[^/]+>/orders', 'app.api.orders.OrderListHandler'),
        webapp2.Route('/consumers/<consumer_key:[^/]+>/pullnotifications', 'app.api.consumers.PullNotificationsHandler'),
        webapp2.Route('/consumers/<consumer_key:[^/]+>', 'app.api.consumers.ConsumerHandler'),
        webapp2.Route('/paymentsubscriptions/<ps_key:[^/]+>', 'app.api.consumers.PaymentSubscriptionHandler'),
        #webapp2.Route('/categories/<category_key:[^/]+>', 'app.api.categories.CategoryHandler'),
        webapp2.Route('/categories/<category_key:[^/]+>/<location_or_bar_key:[^/]+>/products', 'app.api.products.ProductListLegacyHandler'), # Legacy
        webapp2.Route('/products/<product_key:[^/]+>', 'app.api.products.ProductHandler'),
        webapp2.Route('/quote', 'app.api.orders.QuoteHandler'),
        webapp2.Route('/placeorder', 'app.api.orders.PlaceOrderHandler'),
        webapp2.Route('/orders/<order_key:[^/]+>/sendreceipt', 'app.api.orders.SendReceiptHandler'),
        webapp2.Route('/orders/<order_key:[^/]+>', 'app.api.orders.OrderHandler'),        
        webapp2.Route('/favourites/add', 'app.api.favourites.FavouriteAddHandler'),
        webapp2.Route('/favourites/delete', 'app.api.favourites.FavouriteDeleteHandler'),
        webapp2.Route('/favourites/<consumer_key:[^/]+>', 'app.api.favourites.FavouriteListHandler'),

        ])
    ], config={'webapp2_extras.i18n':{'default_timezone':'Europe/Copenhagen'}})
