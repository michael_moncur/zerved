import sys
sys.path.append('app')
sys.path.append('lib')
from google.appengine.ext import deferred
from google.appengine.ext import db
from models.merchants import Bar, Merchant
from decimal import *
import logging

BATCH_SIZE = 100  # ideal batch size may vary based on entity size.

def UpdateSchema(cursor=None, num_updated=0):
    query = Merchant.all()
    if cursor:
        query.with_cursor(cursor)

    to_put = []
    for entity in query.fetch(limit=BATCH_SIZE):
        entity.loyalty_program_name = "Loyalty number"
        entity.loyalty_program_rate = Decimal('0.00')

        to_put.append(entity)

    #TODO: add exclude_from_loyalty=False to all Products in DB

    if to_put:
        db.put(to_put)
        num_updated += len(to_put)
        logging.debug(
            'Put %d entities to Datastore for a total of %d',
            len(to_put), num_updated)
        deferred.defer(
            UpdateSchema, cursor=query.cursor(), num_updated=num_updated)
    else:
        logging.debug(
            'UpdateSchema complete with %d updates!', num_updated)