#!/usr/bin/env python
import webapp2
import sys
import os
sys.path.append('app')
sys.path.append('lib')
from webapp2_extras import routes

debug = os.environ.get('SERVER_SOFTWARE','').startswith('Development') or os.environ['CURRENT_VERSION_ID'].split(".")[0] == 'dev'

app = webapp2.WSGIApplication([
    routes.PathPrefixRoute('/iphone', [
        webapp2.Route('/merchants', 'app.views.core.UpdateIphoneHandler', 'merchant-list'), # Removed
        webapp2.Route('/merchants/<latitude:[^/]+>/<longitude:[^/]+>', 'app.views.core.UpdateIphoneHandler', 'merchant-list-closest'), # Removed
        webapp2.Route('/image/<entity_key:[^/]+>/<width:\d+>/<height:\d+>', 'app.views.core.ImageHandler', 'image'),
        webapp2.Route('/locations', 'app.views.core.UpdateIphoneHandler', 'location-list'), # Removed
        webapp2.Route('/locations/<location_id:\d+>/<group:menu|entrance>', 'app.views.iphone.categories.LocationCategoryListHandler', 'location-category-list'),
        webapp2.Route('/locations/<location_id:\d+>/bars', 'app.views.core.UpdateIphoneHandler', 'bar-list'), # Removed
        webapp2.Route('/locations/<latitude:[^/]+>/<longitude:[^/]+>', 'app.views.core.UpdateIphoneHandler', 'location-list-closest'), # Removed
        webapp2.Route('/locations/<location_id:\d+>/categories/<category_id:\d+>/products', 'app.views.iphone.products.ProductListHandler'),
        webapp2.Route('/bars/<bar_id:\d+>/categories', 'app.views.iphone.categories.BarCategoryListHandler', 'bar-category-list'),
        webapp2.Route('/bars/<bar_id:\d+>/categories/<category_id:\d+>/products', 'app.views.iphone.products.ProductListHandler'),
        webapp2.Route('/categories/<bar_id:\d+>/<group:[^/]+>', 'app.views.iphone.categories.CategoryListLegacyHandler', 'category-list'), # Legacy
        webapp2.Route('/categories/<:\d+>', 'app.views.core.UpdateIphoneHandler'), # Removed
        webapp2.Route('/products/<bar_id:\d+>/<category_id:\d+>', 'app.views.iphone.products.ProductListLegacyHandler', 'product-list'), # Legacy
        webapp2.Route('/savedevicetoken', 'app.views.iphone.consumers.SaveDeviceTokenHandler', 'save-device-token'),
        webapp2.Route('/addtocart', 'app.views.core.UpdateIphoneHandler', 'add-to-cart'), # Removed
        webapp2.Route('/cart/<bar_id:\d+>/<group:[^/]+>', 'app.views.core.UpdateIphoneHandler', 'cart-legacy'), # Removed
        webapp2.Route('/clearcart/<merchant_id:\d+>/<group:[^/]+>', 'app.views.core.UpdateIphoneHandler', 'clear-cart-legacy'), # Removed
        webapp2.Route('/removecartitem/<merchant_id:\d+>/<group:[^/]+>/<item_index:\d+>', 'app.views.core.UpdateIphoneHandler', 'cart-remove-item-legacy'), # Removed
        webapp2.Route('/updatecartitem', 'app.views.core.UpdateIphoneHandler', 'cart-update-item'), # Removed
        webapp2.Route('/cart/<:[^/]+>', 'app.views.core.UpdateIphoneHandler', 'cart'), # Replaced by API
        webapp2.Route('/clearcart/<cart_id:[^/]+>', 'app.views.core.UpdateIphoneHandler', 'clear-cart'), # Removed
        webapp2.Route('/removecartitem/<cart_id:[^/]+>/<item_index:\d+>', 'app.views.core.UpdateIphoneHandler', 'cart-remove-item'), # Removed
        webapp2.Route('/orders', 'app.views.iphone.orders.OrderListHandler', 'orders-list'),
        webapp2.Route('/order/<merchant_id:\d+>/<order_id:\d+>', 'app.views.iphone.orders.OrderHandler', 'order-legacy'), # Legacy
        webapp2.Route('/order/<order_id:\d+>', 'app.views.iphone.orders.OrderHandler', 'order'),
        webapp2.Route('/cancelorder', 'app.views.iphone.orders.CancelOrderHandler', 'cancel-order'),
        webapp2.Route('/completeorder', 'app.views.iphone.orders.CompleteOrderHandler', 'complete-order'),
        webapp2.Route('/sendreceipt', 'app.views.iphone.orders.SendReceiptHandler', 'send-receipt'),
        webapp2.Route('/profile', 'app.views.core.UpdateIphoneHandler', 'profile'), # Replaced by API
        webapp2.Route('/info', 'app.views.iphone.info.InfoHandler', 'info'),
        webapp2.Route('/epaypayment', 'app.views.core.UpdateIphoneHandler', 'epay-payment-legacy'), # Old 2012-2013 Epay removed
        webapp2.Route('/epayauthorize', 'app.views.core.UpdateIphoneHandler', 'epay-payment'), # Old 2012-2013 Epay removed
        webapp2.Route('/epayaccept/<group:[^/]+>', 'app.views.core.UpdateIphoneHandler', 'epay-accept'), # Old 2012-2013 Epay removed
        webapp2.Route('/epaycancel', 'app.views.core.UpdateIphoneHandler', 'epay-cancel'), # Old 2012-2013 Epay removed
        webapp2.Route('/terms', 'app.views.iphone.info.TermsHandler', 'terms'),
        webapp2.Route('/privacy', 'app.views.iphone.info.PrivacyHandler', 'privacy'),
        webapp2.Route('/logininfo', 'app.views.iphone.consumers.LoginInfoHandler', 'logininfo'),
        webapp2.Route('/recommend', 'app.views.iphone.info.RecommendHandler', 'recommend'),
        webapp2.Route('/softwarelicenses', 'app.views.iphone.info.LicensesHandler', 'licenses'),
        ])
    ],
    debug=debug, config={'webapp2_extras.sessions': {'secret_key':'0f9s0f98s0df'}, 'webapp2_extras.i18n':{'default_timezone':'Europe/Copenhagen'}})
