package com.zervedapp.zerved;

import java.util.ArrayList;

import org.json.JSONException;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.zervedapp.zerved.models.Bar;
import com.zervedapp.zerved.models.Category;
import com.zervedapp.zerved.models.MenuCard;
import com.zervedapp.zerved.models.MerchantLocation;

public class CategoryListFragment extends RESTListFragment {
	public static final String TAG = CategoryListFragment.class.getName();
	public static final String ARG_GROUP = "com.zervedapp.zerved.GROUP";
	public static final String ARG_MENU = "com.zervedapp.zerved.MENU";
    public static final String EXTRA_LOCATION = "com.zervedapp.zerved.LOCATION";
    public static final String EXTRA_BAR = "com.zervedapp.zerved.BAR";
    public static final String EXTRA_MENU = "com.zervedapp.zerved.MENU";
    public static final String EXTRA_GROUP = "com.zervedapp.zerved.GROUP";
    public static final String EXTRA_MENUCARDS = "com.zervedapp.zerved.MENUCARDS";
    public static final String EXTRA_CATEGORIES = "com.zervedapp.zerved.CATEGORIES";
    public static final String EXTRA_SELECTED_INDEX = "com.zervedapp.zerved.SELECTED_INDEX";
    public static final String EXTRA_PRODUCT = "com.zervedapp.zerved.EXTRA_PRODUCT";
    
	private ArrayAdapter<Category> mAdapter;
	private static String LOCATION_URI = Config.API_URL+"locations/%s/categories";
	private static String BAR_URI = Config.API_URL+"bars/%s/menus/%s/categories";
	private MerchantLocation mLocation = null;
	private Bar mBar = null;
	private MenuCard mMenu = null;
	private String mGroup;
	private String mCategoriesJson;
	
	public interface LocationBarProvider {
		public MerchantLocation getLocation();
		public Bar getBar();
		public boolean isLocationLoading();
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		
		Bundle args = getArguments();
		mGroup = args.getString(ARG_GROUP);
		
		if(mGroup.equals("menu")) {
			try {
				mMenu = new MenuCard(args.getString(ARG_MENU));
			} catch (JSONException e1) {
				Log.i("MENU", mMenu.toJson());
				e1.printStackTrace();
			}	
		}
		
        mAdapter = new ArrayAdapter<Category>(getActivity(), R.layout.item_label_list);
        this.setListAdapter(mAdapter);
        
        setHasOptionsMenu(true);
        
        reload();
	}
	
	@Override
	public void reload() {
		LocationBarProvider activity = (LocationBarProvider) getActivity();
		mLocation = activity.getLocation();
		mBar = activity.getBar();
		TextView emptyMsg = (TextView) getView().findViewById(R.id.empty_message);
		if (mLocation != null) {
			// Merchant found - load merchant
			emptyMsg.setText(getResources().getText(mGroup.equals("entrance") ? R.string.no_entrance : R.string.no_menu));
			super.reload();
		}
	}
	
	@Override
	public String getUri() {
		if (mGroup.equals("entrance")) {
			return String.format(LOCATION_URI, mLocation.getKey());
		} else {
			return String.format(BAR_URI, mBar.getKey(), mMenu.getKey());
		}
	}
	
	@Override
	public void onJsonLoaded(String json) {
		mCategoriesJson = json;
    	ArrayList<Category> categories = Category.initList(json);
        
        // Load our list adapter with our models.
        mAdapter.clear();
        for (Category category : categories) {
        	mAdapter.add(category);
        }
	}
	
    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
    	Intent intent = new Intent(getActivity(), ProductsActivity.class);
    	intent.putExtra(EXTRA_LOCATION, mLocation.toJson());
    	if (mBar != null) {
    		intent.putExtra(EXTRA_BAR, mBar.toJson());
    		intent.putExtra(EXTRA_MENU, mMenu.toJson());
    	}
    	intent.putExtra(EXTRA_GROUP, mGroup);
    	intent.putExtra(EXTRA_CATEGORIES, mCategoriesJson);
    	intent.putExtra(EXTRA_SELECTED_INDEX, position);
        startActivity(intent);
    }
}