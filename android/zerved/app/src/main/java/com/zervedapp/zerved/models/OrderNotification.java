package com.zervedapp.zerved.models;

import org.json.JSONException;

import android.content.Intent;

public class OrderNotification {
	public static final String TAG = OrderNotification.class.getName();
	
	public String notificationKey;
	public long orderId;
	public String orderKey;
	public String merchantName;
	public String message;
	
	public OrderNotification(String json) {
		try {
			JSONModel data = new JSONModel(json);
			notificationKey = data.getString("key");
			orderId = data.getLong("order_id");
			orderKey = data.getString("order_key");
			merchantName = data.getString("merchant_name");
			message = data.getString("message");
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}
	
	public OrderNotification(Intent intent) {
		notificationKey = intent.getStringExtra("notification_key");
		String orderIdStr = intent.getStringExtra("order_id");
		orderId = Long.parseLong(orderIdStr);
		orderKey = intent.getStringExtra("order_key");
		merchantName = intent.getStringExtra("merchant_name");
		message = intent.getStringExtra("message");
	}
	
	public String getMessageId() {
		// Used to check for duplicate messages
		return notificationKey;
	}
	
	public int getCollapseId() {
		// Used to collect messages about the same order in one Android notification
		// Shorten order ID if too long for int
		if (orderId > Integer.MAX_VALUE) {
			String orderIdStr = Long.toString(orderId);
			orderIdStr = orderIdStr.substring(orderIdStr.length()-8);
			return Integer.parseInt(orderIdStr);
		}
		return (int) orderId;
	}
}
