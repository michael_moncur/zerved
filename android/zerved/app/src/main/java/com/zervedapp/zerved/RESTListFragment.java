package com.zervedapp.zerved;

import android.app.ListFragment;
import android.net.Uri;
import android.os.Bundle;
import android.app.LoaderManager.LoaderCallbacks;
import android.content.Loader;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import com.zervedapp.zerved.loader.RESTLoader;
import com.zervedapp.zerved.loader.RESTLoader.RESTResponse;
import com.zervedapp.zerved.models.ConsumerManager;

public abstract class RESTListFragment extends ListFragment implements LoaderCallbacks<RESTResponse> {
	protected static final int LOADER_REST_LIST = 0x1;
	protected static final String ARGS_URI    = "com.zervedapp.zerved.ARGS_URI";
	protected static final String ARGS_PARAMS = "com.zervedapp.zerved.ARGS_PARAMS";
	protected String mJsonData;
    
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.progress_list, null);
		ListView listView = (ListView) view.findViewById(android.R.id.list);
		listView.setItemsCanFocus(true);
		return view;
	}
	
	public void reload() {
		Uri uri = Uri.parse(getUri());
		Bundle params = getParams();
		Bundle args = new Bundle();
        args.putParcelable(ARGS_URI, uri);
        args.putParcelable(ARGS_PARAMS, params);
		getLoaderManager().restartLoader(LOADER_REST_LIST, args, this);
	}
	
	public abstract String getUri();
	
	public Bundle getParams() {
		return new Bundle();
	}
	
	public RESTLoader.HTTPVerb getVerb() {
		return RESTLoader.HTTPVerb.GET;
	}
	
	public String getAuthorization() {
		return null;
	}
	
	@Override
	public Loader<RESTResponse> onCreateLoader(int id, Bundle args) {
		mJsonData = null;
		
        if (args != null && args.containsKey(ARGS_URI) && args.containsKey(ARGS_PARAMS)) {
            Uri    action = args.getParcelable(ARGS_URI);
            Bundle params = args.getParcelable(ARGS_PARAMS);
            
            return new RESTLoader(getActivity(), getVerb(), action, params, getAuthorization());
        }
        
        getView().findViewById(R.id.progress).setVisibility(View.VISIBLE);
        getView().findViewById(R.id.progress_text).setVisibility(View.VISIBLE);
        getView().findViewById(R.id.empty_message).setVisibility(View.GONE);
        getListView().setEmptyView(getView().findViewById(R.id.progress));
        
        return null;
	}

	@Override
	public void onLoadFinished(Loader<RESTResponse> loader, RESTResponse data) {
		int    code = data.getCode();
        String json = data.getData();
        
        if (code == 200 && !json.equals("")) {
        	mJsonData = json;
        	onJsonLoaded(json);
        }
        else if (code > 0) {
        	Toast.makeText(this.getActivity(), R.string.failed_to_load, Toast.LENGTH_SHORT).show();
        	if (code == 401) {
        		ConsumerManager consumer = ((ZervedApp) getActivity().getApplication()).getConsumer();
        		consumer.mAccessToken = null;
        	}
        } else {
        	// No status code
        	Toast.makeText(this.getActivity(), R.string.no_connection, Toast.LENGTH_SHORT).show();
        }
        
        getView().findViewById(R.id.empty_message).setVisibility(View.VISIBLE);
        getView().findViewById(R.id.progress).setVisibility(View.GONE);
        getView().findViewById(R.id.progress_text).setVisibility(View.GONE);
        getListView().setEmptyView(getView().findViewById(R.id.empty_message));
	}

	@Override
	public void onLoaderReset(Loader<RESTResponse> arg0) {
		// TODO Auto-generated method stub
		
	}
	
	public abstract void onJsonLoaded(String json);

}
