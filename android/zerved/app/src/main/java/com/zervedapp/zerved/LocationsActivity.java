package com.zervedapp.zerved;

import com.crashlytics.android.Crashlytics;
import io.fabric.sdk.android.Fabric;
import java.util.ArrayList;
import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.app.Fragment;
import android.support.v13.app.FragmentStatePagerAdapter;
import android.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.google.android.gcm.GCMRegistrar;
import com.hlidskialf.android.hardware.ShakeListener;
import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;
import com.zervedapp.zerved.models.Country;
import com.zervedapp.zerved.models.MerchantLocation;

public class LocationsActivity extends Activity implements ActionBar.TabListener {
	private static final String TAG = LocationsActivity.class.getName();
	
	private static final int TWO_MINUTES = 1000 * 60 * 2;
	private static final int LOCATION_EXPIRATION = 1000 * 60 * 5; // 5 minutes
	
	AppSectionsPagerAdapter mAppSectionsPagerAdapter;
    ViewPager mViewPager;
	Location mCurrentLocation;
	ActionBar mActionBar;
	NearbyLocationsFragment mListFragment;
	ShakeListener mShaker;
	SlidingMenu mMenu;
	
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
		// Initialize cart and consumer
        ZervedApp app = (ZervedApp) getApplication();
        app.init();
		
		//setContentView(R.layout.fragment);
		setContentView(R.layout.viewpager);
		setTitle(R.string.locations);
		
		mMenu = SlidingMenuHelper.initSlidingMenu(this, "locations");
		
		// Create the adapter that will return a fragment for each tab
        mAppSectionsPagerAdapter = new AppSectionsPagerAdapter(this);

        // Set up the action bar.
        mActionBar = getActionBar();
        mActionBar.setDisplayHomeAsUpEnabled(true);

        // Specify that we will be displaying tabs in the action bar.
        mActionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

        // Set up the ViewPager, attaching the adapter and setting up a listener for when the
        // user swipes between sections.
        mViewPager = (ViewPager) findViewById(R.id.pager);
        mViewPager.setAdapter(mAppSectionsPagerAdapter);
        mViewPager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                // Swiping left on first tab should open the menu, swiping in other sections should switch sections
            	switch (position) {
            	case 0:
            		mMenu.setTouchModeAbove(SlidingMenu.TOUCHMODE_FULLSCREEN);
            		break;
            	default:
					mMenu.setTouchModeAbove(SlidingMenu.TOUCHMODE_NONE);
					break;
            	}
            	mActionBar.setSelectedNavigationItem(position);
            }
        });
        mMenu.setTouchModeAbove(SlidingMenu.TOUCHMODE_FULLSCREEN);

        // For each of the sections in the app, add a tab to the action bar.
        for (int i = 0; i < mAppSectionsPagerAdapter.getCount(); i++) {
            // Create a tab with text corresponding to the page title defined by the adapter.
            // Also specify this Activity object, which implements the TabListener interface, as the
            // listener for when this tab is selected.
        	mActionBar.addTab(
        			mActionBar.newTab()
                            .setText(mAppSectionsPagerAdapter.getPageTitle(i))
                            .setTabListener(this));
        }
        
        // Register device for notifications
        GCMRegistrar.checkDevice(this);
		GCMRegistrar.checkManifest(this);
		final String regId = GCMRegistrar.getRegistrationId(this);
		if (regId.equals("")) {
		  GCMRegistrar.register(this, GCMIntentService.SENDER_ID);
		} else {
		  Log.v(TAG, "Already registered for GCM");
		}
	}
	
	public void onResume() {
		super.onResume();
		refreshTabs();

		// The 'All' tab (country selection tab) is currently handled in a special way.
		// When the user hits the tab, he is now immediately sent to LocationsAlphabeticActivity as there are only
		// Danish merchants anyway.
		// If the user presses the back button when on LocationsAlphabeticActivity then we change the selected tab to the first
		// tab in the actionbar (the 'All' tab is empty).
		final int indexOfAllTab = 1;
		if (mActionBar.getSelectedTab().getPosition() == indexOfAllTab) {
			mActionBar.selectTab((mActionBar.getTabAt((0))));
		}
	}
	
	public void onPause() {
		super.onPause();
	}
	
	public void refreshTabs() {
		// Toggle tab for merchant's own locations depending on merchant ID set in settings
		SharedPreferences settings = getSharedPreferences("ZervedSettings", 0);
		String merchantId = settings.getString("merchant_id", "");
		if (mActionBar.getTabCount() == 3 && !merchantId.equals("")) {
			mAppSectionsPagerAdapter.notifyDataSetChanged();
			mActionBar.addTab(
        			mActionBar.newTab()
                            .setText(mAppSectionsPagerAdapter.getPageTitle(3))
                            .setTabListener(this));
		} else if (mActionBar.getTabCount() == 4 && merchantId.equals("")) {
			mAppSectionsPagerAdapter.notifyDataSetChanged();
			mActionBar.removeTabAt(3);
		}
	}
	
	public void reload() {
		if (mActionBar.getSelectedNavigationIndex() == 0) {
			mAppSectionsPagerAdapter.reload();
		} else {
			mAppSectionsPagerAdapter.notifyDataSetChanged();
		}
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.locations, menu);
		return true;
	}
	
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
    	switch (item.getItemId()) {
			case android.R.id.home:
				mMenu.toggle();
				return true;
			case R.id.reload:
				reload();
				return true;
    	}
    	return super.onOptionsItemSelected(item);
    }
    
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
	    if ( keyCode == KeyEvent.KEYCODE_MENU ) {
	    	mMenu.toggle();
	        return true;
	    }
	    return super.onKeyDown(keyCode, event);
	}
    
   
   @Override
   public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
   }

   @Override
   public void onTabSelected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
	   // Only active danish merchants exist currently. Thus, instead of showing the country selection list, the user is
	   // immediately redirected to the list of Danish merchants.

	   Fragment selectedFragment = mAppSectionsPagerAdapter.getItem(tab.getPosition());
	   if (selectedFragment instanceof CountryListFragment) {
		   Intent intent = new Intent(this, LocationsAlphabeticActivity.class);
		   intent.putExtra(LocationsAlphabeticActivity.EXTRA_COUNTRY, "{\"name\":\"Danmark\",\"country_code\":\"DK\"}");
		   startActivity(intent);
	   }
	   else {
		   // Regular tab
		   // When the given tab is selected, switch to the corresponding page in the ViewPager.
		   mViewPager.setCurrentItem(tab.getPosition());
	   }
   }

   @Override
   public void onTabReselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
   }

	public static class AppSectionsPagerAdapter extends FragmentStatePagerAdapter {
    	private Context mContext;
    	private NearbyLocationsFragment mNearbyFragment;
    	
        public AppSectionsPagerAdapter(LocationsActivity activity) {
            super(activity.getFragmentManager());
            mContext = activity;
        }
        
        public void reload() {
        	if (mNearbyFragment != null && mNearbyFragment.isAdded()) {
        		mNearbyFragment.reload();
        	}
        }

        @Override
        public Fragment getItem(int i) {
            switch (i) {
            	case 0:
            		// Nearby
            		mNearbyFragment = new NearbyLocationsFragment();
                	return mNearbyFragment;
            	case 1:
            		// All
            		Fragment countriesFragment = new CountryListFragment();
                	return countriesFragment;
            	case 2:
            		// Recommend
            		Fragment recommendFragment = new RecommendFragment();
            		return recommendFragment;
            	case 3:
            		// Mine
            		Fragment merchantLocationFragment = new MerchantLocationsFragment();
            		return merchantLocationFragment;
                default:
                	return null;
            }
        }

        @Override
        public int getCount() {
        	SharedPreferences settings = mContext.getSharedPreferences("ZervedSettings", 0);
    		String merchantId = settings.getString("merchant_id", "");
    		if (merchantId.equals("")) {
    			return 3;
    		} else {
    			return 4;
    		}
        }

        @Override
        public CharSequence getPageTitle(int position) {
        	switch (position) {
        		case 0:
        			return mContext.getResources().getText(R.string.nearby);
        		case 1:
        			return mContext.getResources().getText(R.string.all);
        		case 2:
        			return mContext.getResources().getText(R.string.recommend_a_venue);
        		case 3:
        			return mContext.getResources().getText(R.string.mine);
        		default:
        			return "";
        	}
        }
        
        public int getItemPosition(Object item) {
        	return POSITION_NONE;
        }
    }
	
	public static class NearbyLocationsFragment extends RESTListFragment {
		private LocationAdapter mAdapter;
		ArrayList<MerchantLocation> mMerchants;
		
		protected static String URI = Config.API_URL+"locations/nearby/%s/%s";
		protected static String URI_ALL = Config.API_URL+"locations";
		private static final int TRACKING_TIMEOUT = 1000 * 10; // 10 seconds
		private Location mCurrentLocation;
		LocationManager mLocationManager;
		LocationListener mLocationListener;
		boolean mIsLocationLoading = false;
		
		@Override
		public void onActivityCreated(Bundle savedInstanceState) {
			super.onActivityCreated(savedInstanceState);
			
			mAdapter = new LocationAdapter(getActivity(), R.layout.merchant);
	        this.setListAdapter(mAdapter);
	        
	        if (savedInstanceState != null) {
	        	// Recreate fragment from saved instance state
	        	mCurrentLocation = savedInstanceState.getParcelable("location");
	        	if (mCurrentLocation != null) {
	        		mAdapter.location = mCurrentLocation;
	        	}
	        	mJsonData = savedInstanceState.getString("json_data");
	        	if (mJsonData != null) {
		        	onJsonLoaded(mJsonData);
		        	getView().findViewById(R.id.empty_message).setVisibility(View.VISIBLE);
		            getView().findViewById(R.id.progress).setVisibility(View.GONE);
		            getView().findViewById(R.id.progress_text).setVisibility(View.GONE);
		            getListView().setEmptyView(getView().findViewById(R.id.empty_message));
	        	}
	        }
	        
	        // Create location manager and location listener
	        mLocationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
	        mLocationListener = new LocationListener() {
	    	    public void onLocationChanged(Location location) {
	    	    	// If a better location is found, update merchant list
	    	    	//Log.i(TAG, "Location changed to: "+location);
	    	    	if (isBetterLocation(location, mCurrentLocation)) {
	    	    		mCurrentLocation = location;
	    	    		NearbyLocationsFragment.this.reload(mCurrentLocation);
	    	    	}
	    	    }

	    	    public void onStatusChanged(String provider, int status, Bundle extras) {}

	    	    public void onProviderEnabled(String provider) {}

	    	    public void onProviderDisabled(String provider) {}
	        };
		}
		
	    @Override
	    public void onSaveInstanceState(Bundle outState) {
	    	super.onSaveInstanceState(outState);
	    	// Save fragment instance state
	    	if (mCurrentLocation != null) {
	    		outState.putParcelable("location", mCurrentLocation);
	    	}
	    	if (mJsonData != null) {
	    		outState.putString("json_data", mJsonData);
	    	}
	    }
	    
	    @Override
	    public void onResume() {
	    	super.onResume();
			// Reload location there is no existing location, or existing location is old, or if the list is empty
			if (mCurrentLocation == null || (System.currentTimeMillis() - mCurrentLocation.getTime()) > LOCATION_EXPIRATION || mAdapter.isEmpty()) {
				reload();
			}
	    }
	    
	    @Override
	    public void onPause() {
	    	mLocationManager.removeUpdates(mLocationListener);
	    	stop();
	    	super.onPause();
	    }
		
		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
			View view = super.onCreateView(inflater, container, savedInstanceState);
			TextView progressText = (TextView) view.findViewById(R.id.progress_text);
			progressText.setText(R.string.finding_location);
			return view;
		}
		
		public void reset() {
			mAdapter.clear();
			if (getView() == null) {
				return;
			}
			getView().findViewById(R.id.progress).setVisibility(View.VISIBLE);
	        getView().findViewById(R.id.progress_text).setVisibility(View.VISIBLE);
	        getView().findViewById(R.id.empty_message).setVisibility(View.GONE);
	        getListView().setEmptyView(getView().findViewById(R.id.progress));
		}
		
		public void stop() {
			if (getView() == null) {
				return;
			}
			getView().findViewById(R.id.empty_message).setVisibility(View.VISIBLE);
	        getView().findViewById(R.id.progress).setVisibility(View.GONE);
	        getView().findViewById(R.id.progress_text).setVisibility(View.GONE);
	        getListView().setEmptyView(getView().findViewById(R.id.empty_message));
		}
		
		public void reload() {
			// Show loading message
			reset();
			// Use last know location to update places
			Location lastLocation = mLocationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
			if (lastLocation != null) {
				mCurrentLocation = lastLocation;
				reload(mCurrentLocation);
			}
			// Request location updates
			mIsLocationLoading = false;
			if (mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
				mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 10, mLocationListener);
				mIsLocationLoading = true;
			}
			if (mLocationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
				mLocationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 1000, 10, mLocationListener);
				mIsLocationLoading = true;
			}
			// No location service available
			if (!mIsLocationLoading) {
				Toast.makeText(getActivity(), R.string.unable_to_find_location, Toast.LENGTH_SHORT).show();
				// Stop the progress spinner in the list. Hack: it's not yet created yet, so wait a second
				final Handler noLocationHandler = new Handler();
				noLocationHandler.postDelayed(new Runnable() {
					public void run() {
						stop();
					}
				}, 1000);
			}
			// Timeout location updates after 10 secs
			final Handler locationTimeoutHandler = new Handler();
			locationTimeoutHandler.postDelayed(new Runnable() {
				@Override
				public void run() {
					if (mIsLocationLoading) {
						mLocationManager.removeUpdates(mLocationListener);
						mIsLocationLoading = false;
						stop();
						if (mCurrentLocation == null && getActivity() != null) {
							Toast.makeText(getActivity(), R.string.unable_to_find_location, Toast.LENGTH_SHORT).show();
						}
					}
				}
			}, TRACKING_TIMEOUT);
		}
		
		public void reloadList() {
			super.reload();
		}
		
		public void reload(Location location) {
			mCurrentLocation = location;
			this.mAdapter.location = location;
			reloadList();
		}
		
		@Override
		public String getUri() {
			if (mCurrentLocation != null) {
				return String.format(URI, mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude());
			} else {
				return URI_ALL;
			}
		}
		
		@Override
		public void onJsonLoaded(String json) {
			mMerchants = MerchantLocation.initList(json);
            
            // Load our list adapter with our models.
            mAdapter.clear();
            // Add locations to list
            for (MerchantLocation merchant : mMerchants) {
            	mAdapter.add(merchant);
            }
		}
		
		@Override
	    public void onListItemClick(ListView l, View v, int position, long id) {
			// Select venue
			((ZervedApp)getActivity().getApplication()).mLocation = mMerchants.get(position);
			Intent intent = new Intent(getActivity(), LocationActivity.class);
			intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
	    	startActivity(intent);    	    
		}
		
	    /** Determines whether one Location reading is better than the current Location fix
	     * @param location  The new Location that you want to evaluate
	     * @param currentBestLocation  The current Location fix, to which you want to compare the new one
	     */
	   protected boolean isBetterLocation(Location location, Location currentBestLocation) {
	       if (currentBestLocation == null) {
	           // A new location is always better than no location
	           return true;
	       }

	       // Check whether the new location fix is newer or older
	       long timeDelta = location.getTime() - currentBestLocation.getTime();
	       boolean isSignificantlyNewer = timeDelta > TWO_MINUTES;
	       boolean isSignificantlyOlder = timeDelta < -TWO_MINUTES;
	       boolean isNewer = timeDelta > 0;

	       // If it's been more than two minutes since the current location, use the new location
	       // because the user has likely moved
	       if (isSignificantlyNewer) {
	           return true;
	       // If the new location is more than two minutes older, it must be worse
	       } else if (isSignificantlyOlder) {
	           return false;
	       }

	       // Check whether the new location fix is more or less accurate
	       int accuracyDelta = (int) (location.getAccuracy() - currentBestLocation.getAccuracy());
	       boolean isLessAccurate = accuracyDelta > 0;
	       boolean isMoreAccurate = accuracyDelta < 0;
	       boolean isSignificantlyLessAccurate = accuracyDelta > 200;

	       // Check if the old and new location are from the same provider
	       boolean isFromSameProvider = isSameProvider(location.getProvider(),
	               currentBestLocation.getProvider());

	       // Determine location quality using a combination of timeliness and accuracy
	       if (isMoreAccurate) {
	           return true;
	       } else if (isNewer && !isLessAccurate) {
	           return true;
	       } else if (isNewer && !isSignificantlyLessAccurate && isFromSameProvider) {
	           return true;
	       }
	       return false;
	   }
	   
	   /** Checks whether two providers are the same */
	   private boolean isSameProvider(String provider1, String provider2) {
	       if (provider1 == null) {
	         return provider2 == null;
	       }
	       return provider1.equals(provider2);
	   }
	}
	
	public static class CountryListFragment extends RESTListFragment {
		private ArrayAdapter<Country> mAdapter;
		protected static String URI = Config.API_URL+"countries";
		private ArrayList<Country> mCountries;

		@Override
		public void onActivityCreated(Bundle savedInstanceState) {
			super.onActivityCreated(savedInstanceState);
			
	        mAdapter = new ArrayAdapter<Country>(getActivity(), R.layout.item_label_list);
	        this.setListAdapter(mAdapter);
	        
	        reload();
		}
		
		@Override
		public String getUri() {
			return URI;
		}

		@Override
		public void onJsonLoaded(String json) {
			mCountries = Country.initList(json);
			mAdapter.clear();
			for (Country country: mCountries) {
				mAdapter.add(country);
			}
		}
		
		@Override
	    public void onListItemClick(ListView l, View v, int position, long id) {
			Intent intent = new Intent(getActivity(), LocationsAlphabeticActivity.class);
			intent.putExtra(LocationsAlphabeticActivity.EXTRA_COUNTRY, mCountries.get(position).toJson());
	    	startActivity(intent);
		}
	}
	
	public static class MerchantLocationsFragment extends RESTListFragment {
		private LocationAdapter mAdapter;
		ArrayList<MerchantLocation> mLocations;
		protected static String URI = Config.API_URL+"merchants/%s/locations";
		private static final int LOCATION_EXPIRATION = 1000 * 60 * 5; // 5 minutes
		
		@Override
		public void onActivityCreated(Bundle savedInstanceState) {
			super.onActivityCreated(savedInstanceState);
			
			mAdapter = new LocationAdapter(getActivity(), R.layout.merchant);
	        this.setListAdapter(mAdapter);
	        
	        reload();
		}
		
		@Override
		public String getUri() {
			SharedPreferences settings = getActivity().getSharedPreferences("ZervedSettings", 0);
			String merchantId = settings.getString("merchant_id", "");
			return String.format(URI, merchantId);
		}
		
		@Override
		public void reload() {
			// Get last known location, if it is recent
			LocationManager locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
			Location lastLocation = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
			if (lastLocation != null && (System.currentTimeMillis() - lastLocation.getTime()) < LOCATION_EXPIRATION) {
				mAdapter.location = lastLocation;
			} else {
				mAdapter.location = null;
			}
			super.reload();
		}
		
		@Override
		public void onJsonLoaded(String json) {
			mLocations = MerchantLocation.initList(json);
            
            // Load our list adapter with our models.
            mAdapter.clear();
            // Add locations to list
            for (MerchantLocation merchant : mLocations) {
            	mAdapter.add(merchant);
            }
		}
		
		@Override
	    public void onListItemClick(ListView l, View v, int position, long id) {
			// Select venue
			((ZervedApp)getActivity().getApplication()).mLocation = mLocations.get(position);
			Intent intent = new Intent(getActivity(), LocationActivity.class);
			intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			intent.putExtra(LocationActivity.EXTRA_SHOW_ALL_BARS, true);
	    	startActivity(intent);
		}
	}
	
	public static class RecommendFragment extends Fragment {
		@Override
		public void onActivityCreated(Bundle savedInstanceState) {
			super.onActivityCreated(savedInstanceState);
		}		
		
		@SuppressLint("SetJavaScriptEnabled")
		public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		    View mainView = (View) inflater.inflate(R.layout.fragment_web, container, false);
		    WebView webView = (WebView) mainView.findViewById(R.id.webview);
		    webView.loadUrl(Config.SERVER_URL+"/android/recommend");
		    WebSettings settings = webView.getSettings();
		    settings.setJavaScriptEnabled(true);
		    settings.setJavaScriptCanOpenWindowsAutomatically(true);
		    return mainView;
		}
		
	}
}
