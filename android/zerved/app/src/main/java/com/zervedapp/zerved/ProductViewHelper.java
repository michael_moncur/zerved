package com.zervedapp.zerved;

import java.util.ArrayList;

import com.zervedapp.zerved.models.ProductOption;

import android.content.Context;
import android.view.LayoutInflater;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.Spinner;

public class ProductViewHelper {
	Context mContext;
	
	public ProductViewHelper(Context context) {
		mContext = context;
	}
	
	public ArrayAdapter<Integer> getQuantityAdapter(int qty) {
		ArrayAdapter<Integer> quantityAdapter = new ArrayAdapter<Integer>(mContext, R.layout.item_label_list);
		quantityAdapter.clear();
		int maxQty = (qty > 20) ? qty : 20; // Show 1-20 as default, but expand to higher numbers if necessary for current quantiy
		for (int i=1; i<=maxQty; i++) {
			quantityAdapter.add(i);
		}
		return quantityAdapter;
	}
	
	public ArrayAdapter<Integer> getQuantityAdapter() {
		return getQuantityAdapter(0);
	}
	
	public Spinner getProductOptionSpinner(LayoutInflater inflater, ProductOption option) {
		// Create spinner for single selection
		Spinner optionView = (Spinner) inflater.inflate(R.layout.product_option_single, null);
		optionView.setTag(R.id.product_option, option);
		ArrayAdapter<String> optionAdapter = new ArrayAdapter<String>(mContext, R.layout.item_label_list);
		optionAdapter.clear();
		if (!option.getBooleanProperty("required")) {
			String title = String.format(mContext.getResources().getText(R.string.select_x).toString(), option.getName().toLowerCase());
			optionAdapter.add(title);
		}
		for (ProductOption.Value value: option.getValues()) {
			optionAdapter.add(value.toString());
		}
		optionView.setAdapter(optionAdapter);
		return optionView;
	}
	
	public Spinner getProductOptionSpinner(LayoutInflater inflater, ProductOption option, ArrayList<Integer> selectedValues) {
		// Create spinner and set selected value
		Spinner spinner = getProductOptionSpinner(inflater, option);
		if (selectedValues.size() > 0) {
			int selectedIndex = (option.getBooleanProperty("required")) ? selectedValues.get(0) : selectedValues.get(0)+1;
			spinner.setSelection(selectedIndex);
		}
		return spinner;
	}
	
	public CheckBox getProductOptionCheckbox(LayoutInflater inflater, ProductOption option, ProductOption.Value value) {
		// Create checkbox
		CheckBox checkbox = (CheckBox) inflater.inflate(R.layout.product_option_multiple, null);
		checkbox.setTag(R.id.product_option, option);
		checkbox.setTag(R.id.option_index, value.getIndex());
		checkbox.setText(value.toString());
		return checkbox;
	}
	
	public CheckBox getProductOptionCheckbox(LayoutInflater inflater, ProductOption option, ProductOption.Value value, ArrayList<Integer> selectedValues) {
		CheckBox checkbox = getProductOptionCheckbox(inflater, option, value);
		boolean checked = selectedValues.contains(value.getIndex());
		checkbox.setChecked(checked);
		return checkbox;
	}
}
