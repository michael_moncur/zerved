package com.zervedapp.zerved.models;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;

import android.util.Log;

public class ProductOption extends JSONModel {
	private static final String TAG = Product.class.getName();
	
	public ProductOption(String json) throws JSONException {
		super(json);
	}
	
	public String getName() {
		return getStringProperty("name");
	}
	
	public String getId() {
		return getStringProperty("id");
	}
	
	public String getSelectionType() {
		return getStringProperty("selection_type");
	}
	
	public boolean isRequired() {
		return getBooleanProperty("required");
	}
	
	public ArrayList<Value> getValues() {
		ArrayList<Value> list = new ArrayList<Value>();
		try {
			JSONArray values = this.getJSONArray("values");
			for (int i = 0; i < values.length(); i++) {
				list.add(new Value(values.getString(i), i));
			}
		}
		catch (JSONException e) {
            Log.e(TAG, "Failed to parse JSON.", e);
        }
		return list;
	}
	
	public static String [] getValuesLabels(ArrayList<Value> values){
		String [] seq = new String[values.size()];
		for(int i=0; i < values.size(); i++){
			Value v = values.get(i);
			seq[i] = v.getLabel() + " " + v.getPriceLabel();
		}
		return seq;
	}
	
	public class Value extends JSONModel {
		int mIndex;
		
		public Value(String json, int index) throws JSONException {
			super(json);
			mIndex = index;
		}
		
		public int getIndex() {
			return mIndex;
		}
		public String getLabel(){
			return this.getStringProperty("label");
		}
		public String getPriceLabel(){
			return this.getStringProperty("price_label");
		}
		public String toString() {
			return String.format("%s %s", getStringProperty("label"), getStringProperty("price_label"));
		}
	}
}
