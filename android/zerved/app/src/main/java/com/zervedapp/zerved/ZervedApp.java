package com.zervedapp.zerved;

import com.zervedapp.zerved.models.Bar;
import com.zervedapp.zerved.models.Cart;
import com.zervedapp.zerved.models.CartManager;
import com.zervedapp.zerved.models.ConsumerManager;
import com.zervedapp.zerved.models.Event;
import com.zervedapp.zerved.models.MerchantLocation;

import android.app.Application;
import android.util.Log;

import dk.danskebank.mobilepay.sdk.CaptureType;
import dk.danskebank.mobilepay.sdk.Country;
import dk.danskebank.mobilepay.sdk.MobilePay;

public class ZervedApp extends Application {
	CartManager mCartManager;
	ConsumerManager mConsumerManager;
	public MerchantLocation mLocation = null;
	public Event mEvent = null;

	@Override
	public void onCreate() {
		super.onCreate();

		// PROD: "APPDK7502734001"
		// TEST: "APPDK0000000000"
		MobilePay.getInstance().init("APPDK7502734001", Country.DENMARK);
		MobilePay.getInstance().setCaptureType(CaptureType.RESERVE);
		// Set the number of seconds from the MobilePay receipt are shown to the user returns to the merchant app. Default is 1.
		MobilePay.getInstance().setReturnSeconds(1);
		// Set the number of seconds the user has to complete the payment. Default is 0, which is no timeout.
		MobilePay.getInstance().setTimeoutSeconds(120);

	}

	public void init() {
		if (mCartManager == null) {
			mCartManager = new CartManager();
		}
		if (mConsumerManager == null) {
			mConsumerManager = new ConsumerManager();
		}
	}
	
	public Cart getCart(MerchantLocation location, Bar bar, String group, Event event) {
		init();
		return mCartManager.getCart(location, bar, group, event);
	}
	
	public void clearCart(MerchantLocation location, Bar bar, String group, Event event) {
		mCartManager.clearCart(location, bar, group, event);
	}
	
	public ConsumerManager getConsumer() {
		init();
		return mConsumerManager;
	}
	
	public void setTermsAccepted(boolean accepted) {
		mCartManager.setTermsAccepted(accepted);
	}
	
	public boolean termsAcceptedRecently() {
		return mCartManager.termsAcceptedRecently();
	}
}
