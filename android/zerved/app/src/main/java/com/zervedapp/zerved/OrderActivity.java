package com.zervedapp.zerved;

import org.json.JSONException;
import org.json.JSONObject;

import com.zervedapp.zerved.loader.RESTLoader;
import com.zervedapp.zerved.loader.RESTLoader.RESTResponse;
import com.zervedapp.zerved.models.ConsumerManager;
import com.zervedapp.zerved.models.Order;
import com.zervedapp.zerved.models.ZervedDbHelper;

import android.app.ActionBar;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.app.DialogFragment;
import android.content.Loader;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager.LayoutParams;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.TextView.OnEditorActionListener;

public class OrderActivity extends AuthorizedActivity {
	private static final String TAG = OrderActivity.class.getName();
	
	private static final int LOADER_ORDER = 0x1;
	private static final int LOADER_COMPLETE = 0x2;
	private static final int LOADER_CANCEL = 0x3;
	private static final int LOADER_SEND_RECEIPT = 0x4;
	private static final String ARGS_URI    = "com.zervedapp.zerved.ARGS_URI";
    private static final String ARGS_PARAMS = "com.zervedapp.zerved.ARGS_PARAMS";
    public static final String EXTRA_ORDER_KEY = "com.zervedapp.zerved.ORDER_KEY";
    private static final int UPDATE_INTERVAL = 1000*10; // 10 seconds
    
    private static String URI = Config.API_URL+"orders/";
    
    private boolean mLoginCancelled;
    private boolean mResumed = false;
    private String mOrderKey;
    private Order mOrder;
    private StaffPincodeFragment mStaffPincodeFragment;
    private Handler mUpdateHandler;
    private Runnable mUpdateRunner;
    ZervedDbHelper mDbHelper;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
        
        final ActionBar actionBar = getActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        setTitle(R.string.order);
        
        Intent intent = getIntent();
        mOrderKey = intent.getStringExtra(EXTRA_ORDER_KEY);
        
        mLoginCancelled = false;
        
        mDbHelper = new ZervedDbHelper(this);
        
        mUpdateHandler = new Handler();
        mUpdateRunner = new Runnable() {
			@Override
			public void run() {
				reload();
				mUpdateHandler.postDelayed(mUpdateRunner, UPDATE_INTERVAL);
			}
		};
        
        reload();
	}
	
	@Override
	public void onResume() {
		super.onResume();
		// Reload every time the activity is displayed
		// except first time where it's reloaded by onCreate
		if (mResumed) {
			reload();
		} else {
			mResumed = true;
		}
		// Update order page every 10 seconds
		mUpdateHandler.postDelayed(mUpdateRunner, UPDATE_INTERVAL);
	}
	
	@Override
	public void onPause() {
		mUpdateHandler.removeCallbacks(mUpdateRunner);
		super.onPause();
	}
	
	@Override
    protected void onSaveInstanceState(Bundle outState) {
    	if (mStaffPincodeFragment != null)
    		getFragmentManager().beginTransaction().remove(mStaffPincodeFragment).commit();
    	super.onSaveInstanceState(outState);
    }
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.order, menu);
		return true;
	}
	
	@Override
    public boolean onPrepareOptionsMenu(Menu menu) {
		for (int i=0; i<menu.size(); i++) {
    		MenuItem item = menu.getItem(i);
    		if (item.getItemId() == R.id.cancel) {
    			boolean canCancel = (mOrder != null && mOrder.getStringProperty("status").equals("pending"));
    			item.setEnabled(canCancel);
    			item.setVisible(canCancel);
    		} else if (item.getItemId() == R.id.send_receipt) {
    			boolean canSendReceipt = (mOrder != null && mOrder.getStringProperty("status").equals("complete"));
    			item.setEnabled(canSendReceipt);
    			item.setVisible(canSendReceipt);
    		}
		}
		return true;
	}
	
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent upIntent = new Intent(this, OrdersActivity.class);
                upIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(upIntent);
                finish();
                /*
                if (NavUtils.shouldUpRecreateTask(this, upIntent)) {
                    // This activity is not part of the application's task, so create a new task
                    // with a synthesized back stack.
                    TaskStackBuilder.from(this)
                            .addNextIntent(upIntent)
                            .startActivities();
                    finish();
                } else {
                    // This activity is part of the application's task, so simply
                    // navigate up to the hierarchical parent activity.
                    NavUtils.navigateUpTo(this, upIntent);
                }
                */
                return true;
            case R.id.reload:
            	reload();
            	return true;
            case R.id.cancel:
            	cancelOrder(null);
            	return true;
            case R.id.send_receipt:
            	sendReceipt(null);
            	return true;
        }
        return super.onOptionsItemSelected(item);
    }
    
    public void completeOrder(View v) {
    	mStaffPincodeFragment = new StaffPincodeFragment();
    	mStaffPincodeFragment.show(getFragmentManager(), null);
    }
    
    public void staffPincodeCancel(View v) {
    	mStaffPincodeFragment.dismiss();
    }
    
    public void staffPincodeAccept(View v) {
    	EditText input = (EditText) mStaffPincodeFragment.getView().findViewById(R.id.pincode);
    	String code = input.getText().toString();
    	if(mOrder == null || mOrder.getEntrancePincode() == null) {
			Toast toast = Toast.makeText(OrderActivity.this, R.string.no_connection, Toast.LENGTH_SHORT);
			toast.setGravity(Gravity.CENTER, 0, 0);
        	toast.show();
    	}
    	else if (code.equals(mOrder.getEntrancePincode())) {
			// Submit order completion
			JSONObject postParams = new JSONObject();
	    	try {
	    		postParams.put("entrance_pincode", code);
	    		postParams.put("status", "complete");
	    	} catch (JSONException e) {
	    		Log.e(TAG, "Failed to create JSON.", e);
	    	}
			Bundle args = new Bundle();
	    	args.putParcelable(ARGS_URI, Uri.parse(URI+mOrderKey));
	    	Bundle params = new Bundle();
	    	params.putString("json", postParams.toString());
	    	args.putParcelable(ARGS_PARAMS, params);
			getLoaderManager().restartLoader(LOADER_COMPLETE, args, OrderActivity.this);
			mStaffPincodeFragment.dismiss();
		} else {
			Toast toast = Toast.makeText(OrderActivity.this, R.string.wrong_code, Toast.LENGTH_SHORT);
			toast.setGravity(Gravity.CENTER, 0, 0);
        	toast.show();
		}
    }
    
    public void cancelOrder(View v) {
    	AlertDialog.Builder alert = new AlertDialog.Builder(this);
		alert.setMessage(R.string.confirm_cancel_order);
		alert.setPositiveButton(R.string.cancel_order, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
				// Submit cancellation
				JSONObject postParams = new JSONObject();
		    	try {
		    		postParams.put("status", "cancelled");
		    	} catch (JSONException e) {
		    		Log.e(TAG, "Failed to create JSON.", e);
		    	}
				Bundle args = new Bundle();
		    	args.putParcelable(ARGS_URI, Uri.parse(URI+mOrderKey));
		    	Bundle params = new Bundle();
		    	params.putString("json", postParams.toString());
		    	args.putParcelable(ARGS_PARAMS, params);
				getLoaderManager().restartLoader(LOADER_CANCEL, args, OrderActivity.this);
			}
		});
		alert.setNegativeButton(R.string.back, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
				// Canceled.
			}
		});
		alert.show();
    }
    
    public void sendReceipt(View v) {
    	Bundle args = new Bundle();
    	args.putParcelable(ARGS_URI, Uri.parse(URI+mOrderKey+"/sendreceipt"));
    	Bundle params = new Bundle();
    	args.putParcelable(ARGS_PARAMS, params);
		getLoaderManager().restartLoader(LOADER_SEND_RECEIPT, args, OrderActivity.this);
    }
	
	@Override
	public void reload() {
		loadFromCache();
		ConsumerManager consumer = getConsumer();
		if (consumer.mConsumerKey == null || consumer.mAccessToken == null) {
			//setContentView(R.layout.activity_authorized);
			if (!mLoginCancelled) {
				login();
			}
		} else {
			Bundle args = new Bundle();
	    	args.putParcelable(ARGS_URI, Uri.parse(URI+mOrderKey));
	    	args.putParcelable(ARGS_PARAMS, new Bundle());
			getLoaderManager().restartLoader(LOADER_ORDER, args, this);
		}
	}
	
	@Override
	public Loader<RESTResponse> onCreateLoader(int id, Bundle args) {
		if (args != null && args.containsKey(ARGS_URI) && args.containsKey(ARGS_PARAMS)) {
            Uri    action = args.getParcelable(ARGS_URI);
            Bundle params = args.getParcelable(ARGS_PARAMS);
            setProgressBarIndeterminateVisibility(true);
            if (id == LOADER_ORDER) {
    	        return new RESTLoader(this, RESTLoader.HTTPVerb.GET, action, params, getConsumer().mAccessToken);
    		} else if (id == LOADER_COMPLETE || id == LOADER_CANCEL) {
    			return new RESTLoader(this, RESTLoader.HTTPVerb.POST, action, params, getConsumer().mAccessToken);
    		} else if (id == LOADER_SEND_RECEIPT) {
    			Toast.makeText(this, R.string.sending_receipt, Toast.LENGTH_SHORT).show();
    			return new RESTLoader(this, RESTLoader.HTTPVerb.POST, action, params, getConsumer().mAccessToken);
    		}
		}
		return super.onCreateLoader(id, args);
	}
	
	@Override
	public void onLoadFinished(Loader<RESTResponse> loader, RESTResponse data) {
		setProgressBarIndeterminateVisibility(false);
		int    code = data.getCode();
        String json = data.getData();
        if (loader.getId() == LOADER_ORDER || loader.getId() == LOADER_COMPLETE) {
        	if (code == 200) {
	        	try {
	        		// Build order view
					Order order = new Order(json);
					mOrder = order;
					refreshView(order);
					SQLiteDatabase db = mDbHelper.getWritableDatabase();
					order.saveToCache(db);
				} catch (JSONException e) {
					Log.e(TAG, "Failed to parse JSON.", e);
				}
        	} else if (code > 0) {
        		super.onLoadFinished(loader, data);
        	} else {
        		// Conection failed
        		mUpdateHandler.removeCallbacks(mUpdateRunner);
        		Toast.makeText(this, R.string.no_connection, Toast.LENGTH_SHORT).show();
        	}
        } else if (loader.getId() == LOADER_CANCEL) {
        	if (code == 200) {
        		// Order cancelled - finish activity
        		Toast.makeText(this, R.string.order_cancelled, Toast.LENGTH_SHORT).show();
        		finish();
        	} else if (code > 0) {
        		Toast.makeText(this, json, Toast.LENGTH_SHORT).show();
        	} else {
        		Toast.makeText(this, R.string.no_connection, Toast.LENGTH_SHORT).show();
        	}
        } else if (loader.getId() == LOADER_SEND_RECEIPT) {
        	if (code == 200) {
        		Toast.makeText(this, R.string.receipt_sent, Toast.LENGTH_SHORT).show();
        	} else if (code > 0) {
        		Toast.makeText(this, json, Toast.LENGTH_SHORT).show();
        	} else {
        		Toast.makeText(this, R.string.no_connection, Toast.LENGTH_SHORT).show();
        	}
        }
        super.onLoadFinished(loader, data);
	}
	
	private boolean isPaiiOrder(Order order) {
		return order.has("status") && order.getStringProperty("status").equals("pending_payment");
	}
	
	public void refreshView(Order order) {
		LayoutInflater inflater = getLayoutInflater();
    	View view = inflater.inflate(R.layout.order, null);
    	
    	// Title bar
    	setTitle(String.format(getResources().getString(R.string.queue_x), order.getStringProperty("queue_number")));
    	
    	// Status message
    	view.findViewById(R.id.status_message).setVisibility((order.getStatusTitle() != null || order.getStatusDescription() != null) ? View.VISIBLE : View.GONE);
    	if(order.has("event_name") && order.getStringProperty("event_name").length() > 0 && !isPaiiOrder(order)) {
    		// Event order
    		view.findViewById(R.id.status_message).setBackgroundColor(getResources().getColor(R.color.orange));
    	} else if (isPaiiOrder(order)) {
    		view.findViewById(R.id.status_message).setBackgroundColor(getResources().getColor(R.color.red));
    	} else {
    		// Realtime order
    		view.findViewById(R.id.status_message).setBackgroundColor(getResources().getColor(R.color.green));
    	}
    	TextView statusTitle = (TextView) view.findViewById(R.id.status_title);
    	statusTitle.setText(order.getStatusTitle());
    	statusTitle.setVisibility(order.getStatusTitle() != null ? View.VISIBLE : View.GONE);
    	TextView statusDescription = (TextView) view.findViewById(R.id.status_description);
    	statusDescription.setText(order.getStatusDescription());
    	statusDescription.setVisibility(order.getStatusDescription() != null ? View.VISIBLE : View.GONE);
    	
    	// Order number
    	TextView queue_number = (TextView) view.findViewById(R.id.order_number_big);
    	queue_number.setText(String.format(getResources().getString(R.string.queue_x), order.getStringProperty("queue_number")));	
    	
    	// Status progress
    	TextView status = (TextView) view.findViewById(R.id.status_value);
    	status.setText(order.getStringProperty("status_label"));
    	if(order.getStringProperty("status").equals("pending")){
    		status.setTextColor(getResources().getColor(R.color.orange));
    	}
    	
    	// Event info
    	if(order.has("event_name") && order.getStringProperty("event_name").length() > 0){      	
    		view.findViewById(R.id.event_info_container).setVisibility(View.VISIBLE);
        	((TextView) view.findViewById(R.id.event_name)).setText(order.getStringProperty("event_name") + " " + order.getStringProperty("event_time_label"));
        	((TextView) view.findViewById(R.id.event_date)).setText(order.getStringProperty("date_delivery_formatted"));
    	}   
    	// Waiting time
    	else if (order.getIntegerProperty("estimated_waiting_time") > 0) { 
    		view.findViewById(R.id.waiting_time_container).setVisibility(View.VISIBLE);
    		((TextView) view.findViewById(R.id.waiting_time_minutes)).setText(String.format(getResources().getString(R.string.x_min), order.getIntegerProperty("estimated_waiting_time")));
    	}
    	
    	// Service
    	String service = order.getServiceSummary(this);
    	if (service == null) {
    		view.findViewById(R.id.service_container).setVisibility(View.GONE);
    	} else {
    		view.findViewById(R.id.service_container).setVisibility(View.VISIBLE);
    		((TextView) view.findViewById(R.id.service_value)).setText(service);
    	}
    	
    	// Staff button for order completion
    	boolean staffCompletionEnabled = order.getStaffCompletionEnabled();
    	view.findViewById(R.id.for_staff_only).setVisibility(staffCompletionEnabled ? View.VISIBLE : View.GONE);
    	view.findViewById(R.id.complete_order).setVisibility(staffCompletionEnabled ? View.VISIBLE : View.GONE);
    	((Button)view.findViewById(R.id.complete_order)).setText(order.getStringProperty("service").equals("entrance") ? R.string.staff_accept_order : R.string.staff_complete_order);
    	
    	// Receipt header information
    	String locationStr = fitStringToLength(order.getLocationName(), 21);
    	String headerData = locationStr + "\n";
    	if (order.getStringProperty("status").equals("complete")) {
    		headerData += order.getStringProperty("date_completed_formatted");
    	} else {
    		headerData += order.getStringProperty("date_created_formatted");
    	}
    	headerData += "\n" + String.format(getResources().getString(R.string.order_x), order.getStringProperty("order_number"));
    	TextView receiptDataView = (TextView) view.findViewById(R.id.receipt_data);
    	receiptDataView.setText(headerData);
    	TextView zervedAddress = (TextView) view.findViewById(R.id.zerved_address);
    	zervedAddress.setText(order.getStringProperty("company_name")+"\n"+String.format(getResources().getString(R.string.vat_no_x), order.getStringProperty("company_vat")));
    	
    	// Order items
    	LinearLayout itemRows = (LinearLayout) view.findViewById(R.id.items);
    	for (Order.Item item : order.getItems()) {
    		View itemView = inflater.inflate(R.layout.item_order_item, null);
    		((TextView) itemView.findViewById(R.id.quantity)).setText(String.format("%s x ", item.getStringProperty("quantity")));
    		((TextView) itemView.findViewById(R.id.name)).setText(item.getStringProperty("name"));
    		((TextView) itemView.findViewById(R.id.price)).setText(item.getStringProperty("total_before_loyalty_discount_incl_tax_formatted"));
    		TextView options = (TextView) itemView.findViewById(R.id.options);
    		String optionsSummary = item.getOptionsSummary();
    		options.setVisibility(optionsSummary.equals("") ? View.GONE : View.VISIBLE);
    		options.setText(optionsSummary);
    		itemRows.addView(itemView);
    	}
    	
    	// Discount
    	LinearLayout discountRows = (LinearLayout) view.findViewById(R.id.discounts);
    	if(order.getDoubleProperty("total_loyalty_discount_incl_tax") != 0) {
    		View discountView = inflater.inflate(R.layout.item_order_discount, null);
    		((TextView) discountView.findViewById(R.id.discountprice)).setText(order.getStringProperty("total_loyalty_discount_incl_tax_formatted"));
    		discountRows.addView(discountView);
    	}
    	
    	// Totals
    	LinearLayout totalsRows = (LinearLayout) view.findViewById(R.id.totals);
    	TextView totalExclTax = (TextView) inflater.inflate(R.layout.item_order_total, null);
    	totalExclTax.setText(String.format(getResources().getString(R.string.total_excl_tax_x), order.getStringProperty("total_excl_tax_formatted")));
    	totalsRows.addView(totalExclTax);
    	for (Order.TaxTotal taxTotal: order.getTaxTotals()) {
    		TextView taxTotalView = (TextView) inflater.inflate(R.layout.item_order_total, null);
    		taxTotalView.setText(formatTaxTotal(taxTotal));
    		totalsRows.addView(taxTotalView);
    	}
    	TextView totalInclTax = (TextView) inflater.inflate(R.layout.item_order_total, null);
    	totalInclTax.setTypeface(Typeface.MONOSPACE, Typeface.BOLD);
    	totalInclTax.setText(String.format(getResources().getString(R.string.total_incl_tax_x), order.getStringProperty("total_incl_tax_formatted")));
    	totalsRows.addView(totalInclTax);
    	
    	// Money status
    	TextView moneyStatus = (TextView) view.findViewById(R.id.money_status);
    	if (order.getStringProperty("status").equals("pending") || order.getStringProperty("status").equals("preparing")) {
    		moneyStatus.setVisibility(View.VISIBLE);
    		moneyStatus.setText(R.string.money_status_pending);
    	} else if (order.getStringProperty("status").equals("complete")) {
    		moneyStatus.setVisibility(View.VISIBLE);
    		moneyStatus.setText(R.string.money_status_complete);
    	} else {
    		moneyStatus.setVisibility(View.GONE);
    	}
    	
    	// Send receipt button
    	view.findViewById(R.id.send_receipt).setVisibility(order.getStringProperty("status").equals("complete") ? View.VISIBLE : View.GONE);
    	
    	// Cancel order button
    	view.findViewById(R.id.cancel_order).setVisibility(order.getStringProperty("status").equals("pending") ? View.VISIBLE : View.GONE);
    	
    	setContentView(view);
    	invalidateOptionsMenu();
    	
    	// Register for notifications
    	getConsumer().registerGcm(this);
	}
	
	private String formatTaxTotal(Order.TaxTotal taxtotal) {
		String result = fitStringToLength(taxtotal.getStringProperty("name"), 21);
		result = result + " " + taxtotal.getStringProperty("rate") + " %: " + taxtotal.getStringProperty("total_formatted");
		return result;
	}
	
	private String fitStringToLength(String str, int maxLength) {
		String result = str;
		if(result.length() > maxLength) {
			result = result.substring(0, maxLength-3);
			result = result + "...";
		}
		return result;
	}
	
	public void loadFromCache() {
		SQLiteDatabase db = mDbHelper.getReadableDatabase();
		Order order = Order.loadFromCache(db, mOrderKey);
		if (order != null && order.getPropertySet().equals("complete")) {
			refreshView(order);
		}
	}
	
	public static class StaffPincodeFragment extends DialogFragment {
		@Override
		public View onCreateView(LayoutInflater infl, ViewGroup container, Bundle savedInstanceState) {
			LayoutInflater inflater = getActivity().getLayoutInflater();
			View view = inflater.inflate(R.layout.dialog_staff_pincode, null);
			
			final EditText input = (EditText) view.findViewById(R.id.pincode);
			input.setOnEditorActionListener(new OnEditorActionListener() {
				@Override
				public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
				    if (event != null && event.getAction() != KeyEvent.ACTION_DOWN)
				        return false;
					if (event == null || actionId == EditorInfo.IME_ACTION_GO || event.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
						((OrderActivity) StaffPincodeFragment.this.getActivity()).staffPincodeAccept(v);
						return true;
					}
					return false;
				}
			});
			
			getDialog().getWindow().setSoftInputMode(LayoutParams.SOFT_INPUT_STATE_VISIBLE);
			return view;
		}
		
		@Override
	    public Dialog onCreateDialog(Bundle savedInstanceState) {
			Dialog dialog = super.onCreateDialog(savedInstanceState);
	        dialog.setTitle(R.string.enter_staff_pin);
	        return dialog;
		}
	}
}
