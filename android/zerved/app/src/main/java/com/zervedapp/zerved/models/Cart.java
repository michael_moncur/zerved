package com.zervedapp.zerved.models;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

public class Cart {
	private static final String TAG = Cart.class.getName();
	
	private MerchantLocation mLocation;
	private Bar mBar;
	private String mGroup;
	private Event mEvent;
	
	private ArrayList<Item> mItems;
	public String mService;
	public String mTableNumber;
	public boolean mToGo;
	public boolean mTermsAccepted;
	public String mCard;
	public String mPassword;
	public String mCurrency;
	public double mAmount;
	public String mObscuredCardNumber;
	public String mNordpayRegistration;
	public String mNordpayAuthorization;
	public String mNordpayMethod;
	public double mTipPercent;
	public double mSubtotal;
	// Address delivery
	public String mName;
	public String mAddress1;
	public String mAddress2;
	public String mCity;
	public String mPostcode;
	public String mPhone;
	public String mLoyaltyCode;
	public String mDeliveryInstructions;
	
	public Cart(MerchantLocation location, Bar bar, String group, Event event) {
		mGroup = group;
		mService = "";
		setLocation(location);
		setBar(bar);
		mEvent = event;
		mTableNumber = "";
		mTermsAccepted = false;
		mItems = new ArrayList<Item>();
		mTipPercent = 0;
		mToGo = false;
		mName = mAddress1 = mAddress2 = mCity = mPostcode = mPhone = "";
		mSubtotal = 0.0;
		mLoyaltyCode = null;
		mDeliveryInstructions = "";
	}
	
	public MerchantLocation getMerchant() {
		return mLocation;
	}
	
	public void setLocation(MerchantLocation merchant) {
		mLocation = merchant;
    	if (mGroup.equals("entrance")) {
    		mService = "entrance";
    	}
	}
	
	public void setBar(Bar bar) {
		mBar = bar;
    	if (bar != null && bar.getBooleanProperty("counter_service") && !bar.getBooleanProperty("table_service") && !bar.hasToStayAndToGo() && !bar.hasDeliveryService()) {
    		mService = "counter";
    		if (bar.hasCounterServiceToGo()) {
    			mToGo = true;
    		} else if (bar.hasCounterServiceToStay()) {
    			mToGo = false;
    		}
    	}
    	else if (bar != null && !bar.getBooleanProperty("counter_service") && bar.getBooleanProperty("table_service") && !bar.hasDeliveryService()) {
    		mService = "table";
    	}
	}
	
	public double getSubtotal() {
		return mSubtotal;
	}
	
	public Bar getBar(){
		return this.mBar;
	}
	
	public String getGroup() {
		return mGroup;
	}
	
	public Event getEvent() {
		return mEvent;
	}
	
	public boolean hasEvent() {
		return getEvent() != null;
	}
	
	public void addItem(Item item) {
		int index = mItems.indexOf(item);
		if (index == -1) {
			mItems.add(item);
		} else {
			mItems.get(index).addQuantity(item.mQuantity);
		}
	}
	
	public void updateItem(Item oldItem, Item newItem) {
		int index = mItems.indexOf(oldItem);
		// Remove row
		removeItem(index);
		// If other row matches the new configuration
		if (mItems.contains(newItem)) {
			// Merge with matching row
			addItem(newItem);
		} else {
			// Else add new row at old position
			mItems.add(index, newItem);
		}
	}
	
	public void removeItem(int index) {
		mItems.remove(index);
	}
	
	public void removeItem(Item item) {
		mItems.remove(item);
	}
	
	public ArrayList<Item> getItems() {
		return mItems;
	}
	
	public int getEstimatedWaitingTime() {
		if (mEvent != null) {
			return 0; // No waiting time for event orders
		}
		if ((mService.equals("counter") || mService.equals("table")) && mBar.getIntegerProperty("estimated_waiting_time") > 0) {
			return mBar.getIntegerProperty("estimated_waiting_time");
		}
		if (mService.equals("delivery") && mBar.getIntegerProperty("estimated_waiting_time_delivery") > 0) {
			return mBar.getIntegerProperty("estimated_waiting_time_delivery");
		}
		return 0;
	}
	
	public int size() {
		int size = 0;
		for (Item item: mItems) {
			size += item.mQuantity;
		}
		return size;
	}
	
	public void addQuoteItem(int index, JSONObject item) {
		mItems.get(index).addQuoteItem(item);
	}
	
	public int validateQuoteItems() {
		int invalidItems = 0;
		ArrayList<Item> newItems = new ArrayList<Item>();
		for (Item item: mItems) {
			if (item.getQuoteItem().getBooleanProperty("valid")) {
				newItems.add(item);
			} else {
				invalidItems++;
			}
		}
		mItems = newItems;
		return invalidItems;
	}
	
	public String toString() {
		String description = mLocation.getKey() + "_" + mGroup;
		for (Item item: mItems) {
			description += "\n" + item.toString();
		}
		return description;
	}
	
	public JSONObject toJson() {
		JSONObject json = new JSONObject();
		try {
			json.put("group", mGroup);
			if (mGroup.equals("menu")) {
				json.put("bar", mBar.getKey());
			} else {
				json.put("location", mLocation.getKey());
			}
			if(mEvent != null){
				json.put("event", mEvent.getKey());
			}
			json.put("loyalty_code", mLoyaltyCode);
			json.put("service", mService);
			if(mService.equals("delivery")){
				json.put("delivery_name", mName);
				json.put("delivery_address1", mAddress1);
				json.put("delivery_address2", mAddress2);
				json.put("delivery_postcode", mPostcode);
				json.put("delivery_city", mCity);
				json.put("delivery_phone", mPhone);
				json.put("delivery_instructions", mDeliveryInstructions);
			}
			json.put("table_number", mTableNumber);
			json.put("terms_accepted", mTermsAccepted);
			json.put("to_go", mToGo);
			json.put("card", mCard);
			json.put("tip_percent", mTipPercent);
			if (mPassword != null) json.put("pincode", mPassword);
			if (mObscuredCardNumber != null) json.put("obscured_card_number", mObscuredCardNumber);
			if (mNordpayRegistration != null) json.put("nordpay_registration", mNordpayRegistration);
			if (mNordpayAuthorization != null) json.put("nordpay_authorization", mNordpayAuthorization);
			if (mNordpayMethod != null) json.put("nordpay_method", mNordpayMethod);
			JSONArray items = new JSONArray();
			for (Item item: mItems) {
				JSONObject jsonItem = new JSONObject();
				jsonItem.put("product", item.mProduct.getKey());
				jsonItem.put("qty", item.mQuantity);
				JSONArray jsonItemOptions = new JSONArray();
				for (OptionValue option: item.mOptions) {
					JSONObject jsonItemOption = new JSONObject();
					jsonItemOption.put("option_key", option.mOptionKey);
					jsonItemOption.put("option_value", option.mSelectedIndex);
					jsonItemOptions.put(jsonItemOption);
				}
				jsonItem.put("options", jsonItemOptions);
				items.put(jsonItem);
			}
			json.put("items", items);
		}
		catch (JSONException e) {
			Log.e(TAG, "Failed to create JSON.", e);
		}
		return json;
	}
	
	public class Item {
		protected Product mProduct;
		protected int mQuantity;
		protected ArrayList<OptionValue> mOptions;
		protected JSONModel mQuoteItem;
		
		public Item(Product product, int quantity, ArrayList<OptionValue> options) {
			mProduct = product;
			mQuantity = quantity;
			mOptions = options;
		}
		
		public void addQuantity(int quantity) {
			if(mQuantity + quantity < 1)
				mQuantity = 1;
			else mQuantity += quantity;
		}
		
		public int getQuantity(){
			return mQuantity;
		}
		
		public boolean equals(Object object) {
			if (this == object) return true;
			if (!(object instanceof Item)) return false;
			
			Item item = (Item) object;
			return (item.mProduct.getKey().equals(mProduct.getKey()) && item.mOptions.equals(mOptions));
		}
		
		public JSONModel getQuoteItem() {
			return mQuoteItem;
		}
		
		public void addQuoteItem(JSONObject item) {
			try {
				mQuoteItem = new JSONModel(item.toString());
			}
			catch (JSONException e) {
				Log.e(TAG, "Failed to parse JSON.", e);
			}
		}
		
		public ArrayList<Integer> getValuesForOption(String optionKey) {
			ArrayList<Integer> values = new ArrayList<Integer>();
			for (OptionValue option: mOptions) {
				if (option.mOptionKey.equals(optionKey)) {
					values.add(option.mSelectedIndex);
				}
			}
			return values;
		}
		
		public ArrayList<OptionValue> getOptions(){
			return mOptions;
		}
		
		public Product getProduct() {
			return mProduct;
		}
		
		public String toString() {
			String description = mQuantity + " x " + mProduct.getKey();
			for (OptionValue option: mOptions) {
				description += " " + option.toString();
			}
			return description;
		}
	}
}
