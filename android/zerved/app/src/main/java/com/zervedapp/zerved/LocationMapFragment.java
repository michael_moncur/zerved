package com.zervedapp.zerved;

import android.os.Bundle;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class LocationMapFragment extends MapFragment {
    private double lat, lon;
    private String locationName;
    
    public static LocationMapFragment create(double lat, double lon, String locationName) {
    	LocationMapFragment fragment = new LocationMapFragment();
        Bundle args = new Bundle();        
        args.putDouble("lat", lat);
        args.putDouble("lon", lon);     
        args.putString("locationName", locationName);       
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.lat = getArguments().getDouble("lat");
        this.lon = getArguments().getDouble("lon");     
        this.locationName = getArguments().getString("locationName");
    }

    @Override
    public void onResume() {
        super.onResume();
        GoogleMap mMap = super.getMap();
        if (mMap != null) {
        	
        	LatLng locationPosition = new LatLng(this.lat, this.lon); 
        	
        	UiSettings settings = mMap.getUiSettings();
        	settings.setScrollGesturesEnabled(false);
        	settings.setZoomControlsEnabled(false);

        	mMap.addMarker(new MarkerOptions().position(locationPosition).title(this.locationName));
	        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(locationPosition, 15));
	        mMap.animateCamera(CameraUpdateFactory.zoomTo(17), 200, null); 
	        
        }
    }
}