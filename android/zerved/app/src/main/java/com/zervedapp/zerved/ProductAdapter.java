package com.zervedapp.zerved;

import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;
import com.zervedapp.zerved.loader.RESTLoader;
import com.zervedapp.zerved.loader.RESTLoader.RESTResponse;
import com.zervedapp.zerved.models.Cart;
import com.zervedapp.zerved.models.Event;
import com.zervedapp.zerved.models.OptionValue;
import com.zervedapp.zerved.models.Product;
import com.zervedapp.zerved.models.ProductOption;
import com.zervedapp.zerved.models.ProductOption.Value;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Bundle;
import android.app.LoaderManager.LoaderCallbacks;
import android.content.Loader;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class ProductAdapter extends ArrayAdapter<Product> implements LoaderCallbacks<RESTResponse> {

	ProgressDialog mDialog;
	
	public ProductAdapter(Context context, int textViewResourceId) {
		super(context, textViewResourceId);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View row;
		LayoutInflater inflater = LayoutInflater.from(getContext());
		if (null == convertView) {
			row = inflater.inflate(R.layout.product, null);
		} else {
			row = convertView;
		}
		
		// Product object
		final Product product = this.getItem(position);

		final Cart cart =  ((ZervedApp)((ProductsActivity)getContext()).getApplication()).getCart(((ProductsActivity)getContext()).mLocation, 
																									((ProductsActivity)getContext()).mBar,
																									((ProductsActivity)getContext()).mGroup,
																									((ProductsActivity)getContext()).mEvent);
		ArrayList<Cart.Item> cartItems = cart.getItems();   
				
		LinearLayout selectedProducts = (LinearLayout) row.findViewById(R.id.selected_products);
		selectedProducts.removeAllViews();
		for(final Cart.Item item : cartItems)
		{
			if(item.getProduct().getKey().equals(product.getKey()))
			{
				RelativeLayout selectedProduct = (RelativeLayout) inflater.inflate(R.layout.selected_product, null);
				selectedProducts.addView(selectedProduct);
				
				final TextView productQuantity = (TextView) selectedProduct.findViewById(R.id.product_quantity);
				TextView productOptionsLabel = (TextView) selectedProduct.findViewById(R.id.product_desc);
				final TextView productPrice = (TextView) selectedProduct.findViewById(R.id.product_price);
				
				ImageButton plusBtn = (ImageButton) selectedProduct.findViewById(R.id.plus);
				ImageButton minusBtn = (ImageButton) selectedProduct.findViewById(R.id.minus);

				
				// Quantity and product name
				productQuantity.setText(Integer.toString(item.getQuantity()));
		
				// Options
	    		String options = "";	        		
	    		ArrayList<ProductOption> productOptions = product.getProductOptions();
	        	for(OptionValue option: item.getOptions()){
	        		for(ProductOption productOption: productOptions){
	        			if(option.getId().equals(productOption.getId())){
	        				for(Value v: productOption.getValues()){
	        					if(v.getIndex() == option.getValue()){
	        						options += productOption.getName() + ": " + v.getLabel() + ", ";
	        					}
	        				} 
	        			}
	        		}
	    		}
	        	if(options.length() > 0) {
	        		options = options.substring(0, options.length()-2);
	        	}
	        	productOptionsLabel.setText(options);
	        	if(options.equals(""))
	        		productOptionsLabel.setVisibility(View.GONE);
	        	
	        	// Price
	        	if(item.getQuoteItem() != null)
	        		productPrice.setText(item.getQuoteItem().getStringProperty("price_formatted"));
	        	
	        	// Quantity
	        	plusBtn.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						item.addQuantity(1);
						productQuantity.setText(Integer.toString(item.getQuantity()));
						productPrice.setText("--");
			    		updateView();
					}
				});	        	 
	        	minusBtn.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						if(item.getQuantity() <= 1){
							((View)(v.getParent().getParent())).setVisibility(View.GONE);
							cart.removeItem(item);
						}
						else {
							item.addQuantity(-1);
							productQuantity.setText(Integer.toString(item.getQuantity()));
							productPrice.setText("--");
						}						
						updateView();
					}
				});        	
	        	
			}
        	
		}
		
		// Name, description, price
		TextView name = (TextView) row.findViewById(R.id.name);
		TextView description = (TextView) row.findViewById(R.id.product_desc);
		TextView price = (TextView) row.findViewById(R.id.price);
		TextView oldprice = (TextView) row.findViewById(R.id.old_price);
		
		((View)name.getParent().getParent()).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(((ProductsActivity)getContext()), ProductActivity.class);
		    	intent.putExtra(CategoryListFragment.EXTRA_LOCATION, ((ProductsActivity)getContext()).mLocation.toJson());	
		    	if(((ProductsActivity)getContext()).mBar != null){
		    		intent.putExtra(CategoryListFragment.EXTRA_BAR, ((ProductsActivity)getContext()).mBar.toJson());
		    	}
		    	intent.putExtra(CategoryListFragment.EXTRA_PRODUCT, product.toJson());
		    	intent.putExtra(CategoryListFragment.EXTRA_GROUP, ((ProductsActivity)getContext()).mGroup);
		    	((ProductsActivity)getContext()).startActivity(intent);				
			}
		});
		
		name.setText(product.getName());
		description.setText(product.getDescription());
		price.setText(product.getStringProperty("final_price_formatted"));
		if (product.getDoubleProperty("final_price") < product.getDoubleProperty("base_price")) {
			oldprice.setText(product.getStringProperty("base_price_formatted"));
			oldprice.setPaintFlags(oldprice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
		} else {
			oldprice.setText("");
		}
		
		// Tier prices
		LinearLayout tierPrices = (LinearLayout) row.findViewById(R.id.tier_prices);
		
		tierPrices.removeAllViews();
		for (String tierPrice: product.getTieredPriceDescriptions()) {
			TextView tierPriceView = (TextView) inflater.inflate(R.layout.tier_price, null);
			tierPriceView.setText(tierPrice);
			tierPrices.addView(tierPriceView);
		}
		
		// Status message when merchant is open or closed
		Event mEvent = ((ZervedApp)((ProductsActivity)getContext()).getApplication()).mEvent;
		boolean open = (mEvent != null) ? mEvent.isOpenForOrders() : product.isOrderable();
		
		LinearLayout closedLayout = (LinearLayout) row.findViewById(R.id.merchant_closed_layout);
		TextView closedErrorView = (TextView) row.findViewById(R.id.merchant_closed);
		closedErrorView.setText(product.getStringProperty("message_when_closed"));
		closedLayout.setVisibility(!open && position == 0 ? View.VISIBLE : View.GONE);

		LinearLayout openLayout = (LinearLayout) row.findViewById(R.id.merchant_open_layout);
		TextView openTextView = (TextView) row.findViewById(R.id.merchant_open);
		openTextView.setText(R.string.open_for_orders);
		openLayout.setVisibility(open && position == 0 ? View.VISIBLE : View.GONE);
	
		
		// Event Summary
		if(mEvent != null && position == 0){
			((TextView) row.findViewById(R.id.event_name)).setText(mEvent.getName() + " " + mEvent.getTimeStartFormatted());
			((TextView) row.findViewById(R.id.event_date_start_formatted)).setText(mEvent.getDateOnlyStartFormatted());
			((TextView) row.findViewById(R.id.event_time_label)).setText(R.string.serving_time);
			((TextView) row.findViewById(R.id.event_date_delivery_time_formatted)).setText(mEvent.getTimeDeliveryFormatted() + " (" + mEvent.getTimeLabel() + ")");
		
		}
		else row.findViewById(R.id.event_summary).setVisibility(View.GONE);
		
		
		// Add to cart button
		ImageButton addtocart = (ImageButton) row.findViewById(R.id.add_to_cart);
		addtocart.setTag(product);
		addtocart.setEnabled(open);
		if(!open) { 
			addtocart.setBackgroundResource(R.drawable.plusbutton_disabled);
		} 
		else {
			addtocart.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
			    	resetOptions();
			        askForOption(product, 0);
				}
			});
		}

		return row;
	}
	
	
	ArrayList<OptionValue> options = new ArrayList<OptionValue>();
    int checkedItems = 0;
    public void addOption(OptionValue v){
    	if(!this.options.contains(v))
    		this.options.add(v);
    }
    public void removeOption(OptionValue v){
    	if(this.options.contains(v))
    		this.options.remove(v);
    }
    public ArrayList<OptionValue> getOptions(){
    	return this.options;
    }
    public void resetOptions(){
    	this.options = new ArrayList<OptionValue>();
    }
     	
    public void addCheckedItems(int n){
    	this.checkedItems += n;
    }
    
    public void askForOption(final Product product, final int i){

    	final ArrayList<ProductOption> productOptions = product.getProductOptions();
    	
    	if(i < productOptions.size()){
    		  final ProductOption option = productOptions.get(i);
    		  
    		  if (option.getSelectionType().equals("single")) {
    			int selDefault = -1;
    			if(option.isRequired())
    			{
    				selDefault = 0;
        			OptionValue v = new OptionValue(option.getKey(), option.getId(), selDefault);
        			addOption(v);
    			}
    			
              	AlertDialog.Builder builder = new AlertDialog.Builder((ProductsActivity)getContext());
              	builder.setTitle(option.getName())
          		     .setSingleChoiceItems(ProductOption.getValuesLabels(option.getValues()), selDefault, new DialogInterface.OnClickListener() {
          				@Override
          				public void onClick(DialogInterface dialog, int which) {
          					OptionValue v = new OptionValue(option.getKey(), option.getId(), which);
          					for(OptionValue value: getOptions()){
          						if(value.getId().equals(option.getId()))
          							removeOption(value);
          					}
          					addOption(v);
          				}
          			 })
          		     .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
          		         @Override
          		         public void onClick(DialogInterface dialog, int id) {
          		        		askForOption(product, i+1);
          		         }
          		     })
          		     .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
          	             @Override
          	             public void onClick(DialogInterface dialog, int id) {
          	            	 
          	             }
          	         });	
          	  AlertDialog dialog = builder.create();
          	  dialog.show();
              }
              else if (option.getSelectionType().equals("multiple")) {
            	this.checkedItems = 0;
              	AlertDialog.Builder builder = new AlertDialog.Builder((ProductsActivity)getContext());
              	builder.setTitle(option.getName())
              		.setMultiChoiceItems(ProductOption.getValuesLabels(option.getValues()), null,  new DialogInterface.OnMultiChoiceClickListener() {
          		         @Override
          		         public void onClick(DialogInterface dialog, int which, boolean isChecked) {     	        
          		        	 OptionValue v = new OptionValue(option.getKey(), option.getId(), which);
                                if (isChecked) {                    	
                                    addOption(v);                                    
                                    addCheckedItems(1);
                                }
                                else {
                                	removeOption(v);
                                	addCheckedItems(-1); 
                                }
          		         }
          		     })
          		     .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
          		         @Override
          		         public void onClick(DialogInterface dialog, int id) {
          		        	 if(option.isRequired() && checkedItems > 0 || !option.isRequired())
          		        		askForOption(product, i+1);
          		        	 else askForOption(product, i);
          		         }
          		     })
          		     .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
          	             @Override
          	             public void onClick(DialogInterface dialog, int id) {

          	             }
          	         });	
              AlertDialog dialog = builder.create();
          	  dialog.show();
              }
    	}
    	else addItemToCart(product);
    }    

    public void addItemToCart(Product product){
    	mDialog = new ProgressDialog(getContext());
        mDialog.setMessage(getContext().getString(R.string.loading));
        mDialog.setCancelable(false);
        mDialog.show();
		final Cart cart =  ((ZervedApp)((ProductsActivity)getContext()).getApplication()).getCart(((ProductsActivity)getContext()).mLocation, 
				((ProductsActivity)getContext()).mBar,
				((ProductsActivity)getContext()).mGroup,
				((ProductsActivity)getContext()).mEvent);	
		Cart.Item item = cart.new Item(product, 1, getOptions());
    	// Validate and add item
    	String error = product.validateItem((ProductsActivity)getContext(), item);
    	if (error.equals("")) {
    		cart.addItem(item);
    		updateView();
    	} 
    }	
    
    public void updateView(){
		loadPrices();
		((ProductsActivity)getContext()).invalidateOptionsMenu();
    }
    
	private static final int LOADER_QUOTE = 0x1;
	private static final String ARGS_URI    = "com.zervedapp.zerved.ARGS_URI";
    private static final String ARGS_PARAMS = "com.zervedapp.zerved.ARGS_PARAMS";
    private static String URI = Config.API_URL+"quote";

    public void loadPrices(){
    	Bundle args = new Bundle();
    	args.putParcelable(ARGS_URI, Uri.parse(URI));
    	args.putParcelable(ARGS_PARAMS, new Bundle());
    	((ProductsActivity)getContext()).getLoaderManager().restartLoader(LOADER_QUOTE, args, this);
    }
    
	@Override
	public Loader<RESTResponse> onCreateLoader(int id, Bundle args) {
		if (args != null && args.containsKey(ARGS_URI) && args.containsKey(ARGS_PARAMS)) {
            Uri    action = args.getParcelable(ARGS_URI);
            Bundle params = args.getParcelable(ARGS_PARAMS);
        	params.putString("json", ((ZervedApp)((ProductsActivity)getContext()).getApplication()).getCart(((ProductsActivity)getContext()).mLocation, ((ProductsActivity)getContext()).mBar, ((ProductsActivity)getContext()).mGroup, ((ProductsActivity)getContext()).mEvent).toJson().toString());
            if (id == LOADER_QUOTE) {
    	        return new RESTLoader(((ProductsActivity)getContext()), RESTLoader.HTTPVerb.POST, action, params, "");
            } 
		}
		return null;
	}

	@Override
	public void onLoadFinished(Loader<RESTResponse> loader, RESTResponse data) {
		if (mDialog != null) {
			mDialog.dismiss();
		}
		int    code = data.getCode();
        String json = data.getData();
        if (loader.getId() == LOADER_QUOTE) {
        	if (code == 200) {
	        	try {
	        		JSONObject object = (JSONObject) new JSONTokener(json).nextValue();
	        		if (object.has("items")) {
	        			Cart cart = ((ZervedApp)((ProductsActivity)getContext()).getApplication()).getCart(((ProductsActivity)getContext()).mLocation, ((ProductsActivity)getContext()).mBar, ((ProductsActivity)getContext()).mGroup, ((ProductsActivity)getContext()).mEvent);
	        			JSONArray items = object.getJSONArray("items");
	                    for (int i = 0; i < items.length(); i++) {
	                    	JSONObject item = items.getJSONObject(i);
	                    	if(cart.getItems().size() > i) {
	                    		cart.addQuoteItem(i, item);
	                    	}
	                    }
	        		}
	        		notifyDataSetChanged();
	        	} 
	        	catch (JSONException e) {
	        		Log.e("", "Failed to parse JSON.", e);
	        	}
        	}
        	else {/* Error */}        	
        }
    }    

	@Override
	public void onLoaderReset(Loader<RESTResponse> arg0) {

	}

}
