package com.zervedapp.zerved;

import java.util.ArrayList;
import java.util.Collections;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import android.app.ActionBar;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.LoaderManager.LoaderCallbacks;
import android.content.Loader;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.hlidskialf.android.hardware.ShakeListener;
import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;
import com.zervedapp.zerved.loader.RESTLoader;
import com.zervedapp.zerved.loader.RESTLoader.RESTResponse;
import com.zervedapp.zerved.models.Cart;
import com.zervedapp.zerved.models.ConsumerManager;
import com.zervedapp.zerved.models.Favourite;
import com.zervedapp.zerved.models.Product;
import com.zervedapp.zerved.models.ProductOption;
import com.zervedapp.zerved.models.ProductOption.Value;
import com.zervedapp.zerved.models.OptionValue;


public class FavouritesActivity extends AuthorizedActivity implements LoaderCallbacks<RESTLoader.RESTResponse>
{
	@SuppressWarnings("unused")
	private static final String TAG = FavouritesActivity.class.getName();
	
	ActionBar mActionBar;
	FavouriteListFragment mListFragment;
	ShakeListener mShaker;
	SlidingMenu mMenu;
	Cart mCart;
	
	boolean mIsFavouriteLoading = false;
	
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
        // Initialize cart and consumer
        ZervedApp app = (ZervedApp) getApplication();
        app.init();
		
		setContentView(R.layout.fragment);
		setTitle(R.string.favourites);
		
		mMenu = SlidingMenuHelper.initSlidingMenu(this, "favourites");
		
		// Create list fragment
		FragmentManager fm = getFragmentManager();
		mListFragment = new FavouriteListFragment();
		FragmentTransaction ft = fm.beginTransaction();
		ft.add(R.id.fragment_content, mListFragment);
		ft.commit();

		// Monitor shake event
		mShaker = new ShakeListener(this);
		mShaker.setOnShakeListener(new ShakeListener.OnShakeListener() {
			@Override
			public void onShake() {
				mListFragment.reload();
			}
		});
	}
	
	public void onResume() {
		super.onResume();
		reload();
		mShaker.resume();
	}
	
	public void onPause() {
		super.onPause();
		mIsFavouriteLoading = false;
		mShaker.pause();
	}
	
	public void reload() {
		
		ConsumerManager consumer = ((ZervedApp) getApplication()).getConsumer();
		if (consumer.mConsumerKey == null || consumer.mAccessToken == null) {
			// Not logged in
			login();
		} 

		mListFragment.reset();
		mListFragment.reload(consumer);
		mIsFavouriteLoading = true;
		
		// Timeout favourites updates after 10 secs
		final Handler favouriteTimeoutHandler = new Handler();
		favouriteTimeoutHandler.postDelayed(new Runnable() {
			@Override
			public void run() {
				if (mIsFavouriteLoading) {
					mIsFavouriteLoading = false;
					mListFragment.stop();
				}
			}
		}, 10000);
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.favourites, menu);
		return true;
	}
	
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
    	switch (item.getItemId()) {
			case android.R.id.home:
				mMenu.toggle();
				return true;
			case R.id.reload:
				reload();
				return true;
    	}
    	return super.onOptionsItemSelected(item);
    }
    
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
	    if ( keyCode == KeyEvent.KEYCODE_MENU ) {
	    	mMenu.toggle();
	        return true;
	    }
	    return super.onKeyDown(keyCode, event);
	}
	private static final int LOADER_QUOTE = 0x1;
	private static final String ARGS_URI    = "com.zervedapp.zerved.ARGS_URI";
    private static final String ARGS_PARAMS = "com.zervedapp.zerved.ARGS_PARAMS";
    private static String URI_QUOTE = Config.API_URL+"quote";

    public void loadPrices(Cart c){
    	mCart = c;
    	if(mCart != null) {
	    	Bundle args = new Bundle();
	    	args.putParcelable(ARGS_URI, Uri.parse(URI_QUOTE));
	    	args.putParcelable(ARGS_PARAMS, new Bundle());
	    	this.getLoaderManager().restartLoader(LOADER_QUOTE, args, this);
    	}
    }
    
	@Override
	public Loader<RESTResponse> onCreateLoader(int id, Bundle args) {
		if (mCart != null && args != null && args.containsKey(ARGS_URI) && args.containsKey(ARGS_PARAMS)) {
            Uri    action = args.getParcelable(ARGS_URI);
            Bundle params = args.getParcelable(ARGS_PARAMS);
            params.putString("json", mCart.toJson().toString());
            if (id == LOADER_QUOTE) {
    	        return new RESTLoader(this, RESTLoader.HTTPVerb.POST, action, params, "");
            } 
		}
		return null;
	}

	@Override
	public void onLoadFinished(Loader<RESTLoader.RESTResponse> loader, RESTLoader.RESTResponse data) {
		int    code = data.getCode();
        String json = data.getData();
        if (loader.getId() == LOADER_QUOTE) {
        	if (code == 200) {
	        	try {
	        		JSONObject object = (JSONObject) new JSONTokener(json).nextValue();
	        		if (object.has("items")) { 
	        			JSONArray items = object.getJSONArray("items");
	                    for (int i = 0; i < items.length(); i++) {
	                    	JSONObject item = items.getJSONObject(i);
	                    	if(mCart != null)
	                    		mCart.addQuoteItem(i, item);
	                    }
	            		mListFragment.mAdapter.notifyDataSetChanged();
	        		} 	  
	        	} 
	        	catch (JSONException e) {
	        		Log.e("", "Failed to parse JSON.", e);
	        	}
        	}
        	else {/* Error */}        	
        }
    }    

	@Override
	public void onLoaderReset(Loader<RESTResponse> arg0) {

	}
	
	public static class FavouriteListFragment extends RESTListFragment{
		private ArrayAdapter<Favourite> mAdapter;
		ArrayList<Favourite> mFavourites;
		ArrayList<String> sections;
		ConsumerManager mConsumer;
	
		protected static String URI = Config.API_URL+"consumers/%s/favourites";

	    
		
		@Override
		public void onActivityCreated(Bundle savedInstanceState) {
			super.onActivityCreated(savedInstanceState);
			sections = new ArrayList<String>();
			mAdapter = new ArrayAdapter<Favourite>(getActivity(), R.layout.favourite) {
				@Override
	        	public View getView(int position, View convertView, ViewGroup parent) {
	        		View row;
	        		if (null == convertView) {
	        			LayoutInflater inflater = LayoutInflater.from(getActivity());
	        			row = inflater.inflate(R.layout.favourite, null);
	        		} else {
	        			row = convertView;
	        		}
	        		final Favourite favourite = this.getItem(position);
	        		
	        		ViewGroup sectionHeader = (ViewGroup) row.findViewById(R.id.section_header);
	        		TextView sectionName = (TextView) row.findViewById(R.id.section_name);
	        		Button cartButton = (Button) row.findViewById(R.id.cart);
	        		
	        		TextView name = (TextView) row.findViewById(R.id.favourite_name);
	        		TextView description = (TextView) row.findViewById(R.id.favourite_description);
	        		
	        		ImageButton addToCart = (ImageButton) row.findViewById(R.id.addToCart);
	        		ImageButton remove = (ImageButton) row.findViewById(R.id.removeFavourite);

	        		final Cart cart = ((ZervedApp) getActivity().getApplication()).getCart(favourite.getLocation(), favourite.getBar(), (favourite.getBar() == null) ? "entrance" : "menu", null);
    		
	        		addToCart.setOnClickListener(new OnClickListener() {
						@Override
						public void onClick(View v) {
							// Add to cart		
					    	Product product = favourite.getProduct();
					    	
					    	// Create cart item object
					     	ArrayList<OptionValue> options = new ArrayList<OptionValue>();
					     
					     	ArrayList<ProductOption> productOptions = favourite.getProduct().getProductOptions();
				        	for(OptionValue option: favourite.getOptions()){
				        		for(ProductOption productOption: productOptions){
				        			if(option.getId().equals(productOption.getId())){
				        				for(Value value: productOption.getValues()){
				        					if(value.getIndex() == option.getValue()){
				        						options.add(new OptionValue(productOption.getKey(), productOption.getId(), option.getValue()));
				        					}
				        				} 
				        			}
				        		}
			        		}					    	
					    	Cart.Item item = cart.new Item(product, 1, options);
					    	cart.addItem(item);
					    	((FavouritesActivity)getContext()).loadPrices(cart);
					    	notifyDataSetChanged();
						}
					});

	        		remove.setOnClickListener(new OnClickListener() {
						@Override
						public void onClick(View v) {
							// remove from favorites
							AlertDialog.Builder builder = new AlertDialog.Builder((FavouritesActivity)getContext());
			              	builder.setTitle(getString(R.string.delete) + " ?")
			          		     .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
			          		         @Override
			          		         public void onClick(DialogInterface dialog, int id) {
			 							ConsumerManager consumer = ((ZervedApp) getActivity().getApplication()).getConsumer();	
										if (consumer.mConsumerKey == null || consumer.mAccessToken == null) {
											Toast.makeText(getContext(), "Not logged in", Toast.LENGTH_SHORT).show();
										} 
										consumer.deleteFavourite(getActivity(), favourite.getKey());
										reload(consumer);
			          		         }
			          		     })
			          		     .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
			          	             @Override
			          	             public void onClick(DialogInterface dialog, int id) {
			          	            	 
			          	             }
			          	         });	
			          	  AlertDialog dialog = builder.create();
			          	  dialog.show();
			          	  

						}
					});
	        		
	        		name.setText(favourite.getProduct().getName());
	        	
        			if(!favourite.sectionIsVisible())
        				sectionHeader.setVisibility(View.GONE);
        			else {
        				String section_name = "";
        				int max_length = 35 - 3;
        				if(favourite.getBar() == null)
        					section_name = favourite.getLocation().getName();
        				else section_name = favourite.getLocation().getName() + ": " + favourite.getBar().getName();
        				
        				if(section_name.length() > max_length) {
        					section_name = section_name.substring(0, max_length) + "...";
        				}
        				
        				sectionName.setText(section_name);
        				
        				
        				cartButton.setText(String.format(getResources().getString(R.string.cart_x), cart.size()));
        				cartButton.setVisibility(cart.size() > 0 ? View.VISIBLE : View.GONE);
        				cartButton.setOnClickListener(new OnClickListener() {
							
							@Override
							public void onClick(View v) {
								goToCart(favourite);
							}
						});
        			}
	        	        		
	        		String options = "";	        		
	        		ArrayList<ProductOption> productOptions = favourite.getProduct().getProductOptions();
		        	for(OptionValue option: favourite.getOptions()){
		        		for(ProductOption productOption: productOptions){
		        			if(option.getId().equals(productOption.getId())){
		        				for(Value v: productOption.getValues()){
		        					if(v.getIndex() == option.getValue()){
		        						options += productOption.getName() + ": " + v.getLabel() + ", ";
		        					}
		        				} 
		        			}
		        		}
	        		}
		        	if(options.length() > 0) {
		        		options = options.substring(0, options.length()-2);
		        	}
		        	description.setText(options);
		        	
		        	
		        	LinearLayout selectedProducts = (LinearLayout) row.findViewById(R.id.selected_favourites);
		    		selectedProducts.removeAllViews();
		    		for(final Cart.Item item : cart.getItems())
		    		{
		    			if(item.getProduct().getKey().equals(favourite.getProduct().getKey()) && 
		    				item.getOptions().equals(favourite.getOptions())
		    			)
		    			{
		    				LayoutInflater inflater = LayoutInflater.from(getContext());
		    				RelativeLayout selectedProduct = (RelativeLayout) inflater.inflate(R.layout.selected_product, null);
		    				selectedProducts.addView(selectedProduct);
		    				
		    				TextView productQuantity = (TextView) selectedProduct.findViewById(R.id.product_quantity);
		    				TextView productOptionsLabel = (TextView) selectedProduct.findViewById(R.id.product_desc);
		    				TextView productPrice = (TextView) selectedProduct.findViewById(R.id.product_price);
		    				
		    				ImageButton plusBtn = (ImageButton) selectedProduct.findViewById(R.id.plus);
		    				ImageButton minusBtn = (ImageButton) selectedProduct.findViewById(R.id.minus);

		    				
		    				// Quantity and product name
		    				productQuantity.setText(Integer.toString(item.getQuantity()));
		    	        	productOptionsLabel.setText(options);
		    	        	
		    	        	if(options.equals(""))
		    	        		productOptionsLabel.setVisibility(View.GONE);
		    	        	
		    	        	// Price
		    	        	if(item.getQuoteItem() != null)
		    	        		productPrice.setText(item.getQuoteItem().getStringProperty("price_formatted"));
		    	        	
		    	        	// Quantity
		    	        	plusBtn.setOnClickListener(new OnClickListener() {
		    					@Override
		    					public void onClick(View v) {
		    						item.addQuantity(1);
		    						((FavouritesActivity)getContext()).loadPrices(cart);
		    						notifyDataSetChanged();
		    					}
		    				});	        	
		    	        	minusBtn.setOnClickListener(new OnClickListener() {
		    					@Override
		    					public void onClick(View v) {
		    						if(item.getQuantity() <= 1) {
		    							((View)(v.getParent().getParent())).setVisibility(View.GONE);
		    							cart.removeItem(item);
		    						}
		    						else item.addQuantity(-1);
		    						((FavouritesActivity)getContext()).loadPrices(cart);
		    						notifyDataSetChanged();
		    					}
		    				});        	
		    	        	
		    			}		            	
		    		}
		    		return row;
	        	}
			};
	        this.setListAdapter(mAdapter);
		}

		public void goToCart(Favourite favourite) {
			// Set current location
	    	((ZervedApp)getActivity().getApplication()).mLocation = favourite.getLocation();

    		// Start location activity (we have to always start location view to make sure it's updated to the current location)
    		Intent locationIntent = new Intent(getActivity(), LocationActivity.class);
    		locationIntent.putExtra(LocationActivity.EXTRA_SHOW_ALL_BARS, true);
    		locationIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
    		startActivity(locationIntent);
	    	
	    	// Start cart activity
	    	Intent cartIntent = new Intent(getActivity(), CartActivity.class);
	    	cartIntent.putExtra(CategoryListFragment.EXTRA_LOCATION, favourite.getLocation().toJson());
    		if (favourite.getBar() != null) {
    			cartIntent.putExtra(CategoryListFragment.EXTRA_BAR, favourite.getBar().toJson());
        	}
    		cartIntent.putExtra(CategoryListFragment.EXTRA_GROUP, favourite.getBar() != null ? "menu" : "entrance");
    		startActivity(cartIntent);
		}
		
		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
			View view = super.onCreateView(inflater, container, savedInstanceState);
			TextView progressText = (TextView) view.findViewById(R.id.progress_text);
			progressText.setText(R.string.finding_favourites);
			((TextView)view.findViewById(R.id.empty_message)).setText(R.string.empty_favourites_msg);
			return view;
		}
		
		public void reset() {
			sections = new ArrayList<String>();
			mAdapter.clear();
			getView().findViewById(R.id.progress).setVisibility(View.VISIBLE);
	        getView().findViewById(R.id.progress_text).setVisibility(View.VISIBLE);
	        getView().findViewById(R.id.empty_message).setVisibility(View.GONE);
	        getListView().setEmptyView(getView().findViewById(R.id.progress));
		}
		
		public void stop() {
			getView().findViewById(R.id.empty_message).setVisibility(View.VISIBLE);
	        getView().findViewById(R.id.progress).setVisibility(View.GONE);
	        getView().findViewById(R.id.progress_text).setVisibility(View.GONE);
	        getListView().setEmptyView(getView().findViewById(R.id.empty_message));
		} 
		
		public void reload(ConsumerManager consumer) {
			this.mConsumer = consumer;
			reload();
		}	
		
		public void reload() {
			if (this.mConsumer == null) {
				this.mConsumer = ((ZervedApp) getActivity().getApplication()).getConsumer();
			}
			if (this.mConsumer != null && this.mConsumer.mConsumerKey != null) {
				super.reload();
			}
		}

		@Override
		public String getUri() {
			return String.format(URI, this.mConsumer.mConsumerKey);
		}
		
		@Override
		public String getAuthorization() {
			return this.mConsumer.mAccessToken;
		}
		
		@Override
		public void onJsonLoaded(String json) {
			Log.i("Favourites:onJsonLoaded", json);
			mFavourites = Favourite.initList(json);
			Collections.sort(mFavourites, new FavouriteComparator());
            mAdapter.clear();
            sections = new ArrayList<String>();

            for (Favourite favourite : mFavourites) {
            	if(favourite.getBar() == null){
            		if(!sections.contains(favourite.getLocation().getKey())){
            			sections.add(favourite.getLocation().getKey());
            			favourite.setSectionVisibility(true);
            		}
            	} else if(!sections.contains(favourite.getBar().getKey())){
            		sections.add(favourite.getBar().getKey());
               		favourite.setSectionVisibility(true);
            	}         	
            	mAdapter.add(favourite);
            }
		}
		
		@Override
	    public void onListItemClick(ListView l, View v, int position, long id) {
			
		}
	}
}
