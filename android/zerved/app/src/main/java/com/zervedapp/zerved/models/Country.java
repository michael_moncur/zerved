package com.zervedapp.zerved.models;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONTokener;

import android.util.Log;

public class Country extends JSONModel {
	private static final String TAG = Country.class.getName();
	
	public Country(String json) throws JSONException {
		super(json);
	}
	
	public static ArrayList<Country> initList(String json) {
        ArrayList<Country> objectList = new ArrayList<Country>();
        
        try {
        	JSONArray objects = (JSONArray) new JSONTokener(json).nextValue();
            for (int i = 0; i < objects.length(); i++) {
            	Country object = new Country(objects.getString(i));
                objectList.add(object);
            }
            
        }
        catch (JSONException e) {
            Log.e(TAG, "Failed to parse JSON.", e);
        }
        
        return objectList;
	}
	
	public String toString() {
		return getStringProperty("name");
	}
}
