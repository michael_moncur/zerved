package com.zervedapp.zerved;

import android.annotation.SuppressLint;
import android.app.ListFragment;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;
import com.zervedapp.zerved.models.MerchantLocation;

@SuppressLint("ValidFragment")
public class SlidingMenuFragment extends ListFragment {
	SlidingMenu mMenu;
	MenuAdapter mAdapter;
	MerchantLocation mLocation = null;
	String mCurrentTag;
	
	public SlidingMenuFragment() {
		
	}
	
	public SlidingMenuFragment(SlidingMenu menu, String tag) {
		mMenu = menu;
		mCurrentTag = tag;
	}
	
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		mAdapter = new MenuAdapter(getActivity());
		reloadList();
		setListAdapter(mAdapter);
	}
	
	@Override
	public void onResume() {
		super.onResume();
		if (mLocation != ((ZervedApp)getActivity().getApplication()).mLocation) {
			reloadList();
		}
	}
	
	private void reloadList() {
		mLocation = ((ZervedApp)getActivity().getApplication()).mLocation;
		mAdapter.clear();
		if (mLocation != null) {
			mAdapter.add(new MenuItem("location", mLocation.getName(), R.drawable.ic_menu_cart, new Intent(getActivity(), LocationActivity.class)));
		}
		mAdapter.add(new MenuItem("locations", R.string.locations, R.drawable.ic_menu_places, new Intent(getActivity(), LocationsActivity.class)));
		mAdapter.add(new MenuItem("orders", R.string.orders, R.drawable.ic_menu_orders, new Intent(getActivity(), OrdersActivity.class)));
		mAdapter.add(new MenuItem("favourites", R.string.favourites, R.drawable.ic_menu_favourites, new Intent(getActivity(), FavouritesActivity.class)));
		mAdapter.add(new MenuItem("profile", R.string.profile, R.drawable.ic_menu_profile, new Intent(getActivity(), ProfileActivity.class)));
		Intent infoIntent = new Intent(getActivity(), InfoActivity.class);
		infoIntent.putExtra(WebViewActivity.EXTRA_URL, Config.SERVER_URL+"/android/info");
		infoIntent.putExtra(WebViewActivity.EXTRA_TITLE, getResources().getText(R.string.info).toString());
		mAdapter.add(new MenuItem("info", R.string.info, R.drawable.ic_menu_info, infoIntent));
		mAdapter.add(new MenuItem("merchant", R.string.for_merchants, R.drawable.ic_menu_login, new Intent(getActivity(), MerchantActivity.class)));
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.menu_list, null);
		return view;
	}
	
	@Override
	public void onListItemClick(ListView lv, View v, int position, long id) {
		mMenu.toggle();
		MenuItem item = mAdapter.getItem(position);
		Intent intent = item.intent;
		if (!item.tag.equals(mCurrentTag) && intent != null) {
			intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT|Intent.FLAG_ACTIVITY_NO_ANIMATION);
			startActivity(intent);
		}
	}

	private class MenuItem {
		public String tag;
		public String title;
		public int iconRes;
		public Intent intent;
		public MenuItem(String tag, int title, int iconRes, Intent intent) {
			this(tag, getResources().getString(title), iconRes, intent);
		}
		public MenuItem(String tag, String title, int iconRes, Intent intent) {
			this.tag = tag; 
			this.title = title;
			this.iconRes = iconRes;
			this.intent = intent;
		}
	}

	public class MenuAdapter extends ArrayAdapter<MenuItem> {

		public MenuAdapter(Context context) {
			super(context, 0);
		}

		public View getView(int position, View convertView, ViewGroup parent) {
			MenuItem item = getItem(position);
			View view;
			if (item.tag.equals(mCurrentTag)) {
				view = LayoutInflater.from(getContext()).inflate(R.layout.slidingmenu_row_active, null);
			} else {
				view = LayoutInflater.from(getContext()).inflate(R.layout.slidingmenu_row, null);
			}
			ImageView icon = (ImageView) view.findViewById(R.id.row_icon);
			icon.setImageResource(item.iconRes);
			TextView title = (TextView) view.findViewById(R.id.row_title);
			title.setText(item.title);

			return view;
		}

	}
}
