package com.zervedapp.zerved.models;

import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONTokener;
import com.zervedapp.zerved.R;
import android.content.Context;
import android.util.Log;

public class Product extends JSONModel {
	private static final String TAG = Product.class.getName();
	@SuppressWarnings("unused")
	private boolean orderable = true;
	
	public Product(String json) throws JSONException {
		super(json);
	}
	
	public static ArrayList<Product> initList(String json) {
        ArrayList<Product> objectList = new ArrayList<Product>();
        
        try {
        	JSONArray objects = (JSONArray) new JSONTokener(json).nextValue();
            for (int i = 0; i < objects.length(); i++) {
            	Product object = new Product(objects.getString(i));
                objectList.add(object);
            }
            
        }
        catch (JSONException e) {
            Log.e(TAG, "Failed to parse JSON.", e);
        }
        
        return objectList;
	}
	
	public String getName() {
		return this.getStringProperty("name");
	}
	
	public String getDescription() {
		return this.getStringProperty("description");
	}
	
	public String getImageUrl() {
		if (has("image_url") && !getStringProperty("image_url").equals("")  && !getStringProperty("image_url").equals("null") && getStringProperty("image_url") != null) {
			return getStringProperty("image_url");
		} else {
			return null;
		}
	}
	
	public ArrayList<String> getTieredPriceDescriptions() {
		ArrayList<String> list = new ArrayList<String>();
		try {
			JSONArray prices = this.getJSONArray("tiered_price_descriptions");
			for (int i = 0; i < prices.length(); i++) {
				list.add(prices.getString(i));
			}
		}
		catch (JSONException e) {
            Log.e(TAG, "Failed to parse JSON.", e);
        }
		return list;
	}
	
	public ArrayList<ProductOption> getProductOptions() {
		ArrayList<ProductOption> list = new ArrayList<ProductOption>();
		try {
			JSONArray options = this.getJSONArray("product_options");
			for (int i = 0; i < options.length(); i++) {
				list.add(new ProductOption(options.getString(i)));
			}
		}
		catch (JSONException e) {
            Log.e(TAG, "Failed to parse JSON.", e);
        }
		return list;
	}
	
	public String toString() {
		return this.getName();
	}
	
	public String validateItem(Context context, Cart.Item item) {
		for (ProductOption option: getProductOptions()) {
			if (option.getBooleanProperty("required") && item.getValuesForOption(option.getKey()).size() == 0) {
				return String.format(context.getResources().getString(R.string.x_is_required).toString(), option.getName());
			}
		}
		return "";
	}
	
	public boolean isOrderable() {
		return getBooleanProperty("menu_active") && getBooleanProperty("merchant_open");
	}
}
