package com.zervedapp.zerved.models;

import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

public class JSONModel extends JSONObject {
	private static final String TAG = JSONModel.class.getName();
	
	public JSONModel(){
	
	}
	
	public JSONModel(String json) throws JSONException {
		super(json);
	}
	
	public String getStringProperty(String key) {
		try {
			return this.getString(key);
		} catch (JSONException e) {
			Log.e(TAG, "Property not found", e);
		}
		return "";
	}
	
	public Double getDoubleProperty(String key) {
		try {
			return this.getDouble(key);
		} catch (JSONException e) {
			Log.e(TAG, "Property not found", e);
		}
		return new Double(0);
	}
	
	public boolean getBooleanProperty(String key) {
		try {
			return this.getBoolean(key);
		} catch (JSONException e) {
			Log.e(TAG, "Property not found", e);
		}
		return false;
	}
	
	public int getIntegerProperty(String key) {
		try {
			return this.getInt(key);
		} catch (JSONException e) {
			Log.e(TAG, "Property not found", e);
		}
		return 0;
	}
	
	public String getKey() {
		return this.getStringProperty("key");
	}
	
	public String toJson() {
		return super.toString();
	}
}
