package com.zervedapp.zerved.models;

import android.graphics.Color;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.widget.EditText;

public class CreditCardTools {
    /**
     * Format whole or partial credit card number as groups of 4 digits, example: 1234 5678 1234 5678
     * 
     * @param text Raw credit card number
     * @return Formatted credit card number string
     */
    public static String formatCreditCardNumber(CharSequence text)
    {
        StringBuilder formatted = new StringBuilder();
        int count = 0;
        for (int i = 0; i < text.length(); ++i)
        {
            if (Character.isDigit(text.charAt(i)))
            {
                if (count % 4 == 0 && count > 0)
                    formatted.append(" ");
                formatted.append(text.charAt(i));
                ++count;
            }
        }
        return formatted.toString();
    }
    
    /**
     * Validate credit card number using the Luhn algorithm
     * 
     * @param ccNumber Credit card number string without spaces
     * @return Validation result
     */
    public static boolean luhnCheck(String ccNumber)
    {
    	int sum = 0;
    	boolean alternate = false;
    	for (int i = ccNumber.length() - 1; i >= 0; i--)
    	{
        	int n = Integer.parseInt(ccNumber.substring(i, i + 1));
        	if (alternate)
        	{
        		n *= 2;
        		if (n > 9)
        		{
        			n = (n % 10) + 1;
        		}
        	}
        	sum += n;
        	alternate = !alternate;
        }
        return (sum % 10 == 0);
    }
    
    public static class TextLengthWatcher implements TextWatcher
    {
    	int mMinLength;
    	int mMaxLength;
    	EditText mThisField;
    	EditText mNextField;
    	
    	public TextLengthWatcher(int minLength, int maxLength, EditText thisField, EditText nextField) {
    		mMinLength = minLength;
    		mMaxLength = maxLength;
    		mThisField = thisField;
    		mNextField = nextField;
    	}
    	
    	public void setMinLength(int minLength) {
    		mMinLength = minLength;
    	}
    	
    	public void setMaxLength(int maxLength) {
    		mMaxLength = maxLength;
    		if (mThisField.length() > maxLength) {
    			// If text too long for new max length, cut it off
    			mThisField.removeTextChangedListener(this);
    			mThisField.setText(mThisField.getText().subSequence(0, maxLength));
    			mThisField.addTextChangedListener(this);
    		}
    		InputFilter[] filters = new InputFilter[1];
    		filters[0] = new InputFilter.LengthFilter(maxLength);
    		mThisField.setFilters(filters);
    	}
    	
    	public boolean validate() {
    		return mThisField.getText().length() >= mMinLength && mThisField.getText().length() <= mMaxLength;
    	}
    	
		@Override
		public void afterTextChanged(Editable s) {
			if (s.length() >= mMinLength) {
				boolean valid = validate();
				mThisField.setTextColor(valid ? Color.BLACK : Color.RED);
				
				if (valid && s.length() == mMaxLength && mNextField != null) {
					mNextField.requestFocus();
				}
			} else {
				mThisField.setTextColor(Color.BLACK);
			}
		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {
			
		}

		@Override
		public void onTextChanged(CharSequence s, int start, int before,
				int count) {
			
		}
    }
    
    public static class CardNumberWatcher extends TextLengthWatcher
    {
		private boolean spaceDeleted;
    	
    	public CardNumberWatcher(EditText thisField, EditText nextField) {
    		super(19, 19, thisField, nextField);
    	}
    	
    	@Override
    	public boolean validate() {
    		String rawNumber = mThisField.getText().toString().replace(" ", "");
    		return super.validate() && CreditCardTools.luhnCheck(rawNumber);
    	}
    	
		@Override
		public void afterTextChanged(Editable text) {
            // Disable text watcher temporarily to avoid this function being called recursively when we modify the text
			mThisField.removeTextChangedListener(this);

            // record cursor position as setting the text in the textview
            // places the cursor at the end
            int cursorPosition = mThisField.getSelectionStart();
            String withSpaces = CreditCardTools.formatCreditCardNumber(text);
            mThisField.setText(withSpaces);
            // set the cursor at the last position + the spaces added since the
            // space are always added before the cursor
            mThisField.setSelection(cursorPosition + (withSpaces.length() - text.length()));

            // if a space was deleted also deleted just move the cursor
            // before the space
            if (spaceDeleted) {
            	mThisField.setSelection(mThisField.getSelectionStart() - 1);
                spaceDeleted = false;
            }

            // Re-enable text watcher
            mThisField.addTextChangedListener(this);
            
            super.afterTextChanged(text);
		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {
			spaceDeleted = s.subSequence(start, start+count).toString().equals(" ");
			super.beforeTextChanged(s, start, count, after);
		}
    }
    
    public static class MonthWatcher extends TextLengthWatcher
    {
    	public MonthWatcher(EditText thisField, EditText nextField) {
    		super(2, 2, thisField, nextField);
    		attachOnFocusChangeListener();
    	}
    	
    	@Override
    	public boolean validate() {
    		try {
    			int number = Integer.parseInt(mThisField.getText().toString());
        		return number >= 1 && number <= 12;
    		} catch (NumberFormatException e) {
    			return false;
    		}
    	}
    	
    	public void attachOnFocusChangeListener() {
    		mThisField.setOnFocusChangeListener(new OnFocusChangeListener() {
    			@Override
    			public void onFocusChange(View v, boolean hasFocus) {
    				// If only one digit entered when moving away from month, add a zero
    				if (!hasFocus) {
    					if (mThisField.getText().length() == 1) {
    						// Disable text watcher temporarily to avoid text watcher being called when we modify the text
    						mThisField.removeTextChangedListener(MonthWatcher.this);
    						mThisField.setText("0"+mThisField.getText());
    						// Re-enable text watcher
    			            mThisField.addTextChangedListener(MonthWatcher.this);
    					}
    				}
    			}
    		});
    	}
    }
}
