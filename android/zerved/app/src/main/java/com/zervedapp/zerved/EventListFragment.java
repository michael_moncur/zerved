package com.zervedapp.zerved;

import java.util.ArrayList;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import com.zervedapp.zerved.models.Event;

public class EventListFragment extends RESTListFragment {
    public static final String EXTRA_LOCATION = "com.zervedapp.zerved.LOCATION";
    public static final String EXTRA_YEAR = "com.zervedapp.zerved.YEAR";
    public static final String EXTRA_MONTH = "com.zervedapp.zerved.MONTH";
    public static final String EXTRA_DAY = "com.zervedapp.zerved.DAY";
    
    private static String EVENTS_URI = Config.API_URL + "locations/%s/events/%s/%s/%s";
    
	private EventsActivity mActivity;
	private EventAdapter mAdapter;
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		
		mActivity = (EventsActivity) getActivity();
        setHasOptionsMenu(true);
	}
	
	public int getCount(){
		return this.mAdapter == null ? 0 : this.mAdapter.getCount();
	}
	
    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
    	Event e = mAdapter.getItem(position);
    	Intent intent = new Intent(getActivity(), EventActivity.class);
    	intent.putExtra(EventActivity.EXTRA_EVENT, e.toJson() );
        startActivity(intent);    	        
    }
    
    public EventAdapter getAdapter() {
    	return this.mAdapter;
    }

	@Override
	public String getUri() {
        Intent intent = mActivity.getIntent();
        String year = intent.getStringExtra(EventListFragment.EXTRA_YEAR);
        String month = intent.getStringExtra(EventListFragment.EXTRA_MONTH);
        String day = intent.getStringExtra(EventListFragment.EXTRA_DAY);
		return String.format(EVENTS_URI, mActivity.getLocation().getKey(), year, month, day);
	}

	@Override
	public void onJsonLoaded(String json) {
		
		ArrayList<Event> events = Event.initList(json);
        mAdapter = new EventAdapter(getActivity(), R.layout.event_list_item);
        for (Event event: events) {
        	mAdapter.add(event);
        }
        this.setListAdapter(mAdapter);     
		
	}
}