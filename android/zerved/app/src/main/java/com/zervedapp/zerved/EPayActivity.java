package com.zervedapp.zerved;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

//import org.jraf.android.backport.switchwidget.Switch;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;
import android.widget.Switch;

import com.zervedapp.zerved.models.CreditCardTools;

import eu.epay.library.EpayWebView;
import eu.epay.library.PaymentResultListener;

public class EPayActivity extends AuthorizedActivity {
	private WebView view;
	private Map<String, String> data = new LinkedHashMap<String, String>();
	
	public static final String TAG = EPayActivity.class.getName();
	public static final String EXTRA_AMOUNT = "com.zervedapp.zerved.AMOUNT";
	public static final String EXTRA_CURRENCY = "com.zervedapp.zerved.CURRENCY";
	public static final String EXTRA_MUST_SAVE_CARD = "com.zervedapp.zerved.MUST_SAVE_CARD";
	public static final String EXTRA_TITLE = "com.zervedapp.zerved.TITLE";
	public static final String EXTRA_BUTTON_TITLE = "com.zervedapp.zerved.BUTTON_TITLE";
	public static final String EXTRA_PINCODE = "com.zervedapp.zerved.PINCODE";
	public static final String EXTRA_OBSCURED_CARDNO = "com.zervedapp.zerved.EXTRA_OBSCURED_CARDNO";
	public static final String EXTRA_ORDER_ID = "com.zervedapp.zerved.EXTRA_ORDER_ID"; 
	private static final String ARGS_URI    = "com.zervedapp.zerved.ARGS_URI";
    private static final String ARGS_PARAMS = "com.zervedapp.zerved.ARGS_PARAMS";
	private static final int EXPIRATION = 1000 * 60 * 5; // 5 minutes
		
	private double mAmount;
	private String mOrderId;
	private String mCurrency;
	private boolean mMustSaveCard;
	
	EditText mPincode;
	CreditCardTools.TextLengthWatcher mPincodeWatcher;
	Switch mSaveCard;
	Button mConfirmButton;
	boolean mPaymentLoading;
	long mLoadedAt;
	Timer mTimer;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);

		View view = getLayoutInflater().inflate(R.layout.epay, null);
		
		// Intent data
		mAmount = getIntent().getDoubleExtra(EXTRA_AMOUNT, 0);
		mCurrency = getIntent().getStringExtra(EXTRA_CURRENCY);
		mOrderId = getIntent().getStringExtra(EXTRA_ORDER_ID);
		mMustSaveCard = getIntent().getBooleanExtra(EXTRA_MUST_SAVE_CARD, false);
		final ActionBar actionBar = getActionBar();
		actionBar.setTitle(getIntent().getStringExtra(EXTRA_TITLE));
		actionBar.setDisplayHomeAsUpEnabled(true);
		if (getIntent().hasExtra(EXTRA_BUTTON_TITLE)) {
			((Button)view.findViewById(R.id.confirm)).setText(getIntent().getStringExtra(EXTRA_BUTTON_TITLE));
		}
		
		// Setup UI fields
		mPincode = (EditText) view.findViewById(R.id.pincode);
		final EditText pincode = mPincode;
		mSaveCard = (Switch) view.findViewById(R.id.save_card);
		mPincodeWatcher = new CreditCardTools.TextLengthWatcher(4, 4, pincode, null);
		pincode.addTextChangedListener(mPincodeWatcher);
		if (mMustSaveCard) {
			// Hide save card switch
			view.findViewById(R.id.save_card).setVisibility(View.GONE);
		}
		mConfirmButton = (Button) view.findViewById(R.id.confirm);
		setContentView(view);
		setProgressBarIndeterminateVisibility(false);
		
		mLoadedAt = SystemClock.elapsedRealtime();
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
	}
	
	@Override
	public void onResume() {
		super.onResume();
		mTimer = new Timer();
		// Check expiration
		mTimer.schedule(new TimerTask() {
			@Override
			public void run() {
				runOnUiThread(new Runnable() {
					public void run() {
						// Finish activity if expired
						if (SystemClock.elapsedRealtime()-mLoadedAt > EXPIRATION) {
							Toast.makeText(EPayActivity.this, R.string.payment_expired, Toast.LENGTH_SHORT).show();
							finish();
						}
					}
				});
				
			}
		}, 0, 1000);
	}
	
	@Override
	public void onPause() {
		mTimer.cancel();
		super.onPause();
	}
	
	
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
	
	public void togglePaymentLoading(final boolean loading) {
		runOnUiThread(new Runnable() {
			public void run() {
				mPaymentLoading = loading;
				setProgressBarIndeterminateVisibility(loading);
				mConfirmButton.setEnabled(!loading);
				mConfirmButton.setText(loading ? R.string.loading : R.string.confirm_payment);
			}
		});
	}
	
	public void showError(final String msg) {
		runOnUiThread(new Runnable() {
			public void run() {
				Toast.makeText(EPayActivity.this, msg, Toast.LENGTH_SHORT).show();
			}
		});
	}
	
	public void saveCardSwitched(View v) {
		// Toggle pincode visibility
		boolean saveCard = ((Switch) v).isChecked();
		ViewGroup form = (ViewGroup) v.getParent();
		form.findViewById(R.id.pincode_label).setVisibility(saveCard ? View.VISIBLE : View.GONE);
		form.findViewById(R.id.pincode).setVisibility(saveCard ? View.VISIBLE : View.GONE);
		form.findViewById(R.id.info_button).setVisibility(saveCard ? View.VISIBLE : View.GONE);
	}
	
	public void infoClick(View v) {
    	AlertDialog.Builder alert = new AlertDialog.Builder(this);
    	alert.setTitle(R.string.save_card);
    	alert.setNegativeButton(R.string.close, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {}
		});
    	
    	View view = getLayoutInflater().inflate(R.layout.dialog_webview, null);
    	final WebView webView = (WebView) view.findViewById(R.id.webview);
    	final ProgressBar progress = (ProgressBar) view.findViewById(R.id.progress);
    	webView.setWebViewClient(new WebViewClient() {
    		@Override
    		public void onPageFinished(WebView view, String url) {
    			webView.setVisibility(View.VISIBLE);
    			progress.setVisibility(View.INVISIBLE);
    		}
    	});
    	alert.setView(view);
    	
    	webView.loadUrl(Config.SERVER_URL+"/iphone/logininfo");
    	alert.show();
	}
	
	public boolean validate() {
		if ((mMustSaveCard || mSaveCard.isChecked()) && !mPincodeWatcher.validate()) {
			Toast.makeText(this, R.string.invalid_payment_code, Toast.LENGTH_SHORT).show();
			return false;
		}
		return true;
	}
	
	public void submitClick(View v) {
		if (mPaymentLoading || !validate()) {
			return;
		}
		
		togglePaymentLoading(true);
		
		try {
			//Setting data for payment window
			SetDataForPaymentWindow();

			//Set payment view as main
			setContentView(R.layout.epayment);
			WebView browser = (WebView) findViewById(R.id.webView1);
			browser.getSettings().setLoadWithOverviewMode(true);
		    browser.getSettings().setUseWideViewPort(true);
		    view = browser;
			EpayWebView paymentView = new EpayWebView(new PaymentResultListener() {

				@Override
				public void PaymentAccepted(Map<String, String> arg0) {
					Log.d(TAG, "PaymentAccepted" + arg0);
					togglePaymentLoading(false);
					Intent data = getIntent();
					setResult(Activity.RESULT_OK, data);
					finish();
				}

				@Override
				public void PaymentWindowLoaded() {
					setProgressBarIndeterminateVisibility(false);
				}

				@Override
				public void PaymentWindowCancelled() {
					Log.d(TAG, "PaymentWindowCancelled");
				}

				@Override
				public void PaymentWindowLoading() {
					setProgressBarIndeterminateVisibility(true);
				}

				@Override
				public void ErrorOccurred(int arg0, String arg1, String arg2) {
					Toast.makeText(EPayActivity.this, "Payment error: " + arg1 + "," + arg2, Toast.LENGTH_LONG).show();
					Log.d(TAG, "ErrorOccurred" + arg1 + "," + arg2);
				}

				@Override
				public void PaymentLoadingAcceptPage() {
				}

				@Override
				public void Debug(String arg0) {
					Log.d(TAG, "Debug " + arg0);
				}
				}
			
			, view, false, Config.EPAY_MD5_HASH_KEY);
			
			view = paymentView.LoadPaymentWindow(data);
		} catch (Exception e) {
			Log.e(TAG, "Payment failed", e);
			mPaymentLoading = false;
			setProgressBarIndeterminateVisibility(false);
			showError(getResources().getString(R.string.payment_failed));
		}
	}
	
	public Map<String, String> SetDataForPaymentWindow()
	{
		//http://tech.epay.dk/en/specification#258
		data.put("merchantnumber", Config.EPAY_MERCHANT_NUMBER);
		
		//http://tech.epay.dk/en/specification#259
		data.put("currency", mCurrency);
		
		//http://tech.epay.dk/en/specification#260
		data.put("amount", "" + (int)(mAmount * 100));
		
		//http://tech.epay.dk/en/specification#261
		data.put("orderid", mOrderId);

		//http://tech.epay.dk/en/specification#262
		//data.put("windowid", "1");

		// We dont want the MobilePay payment type as an option in the ePay window as it is implemented natively.
		// "Epay support" recommends setting paymentcollection = 1, lockpaymentcollection = 1 and not submitting the
		// paymenttype paramenter to accomplish this.
		//http://tech.epay.dk/en/specification#263
		data.put("paymentcollection", "1");
		//http://tech.epay.dk/en/specification#264
		data.put("lockpaymentcollection", "1");
		//http://tech.epay.dk/en/specification#265
		//data.put("paymenttype", "");
		
		//http://tech.epay.dk/en/specification#266
		data.put("language", "0");
		
		//http://tech.epay.dk/en/specification#267
		data.put("encoding", "UTF-8");
		
		//http://tech.epay.dk/en/specification#269
		try {
			data.put("mobilecssurl", URLEncoder.encode("https://zervedapp.appspot.com/iphone/css/epay.css?v=5", "UTF-8"));
		} catch (UnsupportedEncodingException e) {
			Log.e(TAG, "mobilecssurl urlencoding problem", e);		
		}

		//http://tech.epay.dk/en/specification#270
		data.put("instantcapture", "0");
		
		//http://tech.epay.dk/en/specification#272
		//data.put("splitpayment", "0");
		
		//http://tech.epay.dk/en/specification#276
		data.put("instantcallback", "1");

		//http://tech.epay.dk/en/specification#278
		data.put("ordertext", "Zerved");
		
		//http://tech.epay.dk/en/specification#279
		//data.put("group", "group");

		//http://tech.epay.dk/en/specification#280
		data.put("description", "" + mOrderId);
	
		//http://tech.epay.dk/en/specification#282
		boolean saveCard = ((Switch) findViewById(R.id.save_card)).isChecked();
		String callbackUrlPincodeString = "";
		if (saveCard) {
			data.put("subscription", "1");
			callbackUrlPincodeString = "?pincode=" + mPincode.getText().toString();
		} else {
			data.put("subscription", "0");
		}
		
		//http://tech.epay.dk/en/specification#275
		try {
			data.put("callbackurl", URLEncoder.encode(Config.API_URL + "orders/epaycallback_v2" + callbackUrlPincodeString, "UTF-8"));
		} catch (UnsupportedEncodingException e) {
			Log.e(TAG, "callbackurl urlencoding problem", e);				
		}		
		
		//http://tech.epay.dk/en/specification#283
		data.put("subscriptionname", "Zerved");
				
		//http://tech.epay.dk/en/specification#284
		//data.put("mailreceipt", "");
		
		//http://tech.epay.dk/en/specification#286
		//data.put("googletracker", "0");
		
		//http://tech.epay.dk/en/specification#287
		//data.put("backgroundcolor", "");
		
		//http://tech.epay.dk/en/specification#288
		//data.put("opacity", "");
		
		//http://tech.epay.dk/en/specification#289
		//data.put("declinetext", "This is the decline text");
		
		//data.put("ownreceipt", "1");
		
		//data.put("accepturl", "http://dev.zervedapp.appspot.com/api/v2/orders/epayaccept_v2"); // The accepturl is not used as the app changes the tab when a 'PaymentAccepted' event is received from the ePay mobile SDK. The documentation says accepturl must be filled out if ownreceipt = 1 and is thus supplied.
		
		// Epay doc: hash must not be supplied (the sdk adds it automatically)
		//data.put("hash", "" + md5(hashInputString.toString() + Config.EPAY_MD5_HASH_KEY)); 
		return data;
	}
}