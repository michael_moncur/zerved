package com.zervedapp.zerved.models;


import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;


public class OptionValue extends JSONModel {
	public String mOptionKey;
	public int mSelectedIndex;
	public String mOptionId;
	
	public OptionValue(String json) throws JSONException {
		super(json);
		mOptionKey = "";//this.getStringProperty("option_key");
		mOptionId = this.getStringProperty("option_id");
		mSelectedIndex = this.getIntegerProperty("option_value");
	}
		
	public OptionValue(String optionKey, String optionId, int selectedIndex) {
		mOptionId = optionId;
		mOptionKey = optionKey;
		mSelectedIndex = selectedIndex;
	}
	
	public String getId(){
		return mOptionId;
	}
	
	public int getValue(){
		return mSelectedIndex;
	}
		
	public boolean equals(Object object) {
		if (this == object) return true;
		if (!(object instanceof OptionValue)) return false;
		
		OptionValue optionValue = (OptionValue) object;
		return (optionValue.mSelectedIndex == mSelectedIndex && optionValue.mOptionId.equals(mOptionId));
	}
	
	public String toString() {
		return String.format("(%s, %s, %s)", mOptionKey, mOptionId, mSelectedIndex);
	}
	
	public JSONObject toJsonObject() {
		JSONObject json = new JSONObject();
		try {
			json.put("option_id", mOptionId);			
			json.put("option_value", mSelectedIndex);				
		}
		catch (JSONException e) {
			Log.e("OptionValue", "Failed to create JSON.", e);
		}
		return json;
	}	
	
}
