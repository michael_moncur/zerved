package com.zervedapp.zerved;

import com.koushikdutta.urlimageviewhelper.UrlImageViewHelper;
import com.zervedapp.zerved.models.MerchantLocation;

import android.content.Context;
import android.location.Location;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.TextView;

public class LocationAdapter extends ArrayAdapter<MerchantLocation> {
	public Location location;
	
	public LocationAdapter(Context context, int textViewResourceId) {
		super(context, textViewResourceId);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View row;
		LayoutInflater inflater = LayoutInflater.from(getContext());
		row = inflater.inflate(R.layout.merchant, null);
		
		TextView name = (TextView) row.findViewById(R.id.name);
		TextView description = (TextView) row.findViewById(R.id.description);
		ImageView image = (ImageView) row.findViewById(R.id.image);
		
		MerchantLocation merchant = this.getItem(position);
		name.setText(merchant.getName());
		if (location != null) {
			description.setText(MerchantLocation.formatDistance(getContext(), merchant.getDistance(location)));
		} else {
			description.setText("");
		}
		if (merchant.getImageUrl() == null) {
			image.setVisibility(View.GONE);
		} else {
			image.setVisibility(View.VISIBLE);
			image.setScaleType(ScaleType.FIT_XY);
			UrlImageViewHelper.setUrlDrawable(image, merchant.getImageUrl(), R.drawable.bar_logo_dummy);
		}
		return row;
	}
}
