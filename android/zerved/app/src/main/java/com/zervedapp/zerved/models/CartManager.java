package com.zervedapp.zerved.models;

import java.util.Calendar;
import java.util.HashMap;

public class CartManager {
	private HashMap<String, Cart> mCarts;
	private Calendar mTermsLastAccepted;
	
	public CartManager() {
		mCarts = new HashMap<String, Cart>();
	}
	
	public Cart getCart(MerchantLocation location, Bar bar, String group, Event event) {
		String cartId = getCartId(location, bar, group, event);
		Cart cart;
		if (mCarts.containsKey(cartId)) {
			cart = mCarts.get(cartId);
		}
		else {
			cart = new Cart(location, bar, group, event);
			cart.mTermsAccepted = termsAcceptedRecently();
			mCarts.put(cartId, cart);
		}
		return cart;
	}
	
	public void clearCart(MerchantLocation location, Bar bar, String group, Event event) {
		mCarts.remove(getCartId(location, bar, group, event));
	}
	
	private String getCartId(MerchantLocation location, Bar bar, String group, Event event) {
		if(group.equals("menu")) {
			return bar.getKey() + "_" + group + ((event == null) ? "" : "_" + event.getKey());
		}
		return location.getKey() + "_" + group;		
	}
	
	public void setTermsAccepted(boolean accepted) {
		if (accepted) {
			mTermsLastAccepted = Calendar.getInstance();
		} else {
			mTermsLastAccepted = null;
		}
	}
	
	public boolean termsAcceptedRecently() {
		if (mTermsLastAccepted == null) {
			return false;
		}
		Calendar yesterday = Calendar.getInstance();
		yesterday.add(Calendar.HOUR, -24);
		return mTermsLastAccepted.after(yesterday);
	}
}
