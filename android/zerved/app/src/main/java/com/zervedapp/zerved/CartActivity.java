package com.zervedapp.zerved;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.SystemClock;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Loader;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager.LayoutParams;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.TextView.OnEditorActionListener;

import com.zervedapp.zerved.loader.RESTLoader;
import com.zervedapp.zerved.loader.RESTLoader.RESTResponse;
import com.zervedapp.zerved.models.Bar;
import com.zervedapp.zerved.models.Cart;
import com.zervedapp.zerved.models.Event;
import com.zervedapp.zerved.models.OptionValue;
import com.zervedapp.zerved.models.ConsumerManager;
import com.zervedapp.zerved.models.Favourite;
import com.zervedapp.zerved.models.MerchantLocation;
import com.zervedapp.zerved.models.Product;
import com.zervedapp.zerved.models.ProductOption;

import dk.danskebank.mobilepay.sdk.MobilePay;
import dk.danskebank.mobilepay.sdk.ResultCallback;
import dk.danskebank.mobilepay.sdk.model.FailureResult;
import dk.danskebank.mobilepay.sdk.model.Payment;
import dk.danskebank.mobilepay.sdk.model.SuccessResult;

public class CartActivity extends AuthorizedActivity {
	private static final String TAG = CartActivity.class.getName();
	
	private static final int LOADER_PLACE_ORDER = 0x1;
	private static final int LOADER_CONSUMER = 0x2;
	private static final int LOADER_ADD_FAVOURITE = 0x3;
	private static final int LOADER_GET_FAVOURITES = 0x4;
	private static final int LOADER_ADDRESS = 0x5;
	private static final int LOADER_CANCEL = 0x6;
	private static final String ARGS_URI    = "com.zervedapp.zerved.ARGS_URI";
    private static final String ARGS_PARAMS = "com.zervedapp.zerved.ARGS_PARAMS";
    private static String URI = Config.API_URL+"placeorder";
	private static String ORDERS_URI = Config.API_URL+"orders/";
	private static String CONSUMER_URI = Config.API_URL+"consumers/";
    private static String FAVOURITE_URI = Config.API_URL + "favourites";
    private static final int PAYMENT_REQUEST = 1;
    private static final int EXPIRATION = 1000 * 60; // 1 minute
	private static final int PAYMENT_EPAY = 1;
	private static final int PAYMENT_SWIPP = 2;
	private static final int PAYMENT_MOBILEPAY_APPSWITCH = 3;
	private static final String cardStringMobilePay = "mobilepay_appswitch";

	private int MOBILEPAY_PAYMENT_REQUEST_CODE = 1337;
	private boolean mobilepay_order_is_processing;

	MerchantLocation mLocation;
	Bar mBar;
	String mGroup;
	Event mEvent;
	Cart mCart;
	String mOrderKey;
	String mOrderId;
	ArrayList<Favourite> mFavourites = new ArrayList<Favourite>();
	ItemsFragment mItemsFragment;
	AlertDialog mValidatePasswordAlert;
	boolean mPlaceOrderLoading = false;
	long mLoadedAt;
	Timer mTimer;
	static String loyaltyProgramName;
	static String customAllowedPaymentMethods;
			
	public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
        setContentView(R.layout.fragment);
        
        final ActionBar actionBar = getActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle(R.string.shopping_cart);
        setProgressBarIndeterminateVisibility(false);

		mobilepay_order_is_processing = false;

        Intent intent = getIntent();
        mGroup = intent.getStringExtra(CategoryListFragment.EXTRA_GROUP);
        
        try {
			mLocation = new MerchantLocation(intent.getStringExtra(CategoryListFragment.EXTRA_LOCATION));
			String barJson = intent.getStringExtra(CategoryListFragment.EXTRA_BAR);
			if (barJson != null) {
				mBar = new Bar(barJson);
			}
			mEvent = ((ZervedApp) getApplication()).mEvent;		
			
			if(mEvent != null) {
				actionBar.setSubtitle(mEvent.getName() + " " + mEvent.getTimeLabel());
			}
			else actionBar.setSubtitle(mGroup.equals("menu") ? mBar.getName() : mLocation.getName());
			
			
		} catch (JSONException e) {
			Log.e(TAG, "Failed to parse JSON.", e);
		}
        
        mCart = ((ZervedApp) getApplication()).getCart(mLocation, mBar, mGroup, mEvent);
        
        FragmentManager fm = getFragmentManager();
        mItemsFragment = new ItemsFragment();		
		FragmentTransaction ft = fm.beginTransaction();
		ft.add(R.id.fragment_content, mItemsFragment);
		ft.commit();
		
		// Login and load favourites + customer address + ask for delivery on authentication completed
		login();
			
		mLoadedAt = SystemClock.elapsedRealtime();
		
	}
	
	@Override
	public void onResume() {
		super.onResume();
		if (mobilepay_order_is_processing) {
			Log.d("henrik", "mobilepay_order_is_processing");

			mobilepay_order_is_processing = false;
		}
		mTimer = new Timer();
		// Check expiration
		mTimer.schedule(new TimerTask() {
			@Override
			public void run() {
				runOnUiThread(new Runnable() {
					public void run() {
						// Reload if expired
						if (SystemClock.elapsedRealtime() - mLoadedAt > EXPIRATION) {
							mItemsFragment.reload();
							mLoadedAt = SystemClock.elapsedRealtime();
						}
					}
				});

			}
		}, 0, 1000);
	}
	
	@Override
	public void onPause() {
		mTimer.cancel();
		super.onPause();
	}
	
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
    
    public void onItemClick(View v) {
    	View productView = (View) v.getParent();
    	View additionalInfo = productView.findViewById(R.id.additional);
    	if (additionalInfo.getVisibility() == View.VISIBLE) {
    		additionalInfo.setVisibility(View.GONE);
    	} else {
    		additionalInfo.setVisibility(View.VISIBLE);
    	}
    }
    
    public void viewTerms(View v) {
    	AlertDialog.Builder alert = new AlertDialog.Builder(this);
    	alert.setTitle(R.string.terms_and_conditions);
    	alert.setNegativeButton(R.string.close, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {}
		});
    	
    	View view = getLayoutInflater().inflate(R.layout.dialog_webview, null);
    	final WebView webView = (WebView) view.findViewById(R.id.webview);
    	final ProgressBar progress = (ProgressBar) view.findViewById(R.id.progress);
    	webView.setWebViewClient(new WebViewClient() {
    		@Override
    		public void onPageFinished(WebView view, String url) {
    			webView.setVisibility(View.VISIBLE);
    			progress.setVisibility(View.INVISIBLE);
    		}
    	});
    	alert.setView(view);
    	
    	webView.loadUrl(Config.SERVER_URL + "/android/terms");
    	alert.show();
    }
    
    public void placeOrder(View v) {
    	if (mPlaceOrderLoading) {
    		return;
    	}
    	
    	// Validate service
    	if (mCart.mService.equals("")) {
    		Toast.makeText(this, R.string.error_select_service, Toast.LENGTH_SHORT).show();
    		return;
    	}
    	if (mCart.mService.equals("table") && mCart.mTableNumber.equals("")) {
    		Toast.makeText(this, R.string.error_table_number, Toast.LENGTH_SHORT).show();
    		return;
    	}
    	
    	if(mCart.mService.equals("delivery") && mBar.getDeliveryMinOrderAmount() > mCart.mSubtotal) {
    		Toast.makeText(this, String.format(getResources().getString(R.string.minimum_order_amount_delivery_x), mBar.getDeliveryMinOrderAmountFormatted()), Toast.LENGTH_SHORT).show();
    		return;	
    	}
    	
    	// Validate terms
    	if (!mCart.mTermsAccepted) {
    		Toast.makeText(this, R.string.error_terms, Toast.LENGTH_SHORT).show();
    		return;
    	}

		// Force relogin (for security)
    	login();
    	checkPayment();
    	
    	
    }
    
    private void doPaymentCreditcard(Boolean hasPaymentSubscription)
    {
    	Log.d(TAG, "doPaymentCreditcard" + hasPaymentSubscription);

    	if (hasPaymentSubscription) {
			// Validate password and submit order
    		validatePassword();
    	} else {
    		// Payment needed
			submitOrderWithEPay(); 
    	}
    }
    
    private void doPaymentPaii(String phone)
    {
    	if(PaiiActivity.isPhoneValid(phone)) {
    		submitOrderWithPaii(phone);
    	} else {
    		Intent intent = new Intent(this, PaiiActivity.class);
			intent.putExtra(PaiiActivity.EXTRA_PHONE, phone);
			startActivityForResult(intent, PAYMENT_REQUEST);
    	}
    }

	private void doPaymentMobilePayAppSwitch() {
		submitOrderWithMobilePay();
	}
    
    private void showPaymentDialog(final String phone, final Boolean hasPaymentSubscription, final List<Integer> supportedPaymentMethods)
    {
    	AlertDialog.Builder builder = new AlertDialog.Builder(this);
		List<CharSequence> titles = new ArrayList<CharSequence>();
		for (int i = 0; i < supportedPaymentMethods.size(); i++) {
			titles.add(getTitleForPaymentType(supportedPaymentMethods.get(i)));
		}

		builder.setTitle(getString(R.string.select_payment_method))
			.setItems(titles.toArray(new String[titles.size()]), new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					doPayment(supportedPaymentMethods.get(which), phone, hasPaymentSubscription);
				}
			});
	  AlertDialog dialog = builder.create();
	  dialog.show();
    }

	private String getTitleForPaymentType(int type) {
		switch (type) {
			case PAYMENT_EPAY:
				return getString(R.string.payment_method_creditcard);
			case PAYMENT_SWIPP:
				return getString(R.string.payment_method_paii);
			case PAYMENT_MOBILEPAY_APPSWITCH:
				return getString(R.string.payment_method_mobilepay);
		}

		return "";
	}

	private void doPayment(int type, final String phone, final Boolean hasPaymentSubscription) {
		switch (type) {
			case PAYMENT_EPAY:
				doPaymentCreditcard(hasPaymentSubscription);
				break;
			case PAYMENT_SWIPP:
				doPaymentPaii(phone);
				break;
			case PAYMENT_MOBILEPAY_APPSWITCH:
				doPaymentMobilePayAppSwitch();
				break;
		}
	}

    public Cart getCart(){
    	return this.mCart;
    }
    @Override
    public void onAuthenticationCompleted() {
    	// Load the customers' favourites
    	reloadFavourites();
    	// Load the customer address and ask to select service
    	loadConsumerAddress();
    }
    
    @Override
	public void onAuthenticationFailed() {
		
	}
    
    public void reloadFavourites(){
    	Bundle args = new Bundle();
    	args.putParcelable(ARGS_URI, Uri.parse(CONSUMER_URI+getConsumer().mConsumerKey+"/favourites"));
    	args.putParcelable(ARGS_PARAMS, new Bundle());
		getLoaderManager().restartLoader(LOADER_GET_FAVOURITES, args, this);
    }
    
    public void checkPayment() {
    	Bundle args = new Bundle();
    	args.putParcelable(ARGS_URI, Uri.parse(CONSUMER_URI + getConsumer().mConsumerKey));
    	args.putParcelable(ARGS_PARAMS, new Bundle());
		getLoaderManager().restartLoader(LOADER_CONSUMER, args, this);
    }
    
    public void loadConsumerAddress() {
       	Bundle args = new Bundle();
    	args.putParcelable(ARGS_URI, Uri.parse(CONSUMER_URI + getConsumer().mConsumerKey));
    	args.putParcelable(ARGS_PARAMS, new Bundle());
		getLoaderManager().restartLoader(LOADER_ADDRESS, args, this);
    }
    
    public void validatePassword() {
    	AlertDialog.Builder builder = new AlertDialog.Builder(this);
    	builder.setTitle(R.string.confirm_payment);
    	LayoutInflater inflater = getLayoutInflater();
    	View view = inflater.inflate(R.layout.dialog_validate_password, null);
    	final EditText input = (EditText) view.findViewById(R.id.password);
		builder.setView(view);
		builder.setPositiveButton(R.string.confirm, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				submitOrderWithCurrentCard(input.getText().toString());
			}
		});
		builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// Cancelled
			}
		});
		mValidatePasswordAlert = builder.create();
    	// Keyboard actions
	    input.setOnEditorActionListener(new OnEditorActionListener() {
			@Override
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
				if (event != null && event.getAction() != KeyEvent.ACTION_DOWN)
					return false;
				if (event == null || actionId == EditorInfo.IME_ACTION_GO || event.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
					submitOrderWithCurrentCard(input.getText().toString());
					mValidatePasswordAlert.dismiss();
					return true;
				}
				return false;
			}
		});
		mValidatePasswordAlert.getWindow().setSoftInputMode(LayoutParams.SOFT_INPUT_STATE_VISIBLE);
		mValidatePasswordAlert.show();
    }

	private void startPayment() {
		if (mCart.mCard == cardStringMobilePay) {
			startPaymentMobilePay();
		}
		else {
			startPaymentEpay();
		}
	}

    private void startPaymentEpay() {
    	Intent intent = new Intent(this, EPayActivity.class);
    	intent.putExtra(EPayActivity.EXTRA_MUST_SAVE_CARD, false);
		intent.putExtra(EPayActivity.EXTRA_AMOUNT, mCart.mAmount);
		intent.putExtra(EPayActivity.EXTRA_CURRENCY, mCart.mCurrency);
		intent.putExtra(EPayActivity.EXTRA_ORDER_ID, mOrderId);
		intent.putExtra(EPayActivity.EXTRA_TITLE, getResources().getString(R.string.payment));
		intent.putExtra(EPayActivity.EXTRA_BUTTON_TITLE, getResources().getString(R.string.continue_payment));
		startActivityForResult(intent, PAYMENT_REQUEST);
    }

	private void startPaymentMobilePay() {
		// Check if the MobilePay app is installed on the device.
		boolean isMobilePayInstalled = MobilePay.getInstance().isMobilePayInstalled(getApplicationContext());

		if (isMobilePayInstalled) {
			// MobilePay is present on the system. Create a Payment object.
			Payment payment = new Payment();
			payment.setProductPrice(new BigDecimal(mCart.mAmount));
			payment.setOrderId(mOrderId);
			payment.setServerCallbackUrl(Config.API_URL + "orders/mobilepayappswitchcallback");

			// Create a payment Intent using the Payment object from above.
			Intent paymentIntent = MobilePay.getInstance().createPaymentIntent(payment);

			mobilepay_order_is_processing = true;

			// We now jump to MobilePay to complete the transaction. Start MobilePay and wait for the result using an unique result code of your choice.
			startActivityForResult(paymentIntent, MOBILEPAY_PAYMENT_REQUEST_CODE);
		} else {
			// MobilePay is not installed. Use the SDK to create an Intent to take the user to Google Play and download MobilePay.
			Intent intent = MobilePay.getInstance().createDownloadMobilePayIntent(getApplicationContext());
			startActivity(intent);
		}
	}

    public void submitOrderWithPaii(String phone) {
    	mCart.mCard = "4t";
    	mCart.mPhone = phone;
    	mCart.mNordpayAuthorization = null;
    	mCart.mNordpayRegistration = null;
    	mCart.mNordpayMethod = null;
    	mCart.mObscuredCardNumber = null;
    	submitOrder();
    }
    
    public void submitOrderWithCurrentCard(String password) {
    	mCart.mCard = "current";
    	mCart.mPassword = password;
    	mCart.mObscuredCardNumber = null;
    	mCart.mNordpayRegistration = null;
    	mCart.mNordpayAuthorization = null;
    	submitOrder();
    }
        
    public void submitOrderWithEPay() {
    	mCart.mCard = "epay_new_card";
    	mCart.mObscuredCardNumber = null;
    	mCart.mNordpayRegistration = null;
    	mCart.mNordpayAuthorization = null;
    	submitOrder();
    }

	public void submitOrderWithMobilePay() {
		mCart.mCard = cardStringMobilePay;
		submitOrder();
	}

    public void changeCardClick(View view) {
    	mValidatePasswordAlert.dismiss();
    	submitOrderWithEPay();
    }
    
    private void submitOrder()
    {
    	// Display progress message
    	setProgressBarIndeterminateVisibility(true);
    	mPlaceOrderLoading = true;
    	mItemsFragment.startPlacingOrder();
    	
    	// Build parameters for order
    	JSONObject orderParams = mCart.toJson();
    	
    	// Post to server
    	Bundle args = new Bundle();
    	args.putParcelable(ARGS_URI, Uri.parse(URI));
    	Bundle params = new Bundle();
    	params.putString("json", orderParams.toString());
    	args.putParcelable(ARGS_PARAMS, params);
		getLoaderManager().restartLoader(LOADER_PLACE_ORDER, args, this);
    }
    
    @Override
	public Loader<RESTResponse> onCreateLoader(int id, Bundle args) {
		if (args != null && args.containsKey(ARGS_URI) && args.containsKey(ARGS_PARAMS)) {
            Uri    action = args.getParcelable(ARGS_URI);
            Bundle params = args.getParcelable(ARGS_PARAMS);
            if (id == LOADER_PLACE_ORDER || id == LOADER_ADD_FAVOURITE) {
            	ConsumerManager consumer = ((ZervedApp) getApplication()).getConsumer();
    	        return new RESTLoader(this, RESTLoader.HTTPVerb.POST, action, params, consumer.mAccessToken);
            } else if (id == LOADER_CONSUMER || id == LOADER_GET_FAVOURITES || id == LOADER_ADDRESS) {
            	ConsumerManager consumer = ((ZervedApp) getApplication()).getConsumer();
    	        return new RESTLoader(this, RESTLoader.HTTPVerb.GET, action, params, consumer.mAccessToken);
            }
			else if (id == LOADER_CANCEL) {
				return new RESTLoader(this, RESTLoader.HTTPVerb.POST, action, params, getConsumer().mAccessToken);
			}
		}
		return super.onCreateLoader(id, args);
    }
    
    private void orderPlaced(String order_key) {
    	// Clear cart
		((ZervedApp) getApplication()).clearCart(mLocation, mBar, mGroup, mEvent);
		// Start pulling notifications
		NotificationListener nl = new NotificationListener();
		//Log.i(TAG, "Start notification listener");
		NotificationService.scheduleAlarms(nl, this);
		// Launch order activity
		Intent intent = new Intent(this, OrderActivity.class);
		intent.putExtra(OrderActivity.EXTRA_ORDER_KEY, order_key);
		startActivity(intent);
		// Finish cart activity
		finish();
    }
    
    @Override
	public void onLoadFinished(Loader<RESTLoader.RESTResponse> loader, RESTLoader.RESTResponse data) {
    	Log.d(TAG, "onLoadFinished");
		int    code = data.getCode();
        String json = data.getData();
        if (loader.getId() == LOADER_PLACE_ORDER) {
        	// End progress spinner
        	setProgressBarIndeterminateVisibility(false);
        	mPlaceOrderLoading = false;
        	if (code == 200) {
	        	try {
	        		JSONObject object = (JSONObject) new JSONTokener(json).nextValue();
	            	Log.d(TAG, "onLoadFinished 200");

	        		mOrderKey = object.getString("order_key");
	        		mOrderId = object.getString("order_id");
	        		if (object.has("payment_needed") && object.getBoolean("payment_needed")) {
	        			// Payment needed
	        			// Launch payment activity
	                	mItemsFragment.endPlacingOrder();
	        			startPayment();
	        		} else {
	        			// Order placed
	        			orderPlaced(mOrderKey);
	        		}
	        	} catch (JSONException e) {
	        		Log.e(TAG, "Failed to parse JSON.", e);
	        	}
        	} else if (code == 403) {
        		// Wrong password
            	mItemsFragment.endPlacingOrder();
        		validatePassword();
        		Toast toast = Toast.makeText(this, json, Toast.LENGTH_SHORT);
        		toast.setGravity(Gravity.CENTER, 0, 0);
        		toast.show();
        	} else {
        		// Error
            	mItemsFragment.endPlacingOrder();
        		Toast toast = Toast.makeText(this, json, Toast.LENGTH_SHORT);
        		toast.setGravity(Gravity.CENTER, 0, 0);
        		toast.show();
        		if (code == 409) {
        			// Validation error - reload quote/bar information
        			mItemsFragment.reload();
        		}
        	}
        } else if (loader.getId() == LOADER_CONSUMER) {
        	if (code == 200) {
        		try {
        			JSONObject object = (JSONObject) new JSONTokener(json).nextValue();
        			Boolean hasPaymentSubscription = object.has("current_payment_subscription");
        			String phone = object.has("phone") ? object.getString("phone") : "";

					Log.d(TAG, "paymentMethods: " + customAllowedPaymentMethods);
					if (customAllowedPaymentMethods == null) {
						doPaymentCreditcard(hasPaymentSubscription);
					}
					else {
						// Custom allowed payment methods specified.
						List<String> customAllowedPaymentMethodsList = Arrays.asList(customAllowedPaymentMethods.split(","));
						boolean epayIsSupported = customAllowedPaymentMethodsList.contains("epay");
						boolean swippIsSupported = customAllowedPaymentMethodsList.contains("swipp");
						boolean mobilePayAppSwitchIsSupported = customAllowedPaymentMethodsList.contains("mobilepay_appswitch");
						List<Integer> supportedPaymentMethods = new ArrayList<Integer>();
						if (epayIsSupported) {
							supportedPaymentMethods.add(PAYMENT_EPAY);
						}

						if (swippIsSupported) {
							supportedPaymentMethods.add(PAYMENT_SWIPP);
						}

						if (mobilePayAppSwitchIsSupported) {
							supportedPaymentMethods.add(PAYMENT_MOBILEPAY_APPSWITCH);
						}

						if (supportedPaymentMethods.size() > 1) {
							showPaymentDialog(phone, hasPaymentSubscription, supportedPaymentMethods);
						}
						else if (epayIsSupported) {
							doPaymentCreditcard(hasPaymentSubscription);
						}
						else if (swippIsSupported) {
							doPaymentPaii(phone);
						}
						else if (mobilePayAppSwitchIsSupported) {
							doPaymentMobilePayAppSwitch();
						}
						else {
							AlertDialog.Builder builder = new AlertDialog.Builder(this);
							builder.setTitle(getString(R.string.no_payment_methods_found))
									.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
										@Override
										public void onClick(DialogInterface dialog, int id) {
											reloadFragment();
										}
									});
							AlertDialog dialog = builder.create();
							dialog.show();
						}
					}

        		} catch (JSONException e) {
	        		Log.e(TAG, "Failed to parse JSON.", e);
	        	}
        	} else {
        		// Error
        		Toast.makeText(this, json, Toast.LENGTH_SHORT).show();
        	}
        } else if (loader.getId() == LOADER_ADD_FAVOURITE) {
        	if (code == 200) {
        		Toast.makeText(this, R.string.added_to_favourites, Toast.LENGTH_SHORT).show();
        		reloadFavourites();
        	} else {
        		// Error
        		Toast.makeText(this, json, Toast.LENGTH_SHORT).show();
        	}
        } else if (loader.getId() == LOADER_GET_FAVOURITES) {
        	if (code == 200) {
        		mFavourites = Favourite.initList(json);
        		mItemsFragment.reload();
        	} else {
        		Toast.makeText(this, R.string.favourites_not_available, Toast.LENGTH_SHORT).show();
        	}

        }
		else if (loader.getId() == LOADER_CANCEL) {
			if (code == 200) {
				// Order cancelled
				Toast.makeText(this, R.string.order_cancelled, Toast.LENGTH_SHORT).show();
				//finish();
			}
			else if (code > 0) {
				Toast.makeText(this, json, Toast.LENGTH_SHORT).show();
			}
			else {
				Toast.makeText(this, R.string.no_connection, Toast.LENGTH_SHORT).show();
			}
		}
		else if (loader.getId() == LOADER_ADDRESS) {
        	if(code == 200) {
        		JSONObject object;
				try {
					object = (JSONObject) new JSONTokener(json).nextValue();
	        		if (object.has("name") && object.getString("name").length() > 0 && getCart().mName.length() == 0) {
	        			getCart().mName = object.getString("name").equals("null") ? "" :  object.getString("name");
	        		}
	        		if (object.has("address1") && object.getString("address1").length() > 0 && getCart().mAddress1.length() == 0) {
	        			getCart().mAddress1 = object.getString("address1").equals("null") ? "" :  object.getString("address1");
	        		}
	        		if (object.has("address2") && object.getString("address2").length() > 0 && getCart().mAddress2.length() == 0) {
	        			getCart().mAddress2 =  object.getString("address2").equals("null") ? "" :  object.getString("address2");
	        		}
	        		if (object.has("city") && object.getString("city").length() > 0 && getCart().mCity.length() == 0) {
	        			getCart().mCity = object.getString("city").equals("null") ? "" :  object.getString("city");
	        		}
	        		if (object.has("postcode") && object.getString("postcode").length() > 0 && getCart().mPostcode.length() == 0) {
	        			getCart().mPostcode = object.getString("postcode").equals("null") ? "" :  object.getString("postcode");
	        		}
	        		if (object.has("phone") && object.getString("phone").length() > 0 && getCart().mPhone.length() == 0) {
	        			getCart().mPhone = object.getString("phone").equals("null") ? "" :  object.getString("phone");
	        		}
				} catch (JSONException e) {
					e.printStackTrace();
				}
        	} 
        	// Service popup
    		if (mGroup.equals("menu") && mCart.mService.equals("") || (mCart.mService.equals("table") && mCart.mTableNumber.equals(""))) {
    			askForDelivery();
			}
		}

        super.onLoadFinished(loader, data);
    }
    
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == PAYMENT_REQUEST) {
			if (resultCode == Activity.RESULT_OK) {
				if (data != null && data.hasExtra(PaiiActivity.EXTRA_PHONE)) {
					// Payment completed - submit order
					submitOrderWithPaii(data.getStringExtra(PaiiActivity.EXTRA_PHONE));
				}
				else {
					orderPlaced(mOrderKey);
				} 
			}
		}
		if (requestCode == MOBILEPAY_PAYMENT_REQUEST_CODE) {
			// The request code matches our MobilePay Intent
			mobilepay_order_is_processing = false;
			MobilePay.getInstance().handleResult(resultCode, data, new ResultCallback() {
				@Override
				public void onSuccess(SuccessResult result) {
					// The payment succeeded - you can deliver the product.
					orderPlaced(mOrderKey);
				}
				@Override
				public void onFailure(FailureResult result) {
					// The payment failed. FailureResult object holds further information.

					// You should inform the user why the error happened. See the list of possible error codes for more information.

					if (result.getErrorCode() == MobilePay.ERROR_RESULT_CODE_UPDATE_APP) {
						// Notify the user to update MobilePay.
						showPaymentResultDialog(getString(R.string.payment_result_dialog_error_update_title), getString(R.string.payment_result_dialog_error_update_message));
					}
					else if (result.getErrorCode() == MobilePay.ERROR_RESULT_CODE_LIMITS_EXCEEDED) {
						showPaymentResultDialog(getString(R.string.payment_result_dialog_error_limits_exceeded_title), getString(R.string.payment_result_dialog_error_limits_exceeded_message));
					}
					else if (result.getErrorCode() == MobilePay.ERROR_RESULT_CODE_TIMEOUT) {
						showPaymentResultDialog(getString(R.string.payment_result_dialog_error_timeout_title), getString(R.string.payment_result_dialog_error_timeout_message));
					}
					else if (result.getErrorCode() == MobilePay.ERROR_RESULT_CODE_MERCHANT_TIMEOUT) {
						showPaymentResultDialog(getString(R.string.payment_result_dialog_error_merchant_timeout_title), getString(R.string.payment_result_dialog_error_merchant_timeout_message));
					}
					else {
						// Generic error
						showPaymentResultDialog(getString(R.string.payment_result_dialog_generic_error_title), getString(R.string.payment_result_dialog_generic_error_prefix) + " " + String.valueOf(result.getErrorCode()));
						Log.e(TAG, "Generic MobilePay error: " + String.valueOf(result.getErrorCode()) + " " + result.getErrorMessage());
					}


					cancelOrder();
				}
				@Override
				public void onCancel() {
					// The payment was cancelled.
					// The payment was cancelled, which means the user jumped back from MobilePay before processing the payment.
					showPaymentResultDialog(getString(R.string.payment_result_dialog_cancelled_title), getString(R.string.payment_result_dialog_cancelled_message));
					cancelOrder();
				}
			});
		}
	}

	private void cancelOrder() {
		// Submit cancellation
		JSONObject postParams = new JSONObject();
		try {
			postParams.put("status", "cancelled");
		} catch (JSONException e) {
			Log.e(TAG, "Failed to create JSON.", e);
		}
		Bundle args = new Bundle();
		args.putParcelable(ARGS_URI, Uri.parse(ORDERS_URI+mOrderKey));
		Bundle params = new Bundle();
		params.putString("json", postParams.toString());
		args.putParcelable(ARGS_PARAMS, params);
		getLoaderManager().restartLoader(LOADER_CANCEL, args, CartActivity.this);
	}

	private void showPaymentResultDialog(String title, String content) {
		// In this demo app we show a simple dialog with information of the transaction.
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle(title)
				.setMessage(content)
				.setPositiveButton(getString(R.string.ok), null);
		AlertDialog dialog = builder.create();
		dialog.show();
	}

    public void updateItem(View view) {
    	
    	reloadFavourites();
    	
    	Cart.Item cartItem = (Cart.Item) view.getTag();
    	Product product = cartItem.getProduct();
    	Spinner quantity = (Spinner) view.findViewById(R.id.quantity);
    	Cart cart = ((ZervedApp) getApplication()).getCart(mLocation, mBar, mGroup, mEvent);
    	
    	// Collect selected options
    	ArrayList<OptionValue> options = new ArrayList<OptionValue>();
    	LinearLayout optionsView = (LinearLayout) view.findViewById(R.id.options);
    	for (int i=0; i < optionsView.getChildCount(); i++) {
    		View optionView = optionsView.getChildAt(i);
    		ProductOption productOption = (ProductOption) optionView.getTag(R.id.product_option);
    		if ((optionView instanceof Spinner)) {
    			int pos = ((Spinner) optionView).getSelectedItemPosition();
    			// For non-required select-one options, position 0 is the title
    			int value = productOption.getBooleanProperty("required") ? pos : pos-1;
    			if (value >= 0) {
    				options.add(new OptionValue(productOption.getKey(), productOption.getId(), value));
    			}
    		} else if ((optionView instanceof CheckBox)) {
    			if (((CheckBox) optionView).isChecked()) {
    				int index = (Integer) optionView.getTag(R.id.option_index);
    				options.add(new OptionValue(productOption.getKey(), productOption.getId(), index));
    			}
    		}
    	}
    	
    	// Create cart item object
    	Cart.Item newItem = cart.new Item(product, (Integer) quantity.getSelectedItem(), options);
    	
    	// Validate and add item
    	String error = product.validateItem(this, newItem);
    	if (error.equals("")) {
    		Log.i(TAG, "Update item");
    		cart.updateItem(cartItem, newItem);
    		Log.i(TAG, "Update adapter");
    		mItemsFragment.refreshAdapter();
    		Log.i(TAG, "Reload quote");
    		mItemsFragment.reload();
    	} else {
    		Toast.makeText(this, error, Toast.LENGTH_SHORT).show();
    	}
    }
    
    public void deleteItem(View view) {
    	Cart.Item cartItem = (Cart.Item) view.getTag();
    	mCart.removeItem(cartItem);
    	mItemsFragment.reload();
    }
    
    public void addFavourite(Cart.Item cartItem) {
		JSONObject postParams = new JSONObject();
		Product product = cartItem.getProduct();
		ArrayList<OptionValue> itemOptions = cartItem.getOptions();
		
		// options array
    	JSONArray options = new JSONArray();	
		for(OptionValue option: itemOptions){
			options.put(option.toJsonObject());
		}
		
    	try {
    		postParams.put("product", product.getKey());
    		postParams.put("options", options);
    		if(this.mGroup.equals("menu"))
    			postParams.put("bar", this.mBar.getKey());
    		else 
    			postParams.put("location", this.mLocation.getKey() );    		
    	} catch (JSONException e) {
    		Log.e(TAG, "Failed to create JSON.", e);
    	}
		Bundle args = new Bundle();
    	args.putParcelable(ARGS_URI, Uri.parse(FAVOURITE_URI));
    	Bundle params = new Bundle();
    	params.putString("json", postParams.toString());
    	args.putParcelable(ARGS_PARAMS, params);
		getLoaderManager().restartLoader(LOADER_ADD_FAVOURITE, args, this);
	}

    public void removeFavourite(String key){
    	getConsumer().deleteFavourite(this, key);
    }
    
	public static class ItemsFragment extends RESTListFragment {
		
		private static final String TAG = ItemsFragment.class.getName();
		private ArrayAdapter<Cart.Item> mAdapter;
		
		protected static String URI = Config.API_URL+"quote";
		
		View mFooterView, mHeaderView;
		
		@Override
		public String getAuthorization() {
			return ((ZervedApp) getActivity().getApplication()).getConsumer().mAccessToken;
		}
		
		@Override
		public void onActivityCreated(Bundle savedInstanceState) {
			super.onActivityCreated(savedInstanceState);
			
			mAdapter = new CartItemAdapter(getActivity(), R.layout.cart_item);
	        this.setListAdapter(mAdapter);
	        reload();
		}
		
		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
			View view = inflater.inflate(R.layout.progress_list, null);
			ListView listView = (ListView) view.findViewById(android.R.id.list);
			listView.setItemsCanFocus(true);
			
			mFooterView = inflater.inflate(R.layout.cart_footer, null);

			CheckBox acceptTerms = (CheckBox) mFooterView.findViewById(R.id.accept_terms);
			acceptTerms.setChecked(getCart().mTermsAccepted);
			acceptTerms.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
				@Override
				public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
					getCart().mTermsAccepted = isChecked;
					((ZervedApp) getActivity().getApplication()).setTermsAccepted(isChecked);
				}
			});
			
			listView.addFooterView(mFooterView);
			
			mHeaderView = inflater.inflate(R.layout.event_summary, null);
			Event mEvent = getCart().getEvent();
			
			// Event Summary
			if(mEvent != null){
				((TextView) mHeaderView.findViewById(R.id.event_name)).setText(mEvent.getName() + " " + mEvent.getTimeStartFormatted());
				((TextView) mHeaderView.findViewById(R.id.event_date_start_formatted)).setText(mEvent.getDateOnlyStartFormatted());
				((TextView) mHeaderView.findViewById(R.id.event_time_label)).setText(R.string.serving_time);
				((TextView) mHeaderView.findViewById(R.id.event_date_delivery_time_formatted)).setText(mEvent.getTimeDeliveryFormatted() + " (" + mEvent.getTimeLabel() + ")");
				
				listView.addHeaderView(mHeaderView);
			}
						
			return view;
		}
		
		@Override
		public void reload() {
			getActivity().setProgressBarIndeterminateVisibility(true);
			super.reload();
		}
		
		@Override
		public String getUri() {
			return URI;
		}
		
		@Override
		public Bundle getParams() {
			Bundle params = new Bundle();
			params.putString("json", getCart().toJson().toString());
			return params;
		}
		
		@Override
		public RESTLoader.HTTPVerb getVerb() {
			return RESTLoader.HTTPVerb.POST;
		}
		
		private Cart getCart() {
			return ((CartActivity) getActivity()).mCart;
		}
		
		@Override
		public void onLoadFinished(Loader<RESTLoader.RESTResponse> loader, RESTLoader.RESTResponse data) {
			getActivity().setProgressBarIndeterminateVisibility(false);
			super.onLoadFinished(loader, data);
		}
		
		@Override
		public void onJsonLoaded(String json) {
			try {
            	JSONObject quote = (JSONObject) new JSONTokener(json).nextValue();

            	// Update merchant data
            	MerchantLocation location = null;
            	if (quote.has("location")) {
            		location = new MerchantLocation(quote.getJSONObject("location").toString());
            		getCart().setLocation(location);
            	}
            	Bar bar = null;
            	if (quote.has("bar")) {
            		bar = new Bar(quote.getJSONObject("bar").toString());
            		getCart().setBar(bar);
            	}

				if (quote.has("custom_allowed_payment_methods")) {
					customAllowedPaymentMethods = quote.getString("custom_allowed_payment_methods").toString();
				}
				else {
					customAllowedPaymentMethods = null;
				}

    	    	// Init service
    	    	if (getCart().mService.equals("")) {
    	    		if (getCart().getGroup().equals("entrance")) {
    	    			getCart().mService = "entrance";
    	    		}
    	    		else if (bar != null && bar.getBooleanProperty("counter_service") && !bar.getBooleanProperty("table_service") && !bar.hasToStayAndToGo() && !bar.hasDeliveryService()) {
    	    			getCart().mService = "counter";
    	    			if (bar.hasCounterServiceToGo()) {
    	    				getCart().mToGo = true;
    	    			} else if (bar.hasCounterServiceToStay()) {
    	    				getCart().mToGo = false;
    	    			}
    	    		} 
    	    		else if (bar != null && bar.getBooleanProperty("table_service") && !bar.getBooleanProperty("counter_service") && !bar.hasDeliveryService()) {
    	    			getCart().mService = (getCart().mTableNumber.length() > 0)  ? "table" : "";
    	    		}   
    	    	} else if (getCart().mService.equals("counter") && bar != null && !bar.getBooleanProperty("counter_service")) {
    	    		getCart().mService = "";
    	    	} else if (getCart().mService.equals("table") && bar != null && !bar.getBooleanProperty("table_service")) {
    	    		getCart().mService = "";
    	    	} else if (getCart().mService.equals("delivery") && bar != null && !bar.getBooleanProperty("delivery_service")) {
    	    		getCart().mService = "";
    	    	}
    	    	
    	    	updateDeliveryView();
    	    	
    	    	TextView totalLabel = (TextView) mFooterView.findViewById(R.id.total_label);
    	    	getCart().mCurrency = quote.getString("currency");
    	    	totalLabel.setText(String.format(getResources().getText(R.string.total_x).toString(), quote.getString("currency")));
    	    	TextView total = (TextView) mFooterView.findViewById(R.id.total);
    	    	getCart().mAmount = quote.getDouble("total_incl_tax");
    	    	total.setText(quote.getString("total_incl_tax_formatted"));
    	    	
    	    	getCart().mSubtotal = quote.getDouble("subtotal"); 
    	    	
    	    	// Update tip
    	    	if (quote.getBoolean("tip_enabled")) {
    	    		mFooterView.findViewById(R.id.tip_row).setVisibility(View.VISIBLE);
	    	    	TextView tipAmount = (TextView) mFooterView.findViewById(R.id.tip_amount);
	    	    	ImageButton tipAdd = (ImageButton) mFooterView.findViewById(R.id.tip_add);
	    	    	if(getCart().mTipPercent == 0.0){
	    	    		tipAmount.setVisibility(View.GONE);
	    	    		tipAdd.setVisibility(View.VISIBLE);
	    	    		tipAdd.setOnClickListener(new OnClickListener() {
							@Override
							public void onClick(View v) {
								((CartActivity)getActivity()).selectTip(v);								
							}
						});
	    	    	}
	    	    	else {
	    	    		tipAdd.setVisibility(View.GONE);	   
	    	    		tipAmount.setVisibility(View.VISIBLE);	    	    	 
	    	    		tipAmount.setText(quote.getString("tip_formatted"));
	    	    	}
    	    	} else {
    	    		mFooterView.findViewById(R.id.tip_row).setVisibility(View.GONE);
    	    		getCart().mTipPercent = 0.0;
    	    	}
    	    	
    	    	// Fees
    	    	LinearLayout feesList = (LinearLayout) mFooterView.findViewById(R.id.fees_list);
    	    	feesList.removeAllViews();
    	    	if (quote.has("fees")) {
    	    		JSONArray fees = quote.getJSONArray("fees");
    	    		for (int i=0; i < fees.length(); i++) {
    	    			JSONObject fee = fees.getJSONObject(i);
    	    			View feeView = getActivity().getLayoutInflater().inflate(R.layout.item_fee, null);
    	    			((TextView) feeView.findViewById(R.id.fee_label)).setText(fee.getString("name"));
    	    			((TextView) feeView.findViewById(R.id.fee_price)).setText(fee.getString("price_formatted"));
    	    			feesList.addView(feeView);
    	    		}
    	    	}
    	    	
    	    	// Waiting time
    	    	if (getCart().getEstimatedWaitingTime() > 0) {
    	    		mFooterView.findViewById(R.id.waiting_time_container).setVisibility(View.VISIBLE);
    	    		((TextView) mFooterView.findViewById(R.id.waiting_time_minutes)).setText(String.format(getResources().getText(R.string.x_min).toString(), getCart().getEstimatedWaitingTime()));
    	    	}
    	    	
    	    	// Loyalty code
    	    	if (quote.has("loyalty_code") && !quote.getString("loyalty_code").equals("")) {
    	    		getCart().mLoyaltyCode = encodeHTMLEntities(quote.getString("loyalty_code"));
    	    	}
    	    	if (getCart().mLoyaltyCode == null || getCart().mLoyaltyCode.equals("")) {
    	    		((TextView) mFooterView.findViewById(R.id.loyalty_value)).setText(R.string.add);
    	    		mFooterView.findViewById(R.id.discount_row).setVisibility(View.GONE);
    	    	} else {
    	    		((TextView) mFooterView.findViewById(R.id.loyalty_value)).setText(decodeHTMLEntities(getCart().mLoyaltyCode));
    	    		if(quote.has("loyalty_code_valid") && quote.has("total_loyalty_discount_incl_tax_formatted")) {
    	    			if(quote.getBoolean("loyalty_code_valid")) {
    	    				mFooterView.findViewById(R.id.discount_row).setVisibility(View.VISIBLE);
    	    				((TextView)mFooterView.findViewById(R.id.discount_amount)).setText(quote.getString("total_loyalty_discount_incl_tax_formatted"));
    	    			} else {
    	    				mFooterView.findViewById(R.id.discount_row).setVisibility(View.GONE);
    	    				if(quote.has("loyalty_code") && !quote.getString("loyalty_code").equals("")) {
    	    					Toast.makeText(getActivity(), getString(R.string.loyalty_number_invalid), Toast.LENGTH_SHORT).show();
    	    				}
    	    			}
    	    		} else {
	    				mFooterView.findViewById(R.id.discount_row).setVisibility(View.GONE);
    	    		}
    	    	}
    	    	mFooterView.findViewById(R.id.loyalty_layout).setVisibility((!getCart().getGroup().equals("entrance") && quote.getBoolean("has_loyalty_program")) ? View.VISIBLE : View.GONE);
    	    	
    	    	if(quote.has("has_loyalty_program") && quote.getBoolean("has_loyalty_program") && quote.has("loyalty_program_name")) {
    	    		if(!quote.getString("loyalty_program_name").equals("")) {
    	    			loyaltyProgramName = quote.getString("loyalty_program_name");
    	    		} else {
    	    			loyaltyProgramName = getString(R.string.loyalty_number);
    	    		}
    	    		TextView tv = (TextView)mFooterView.findViewById(R.id.loyalty_label);
    	    		tv.setText(loyaltyProgramName);
    	    	}
    	    	
    	    	// Update cart items list
            	JSONArray items = quote.getJSONArray("items");
                for (int i = 0; i < items.length(); i++) {
                	JSONObject item = items.getJSONObject(i);
                	getCart().addQuoteItem(i, item);
                }
                // Remove invalid/outdated items
                int invalidItems = getCart().validateQuoteItems();
                if (invalidItems > 0) {
                	String invalidItemsMessage = getResources().getQuantityString(R.plurals.invalid_items, invalidItems, invalidItems);
                	Toast.makeText(getActivity(), invalidItemsMessage, Toast.LENGTH_LONG).show();
                }

                refreshAdapter();
                
            }
            catch (JSONException e) {
                Log.e(TAG, "Failed to parse JSON.", e);
            }
		}
		
		public void refreshAdapter() {
			mAdapter.clear();
            for (Cart.Item item: getCart().getItems()) {
            	mAdapter.add(item);
            }
		}
		
		public void startPlacingOrder() {
			// Hide shopping cart view and show a progess text
			TextView msgView = (TextView) getView().findViewById(R.id.progress_text);
	    	msgView.setText(R.string.placing_order);
			getView().findViewById(R.id.progress).setVisibility(View.VISIBLE);
	        getView().findViewById(R.id.progress_text).setVisibility(View.VISIBLE);
	        getListView().setVisibility(View.GONE);
		}
		
		public void endPlacingOrder() {
			// Hide progress text and redisplay shopping cart
			TextView msgView = (TextView) getView().findViewById(R.id.progress_text);
	    	msgView.setText("");
	        getView().findViewById(R.id.progress).setVisibility(View.GONE);
	        getView().findViewById(R.id.progress_text).setVisibility(View.GONE);
	        getListView().setVisibility(View.VISIBLE);
		}

		private int deliveryMethodsCount() {
			int count = 0;
			Bar bar = getCart().getBar();
			if (bar.hasCounterService()) {
				if (bar.hasToStayAndToGo()) {
					count += 2;
				}
				else {
					count++;
				}
			}

			if (bar.hasTableService()) {
				count++;
			}

			if (bar.hasDeliveryService()) {
				count++;
			}

			return count;
		}

		public void updateDeliveryView() {			
	    	RelativeLayout deliveryLayout = (RelativeLayout) mFooterView.findViewById(R.id.delivery_layout);
	    	TextView deliveryChoice = (TextView) mFooterView.findViewById(R.id.delivery_choice);
	    	
	    	if(getCart().getGroup().equals("entrance")){
	    		deliveryLayout.setVisibility(View.GONE);
	    	}
			else if (deliveryMethodsCount() <= 1) {
				deliveryLayout.setVisibility(View.GONE);
			}
	    	else {  
	    		if(getCart().mService.equals("delivery")){
	    			deliveryChoice.setText(String.format(getString(R.string.address_delivery_x), getCart().mPostcode));
	    		}
	    		else if(getCart().mService.equals("table")) {
	    			deliveryChoice.setText(getString(R.string.serve_at_table) + " " + getCart().mTableNumber);
	    		} 
	    		else if (getCart().mService.equals("counter")) {
	    			if(getCart().getBar().hasToStayAndToGo()) {
		    			if(getCart().mToGo){
		    				deliveryChoice.setText(R.string.counter_to_go);
		    			}
		    			else deliveryChoice.setText(R.string.counter_to_stay);
	    			}
	    			else deliveryChoice.setText(R.string.pickup_at_counter);
	    		} else {
	    			deliveryChoice.setText(R.string.select);
	    		}
	    	}
		}
	}

	public void askForDelivery(){
				
		final Cart cart = getCart();
		final Bar bar = cart.getBar();
      	final InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		LayoutInflater inflater = getLayoutInflater();
		final View tbnView = inflater.inflate(R.layout.delivery, null);

		RadioGroup services = (RadioGroup) tbnView.findViewById(R.id.services);
		
		RadioButton serviceCounter = (RadioButton) tbnView.findViewById(R.id.service_counter);
			final RadioGroup radioGroup_to_go = (RadioGroup) tbnView.findViewById(R.id.tostay_togo);
		
		RadioButton serviceTable = (RadioButton) tbnView.findViewById(R.id.service_table);
        	final LinearLayout tableNumberLine = (LinearLayout) tbnView.findViewById(R.id.table_nr_layout);
        	final EditText tableNumber = (EditText) tbnView.findViewById(R.id.enter_table_number);

        
        RadioButton serviceDelivery = (RadioButton) tbnView.findViewById(R.id.service_delivery);
        
        final LinearLayout addressDelivery = (LinearLayout) tbnView.findViewById(R.id.address_delivery);
        
    		final EditText deliveryName = (EditText) tbnView.findViewById(R.id.delivery_name_field);
    		final EditText deliveryAddress1 = (EditText) tbnView.findViewById(R.id.delivery_address1_field);
    		final EditText deliveryAddress2 = (EditText) tbnView.findViewById(R.id.delivery_address2_field);
    		final EditText deliveryCity = (EditText) tbnView.findViewById(R.id.delivery_city_field);
    		final EditText deliveryPostcode = (EditText) tbnView.findViewById(R.id.delivery_postcode_field);
    		final EditText deliveryPhone = (EditText) tbnView.findViewById(R.id.delivery_phone_field);
    		final EditText deliveryInstructions = (EditText) tbnView.findViewById(R.id.delivery_instructions_field);

    	final TextView deliveryWarning = (TextView) tbnView.findViewById(R.id.delivery_warning);

        
        final Button confirmDialog = (Button) tbnView.findViewById(R.id.confirm_dialog);

        // Check selected service button
		if (cart.mService.equals("table")) 
			services.check(R.id.service_table);
		else if(cart.mService.equals("delivery"))
			services.check(R.id.service_delivery);
		else if (cart.mService.equals("counter")) {
			services.check(R.id.service_counter);
		}
        
        int counterVisibility = View.GONE, tableVisibility = View.GONE, deliveryVisibility = View.GONE;
        if (cart.getGroup().equals("menu")) {      	
            counterVisibility = bar.hasCounterService() ? View.VISIBLE : View.GONE;
            tableVisibility = bar.hasTableService() ? View.VISIBLE : View.GONE;
            deliveryVisibility = bar.hasDeliveryService() ? View.VISIBLE : View.GONE;
        }
        
        // Counter
        serviceCounter.setVisibility(counterVisibility);
        radioGroup_to_go.setVisibility((bar.hasCounterService() && bar.hasToStayAndToGo() && serviceCounter.isChecked()) ? View.VISIBLE : View.GONE);
        if (cart.mToGo) {
        	((RadioButton) tbnView.findViewById(R.id.togo)).setChecked(true);
        }
        
        // Table
        serviceTable.setVisibility(tableVisibility);
        tableNumberLine.setVisibility(View.GONE);
        if(cart.mService.equals("table")){
        	tableNumber.setText(cart.mTableNumber);        
        	tableNumberLine.setVisibility(View.VISIBLE);
        	tableNumber.requestFocus();
         	imm.showSoftInput(tableNumber, InputMethodManager.SHOW_IMPLICIT);
        }
        
        // Address delivery
        serviceDelivery.setVisibility(deliveryVisibility);
        addressDelivery.setVisibility(View.GONE);
        if(cart.mService.equals("delivery")){   
            deliveryName.setText(cart.mName);
            deliveryAddress1.setText(cart.mAddress1);
            deliveryAddress2.setText(cart.mAddress2);
            deliveryCity.setText(cart.mCity);
            deliveryPostcode.setText(cart.mPostcode);
            deliveryPhone.setText(cart.mPhone);
            deliveryInstructions.setText(cart.mDeliveryInstructions);
        	addressDelivery.setVisibility(View.VISIBLE);
        	if(bar.getDeliveryMinOrderAmount() > 0){
             	deliveryWarning.setText(String.format(getResources().getString(R.string.minimum_order_amount_delivery_x), bar.getDeliveryMinOrderAmountFormatted()));
             	deliveryWarning.setVisibility(View.VISIBLE);
            }
            else deliveryWarning.setVisibility(View.GONE);
        }       
         
        services.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                public void onCheckedChanged(RadioGroup group, int checkedId) {
                        switch (checkedId) {
                                case R.id.service_counter:
                                		cart.mService = "counter";         
                                        if(bar != null && bar.hasToStayAndToGo()){
                                        	radioGroup_to_go.setVisibility(View.VISIBLE);                              
                                        }
                                      	tableNumberLine.setVisibility(View.GONE);
                                      	addressDelivery.setVisibility(View.GONE);
                                      	imm.hideSoftInputFromWindow(tableNumber.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
                                        break;
                                case R.id.service_table:
                                        cart.mService = "table";
                                        radioGroup_to_go.setVisibility(View.GONE);
                                    	addressDelivery.setVisibility(View.GONE);
                                        tableNumberLine.setVisibility(View.VISIBLE);
                                        tableNumber.requestFocus();
                                     	imm.showSoftInput(tableNumber, InputMethodManager.SHOW_IMPLICIT);
                                        break;
                                case R.id.service_delivery:
                                		cart.mService = "delivery";
                                		radioGroup_to_go.setVisibility(View.GONE);
                                		tableNumberLine.setVisibility(View.GONE);
                                		addressDelivery.setVisibility(View.VISIBLE);
                                        deliveryName.setText(cart.mName);
                                        deliveryAddress1.setText(cart.mAddress1);
                                        deliveryAddress2.setText(cart.mAddress2);
                                        deliveryCity.setText(cart.mCity);
                                        deliveryPostcode.setText(cart.mPostcode);
                                        deliveryPhone.setText(cart.mPhone);
                                        deliveryInstructions.setText(cart.mDeliveryInstructions);
                                        
                                        if(bar.getDeliveryMinOrderAmount() > 0){
                                        	deliveryWarning.setText(String.format(getResources().getString(R.string.minimum_order_amount_delivery_x), bar.getDeliveryMinOrderAmountFormatted()));
                                        	deliveryWarning.setVisibility(View.VISIBLE);
                                        }
                                        else deliveryWarning.setVisibility(View.GONE);
                                        break;
                        }
                }
        });
        
        radioGroup_to_go.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {                           
                @Override
                public void onCheckedChanged(RadioGroup arg0, int arg1) {
                        cart.mToGo = ((RadioButton) tbnView.findViewById(R.id.togo)).isChecked();                                
                }
        });
        
        builder.setTitle(getString(R.string.service)).setView(tbnView);
      	final AlertDialog dialog = builder.create();
      	dialog.setCancelable(false);
      	dialog.getWindow().setSoftInputMode(LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        confirmDialog.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if(cart.mService.equals("table")){
					if(tableNumber.getText().toString().equals("")){
						tableNumber.requestFocus();
						imm.showSoftInput(tableNumber, InputMethodManager.SHOW_IMPLICIT);
					}			
					else {
						dialog.dismiss();
						imm.hideSoftInputFromWindow(tableNumber.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
						cart.mTableNumber = tableNumber.getText().toString(); 
						mItemsFragment.updateDeliveryView();	
						mItemsFragment.reload();
					}
				}
				else if(cart.mService.equals("delivery")){
					if(deliveryName.getText().toString().equals("")){
						deliveryName.requestFocus();
						imm.showSoftInput(deliveryName, InputMethodManager.SHOW_IMPLICIT);
						Toast.makeText(CartActivity.this, String.format(getResources().getString(R.string.x_is_required), getResources().getString(R.string.delivery_name)), Toast.LENGTH_SHORT).show();
					}
					else if(deliveryAddress1.getText().toString().equals("")) {
						deliveryAddress1.requestFocus();
						imm.showSoftInput(deliveryAddress1, InputMethodManager.SHOW_IMPLICIT);   
						Toast.makeText(CartActivity.this, String.format(getResources().getString(R.string.x_is_required), getResources().getString(R.string.delivery_address1)), Toast.LENGTH_SHORT).show();
        			}
					else if(deliveryCity.getText().toString().equals("")){
						deliveryCity.requestFocus();
						imm.showSoftInput(deliveryCity, InputMethodManager.SHOW_IMPLICIT);
						Toast.makeText(CartActivity.this, String.format(getResources().getString(R.string.x_is_required), getResources().getString(R.string.delivery_city)), Toast.LENGTH_SHORT).show();
					}
					else if(deliveryPostcode.getText().toString().equals("")){
						deliveryPostcode.requestFocus();
						imm.showSoftInput(deliveryPostcode, InputMethodManager.SHOW_IMPLICIT);
						Toast.makeText(CartActivity.this, String.format(getResources().getString(R.string.x_is_required), getResources().getString(R.string.delivery_postcode)), Toast.LENGTH_SHORT).show();
					}
					else if (deliveryPhone.getText().toString().equals("")){
						deliveryPhone.requestFocus();
						imm.showSoftInput(deliveryPhone, InputMethodManager.SHOW_IMPLICIT);
						Toast.makeText(CartActivity.this, String.format(getResources().getString(R.string.x_is_required), getResources().getString(R.string.delivery_phone)), Toast.LENGTH_SHORT).show();
					}
					else {
						dialog.dismiss();
						imm.hideSoftInputFromWindow(tableNumber.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
						cart.mName = deliveryName.getText().toString();
						cart.mAddress1 = deliveryAddress1.getText().toString();
						cart.mAddress2 = deliveryAddress2.getText().toString();
						cart.mCity = deliveryCity.getText().toString();
						cart.mPostcode = deliveryPostcode.getText().toString();
						cart.mPhone = deliveryPhone.getText().toString();
						cart.mDeliveryInstructions = deliveryInstructions.getText().toString();
						mItemsFragment.updateDeliveryView();	
						mItemsFragment.reload();
						 
					}
				} else if (cart.mService.equals("counter")) {
					dialog.dismiss();
					imm.hideSoftInputFromWindow(tableNumber.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
					mItemsFragment.updateDeliveryView();	
					mItemsFragment.reload();
				}
				else {
					Toast.makeText(CartActivity.this, R.string.error_select_service, Toast.LENGTH_SHORT).show();
				}
			}
		});
        
      	dialog.show();
	}
	

	
	public void selectService(View v){
		askForDelivery();
	}	
	
	public void reloadFragment(){
		this.mItemsFragment.reload();
	}
	
	public void selectTip(View v){
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		LayoutInflater inflater = getLayoutInflater();
		final View tipView = inflater.inflate(R.layout.tip, null);
		
        final TextView tipPercentage = (TextView) tipView.findViewById(R.id.tip_percentage);
        final SeekBar seekBar = (SeekBar) tipView.findViewById(R.id.seekBar);

        seekBar.setProgress((int) getCart().mTipPercent);
        seekBar.setMax(25);
        tipPercentage.setText(Integer.toString((int)getCart().mTipPercent) + " %");
        
        seekBar.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            	getCart().mTipPercent = (double) seekBar.getProgress();
            }
            
            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                    
            }
            
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress,  boolean fromUser) {
            	tipPercentage.setText(Integer.toString(progress) + " %");
            }
        });          
        
        builder.setTitle(getString(R.string.tip))
        	 .setView(tipView)
		     .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
  		         @Override
  		         public void onClick(DialogInterface dialog, int id) {
  		        	 reloadFragment();
  		         }
  		     });
      	AlertDialog dialog = builder.create();
      	dialog.show();
	}
	
	
	public void editLoyalty(View view) {
		AlertDialog.Builder alert = new AlertDialog.Builder(this);
		alert.setMessage(this.loyaltyProgramName);
		final EditText input = new EditText(this);
		if (getCart().mLoyaltyCode != null) {
			input.setText(decodeHTMLEntities(getCart().mLoyaltyCode));
		}
		alert.setView(input);
		alert.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
				getCart().mLoyaltyCode = encodeHTMLEntities(input.getText().toString());
				mItemsFragment.reload();
			}
		});
		alert.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
				// Canceled.
			}
		});
		AlertDialog alertDialog = alert.create();
		alertDialog.getWindow().setSoftInputMode(LayoutParams.SOFT_INPUT_STATE_VISIBLE);
		alertDialog.show();
	}
	
	private static String encodeHTMLEntities(String loyaltyCode) {
		String result = loyaltyCode;
		if(result != null) {
			result = result.replaceAll("æ", "&aelig;");
			result = result.replaceAll("ø", "&oslash;");
			result = result.replaceAll("å", "&aring;");
			result = result.replaceAll("Æ", "&AElig;");
			result = result.replaceAll("Ø", "&Oslash;");
			result = result.replaceAll("Å", "&Aring;");
		}
		return result;
	}
	
	private static String decodeHTMLEntities(String loyaltyCode) {
		String result = loyaltyCode;
		if(result != null) {
			result = result.replaceAll("&aelig;", "æ");
			result = result.replaceAll("&oslash;", "ø");
			result = result.replaceAll("&aring;", "å");
			result = result.replaceAll("&AElig;", "Æ");
			result = result.replaceAll("&Oslash;", "Ø");
			result = result.replaceAll("&Aring;", "Å");
		}
		return result;
	}
}
