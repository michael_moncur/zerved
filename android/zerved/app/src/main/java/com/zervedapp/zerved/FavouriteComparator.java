package com.zervedapp.zerved;

import java.util.Comparator;

import com.zervedapp.zerved.models.Favourite;

public class FavouriteComparator implements Comparator<Favourite> {

	@Override
	public int compare(Favourite arg0, Favourite arg1) {
		
		if(arg0.getBar() != null && arg1.getBar() != null)
			return arg0.getBar().getKey().compareTo(arg1.getBar().getKey());
			
		
		if(arg0.getLocation() != null && arg1.getLocation() != null)
			return arg0.getLocation().getKey().compareTo(arg1.getLocation().getKey());
		
		
		
		return -1;
	}

}
