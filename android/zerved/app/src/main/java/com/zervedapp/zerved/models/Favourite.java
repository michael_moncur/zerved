package com.zervedapp.zerved.models;

import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONTokener;

import com.zervedapp.zerved.models.OptionValue;

import android.util.Log;

public class Favourite extends JSONModel {
	private static final String TAG = Favourite.class.getName();
	
	private Bar mBar = null;
	private MerchantLocation mMerchantLocation = null;
	private Product mProduct = null;
	private ArrayList<OptionValue> mOptions = new ArrayList<OptionValue>();
	private boolean showSectionName = false;
	/**
	 * 
	 * Create favourite from JSON string
	 * 
	 * @param json
	 * @throws JSONException
	 */
	public Favourite(String json) throws JSONException {
		super(json);
		if (has("bar")) {
			mBar = new Bar(getString("bar"));
		}
		if (has("location")) {
			mMerchantLocation = new MerchantLocation(getString("location"));
		}
		if (has("product")) {
			mProduct = new Product(getString("product"));
		}
		if (has("options")) {
			try {
				JSONArray options = this.getJSONArray("options");
				for (int i = 0; i < options.length(); i++) {
					Log.i("OPTIONVALUE", options.getString(i));
					mOptions.add(new OptionValue(options.getString(i)));
				}
			}
			catch (JSONException e) {
	            Log.e(TAG, "Failed to parse JSON.", e);
	        }
		}
	}
		
	public Favourite(Product p, Bar b, MerchantLocation l){
		super();
		this.mProduct = p;
		this.mBar = b;
		this.mMerchantLocation = l;
	}
	
	/**
	 * Create list of favourite objects from JSON string
	 * 
	 * @param json
	 * @return ArrayList of Favourite
	 */
	public static ArrayList<Favourite> initList(String json) {
        ArrayList<Favourite> objectList = new ArrayList<Favourite>();
        try {
        	JSONArray objects = (JSONArray) new JSONTokener(json).nextValue();
            for (int i = 0; i < objects.length(); i++) {
            	Favourite object = new Favourite(objects.getString(i));
                objectList.add(object);
            }
            
        }
        catch (JSONException e) {
            Log.e(TAG, "Failed to parse JSON.", e);
        }
        
        return objectList;
    }
			
	public String toString() {
		return getStringProperty("id");
	}
	
	public ArrayList<OptionValue> getOptions() {
		return mOptions;
	}
	
	public void setOptions( ArrayList<OptionValue> options ) {
		this.mOptions = options;
	}
		
	public Bar getBar() {
		return mBar;
	}
		
	public MerchantLocation getLocation() {
		return mMerchantLocation;
	}
	
	public Product getProduct() {
		return mProduct;
	}	
	
	@Override 
	public boolean equals(Object o) {

	    if ( this == o ) return true;
	    if ( !(o instanceof Favourite) ) 
	    	return false;
	   
	    Favourite favourite = (Favourite)o;
	    if(!this.getProduct().getKey().equals(favourite.getProduct().getKey()))
	    	return false;
	    
	    if(this.getBar() != null && favourite.getBar() != null){
	    	if(!this.getBar().getKey().equals(favourite.getBar().getKey()))
	    		return false;
	    }
	    
	    if(this.getLocation() != null && favourite.getLocation() != null){
	    	if(!this.getLocation().getKey().equals(favourite.getLocation().getKey()))
	    		return false;
	    }
	    
	    if(this.getOptions().size() != favourite.getOptions().size())
	    	return false;
	    
	    boolean found = false;
	    for(OptionValue v1: this.getOptions()){
	    	found = false;
		    for(OptionValue v2: favourite.getOptions()){
		    	if(v2.equals(v1)){
		    		found = true;
		    		break;
		    	}
		    }
		    if(!found)
		    	return false;
	    }	    
	   
	    return true;
	}
	
	public void setSectionVisibility(boolean b){
		this.showSectionName = b;
	}
	
	public boolean sectionIsVisible(){
		return this.showSectionName;
	}
}
