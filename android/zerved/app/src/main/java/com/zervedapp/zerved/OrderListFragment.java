package com.zervedapp.zerved;

import java.util.ArrayList;

import com.zervedapp.zerved.loader.RESTLoader;
import com.zervedapp.zerved.loader.RESTLoader.RESTResponse;
import com.zervedapp.zerved.models.ConsumerManager;
import com.zervedapp.zerved.models.Order;
import com.zervedapp.zerved.models.ZervedContract;
import com.zervedapp.zerved.models.ZervedDbHelper;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.content.Loader;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class OrderListFragment extends RESTListFragment {
	private static final String TAG = OrderListFragment.class.getName();
	public static final String ARG_STATE = "com.zervedapp.zerved.STATE";
	protected static String URI =  Config.API_URL + "consumers/%s/orders/%s";
	private ArrayAdapter<Order> mAdapter;
	ConsumerManager mConsumer;
	String mState;
	ZervedDbHelper mDbHelper;
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		
        mAdapter = new OrderAdapter(getActivity(), R.layout.item_order);
        this.setListAdapter(mAdapter);
        
        Bundle fragmentArgs = this.getArguments();
        mState = fragmentArgs.getString(ARG_STATE);
        
        mDbHelper = new ZervedDbHelper(getActivity());
        
        reload();
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = super.onCreateView(inflater, container, savedInstanceState);
		TextView emptyMsg = (TextView) view.findViewById(R.id.empty_message);
		emptyMsg.setText(R.string.no_orders);
		return view;
	}
	
	public void reload(ConsumerManager consumer) {
		mConsumer = consumer;
		reload();
	}
	
	public void reload() {
		if (mConsumer == null) {
			mConsumer = ((ZervedApp) getActivity().getApplication()).getConsumer();
		}
		
		loadFromCache();
        Log.i(TAG, "Orders loaded from db");
		
		if (mConsumer != null && isAdded() && mConsumer.mConsumerKey != null) {
			super.reload();
		}
	}
	
	@Override
	public String getUri() {
		return String.format(URI, mConsumer.mConsumerKey, mState);
	}
	
	@Override
	public String getAuthorization() {
		return mConsumer.mAccessToken;
	}
	
	@Override
	public Loader<RESTResponse> onCreateLoader(int id, Bundle args) {
        if (args != null && args.containsKey(ARGS_URI) && args.containsKey(ARGS_PARAMS)) {
            Uri    action = args.getParcelable(ARGS_URI);
            Bundle params = args.getParcelable(ARGS_PARAMS);
            
            return new RESTLoader(getActivity(), getVerb(), action, params, getAuthorization());
        }
        return null;
	}

	@Override
	public void onJsonLoaded(String json) {
    	ArrayList<Order> orders = Order.initList(json);
        
        mAdapter.clear();
        for (Order order : orders) {
        	mAdapter.add(order);
        }
        mAdapter.notifyDataSetChanged();
        
        // Update local database
        updateCache(orders);
        Log.i(TAG, "Orders saved");
	}
	
	@Override
    public void onListItemClick(ListView l, View v, int position, long id) {
		if (mAdapter.isEnabled(position)) {
			Intent intent = new Intent(getActivity(), OrderActivity.class);
			Order order = (Order) mAdapter.getItem(position);
			intent.putExtra(OrderActivity.EXTRA_ORDER_KEY, order.getKey());
			startActivity(intent);
		}
    }
	
	public void loadFromCache() {
		// Hide progress spinner
		getView().findViewById(R.id.empty_message).setVisibility(View.VISIBLE);
        getView().findViewById(R.id.progress).setVisibility(View.GONE);
        getView().findViewById(R.id.progress_text).setVisibility(View.GONE);
        getListView().setEmptyView(getView().findViewById(R.id.empty_message));
        
        // Fetch orders filtered by status
		SQLiteDatabase db = mDbHelper.getReadableDatabase();
		String[] projection = {
				ZervedContract.Order.COLUMN_NAME_KEY,
				ZervedContract.Order.COLUMN_NAME_STATUS,
				ZervedContract.Order.COLUMN_NAME_DATE_CREATED,
				ZervedContract.Order.COLUMN_NAME_JSON
		};
		String selection;
		String[] selectionArgs;
		if (mState.equals("open")) {
        	selection = ZervedContract.Order.COLUMN_NAME_STATUS+" IN (?, ?)";
        	selectionArgs = new String[2];
        	selectionArgs[0] = "pending";
        	selectionArgs[1] = "preparing";
        } else {
        	selection = ZervedContract.Order.COLUMN_NAME_STATUS+" = ?";
        	selectionArgs = new String[1];
        	selectionArgs[0] = "complete";
        }
		String sortOrder = ZervedContract.Order.COLUMN_NAME_DATE_CREATED + " DESC";
		Cursor c = db.query(ZervedContract.Order.TABLE_NAME, projection, selection, selectionArgs, null, null, sortOrder);
		ArrayList<Order> orders = Order.initList(c);
		
		// Show orders in list
        mAdapter.clear();
        for (Order order : orders) {
        	mAdapter.add(order);
        }
        mAdapter.notifyDataSetChanged();
	}
	
	public void updateCache(ArrayList<Order> orders) {
		// Delete orders that are not in the loaded orders
		
		// Collect keys of loaded orders
		String orderKeys = "";
		for (int i=0; i<orders.size(); i++) {
			Order order = orders.get(i);
			orderKeys = orderKeys + "'"+order.getKey()+"'";
			if (i < orders.size() - 1) {
				orderKeys = orderKeys + ",";
			}
		}
		
		// Delete orders in the current status that was not in the loaded list (because they must have changed status)
		SQLiteDatabase db = mDbHelper.getWritableDatabase();
		String selection;
		String[] selectionArgs;
		if (mState.equals("open")) {
        	selection = ZervedContract.Order.COLUMN_NAME_STATUS+" IN (?, ?) AND " + ZervedContract.Order.COLUMN_NAME_KEY + " NOT IN ("+orderKeys+")";
        	selectionArgs = new String[2];
        	selectionArgs[0] = "pending";
        	selectionArgs[1] = "preparing";
        } else {
        	selection = ZervedContract.Order.COLUMN_NAME_STATUS+" IN (?, ?) AND " + ZervedContract.Order.COLUMN_NAME_KEY + " NOT IN ("+orderKeys+")";
        	selectionArgs = new String[1];
        	selectionArgs[0] = "complete";
        }
		db.delete(ZervedContract.Order.TABLE_NAME, selection, selectionArgs);
		
		// Save loaded orders
        for (Order order: orders) {
        	order.saveToCache(db);
        }
	}
}
