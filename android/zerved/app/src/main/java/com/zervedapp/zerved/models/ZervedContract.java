package com.zervedapp.zerved.models;

import android.provider.BaseColumns;

public final class ZervedContract {
	public ZervedContract() {}
	
	public static abstract class Order implements BaseColumns {
		public static final String TABLE_NAME = "'order'";
		public static final String COLUMN_NAME_KEY = "key";
		public static final String COLUMN_NAME_STATUS = "status";
		public static final String COLUMN_NAME_DATE_CREATED = "date_created";
		public static final String COLUMN_NAME_JSON = "json";
		public static final String COLUMN_NAME_PROPERTY_SET = "property_set";
	}
}
