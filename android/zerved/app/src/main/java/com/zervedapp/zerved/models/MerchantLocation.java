package com.zervedapp.zerved.models;

import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONTokener;
import com.zervedapp.zerved.R;
import android.content.Context;
import android.location.Location;
import android.util.Log;

public class MerchantLocation extends JSONModel {
	private static final String TAG = MerchantLocation.class.getName();
	private ArrayList<Bar> mBars = new ArrayList<Bar>();
	private ArrayList<Event> mEvents = new ArrayList<Event>();
	private ArrayList<Category> mEntranceCategories = new ArrayList<Category>();
	private ArrayList<String> mCoverURLs;
	
	public MerchantLocation(String json) throws JSONException {
		super(json);
		if (has("bars")) 
			mBars = Bar.initList(getString("bars"));	
		
		if(has("upcoming_events")) 
			mEvents = Event.initList(getString("upcoming_events"));
		
		if(has("entrance_categories"))
			mEntranceCategories = Category.initList(getString("entrance_categories"));
			
		setCoverURLs();
	}
	
	public static ArrayList<MerchantLocation> initList(String json) {
        ArrayList<MerchantLocation> objectList = new ArrayList<MerchantLocation>();
        
        try {
        	JSONArray objects = (JSONArray) new JSONTokener(json).nextValue();
            for (int i = 0; i < objects.length(); i++) {
            	MerchantLocation object = new MerchantLocation(objects.getString(i));
                objectList.add(object);
            }
            
        }
        catch (JSONException e) {
            Log.e(TAG, "Failed to parse JSON.", e);
        }
        
        return objectList;
	}
	
	public String getName() {
		return this.getStringProperty("name");
	}
	
	public float getDistance(Location userLoc) {
		return this.getLocation().distanceTo(userLoc);
	}
	
	public Location getLocation() {
		Location loc = new Location("");
		try {
			loc.setLatitude(this.getDouble("latitude"));
			loc.setLongitude(this.getDouble("longitude"));
		} catch (JSONException e) {
			
		}
		return loc;
	}
	
	public String getCountry() {
		return this.getStringProperty("country");
	}
	
	public String getImageUrl() {
		try {
			JSONModel merchant = new JSONModel(getString("merchant"));
			if (merchant.has("image_url") && !merchant.getStringProperty("image_url").equals("")) {
				return merchant.getStringProperty("image_url");
			}
		} catch (JSONException e) {
			
		}
		return null;
	}
	
	public boolean hasCovers(){
		return numberOfCovers() > 0;
	}
	
	public int numberOfCovers(){
		return getCoverURLs().size();
	}
	
	public ArrayList<String> getCoverURLs(){
		return mCoverURLs;
	}
	
	public String getCoverURL(int p){
		return (p>=0 && p<mCoverURLs.size()) ? mCoverURLs.get(p) : "";
	}
	
	public void setCoverURLs(){
		mCoverURLs = new ArrayList<String>(); 
		JSONArray objects;
		try {
			objects = getJSONArray("cover_urls");		
	        for (int i = 0; i < objects.length(); i++) {
	        	String object = objects.getString(i);
	        	mCoverURLs.add(object);
	        }
		}
	    catch (JSONException e) {
//			e.printStackTrace();
		}
	}

	// Bars
	public ArrayList<Bar> getBars() {
		return mBars;
	}	
	
	public boolean hasBars() {
		return getNumberOfBars()>0;
	}
	
	public boolean hasOpenBars(){
		for (Bar b : mBars){
			if(b.isOpen())
				return true;
		}
		return false;
	}
	public int getNumberOfBars(){
		return this.mBars.size();
	}
	
	// Events
	public ArrayList<Event> getEvents() {
		return mEvents;
	}
	
	
	// Events
	public ArrayList<Event> getEvents(int max) {
		ArrayList<Event> events = new ArrayList<Event>();
		for(int i = 0; i < Math.min(mEvents.size(), max); i ++){
			events.add(mEvents.get(i));
		}
		return events;
	}
	
	
	public boolean hasEvents() {
		return getNumberOfEvents()>0;
	}	
	
	public int getNumberOfEvents(){
		return this.mEvents.size();
	}
	
	// Entrance categories
	public ArrayList<Category> getEntranceCategories() {
		return mEntranceCategories;
	}
	
	public boolean hasEntranceCategories() {
		return getNumberOfEntranceCategories() > 0;
	}
	
	public int getNumberOfEntranceCategories(){
		return this.mEntranceCategories.size();
	}
		
	public String toString() {
		return this.getName();
	}
	
	
	public boolean isComplete() {
		// This model may have only some of the data loaded. One of attributes in the full data version is has_entrance_categories
		return has("has_entrance_categories");
	}
	
	public boolean infoPanelEnabled() {
	    return getBooleanProperty("info_panel_enabled");
	}
		
	public static String formatDistance(Context context, float meters) {
		int rounddist = (int)meters;
		String lang = context.getResources().getConfiguration().locale.getLanguage();
		if (lang.equals("en")) {
			// Miles
			int yards = (int)Math.round(meters/0.9144);
			int miles = (int)Math.round(meters/1609.344);
			// <30 yards is here
			if (yards < 30) {
				return context.getResources().getString(R.string.here);
			}
			// <1000 yards round to closest 50 yards
			else if (yards < 1000) {
				rounddist = (yards + 50) / 50 * 50;
				return String.format(context.getResources().getString(R.string.x_yards), rounddist);
			}
			// <1750 yards round to closest 100 yards
			else if (yards < 1750) {
				rounddist = (yards + 100) / 100 * 100;
				return String.format(context.getResources().getString(R.string.x_yards), rounddist);
			}
			// >1750 yards round to closest mile
			else {
				return context.getResources().getQuantityString(R.plurals.miles, miles, miles);
			}
		} else {
			// Meters
			// <30 meters is here
			if (meters < 30) {
				return context.getResources().getString(R.string.here);
			}
			// <1km round to closest 50m
			else if (meters < 1000) {
				rounddist = (rounddist + 50) / 50 * 50;
				return String.format(context.getResources().getString(R.string.x_meters), rounddist);
			// <2km round to closest 100m
			} else if (meters < 2000) {
				rounddist = (rounddist + 100) / 100 * 100;
				return String.format(context.getResources().getString(R.string.x_meters), rounddist);
			// >2km round to closest km
			} else {
				rounddist = rounddist/1000;
				return String.format(context.getResources().getString(R.string.x_kilometres), rounddist);
			}
		}
	}
}
