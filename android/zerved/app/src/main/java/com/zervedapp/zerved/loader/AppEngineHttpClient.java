package com.zervedapp.zerved.loader;

import android.content.Context;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.SingleClientConnManager;

import java.security.KeyStore;

public class AppEngineHttpClient extends DefaultHttpClient {

	  final Context context;

	  public AppEngineHttpClient(Context context) {
	    this.context = context;
	  }

	  @Override protected ClientConnectionManager createClientConnectionManager() {
	    SchemeRegistry registry = new SchemeRegistry();
	    registry.register(
	        new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
	    //registry.register(new Scheme("https", newSslSocketFactory(), 443));
	    registry.register(new Scheme("https", SSLSocketFactory.getSocketFactory(), 443));
	    return new SingleClientConnManager(getParams(), registry);
	  }

	  /*
	  private SSLSocketFactory newSslSocketFactory() {
	    try {
	      KeyStore trusted = KeyStore.getInstance(KeyStore.getDefaultType());
	      trusted.load(null, null);
	      SSLSocketFactory sf = new TrustAllSSLSocketFactory(trusted);
	      sf.setHostnameVerifier(SSLSocketFactory.STRICT_HOSTNAME_VERIFIER);
	      return sf;
	    } catch (Exception e) {
	      throw new AssertionError(e);
	    }
	  }
	  */
}
