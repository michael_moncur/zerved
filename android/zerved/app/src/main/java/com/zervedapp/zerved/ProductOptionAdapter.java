package com.zervedapp.zerved;

import java.util.ArrayList;

import com.zervedapp.zerved.models.OptionValue;
import com.zervedapp.zerved.models.ProductOption;
import com.zervedapp.zerved.models.ProductOption.Value;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;


public class ProductOptionAdapter extends ArrayAdapter<ProductOption>{
	@SuppressWarnings("unused")
	private static final String TAG = ProductOptionAdapter.class.getName();
	Context mContext;
	ArrayList<OptionValue> mSelectedValues;
	 
	public ProductOptionAdapter(Context context, int textViewResourceId, ArrayList<OptionValue> selectedValues) {
		super(context, textViewResourceId);
		mContext = context;
		mSelectedValues = selectedValues;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View row;
		LayoutInflater inflater = LayoutInflater.from(getContext());
		if (null == convertView) {
			row = inflater.inflate(R.layout.option_list_item, null);
		} else {
			row = convertView;
		}
		
		final ProductOption productOption = this.getItem(position);
		
		TextView nameTextView = (TextView) row.findViewById(R.id.option_name);
		TextView valueTextView = (TextView) row.findViewById(R.id.option_value);
		
		nameTextView.setText(productOption.getName());
		String valueText = "";	
		
		// Filter selected values for this option
		ArrayList<OptionValue> selectedValuesForOption = new ArrayList<OptionValue>();
		for(OptionValue value: mSelectedValues){
			if(value.getId().equals(productOption.getId())){
				selectedValuesForOption.add(value);
			}
		}
		
		if (productOption.getSelectionType().equals("single")) {
			// Single value
			if (selectedValuesForOption.size() > 0) {
				OptionValue value = selectedValuesForOption.get(0);
				valueText = ProductOption.getValuesLabels(productOption.getValues())[value.getValue()];
			} else {
				// Nothing selected
				valueText = getContext().getResources().getString(R.string.choose);
			}
		} else {
			// Multiple values
			for(Value productOptionValue: productOption.getValues()) {
				// If selected, add to string
				for (OptionValue selectedValue: selectedValuesForOption) {
					if(productOptionValue.getIndex() == selectedValue.getValue()){
						valueText += productOptionValue + ", ";
					}
				}
			}
			// Remove last comma
			if (valueText.length() > 2) {
				valueText = valueText.substring(0, valueText.length()-2);
			}
			if (selectedValuesForOption.size() == 0) {
				// Nothing selected
				valueText = getContext().getResources().getString(R.string.choose);
			}
		}
		valueTextView.setText(valueText);
		
		row.setTag(productOption);
		
		return row;
	}
}
