package com.zervedapp.zerved.models;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONTokener;

import android.util.Log;

public class MenuCard extends JSONModel {
	private static final String TAG = MenuCard.class.getName();
	
	public MenuCard(String json) throws JSONException {
		super(json);
	}
	
	public static ArrayList<MenuCard> initList(String json) {
        ArrayList<MenuCard> objectList = new ArrayList<MenuCard>();
        
        try {
        	JSONArray objects = (JSONArray) new JSONTokener(json).nextValue();
            for (int i = 0; i < objects.length(); i++) {
            	MenuCard object = new MenuCard(objects.getString(i));
            	objectList.add(object);
            }
            
        }
        catch (JSONException e) {
            Log.e(TAG, "Failed to parse JSON.", e);
        }
        
        return objectList;
	}
	
	public String getName() {
		return getStringProperty("name");
	}
	
	public String toString() {
		return this.getName();
	}
	public ArrayList<Category> getCategories() {
		JSONArray objects;
		ArrayList<Category> categories = new ArrayList<Category>();
		try {
			objects = getJSONArray("categories");
			for (int i = 0; i < objects.length(); i++) {
				categories.add(new Category(objects.getString(i)));
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
        return categories;
	}
	
	@Override
	public boolean equals(Object o){
		if(!(o instanceof MenuCard))
			return false;
		
		MenuCard c = (MenuCard)o;
		return c.getKey().equals(this.getKey());
	}
}
