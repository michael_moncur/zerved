package com.zervedapp.zerved;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;
import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;
import com.viewpagerindicator.CirclePageIndicator;
import com.zervedapp.zerved.loader.RESTLoader;
import com.zervedapp.zerved.models.Bar;
import com.zervedapp.zerved.models.CartManager;
import com.zervedapp.zerved.models.Event;
import com.zervedapp.zerved.models.MerchantLocation;
import com.zervedapp.zerved.models.Category;
import com.zervedapp.zerved.models.MenuCard;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.app.LoaderManager.LoaderCallbacks;
import android.content.Loader;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class LocationActivity extends Activity implements LoaderCallbacks<RESTLoader.RESTResponse>, CategoryListFragment.LocationBarProvider {
	private static final String TAG = LocationActivity.class.getName();

    ViewPager mViewPager;
    MerchantLocation mLocation = null;
    CartManager mCartManager;
    boolean mIsLocationLoading = false;
    LocationManager mLocationManager;
    LocationListener mLocationListener;
    SlidingMenu mMenu;
    boolean mShowAllBars;
		
    CategoryListFragment categoryListFragment;
    //BarListFragment barListFragment;
    EventListFragment eventListFragment;
    
    LinearLayout layout_bars;
    LinearLayout layout_entrances;
    LinearLayout layout_events;
    ViewPager viewPager;
    PagerAdapter adapter;
    int viewPagerHeight;
    
    
    private static final int LOADER_MERCHANT = 0x1;
	private static final String ARGS_URI    = "com.zervedapp.zerved.ARGS_URI";
    private static final String ARGS_PARAMS = "com.zervedapp.zerved.ARGS_PARAMS";
    private static String URI = Config.API_URL+"locations/";
    private static String URI_ALL_BARS = Config.API_URL+"locations/withallbars/";
    @SuppressWarnings("unused")
	private static final int LOCATION_ACCURACY_THRESHOLD = 200; // Search until location found within this distance (meters)
    public static final String EXTRA_LOCATION = "com.zervedapp.zerved.LOCATION";
    public static final String EXTRA_GROUP = "com.zervedapp.zerved.GROUP";
    public static final String EXTRA_CATEGORIES = "com.zervedapp.zerved.CATEGORIES";
    public static final String EXTRA_SELECTED_INDEX = "com.zervedapp.zerved.SELECTED_INDEX";
    
    public static final String EXTRA_SHOW_ALL_BARS = "com.zervedapp.zerved.SHOW_ALL_BARS";
    
     
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        getIntent().setAction("location_activity");
        
        mMenu = SlidingMenuHelper.initSlidingMenu(this, "location");
        mMenu.setSlidingEnabled(false);
                
		// Load merchant selected in merchant list
        mLocation = ((ZervedApp) getApplication()).mLocation;
        mShowAllBars = getIntent().getBooleanExtra(EXTRA_SHOW_ALL_BARS, false);
        
        setContentView(R.layout.location);
       
        final ActionBar actionBar = getActionBar();
        actionBar.setTitle(mLocation.getName());
        
        // Magic: Reconnect with existing loaders after configuration changes (e.g. screen orientation)
        if (getLoaderManager().getLoader(LOADER_MERCHANT) != null) {
        	getLoaderManager().initLoader(LOADER_MERCHANT, null, this);
        }

        reload();
    }

    @Override
    public void onResume(){
    	
    	((ZervedApp) getApplication()).mEvent = null;
    	String action = getIntent().getAction();
        if(action == null || !action.equals("location_activity")) {
            Intent intent = new Intent(this, LocationActivity.class);
            startActivity(intent);
            finish();
        }
        else getIntent().setAction(null);
    	
    	super.onResume();
    }
    
    public void reload() {
        if (mLocation != null) {
        	if (mLocation.isComplete()) {
        		((ZervedApp) getApplication()).mEvent = null;
        		LayoutInflater inflater = LayoutInflater.from(this);
        		View view = inflater.inflate(R.layout.location, null);       
        		setContentView(view);
        		
        		// Image and map
                viewPager = (ViewPager) findViewById(R.id.viewpager);   
                viewPagerHeight = (int) (0.5625 * (float)getWindowManager().getDefaultDisplay().getWidth());
                adapter = new LocationPagerAdapter(getFragmentManager(), this);  
                viewPager.setAdapter(adapter);
                viewPager.getLayoutParams().height = viewPagerHeight;
                
                Button moreEvents = (Button) findViewById(R.id.button_more_events);
                
                if(mLocation.hasCovers()) {
                	//Bind the circles indicator to the view pager
                	CirclePageIndicator mIndicator = (CirclePageIndicator)findViewById(R.id.titles);
                	mIndicator.setViewPager(viewPager);
                }    
                
                // create our list and custom adapter  
                final SeparatedListAdapter adapter = new SeparatedListAdapter(this);    
                if(mLocation.hasEntranceCategories())
                	adapter.addSection(getResources().getString(R.string.entrance),  new ArrayAdapter<Category>(this,R.layout.item_label_list, mLocation.getEntranceCategories()));
             
                if(mLocation.hasEvents() && mLocation.hasOpenBars()) {
                	
                		adapter.addSection(getResources().getString(R.string.what_would_you_like_to_do), new LocationEventAdapter(this, R.layout.item_location_choice, new String[] {getResources().getString(R.string.order_for_now), getResources().getString(R.string.preorder_for_later)} ));
            	    
                		/*	
                	 	EventAdapter mAdapter = new EventAdapter(this, R.layout.event_list_item);
            	        for (Event event: mLocation.getEvents(2)) {
            	        	mAdapter.add(event);
            	        }
            			adapter.addSection(getResources().getString(R.string.events), mAdapter);
            			moreEvents.setVisibility((mLocation.getNumberOfEvents() > 2) ? View.VISIBLE : View.GONE);
            			*/
                }
                else {
                	if(mLocation.hasBars() && !mLocation.hasEvents()){
                		if(mLocation.getNumberOfBars() == 1) {
                			Bar bar = mLocation.getBars().get(0);
                			if(bar.getMenus().size() == 1){
                				MenuCard menu = bar.getMenus().get(0);
                				adapter.addSection(getResources().getString(R.string.menu),  new ArrayAdapter<Category>(this,R.layout.item_label_list, menu.getCategories()));
                			}
                			else {
                				adapter.addSection(getResources().getString(R.string.menu),  new ArrayAdapter<MenuCard>(this,R.layout.item_label_list, bar.getMenus()));
                			}
                		}                			
                		else adapter.addSection(getResources().getString(R.string.menu),  new ArrayAdapter<Bar>(this,R.layout.item_label_list, mLocation.getBars()));  
                	}
                	if(mLocation.hasEvents()){
            	              			
            	        EventAdapter mAdapter = new EventAdapter(this, R.layout.event_list_item);
            	        for (Event event: mLocation.getEvents(2)) {
            	        	mAdapter.add(event);
            	        }
            			adapter.addSection(getResources().getString(R.string.preorder_for_event), mAdapter);
            			moreEvents.setVisibility((mLocation.getNumberOfEvents() > 2) ? View.VISIBLE : View.GONE);
                	}
                }                
                
                final ListView list = (ListView)  view.findViewById(R.id.location_listview);   
                list.setAdapter(adapter);  
                list.setOnItemClickListener(new OnItemClickListener() {
					@Override
					public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
						Object o = adapter.getItem(position);
						
						// Category						
						if(o instanceof Category){
							
							Category category = (Category)o;
							boolean entrance = mLocation.getEntranceCategories().contains(o);						
							
					    	Intent intent = new Intent(getApplicationContext(), ProductsActivity.class);
					    	intent.putExtra(EXTRA_LOCATION, mLocation.toJson());
					    	if(entrance){
					    		intent.putExtra(EXTRA_GROUP, "entrance");
					    	}
					    	else {
					    		intent.putExtra(EXTRA_GROUP, "menu");
					    		intent.putExtra(CategoryListFragment.EXTRA_BAR, mLocation.getBars().get(0).toJson());
					    		intent.putExtra(CategoryListFragment.EXTRA_MENU, mLocation.getBars().get(0).getMenus().get(0).toJson() );
					    	}
					    	intent.putExtra(EXTRA_CATEGORIES, "[" + category.toJson().toString() + "]");
					    	intent.putExtra(EXTRA_SELECTED_INDEX, position);
					        startActivity(intent);
						}					
						
						// Bar
						else if(o instanceof Bar){	
				    		Bar bar = (Bar)o;
				    		int nb_menus = bar.getMenus().size();
				    		// If only one menu the bar, go directly to the categories
				    		if(nb_menus == 1) {
				    	    	Intent intent = new Intent(getApplicationContext(), CategoriesActivity.class);
				    	    	intent.putExtra(CategoryListFragment.EXTRA_LOCATION, mLocation.toJson());
				    	    	intent.putExtra(CategoryListFragment.EXTRA_BAR, bar.toJson());
				    	    	intent.putExtra(CategoryListFragment.EXTRA_GROUP, "menu");
				    	    	intent.putExtra(CategoryListFragment.EXTRA_SELECTED_INDEX, position);
				    	    	intent.putExtra(CategoryListFragment.EXTRA_MENU, bar.getMenus().get(0).toJson());
				    	        startActivity(intent);
				    		}
				    		// Otherwise show the menus list
				    		else if (nb_menus > 1) {
					        	Intent intent = new Intent(getApplicationContext(), MenuCardsActivity.class);
					        	intent.putExtra(MenuCardListFragment.EXTRA_BAR, bar.toJson());
					        	intent.putExtra(MenuCardListFragment.EXTRA_LOCATION, mLocation.toJson());
					            startActivity(intent);
				    		}							
						}
						
						// Event
						else if(o instanceof Event) {
					    	Event e = (Event)o;
					    	((ZervedApp) getApplication()).mEvent = e ;
					    	Intent intent = new Intent(getApplicationContext(), EventActivity.class);
					    	intent.putExtra(EventActivity.EXTRA_EVENT, e.toJson() );
					        startActivity(intent);    	 
						}
						
						// Menu
						else if(o instanceof MenuCard) {
							MenuCard menu = (MenuCard)o;
			    	    	Intent intent = new Intent(getApplicationContext(), CategoriesActivity.class);
			    	    	intent.putExtra(CategoryListFragment.EXTRA_LOCATION, mLocation.toJson());
			    	    	intent.putExtra(CategoryListFragment.EXTRA_BAR, mLocation.getBars().get(0).toJson());
			    	    	intent.putExtra(CategoryListFragment.EXTRA_GROUP, "menu");
			    	    	intent.putExtra(CategoryListFragment.EXTRA_SELECTED_INDEX, position);
			    	    	intent.putExtra(CategoryListFragment.EXTRA_MENU, menu.toJson());
			    	        startActivity(intent);
						}
						
						// Order for Now / Order for Events
						else if(o instanceof String){
							String s = (String)o;
							//Order for NOW
							if(s.equals(getResources().getString(R.string.order_for_now))){
								((ZervedApp) getApplication()).mEvent = null;
								if(mLocation.getNumberOfBars() == 1){
						    		Bar bar = mLocation.getBars().get(0);
						    		int nb_menus = bar.getMenus().size();
						    		// If only one menu the bar, go directly to the categories
						    		if(nb_menus == 1) {
						    			
						    	    	Intent intent = new Intent(getApplicationContext(), CategoriesActivity.class);
						    	    	intent.putExtra(CategoryListFragment.EXTRA_LOCATION, mLocation.toJson());
						    	    	intent.putExtra(CategoryListFragment.EXTRA_BAR, bar.toJson());
						    	    	intent.putExtra(CategoryListFragment.EXTRA_GROUP, "menu");
						    	    	intent.putExtra(CategoryListFragment.ARG_GROUP, "menu");
						    	    	intent.putExtra(CategoryListFragment.EXTRA_SELECTED_INDEX, position);
						    	    	intent.putExtra(CategoryListFragment.EXTRA_MENU, bar.getMenus().get(0).toJson());
						    	    	intent.putExtra(CategoryListFragment.ARG_MENU, bar.getMenus().get(0).toJson());							 
						    	    	intent.putExtra(CategoryListFragment.EXTRA_CATEGORIES, bar.getMenus().get(0).getCategories().toString());						    	    	
						    	        startActivity(intent);
						    	        					    	        
						    	    
						    	        
						    		}
						    		// Otherwise show the menus list
						    		else if (nb_menus > 1) {
							        	Intent intent = new Intent(getApplicationContext(), MenuCardsActivity.class);
							        	intent.putExtra(MenuCardListFragment.EXTRA_BAR, bar.toJson());
							        	intent.putExtra(MenuCardListFragment.EXTRA_LOCATION, mLocation.toJson());
							            startActivity(intent);
						    		}	
								}
								else {
						        	Intent intent = new Intent(getApplicationContext(), BarsActivity.class);
						        	intent.putExtra(BarListFragment.EXTRA_LOCATION, mLocation.toJson());
						            startActivity(intent);
								}
							}
							//Order for EVENT
							else if(s.equals(getResources().getString(R.string.preorder_for_later))){
								Intent intent = new Intent(getApplicationContext(), EventsActivity.class);
								intent.putExtra(EventListFragment.EXTRA_LOCATION, mLocation.toJson());
								startActivity(intent);
							}
							
						}					

					}
				});
               // Utility.setListViewHeightBasedOnChildren(list);

                list.postDelayed(new Runnable() {
                	public void run() {
                		Utility.setListViewHeightBasedOnChildren(list);
                	}
                }, 400);

                
                moreEvents.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						Intent intent = new Intent(getApplicationContext(), EventsActivity.class);
						intent.putExtra(EventListFragment.EXTRA_LOCATION, mLocation.toJson());
						startActivity(intent);						
					}
				});
                
                
        	} else {
        		reloadLoc();
        	}
        }
    }
        
    public void reloadLoc(){
		// Load complete location data
		String uri = URI;
		if (mShowAllBars) {
			uri = URI_ALL_BARS;
		}
    	Bundle args = new Bundle();
    	args.putParcelable(ARGS_URI, Uri.parse(uri+mLocation.getKey()));
    	args.putParcelable(ARGS_PARAMS, new Bundle());
    	getLoaderManager().restartLoader(LOADER_MERCHANT, args, LocationActivity.this);   	
    }
    
    @Override
    public Loader<RESTLoader.RESTResponse> onCreateLoader(int id, Bundle args) {
        if (args != null && args.containsKey(ARGS_URI) && args.containsKey(ARGS_PARAMS)) {
            Uri    action = args.getParcelable(ARGS_URI);
            Bundle params = args.getParcelable(ARGS_PARAMS);
            
            return new RESTLoader(this, RESTLoader.HTTPVerb.GET, action, params);
        }
        
        return null;
    }

    @Override
    public void onLoadFinished(Loader<RESTLoader.RESTResponse> loader, RESTLoader.RESTResponse data) {
        int    code = data.getCode();
        String json = data.getData();
        
        // Check to see if we got an HTTP 200 code and have some data.
        if (code == 200 && !json.equals("")) {
        	try {
        		JSONObject object = (JSONObject) new JSONTokener(json).nextValue();
        		if (object.has("key")) {
        			this.mLocation = new MerchantLocation(json);
        			((ZervedApp) getApplication()).mLocation = mLocation;
        			reload();
        		} else {
        			
        		}
        	} catch (JSONException e) {
        		Log.e(TAG, "Failed to parse JSON.", e);
        	}
        }
        else {
            Toast.makeText(this, "Failed to load data.", Toast.LENGTH_SHORT).show();
        }
    }
    
    @Override
    public void onLoaderReset(Loader<RESTLoader.RESTResponse> loader) {
    }
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
    	switch (item.getItemId()) {
    	case android.R.id.home:
			mMenu.toggle();
			return true;
    	}
    	return super.onOptionsItemSelected(item);
    }
    
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
	    if ( keyCode == KeyEvent.KEYCODE_MENU ) {
	    	mMenu.toggle();
	        return true;
	    }
	    return super.onKeyDown(keyCode, event);
	}
    
    public MerchantLocation getLocation() {
    	return mLocation;
    }
    
    public Bar getBar() {
    	return null;
    }
    
    public boolean isLocationLoading() {
    	return mIsLocationLoading;
    }

    public static class EmptyMenuFragment extends Fragment {
	    @Override
	    public View onCreateView(LayoutInflater inflater, ViewGroup container,
	                             Bundle savedInstanceState) {
	        View view = inflater.inflate(R.layout.progress_list, container, false);
	        view.findViewById(R.id.progress).setVisibility(View.GONE);
	        view.findViewById(R.id.progress_text).setVisibility(View.GONE);
	        view.findViewById(R.id.empty_message).setVisibility(View.VISIBLE);
	        ((TextView)view.findViewById(R.id.empty_message)).setText(R.string.no_menu);
	        return view;
	    }
    }    

}
