package com.zervedapp.zerved;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;

import com.commonsware.cwac.wakeful.WakefulIntentService;
import com.zervedapp.zerved.loader.AppEngineHttpClient;
import com.zervedapp.zerved.models.Notifier;
import com.zervedapp.zerved.models.OrderNotification;

public class NotificationService extends WakefulIntentService {
	private static final String TAG = NotificationService.class.getName();
	
	public NotificationService() {
		super("NotificationService");
	}
	
	@Override
	protected void doWakefulWork(Intent intent) {
		// Consumer credentials
		SharedPreferences settings = getSharedPreferences("ZervedSettings", 0);
	    String consumerKey = settings.getString(Config.SERVER_URL+"_consumerKey", "");
	    String password = settings.getString(Config.SERVER_URL+"_password", "");
	    
    	try {
    		//Log.i(TAG, "Pulling notifications");
    		// Setup request
    		HttpPost request = new HttpPost(Config.API_URL+"consumers/"+consumerKey+"/pullnotifications");
    		request.setHeader("Content-Type", "application/json");
    		JSONObject userParams = new JSONObject();
    		userParams.put("password", password);
    		HttpEntity formEntity = new StringEntity(userParams.toString(), "UTF-8");
			request.setEntity(formEntity);
			
			// Pull notifications from server
			HttpClient client = new AppEngineHttpClient(this);
			HttpResponse response = client.execute(request);
			StatusLine status = response.getStatusLine();
			//Log.i(TAG, status.toString());
			HttpEntity entity = response.getEntity();
			
			if (status.getStatusCode() == 200 || status.getStatusCode() == 304) {
				// Process received notifications
				if (entity != null) {
					String json = EntityUtils.toString(entity);
					JSONArray objects = (JSONArray) new JSONTokener(json).nextValue();
		            for (int i = 0; i < objects.length(); i++) {
		            	OrderNotification orderNotification = new OrderNotification(objects.getString(i));
		            	Notifier.notifyOrderEvent(this, orderNotification);
		            }
				}
			} else {
				// Unexpected status code or 204 (= no more orders to check) - shut down service
				NotificationService.cancelAlarms(this);
			}
		// Errors - shut down service
    	} catch (JSONException e) {
    		Log.e(TAG, "Failed to create JSON.", e);
    		NotificationService.cancelAlarms(this);
    	} catch (ClassCastException e) {
    		e.printStackTrace();
			NotificationService.cancelAlarms(this);
		} catch (UnsupportedEncodingException e1) {
			e1.printStackTrace();
			NotificationService.cancelAlarms(this);
		} catch (ClientProtocolException e) {
			e.printStackTrace();
			NotificationService.cancelAlarms(this);
		} catch (IOException e) {
			e.printStackTrace();
			NotificationService.cancelAlarms(this);
		}
	}
}
