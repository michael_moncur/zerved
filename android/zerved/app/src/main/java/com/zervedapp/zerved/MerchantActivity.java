package com.zervedapp.zerved;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.InputType;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager.LayoutParams;
import android.widget.EditText;
import android.widget.TextView;

import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;

public class MerchantActivity extends Activity {
	SlidingMenu mMenu;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
    	super.onCreate(savedInstanceState);
        
        // Setup action bar
        final ActionBar actionBar = getActionBar();
        actionBar.setTitle(R.string.for_merchants);
        mMenu = SlidingMenuHelper.initSlidingMenu(this, "merchant");
        
        setContentView(R.layout.merchant_settings);
        TextView merchantId = (TextView) findViewById(R.id.merchant_id);
        SharedPreferences settings = getSharedPreferences("ZervedSettings", 0);
        String id = settings.getString("merchant_id", "");
        if (id.equals("")) {
        	merchantId.setText(R.string.click_to_set);
        } else {
        	merchantId.setText(id);
        }
    }
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                mMenu.toggle();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
    
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
	    if ( keyCode == KeyEvent.KEYCODE_MENU ) {
	    	mMenu.toggle();
	        return true;
	    }
	    return super.onKeyDown(keyCode, event);
	}
    
    public void changeMerchantId(View view) {
    	AlertDialog.Builder alert = new AlertDialog.Builder(this);
		alert.setMessage(R.string.merchant_id);
		final EditText input = new EditText(this);
		input.setInputType(InputType.TYPE_CLASS_NUMBER);
		input.setHint("000000");
		SharedPreferences settings = getSharedPreferences("ZervedSettings", 0);
		input.setText(settings.getString("merchant_id", ""));
		alert.setView(input);
		alert.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
				SharedPreferences settings = getSharedPreferences("ZervedSettings", 0);
				SharedPreferences.Editor editor = settings.edit();
				String id = input.getText().toString();
				editor.putString("merchant_id", id);
				editor.commit();
				
				TextView merchantId = (TextView) findViewById(R.id.merchant_id);
		        if (id.equals("")) {
		        	merchantId.setText(R.string.click_to_set);
		        } else {
		        	merchantId.setText(id);
		        }
			}
		});
		alert.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
				// Canceled.
			}
		});
		AlertDialog alertDialog = alert.create();
		alertDialog.getWindow().setSoftInputMode(LayoutParams.SOFT_INPUT_STATE_VISIBLE);
		alertDialog.show();
    }
}
