package com.zervedapp.zerved;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.app.LoaderManager.LoaderCallbacks;
import android.content.Loader;
import android.text.InputType;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager.LayoutParams;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;
import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;
import com.zervedapp.zerved.loader.RESTLoader;
import com.zervedapp.zerved.loader.RESTLoader.RESTResponse;
import com.zervedapp.zerved.models.ConsumerManager;

public class ProfileActivity extends AuthorizedActivity implements LoaderCallbacks<RESTResponse> {
	private static final String TAG = ProfileActivity.class.getName();
	
	private static final int LOADER_PROFILE = 0x1;
	private static final int LOADER_EMAIL = 0x2;
	private static final int LOADER_ADDRESS = 0x5;
	private static final int LOADER_CURRENT_PAYMENT = 0x3;
	private static final int LOADER_DELETE_PAYMENT = 0x4;
	private static final String ARGS_URI    = "com.zervedapp.zerved.ARGS_URI";
    private static final String ARGS_PARAMS = "com.zervedapp.zerved.ARGS_PARAMS";
    
    private static String URI = Config.API_URL+"consumers/";
    private static String PAYMENT_SUBSCRIPTION_URI = Config.API_URL+"paymentsubscriptions/";
	private static final int PAYMENT_REQUEST = 1;
    
    SlidingMenu mMenu;
    
	@Override
    public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		final ActionBar actionBar = getActionBar();
        actionBar.setTitle(R.string.profile);
        
        mMenu = SlidingMenuHelper.initSlidingMenu(this, "profile");
	}
	
	@Override
	public void onResume() {
		super.onResume();
		reload();
	}
	
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                mMenu.toggle();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
    
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
	    if ( keyCode == KeyEvent.KEYCODE_MENU ) {
	    	mMenu.toggle();
	        return true;
	    }
	    return super.onKeyDown(keyCode, event);
	}
	
	@Override
	public void reload() {
		invalidateOptionsMenu();
		ConsumerManager consumer = getConsumer();
		if (consumer.mConsumerKey == null || consumer.mAccessToken == null) {
			//setContentView(R.layout.activity_authorized);
			login();
		} else {
			setContentView(R.layout.progress);
			Bundle args = new Bundle();
	    	args.putParcelable(ARGS_URI, Uri.parse(URI+getConsumer().mConsumerKey));
	    	args.putParcelable(ARGS_PARAMS, new Bundle());
			getLoaderManager().restartLoader(LOADER_PROFILE, args, this);
		}
	}
	
	@Override
	public Loader<RESTResponse> onCreateLoader(int id, Bundle args) {
		if (args != null && args.containsKey(ARGS_URI) && args.containsKey(ARGS_PARAMS)) {
            Uri    action = args.getParcelable(ARGS_URI);
            Bundle params = args.getParcelable(ARGS_PARAMS);
            if (id == LOADER_PROFILE) {
    	        return new RESTLoader(this, RESTLoader.HTTPVerb.GET, action, params, getConsumer().mAccessToken);
    		} else if (id == LOADER_EMAIL || id == LOADER_ADDRESS) {
    	        return new RESTLoader(this, RESTLoader.HTTPVerb.POST, action, params, getConsumer().mAccessToken);
    		} else if (id == LOADER_CURRENT_PAYMENT) {
    			return new RESTLoader(this, RESTLoader.HTTPVerb.POST, action, params, getConsumer().mAccessToken);
    		} else if (id == LOADER_DELETE_PAYMENT) {
    			return new RESTLoader(this, RESTLoader.HTTPVerb.DELETE, action, params, getConsumer().mAccessToken);
    		}
		}
		return super.onCreateLoader(id, args);
	}
	
	@Override
	public void onLoadFinished(Loader<RESTResponse> loader, RESTResponse data) {
		int    code = data.getCode();
        String json = data.getData();
        
		if (loader.getId() == LOADER_EMAIL) {
			if (code == 200) {
				try {
	        		JSONObject object = (JSONObject) new JSONTokener(json).nextValue();
	        		ConsumerManager consumer = ((ZervedApp) getApplication()).getConsumer();
	        		consumer.mAccessToken = object.getString("access_token");
	        		consumer.mConsumerKey = object.getString("key");
	        		SharedPreferences settings = getSharedPreferences("ZervedSettings", 0);
	        		SharedPreferences.Editor editor = settings.edit();
	        		editor.putString("email", object.getString("email"));
	        		editor.commit();
	        		reload();
	        	} catch (JSONException e) {
	        		Log.e(TAG, "Failed to parse JSON.", e);
	        	}
			}
			else if (code == 409) {
				Toast toast = Toast.makeText(this, data.getData(), Toast.LENGTH_SHORT);
	        	toast.setGravity(Gravity.CENTER, 0, 0);
	        	toast.show();
			}
		} else if (loader.getId() == LOADER_PROFILE && code == 200) {
			// Update email view
	    	LayoutInflater inflater = getLayoutInflater();
	    	View view = inflater.inflate(R.layout.profile, null);
	    	TextView emailSummary = (TextView) view.findViewById(R.id.email_summary);
	    	SharedPreferences settings = getSharedPreferences("ZervedSettings", 0);
	    	String email = settings.getString("email", "");
	    	if (email.equals("")) {
	    		emailSummary.setText(getResources().getString(R.string.click_to_add_email));
	    	} else {
	    		emailSummary.setText(email);
	    	}
	    	
    		final EditText deliveryName = (EditText) view.findViewById(R.id.delivery_name_field);
    		final EditText deliveryAddress1 = (EditText) view.findViewById(R.id.delivery_address1_field);
    		final EditText deliveryAddress2 = (EditText) view.findViewById(R.id.delivery_address2_field);
    		final EditText deliveryCity = (EditText) view.findViewById(R.id.delivery_city_field);
    		final EditText deliveryPostcode = (EditText) view.findViewById(R.id.delivery_postcode_field);
    		final EditText deliveryPhone = (EditText) view.findViewById(R.id.delivery_phone_field);
	    	
	    	// Build credit card list
	    	LinearLayout creditCardList = (LinearLayout) view.findViewById(R.id.credit_card_list);
	    	creditCardList.removeAllViews();
	    	try {
				JSONObject profile = (JSONObject) new JSONTokener(json).nextValue();
				JSONArray paymentSubscriptions = profile.getJSONArray("payment_subscriptions");
				
				deliveryName.setText(profile.getString("name").equals("null") ? "" : profile.getString("name"));
                deliveryAddress1.setText(profile.getString("address1").equals("null") ? "" : profile.getString("address1"));
                deliveryAddress2.setText(profile.getString("address2").equals("null") ? "" : profile.getString("address2"));
                deliveryCity.setText(profile.getString("city").equals("null") ? "" : profile.getString("city"));
                deliveryPostcode.setText(profile.getString("postcode").equals("null") ? "" : profile.getString("postcode"));
                deliveryPhone.setText(profile.getString("phone").equals("null") ? "" : profile.getString("phone"));
				
				view.findViewById(R.id.active_label).setVisibility(paymentSubscriptions.length() == 0 ? View.GONE : View.VISIBLE);
				String currentPaymentKey = "";
				if (profile.has("current_payment_subscription")) {
					currentPaymentKey = profile.getJSONObject("current_payment_subscription").getString("key");
				}
				for (int i = 0; i < paymentSubscriptions.length(); i++) {
					// Add credit card description and buttons
					JSONObject paymentSubscription = (JSONObject) paymentSubscriptions.getJSONObject(i);
					View card = inflater.inflate(R.layout.item_credit_card, null);
					RadioButton useForPayments = (RadioButton) card.findViewById(R.id.use_for_payments);
					useForPayments.setText(paymentSubscription.getString("description"));
					useForPayments.setTag(paymentSubscription.getString("key"));
					useForPayments.setChecked(currentPaymentKey.equals(paymentSubscription.getString("key")));
					Button delete = (Button) card.findViewById(R.id.delete);
					delete.setTag(paymentSubscription.getString("key"));
					creditCardList.addView(card);
				}
			} catch (JSONException e) {
				Log.e(TAG, "Failed to parse JSON.", e);
			}
	    	setContentView(view);
		} else if (loader.getId() == LOADER_CURRENT_PAYMENT || loader.getId() == LOADER_DELETE_PAYMENT) {
			reload();
			if (code != 200) {
				Toast.makeText(this, data.getData(), Toast.LENGTH_SHORT).show();
			}
		} else if (loader.getId() == LOADER_ADDRESS) {
			if (code == 200) {
				Toast.makeText(this, getResources().getString(R.string.profile_updated), Toast.LENGTH_SHORT).show();
			}
			reload();
		}
		super.onLoadFinished(loader, data);
	}

	@Override
	public void onJsonLoaded(String json) {
    	
	}
	
	public void changeEmail(View view) {
		AlertDialog.Builder alert = new AlertDialog.Builder(this);
		alert.setMessage(R.string.enter_new_email);
		final EditText input = new EditText(this);
		input.setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
		input.setHint(R.string.email);
		alert.setView(input);
		alert.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
				// Validate email address
				if (!input.getText().toString().equals("") && !input.getText().toString().matches("[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+")) {
					Toast toast = Toast.makeText(ProfileActivity.this, R.string.invalid_email, Toast.LENGTH_SHORT);
		        	toast.setGravity(Gravity.CENTER, 0, 0);
		        	toast.show();
		        	return;
				}
				JSONObject postParams = new JSONObject();
		    	try {
		    		postParams.put("email", input.getText().toString());
		    	} catch (JSONException e) {
		    		Log.e(TAG, "Failed to create JSON.", e);
		    	}
				Bundle args = new Bundle();
		    	args.putParcelable(ARGS_URI, Uri.parse(URI+getConsumer().mConsumerKey));
		    	Bundle params = new Bundle();
		    	params.putString("json", postParams.toString());
		    	args.putParcelable(ARGS_PARAMS, params);
				getLoaderManager().restartLoader(LOADER_EMAIL, args, ProfileActivity.this);
			}
		});
		alert.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
				// Canceled.
			}
		});
		AlertDialog alertDialog = alert.create();
		alertDialog.getWindow().setSoftInputMode(LayoutParams.SOFT_INPUT_STATE_VISIBLE);
		alertDialog.show();
	}
	
	public void saveAddress(View v) {

		EditText deliveryName = (EditText) findViewById(R.id.delivery_name_field);
		EditText deliveryAddress1 = (EditText) findViewById(R.id.delivery_address1_field);
		EditText deliveryAddress2 = (EditText) findViewById(R.id.delivery_address2_field);
		EditText deliveryCity = (EditText) findViewById(R.id.delivery_city_field);
		EditText deliveryPostcode = (EditText) findViewById(R.id.delivery_postcode_field);
		EditText deliveryPhone = (EditText) findViewById(R.id.delivery_phone_field);
				
		JSONObject postParams = new JSONObject();
    	try {
    		postParams.put("name", deliveryName.getText().toString());
    		postParams.put("address1", deliveryAddress1.getText().toString());
    		postParams.put("address2", deliveryAddress2.getText().toString());
    		postParams.put("postcode", deliveryPostcode.getText().toString());
    		postParams.put("city", deliveryCity.getText().toString());
    		postParams.put("phone", deliveryPhone.getText().toString());
    	} catch (JSONException e) {
    		Log.e(TAG, "Failed to create JSON.", e);
    	}
		Bundle args = new Bundle();
    	args.putParcelable(ARGS_URI, Uri.parse(URI + getConsumer().mConsumerKey));
    	Bundle params = new Bundle();
    	params.putString("json", postParams.toString());
    	args.putParcelable(ARGS_PARAMS, params);
		getLoaderManager().restartLoader(LOADER_CURRENT_PAYMENT, args, this);
		setContentView(R.layout.progress);		
	}
	
	public void deleteCard(View v) {
		// Post payment subscription key/credit card to server
		Bundle args = new Bundle();
    	args.putParcelable(ARGS_URI, Uri.parse(PAYMENT_SUBSCRIPTION_URI + (String) v.getTag()));
    	args.putParcelable(ARGS_PARAMS, new Bundle());
		getLoaderManager().restartLoader(LOADER_DELETE_PAYMENT, args, this);
		setContentView(R.layout.progress);
	}

	public void addCard(View v) {
		Intent intent = new Intent(this, EPayActivity.class);
		intent.putExtra(EPayActivity.EXTRA_MUST_SAVE_CARD, true);
		intent.putExtra(EPayActivity.EXTRA_AMOUNT, 0.00);
		intent.putExtra(EPayActivity.EXTRA_CURRENCY, "208"); // 208 = Danish Krone.
		intent.putExtra(EPayActivity.EXTRA_TITLE, getResources().getString(R.string.payment));
		intent.putExtra(EPayActivity.EXTRA_BUTTON_TITLE, getResources().getString(R.string.continue_payment));

		// Important: The save_card_prefix_identifier must be the same on the GAE server (gae/app/api/epay.py).
		// It is used to distinguish regular orders from "save card" orders.
		String save_card_prefix_identifier = "save";
		// When saving card, orderid must be prefix+consumerid.
		String prefixedOrderId = save_card_prefix_identifier + getConsumer().mConsumerId;
		intent.putExtra(EPayActivity.EXTRA_ORDER_ID, prefixedOrderId);

		startActivityForResult(intent, PAYMENT_REQUEST);
	}

	public void useForPayments(View v) {
		// Check/uncheck radio buttons
		ViewGroup cardList = (ViewGroup) v.getParent().getParent();
		for (int i=0; i<cardList.getChildCount(); i++) {
			RadioButton card = (RadioButton) cardList.getChildAt(i).findViewById(R.id.use_for_payments);
			card.setChecked(card == v);
		}
		
		// Post selected payment subscription/credit card to server
		JSONObject postParams = new JSONObject();
    	try {
    		postParams.put("current_payment_subscription", (String)v.getTag());
    	} catch (JSONException e) {
    		Log.e(TAG, "Failed to create JSON.", e);
    	}
		Bundle args = new Bundle();
    	args.putParcelable(ARGS_URI, Uri.parse(URI+getConsumer().mConsumerKey));
    	Bundle params = new Bundle();
    	params.putString("json", postParams.toString());
    	args.putParcelable(ARGS_PARAMS, params);
		getLoaderManager().restartLoader(LOADER_CURRENT_PAYMENT, args, this);
		setContentView(R.layout.progress);
	}
	
	public void deleteAccount(View v) {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle(R.string.warning)
			.setMessage(R.string.delete_account_message);
		builder.setPositiveButton(R.string.delete, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				getConsumer().deleteAccount(ProfileActivity.this, ProfileActivity.this);
				reload();
			}
		});
		builder.setNegativeButton(R.string.keep, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// Cancelled
			}
		});
		AlertDialog dialog = builder.create();
		dialog.show();
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == PAYMENT_REQUEST) {
			if (resultCode == Activity.RESULT_OK) {
				Log.d(TAG, "todo");
			}
		}
	}
}
