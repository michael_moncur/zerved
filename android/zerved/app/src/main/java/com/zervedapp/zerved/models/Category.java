package com.zervedapp.zerved.models;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONTokener;

import android.util.Log;

public class Category extends JSONModel {
	private static final String TAG = Category.class.getName();
	
	public Category(String json) throws JSONException {
		super(json);
	}
	
	public static ArrayList<Category> initList(String json) {
        ArrayList<Category> objectList = new ArrayList<Category>();
        
        try {
        	JSONArray objects = (JSONArray) new JSONTokener(json).nextValue();
            for (int i = 0; i < objects.length(); i++) {
                //JSONObject object = objects.getJSONObject(i);
            	Category object = new Category(objects.getString(i));
                objectList.add(object);
            }
            
        }
        catch (JSONException e) {
            Log.e(TAG, "Failed to parse JSON.", e);
        }
        
        return objectList;
	}
	
	public String getName() {
		return this.getStringProperty("name");
	}
	
	public String toString() {
		return this.getName();
	}
	
	@Override
	public boolean equals(Object o){
		if(! (o instanceof Category))
			return false;
		return ((Category)o).getKey().equals(this.getKey());
	}
	
}
