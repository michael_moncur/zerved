package com.zervedapp.zerved;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import android.animation.ValueAnimator;
import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.app.TaskStackBuilder;
import android.app.LoaderManager.LoaderCallbacks;
import android.content.Loader;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.koushikdutta.urlimageviewhelper.UrlImageViewHelper;
import com.zervedapp.zerved.loader.RESTLoader;
import com.zervedapp.zerved.loader.RESTLoader.RESTResponse;
import com.zervedapp.zerved.models.Bar;
import com.zervedapp.zerved.models.Cart;
import com.zervedapp.zerved.models.Event;
import com.zervedapp.zerved.models.MerchantLocation;
import com.zervedapp.zerved.models.OptionValue;
import com.zervedapp.zerved.models.Product;
import com.zervedapp.zerved.models.ProductOption;

public class ProductActivity  extends Activity implements LoaderCallbacks<RESTResponse>  {
	public static final String TAG = ProductActivity.class.getName();
	
	private Product mProduct;
	private MerchantLocation mLocation; 
	private Bar mBar;
	private String mGroup;
	Event mEvent = null;
	
	ViewPager viewPager;
	PagerAdapter adapter;
	int viewPagerHeight;
	int quantity = 1;
	
	ProgressDialog mDialog;
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.productactivity);
        
        final ActionBar actionBar = getActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        
        Intent intent = getIntent();
        mGroup = intent.getStringExtra(CategoryListFragment.EXTRA_GROUP);
               
        
        String productJson = intent.getStringExtra(CategoryListFragment.EXTRA_PRODUCT);
        try {
        	mProduct = new Product(productJson);
		} catch (JSONException e) {
			Log.e(TAG, "Failed to parse JSON.", e);
		}
        
        if(mGroup.equals("menu")){
	        String barJson = intent.getStringExtra(CategoryListFragment.EXTRA_BAR);
			try {
				mBar = new Bar(barJson);
			} catch (JSONException e) {
				Log.e(TAG, "Failed to parse JSON.", e);
			}
	        Log.i(TAG, "Bar: " + mBar);
        }
        
        mLocation = ((ZervedApp) getApplication()).mLocation; 
        mEvent = ((ZervedApp) getApplication()).mEvent;
      
        setTitle(mProduct.getName());
        resetOptions(); // Set required options to first possible value
        refreshView();
	}
	
    @Override
    public void onResume() {
    	super.onResume();
    	updateView();
    }
	
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
    	MenuInflater inflater = getMenuInflater();
    	inflater.inflate(R.menu.products, menu);
    	return true;
    }
    
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
    	for (int i=0; i<menu.size(); i++) {
    		MenuItem item = menu.getItem(i);
    		if (item.getItemId() == R.id.cart) {
    			// Update cart button to show number of items
    			Cart cart = ((ZervedApp) getApplication()).getCart(mLocation, mBar, mGroup, mEvent);
    			int s = cart.size();
    			View view;
    			if(s > 0){
	    			view = getLayoutInflater().inflate(R.layout.custom_cart_layout, null);
					final TextView cartBtnText = (TextView) view.findViewById(R.id.cart_btn_text);

					CategoriesActivity.getFlashAnimation(cartBtnText).start();

					cartBtnText.setOnClickListener(new OnClickListener() {
						@Override
						public void onClick(View v) {
							startActivity(getCartIntent());
						}
					});
    			}
    			else {
    				view = getLayoutInflater().inflate(R.layout.custom_cart_layout_disabled, null);
    			}
    			((TextView) view.findViewById(R.id.cart_btn_text)).setText(String.format(getResources().getString(R.string.cart_x), cart.size()));
    			item.setActionView(view);
    		}
    	}
    	return true;
    }
    
    public Intent getCartIntent() {
    	Intent intent = new Intent(this, CartActivity.class);
    	intent.putExtra(CategoryListFragment.EXTRA_LOCATION, mLocation.toJson());
    	if (mBar != null) {
    		intent.putExtra(CategoryListFragment.EXTRA_BAR, mBar.toJson());
    	}
    	intent.putExtra(CategoryListFragment.EXTRA_GROUP, mGroup);
    	return intent;
    }
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // This is called when the Home (Up) button is pressed in the action bar.
                // Create a simple intent that starts the hierarchical parent activity and
                // use NavUtils in the Support Package to ensure proper handling of Up.
                Intent upIntent = new Intent(this, LocationActivity.class);
                if (shouldUpRecreateTask(upIntent)) {
                    // This activity is not part of the application's task, so create a new task
                    // with a synthesized back stack.
                    TaskStackBuilder.create(this)
                            // If there are ancestor activities, they should be added here.
                            .addNextIntent(upIntent)
                            .startActivities();
                    finish();
                } else {
                    // This activity is part of the application's task, so simply
                    // navigate up to the hierarchical parent activity.
                    //NavUtils.navigateUpTo(this, upIntent);
                	// navigateUpTo recreates the activity which will revert to first tab - instead we simply finish this activity to go back without recreating the parent
                	finish();
                }
                return true;
            case R.id.cart:
            	Intent intent = new Intent(this, CartActivity.class);
            	intent.putExtra(CategoryListFragment.EXTRA_LOCATION, mLocation.toJson());
            	if (mBar != null) {
            		intent.putExtra(CategoryListFragment.EXTRA_BAR, mBar.toJson());
            	}
            	intent.putExtra(CategoryListFragment.EXTRA_GROUP, mGroup);
            	startActivity(intent);
            	return true;
        }
        return super.onOptionsItemSelected(item);
    }

	public MerchantLocation getLocation() {
		return mLocation;
	}

	public Bar getBar() {
		return mBar;
	}
	
	public void addQuantity(int n){
		this.quantity += n;
		if(this.quantity < 1) {
			this.quantity = 1;
		}
	}
	public int getQuantity() {
		return this.quantity;
	}
	
	public void refreshView() {
		
		LayoutInflater inflater = LayoutInflater.from(this);
		View view = inflater.inflate(R.layout.productactivity, null);       
		setContentView(view);

		// Status message when merchant is open or closed
		Event mEvent = ((ZervedApp)getApplication()).mEvent;
		boolean open = (mEvent != null) ? mEvent.isOpenForOrders() : mProduct.isOrderable();
		
		LinearLayout closedLayout = (LinearLayout) findViewById(R.id.merchant_closed_layout);
		TextView closedErrorView = (TextView) findViewById(R.id.merchant_closed);
		closedErrorView.setText(mProduct.getStringProperty("message_when_closed"));
		closedLayout.setVisibility(!open ? View.VISIBLE : View.GONE);

		LinearLayout openLayout = (LinearLayout) findViewById(R.id.merchant_open_layout);
		TextView openTextView = (TextView) findViewById(R.id.merchant_open);
		openTextView.setText(R.string.open_for_orders);
		openLayout.setVisibility(open ? View.VISIBLE : View.GONE);
	
		// Product header
		View headerView;
		if (mProduct.getImageUrl() != null) {
			// Header layout with product image
			headerView = inflater.inflate(R.layout.product_header_withimage, null);
		} else {
			// Header layout without product image
			headerView = inflater.inflate(R.layout.product_header_noimage, null);
		}
		
		// Put header into layout
		((FrameLayout)findViewById(R.id.product_header)).addView(headerView);
		
		// Product Image
		if (mProduct.getImageUrl() != null) {
			ImageView image = (ImageView) headerView.findViewById(R.id.image);
			UrlImageViewHelper.setUrlDrawable(image, mProduct.getImageUrl());
		}
        
        // Product Name / Price
        TextView productName = (TextView) findViewById(R.id.product_name);
        TextView price = (TextView) findViewById(R.id.product_price);   
        
        productName.setText(mProduct.getName());
        price.setText(mProduct.getStringProperty("final_price_formatted"));
        
        // Quantity
        refreshQuantity();
		ImageButton plusBtn = (ImageButton) findViewById(R.id.main_plus);
		ImageButton minusBtn = (ImageButton) findViewById(R.id.main_minus);
    	plusBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				addQuantity(1);
				refreshQuantity();
			}
		});	        	 
    	minusBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if(getQuantity() > 1)
					addQuantity(-1);
				
				refreshQuantity();
			}
		});        	
        
    	// Product description
        TextView productDesc = (TextView) findViewById(R.id.product_desc);
        productDesc.setText(mProduct.getDescription());

		// Tier prices
		LinearLayout tierPrices = (LinearLayout) findViewById(R.id.tier_prices);
		tierPrices.removeAllViews();
		for (String tierPrice: mProduct.getTieredPriceDescriptions()) {
			TextView tierPriceView = (TextView) inflater.inflate(R.layout.tier_price, null);
			tierPriceView.setText(tierPrice);
			tierPrices.addView(tierPriceView);
		}
		
		// Options
		refreshOptions();
		
		// Add to Cart
		Button addToCart = (Button) findViewById(R.id.add_to_cart);
		addToCart.setEnabled(open);
		addToCart.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				addToCart();				
			}
		});
		
		// Yellow lines
		refreshSelectedProducts();
	}
	
	public void refreshOptions() {
		TextView optionsHeader = (TextView) findViewById(R.id.options_header);
		LinearLayout options = (LinearLayout) findViewById(R.id.options);
		options.removeAllViews();
		
		if(mProduct.getProductOptions().size() == 0){
			optionsHeader.setVisibility(View.GONE);
			options.setVisibility(View.GONE);
		}
		else {
			
			ProductOptionAdapter productOptionAdapter =  new ProductOptionAdapter(this, R.layout.option_list_item, getOptions());
			for (ProductOption option: mProduct.getProductOptions()) {
		    	productOptionAdapter.add(option);
		    }
			for (int i=0; i<productOptionAdapter.getCount(); i++) {
				options.addView(productOptionAdapter.getView(i, null, null));
			}
			
		}
	}
	
	public void refreshQuantity() {
		TextView productQty = (TextView) findViewById(R.id.main_product_quantity);	
		productQty.setText(Integer.toString(getQuantity()));
	}
	
	public void refreshSelectedProducts(){
		// Yellow cart lines removed
		/*
		LayoutInflater inflater = LayoutInflater.from(this);
		final Cart cart =  ((ZervedApp)getApplication()).getCart(mLocation, mBar, mGroup, mEvent);
		ArrayList<Cart.Item> cartItems = cart.getItems();   
		
		LinearLayout selectedProducts = (LinearLayout) findViewById(R.id.selected_products);
		selectedProducts.removeAllViews();
		for(final Cart.Item item : cartItems)
		{
			if(item.getProduct().getKey().equals(mProduct.getKey()))
			{
				RelativeLayout selectedProduct = (RelativeLayout) inflater.inflate(R.layout.selected_product, null);
				selectedProducts.addView(selectedProduct);
				
				final TextView productQuantity = (TextView) selectedProduct.findViewById(R.id.product_quantity);
				TextView productOptionsLabel = (TextView) selectedProduct.findViewById(R.id.product_desc);
				final TextView productPrice = (TextView) selectedProduct.findViewById(R.id.product_price);
				
				ImageButton plusBtn = (ImageButton) selectedProduct.findViewById(R.id.plus);
				ImageButton minusBtn = (ImageButton) selectedProduct.findViewById(R.id.minus);

				
				// Quantity and product name
				productQuantity.setText(Integer.toString(item.getQuantity()));
		
				// Options
	    		String opt = "";	        		
	    		ArrayList<ProductOption> productOptions = mProduct.getProductOptions();
	        	for(OptionValue option: item.getOptions()){
	        		for(ProductOption productOption: productOptions){
	        			if(option.getId().equals(productOption.getId())){
	        				for(Value v: productOption.getValues()){
	        					if(v.getIndex() == option.getValue()){
	        						opt += productOption.getName() + ": " + v.getLabel() + ", ";
	        					}
	        				} 
	        			}
	        		}
	    		}
	        	if(opt.length() > 0) {
	        		opt = opt.substring(0, opt.length()-2);
	        	}
	        	productOptionsLabel.setText(opt);
	        	if(opt.equals(""))
	        		productOptionsLabel.setVisibility(View.GONE);
	        	
	        	// Price
	        	if(item.getQuoteItem() != null)
	        		productPrice.setText(item.getQuoteItem().getStringProperty("price_formatted"));
	        	
	        	// Quantity
	        	plusBtn.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						item.addQuantity(1);
						productQuantity.setText(Integer.toString(item.getQuantity()));
						productPrice.setText("--");
						updateView();
					}
				});	        	 
	        	minusBtn.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						if(item.getQuantity() <= 1){
							((View)(v.getParent().getParent())).setVisibility(View.GONE);
							cart.removeItem(item);
						}
						else {
							item.addQuantity(-1);
							productQuantity.setText(Integer.toString(item.getQuantity()));
							productPrice.setText("--");
						}						
						updateView();
					}
				});       	
			}
		}
		*/
	}
	
	ArrayList<OptionValue> options = new ArrayList<OptionValue>();
    int checkedItems = 0;
    public void addOption(OptionValue v){
    	sortOptions();
    	if(!this.options.contains(v))
    		this.options.add(v);
    }
    public void removeOption(OptionValue v){
    	sortOptions();
    	if(this.options.contains(v))
    		this.options.remove(v);
    }
    public ArrayList<OptionValue> getOptions(){
    	sortOptions();
    	return this.options;
    }
    public void initOptions(){
    	for(ProductOption option: mProduct.getProductOptions()){
    		if(option.isRequired() && option.getSelectionType().equals("single")){
    			addOption( new OptionValue(option.getKey(), option.getId(), 0) );
    		}
    	}
    }
    public void resetOptions(){
    	this.options = new ArrayList<OptionValue>();
    	initOptions();
    }
    public void sortOptions(){
    	// Sort the selected options
    	ArrayList<ProductOption> mProductOptions = mProduct.getProductOptions();
    	ArrayList<OptionValue> sortedOptions = new ArrayList<OptionValue>();
    	for(ProductOption po : mProductOptions) {
    		for(OptionValue ov : this.options){
    			if(ov.getId().equals(po.getId())){
	    			sortedOptions.add(ov);
	    		}
	    	}
    	}
    	this.options = sortedOptions;
    }
    public void addCheckedItems(int n){
    	this.checkedItems += n;
    }
    
    public void askForOption(final View view) {
    	final ProductOption option = (ProductOption) ((RelativeLayout)view).getTag();
    	
    	if (option.getSelectionType().equals("single")) {
    		
			int selDefault = option.isRequired() ? 0 : -1;
			for(OptionValue v : getOptions()){
				if(v.getId().equals(option.getId()))
					selDefault = v.mSelectedIndex;
			}
			
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setTitle(option.getName())
				.setSingleChoiceItems(ProductOption.getValuesLabels(option.getValues()), selDefault, new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						OptionValue v = new OptionValue(option.getKey(), option.getId(), which);
						
						// Single choice item: remove previous choice and add new one
						OptionValue found = null;
						for(OptionValue value: getOptions()){
							if(value.getId().equals(option.getId()))
								found = value;
						}
						if(found != null)
							removeOption(found);
						
						addOption(v);
					}
				 })
				 .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
				     @Override
				     public void onClick(DialogInterface dialog, int id) {
				    	 refreshOptions();
				     }
				 });
			AlertDialog dialog = builder.create();
			dialog.show();
    	}
    	else if (option.getSelectionType().equals("multiple")) {
    		this.checkedItems = 0;
    		boolean [] selectedIndexes = new boolean[option.getValues().size()];
	    	
			for(OptionValue v : getOptions()){
				if(v.getId().equals(option.getId())){
					selectedIndexes[v.mSelectedIndex] = true;
					this.checkedItems++;
				}
			}
			
    		AlertDialog.Builder builder = new AlertDialog.Builder(this);
    		builder.setTitle(option.getName())
      		.setMultiChoiceItems(ProductOption.getValuesLabels(option.getValues()), selectedIndexes,  new DialogInterface.OnMultiChoiceClickListener() {
  		         @Override
  		         public void onClick(DialogInterface dialog, int which, boolean isChecked) {     	        
  		        	OptionValue v = new OptionValue(option.getKey(), option.getId(), which);
					if (isChecked) {                    	
					    addOption(v);                                    
					    addCheckedItems(1);
					}
					else {
						removeOption(v);
						addCheckedItems(-1); 
					}
  		         }
  		     })
  		     .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
  		         @Override
  		         public void onClick(DialogInterface dialog, int id) {
  		        	 if(option.isRequired() && checkedItems == 0) {
  		        		 askForOption(view);
  		        	 }
  		        	 refreshOptions();
  		         }
  		     });
	      AlertDialog dialog = builder.create();
	  	  dialog.show();
      }
    }    
	
	public void addToCart() {
		mDialog = new ProgressDialog(this);
        mDialog.setMessage(getString(R.string.loading));
        mDialog.setCancelable(false);
        mDialog.show();
		final Cart cart = ((ZervedApp) getApplication()).getCart(mLocation, mBar, mGroup, mEvent);	
		Cart.Item item = cart.new Item(mProduct, getQuantity(), getOptions());
    	// Validate and add item
    	String error = mProduct.validateItem(this, item);
    	if (error.equals("")) {
    		cart.addItem(item);
    		//resetOptions();
    		//refreshOptions();
    		//updateView();
    		loadPricesAndReturnToList();
    	} else {
    		mDialog.dismiss();
    		Toast.makeText(this, error, Toast.LENGTH_SHORT).show();
    	}
	}
	
	//=============================================================
	// Load cart items and prices to be displayed as "yellow" lines
	
	private static final int LOADER_QUOTE = 0x1;
	private static final int LOADER_QUOTE_AND_RETURN = 0x2;
	private static final String ARGS_URI    = "com.zervedapp.zerved.ARGS_URI";
    private static final String ARGS_PARAMS = "com.zervedapp.zerved.ARGS_PARAMS";
    private static String URI = Config.API_URL+"quote";

    public void loadPrices(){
    	Bundle args = new Bundle();
    	args.putParcelable(ARGS_URI, Uri.parse(URI));
    	args.putParcelable(ARGS_PARAMS, new Bundle());
    	getLoaderManager().restartLoader(LOADER_QUOTE, args, this);
    }
    
    public void loadPricesAndReturnToList(){
    	Bundle args = new Bundle();
    	args.putParcelable(ARGS_URI, Uri.parse(URI));
    	args.putParcelable(ARGS_PARAMS, new Bundle());
    	getLoaderManager().restartLoader(LOADER_QUOTE_AND_RETURN, args, this);
    }
	
    public void updateView(){
		//loadPrices(); // Yellow cart lines removed
		invalidateOptionsMenu();
    }
    
	@Override
	public Loader<RESTResponse> onCreateLoader(int id, Bundle args) {
		if (args != null && args.containsKey(ARGS_URI) && args.containsKey(ARGS_PARAMS)) {
            Uri    action = args.getParcelable(ARGS_URI);
            Bundle params = args.getParcelable(ARGS_PARAMS);
        	params.putString("json", ((ZervedApp)getApplication()).getCart(mLocation, mBar, mGroup, mEvent).toJson().toString());
            if (id == LOADER_QUOTE || id == LOADER_QUOTE_AND_RETURN) {
    	        return new RESTLoader(this, RESTLoader.HTTPVerb.POST, action, params, "");
            } 
		}
		return null;
	}

	@Override
	public void onLoadFinished(Loader<RESTResponse> loader, RESTResponse data) {
		if (mDialog != null) {
			mDialog.dismiss();
		}
		int    code = data.getCode();
        String json = data.getData();
        if (loader.getId() == LOADER_QUOTE || loader.getId() == LOADER_QUOTE_AND_RETURN) {
        	if (code == 200) {
	        	try {
	        		JSONObject object = (JSONObject) new JSONTokener(json).nextValue();
	        		if (object.has("items")) { 
	        			JSONArray items = object.getJSONArray("items");
	                    for (int i = 0; i < items.length(); i++) {
	                    	JSONObject item = items.getJSONObject(i);
	                    	 ((ZervedApp)getApplication()).getCart(mLocation, mBar, mGroup, mEvent).addQuoteItem(i, item);
	                    }
	        		} 	  
	        	} 
	        	catch (JSONException e) {
	        		Log.e("", "Failed to parse JSON.", e);
	        	}
	        	if (loader.getId() == LOADER_QUOTE_AND_RETURN) {
	        		finish();
	        	} else {
	        		refreshSelectedProducts();
	        	}
        	}
        	else {
        		mDialog.dismiss();
        	}        	
        }
	}

	@Override
	public void onLoaderReset(Loader<RESTResponse> arg0) {
	}
}