package com.zervedapp.zerved.models;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONTokener;

import android.util.Log;

public class Bar extends JSONModel {
	private static final String TAG = Bar.class.getName();
	
	public Bar(String json) throws JSONException {
		super(json);
	}
	
	public static ArrayList<Bar> initList(String json) {
        ArrayList<Bar> objectList = new ArrayList<Bar>();
        
        try {
        	JSONArray objects = (JSONArray) new JSONTokener(json).nextValue();
            for (int i = 0; i < objects.length(); i++) {
            	Bar object = new Bar(objects.getString(i));
            	objectList.add(object);
            }
            
        }
        catch (JSONException e) {
            Log.e(TAG, "Failed to parse JSON.", e);
        }
        
        return objectList;
	}
	
	public String getName() {
		return getStringProperty("name");
	}
	
	public boolean hasTableService(){
		return has("table_service") && getBooleanProperty("table_service") == true;
	}
	
	public boolean hasCounterService(){
		return hasCounterServiceToStay() || hasCounterServiceToGo();
	}
	
	public boolean hasToStayAndToGo(){
		return hasCounterServiceToStay() && hasCounterServiceToGo();
	}
	
	public boolean hasCounterServiceToStay() {
		return has("counter_service_to_stay") && getBooleanProperty("counter_service_to_stay") == true;
	}
	
	public boolean hasCounterServiceToGo() {
		return has("counter_service_to_go") && getBooleanProperty("counter_service_to_go") == true;
	}
	
	public boolean hasDeliveryService(){
		return has("delivery_service") && getBooleanProperty("delivery_service") == true;
	}
	
	public String getDeliveryMinOrderAmountFormatted(){
		return getStringProperty("delivery_min_order_amount_formatted");
	}

	public double getDeliveryMinOrderAmount(){
		return getDoubleProperty("delivery_min_order_amount");
	}
	
	
	public boolean isOpen(){
		return has("open") && getBooleanProperty("open");
	}
	
	public String toString() {
		return this.getName();
	}
	public ArrayList<MenuCard> getMenus() {
		JSONArray objects;
		ArrayList<MenuCard> menus = new ArrayList<MenuCard>();
		try {
			objects = getJSONArray("menus");
			for (int i = 0; i < objects.length(); i++) {
				menus.add(new MenuCard(objects.getString(i)));
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
        return menus;
	}
}
