package com.zervedapp.zerved;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.os.SystemClock;

import com.commonsware.cwac.wakeful.WakefulIntentService;
import com.commonsware.cwac.wakeful.WakefulIntentService.AlarmListener;

public class NotificationListener implements AlarmListener {
	public static final int PULL_INTERVAL_SECONDS = 15;
	
	public NotificationListener() { }
	
	@Override
	public long getMaxAge() {
		return 120*1000;
	}

	@Override
	public void scheduleAlarms(AlarmManager mgr, PendingIntent pi,
			Context context) {
		mgr.setRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP, SystemClock.elapsedRealtime()+PULL_INTERVAL_SECONDS*1000, PULL_INTERVAL_SECONDS*1000, pi);
	}

	@Override
	public void sendWakefulWork(Context context) {
		WakefulIntentService.sendWakefulWork(context, NotificationService.class);
	}

}
