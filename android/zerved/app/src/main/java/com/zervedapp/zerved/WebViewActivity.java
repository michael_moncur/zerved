package com.zervedapp.zerved;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.webkit.JavascriptInterface;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import com.zervedapp.zerved.models.ConsumerManager;

public class WebViewActivity extends Activity {
	private static final String TAG = WebViewActivity.class.getName();
	
	public static final String EXTRA_URL = "com.zervedapp.zerved.URL";
	public static final String EXTRA_TITLE = "com.zervedapp.zerved.TITLE";
	
	protected WebView mWebView;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// Progress bar
		getWindow().requestFeature(Window.FEATURE_PROGRESS);
		getWindow().setFeatureInt(Window.FEATURE_PROGRESS, Window.PROGRESS_VISIBILITY_ON);
		
		Intent intent = getIntent();
		
		// Title
		final ActionBar actionBar = getActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle(intent.getStringExtra(EXTRA_TITLE));
        
        // Web view
        initWebView();
		setContentView(mWebView);
		mWebView.loadUrl(intent.getStringExtra(EXTRA_URL), getAdditionalHeaders());
	}
	
	protected void initWebView() {
        mWebView = new WebView(this);
		
		// Webview javascript callbacks
        mWebView.addJavascriptInterface(new Object() {
			// View terms popup
			@JavascriptInterface
			public void showTerms() {
				runOnUiThread(new Runnable() {
					@Override
					public void run() {
						AlertDialog.Builder alert = new AlertDialog.Builder(WebViewActivity.this);
						alert.setTitle(R.string.terms_and_conditions);
						alert.setNegativeButton(R.string.close, new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int which) {}
						});

						View view = getLayoutInflater().inflate(R.layout.dialog_webview, null);
						final WebView webView = (WebView) view.findViewById(R.id.webview);
						final ProgressBar progress = (ProgressBar) view.findViewById(R.id.progress);
						webView.setWebViewClient(new WebViewClient() {
							@Override
							public void onPageFinished(WebView view, String url) {
								webView.setVisibility(View.VISIBLE);
								progress.setVisibility(View.INVISIBLE);
							}
						});
						alert.setView(view);

						webView.loadUrl(Config.SERVER_URL+"/android/terms");
						alert.show();
					}
				});
			}

			@JavascriptInterface
			public void showLicenses() {
				runOnUiThread(new Runnable() {
					@Override
					public void run() {
						AlertDialog.Builder alert = new AlertDialog.Builder(WebViewActivity.this);
						//alert.setTitle(R.string.terms_and_conditions);
						alert.setNegativeButton(R.string.close, new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int which) {}
						});

						View view = getLayoutInflater().inflate(R.layout.dialog_webview, null);
						final WebView webView = (WebView) view.findViewById(R.id.webview);
						final ProgressBar progress = (ProgressBar) view.findViewById(R.id.progress);
						webView.setWebViewClient(new WebViewClient() {
							@Override
							public void onPageFinished(WebView view, String url) {
								webView.setVisibility(View.VISIBLE);
								progress.setVisibility(View.INVISIBLE);
							}
						});
						alert.setView(view);

						webView.loadUrl(Config.SERVER_URL+"/android/softwarelicenses");
						alert.show();
					}
				});
			}

			@JavascriptInterface
			public void recommendVenue() {
				runOnUiThread(new Runnable() {
					@Override
					public void run() {
						Intent intent = new Intent(WebViewActivity.this, WebViewActivity.class);
						intent.putExtra(WebViewActivity.EXTRA_URL, Config.SERVER_URL+"/android/recommend");
						intent.putExtra(WebViewActivity.EXTRA_TITLE, getResources().getText(R.string.recommend_a_venue).toString());
						startActivity(intent);
					}
				});
			}
		}, "app");
		WebSettings settings = mWebView.getSettings();
		settings.setJavaScriptEnabled(true);
		
		mWebView.setWebViewClient(new WebViewClient() {
			// Add authorization header to webview
			@Override
			public boolean shouldOverrideUrlLoading (WebView view, String url) {
				if (url.startsWith(Config.SERVER_URL)) {
					// Internal link
					mWebView.loadUrl(url, getAdditionalHeaders());
					return true;
				} else {
					// External link, open browser
					Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
					startActivity(intent);
					return true;
				}
			}
			
			// Ability to run code after page loaded
			@Override
			public void onPageFinished(WebView view, String url) {
				WebViewActivity.this.onPageFinished(view, url);
			}
		});
		
		// Use webview progress for activity progress bar
		final Activity activity = this;
		mWebView.setWebChromeClient(new WebChromeClient() {
			public void onProgressChanged(WebView view, int progress) {
				activity.setProgress(progress * 100);
			}
		});
	}
	
	@Override
    public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == android.R.id.home) {
			finish();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	protected Map<String, String> getAdditionalHeaders() {
		HashMap<String, String> headers = new HashMap<String, String>();
		ConsumerManager consumer = ((ZervedApp) getApplication()).getConsumer();
		if (consumer.mAccessToken != null && !consumer.mAccessToken.equals("")) {
			try {
				headers.put("Authorization", "Basic "+Base64.encodeToString(consumer.mAccessToken.getBytes("UTF-8"), Base64.NO_WRAP));
			} catch (UnsupportedEncodingException e) {
				Log.e(TAG, e.getMessage());
			}
		}
		return headers;
	}
	
	protected void onPageFinished(WebView view, String url) {
		
	}
}
