package com.zervedapp.zerved;

import java.util.ArrayList;
import java.util.Calendar;

import org.json.JSONException;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.app.TaskStackBuilder;
import android.app.LoaderManager.LoaderCallbacks;
import android.content.Loader;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.DatePicker;
import android.widget.DatePicker.OnDateChangedListener;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.TextView;

import com.zervedapp.zerved.loader.RESTLoader;
import com.zervedapp.zerved.loader.RESTLoader.RESTResponse;
import com.zervedapp.zerved.models.Event;
import com.zervedapp.zerved.models.MerchantLocation;

public class EventsActivity extends Activity implements LoaderCallbacks<RESTLoader.RESTResponse> {
	public static final String TAG = EventsActivity.class.getName();
	private static final int LOADER_EVENTS = 0x1;
	private static final String ARGS_URI    = "com.zervedapp.zerved.ARGS_URI";
    private static final String ARGS_PARAMS = "com.zervedapp.zerved.ARGS_PARAMS";
	private MerchantLocation mLocation;
	private EventAdapter mAdapter;
	private int year = Calendar.getInstance().get(Calendar.YEAR), month =  Calendar.getInstance().get(Calendar.MONTH), day =  Calendar.getInstance().get(Calendar.DAY_OF_MONTH);
	
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
        setContentView(R.layout.events);
        setProgressBarIndeterminateVisibility(false);
        final ActionBar actionBar = getActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        
        // Get location and bar
        Intent intent = getIntent();

        String locationJson = intent.getStringExtra(EventListFragment.EXTRA_LOCATION);
        try {
        	mLocation = new MerchantLocation(locationJson);
		} catch (JSONException e) {
			Log.e(TAG, "Failed to parse JSON.", e);
		}
        Log.i(TAG, "Location: " + mLocation);        

        setTitle(String.format(getResources().getString(R.string.events_at_x), mLocation.getName()));
		
        if (getLoaderManager().getLoader(LOADER_EVENTS) != null) {
        	getLoaderManager().initLoader(LOADER_EVENTS, null, this);
        }
        
        reload();
	}
	
	public void reload() {
		
 		LayoutInflater inflater = LayoutInflater.from(this);
		View view = inflater.inflate(R.layout.events, null);       
		setContentView(view);

		DatePicker datePicker = (DatePicker)  view.findViewById(R.id.datePicker1);
		if(android.os.Build.VERSION.SDK_INT >= 11) {
			datePicker.setCalendarViewShown(false);
		}
		datePicker.init(this.year, this.month, this.day, new OnDateChangedListener() {
			@Override
			public void onDateChanged(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
				startLoader(year, monthOfYear,dayOfMonth);
				
			}
		});
		
		TextView noEvents = (TextView) view.findViewById(R.id.no_events);
		noEvents.setVisibility(View.VISIBLE);
		setProgressBarIndeterminateVisibility(false);
		if(mAdapter != null){

	        final SeparatedListAdapter adapter = new SeparatedListAdapter(this);    
	        ListView list = (ListView)  view.findViewById(R.id.events_listview);   
	        adapter.addSection(getResources().getString(R.string.events), mAdapter);
	        list.setAdapter(adapter);  
	        list.setOnItemClickListener(new OnItemClickListener() {
	        	@Override
				public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
					Object o = adapter.getItem(position);
			    	Event e = (Event)o;
			    	((ZervedApp) getApplication()).mEvent = e ;
			    	Intent intent = new Intent(getApplicationContext(), EventActivity.class);
			    	intent.putExtra(EventActivity.EXTRA_EVENT, e.toJson() );
			        startActivity(intent);    	
	        	}
	        });
	        Utility.setListViewHeightBasedOnChildren(list);

	        if(mAdapter.getCount() > 0)
	        	noEvents.setVisibility(View.GONE);
	        
	        
		} else {
			startLoader(datePicker.getYear(), datePicker.getMonth(), datePicker.getDayOfMonth());
     	}
	}

	
	public void startLoader(int year, int month, int day){
		setProgressBarIndeterminateVisibility(true);
		this.year = year;
		this.month = month;
		this.day = day;
    	Bundle args = new Bundle();
    	args.putParcelable(ARGS_URI, Uri.parse(Config.API_URL + "locations/" + mLocation.getKey() + "/events/" +  year + "/" + (month+1) + "/"+ day));
    	args.putParcelable(ARGS_PARAMS, new Bundle());
    	getLoaderManager().restartLoader(LOADER_EVENTS, args, this);
	}

	
    @Override
    public void onResume() {
    	super.onResume();
    }
	
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
    	return true;
    }
    
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
    	return true;
    }
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // This is called when the Home (Up) button is pressed in the action bar.
                // Create a simple intent that starts the hierarchical parent activity and
                // use NavUtils in the Support Package to ensure proper handling of Up.
                Intent upIntent = new Intent(this, LocationActivity.class);
                if (shouldUpRecreateTask(upIntent)) {
                    // This activity is not part of the application's task, so create a new task
                    // with a synthesized back stack.
                    TaskStackBuilder.create(this)
                            // If there are ancestor activities, they should be added here.
                            .addNextIntent(upIntent)
                            .startActivities();
                    finish();
                } else {
                    // This activity is part of the application's task, so simply
                    // navigate up to the hierarchical parent activity.
                    //NavUtils.navigateUpTo(this, upIntent);
                	// navigateUpTo recreates the activity which will revert to first tab - instead we simply finish this activity to go back without recreating the parent
                	finish();
                }
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

	public MerchantLocation getLocation() {
		return mLocation;
	}

	@Override
	public Loader<RESTResponse> onCreateLoader(int id, Bundle args) {
		  Uri    action = args.getParcelable(ARGS_URI);
          Bundle params = args.getParcelable(ARGS_PARAMS);
          return new RESTLoader(this, RESTLoader.HTTPVerb.GET, action, params, "");
	}

	@Override
	public void onLoadFinished(Loader<RESTResponse> arg0, RESTResponse data) {
		int    code = data.getCode();
        String json = data.getData();
    	if (code == 200) {
        	ArrayList<Event> events = Event.initList(json);
			mAdapter = new EventAdapter(this, R.layout.event_list_item);
			for (Event event: events) {
				mAdapter.add(event);
			}
			reload();
    	}
    	else {
    	}        	
	}

	@Override
	public void onLoaderReset(Loader<RESTResponse> arg0) {
	}
}
