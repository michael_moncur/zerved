package com.zervedapp.zerved;

import java.util.ArrayList;

import org.json.JSONException;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.TaskStackBuilder;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;

import com.zervedapp.zerved.models.Country;
import com.zervedapp.zerved.models.MerchantLocation;

public class LocationsAlphabeticActivity extends Activity {
	public static final String TAG = LocationsAlphabeticActivity.class.getName();
	public static final String EXTRA_COUNTRY = "com.zervedapp.zerved.COUNTRY";
	private Country mCountry;
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment);
        
        final ActionBar actionBar = getActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        
        // Get location and bar
        Intent intent = getIntent();
        String countryJson = intent.getStringExtra(EXTRA_COUNTRY);
		try {
			mCountry = new Country(countryJson);
			actionBar.setTitle(mCountry.toString());
		} catch (JSONException e) {
			Log.e(TAG, "Failed to parse JSON.", e);
		}
        
		// Create category list fragment
        FragmentManager fm = getFragmentManager();
        Fragment fragment = new LocationListFragment();
        Bundle args = new Bundle();
        args.putString(LocationListFragment.EXTRA_COUNTRY_CODE, mCountry.getStringProperty("country_code"));
        fragment.setArguments(args);
		FragmentTransaction ft = fm.beginTransaction();
		ft.add(R.id.fragment_content, fragment);
		ft.commit();
	}
	
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // This is called when the Home (Up) button is pressed in the action bar.
                // Create a simple intent that starts the hierarchical parent activity and
                // use NavUtils in the Support Package to ensure proper handling of Up.
                Intent upIntent = new Intent(this, LocationActivity.class);
                if (shouldUpRecreateTask(upIntent)) {
                    // This activity is not part of the application's task, so create a new task
                    // with a synthesized back stack.
                    TaskStackBuilder.create(this)
                            // If there are ancestor activities, they should be added here.
                            .addNextIntent(upIntent)
                            .startActivities();
                    finish();
                } else {
                	finish();
                }
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
	
	public static class LocationListFragment extends RESTListFragment {
		public static final String EXTRA_COUNTRY_CODE = "com.zervedapp.zerved.COUNTRY_CODE";
		
		private LocationAdapter mAdapter;
		ArrayList<MerchantLocation> mMerchants;
		
		protected static String URI = Config.API_URL+"locations/country/%s";
		private static final int LOCATION_EXPIRATION = 1000 * 60 * 5; // 5 minutes
		private String mCountryCode;
		
		@Override
		public void onActivityCreated(Bundle savedInstanceState) {
			super.onActivityCreated(savedInstanceState);
			
			Bundle args = getArguments();
			mCountryCode = args.getString(EXTRA_COUNTRY_CODE);
			mAdapter = new LocationAdapter(getActivity(), R.layout.merchant);
	        this.setListAdapter(mAdapter);
	        
	        reload();
		}
		
		@Override
		public String getUri() {
			return String.format(URI, mCountryCode);
		}
		
		@Override
		public void reload() {
			// Get last known location, if it is recent
			LocationManager locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
			Location lastLocation = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
			if (lastLocation != null && (System.currentTimeMillis() - lastLocation.getTime()) < LOCATION_EXPIRATION) {
				mAdapter.location = lastLocation;
			} else {
				mAdapter.location = null;
			}
			super.reload();
		}
		
		@Override
		public void onJsonLoaded(String json) {
			mMerchants = MerchantLocation.initList(json);
            
            // Load our list adapter with our models.
            mAdapter.clear();
            // Add locations to list
            for (MerchantLocation merchant : mMerchants) {
            	mAdapter.add(merchant);
            }
		}
		
		@Override
	    public void onListItemClick(ListView l, View v, int position, long id) {
			// Select venue
			((ZervedApp)getActivity().getApplication()).mLocation = mMerchants.get(position);
			Intent intent = new Intent(getActivity(), LocationActivity.class);
			intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
	    	startActivity(intent);
		}
	}
}
