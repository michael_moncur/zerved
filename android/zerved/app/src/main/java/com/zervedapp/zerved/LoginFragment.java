package com.zervedapp.zerved;

import com.zervedapp.zerved.models.ConsumerManager;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.app.DialogFragment;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager.LayoutParams;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.TextView.OnEditorActionListener;

public class LoginFragment extends DialogFragment {
	public int mLoginTitle = R.string.login_title;
	public int mCreateAccountTitle = R.string.create_account_title;
    
    protected ProgressDialog mProgress;
    LoginListener mListener;
    
    public interface LoginListener {
    	public void onLoginWithEmailAndPassword(String email, String password);
    	public void onLoginCancelled();
    	public void onRequestNewAccount();
    	public void onRequestResetPasswordForEmail(String email);
    }
    
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        ConsumerManager consumer = ((ZervedApp) activity.getApplication()).getConsumer();
        try {
            mListener = (LoginListener) consumer;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement LoginListener");
        }
    }
	
	@Override
	public View onCreateView(LayoutInflater infl, ViewGroup container, Bundle savedInstanceState) {
	    // Get the layout inflater
	    LayoutInflater inflater = getActivity().getLayoutInflater();
	    
	    // Inflate the layout for the dialog
	    // Pass null as the parent view because its going in the dialog layout
	    View view = inflater.inflate(R.layout.dialog_login, null);
	    
	    // Setup tabs
	    TabHost tabs = (TabHost) view.findViewById(R.id.login_tabs);
	    tabs.setup();
	    TabHost.TabSpec tab1 = tabs.newTabSpec("tab_login");
	    tab1.setContent(R.id.tab_login);
	    tab1.setIndicator(getResources().getText(R.string.login));
	    tabs.addTab(tab1);
	    TabHost.TabSpec tab2 = tabs.newTabSpec("tab_create");
	    tab2.setContent(R.id.tab_create);
	    tab2.setIndicator(getResources().getText(R.string.create_account));
	    tabs.addTab(tab2);
	    
	    // Setup labels
	    ((TextView) view.findViewById(R.id.login_title)).setText(mLoginTitle);
	    ((TextView) view.findViewById(R.id.create_account_title)).setText(mCreateAccountTitle);
	    
	    // Fill interface with values
	    SharedPreferences settings = getActivity().getSharedPreferences("ZervedSettings", 0);
	    String email = settings.getString("email", "");
	    EditText loginPassword = (EditText) view.findViewById(R.id.login_password);
	    if (email.equals("")) {
	    	/*EditText createEmail = (EditText) view.findViewById(R.id.create_account_email);
	    	tabs.setCurrentTabByTag("tab_create");
	    	createEmail.requestFocus();*/
	    } else {
	    	EditText loginEmail = (EditText) view.findViewById(R.id.login_email);
	    	loginEmail.setText(settings.getString("email", ""));
	    	loginPassword.requestFocus();
	    }
	    
	    // Keyboard actions
	    loginPassword.setOnEditorActionListener(new OnEditorActionListener() {
			@Override
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
			    if (event != null && event.getAction() != KeyEvent.ACTION_DOWN)
			        return false;
				if (event == null || actionId == EditorInfo.IME_ACTION_GO || event.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
					LoginFragment.this.loginClick(v);
					return true;
				}
				return false;
			}
		});
	    
	    getDialog().getWindow().setSoftInputMode(LayoutParams.SOFT_INPUT_STATE_VISIBLE);
	    
	    return view;
	}
	
    public void loginClick(View view) {
    	View fragmentView = getView();
    	EditText email = (EditText) fragmentView.findViewById(R.id.login_email);
		EditText password = (EditText) fragmentView.findViewById(R.id.login_password);
		
		if (email.getText().toString().equals("") || password.getText().toString().equals("")) {
			Toast toast = Toast.makeText(getActivity(), R.string.email_password_required, Toast.LENGTH_SHORT);
        	toast.setGravity(Gravity.CENTER, 0, 0);
        	toast.show();
        	return;
		}
		
		SharedPreferences settings = getActivity().getSharedPreferences("ZervedSettings", 0);
		SharedPreferences.Editor editor = settings.edit();
		editor.putString("email", email.getText().toString());
		editor.commit();
		
		mListener.onLoginWithEmailAndPassword(email.getText().toString(), password.getText().toString());
    }
    
    public void createAccountClick(View view) {
		mListener.onRequestNewAccount();
    }
    
    public void cancelClick(View view) {
    	dismiss();
    	mListener.onLoginCancelled();
    }
    
    public void forgotPassword(View view) {
    	View fragmentView = getView();
    	EditText email = (EditText) fragmentView.findViewById(R.id.login_email);
    	final String emailAddress = email.getText().toString();
    	if (emailAddress.equals("")) {
    		Toast.makeText(getActivity(), R.string.invalid_email, Toast.LENGTH_SHORT).show();
    	} else {
	    	AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
	    	builder.setMessage(String.format(getResources().getString(R.string.send_password_reset_link_to_x), emailAddress))
	    		.setTitle(R.string.reset_password);
	    	builder.setPositiveButton(R.string.send_reset_link, new DialogInterface.OnClickListener() {
	    		public void onClick(DialogInterface dialog, int id) {
	    			mListener.onRequestResetPasswordForEmail(emailAddress);
	    		}
	    	});
	    	builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
	    		public void onClick(DialogInterface dialog, int id) {
	                // Cancelled
	    		}
	        });
	    	AlertDialog dialog = builder.create();
	    	dialog.show();
    	}
    }
}
