package com.zervedapp.zerved.models;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONTokener;

import android.util.Log;

public class Event extends JSONModel {
	private static final String TAG = Event.class.getName();
	
	private ArrayList<Bar> mBars = new ArrayList<Bar>();
	
	public Event(String json) throws JSONException {
		super(json);
		
		if(has("bars"))
			mBars = Bar.initList(getString("bars"));	
	}
	
	public static ArrayList<Event> initList(String json) {
        ArrayList<Event> objectList = new ArrayList<Event>();
        try {
        	JSONArray objects = (JSONArray) new JSONTokener(json).nextValue();
            for (int i = 0; i < objects.length(); i++) {
            	Event object = new Event(objects.getString(i));
            	objectList.add(object);
            }            
        }
        catch (JSONException e) {
            Log.e(TAG, "Failed to parse JSON.", e);
        }        
        return objectList;
	}
	
	public String getName() {
		return getStringProperty("name");
	}
	
	public String getTimeLabel() {
		return getStringProperty("time_label");
	}
	
	public String getDateStartFormatted(){
		return getStringProperty("date_start_formatted");
	}

	
	public String getDateOnlyStartFormatted(){
		return getStringProperty("date_start_date_formatted");
	}
	
	public String getTimeStartFormatted(){
		return getStringProperty("date_start_time_formatted");
	}
	
	public String getDateDeliveryFormatted(){
		return getStringProperty("date_delivery_formatted");
	}
	
	public String getTimeDeliveryFormatted(){
		return getStringProperty("date_delivery_time_formatted");
	}
	
	public boolean isOpenForOrders() {
		return getBooleanProperty("open_for_orders");
	}
	
	public String toString() {
		return this.getName();
	}
	
	public ArrayList<Bar> getBars(){
		return mBars;
	}
	
	public int getNumberOfbars(){
		return mBars.size();
	}
	
	public boolean hasBars(){
		return getNumberOfbars()>0;
	}
	public ArrayList<MenuCard> getMenus() {
		ArrayList<MenuCard> menus = new ArrayList<MenuCard>();
		for(Bar b: mBars){
			for(MenuCard m : b.getMenus()){
				if(!menus.contains(m)){
					menus.add(m);
				}
			}
		}
        return menus;
	}
}
