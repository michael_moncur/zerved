package com.zervedapp.zerved;

public class Config {
    public final static String SERVER_URL = BuildConfig.SERVER_URL;
    public final static String API_URL = SERVER_URL+"/api/v2/";
    public static final String EPAY_MERCHANT_NUMBER = BuildConfig.EPAY_MERCHANT_NUMBER;
    public static final String EPAY_MD5_HASH_KEY = "b80d9f7t83d45f"; // Note: As the app binary can be extracted and this string can be read in clear text, we cannot base the security on the MD5 hash key alone.
}