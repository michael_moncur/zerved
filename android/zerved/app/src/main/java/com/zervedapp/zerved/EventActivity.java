package com.zervedapp.zerved;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.app.TaskStackBuilder;
import android.app.LoaderManager.LoaderCallbacks;
import android.content.Loader;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

import com.zervedapp.zerved.loader.RESTLoader;
import com.zervedapp.zerved.loader.RESTLoader.RESTResponse;
import com.zervedapp.zerved.models.Bar;
import com.zervedapp.zerved.models.Event;
import com.zervedapp.zerved.models.MenuCard;
import com.zervedapp.zerved.models.Category;
import com.zervedapp.zerved.models.MerchantLocation;

public class EventActivity extends Activity implements LoaderCallbacks<RESTLoader.RESTResponse>{
	
	private static final String TAG = EventActivity.class.getName();	
	private static final int LOADER_EVENT = 0x1;
	private static final String ARGS_URI    = "com.zervedapp.zerved.ARGS_URI";
    private static final String ARGS_PARAMS = "com.zervedapp.zerved.ARGS_PARAMS";
    public static final String EXTRA_EVENT = "com.zervedapp.zerved.EXTRA_EVENT";
    private static String URI_EVENT = Config.API_URL+"events/";
    
    
	private Event mEvent;
	private MerchantLocation mLocation;

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

        getActionBar().setDisplayHomeAsUpEnabled(true);	
        
		setContentView(R.layout.event);
		
		mLocation = ((ZervedApp) getApplication()).mLocation;
		
        Intent intent = getIntent();
        String eventJson = intent.getStringExtra(EventActivity.EXTRA_EVENT);
        try {
        	mEvent = new Event(eventJson);
		} catch (JSONException e) {
			Log.e(TAG, "Failed to parse JSON.", e);
		}
		
        if(mEvent != null) {
        	setTitle(mEvent.getName());
        	// The event instance doesn't contain the bars/menus/categories yet
        	Bundle args = new Bundle();
	    	args.putParcelable(ARGS_URI, Uri.parse(URI_EVENT+mEvent.getKey()));
	    	Log.i("URI", Uri.parse(URI_EVENT+mEvent.getKey()).toString());
	    	args.putParcelable(ARGS_PARAMS, new Bundle());
	    	if(getLoaderManager().getLoader(LOADER_EVENT) == null){
	    		getLoaderManager().initLoader(LOADER_EVENT, args, EventActivity.this);
	    	}
        }
		
	}
	
	public void onResume() {
		super.onResume();
		reload();
	}
	
	public void onPause() {
		super.onPause();
	}
	
	public void reload() {	
		setTitle(mEvent.getName());	
		
		LayoutInflater inflater = LayoutInflater.from(this);
		View view = inflater.inflate(R.layout.event, null);       
		setContentView(view);
		
		((TextView) findViewById(R.id.event_name)).setText(mEvent.getName() + " " + mEvent.getTimeStartFormatted());
		((TextView) findViewById(R.id.event_date_start_formatted)).setText(mEvent.getDateOnlyStartFormatted());
		((TextView) findViewById(R.id.event_time_label)).setText(R.string.serving_time);
		((TextView) findViewById(R.id.event_date_delivery_time_formatted)).setText(mEvent.getTimeDeliveryFormatted() + " (" + mEvent.getTimeLabel() + ")");
	

		
        final SeparatedListAdapter adapter = new SeparatedListAdapter(this);    
        if(mEvent.getNumberOfbars() > 1){
        	adapter.addSection(getResources().getString(R.string.bars),  new ArrayAdapter<Bar>(this,R.layout.item_label_list, mEvent.getBars()));  
        }
        else if(mEvent.getNumberOfbars() == 1){
        	if(mEvent.getBars().get(0).getMenus().size() == 1){
        		MenuCard menu = mEvent.getBars().get(0).getMenus().get(0);
        		adapter.addSection(getResources().getString(R.string.categories),  new ArrayAdapter<Category>(this,R.layout.item_label_list, menu.getCategories()));  
        	}
        	else {
        		adapter.addSection(getResources().getString(R.string.menu),  new ArrayAdapter<MenuCard>(this,R.layout.item_label_list, mEvent.getBars().get(0).getMenus()));  
        	}
        }
       
        ListView list = (ListView)  view.findViewById(R.id.event_listview);   
        list.setAdapter(adapter);  
        list.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
				Object o = adapter.getItem(position);					
				// Bar
				if(o instanceof Bar){	
		    		Bar bar = (Bar)o;
		    		int nb_menus = bar.getMenus().size();
		    		// If only one menu the bar, go directly to the categories
		    		if(nb_menus == 1) {
		    			if(bar.getMenus().get(0).getCategories().size() == 1){
		    				startProductsActivity(bar, bar.getMenus().get(0), bar.getMenus().get(0).getCategories().get(0), position);
		    			}
		    			else {
			    			Intent intent = new Intent(getApplicationContext(), CategoriesActivity.class);
			    	    	intent.putExtra(CategoryListFragment.EXTRA_LOCATION, mLocation.toJson());
			    	    	intent.putExtra(CategoryListFragment.EXTRA_BAR, bar.toJson());
			    	    	intent.putExtra(CategoryListFragment.EXTRA_GROUP, "menu");
			    	    	intent.putExtra(CategoryListFragment.ARG_GROUP, "menu");
			    	    	intent.putExtra(CategoryListFragment.EXTRA_SELECTED_INDEX, position);
			    	    	intent.putExtra(CategoryListFragment.EXTRA_MENU, bar.getMenus().get(0).toJson());
			    	    	intent.putExtra(CategoryListFragment.ARG_MENU, bar.getMenus().get(0).toJson());
			    	        startActivity(intent);
		    			}
		    		}
		    		// Otherwise show the menus list
		    		else if (nb_menus > 1) {
			        	Intent intent = new Intent(getApplicationContext(), MenuCardsActivity.class);
			        	intent.putExtra(MenuCardListFragment.EXTRA_BAR, bar.toJson());
			        	intent.putExtra(MenuCardListFragment.EXTRA_LOCATION, mLocation.toJson());
			            startActivity(intent);
		    		}							
				}
				else if(o instanceof MenuCard){
					MenuCard menu = (MenuCard)o;
					if(menu.getCategories().size() == 1){
	    				startProductsActivity(mEvent.getBars().get(0), menu, menu.getCategories().get(0), position);
	    			}
	    			else {
		    			Intent intent = new Intent(getApplicationContext(), CategoriesActivity.class);
		    	    	intent.putExtra(CategoryListFragment.EXTRA_LOCATION, mLocation.toJson());
		    	    	intent.putExtra(CategoryListFragment.EXTRA_BAR, mEvent.getBars().get(0).toJson());
		    	    	intent.putExtra(CategoryListFragment.EXTRA_GROUP, "menu");
		    	    	intent.putExtra(CategoryListFragment.ARG_GROUP, "menu");
		    	    	intent.putExtra(CategoryListFragment.EXTRA_SELECTED_INDEX, position);
		    	    	intent.putExtra(CategoryListFragment.EXTRA_MENU, menu.toJson());
		    	    	intent.putExtra(CategoryListFragment.ARG_MENU, menu.toJson());
		    	        startActivity(intent);
	    			}
				}
				else if(o instanceof Category){
					startProductsActivity(mEvent.getBars().get(0), mEvent.getBars().get(0).getMenus().get(0), (Category)o, position);
				}
			}
		});
        Utility.setListViewHeightBasedOnChildren(list);
		
	}
	
	
	public void startProductsActivity(Bar bar, MenuCard menu, Category category, int position){
    	Intent intent = new Intent(getApplicationContext(), ProductsActivity.class);
    	intent.putExtra(CategoryListFragment.EXTRA_LOCATION, mLocation.toJson());
    	intent.putExtra(CategoryListFragment.EXTRA_BAR, bar.toJson());
    	intent.putExtra(CategoryListFragment.EXTRA_GROUP, "menu");		
    	intent.putExtra(CategoryListFragment.ARG_GROUP, "menu");	
    	intent.putExtra(CategoryListFragment.EXTRA_CATEGORIES, "[" + category.toJson().toString() + "]");
    	intent.putExtra(CategoryListFragment.EXTRA_SELECTED_INDEX, position);   	
    	intent.putExtra(CategoryListFragment.EXTRA_MENU, menu.toJson());
    	intent.putExtra(CategoryListFragment.ARG_MENU, menu.toJson());	
        startActivity(intent);
	}
	
	
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
    	return true;
    }
    
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
    	return true;
    }
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // This is called when the Home (Up) button is pressed in the action bar.
                // Create a simple intent that starts the hierarchical parent activity and
                // use NavUtils in the Support Package to ensure proper handling of Up.
                Intent upIntent = new Intent(this, LocationActivity.class);
                if (shouldUpRecreateTask(upIntent)) {
                    // This activity is not part of the application's task, so create a new task
                    // with a synthesized back stack.
                    TaskStackBuilder.create(this)
                            // If there are ancestor activities, they should be added here.
                            .addNextIntent(upIntent)
                            .startActivities();
                    finish();
                } else {
                    // This activity is part of the application's task, so simply
                    // navigate up to the hierarchical parent activity.
                    //NavUtils.navigateUpTo(this, upIntent);
                	// navigateUpTo recreates the activity which will revert to first tab - instead we simply finish this activity to go back without recreating the parent
                	finish();
                }
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
    
	@Override
	public Loader<RESTResponse> onCreateLoader(int id, Bundle args) {
        if (args != null && args.containsKey(ARGS_URI) && args.containsKey(ARGS_PARAMS)) {
            Uri    action = args.getParcelable(ARGS_URI);
            Bundle params = args.getParcelable(ARGS_PARAMS);            
            return new RESTLoader(this, RESTLoader.HTTPVerb.GET, action, params);
        }        
        return null;
	}

	@Override
	public void onLoadFinished(Loader<RESTResponse> loader, RESTResponse data) {
        int    code = data.getCode();
        String json = data.getData();

        if (code == 200 && !json.equals("")) {
        	try {
        		JSONObject object = (JSONObject) new JSONTokener(json).nextValue();
        		if (object.has("key")) {
        			this.mEvent = new Event(json);
                	((ZervedApp) getApplication()).mEvent = this.mEvent;
            		if(mEvent.getBars().size() == 0)
            			Toast.makeText(this, R.string.no_bars, Toast.LENGTH_LONG).show();
            		if(mEvent.getBars().size() == 1 && mEvent.getBars().get(0).getMenus().size() == 0)
            			Toast.makeText(this, R.string.no_menu, Toast.LENGTH_LONG).show();
        			reload();
        		} else {
        			
        		}
        	} catch (JSONException e) {
        		Log.e(TAG, "Failed to parse JSON.", e);
        	}
        }
        else {
            Toast.makeText(this, "Failed to load data.", Toast.LENGTH_SHORT).show();
        }
		
	}

	@Override
	public void onLoaderReset(Loader<RESTResponse> arg0) {}

}
