package com.zervedapp.zerved;

import java.util.ArrayList;

import org.json.JSONException;

import com.zervedapp.zerved.models.Bar;
import com.zervedapp.zerved.models.Cart;
import com.zervedapp.zerved.models.Category;
import com.zervedapp.zerved.models.Event;
import com.zervedapp.zerved.models.MenuCard;
import com.zervedapp.zerved.models.MerchantLocation;
import com.zervedapp.zerved.models.Product;

import android.animation.ValueAnimator;
import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.TaskStackBuilder;
import android.support.v13.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class ProductsActivity extends Activity {
	private static final String TAG = ProductsActivity.class.getName();
    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide fragments representing
     * each object in a collection. We use a {@link FragmentStatePagerAdapter}
     * derivative, which will destroy and re-create fragments as needed, saving and restoring their
     * state in the process. This is important to conserve memory and is a best practice when
     * allowing navigation between objects in a potentially large collection.
     */
    CategoriesPagerAdapter mCategoriesPagerAdapter;
    
    ArrayList<Category> mCategories;

    /**
     * The {@link ViewPager} that will display the object collection.
     */
    ViewPager mViewPager;
    
    MerchantLocation mLocation;
    Bar mBar;
    Event mEvent = null;
    MenuCard mMenu;
    String mGroup;

    
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.title_strip_pager);
        
        Intent intent = getIntent();
        mCategories = Category.initList(intent.getStringExtra(CategoryListFragment.EXTRA_CATEGORIES));
        int selected = intent.getIntExtra(CategoryListFragment.EXTRA_SELECTED_INDEX, 0);
        mGroup = intent.getStringExtra(CategoryListFragment.EXTRA_GROUP);

        // Set up action bar.
        final ActionBar actionBar = getActionBar();

        // Specify that the Home button should show an "Up" caret, indicating that touching the
        // button will take the user one step up in the application's hierarchy.
        actionBar.setDisplayHomeAsUpEnabled(true);
        
        try {
			mLocation = new MerchantLocation(intent.getStringExtra(CategoryListFragment.EXTRA_LOCATION));
			String barJson = intent.getStringExtra(CategoryListFragment.EXTRA_BAR);
			if (barJson != null) {
				mBar = new Bar(barJson);
			}
			String menuJson = intent.getStringExtra(CategoryListFragment.EXTRA_MENU);
			if (menuJson != null) {
				mMenu = new MenuCard(menuJson);
			}
			if (mGroup.equals("menu")) {
				actionBar.setTitle(mMenu.getName());
			} else if (mGroup.equals("entrance")) {
				actionBar.setTitle(mLocation.getName());
			}
	     
		    mEvent = ((ZervedApp) getApplication()).mEvent;
	        		
			
		} catch (JSONException e) {
			Log.e(TAG, "Failed to parse JSON.", e);
		}

        // Create an adapter that when requested, will return a fragment representing an object in
        // the collection.
        // 
        // ViewPager and its adapters use support library fragments, so we must use
        // getFragmentManager.
        mCategoriesPagerAdapter = new CategoriesPagerAdapter(getFragmentManager());
        mCategoriesPagerAdapter.mCategories = mCategories;
        mCategoriesPagerAdapter.mGroup = mGroup;
        mCategoriesPagerAdapter.mLocation = mLocation;
        mCategoriesPagerAdapter.mBar = mBar;
        mCategoriesPagerAdapter.mMenu = mMenu;

        // Set up the ViewPager, attaching the adapter.
        mViewPager = (ViewPager) findViewById(R.id.pager);
        mViewPager.setAdapter(mCategoriesPagerAdapter);
        mViewPager.setCurrentItem(selected);
    }
    
    @Override
    public void onResume() {
    	super.onResume();
    	// Refresh cart button
    	invalidateOptionsMenu();
    }
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
    	MenuInflater inflater = getMenuInflater();
    	inflater.inflate(R.menu.products, menu);
    	return true;
    }
    
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
    	for (int i=0; i<menu.size(); i++) {
    		MenuItem item = menu.getItem(i);
    		if (item.getItemId() == R.id.cart) {
    			// Update cart button to show number of items
    			Cart cart = ((ZervedApp) getApplication()).getCart(mLocation, mBar, mGroup, mEvent);
    			int s = cart.size();
    			View view;
    			if(s > 0){
	    			view = getLayoutInflater().inflate(R.layout.custom_cart_layout, null);
					final TextView cartBtnText = (TextView) view.findViewById(R.id.cart_btn_text);

					CategoriesActivity.getFlashAnimation(cartBtnText).start();

					cartBtnText.setOnClickListener(new OnClickListener() {
						@Override
						public void onClick(View v) {
							startActivity(getCartIntent());
						}
					});
    			}
    			else {
    				view = getLayoutInflater().inflate(R.layout.custom_cart_layout_disabled, null);
    			}
    			((TextView) view.findViewById(R.id.cart_btn_text)).setText(String.format(getResources().getString(R.string.cart_x), cart.size()));
    			item.setActionView(view);
    		}
    	}
    	return true;
    }
    
    public Intent getCartIntent() {
    	Intent intent = new Intent(this, CartActivity.class);
    	intent.putExtra(CategoryListFragment.EXTRA_LOCATION, mLocation.toJson());
    	if (mBar != null) {
    		intent.putExtra(CategoryListFragment.EXTRA_BAR, mBar.toJson());
    	}
    	intent.putExtra(CategoryListFragment.EXTRA_GROUP, mGroup);
    	return intent;
    }
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // This is called when the Home (Up) button is pressed in the action bar.
                // Create a simple intent that starts the hierarchical parent activity and
                // use NavUtils in the Support Package to ensure proper handling of Up.
                Intent upIntent = new Intent(this, LocationActivity.class);
                if (shouldUpRecreateTask(upIntent)) {
                    // This activity is not part of the application's task, so create a new task
                    // with a synthesized back stack.
                    TaskStackBuilder.create(this)
                            // If there are ancestor activities, they should be added here.
                            .addNextIntent(upIntent)
                            .startActivities();
                    finish();
                } else {
                    // This activity is part of the application's task, so simply
                    // navigate up to the hierarchical parent activity.
                    //NavUtils.navigateUpTo(this, upIntent);
                	// navigateUpTo recreates the activity which will revert to first tab - instead we simply finish this activity to go back without recreating the parent
                	finish();
                }
                return true;
            case R.id.cart:
            	Intent intent = new Intent(this, CartActivity.class);
            	intent.putExtra(CategoryListFragment.EXTRA_LOCATION, mLocation.toJson());
            	if (mBar != null) {
            		intent.putExtra(CategoryListFragment.EXTRA_BAR, mBar.toJson());
            	}
            	intent.putExtra(CategoryListFragment.EXTRA_GROUP, mGroup);
            	startActivity(intent);
            	return true;
        }
        return super.onOptionsItemSelected(item);
    }
    

    
    /**
     * A {@link FragmentStatePagerAdapter} that returns a fragment
     * representing an object in the collection.
     */
    public static class CategoriesPagerAdapter extends FragmentStatePagerAdapter {
    	ArrayList<Category> mCategories;
    	MerchantLocation mLocation;
    	Bar mBar;
    	MenuCard mMenu;
    	String mGroup;
    	
        public CategoriesPagerAdapter(FragmentManager fm) {
            super(fm);
        }
        
        /*public void setCategories(ArrayList<Category> categories) {
        	this.mCategories = categories;
        }*/

        @Override
        public Fragment getItem(int i) {
        	Fragment fragment = new ProductListFragment();
            Bundle args = new Bundle();
            String categoryKey = this.mCategories.get(i).getKey();
            args.putString("category_key", categoryKey);
            args.putString("group", mGroup);
            args.putString("location", mLocation.getKey());
            if (mBar != null) {
            	args.putString("bar", mBar.getKey());
                args.putString("menu", mMenu.getKey());
            }
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public int getCount() {
            // For this contrived example, we have a 100-object collection.
            return mCategories.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
        	return mCategories.get(position).toString();

        }    
	    
	    public static class ProductListFragment extends RESTListFragment {
			
			@SuppressWarnings("unused")
			private static final String TAG = ProductListFragment.class.getName();
			private ArrayAdapter<Product> mAdapter;
			
			protected static String LOCATION_URI = Config.API_URL+"locations/%s/categories/%s/products";
			protected static String BAR_URI = Config.API_URL+"bars/%s/menus/%s/categories/%s/products";
			private String mCategoryKey;
			private String mLocationKey;
	    	private String mBarKey;
	    	private String mMenuKey;
	    	private String mGroup;
			
			@Override
			public void onActivityCreated(Bundle savedInstanceState) {
				super.onActivityCreated(savedInstanceState);
				
		        mAdapter = new ProductAdapter(getActivity(), R.layout.product);
		        this.setListAdapter(mAdapter);
		        
		        Bundle fragmentArgs = this.getArguments();
		        mCategoryKey = fragmentArgs.getString("category_key");
		        mGroup = fragmentArgs.getString("group");
		        mLocationKey = fragmentArgs.getString("location");
		        if(mGroup.equals("menu")) {
			        mBarKey = fragmentArgs.getString("bar");
			        mMenuKey = fragmentArgs.getString("menu");
		        }
		        reload();
			}
			
			@Override
			public void onResume() {
				super.onResume();
				// Update view (cart items may have changed if we're going back from the cart)
				mAdapter.notifyDataSetChanged();
			}
			
			@Override
			public String getUri() {
				if (mGroup.equals("menu")) {
					return String.format(BAR_URI, mBarKey, mMenuKey, mCategoryKey);
				} else {
					return String.format(LOCATION_URI, mLocationKey, mCategoryKey);
				}
			}
			
			@Override
			public void onJsonLoaded(String json) {
				ArrayList<Product> products = Product.initList(json);
	            
	            // Load our list adapter with our models.
	            mAdapter.clear();
	            for (Product product : products) {
	            	mAdapter.add(product);
	            }
			}
		}
    }
           
}
