package com.zervedapp.zerved;

import java.io.InputStream;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

public class ImageFragment extends Fragment {
	private ImageView image;
	private String imageURL;
	private int height;

	public static ImageFragment create(int height, String url) {
		ImageFragment f = new ImageFragment();
		Bundle args = new Bundle();        
        args.putInt("height", height);     
        args.putString("imageURL", url);       
        f.setArguments(args);
		return f;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);		
		this.height = getArguments().getInt("height");
		this.imageURL = getArguments().getString("imageURL");		
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View root = inflater.inflate(R.layout.location_image, container, false);
		image = (ImageView) root.findViewById(R.id.location_image);
        image.getLayoutParams().height = height;
		new DownloadImageTask(image).execute(imageURL);                
		return root;
	}
	
	public void onResume(){
		super.onResume();
	}
	
	private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
	    	ImageView bmImage;

	    	public DownloadImageTask(ImageView bmImage) {
	    	    this.bmImage = bmImage;
	    	}

	    	protected Bitmap doInBackground(String... urls) {
	    	    String urldisplay = urls[0];
	    	    Bitmap mIcon11 = null;
	    	    try {
	    	        InputStream in = new java.net.URL(urldisplay).openStream();
	    	        mIcon11 = BitmapFactory.decodeStream(in);
	    	    } catch (Exception e) {
	    	        Log.e("Error", e.getMessage());
	    	        e.printStackTrace();
	    	    }
	    	    return mIcon11;
	    	}

	    	protected void onPostExecute(Bitmap result) {
	    	    bmImage.setImageBitmap(result);
	    	}
	    }

}