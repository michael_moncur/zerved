package com.zervedapp.zerved;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class LocationEventAdapter extends ArrayAdapter<String> {

	private String [] objects;

	public LocationEventAdapter(Context context, int textViewResourceId, String [] objects) {
		super(context, textViewResourceId, objects);
		this.objects = objects;
	}

	public View getView(int position, View convertView, ViewGroup parent){

		View v = convertView;
		if (v == null) {
			LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			v = inflater.inflate(R.layout.item_location_choice, null);
		}
		String s = objects[position];

		TextView title = (TextView) v.findViewById(R.id.title);
		TextView desc = (TextView) v.findViewById(R.id.desc);

		title.setText(s);
		if(s.equals(getContext().getResources().getString(R.string.order_for_now)))
			desc.setText(getContext().getResources().getString(R.string.order_for_now_subtitle));
		else {
			title.setTextColor(getContext().getResources().getColor(R.color.orange));
			desc.setText(getContext().getResources().getString(R.string.preorder_subtitle));
		}

		return v;
	}
}
