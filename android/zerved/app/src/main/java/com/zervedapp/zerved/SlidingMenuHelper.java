package com.zervedapp.zerved;

import android.app.Activity;

import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;

public class SlidingMenuHelper {
	public static SlidingMenu initSlidingMenu(Activity activity, String tag) {
		// Enable up button
		activity.getActionBar().setDisplayHomeAsUpEnabled(true);
		
		// Create menu
		SlidingMenu menu = new SlidingMenu(activity);
		menu.setTouchModeAbove(SlidingMenu.TOUCHMODE_FULLSCREEN);
		menu.setBehindOffsetRes(R.dimen.slidingmenu_offset);
		menu.setFadeDegree(0.35f);
		menu.attachToActivity(activity, SlidingMenu.SLIDING_WINDOW);
		menu.setMenu(R.layout.menu_frame);
		
		// Create list fragment for menu
		activity.getFragmentManager()
		.beginTransaction()
		.replace(R.id.menu_frame, new SlidingMenuFragment(menu, tag))
		.commit();
		
		return menu;
	}
}
