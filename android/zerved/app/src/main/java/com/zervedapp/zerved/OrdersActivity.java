package com.zervedapp.zerved;

import android.app.ActionBar;
import android.content.Context;
import android.os.Bundle;
import android.app.Fragment;
import android.support.v13.app.FragmentStatePagerAdapter;
import android.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.view.KeyEvent;
import android.view.MenuItem;

import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;
import com.zervedapp.zerved.models.ConsumerManager;

public class OrdersActivity extends AuthorizedActivity implements ActionBar.TabListener {
	@SuppressWarnings("unused")
	private static final String TAG = OrdersActivity.class.getName();
	
	private boolean mResumed = false;
	SlidingMenu mMenu;
	OrderListsPagerAdapter mOrderListsPagerAdapter;
    ViewPager mViewPager;
	
	public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        setContentView(R.layout.viewpager);
        setTitle(R.string.orders);
        
        // Setup action bar
        final ActionBar actionBar = getActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
        mMenu = SlidingMenuHelper.initSlidingMenu(this, "orders");
        
        // Setup view pager
        mOrderListsPagerAdapter = new OrderListsPagerAdapter(this);
        mViewPager = (ViewPager) findViewById(R.id.pager);
        mViewPager.setAdapter(mOrderListsPagerAdapter);
        mViewPager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                // Swiping left on first tab should open the menu, swiping in other sections should switch sections
            	switch (position) {
            	case 0:
            		mMenu.setTouchModeAbove(SlidingMenu.TOUCHMODE_FULLSCREEN);
            		break;
            	default:
					mMenu.setTouchModeAbove(SlidingMenu.TOUCHMODE_NONE);
					break;
            	}
            	actionBar.setSelectedNavigationItem(position);
            }
        });
        mMenu.setTouchModeAbove(SlidingMenu.TOUCHMODE_FULLSCREEN);
        
        // Add tabs
        for (int i = 0; i < mOrderListsPagerAdapter.getCount(); i++) {
        	actionBar.addTab(
        			actionBar.newTab()
                            .setText(mOrderListsPagerAdapter.getPageTitle(i))
                            .setTabListener(this));
        }
        
        reload();
	}
	
	@Override
	public void onResume() {
		super.onResume();
		// Reload every time the activity is displayed
		// except first time where it's reloaded by onCreate
		if (mResumed) {
			reload();
		} else {
			mResumed = true;
		}
	}
	
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
            	mMenu.toggle();
    	        return true;
        }
        return super.onOptionsItemSelected(item);
    }
    
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
	    if ( keyCode == KeyEvent.KEYCODE_MENU ) {
	    	mMenu.toggle();
	        return true;
	    }
	    return super.onKeyDown(keyCode, event);
	}
	
	@Override
	public void reload() {
		ConsumerManager consumer = getConsumer();
		if (consumer.mConsumerKey == null || consumer.mAccessToken == null) {
			// Not logged in
			login();
		} else {
			// Logged in - load list
			mOrderListsPagerAdapter.notifyDataSetChanged();
		}
	}
	
	@Override
	public void onAuthenticationFailed() {
		// Don't change view like in AuthorizedActivity, because listfragment will not work if the activity view is changed
		// Instead finish activity
		finish();
	}

	@Override
	public void onTabSelected(ActionBar.Tab tab, FragmentTransaction ft) {
		// When the given tab is selected, switch to the corresponding page in the ViewPager.
        mViewPager.setCurrentItem(tab.getPosition());
	}

	@Override
	public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction ft) { }

	@Override
	public void onTabReselected(ActionBar.Tab tab, FragmentTransaction ft) { }
	
	public static class OrderListsPagerAdapter extends FragmentStatePagerAdapter {
    	private Context mContext;
    	
        public OrderListsPagerAdapter(OrdersActivity activity) {
            super(activity.getFragmentManager());
            mContext = activity;
        }

        @Override
        public Fragment getItem(int i) {
        	Fragment fragment;
        	Bundle args;
        	fragment = new OrderListFragment();
        	args = new Bundle();
            args.putString(OrderListFragment.ARG_STATE, i==0 ? "open" : "closed");
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
        	switch (position) {
        		case 0:
        			return mContext.getResources().getText(R.string.open);
        		case 1:
        			return mContext.getResources().getText(R.string.closed);
        		default:
        			return "";
        	}
        }
        
        public int getItemPosition(Object item) {
        	return POSITION_NONE;
        }
    }
    
}
