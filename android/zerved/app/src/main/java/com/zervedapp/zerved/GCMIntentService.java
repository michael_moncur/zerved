package com.zervedapp.zerved;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.google.android.gcm.GCMBaseIntentService;
import com.zervedapp.zerved.models.Notifier;
import com.zervedapp.zerved.models.OrderNotification;

public class GCMIntentService extends GCMBaseIntentService {
	public static final String TAG = GCMIntentService.class.getName();
	public static final String SENDER_ID = "285169264817";

	public GCMIntentService() {
		super(SENDER_ID);
	}

	@Override
	protected void onError(Context context, String errorId) {
		Log.d(TAG, errorId);
	}

	@Override
	protected void onMessage(Context context, Intent intent) {
		OrderNotification orderNotification = new OrderNotification(intent);
		Notifier.notifyOrderEvent(context, orderNotification);
	}

	@Override
	protected void onRegistered(Context context, String regId) {
		
	}

	@Override
	protected void onUnregistered(Context context, String regId) {
		
	}

}
