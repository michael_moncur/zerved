package com.zervedapp.zerved;

import com.zervedapp.zerved.models.Event;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;


public class EventAdapter extends ArrayAdapter<Event>{
	@SuppressWarnings("unused")
	private static final String TAG = EventAdapter.class.getName();
	Context mContext;
	
	public EventAdapter(Context context, int textViewResourceId) {
		super(context, textViewResourceId);
		mContext = context;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View row;
		LayoutInflater inflater = LayoutInflater.from(getContext());
		if (null == convertView) {
			row = inflater.inflate(R.layout.event_list_item, null);
		} else {
			row = convertView;
		}
		
		final Event event = this.getItem(position);
		
		TextView name = (TextView) row.findViewById(R.id.event_name);
		TextView dateStartFormatted = (TextView) row.findViewById(R.id.event_date_start_formatted);
		TextView timeLabel = (TextView) row.findViewById(R.id.event_time_label);
		TextView deliveryTime = (TextView) row.findViewById(R.id.event_date_delivery_time_formatted);
		
		name.setText(event.getName() + " " + event.getTimeStartFormatted());
		dateStartFormatted.setText(event.getDateStartFormatted());
		timeLabel.setText("(" + event.getTimeLabel() + ")");
		deliveryTime.setText(getContext().getResources().getString(R.string.serving_time) + " " + event.getTimeDeliveryFormatted());
		
		if(getContext() instanceof EventsActivity){
			dateStartFormatted.setVisibility(View.GONE);
		}
		
		return row;
	}
}
