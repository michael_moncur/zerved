package com.zervedapp.zerved;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.zervedapp.zerved.models.Bar;
import com.zervedapp.zerved.models.MerchantLocation;

public class BarListFragment extends RESTListFragment {
	public static final String ARG_GROUP = "com.zervedapp.zerved.GROUP";
	public static final String EXTRA_LOCATION = "com.zervedapp.zerved.EXTRA_LOCATION";
	
	private BarsActivity mActivity;
	private ArrayAdapter<Bar> mAdapter;
	private String mBarsJson;
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		
		mActivity = (BarsActivity) getActivity();
        mAdapter = new ArrayAdapter<Bar>(getActivity(), R.layout.item_label_list);
        
        this.setListAdapter(mAdapter);
        
        setHasOptionsMenu(true);
        reload();
	}
	
	public int getCount(){
		return this.mAdapter == null ? 0 : this.mAdapter.getCount();
	}

	public ArrayAdapter<Bar> getAdapter(){
		return this.mAdapter;
	}
	
    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
    	BarsActivity activity = (BarsActivity) getActivity();
		MerchantLocation location = activity.getLocation();
		Bar bar = location.getBars().get(position);
		int nb_menus = bar.getMenus().size();
		// If only one menu the bar, go directly to the categories
		if(nb_menus == 1) {
	    	Intent intent = new Intent(getActivity(), CategoriesActivity.class);
	    	intent.putExtra(CategoryListFragment.EXTRA_LOCATION, location.toJson());
	    	intent.putExtra(CategoryListFragment.EXTRA_BAR, bar.toJson());
	    	intent.putExtra(CategoryListFragment.EXTRA_GROUP, "menu");
	    	intent.putExtra(CategoryListFragment.ARG_GROUP, "menu");
	    	intent.putExtra(CategoryListFragment.EXTRA_SELECTED_INDEX, position);
	    	intent.putExtra(CategoryListFragment.EXTRA_MENU, bar.getMenus().get(0).toJson());
	    	intent.putExtra(CategoryListFragment.ARG_MENU, bar.getMenus().get(0).toJson());
	        startActivity(intent);
		}
		// Otherwise show the menus list
		else if (nb_menus > 1) {
        	Intent intent = new Intent(getActivity(), MenuCardsActivity.class);
        	intent.putExtra(MenuCardListFragment.EXTRA_BAR, bar.toJson());
        	intent.putExtra(MenuCardListFragment.EXTRA_LOCATION, location.toJson());
            startActivity(intent);
		}
    }

	@Override
	public String getUri() {
		return Config.API_URL + "locations/" + ((BarsActivity) getActivity()).getLocation().getKey();
	}

	@Override
	public void onJsonLoaded(String json) {
		mBarsJson = json;			
        for (Bar bar: mActivity.getLocation().getBars()) {
        	mAdapter.add(bar);
        }	
	}
}