package com.zervedapp.zerved.models;

import com.zervedapp.zerved.OrderActivity;
import com.zervedapp.zerved.R;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;

public class Notifier {
	public static void notifyOrderEvent(Context context, OrderNotification orderNotification) {
		// Check that message is not already created
		SharedPreferences settings = context.getSharedPreferences("ZervedNotifications", 0);
		boolean notificationCreated = settings.getBoolean(orderNotification.getMessageId(), false);
		if (notificationCreated) {
			return;
		}
		
		// Build notification
		NotificationCompat.Builder mBuilder =
		        new NotificationCompat.Builder(context)
		        .setSmallIcon(R.drawable.ic_notification_zerved)
		        .setContentTitle(orderNotification.merchantName)
		        .setContentText(orderNotification.message)
		        .setTicker(orderNotification.message)
		        .setDefaults(Notification.DEFAULT_VIBRATE | Notification.DEFAULT_SOUND | Notification.DEFAULT_LIGHTS)
		        .setAutoCancel(true);
		
		// Link notification to order view
		Intent resultIntent = new Intent(context, OrderActivity.class);
		resultIntent.putExtra(OrderActivity.EXTRA_ORDER_KEY, orderNotification.orderKey);

		// The stack builder object will contain an artificial back stack for the
		// started Activity.
		TaskStackBuilder stackBuilder = TaskStackBuilder.from(context);
		stackBuilder.addParentStack(OrderActivity.class);
		
		// Adds the Intent that starts the Activity to the top of the stack
		stackBuilder.addNextIntent(resultIntent);
		PendingIntent resultPendingIntent =
		        stackBuilder.getPendingIntent(
		            0,
		            PendingIntent.FLAG_UPDATE_CURRENT
		        );
		mBuilder.setContentIntent(resultPendingIntent);
		
		// Launch notification
		NotificationManager mNotificationManager =
		    (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
		// id allows you to update the notification later on.
		mNotificationManager.notify(orderNotification.getCollapseId(), mBuilder.getNotification());
		
		// Remember message has been processed
		SharedPreferences.Editor editor = settings.edit();
		editor.putBoolean(orderNotification.getMessageId(), true);
		editor.commit();
	}
}
