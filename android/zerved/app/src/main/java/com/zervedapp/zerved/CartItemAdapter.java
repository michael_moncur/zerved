package com.zervedapp.zerved;

import java.util.ArrayList;
import com.zervedapp.zerved.models.Cart;
import com.zervedapp.zerved.models.Favourite;
import com.zervedapp.zerved.models.JSONModel;
import com.zervedapp.zerved.models.Product;
import com.zervedapp.zerved.models.ProductOption;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;


public class CartItemAdapter extends ArrayAdapter<Cart.Item>{
	@SuppressWarnings("unused")
	private static final String TAG = CartItemAdapter.class.getName();
	Context mContext;
	
	public CartItemAdapter(Context context, int textViewResourceId) {
		super(context, textViewResourceId);
		mContext = context;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View row;
		LayoutInflater inflater = LayoutInflater.from(getContext());
		if (null == convertView) {
			row = inflater.inflate(R.layout.cart_item, null);
		} else {
			row = convertView;
		}
		
		final Cart.Item cartItem = this.getItem(position);
		Product product = cartItem.getProduct();
		
		Favourite item = new Favourite(product, ((CartActivity)mContext).mBar, ((CartActivity)mContext).mLocation);
		item.setOptions(cartItem.getOptions());
		Favourite itemFavourite = null;
		boolean isFavourite = false;
		ArrayList<Favourite> favourites = ((CartActivity)mContext).mFavourites;
		for(Favourite f: favourites){
			if(f.equals(item)){
				isFavourite = true;
				itemFavourite = f;
				break;
			}
		}
		
		JSONModel quoteItem = cartItem.getQuoteItem();
		TextView name = (TextView) row.findViewById(R.id.item_name);
		TextView price = (TextView) row.findViewById(R.id.item_price);
		ImageButton favouriteButton = (ImageButton) row.findViewById(R.id.favouriteButton);
		name.setText(Integer.toString(cartItem.getQuantity()) + " x " + cartItem.getProduct().getName());
		if (quoteItem != null) {
			price.setText(quoteItem.getStringProperty("price_formatted"));
		} else {
			price.setText("--");
		}
		
		ProductViewHelper helper = new ProductViewHelper(getContext());
		
		Spinner quantity = (Spinner) row.findViewById(R.id.quantity);
		quantity.setAdapter(helper.getQuantityAdapter(cartItem.getQuantity()));
		quantity.setSelection(cartItem.getQuantity()-1);
		// Update cart when selecting quantity
		quantity.setOnItemSelectedListener(new OnItemSelectedListener() {
			boolean enabled = false;
			@Override
			public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
				// A bug in Android causes onItemSelected to be called when the spinner is first displayed, so we simply ignore the first call
				if (!enabled) {
					enabled = true;
					return;
				}
				View productAdditional = (View) parentView.getParent();
				((CartActivity)mContext).updateItem(productAdditional);
			}
			@Override
			public void onNothingSelected(AdapterView<?> parentView) {
				
			}
		});
		
		if(isFavourite){ 
			favouriteButton.setBackgroundResource(R.drawable.ic_is_favourite);
			favouriteButton.setImageResource(R.drawable.ic_is_favourite);
		}
		else {
			favouriteButton.setBackgroundResource(R.drawable.ic_is_not_favourite);
			favouriteButton.setImageResource(R.drawable.ic_is_not_favourite);
		}
		favouriteButton.setEnabled(true);
		if(((CartActivity)mContext).mEvent != null){
			favouriteButton.setVisibility(View.GONE);
			RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams)name.getLayoutParams();
			params.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
			name.setLayoutParams(params); 
		}
		
		final boolean isf = isFavourite;
		final String key = (itemFavourite != null) ? itemFavourite.getKey() : "";
		favouriteButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				ImageButton b = (ImageButton) v;
				b.setEnabled(false);
				if(!isf){
					b.setBackgroundResource(R.drawable.ic_is_favourite);
					((CartActivity)mContext).addFavourite(cartItem);
				}
				else {
					b.setBackgroundResource(R.drawable.ic_is_not_favourite);
					((CartActivity)mContext).removeFavourite(key);
				}
			}
		});
		
		View productAdditional = row.findViewById(R.id.additional);
		productAdditional.setTag(cartItem);
		
		LinearLayout options = (LinearLayout) row.findViewById(R.id.options);
		options.removeAllViews();
		for (ProductOption option: product.getProductOptions()) {
			if (option.getSelectionType().equals("single")) {
				Spinner optionSpinner = helper.getProductOptionSpinner(inflater, option, cartItem.getValuesForOption(option.getKey()));
				// Update cart when selecting option
				optionSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {
					boolean enabled = false;
					@Override
					public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
						// A bug in Android causes onItemSelected to be called when the spinner is first displayed, so we simply ignore the first call
						if (!enabled) {
							enabled = true;
							return;
						}
						View productAdditional = (View) parentView.getParent().getParent();
						((CartActivity)mContext).updateItem(productAdditional);
					}
					@Override
					public void onNothingSelected(AdapterView<?> parentView) {
						
					}
				});
				options.addView(optionSpinner);
			} else if (option.getSelectionType().equals("multiple")) {
				for (ProductOption.Value value: option.getValues()) {
					CheckBox optionCheck = helper.getProductOptionCheckbox(inflater, option, value, cartItem.getValuesForOption(option.getKey()));
					// Update cart when selecting option
					optionCheck.setOnCheckedChangeListener(new OnCheckedChangeListener() {
						@Override
						public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
							View productAdditional = (View) buttonView.getParent().getParent();
							((CartActivity)mContext).updateItem(productAdditional);
						}
					});
					options.addView(optionCheck);
				}
			}
		}
		
		ImageButton delete = (ImageButton) row.findViewById(R.id.item_delete);
		delete.setTag(cartItem);
		
		return row;
	}
}
