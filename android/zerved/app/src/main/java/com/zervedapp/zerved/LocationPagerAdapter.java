package com.zervedapp.zerved;

import com.zervedapp.zerved.models.MerchantLocation;
import android.content.Context;
import android.app.Fragment;
import android.app.FragmentManager;
import android.support.v13.app.FragmentPagerAdapter;
 
public class LocationPagerAdapter extends FragmentPagerAdapter {

	private Context context; 	
	MerchantLocation location;
   
    public LocationPagerAdapter(FragmentManager fm, Context c) {
    	super(fm);
    	context = c;
        this.location = ((LocationActivity) context).mLocation;
    }
 
    @Override
    public Fragment getItem(int arg0) { 
    	if(this.location.hasCovers()) {
    		if(arg0 == 0 || arg0 > 1){
    			return ImageFragment.create(((LocationActivity) context).viewPagerHeight, this.location.getCoverURL(arg0 == 0 ? arg0 : arg0-1));
    		}
    		else return getMapFragment(this.location);    		
    	}
    	else return getMapFragment(this.location);
    }

    @Override
    public int getCount() {
        return this.location.numberOfCovers() + 1;
    }
    
    public Fragment getMapFragment(MerchantLocation location){
		return LocationMapFragment.create(
				location.getLocation().getLatitude(), 
				location.getLocation().getLongitude(), 
				location.getName()
				);	    	
    }
}