package com.zervedapp.zerved;

import java.math.BigInteger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.json.JSONException;
import org.json.JSONObject;

import com.zervedapp.zerved.loader.RESTLoader;
import com.zervedapp.zerved.loader.RESTLoader.RESTResponse;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import android.content.Loader;
import android.text.util.Linkify;

public class PaiiActivity extends AuthorizedActivity {
	private static final String TAG = PaiiActivity.class.getName();
	
	public static final String EXTRA_PHONE = "com.zervedapp.zerved.PHONE";
	private static final String ARGS_URI    = "com.zervedapp.zerved.ARGS_URI";
    private static final String ARGS_PARAMS = "com.zervedapp.zerved.ARGS_PARAMS";
	private static final int LOADER_SAVE_PHONE = 0x1;
    private static final String URI = Config.API_URL+"consumers/";
    private static final String PAII_APP_URL = "http://app.paii.dk";

    private String mPhone;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_paii);
		
		final ActionBar actionBar = getActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setIcon(R.drawable.paii_logo);
		
		// Link to Paii app
		TextView infotxt = (TextView) findViewById(R.id.paii_infotext);
		addLink(infotxt, getString(R.string.paii_infotext_linkpattern), PAII_APP_URL);
		
		// Submit when phone number is entered
		EditText phonefield = (EditText) findViewById(R.id.paii_phone_field);
		phonefield.setOnEditorActionListener(new TextView.OnEditorActionListener() {
			@Override
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
				if(actionId == EditorInfo.IME_ACTION_DONE)
				{
					savePhone();
					return true;
				}
				return false;
			}
		});
		
		// Present the keyboard
		showKeyboard();
	}
	
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
    	for (int i=0; i<menu.size(); i++) {
    		MenuItem item = menu.getItem(i);
    		if (item.getItemId() == R.id.paii_confirm_item) {
    			View view = getLayoutInflater().inflate(R.layout.paii_confirm_button, null);
    			((TextView) view.findViewById(R.id.paii_confirm_btn_text)).setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						savePhone();
					}    			
    			});
    			item.setActionView(view);
    		}
    	}
    	return true;
    }
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.paii, menu);
		return super.onCreateOptionsMenu(menu);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch(item.getItemId()) {
			case android.R.id.home:
				hideKeyboard();
	            finish();
	            return true;
			default:
				return super.onOptionsItemSelected(item);
		}
	}
	
	
	public static boolean isPhoneValid(String phone) {
		// Is set
		if(phone == null || phone.equals("null") || phone.equals("")) {
			return false;
		}
		
		// Only contains numbers?
		try {
			new BigInteger(phone);
		} catch(NumberFormatException ex) {
			return false;
		}
		
		// Correct amount of numbers?
		if(phone.length() != 8) {
			return false;
		}
		
		return true;
	}
    
	public static void addLink(TextView textView, String patternToMatch,
	        final String link) {
	    Linkify.TransformFilter filter = new Linkify.TransformFilter() {
	        @Override public String transformUrl(Matcher match, String url) {
	            return link;
	        }
	    };
	    Linkify.addLinks(textView, Pattern.compile(patternToMatch), null, null,
	            filter);
	}
	
	private void savePhone()
	{
		EditText phonefield = (EditText) findViewById(R.id.paii_phone_field);
		mPhone = phonefield.getText().toString();
		
		if(!isPhoneValid(mPhone)) {
			Toast.makeText(getBaseContext(), R.string.paii_error_invalid_phone, Toast.LENGTH_SHORT).show();
			return;
		}
		
		JSONObject postParams = new JSONObject();
    	try {
    		postParams.put("phone", mPhone);
    	} catch (JSONException e) {
    		Log.e(TAG, "Failed to create JSON.", e);
    	}
		Bundle args = new Bundle();
    	args.putParcelable(ARGS_URI, Uri.parse(URI+getConsumer().mConsumerKey));
    	Bundle params = new Bundle();
    	params.putString("json", postParams.toString());
    	args.putParcelable(ARGS_PARAMS, params);
		getLoaderManager().restartLoader(LOADER_SAVE_PHONE, args, this);
	}
	
	private void showKeyboard() {
		((InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE)).toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
	}
	
	private void hideKeyboard() {
		EditText phonefield = (EditText) findViewById(R.id.paii_phone_field);
		if(phonefield != null) {
			((InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(phonefield.getWindowToken(), 0);
		}
	}
	
	private void submitPayment() {
		hideKeyboard();
		
		Intent data = getIntent();
		data.putExtra(EXTRA_PHONE, mPhone);
		setResult(Activity.RESULT_OK, data);
		finish();
	}
	
	@Override
	public Loader<RESTResponse> onCreateLoader(int id, Bundle args) {
		if (args != null && args.containsKey(ARGS_URI) && args.containsKey(ARGS_PARAMS)) {
            Uri    action = args.getParcelable(ARGS_URI);
            Bundle params = args.getParcelable(ARGS_PARAMS);
            if (id == LOADER_SAVE_PHONE) {
    			return new RESTLoader(this, RESTLoader.HTTPVerb.POST, action, params, getConsumer().mAccessToken);
    		}
		}
		return super.onCreateLoader(id, args);
	}
	
	@Override
	public void onLoadFinished(Loader<RESTResponse> loader, RESTResponse data) {
		int    code = data.getCode();
        String json = data.getData();
        
        if(loader.getId() == LOADER_SAVE_PHONE) {
        	if (code == 200) {
        		// Phone number was saved, submit order
				submitPayment();
        	} else {
        		Toast.makeText(this, getString(R.string.paii_error_saving_phone), Toast.LENGTH_SHORT).show();
        	}
        	reload();
        }
		super.onLoadFinished(loader, data);
	}	
}
