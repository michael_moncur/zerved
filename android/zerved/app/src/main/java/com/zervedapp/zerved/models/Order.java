package com.zervedapp.zerved.models;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import com.zervedapp.zerved.R;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class Order extends JSONModel {
	private static final String TAG = Order.class.getName();
	
	/**
	 * Create order from JSON string
	 * 
	 * @param json
	 * @throws JSONException
	 */
	public Order(String json) throws JSONException {
		super(json);
	}
	
	/**
	 * Create list of order objects from JSON string
	 * 
	 * @param json
	 * @return ArrayList of Orders
	 */
	public static ArrayList<Order> initList(String json) {
        ArrayList<Order> objectList = new ArrayList<Order>();
        
        try {
        	JSONArray objects = (JSONArray) new JSONTokener(json).nextValue();
            for (int i = 0; i < objects.length(); i++) {
            	Order object = new Order(objects.getString(i));
                objectList.add(object);
            }
            
        }
        catch (JSONException e) {
            Log.e(TAG, "Failed to parse JSON.", e);
        }
        
        return objectList;
    }
	
	/**
	 * Create list of orders from a database query
	 * 
	 * @param c Cursor from query
	 * @return ArrayList of Orders
	 */
	public static ArrayList<Order> initList(Cursor c) {
		ArrayList<Order> objectList = new ArrayList<Order>();
		try {
			while (c.moveToNext()) {
				Order order = new Order(c.getString(c.getColumnIndexOrThrow(ZervedContract.Order.COLUMN_NAME_JSON)));
				objectList.add(order);
			}
		}
        catch (JSONException e) {
            Log.e(TAG, "Failed to parse JSON.", e);
        }
		return objectList;
	}
	
	public String toString() {
		return getStringProperty("order_number");
	}
	
	public String getImageUrl() {
		try {
			JSONModel merchant = new JSONModel(getString("merchant"));
			if (merchant.has("image_url") && !merchant.getStringProperty("image_url").equals("")) {
				return merchant.getStringProperty("image_url");
			}
		} catch (JSONException e) {
			
		}
		return null;
	}
	
	public String getEntrancePincode() {
		try {
			JSONModel merchant = new JSONModel(getString("merchant"));
			if (merchant != null && merchant.has("entrance_pincode") && !merchant.getStringProperty("entrance_pincode").equals("")) {
				return merchant.getStringProperty("entrance_pincode");
			}
		} catch (JSONException e) {
			
		}
		return null;
	}
	
	public boolean getServeWithConsumerDevice() {
		try {
			JSONModel merchant = new JSONModel(getString("merchant"));
			if (merchant.has("serve_with_consumer_device")) {
				return merchant.getBooleanProperty("serve_with_consumer_device");
			}
		} catch (JSONException e) {
			
		}
		return false;
	}
	
	public boolean getStaffCompletionEnabled() {
		// Pending entrance order, or preparing menu order if device completion enabled
		return ((getStringProperty("service").equals("entrance") && getStringProperty("status").equals("pending"))
				|| (getStringProperty("status").equals("preparing") && getServeWithConsumerDevice()));
	}
	
	public String getLocationName() {
		return (isNull("location_name")) ? getStringProperty("merchant_name") : getStringProperty("location_name");
	}
	
	public String getStatusTitle() {
		if (has("status_title_for_consumer") && !isNull("status_title_for_consumer")) {
			return getStringProperty("status_title_for_consumer");
		}
		return null;
	}
	
	public String getStatusDescription() {
		if (has("status_description_for_consumer") && !isNull("status_description_for_consumer")) {
			return getStringProperty("status_description_for_consumer");
		}
		return null;
	}
	
	public String getServiceSummary(Context context) {
		String service = getStringProperty("service");
		if (service.equals("counter")) {
			return context.getString(R.string.pickup_at_counter);
		} else if (service.equals("table")) {
			return context.getString(R.string.serve_at_table);
		} else if (service.equals("delivery")) {
			return context.getString(R.string.deliver_to_address);
		}
		return null;
	}
	
	public ArrayList<TaxTotal> getTaxTotals() {
		ArrayList<TaxTotal> items = new ArrayList<TaxTotal>();
		try {
			JSONArray objects = getJSONArray("total_tax_grouped");
			for (int i = 0; i < objects.length(); i++) {
				TaxTotal object = new TaxTotal(objects.getString(i));
	            items.add(object);
			}
		}
		catch (JSONException e) {
            Log.e(TAG, "Failed to parse JSON.", e);
        }
		return items;
	}
	
	public ArrayList<Item> getItems() {
		ArrayList<Item> items = new ArrayList<Item>();
		try {
			JSONArray objects = getJSONArray("items");
			for (int i = 0; i < objects.length(); i++) {
				Item object = new Item(objects.getString(i));
	            items.add(object);
			}
		}
		catch (JSONException e) {
            Log.e(TAG, "Failed to parse JSON.", e);
        }
		return items;
	}
	
	public String getPropertySet() {
		if (has("items")) {
			return "complete";
		} else {
			return "sparse";
		}
	}
	
	/**
	 * Load the order model from the DB cache if it exists
	 * 
	 * @param db
	 * @param key
	 * @return Order or null
	 */
	public static Order loadFromCache(SQLiteDatabase db, String key) {
		String[] projection = {	ZervedContract.Order.COLUMN_NAME_JSON };
		String selection = ZervedContract.Order.COLUMN_NAME_KEY + " = ?";
		String[] selectionArgs = { key };
		Cursor c = db.query(ZervedContract.Order.TABLE_NAME, projection, selection, selectionArgs, null, null, null);
		if (c.getCount() > 0) {
			c.moveToFirst();
			try {
				Order order = new Order(c.getString(c.getColumnIndexOrThrow(ZervedContract.Order.COLUMN_NAME_JSON)));
				return order;
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		return null;
	}
	
	/**
	 * Save model to DB cache
	 * It will update the cache if the model already exists, except if the cached model has more data
	 * @param db
	 */
	public void saveToCache(SQLiteDatabase db) {
		// Save model
		ContentValues values = new ContentValues();
    	values.put(ZervedContract.Order.COLUMN_NAME_KEY, getStringProperty("key"));
    	values.put(ZervedContract.Order.COLUMN_NAME_STATUS, getStringProperty("status"));
    	values.put(ZervedContract.Order.COLUMN_NAME_DATE_CREATED, getStringProperty("date_created"));
    	values.put(ZervedContract.Order.COLUMN_NAME_JSON, toJson());
    	values.put(ZervedContract.Order.COLUMN_NAME_PROPERTY_SET, getPropertySet());
    	// If save a model that already exists, only allow it if the data contains all properties
    	//int conflict = getPropertySet().equals("complete") ? SQLiteDatabase.CONFLICT_REPLACE : SQLiteDatabase.CONFLICT_IGNORE;
    	//db.insertWithOnConflict(ZervedContract.Order.TABLE_NAME, null, values, conflict);
    	Order existingOrder = Order.loadFromCache(db, getKey());
    	if (existingOrder != null) {
    		// Update if allowed
    		if (getPropertySet().equals("complete") || !existingOrder.getPropertySet().equals("complete")) {
    			String selection = ZervedContract.Order.COLUMN_NAME_KEY + " = ?";
    			String[] selectionArgs = { getKey() };
    			db.update(ZervedContract.Order.TABLE_NAME, values, selection, selectionArgs);
    		}
    	} else {
    		// Insert new
    		db.insert(ZervedContract.Order.TABLE_NAME, null, values);
    	}
	}
	
	public class TaxTotal extends JSONModel {
		public TaxTotal(String json) throws JSONException {
			super(json);
		}
	}
	
	public class Item extends JSONModel {
		public Item(String json) throws JSONException {
			super(json);
		}
		
		public String getOptionsSummary() {
			String summary = "";
			try {
				JSONArray options = getJSONArray("option_values");
				for (int i = 0; i < options.length(); i++) {
					JSONObject option = options.getJSONObject(i);
					summary += option.getString("option_name") + ":\n  ";
					JSONArray values = option.getJSONArray("values");
					summary += values.getJSONObject(0).getString("value_label");
					for (int j = 1; j < values.length(); j++) {
						summary += "\n  " + values.getJSONObject(j).getString("value_label");
					}
					if (i < options.length()-1) {
						summary += "\n";
					}
				}
			}
			catch (JSONException e) {
	            Log.e(TAG, "Failed to parse JSON.", e);
	        }
			return summary;
		}
	}
}
