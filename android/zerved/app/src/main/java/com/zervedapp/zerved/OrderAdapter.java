package com.zervedapp.zerved;

import com.koushikdutta.urlimageviewhelper.UrlImageViewHelper;
import com.zervedapp.zerved.models.Order;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.TextView;

public class OrderAdapter extends ArrayAdapter<Order> {
	public OrderAdapter(Context context, int textViewResourceId) {
		super(context, textViewResourceId);
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View row;
		LayoutInflater inflater = LayoutInflater.from(getContext());
		if (null == convertView) {
			row = inflater.inflate(R.layout.item_order, null);
		} else {
			row = convertView;
		}
		
		Order order = this.getItem(position);
		
		// Image
		ImageView image = (ImageView) row.findViewById(R.id.image);
		image.setScaleType(ScaleType.FIT_XY);
		String imageUrl = order.getImageUrl();
		if (imageUrl == null) {
			image.setVisibility(View.GONE);
		} else {
			UrlImageViewHelper.setUrlDrawable(image, imageUrl);
		}
		
		// Order number and Merchant name
		TextView title = (TextView) row.findViewById(R.id.merchant);
		title.setText(String.format(getContext().getResources().getString(R.string.no_x_at_location), order.getStringProperty("queue_number"), order.getLocationName()));
		
		// Order date
		TextView date = (TextView) row.findViewById(R.id.date);
		date.setText(order.getStringProperty("date_created_formatted"));
		
		return row;
	}
}
