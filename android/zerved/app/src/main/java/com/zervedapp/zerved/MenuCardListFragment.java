package com.zervedapp.zerved;

import java.util.ArrayList;

import org.json.JSONException;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.zervedapp.zerved.models.Bar;
import com.zervedapp.zerved.models.Event;
import com.zervedapp.zerved.models.MenuCard;
import com.zervedapp.zerved.models.MerchantLocation;

public class MenuCardListFragment extends RESTListFragment {
	public static final String TAG = MenuCardListFragment.class.getName();
	public static final String ARG_GROUP = "com.zervedapp.zerved.GROUP";
    public static final String EXTRA_LOCATION = "com.zervedapp.zerved.LOCATION";
    public static final String EXTRA_BAR = "com.zervedapp.zerved.BAR";
    public static final String EXTRA_MENU = "com.zervedapp.zerved.MENU";
    public static final String EXTRA_MENUCARDS = "com.zervedapp.zerved.MENUCARDS";
    public static final String EXTRA_SELECTED_INDEX = "com.zervedapp.zerved.SELECTED_INDEX";
    
	private ArrayAdapter<MenuCard> mAdapter;
	private static String MENUCARD_URI = Config.API_URL+"bars/%s/menus";
	private static String EVENT_URI = Config.API_URL+"events/%s";
	private Bar mBar = null;
	private MerchantLocation mLocation = null;
	private String mMenuCardsJson;
	private Event mEvent;
	
	public interface LocationBarProvider {
		public MerchantLocation getLocation();
		public Bar getBar();
		public boolean isLocationLoading();
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		mEvent = ((MenuCardsActivity) getActivity()).getEvent();
        mAdapter = new ArrayAdapter<MenuCard>(getActivity(), R.layout.item_label_list);
        this.setListAdapter(mAdapter);
        setHasOptionsMenu(true);
        reload();
	}
	
	@Override
	public void reload() {
		LocationBarProvider activity = (LocationBarProvider) getActivity();
		mBar = activity.getBar();
		mLocation = activity.getLocation();
		TextView emptyMsg = (TextView) getView().findViewById(R.id.empty_message);
		emptyMsg.setText(getResources().getText(R.string.no_menu));
		super.reload();
	}
	
	@Override
	public String getUri() {
		if(mEvent != null){
			return String.format(EVENT_URI, mEvent.getKey());
		}
		else return String.format(MENUCARD_URI, mBar.getKey());
	}
	
	@Override
	public void onJsonLoaded(String json) {
		ArrayList<MenuCard> menus = new ArrayList<MenuCard>();
		if( mEvent != null ){
			try {
				mEvent = new Event(json);
		    	menus = mEvent.getMenus(); 
		    	mMenuCardsJson = menus.toString();
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		else {
			mMenuCardsJson = json;
	    	menus = MenuCard.initList(json);
		}        
        // Load our list adapter with our models.
        mAdapter.clear();
        for (MenuCard menu : menus) {
        	mAdapter.add(menu);
        }
	}
	
    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
    	Intent intent = new Intent(getActivity(), CategoriesActivity.class);
    	intent.putExtra(CategoryListFragment.EXTRA_LOCATION, mLocation.toJson());
    	intent.putExtra(CategoryListFragment.EXTRA_BAR, mBar.toJson());
    	intent.putExtra(CategoryListFragment.EXTRA_GROUP, "menu");
    	intent.putExtra(CategoryListFragment.ARG_GROUP, "menu");
    	intent.putExtra(CategoryListFragment.EXTRA_MENUCARDS, mMenuCardsJson);
    	intent.putExtra(CategoryListFragment.EXTRA_SELECTED_INDEX, position);
    	intent.putExtra(CategoryListFragment.EXTRA_MENU, mAdapter.getItem(position).toJson());
    	intent.putExtra(CategoryListFragment.ARG_MENU, mAdapter.getItem(position).toJson());
    	//intent.putExtra(CategoryListFragment.EXTRA_EVENT, "null");    	
        startActivity(intent);
    }
}