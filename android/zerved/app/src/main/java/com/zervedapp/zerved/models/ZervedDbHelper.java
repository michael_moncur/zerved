package com.zervedapp.zerved.models;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class ZervedDbHelper extends SQLiteOpenHelper {
	// If you change the database schema, you must increment the database version.
    public static final int DATABASE_VERSION = 4;
    public static final String DATABASE_NAME = "Zerved.db";
    
    public static final String SQL_CREATE_ENTRIES =
    		"CREATE TABLE " + ZervedContract.Order.TABLE_NAME + " (" +
    		ZervedContract.Order._ID + " INTEGER PRIMARY KEY,"+
    		ZervedContract.Order.COLUMN_NAME_KEY + " TEXT UNIQUE,"+
    		ZervedContract.Order.COLUMN_NAME_STATUS + " TEXT,"+
    		ZervedContract.Order.COLUMN_NAME_DATE_CREATED + " DATETIME,"+
    		ZervedContract.Order.COLUMN_NAME_JSON + " TEXT,"+
    		ZervedContract.Order.COLUMN_NAME_PROPERTY_SET + " TEXT"+
    		");";
    
    public static final String SQL_DELETE_ENTRIES =
    		"DROP TABLE IF EXISTS " + ZervedContract.Order.TABLE_NAME;
    
    public ZervedDbHelper(Context context) {
    	super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }
    
	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(SQL_CREATE_ENTRIES);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // This database is only a cache for online data, so its upgrade policy is
        // to simply to discard the data and start over
        db.execSQL(SQL_DELETE_ENTRIES);
        onCreate(db);
	}
	
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }
}
