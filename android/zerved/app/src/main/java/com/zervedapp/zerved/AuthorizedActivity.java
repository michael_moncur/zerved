package com.zervedapp.zerved;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.app.LoaderManager.LoaderCallbacks;
import android.content.Loader;
import android.view.View;

import com.zervedapp.zerved.loader.RESTLoader;
import com.zervedapp.zerved.loader.RESTLoader.RESTResponse;
import com.zervedapp.zerved.models.ConsumerManager;

public class AuthorizedActivity extends Activity implements LoaderCallbacks<RESTLoader.RESTResponse>, ConsumerManager.AuthenticationListener {
	protected LoginFragment mLoginFragment;
    protected ProgressDialog mProgress;
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
    }
    
    @Override
    protected void onSaveInstanceState(Bundle outState) {
    	// Remove fragment because it will be recreated when activity is reloaded
    	if (mLoginFragment != null)
    		getFragmentManager().beginTransaction().remove(mLoginFragment).commit();
    	super.onSaveInstanceState(outState);
    }
    
    public void reload() {
    	
    }
    
    public void login() {
    	getConsumer().authenticate(this, this);
	}
    
	protected ConsumerManager getConsumer() {
		return ((ZervedApp) getApplication()).getConsumer();
	}
    
    public void loginOrCreateClick(View view) {
    	login();
    }
    
    @Override
	public Loader<RESTResponse> onCreateLoader(int id, Bundle args) {
        return null;
	}

	@Override
	public void onLoadFinished(Loader<RESTLoader.RESTResponse> loader, RESTLoader.RESTResponse data) {
		int    code = data.getCode();
        String body = data.getData();
		
		if (code == 200 && !body.equals("")) {
			// Content loaded
			onJsonLoaded(body);
		} else if (code == 401) {
			getConsumer().mAccessToken = null;
			onAuthenticationFailed();
		}
	}

	@Override
	public void onLoaderReset(Loader<RESTResponse> arg0) {
		
	}
	
	public void onJsonLoaded(String json) {
		
	}
	
	/* AuthenticationListener methods */
	
	public void onAuthenticationCompleted() {
		reload();
	}
	
	public void onAuthenticationFailed() {
		setContentView(R.layout.activity_authorized);
	}
	
	public void setLoginFragment(LoginFragment fragment) {
		mLoginFragment = fragment;
	}
	
	/* Pass onClick functions to fragment */
	
	public void cancelClick(View view) {
		mLoginFragment.cancelClick(view);
	}
	
	public void loginClick(View view) {
		mLoginFragment.loginClick(view);
	}
	
	public void createAccountClick(View view) {
		mLoginFragment.createAccountClick(view);
	}
	
	public void forgotPassword(View view) {
		mLoginFragment.forgotPassword(view);
	}
}
