package com.zervedapp.zerved;

import org.json.JSONException;

import android.app.Activity;
import android.app.ActionBar;
import android.content.Intent;
import android.os.Bundle;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.TaskStackBuilder;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.zervedapp.zerved.models.Event;
import com.zervedapp.zerved.models.MerchantLocation;

public class BarsActivity extends Activity {
	public static final String TAG = BarsActivity.class.getName();
	private MerchantLocation mLocation;
	private Event mEvent;
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_event_summary);

        final ActionBar actionBar = getActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        
        // Get location and bar
        Intent intent = getIntent();

        mEvent = ((ZervedApp) getApplication()).mEvent;
        
        String locationJson = intent.getStringExtra(BarListFragment.EXTRA_LOCATION);
        try {
        	mLocation = new MerchantLocation(locationJson);
		} catch (JSONException e) {
			Log.e(TAG, "Failed to parse JSON.", e);
		}
        Log.i(TAG, "Location: " + mLocation);        

        setTitle(getResources().getString(R.string.sales_points));
        
        
		// Event Summary
		if(mEvent != null){
			((TextView) findViewById(R.id.event_name)).setText(mEvent.getName() + " " + mEvent.getTimeStartFormatted());
			((TextView) findViewById(R.id.event_date_start_formatted)).setText(mEvent.getDateOnlyStartFormatted());
			((TextView) findViewById(R.id.event_time_label)).setText(R.string.serving_time);
			((TextView) findViewById(R.id.event_date_delivery_time_formatted)).setText(mEvent.getTimeDeliveryFormatted() + " (" + mEvent.getTimeLabel() + ")");
		}
		else findViewById(R.id.event_summary).setVisibility(View.GONE);
        
		// Create bar list fragment
        FragmentManager fm = getFragmentManager();
        Fragment fragment = new BarListFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
		FragmentTransaction ft = fm.beginTransaction();
		ft.add(R.id.fragment_content, fragment);
		ft.commit();
	}
	
    @Override
    public void onResume() {
    	super.onResume();
    	invalidateOptionsMenu();
    }
	
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
    	return true;
    }
    
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
    	return true;
    }
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // This is called when the Home (Up) button is pressed in the action bar.
                // Create a simple intent that starts the hierarchical parent activity and
                // use NavUtils in the Support Package to ensure proper handling of Up.
                Intent upIntent = new Intent(this, LocationActivity.class);
                if (shouldUpRecreateTask(upIntent)) {
                    // This activity is not part of the application's task, so create a new task
                    // with a synthesized back stack.
                    TaskStackBuilder.create(this)
                            // If there are ancestor activities, they should be added here.
                            .addNextIntent(upIntent)
                            .startActivities();
                    finish();
                } else {
                    // This activity is part of the application's task, so simply
                    // navigate up to the hierarchical parent activity.
                    //NavUtils.navigateUpTo(this, upIntent);
                	// navigateUpTo recreates the activity which will revert to first tab - instead we simply finish this activity to go back without recreating the parent
                	finish();
                }
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

	public MerchantLocation getLocation() {
		return mLocation;
	}
}
