package com.zervedapp.zerved;

import org.json.JSONException;
import org.w3c.dom.Text;

import android.animation.ValueAnimator;
import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.TaskStackBuilder;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

import com.zervedapp.zerved.models.Bar;
import com.zervedapp.zerved.models.Cart;
import com.zervedapp.zerved.models.Event;
import com.zervedapp.zerved.models.MenuCard;
import com.zervedapp.zerved.models.MerchantLocation;

public class CategoriesActivity extends Activity implements CategoryListFragment.LocationBarProvider {
	public static final String TAG = CategoriesActivity.class.getName();
	private MerchantLocation mLocation;
	private Bar mBar;
	private MenuCard mMenu;
	private String mGroup = "menu";
	private Event mEvent;
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_event_summary);
        
        final ActionBar actionBar = getActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        
        // Get location and bar
        Intent intent = getIntent();

        String locationJson = intent.getStringExtra(CategoryListFragment.EXTRA_LOCATION);
        try {
        	mLocation = new MerchantLocation(locationJson);
		} catch (JSONException e) {
			Log.e(TAG, "Failed to parse JSON.", e);
		}
        Log.i(TAG, "Location: " + mLocation);
        
        String barJson = intent.getStringExtra(CategoryListFragment.EXTRA_BAR);
		try {
			mBar = new Bar(barJson);
		} catch (JSONException e) {
			Log.e(TAG, "Failed to parse JSON.", e);
		}
        Log.i(TAG, "Bar: " + mBar);
		
        if(mGroup.equals("menu")) {
        String menuJson = intent.getStringExtra(CategoryListFragment.EXTRA_MENU);
		try {
			mMenu = new MenuCard(menuJson);
			actionBar.setTitle(mMenu.getName());
		} catch (JSONException e) {
			Log.e(TAG, "Failed to parse JSON.", e);
		}        
        Log.i(TAG, "Menu: " + mMenu);
        }      
        
        mEvent = ((ZervedApp) getApplication()).mEvent;
		
        // Event Summary
		if(mEvent != null){
			((TextView) findViewById(R.id.event_name)).setText(mEvent.getName() + " " + mEvent.getTimeStartFormatted());
			((TextView) findViewById(R.id.event_date_start_formatted)).setText(mEvent.getDateOnlyStartFormatted());
			((TextView) findViewById(R.id.event_time_label)).setText(R.string.serving_time);
			((TextView) findViewById(R.id.event_date_delivery_time_formatted)).setText(mEvent.getTimeDeliveryFormatted() + " (" + mEvent.getTimeLabel() + ")");
		}
		else findViewById(R.id.event_summary).setVisibility(View.GONE);
		
		// Create category list fragment
        FragmentManager fm = getFragmentManager();
        Fragment fragment = new CategoryListFragment();
        Bundle args = new Bundle();
        args.putString(CategoryListFragment.ARG_GROUP, mGroup);
        if(mGroup.equals("menu")) {
        args.putString(CategoryListFragment.ARG_MENU, mMenu.toJson());
        }
        fragment.setArguments(args);
		FragmentTransaction ft = fm.beginTransaction();
		ft.add(R.id.fragment_content, fragment);
		ft.commit();
	}
	
    @Override
    public void onResume() {
    	super.onResume();
    	// Refresh cart button
    	invalidateOptionsMenu();
    }
	
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
    	MenuInflater inflater = getMenuInflater();
    	inflater.inflate(R.menu.products, menu);
    	return true;
    }
    
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
    	for (int i=0; i<menu.size(); i++) {
    		MenuItem item = menu.getItem(i);
    		if (item.getItemId() == R.id.cart) {
    			// Update cart button to show number of items
    			Cart cart = ((ZervedApp) getApplication()).getCart(mLocation, mBar, mGroup, mEvent);
    			int s = cart.size();
    			View view;
    			if(s > 0){
	    			view = getLayoutInflater().inflate(R.layout.custom_cart_layout, null);
					final TextView cartBtnText = (TextView) view.findViewById(R.id.cart_btn_text);

					getFlashAnimation(cartBtnText).start();

					cartBtnText.setOnClickListener(new OnClickListener() {
						@Override
						public void onClick(View v) {
							startActivity(getCartIntent());
						}
					});
    			}
    			else {
    				view = getLayoutInflater().inflate(R.layout.custom_cart_layout_disabled, null);
    			}
    			((TextView) view.findViewById(R.id.cart_btn_text)).setText(String.format(getResources().getString(R.string.cart_x), cart.size()));
    			item.setActionView(view);
    		}
    	}
    	return true;
    }

	public static ValueAnimator getFlashAnimation(final TextView textView) {
		final float[] from = new float[3],
				to =  new float[3];

		Color.colorToHSV(Color.parseColor("#FFFFFFFF"), from);   // from white
		Color.colorToHSV(Color.parseColor("#4e9457"), to);     // to green

		ValueAnimator anim = ValueAnimator.ofFloat(0, 1);   // animate from 0 to 1
		anim.setDuration(500);                              // for 300 ms
		anim.setRepeatCount(3);
		anim.setRepeatMode(ValueAnimator.RESTART);

		final float[] hsv  = new float[3];                  // transition color
		anim.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
			@Override
			public void onAnimationUpdate(ValueAnimator animation) {
				// Transition along each axis of HSV (hue, saturation, value)
				hsv[0] = from[0] + (to[0] - from[0]) * animation.getAnimatedFraction();
				hsv[1] = from[1] + (to[1] - from[1]) * animation.getAnimatedFraction();
				hsv[2] = from[2] + (to[2] - from[2]) * animation.getAnimatedFraction();

				textView.setBackgroundColor(Color.HSVToColor(hsv));
			}
		});

		return anim;
	}

    public Intent getCartIntent() {
    	Intent intent = new Intent(this, CartActivity.class);
    	intent.putExtra(CategoryListFragment.EXTRA_LOCATION, mLocation.toJson());
    	if (mBar != null) {
    		intent.putExtra(CategoryListFragment.EXTRA_BAR, mBar.toJson());
    	}
    	intent.putExtra(CategoryListFragment.EXTRA_GROUP, mGroup);
    	return intent;
    }
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent upIntent = new Intent(this, LocationActivity.class);
                if (shouldUpRecreateTask(upIntent))
                    TaskStackBuilder.create(this).addNextIntent(upIntent) .startActivities();
                
                finish();                
                return true;
            case R.id.cart:
            	Intent intent = new Intent(this, CartActivity.class);
            	intent.putExtra(CategoryListFragment.EXTRA_LOCATION, mLocation.toJson());
            	if (mBar != null) {
            		intent.putExtra(CategoryListFragment.EXTRA_BAR, mBar.toJson());
            	}
            	intent.putExtra(CategoryListFragment.EXTRA_GROUP, mGroup);
            	startActivity(intent);
            	return true;
        }
        return super.onOptionsItemSelected(item);
    }

	@Override
	public MerchantLocation getLocation() {
		return mLocation;
	}

	@Override
	public Bar getBar() {
		return mBar;
	}

	@Override
	public boolean isLocationLoading() {
		return false;
	}
}
