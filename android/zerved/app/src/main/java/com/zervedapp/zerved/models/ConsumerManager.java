package com.zervedapp.zerved.models;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.DialogFragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.LoaderManager.LoaderCallbacks;
import android.content.Loader;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.widget.Toast;
import com.google.android.gcm.GCMRegistrar;
import com.zervedapp.zerved.CartActivity;
import com.zervedapp.zerved.Config;
import com.zervedapp.zerved.LoginFragment;
import com.zervedapp.zerved.LoginFragment.LoginListener;
import com.zervedapp.zerved.R;
import com.zervedapp.zerved.loader.AppEngineHttpClient;
import com.zervedapp.zerved.loader.RESTLoader;
import com.zervedapp.zerved.loader.RESTLoader.RESTResponse;

public class ConsumerManager implements LoaderCallbacks<RESTResponse>, LoginListener {
	public static final String TAG = ConsumerManager.class.getName();
	
	private static String URI = Config.API_URL+"consumers/";
	private static String FAV_DELETE_URI = Config.API_URL+"favourites/";
	private static final int LOADER_LOGIN = 100;
	private static final int LOADER_CREATEACCOUNT = 101;
	private static final int LOADER_RESET_PASSWORD = 102;
	private static final int LOADER_LOGIN_DIALOG = 103;
	private static final int LOADER_CREATEACCOUNT_DIALOG = 104;
	private static final int LOADER_DELETE_ACCOUNT = 105;
	private static final int LOADER_DELETE_FAVOURITE = 106;
	private static final String ARGS_URI    = "com.zervedapp.zerved.ARGS_URI";
    private static final String ARGS_PARAMS = "com.zervedapp.zerved.ARGS_PARAMS";
	
	public String mAccessToken;
	public String mConsumerKey;
	public String mConsumerId;
	private String mGcmRegId = "";
	Activity mActivity;
	AuthenticationListener mListener;
	String mEnteredPassword;
	LoginFragment mLoginFragment;
	
	public interface AuthenticationListener {
		public void onAuthenticationCompleted();
		public void onAuthenticationFailed();
		public void setLoginFragment(LoginFragment fragment);
	}
	
	public void authenticate(Activity activity, AuthenticationListener listener) {
		mActivity = activity;
		mListener = listener;
		SharedPreferences settings = activity.getSharedPreferences("ZervedSettings", 0);
	    String email = settings.getString("email", "");
	    String consumerKey = settings.getString(Config.SERVER_URL+"_consumerKey", "");
	    String password = settings.getString(Config.SERVER_URL+"_password", "");
	    if (!consumerKey.equals("") && !password.equals("")) {
	    	// Login with saved credentials
	    	Bundle args = new Bundle();
	    	args.putParcelable(ARGS_URI, Uri.parse(URI+"login"));
	    	JSONObject userParams = new JSONObject();
	    	try {
	    		userParams.put("key", consumerKey);
	    		userParams.put("password", password);
	    	} catch (JSONException e) {
	    		Log.e(TAG, "Failed to create JSON.", e);
	    	}
	    	Bundle params = new Bundle();
	    	params.putString("json", userParams.toString());
	    	args.putParcelable(ARGS_PARAMS, params);
			mActivity.getLoaderManager().restartLoader(LOADER_LOGIN, args, this);
	    } else if (!email.equals("")) {
	    	// Email from old account saved: Show login dialog
	    	FragmentManager fragmentManager = mActivity.getFragmentManager();
			mLoginFragment = new LoginFragment();
			mLoginFragment.setStyle(DialogFragment.STYLE_NO_TITLE, 0);
			mLoginFragment.show(fragmentManager, null);
			mListener.setLoginFragment(mLoginFragment);
	    } else {
	    	// Create new account
	    	Bundle args = new Bundle();
	    	args.putParcelable(ARGS_URI, Uri.parse(URI+"create"));
	    	JSONObject userParams = new JSONObject();
	    	try {
	    		userParams.put("device", "android");
	    	} catch (JSONException e) {
	    		Log.e(TAG, "Failed to create JSON.", e);
	    	}
	    	Bundle params = new Bundle();
	    	params.putString("json", userParams.toString());
	    	args.putParcelable(ARGS_PARAMS, params);
			mActivity.getLoaderManager().restartLoader(LOADER_CREATEACCOUNT, args, this);
	    }
	}
	
	@Override
	public Loader<RESTResponse> onCreateLoader(int id, Bundle args) {
        if (args != null && args.containsKey(ARGS_URI) && args.containsKey(ARGS_PARAMS)) {
            Uri    action = args.getParcelable(ARGS_URI);
            Bundle params = args.getParcelable(ARGS_PARAMS);
            if (id == LOADER_LOGIN || id == LOADER_CREATEACCOUNT || id == LOADER_RESET_PASSWORD || id == LOADER_LOGIN_DIALOG || id == LOADER_CREATEACCOUNT_DIALOG) {
	            return new RESTLoader(mActivity, RESTLoader.HTTPVerb.POST, action, params);
	        } else if (id == LOADER_DELETE_ACCOUNT || id == LOADER_DELETE_FAVOURITE) {
	        	return new RESTLoader(mActivity, RESTLoader.HTTPVerb.DELETE, action, params, mAccessToken);
	        }
    	}
        return null;
	}

	@Override
	public void onLoadFinished(Loader<RESTResponse> loader, RESTResponse data) {
		int    code = data.getCode();
        String body = data.getData();
        
		if (loader.getId() == LOADER_LOGIN || loader.getId() == LOADER_LOGIN_DIALOG) {
	        if (code == 200 && !body.equals("")) {
	        	// Login OK
	        	try {
	        		JSONObject object = (JSONObject) new JSONTokener(body).nextValue();
	        		mAccessToken = object.getString("access_token");
	        		mConsumerKey = object.getString("key");
					mConsumerId = object.getString("id");
	        		if (loader.getId() == LOADER_LOGIN_DIALOG) {
	        			// Save login on phone
	        			SharedPreferences settings = mActivity.getSharedPreferences("ZervedSettings", 0);
		        		SharedPreferences.Editor editor = settings.edit();
		        		editor.putString(Config.SERVER_URL+"_consumerKey", mConsumerKey);
		        		if (mEnteredPassword != null) {
		        			editor.putString(Config.SERVER_URL+"_password", mEnteredPassword);
		        		}
		        		editor.commit();
		        		FragmentManager fm = mLoginFragment.getFragmentManager();
		        		FragmentTransaction ft = fm.beginTransaction();
			        	ft.remove(mLoginFragment);
			        	ft.commitAllowingStateLoss();
	        		}
	        	} catch (JSONException e) {
	        		Log.e(TAG, "Failed to parse JSON.", e);
	        	}
	        	mListener.onAuthenticationCompleted();
	        } else if (code == 404) {
	        	// Acount does not exists, delete saved credentials
	    		SharedPreferences settings = mActivity.getSharedPreferences("ZervedSettings", 0);
	    		SharedPreferences.Editor editor = settings.edit();
	    		editor.remove(Config.SERVER_URL+"_consumerKey");
	    		editor.remove(Config.SERVER_URL+"_email");
	    		editor.remove(Config.SERVER_URL+"_password");
	    		if (!settings.getString("email", "").equals("")) {
	    			editor.remove("email");
	    		}
	    		editor.commit();
	    		mConsumerKey = null;
				mConsumerId = null;
	    		mGcmRegId = null;
	        }
	        else if (code > 0) {
	        	// Login error
	        	Toast toast = Toast.makeText(mActivity, body, Toast.LENGTH_SHORT);
	        	toast.setGravity(Gravity.CENTER, 0, 0);
	        	toast.show();
	        	// Delete login from phone
	        	SharedPreferences settings = mActivity.getSharedPreferences("ZervedSettings", 0);
        		SharedPreferences.Editor editor = settings.edit();
        		editor.putString(Config.SERVER_URL+"_consumerKey", "");
        		editor.putString(Config.SERVER_URL+"_password", "");
        		editor.commit();
	        } else {
	        	Toast.makeText(mActivity, R.string.no_connection, Toast.LENGTH_SHORT).show();
	        }
		} else if (loader.getId() == LOADER_CREATEACCOUNT || loader.getId() == LOADER_CREATEACCOUNT_DIALOG) {
			if (code == 200 && !body.equals("")) {
	        	// Account created
	        	try {
	        		JSONObject object = (JSONObject) new JSONTokener(body).nextValue();
	        		mAccessToken = object.getString("access_token");
	        		mConsumerKey = object.getString("key");
					mConsumerId = object.getString("id");
	        		String password = object.getString("password");
	        		// Save login on phone
	        		SharedPreferences settings = mActivity.getSharedPreferences("ZervedSettings", 0);
	        		SharedPreferences.Editor editor = settings.edit();
	        		editor.putString(Config.SERVER_URL+"_consumerKey", mConsumerKey);
	        		editor.putString(Config.SERVER_URL+"_password", password);
	        		editor.commit();
	        		if (loader.getId() == LOADER_CREATEACCOUNT_DIALOG) {
	        			FragmentManager fm = mLoginFragment.getFragmentManager();
		        		FragmentTransaction ft = fm.beginTransaction();
			        	ft.remove(mLoginFragment);
			        	ft.commitAllowingStateLoss();
	        		}
	        	} catch (JSONException e) {
	        		Log.e(TAG, "Failed to parse JSON.", e);
	        	}
	        	mListener.onAuthenticationCompleted();
	        }
	        else {
	        	// Create account error
	        	Toast toast = Toast.makeText(mActivity, body, Toast.LENGTH_SHORT);
	        	toast.setGravity(Gravity.CENTER, 0, 0);
	        	toast.show();
	        }
		} else if (loader.getId() == LOADER_RESET_PASSWORD) {
			Toast toast;
			if (code == 200) {
				toast = Toast.makeText(mActivity, R.string.please_check_email_for_reset, Toast.LENGTH_LONG);
			} else {
				toast = Toast.makeText(mActivity, body, Toast.LENGTH_SHORT);
			}
			toast.setGravity(Gravity.CENTER, 0, 0);
			toast.show();
		} else if(loader.getId() == LOADER_DELETE_FAVOURITE){
			if (code == 200) {
				Toast.makeText(mActivity, R.string.removed_from_favourites, Toast.LENGTH_LONG).show();
			}
			else Toast.makeText(mActivity, R.string.error_not_removed_from_favourites, Toast.LENGTH_LONG).show();
		}
		
		if (code == 401) {
			mAccessToken = null;
			mListener.onAuthenticationFailed();
		}
	}

	@Override
	public void onLoaderReset(Loader<RESTResponse> arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onLoginWithEmailAndPassword(String email, String password) {
		Bundle args = new Bundle();
    	args.putParcelable(ARGS_URI, Uri.parse(URI+"login"));
    	JSONObject userParams = new JSONObject();
    	try {
    		userParams.put("email", email);
    		userParams.put("password", password);
    	} catch (JSONException e) {
    		Log.e(TAG, "Failed to create JSON.", e);
    	}
    	Bundle params = new Bundle();
    	params.putString("json", userParams.toString());
    	args.putParcelable(ARGS_PARAMS, params);
    	mEnteredPassword = password;
		mActivity.getLoaderManager().restartLoader(LOADER_LOGIN_DIALOG, args, this);
	}

	@Override
	public void onLoginCancelled() {
		mListener.onAuthenticationFailed();
	}

	@Override
	public void onRequestNewAccount() {
		Bundle args = new Bundle();
    	args.putParcelable(ARGS_URI, Uri.parse(URI+"create"));
    	JSONObject userParams = new JSONObject();
    	try {
    		userParams.put("device", "android");
    	} catch (JSONException e) {
    		Log.e(TAG, "Failed to create JSON.", e);
    	}
    	Bundle params = new Bundle();
    	params.putString("json", userParams.toString());
    	args.putParcelable(ARGS_PARAMS, params);
		mActivity.getLoaderManager().restartLoader(LOADER_CREATEACCOUNT_DIALOG, args, this);
	}

	@Override
	public void onRequestResetPasswordForEmail(String email) {
		Bundle args = new Bundle();
    	args.putParcelable(ARGS_URI, Uri.parse(URI+"resetpassword"));
    	JSONObject userParams = new JSONObject();
    	try {
    		userParams.put("email", email);
    	} catch (JSONException e) {
    		Log.e(TAG, "Failed to create JSON.", e);
    	}
    	Bundle params = new Bundle();
    	params.putString("json", userParams.toString());
    	args.putParcelable(ARGS_PARAMS, params);
		mActivity.getLoaderManager().restartLoader(LOADER_RESET_PASSWORD, args, this);
	}
	
	public void deleteAccount(Activity activity, AuthenticationListener listener)
	{
		mActivity = activity;
		mListener = listener;
		
		// Delete account on server
		Bundle args = new Bundle();
    	args.putParcelable(ARGS_URI, Uri.parse(URI+mConsumerKey));
    	Bundle params = new Bundle();
    	args.putParcelable(ARGS_PARAMS, params);
		mActivity.getLoaderManager().restartLoader(LOADER_DELETE_ACCOUNT, args, this);
		
		// Delete saved login
		SharedPreferences settings = mActivity.getSharedPreferences("ZervedSettings", 0);
		SharedPreferences.Editor editor = settings.edit();
		editor.remove(Config.SERVER_URL+"_consumerKey");
		editor.remove(Config.SERVER_URL+"_email");
		editor.remove(Config.SERVER_URL+"_password");
		if (!settings.getString("email", "").equals("")) {
			editor.remove("email");
		}
		editor.commit();
		mConsumerKey = null;
		mConsumerId = null;
		mGcmRegId = null;
	}
	
	public void deleteFavourite(Activity activity, String key)
	{

		// Delete favourite on server
		Bundle args = new Bundle();
    	args.putParcelable(ARGS_URI, Uri.parse(FAV_DELETE_URI+key));
    	Bundle params = new Bundle();
    	args.putParcelable(ARGS_PARAMS, params);
		activity.getLoaderManager().restartLoader(LOADER_DELETE_FAVOURITE, args, this);
		
		if(activity.getComponentName().getClassName().equals(CartActivity.class.getName()) )
			((CartActivity)activity).reloadFavourites();
	}
	
	public void registerGcm(Context context) {
		final String regId = GCMRegistrar.getRegistrationId(context);
		if (!regId.equals(mGcmRegId)) {
			// Run async task to send registration ID to server
			new RegisterGCMTask(mConsumerKey, mAccessToken, regId, context).execute();
			mGcmRegId = regId;
		}
	}
	
	private class RegisterGCMTask extends AsyncTask<Void, Void, Void> {
		String mConsumerKey;
		String mAccessToken;
		String mRegId;
		Context mContext;
		
		public RegisterGCMTask(String consumerKey, String accessToken, String regId, Context context) {
			mConsumerKey = consumerKey;
			mAccessToken = accessToken;
			mRegId = regId;
			mContext = context;
		}
		
		@Override
		protected Void doInBackground(Void... params) {
			HttpPost request = new HttpPost();
			HttpClient client = new AppEngineHttpClient(mContext);
			try {
				// Set consumer URI
				request.setURI(new URI(URI + mConsumerKey));
				// Set parameters as json string
				JSONObject postParams = new JSONObject();
				postParams.put("gcm_registration_id", mRegId);
				request.setHeader("Content-Type", "application/json");
	        	request.setEntity(new StringEntity(postParams.toString(), "UTF-8"));
	        	// Add consumer access token
	        	if (mAccessToken != null) {
	        		request.setHeader("Authorization", "Basic "+Base64.encodeToString(mAccessToken.getBytes("UTF-8"), Base64.NO_WRAP));
	        	}
	        	// Execute request
				HttpResponse response = client.execute(request);
			}
			catch (URISyntaxException e) {
				Log.e(TAG, "URI syntax was incorrect.", e);
			}
			catch (UnsupportedEncodingException e) {
				Log.e(TAG, "A UrlEncodedFormEntity was created with an unsupported encoding.", e);
			}
			catch (ClientProtocolException e) {
	            Log.e(TAG, "There was a problem when sending the request.", e);
	        }
			catch (IOException e) {
	            Log.e(TAG, "There was a problem when sending the request.", e);
	        }
			catch (JSONException e) {
	    		Log.e(TAG, "Failed to create JSON.", e);
	    	}
			return null;
		}
	}
}
